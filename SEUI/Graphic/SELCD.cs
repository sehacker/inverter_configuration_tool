using SEUI.Base;
using SEUI.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace SEUI.Graphic
{
	public class SELCD : SEPictureBox, ISEUIGraphic, ISEUI
	{
		private const int LETTER_SPACING = 1;

		private const int NUM_OF_LETTERS_IN_ROW = 20;

		private const int NUM_OF_ROWS = 4;

		private IContainer components = null;

		private static Color COLOR_TRANSPARENT = Color.Blue;

		private static Color COLOR_TEXT = Color.White;

		private List<string> _lines = null;

		private List<string> _previousLines = null;

		private bool _shouldUpdateText = false;

		private Font _font;

		private float _nextDrawingX = 0f;

		private float _nextDrawingY = 0f;

		private float _letterWidth = 0f;

		private float _letterHeight = 0f;

		private Bitmap _backCanvas;

		private Graphics _backG;

		private SolidBrush _backBrush;

		private Bitmap _textCanvas;

		private Graphics _textG;

		private SolidBrush _textBrush;

		public List<string> Lines
		{
			get
			{
				return this._lines;
			}
			set
			{
				this._previousLines = this._lines;
				this._lines = value;
				this._shouldUpdateText = true;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}

		public SELCD()
		{
			this.InitializeComponent();
			this._font = new Font(FontFamily.GenericSansSerif, 10f, FontStyle.Bold);
		}

		private void ReleaseBackMemory()
		{
			if (this._backBrush != null)
			{
				this._backBrush.Dispose();
				this._backBrush = null;
			}
			if (this._backG != null)
			{
				this._backG.Dispose();
				this._backG = null;
			}
			if (this._backCanvas != null)
			{
				this._backCanvas.Dispose();
				this._backCanvas = null;
			}
		}

		private void ReleaseTextMemory()
		{
			if (this._textBrush != null)
			{
				this._textBrush.Dispose();
				this._textBrush = null;
			}
			if (this._textG != null)
			{
				this._textG.Dispose();
				this._textG = null;
			}
			if (this._textCanvas != null)
			{
				this._textCanvas.Dispose();
				this._textCanvas = null;
			}
		}

		private void ClearTextCanvas()
		{
			this.ReleaseTextMemory();
			this._nextDrawingX = 0f;
			this._nextDrawingY = 0f;
			this._textCanvas = new Bitmap(base.Width, base.Height);
			this._textG = Graphics.FromImage(this._textCanvas);
			this._textBrush = new SolidBrush(SELCD.COLOR_TEXT);
			this._textG.FillRectangle(this._backBrush, 0, 0, base.Width, base.Height);
		}

		private void DrawBackground()
		{
			if (this._backCanvas == null || this._backG == null)
			{
				this._backCanvas = new Bitmap(base.Size.Width, base.Size.Height);
				this._backG = Graphics.FromImage(this._backCanvas);
				this._backBrush = new SolidBrush(SELCD.COLOR_TRANSPARENT);
				this._backG.FillRectangle(this._backBrush, 0, 0, base.Width, base.Height);
			}
		}

		private void DrawText()
		{
			if (this._shouldUpdateText)
			{
				int num = 19;
				int num2 = 3;
				this._letterWidth = (float)(base.Width - num) / 20f;
				this._letterHeight = (float)(base.Height - num2) / 4f;
				this.ClearTextCanvas();
				Bitmap image = new Bitmap(base.Width, base.Height);
				Graphics graphics = Graphics.FromImage(image);
				int num3 = (this._lines != null) ? this._lines.Count : 0;
				for (int i = 0; i < num3; i++)
				{
					string text = this._lines[i];
					char[] array = text.ToCharArray();
					int num4 = array.Length;
					for (int j = 0; j < num4; j++)
					{
						this.DrawLetter(array[j], j, i);
					}
				}
			}
		}

		private void DrawLetter(char c, int pos, int lineNum)
		{
			if (pos % 20 == 0)
			{
				this._nextDrawingX = 0f;
				this._nextDrawingY = (float)lineNum * (this._letterHeight + 1f);
			}
			if (c.Equals('#'))
			{
				c = ' ';
			}
			string s = c.ToString();
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Near;
			stringFormat.LineAlignment = StringAlignment.Center;
			RectangleF layoutRectangle = new RectangleF(this._nextDrawingX, this._nextDrawingY, this._letterWidth, this._letterHeight);
			this._textG.DrawString(s, this._font, this._textBrush, layoutRectangle, stringFormat);
			this._nextDrawingX += this._letterWidth + 1f;
		}

		private void Flip(Graphics g)
		{
			ImageAttributes imageAttributes = new ImageAttributes();
			imageAttributes.SetColorKey(SELCD.COLOR_TRANSPARENT, SELCD.COLOR_TRANSPARENT);
			g.DrawImage(this._backCanvas, new Rectangle(0, 0, base.Width, base.Height), 0, 0, this._backCanvas.Width, this._backCanvas.Height, GraphicsUnit.Pixel, imageAttributes);
			g.DrawImage(this._textCanvas, new Rectangle(0, 0, base.Width, base.Height), 0, 0, this._backCanvas.Width, this._backCanvas.Height, GraphicsUnit.Pixel, imageAttributes);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			this.DrawBackground();
			this.DrawText();
			this.Flip(e.Graphics);
			base.OnPaint(e);
		}
	}
}
