using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace SEUI.Data
{
	public class SEDataGridView : DataGridView, ISEUIData, ISEUI
	{
		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = true;

		private SEDataGridViewThreadSafeAccess _tsAccess = null;

		private IContainer components = null;

		public SEDataGridViewThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SEDataGridViewThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		public SEDataGridView()
		{
			this.InitializeComponent();
			base.Rows.Clear();
			base.Columns.Clear();
		}

		public List<int> GetIndicesOfVisibleColumns()
		{
			List<int> list = new List<int>(0);
			if (base.Columns != null)
			{
				int count = base.Columns.Count;
				for (int i = 0; i < count; i++)
				{
					DataGridViewColumn dataGridViewColumn = base.Columns[i];
					if (dataGridViewColumn.Visible)
					{
						list.Add(i);
					}
				}
			}
			return list;
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}
	}
}
