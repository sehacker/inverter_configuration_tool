using System;
using System.Drawing;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEControlThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		private SEControlCollectionThreadSafeAccess _tsCollection = null;

		public ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public string Name
		{
			get
			{
				return (string)this.TSControl.Get("Name");
			}
			set
			{
				this.TSControl.Set("Name", value);
			}
		}

		public string Text
		{
			get
			{
				return (string)this.TSControl.Get("Text");
			}
			set
			{
				this.TSControl.Set("Text", value);
			}
		}

		public Font Font
		{
			get
			{
				return (Font)this.TSControl.Get("Font");
			}
			set
			{
				this.TSControl.Set("Font", value);
			}
		}

		public Point Location
		{
			get
			{
				return (Point)this.TSControl.Get("Location");
			}
			set
			{
				this.TSControl.Set("Location", value);
			}
		}

		public Size Size
		{
			get
			{
				return (Size)this.TSControl.Get("Size");
			}
			set
			{
				this.TSControl.Set("Size", value);
			}
		}

		public Color ForeColor
		{
			get
			{
				return (Color)this.TSControl.Get("ForeColor");
			}
			set
			{
				this.TSControl.Set("ForeColor", value);
			}
		}

		public Color BackColor
		{
			get
			{
				return (Color)this.TSControl.Get("BackColor");
			}
			set
			{
				this.TSControl.Set("BackColor", value);
			}
		}

		public bool Enabled
		{
			get
			{
				return (bool)this.TSControl.Get("Enabled");
			}
			set
			{
				this.TSControl.Set("Enabled", value);
			}
		}

		public bool Visible
		{
			get
			{
				return (bool)this.TSControl.Get("Visible");
			}
			set
			{
				this.TSControl.Set("Visible", value);
			}
		}

		public bool Focused
		{
			get
			{
				return (bool)this.TSControl.Get("Focused");
			}
			set
			{
				this.TSControl.Set("Focused", value);
			}
		}

		public Image BackgroundImage
		{
			get
			{
				return (Image)this.TSControl.Get("BackgroundImage");
			}
			set
			{
				this.TSControl.Set("BackgroundImage", value);
			}
		}

		public ImageLayout BackgroundImageLayout
		{
			get
			{
				return (ImageLayout)this.TSControl.Get("BackgroundImageLayout");
			}
			set
			{
				this.TSControl.Set("BackgroundImageLayout", value);
			}
		}

		public object Tag
		{
			get
			{
				return this.TSControl.Get("Tag");
			}
			set
			{
				this.TSControl.Set("Tag", value);
			}
		}

		public Control Parent
		{
			get
			{
				return (Control)this.TSControl.Get("Parent");
			}
			set
			{
				this.TSControl.Set("Parent", value);
			}
		}

		public Rectangle Bounds
		{
			get
			{
				return (Rectangle)this.TSControl.Get("Bounds");
			}
			set
			{
				this.TSControl.Set("Bounds", value);
			}
		}

		public Padding Margin
		{
			get
			{
				return (Padding)this.TSControl.Get("Margin");
			}
			set
			{
				this.TSControl.Set("Margin", value);
			}
		}

		public Padding Padding
		{
			get
			{
				return (Padding)this.TSControl.Get("Padding");
			}
			set
			{
				this.TSControl.Set("Padding", value);
			}
		}

		public SEControlCollectionThreadSafeAccess Controls
		{
			get
			{
				if (this._tsCollection == null)
				{
					this._tsCollection = new SEControlCollectionThreadSafeAccess(this.TSControl.Invoker);
				}
				return this._tsCollection;
			}
		}

		public Cursor Cursor
		{
			get
			{
				return (Cursor)this.TSControl.Get("Cursor");
			}
			set
			{
				this.TSControl.Set("Cursor", value);
			}
		}

		public void Refresh()
		{
			this.TSControl.InvokeMethod("Refresh", null);
		}

		public void Focus()
		{
			this.TSControl.InvokeMethod("Focus", null);
		}

		public SEControlThreadSafeAccess(Control control)
		{
			this.TSControl = new ThreadSafeControl(control, control);
		}
	}
}
