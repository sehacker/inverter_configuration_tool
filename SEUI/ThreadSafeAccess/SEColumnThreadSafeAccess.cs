using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEColumnThreadSafeAccess
	{
		private SECellCollectionThreadSafeAccess _tsCellCollection = null;

		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public int DisplayIndex
		{
			get
			{
				return (int)this.TSControl.Get("DisplayIndex");
			}
			set
			{
				this.TSControl.Set("DisplayIndex", value);
			}
		}

		public string HeaderText
		{
			get
			{
				return (string)this.TSControl.Get("HeaderText");
			}
			set
			{
				this.TSControl.Set("HeaderText", value);
			}
		}

		public int Index
		{
			get
			{
				return (int)this.TSControl.Get("Index");
			}
			set
			{
				this.TSControl.Set("Index", value);
			}
		}

		public string Name
		{
			get
			{
				return (string)this.TSControl.Get("Name");
			}
			set
			{
				this.TSControl.Set("Name", value);
			}
		}

		public bool ReadOnly
		{
			get
			{
				return (bool)this.TSControl.Get("ReadOnly");
			}
			set
			{
				this.TSControl.Set("ReadOnly", value);
			}
		}

		public bool Resizable
		{
			get
			{
				return (bool)this.TSControl.Get("Resizable");
			}
			set
			{
				this.TSControl.Set("Resizable", value);
			}
		}

		public bool Selected
		{
			get
			{
				return (bool)this.TSControl.Get("Selected");
			}
			set
			{
				this.TSControl.Set("Selected", value);
			}
		}

		public DataGridViewColumnSortMode SortMode
		{
			get
			{
				return (DataGridViewColumnSortMode)this.TSControl.Get("SortMode");
			}
			set
			{
				this.TSControl.Set("SortMode", value);
			}
		}

		public string ToolTipText
		{
			get
			{
				return (string)this.TSControl.Get("ToolTipText");
			}
			set
			{
				this.TSControl.Set("ToolTipText", value);
			}
		}

		public ValueType ValueType
		{
			get
			{
				return (ValueType)this.TSControl.Get("ValueType");
			}
			set
			{
				this.TSControl.Set("ValueType", value);
			}
		}

		public bool Visible
		{
			get
			{
				return (bool)this.TSControl.Get("Visible");
			}
			set
			{
				this.TSControl.Set("Visible", value);
			}
		}

		public int Width
		{
			get
			{
				return (int)this.TSControl.Get("Width");
			}
			set
			{
				this.TSControl.Set("Width", value);
			}
		}

		public SEColumnThreadSafeAccess(DataGridViewColumn dgvColumn)
		{
			this.TSControl = new ThreadSafeControl(dgvColumn, dgvColumn.DataGridView);
		}
	}
}
