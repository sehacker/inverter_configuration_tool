using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SECellThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public int ColumnIndex
		{
			get
			{
				return (int)this.TSControl.Get("ColumnIndex");
			}
		}

		public bool ReadOnly
		{
			get
			{
				return (bool)this.TSControl.Get("ReadOnly");
			}
			set
			{
				this.TSControl.Set("ReadOnly", value);
			}
		}

		public bool Resizable
		{
			get
			{
				return (bool)this.TSControl.Get("Resizable");
			}
			set
			{
				this.TSControl.Set("Resizable", value);
			}
		}

		public int RowIndex
		{
			get
			{
				return (int)this.TSControl.Get("RowIndex");
			}
		}

		public bool Selected
		{
			get
			{
				return (bool)this.TSControl.Get("Selected");
			}
			set
			{
				this.TSControl.Set("Selected", value);
			}
		}

		public DataGridViewCellStyle Style
		{
			get
			{
				return (DataGridViewCellStyle)this.TSControl.Get("Style");
			}
			set
			{
				this.TSControl.Set("Style", value);
			}
		}

		public string ToolTipText
		{
			get
			{
				return (string)this.TSControl.Get("ToolTipText");
			}
			set
			{
				this.TSControl.Set("ToolTipText", value);
			}
		}

		public object Value
		{
			get
			{
				return this.TSControl.Get("Value");
			}
			set
			{
				this.TSControl.Set("Value", value);
			}
		}

		public ValueType ValueType
		{
			get
			{
				return (ValueType)this.TSControl.Get("ValueType");
			}
			set
			{
				this.TSControl.Set("ValueType", value);
			}
		}

		public bool Visible
		{
			get
			{
				return (bool)this.TSControl.Get("Visible");
			}
			set
			{
				this.TSControl.Set("Visible", value);
			}
		}

		public SECellThreadSafeAccess(DataGridViewCell dgvCell)
		{
			this.TSControl = new ThreadSafeControl(dgvCell, dgvCell.DataGridView);
		}
	}
}
