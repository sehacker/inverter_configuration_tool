using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEColumnCollectionThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public int Count
		{
			get
			{
				return (int)this.TSControl.Get("Count");
			}
			set
			{
				this.TSControl.Set("Count", value);
			}
		}

		public SEColumnThreadSafeAccess this[int index]
		{
			get
			{
				SEColumnThreadSafeAccess result = null;
				try
				{
					DataGridViewColumn dgvColumn = (DataGridViewColumn)this.TSControl.Get("Item", new object[]
					{
						index
					});
					result = new SEColumnThreadSafeAccess(dgvColumn);
				}
				catch (Exception var_2_38)
				{
				}
				return result;
			}
		}

		public SEColumnThreadSafeAccess this[string columnName]
		{
			get
			{
				SEColumnThreadSafeAccess result = null;
				try
				{
					DataGridViewColumn dgvColumn = (DataGridViewColumn)this.TSControl.Get("Item", new object[]
					{
						columnName
					});
					result = new SEColumnThreadSafeAccess(dgvColumn);
				}
				catch (Exception var_2_33)
				{
				}
				return result;
			}
		}

		public void Add(DataGridViewColumn dataGridViewColumn)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				dataGridViewColumn
			});
		}

		public void Add(string columnName, string headerText)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				columnName,
				headerText
			});
		}

		public void Clear()
		{
			this.TSControl.InvokeMethod("Clear", null);
		}

		public bool Contains(DataGridViewColumn dataGridViewColumn)
		{
			return (bool)this.TSControl.InvokeMethod("Contains", new object[]
			{
				dataGridViewColumn
			});
		}

		public bool Contains(string columnName)
		{
			return (bool)this.TSControl.InvokeMethod("Contains", new object[]
			{
				columnName
			});
		}

		public int IndexOf(DataGridViewColumn dataGridViewColumn)
		{
			return (int)this.TSControl.InvokeMethod("IndexOf", new object[]
			{
				dataGridViewColumn
			});
		}

		public int Insert(int columnIndex, DataGridViewColumn dataGridViewColumn)
		{
			return (int)this.TSControl.InvokeMethod("Insert", new object[]
			{
				columnIndex,
				dataGridViewColumn
			});
		}

		public void Remove(DataGridViewColumn dataGridViewColumn)
		{
			this.TSControl.InvokeMethod("Remove", new object[]
			{
				dataGridViewColumn
			});
		}

		public void Remove(string columnName)
		{
			this.TSControl.InvokeMethod("Remove", new object[]
			{
				columnName
			});
		}

		public void RemoveAt(int index)
		{
			this.TSControl.InvokeMethod("RemoveAt", new object[]
			{
				index
			});
		}

		public SEColumnCollectionThreadSafeAccess(DataGridView dgv)
		{
			this.TSControl = new ThreadSafeControl(dgv.Columns, dgv);
		}
	}
}
