using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SECellCollectionThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public SECellThreadSafeAccess this[int columnIndex]
		{
			get
			{
				SECellThreadSafeAccess result = null;
				try
				{
					DataGridViewCell dgvCell = (DataGridViewCell)this.TSControl.Get("Item", new object[]
					{
						columnIndex
					});
					result = new SECellThreadSafeAccess(dgvCell);
				}
				catch (Exception var_2_38)
				{
				}
				return result;
			}
		}

		public SECellThreadSafeAccess this[string columnName]
		{
			get
			{
				SECellThreadSafeAccess result = null;
				try
				{
					DataGridViewCell dgvCell = (DataGridViewCell)this.TSControl.Get("Item", new object[]
					{
						columnName
					});
					result = new SECellThreadSafeAccess(dgvCell);
				}
				catch (Exception var_2_33)
				{
				}
				return result;
			}
		}

		public int Count
		{
			get
			{
				return (int)this.TSControl.Get("Count");
			}
			set
			{
				this.TSControl.Set("Count", value);
			}
		}

		public void Add(DataGridViewCell dataGridViewCell)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				dataGridViewCell
			});
		}

		public void Clear()
		{
			this.TSControl.InvokeMethod("Clear", null);
		}

		public bool Contains(DataGridViewCell dataGridViewCell)
		{
			return (bool)this.TSControl.InvokeMethod("Contains", new object[]
			{
				dataGridViewCell
			});
		}

		public SECellCollectionThreadSafeAccess(DataGridViewRow dgvRow)
		{
			this.TSControl = new ThreadSafeControl(dgvRow.Cells, dgvRow.DataGridView);
		}
	}
}
