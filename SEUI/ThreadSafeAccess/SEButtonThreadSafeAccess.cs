using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEButtonThreadSafeAccess : SEControlThreadSafeAccess
	{
		public bool UseVisualStylesBackColor
		{
			get
			{
				return (bool)base.TSControl.Get("UseVisualStylesBackColor");
			}
			set
			{
				base.TSControl.Set("UseVisualStylesBackColor", value);
			}
		}

		public SEButtonThreadSafeAccess(Button button) : base(button)
		{
		}
	}
}
