using System;
using System.Drawing;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SETabControlThreadSafeAccess : SEControlThreadSafeAccess
	{
		private SETabPageCollectionThreadSafeAccess _tsTabPageCollection = null;

		public TabAppearance TabAppearance
		{
			get
			{
				return (TabAppearance)base.TSControl.Get("TabAppearance");
			}
			set
			{
				base.TSControl.Set("TabAppearance", value);
			}
		}

		public TabDrawMode DrawMode
		{
			get
			{
				return (TabDrawMode)base.TSControl.Get("DrawMode");
			}
			set
			{
				base.TSControl.Set("DrawMode", value);
			}
		}

		public bool HotTrack
		{
			get
			{
				return (bool)base.TSControl.Get("HotTrack");
			}
			set
			{
				base.TSControl.Set("HotTrack", value);
			}
		}

		public ImageList ImageList
		{
			get
			{
				return (ImageList)base.TSControl.Get("ImageList");
			}
			set
			{
				base.TSControl.Set("ImageList", value);
			}
		}

		public Size ItemSize
		{
			get
			{
				return (Size)base.TSControl.Get("ItemSize");
			}
			set
			{
				base.TSControl.Set("ItemSize", value);
			}
		}

		public bool Multiline
		{
			get
			{
				return (bool)base.TSControl.Get("Multiline");
			}
			set
			{
				base.TSControl.Set("Multiline", value);
			}
		}

		public int RowCount
		{
			get
			{
				return (int)base.TSControl.Get("RowCount");
			}
			set
			{
				base.TSControl.Set("RowCount", value);
			}
		}

		public int SelectedIndex
		{
			get
			{
				return (int)base.TSControl.Get("SelectedIndex");
			}
			set
			{
				base.TSControl.Set("SelectedIndex", value);
			}
		}

		public TabPage SelectedTab
		{
			get
			{
				return (TabPage)base.TSControl.Get("SelectedTab");
			}
			set
			{
				base.TSControl.Set("SelectedTab", value);
			}
		}

		public bool ShowToolTips
		{
			get
			{
				return (bool)base.TSControl.Get("ShowToolTips");
			}
			set
			{
				base.TSControl.Set("ShowToolTips", value);
			}
		}

		public TabSizeMode SizeMode
		{
			get
			{
				return (TabSizeMode)base.TSControl.Get("SizeMode");
			}
			set
			{
				base.TSControl.Set("SizeMode", value);
			}
		}

		public int TabCount
		{
			get
			{
				return (int)base.TSControl.Get("TabCount");
			}
			set
			{
				base.TSControl.Set("TabCount", value);
			}
		}

		public SETabPageCollectionThreadSafeAccess TabPages
		{
			get
			{
				if (this._tsTabPageCollection == null)
				{
					this._tsTabPageCollection = new SETabPageCollectionThreadSafeAccess((TabControl)base.TSControl.Invoker);
				}
				return this._tsTabPageCollection;
			}
		}

		public SETabControlThreadSafeAccess(TabControl tbc) : base(tbc)
		{
		}
	}
}
