using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEImageCellThreadSafeAccess : SECellThreadSafeAccess
	{
		public DataGridViewImageCellLayout ImageLayout
		{
			get
			{
				return (DataGridViewImageCellLayout)base.TSControl.Get("ImageLayout");
			}
			set
			{
				base.TSControl.Set("ImageLayout", value);
			}
		}

		public SEImageCellThreadSafeAccess(DataGridViewImageCell dgvImageCell) : base(dgvImageCell)
		{
		}
	}
}
