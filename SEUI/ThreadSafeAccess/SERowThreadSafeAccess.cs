using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SERowThreadSafeAccess
	{
		private SECellCollectionThreadSafeAccess _tsCellCollection = null;

		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public int Index
		{
			get
			{
				return (int)this.TSControl.Get("Index");
			}
		}

		public bool ReadOnly
		{
			get
			{
				return (bool)this.TSControl.Get("ReadOnly");
			}
			set
			{
				this.TSControl.Set("ReadOnly", value);
			}
		}

		public bool Resizable
		{
			get
			{
				return (bool)this.TSControl.Get("Resizable");
			}
			set
			{
				this.TSControl.Set("Resizable", value);
			}
		}

		public bool Selected
		{
			get
			{
				return (bool)this.TSControl.Get("Selected");
			}
			set
			{
				this.TSControl.Set("Selected", value);
			}
		}

		public bool Visible
		{
			get
			{
				return (bool)this.TSControl.Get("Visible");
			}
			set
			{
				this.TSControl.Set("Visible", value);
			}
		}

		public SECellCollectionThreadSafeAccess Cells
		{
			get
			{
				if (this._tsCellCollection == null)
				{
					this._tsCellCollection = new SECellCollectionThreadSafeAccess((DataGridViewRow)this.TSControl.Accessor);
				}
				return this._tsCellCollection;
			}
		}

		public DataGridViewCellStyle DefaultCellStyle
		{
			get
			{
				return (DataGridViewCellStyle)this.TSControl.Get("DefaultCellStyle");
			}
			set
			{
				this.TSControl.Set("DefaultCellStyle", value);
			}
		}

		public SERowThreadSafeAccess(DataGridViewRow dgvRow)
		{
			this.TSControl = new ThreadSafeControl(dgvRow, dgvRow.DataGridView);
		}

		public void SetValues(params object[] values)
		{
			this.TSControl.InvokeMethod("SetValues", values);
		}
	}
}
