using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SENumericUpDownThreadSafeAccess : SEControlThreadSafeAccess
	{
		public int DecimalPlaces
		{
			get
			{
				return (int)base.TSControl.Get("DecimalPlaces");
			}
			set
			{
				base.TSControl.Set("DecimalPlaces", value);
			}
		}

		public bool Hexadecimal
		{
			get
			{
				return (bool)base.TSControl.Get("Hexadecimal");
			}
			set
			{
				base.TSControl.Set("Hexadecimal", value);
			}
		}

		public int Increment
		{
			get
			{
				return (int)base.TSControl.Get("Increment");
			}
			set
			{
				base.TSControl.Set("Increment", value);
			}
		}

		public bool InterceptArrowKeys
		{
			get
			{
				return (bool)base.TSControl.Get("InterceptArrowKeys");
			}
			set
			{
				base.TSControl.Set("InterceptArrowKeys", value);
			}
		}

		public int Minimum
		{
			get
			{
				return (int)base.TSControl.Get("Minimum");
			}
			set
			{
				base.TSControl.Set("Minimum", value);
			}
		}

		public int Maximum
		{
			get
			{
				return (int)base.TSControl.Get("Maximum");
			}
			set
			{
				base.TSControl.Set("Maximum", value);
			}
		}

		public bool ThousandsSeparator
		{
			get
			{
				return (bool)base.TSControl.Get("ThousandsSeparator");
			}
			set
			{
				base.TSControl.Set("ThousandsSeparator", value);
			}
		}

		public decimal Value
		{
			get
			{
				return (decimal)base.TSControl.Get("Value");
			}
			set
			{
				base.TSControl.Set("Value", value);
			}
		}

		public SENumericUpDownThreadSafeAccess(NumericUpDown nud) : base(nud)
		{
		}
	}
}
