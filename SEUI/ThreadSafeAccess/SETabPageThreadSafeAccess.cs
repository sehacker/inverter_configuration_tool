using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SETabPageThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public string Name
		{
			get
			{
				return (string)this.TSControl.Get("Name");
			}
			set
			{
				this.TSControl.Set("Name", value);
			}
		}

		public SETabPageThreadSafeAccess(TabPage page, TabControl tbc)
		{
			this.TSControl = new ThreadSafeControl(page, tbc);
		}
	}
}
