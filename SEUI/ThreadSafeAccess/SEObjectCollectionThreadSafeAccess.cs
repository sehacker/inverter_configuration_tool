using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEObjectCollectionThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public int Count
		{
			get
			{
				return (int)this.TSControl.Get("Count");
			}
			set
			{
				this.TSControl.Set("Count", value);
			}
		}

		public object this[int index]
		{
			get
			{
				object result = null;
				try
				{
					result = this.TSControl.Get("Item", new object[]
					{
						index
					});
				}
				catch (Exception var_1_29)
				{
				}
				return result;
			}
		}

		public void Add(object value)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				value
			});
		}

		public void AddRange(object[] controls)
		{
			this.TSControl.InvokeMethod("AddRange", new object[]
			{
				controls
			});
		}

		public void Clear()
		{
			this.TSControl.InvokeMethod("Clear", null);
		}

		public bool Contains(object item)
		{
			return (bool)this.TSControl.InvokeMethod("Contains", new object[]
			{
				item
			});
		}

		public int IndexOf(object item)
		{
			return (int)this.TSControl.InvokeMethod("IndexOf", new object[]
			{
				item
			});
		}

		public void Remove(object value)
		{
			this.TSControl.InvokeMethod("Remove", new object[]
			{
				value
			});
		}

		public void RemoveAt(int index)
		{
			this.TSControl.InvokeMethod("RemoveAt", new object[]
			{
				index
			});
		}

		public SEObjectCollectionThreadSafeAccess(ComboBox comboBox)
		{
			this.TSControl = new ThreadSafeControl(comboBox.Items, comboBox);
		}
	}
}
