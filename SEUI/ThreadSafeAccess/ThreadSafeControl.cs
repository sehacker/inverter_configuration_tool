using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class ThreadSafeControl
	{
		private delegate object GetDelegate(string propertyName, object[] index);

		private delegate void SetDelegate(string propertyName, object propertyValue, object[] index);

		private delegate object InvokeMethodDelegate(string propertyName, params object[] parameters);

		private const int THREAD_SLEEP_TIME_MS = 10;

		private Control _enclosingInvoker = null;

		private object _enclosingAccessor = null;

		private bool _shouldExitGet = false;

		private bool _wasGetInvoked = false;

		private bool _shouldExitSet = false;

		private bool _wasSetInvoked = false;

		private bool _shouldExitMethodInvoke = false;

		private bool _wasMethodInvoked = false;

		private object _retValGet = null;

		private object _retValMethodInvoke = null;

		public Control Invoker
		{
			get
			{
				return this._enclosingInvoker;
			}
			set
			{
				this._enclosingInvoker = value;
			}
		}

		public object Accessor
		{
			get
			{
				return this._enclosingAccessor;
			}
		}

		public ThreadSafeControl(object accessor, Control invoker)
		{
			if (accessor == null)
			{
				throw new NullReferenceException("Accessor cannot be null!");
			}
			if (invoker == null)
			{
				throw new NullReferenceException("Invoker cannot be null!");
			}
			this._enclosingAccessor = accessor;
			this._enclosingInvoker = invoker;
		}

		public object Get(string propertyName)
		{
			return this.Get(propertyName, null);
		}

		public object Get(string propertyName, object[] index)
		{
			if (this._enclosingInvoker.InvokeRequired)
			{
				this._wasGetInvoked = true;
				bool flag = false;
				while (!this._shouldExitGet)
				{
					if (!flag)
					{
						flag = true;
						this._enclosingInvoker.BeginInvoke(new ThreadSafeControl.GetDelegate(this.Get), new object[]
						{
							propertyName,
							index
						});
					}
					Thread.Sleep(10);
				}
				this._shouldExitGet = false;
			}
			else
			{
				try
				{
					this._retValGet = this.GetUnsafe(propertyName, index);
				}
				catch (Exception var_1_8A)
				{
				}
				this._shouldExitGet = this._wasGetInvoked;
			}
			this._wasGetInvoked = false;
			return this._retValGet;
		}

		public void Set(string propertyName, object properyValue)
		{
			this.Set(propertyName, properyValue, null);
		}

		public void Set(string propertyName, object properyValue, object[] index)
		{
			if (this._enclosingInvoker.InvokeRequired)
			{
				this._wasSetInvoked = true;
				bool flag = false;
				while (!this._shouldExitSet)
				{
					if (!flag)
					{
						flag = true;
						this._enclosingInvoker.BeginInvoke(new ThreadSafeControl.SetDelegate(this.Set), new object[]
						{
							propertyName,
							properyValue,
							index
						});
					}
					Thread.Sleep(10);
				}
				this._shouldExitSet = false;
			}
			else
			{
				try
				{
					this.SetUnsafe(propertyName, properyValue, index);
				}
				catch (Exception var_1_86)
				{
				}
				this._shouldExitSet = this._wasSetInvoked;
			}
			this._wasSetInvoked = false;
		}

		public object InvokeMethod(string methodName, params object[] parameters)
		{
			if (this._enclosingInvoker.InvokeRequired)
			{
				this._wasMethodInvoked = true;
				bool flag = false;
				while (!this._shouldExitMethodInvoke)
				{
					if (!flag)
					{
						flag = true;
						this._enclosingInvoker.BeginInvoke(new ThreadSafeControl.InvokeMethodDelegate(this.InvokeMethod), new object[]
						{
							methodName,
							parameters
						});
					}
					Thread.Sleep(10);
				}
				this._shouldExitMethodInvoke = false;
			}
			else
			{
				try
				{
					this._retValMethodInvoke = this.InvokeMethodUnsafe(methodName, parameters);
				}
				catch (Exception var_1_8A)
				{
				}
				this._shouldExitMethodInvoke = this._wasMethodInvoked;
			}
			this._wasMethodInvoked = false;
			return this._retValMethodInvoke;
		}

		private object GetUnsafe(string propertyName, object[] index)
		{
			object result = null;
			Type type = this._enclosingAccessor.GetType();
			List<Type> list = new List<Type>(0);
			if (index != null)
			{
				int num = index.Length;
				for (int i = 0; i < num; i++)
				{
					list.Add(index[i].GetType());
				}
			}
			PropertyInfo property = type.GetProperty(propertyName, list.ToArray());
			if (property == null)
			{
				throw new ArgumentException("No such property exists!");
			}
			if (property != null && property.CanRead)
			{
				result = property.GetValue(this._enclosingAccessor, index);
			}
			return result;
		}

		private void SetUnsafe(string propertyName, object propertyValue, object[] index)
		{
			if (propertyValue == null && !this.IsPropertyNullable(propertyName))
			{
				throw new ArgumentException("Property may not be assigned a NULL value!");
			}
			Type type = this._enclosingAccessor.GetType();
			List<Type> list = new List<Type>(0);
			if (index != null)
			{
				int num = index.Length;
				for (int i = 0; i < num; i++)
				{
					list.Add(index[i].GetType());
				}
			}
			PropertyInfo property = type.GetProperty(propertyName, list.ToArray());
			if (property == null)
			{
				throw new ArgumentException("No such property exists!");
			}
			if (property != null && property.CanWrite)
			{
				property.SetValue(this._enclosingAccessor, propertyValue, index);
			}
		}

		private object InvokeMethodUnsafe(string methodName, params object[] parameters)
		{
			Type type = this._enclosingAccessor.GetType();
			List<Type> list = new List<Type>(0);
			int num = (parameters != null) ? parameters.Length : 0;
			if (num > 0)
			{
				for (int i = 0; i < num; i++)
				{
					Type item = (parameters[i] != null) ? parameters[i].GetType() : null;
					list.Add(item);
				}
			}
			MethodInfo method = type.GetMethod(methodName, list.ToArray());
			if (method == null)
			{
				throw new Exception("No such method was found matching given name and parameters!");
			}
			parameters = ((parameters == null) ? new object[0] : parameters);
			return method.Invoke(this._enclosingAccessor, parameters);
		}

		private bool IsPropertyNullable(string propertyName)
		{
			Type type = this._enclosingAccessor.GetType();
			PropertyInfo property = type.GetProperty(propertyName);
			if (property == null)
			{
				throw new ArgumentException("No such property exists!");
			}
			Type propertyType = property.PropertyType;
			return !propertyType.IsValueType || Nullable.GetUnderlyingType(propertyType) != null;
		}
	}
}
