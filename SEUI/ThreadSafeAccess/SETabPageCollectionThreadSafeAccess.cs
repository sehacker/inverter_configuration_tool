using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SETabPageCollectionThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return (bool)this.TSControl.Get("IsReadOnly");
			}
			set
			{
				this.TSControl.Set("IsReadOnly", value);
			}
		}

		public int Count
		{
			get
			{
				return (int)this.TSControl.Get("Count");
			}
			set
			{
				this.TSControl.Set("Count", value);
			}
		}

		public SETabPageThreadSafeAccess this[int index]
		{
			get
			{
				SETabPageThreadSafeAccess result = null;
				try
				{
					TabPage page = (TabPage)this.TSControl.Get("Item", new object[]
					{
						index
					});
					result = new SETabPageThreadSafeAccess(page, (TabControl)this.TSControl.Invoker);
				}
				catch (Exception var_2_48)
				{
				}
				return result;
			}
		}

		public SETabPageThreadSafeAccess this[string key]
		{
			get
			{
				SETabPageThreadSafeAccess result = null;
				try
				{
					TabPage page = (TabPage)this.TSControl.Get("Item", new object[]
					{
						key
					});
					result = new SETabPageThreadSafeAccess(page, (TabControl)this.TSControl.Invoker);
				}
				catch (Exception var_2_43)
				{
				}
				return result;
			}
		}

		public void Add(string text)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				text
			});
		}

		public void Add(TabPage value)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				value
			});
		}

		public void Add(string key, string text)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				key,
				text
			});
		}

		public void Add(string key, string text, int imageIndex)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				key,
				text,
				imageIndex
			});
		}

		public void Add(string key, string text, string imageKey)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				key,
				text,
				imageKey
			});
		}

		public void AddRange(TabPage[] tabPages)
		{
			this.TSControl.InvokeMethod("AddRange", new object[]
			{
				tabPages
			});
		}

		public void Clear()
		{
			this.TSControl.InvokeMethod("Clear", null);
		}

		public bool Contains(TabPage page)
		{
			return (bool)this.TSControl.InvokeMethod("Contains", new object[]
			{
				page
			});
		}

		public bool ContainsKey(string key)
		{
			return (bool)this.TSControl.InvokeMethod("ContainsKey", new object[]
			{
				key
			});
		}

		public int IndexOf(TabPage page)
		{
			return (int)this.TSControl.InvokeMethod("IndexOf", new object[]
			{
				page
			});
		}

		public int IndexOfKey(string key)
		{
			return (int)this.TSControl.InvokeMethod("IndexOfKey", new object[]
			{
				key
			});
		}

		public void Insert(int index, string text)
		{
			this.TSControl.InvokeMethod("Insert", new object[]
			{
				index,
				text
			});
		}

		public void Insert(int index, TabPage tabPage)
		{
			this.TSControl.InvokeMethod("Insert", new object[]
			{
				index,
				tabPage
			});
		}

		public void Insert(int index, string key, string text)
		{
			this.TSControl.InvokeMethod("Insert", new object[]
			{
				index,
				key,
				text
			});
		}

		public void Insert(int index, string key, string text, int imageIndex)
		{
			this.TSControl.InvokeMethod("Insert", new object[]
			{
				index,
				key,
				text,
				imageIndex
			});
		}

		public void Insert(int index, string key, string text, string imageKey)
		{
			this.TSControl.InvokeMethod("Insert", new object[]
			{
				index,
				key,
				text,
				imageKey
			});
		}

		public void Remove(TabPage value)
		{
			this.TSControl.InvokeMethod("Remove", new object[]
			{
				value
			});
		}

		public void RemoveAt(int index)
		{
			this.TSControl.InvokeMethod("RemoveAt", new object[]
			{
				index
			});
		}

		public void RemoveByKey(string key)
		{
			this.TSControl.InvokeMethod("RemoveByKey", new object[]
			{
				key
			});
		}

		public SETabPageCollectionThreadSafeAccess(TabControl tbc)
		{
			this.TSControl = new ThreadSafeControl(tbc.TabPages, tbc);
		}
	}
}
