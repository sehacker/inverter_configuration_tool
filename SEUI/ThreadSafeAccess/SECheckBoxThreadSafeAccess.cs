using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SECheckBoxThreadSafeAccess : SEControlThreadSafeAccess
	{
		public bool Checked
		{
			get
			{
				return (bool)base.TSControl.Get("Checked");
			}
			set
			{
				base.TSControl.Set("Checked", value);
			}
		}

		public CheckState CheckState
		{
			get
			{
				return (CheckState)base.TSControl.Get("CheckState");
			}
			set
			{
				base.TSControl.Set("CheckState", value);
			}
		}

		public SECheckBoxThreadSafeAccess(CheckBox checkBox) : base(checkBox)
		{
		}
	}
}
