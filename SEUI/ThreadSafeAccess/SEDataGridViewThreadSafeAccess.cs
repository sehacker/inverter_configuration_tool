using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEDataGridViewThreadSafeAccess : SEControlThreadSafeAccess
	{
		private SEColumnCollectionThreadSafeAccess _tsColumnCollection = null;

		private SERowCollectionThreadSafeAccess _tsRowCollection = null;

		public Color BackgroundColor
		{
			get
			{
				return (Color)base.TSControl.Get("BackgroundColor");
			}
			set
			{
				base.TSControl.Set("BackgroundColor", value);
			}
		}

		public SEColumnCollectionThreadSafeAccess Columns
		{
			get
			{
				if (this._tsColumnCollection == null)
				{
					this._tsColumnCollection = new SEColumnCollectionThreadSafeAccess((DataGridView)base.TSControl.Invoker);
				}
				return this._tsColumnCollection;
			}
		}

		public SERowCollectionThreadSafeAccess Rows
		{
			get
			{
				if (this._tsRowCollection == null)
				{
					this._tsRowCollection = new SERowCollectionThreadSafeAccess((DataGridView)base.TSControl.Invoker);
				}
				return this._tsRowCollection;
			}
		}

		public object DataSource
		{
			get
			{
				return base.TSControl.Get("DataSource");
			}
			set
			{
				base.TSControl.Set("DataSource", value);
			}
		}

		public DataGridViewCell CurrentCell
		{
			get
			{
				return (DataGridViewCell)base.TSControl.Get("CurrentCell");
			}
			set
			{
				base.TSControl.Set("CurrentCell", value);
			}
		}

		public DataGridViewSelectedCellCollection SelectedCells
		{
			get
			{
				return (DataGridViewSelectedCellCollection)base.TSControl.Get("SelectedCells");
			}
			set
			{
				base.TSControl.Set("SelectedCells", value);
			}
		}

		public int FirstDisplayedScrollingRowIndex
		{
			get
			{
				return (int)base.TSControl.Get("FirstDisplayedScrollingRowIndex");
			}
			set
			{
				base.TSControl.Set("FirstDisplayedScrollingRowIndex", value);
			}
		}

		public SEDataGridViewThreadSafeAccess(DataGridView dgv) : base(dgv)
		{
		}

		public void Sort(DataGridViewColumn dgvCol, ListSortDirection direction)
		{
			base.TSControl.InvokeMethod("Sort", new object[]
			{
				dgvCol,
				direction
			});
		}
	}
}
