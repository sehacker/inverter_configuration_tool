using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEFormThreadSafeAccess : SEControlThreadSafeAccess
	{
		public SEFormThreadSafeAccess(Form form) : base(form)
		{
		}

		public DialogResult Show()
		{
			return (DialogResult)base.TSControl.InvokeMethod("Show", null);
		}

		public DialogResult Show(IWin32Window owner)
		{
			return (DialogResult)base.TSControl.InvokeMethod("Show", new object[]
			{
				owner
			});
		}

		public DialogResult ShowDialog()
		{
			return (DialogResult)base.TSControl.InvokeMethod("ShowDialog", null);
		}

		public DialogResult ShowDialog(IWin32Window owner)
		{
			return (DialogResult)base.TSControl.InvokeMethod("ShowDialog", new object[]
			{
				owner
			});
		}
	}
}
