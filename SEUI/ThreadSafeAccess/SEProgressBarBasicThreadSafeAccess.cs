using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEProgressBarBasicThreadSafeAccess : SEControlThreadSafeAccess
	{
		public int Minimum
		{
			get
			{
				return (int)base.TSControl.Get("Minimum");
			}
			set
			{
				base.TSControl.Set("Minimum", value);
			}
		}

		public int Maximum
		{
			get
			{
				return (int)base.TSControl.Get("Maximum");
			}
			set
			{
				base.TSControl.Set("Maximum", value);
			}
		}

		public int Value
		{
			get
			{
				return (int)base.TSControl.Get("Value");
			}
			set
			{
				base.TSControl.Set("Value", value);
			}
		}

		public int Step
		{
			get
			{
				return (int)base.TSControl.Get("Step");
			}
			set
			{
				base.TSControl.Set("Step", value);
			}
		}

		public ProgressBarStyle Style
		{
			get
			{
				return (ProgressBarStyle)base.TSControl.Get("Style");
			}
			set
			{
				base.TSControl.Set("Style", value);
			}
		}

		public SEProgressBarBasicThreadSafeAccess(ProgressBar bar) : base(bar)
		{
		}

		public void PerformStep()
		{
			base.TSControl.InvokeMethod("PerformStep", null);
		}
	}
}
