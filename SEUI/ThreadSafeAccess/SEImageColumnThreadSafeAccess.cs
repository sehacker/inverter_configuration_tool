using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEImageColumnThreadSafeAccess : SEColumnThreadSafeAccess
	{
		public DataGridViewImageCellLayout ImageLayout
		{
			get
			{
				return (DataGridViewImageCellLayout)base.TSControl.Get("ImageLayout");
			}
			set
			{
				base.TSControl.Set("ImageLayout", value);
			}
		}

		public SEImageColumnThreadSafeAccess(DataGridViewColumn dgvColumn) : base(dgvColumn)
		{
		}
	}
}
