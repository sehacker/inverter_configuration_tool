using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEControlCollectionThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public Control Owner
		{
			get
			{
				return (Control)this.TSControl.Get("Owner");
			}
			set
			{
				this.TSControl.Set("Owner", value);
			}
		}

		public int Count
		{
			get
			{
				return (int)this.TSControl.Get("Count");
			}
		}

		public SEControlThreadSafeAccess this[int index]
		{
			get
			{
				SEControlThreadSafeAccess result = null;
				try
				{
					Control control = (Control)this.TSControl.Get("Item", new object[]
					{
						index
					});
					result = new SEControlThreadSafeAccess(control);
				}
				catch (Exception var_2_38)
				{
				}
				return result;
			}
		}

		public SEControlThreadSafeAccess this[string key]
		{
			get
			{
				SEControlThreadSafeAccess result = null;
				try
				{
					Control control = (Control)this.TSControl.Get("Item", new object[]
					{
						key
					});
					result = new SEControlThreadSafeAccess(control);
				}
				catch (Exception var_2_33)
				{
				}
				return result;
			}
		}

		public void Add(Control value)
		{
			this.TSControl.InvokeMethod("Add", new object[]
			{
				value
			});
		}

		public void AddRange(Control[] controls)
		{
			this.TSControl.InvokeMethod("AddRange", new object[]
			{
				controls
			});
		}

		public void Clear()
		{
			this.TSControl.InvokeMethod("Clear", null);
		}

		public bool Contains(Control control)
		{
			return (bool)this.TSControl.InvokeMethod("Contains", new object[]
			{
				control
			});
		}

		public int IndexOf(Control control)
		{
			return (int)this.TSControl.InvokeMethod("IndexOf", new object[]
			{
				control
			});
		}

		public void Remove(Control value)
		{
			this.TSControl.InvokeMethod("Remove", new object[]
			{
				value
			});
		}

		public void RemoveAt(int index)
		{
			this.TSControl.InvokeMethod("RemoveAt", new object[]
			{
				index
			});
		}

		public SEControlCollectionThreadSafeAccess(Control control)
		{
			this.TSControl = new ThreadSafeControl(control.Controls, control);
		}
	}
}
