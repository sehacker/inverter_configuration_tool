using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SEUI.Common
{
	public class SELabel : Label, ISEUICommon, ISEUI
	{
		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = false;

		private SELabelThreadSafeAccess _tsAccess = null;

		private IContainer components = null;

		public SELabelThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SELabelThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		public SELabel()
		{
			this.InitializeComponent();
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}
	}
}
