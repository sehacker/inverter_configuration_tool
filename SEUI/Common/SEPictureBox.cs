using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEUI.Common
{
	public class SEPictureBox : PictureBox, ISEUICommon, ISEUI
	{
		private const int NO_SELECTED_IMAGE = -1;

		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = true;

		private List<Image> _imageListFore;

		private int _selectedImageFore = -1;

		private List<Image> _imageListBack;

		private int _selectedImageBack = -1;

		private SEPictureBoxThreadSafeAccess _tsAccess = null;

		private IContainer components = null;

		public List<Image> ImageListFore
		{
			get
			{
				return this._imageListFore;
			}
			set
			{
				this._imageListFore = value;
			}
		}

		public int SelectedImageFore
		{
			get
			{
				return this._selectedImageFore;
			}
			set
			{
				this._selectedImageFore = value;
				this.Refresh();
			}
		}

		public List<Image> ImageListBack
		{
			get
			{
				return this._imageListBack;
			}
			set
			{
				this._imageListBack = value;
				this.SetImage(this._selectedImageBack, true);
				this.Refresh();
			}
		}

		public int SelectedImageBack
		{
			get
			{
				return this._selectedImageBack;
			}
			set
			{
				this._selectedImageBack = value;
				this.SetImage(this._selectedImageBack, false);
				this.Refresh();
			}
		}

		public SEPictureBoxThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SEPictureBoxThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		public SEPictureBox()
		{
			this.InitializeComponent();
			this._imageListFore = new List<Image>(0);
			this._imageListBack = new List<Image>(0);
		}

		private void NextImage(bool isFore)
		{
			List<Image> list = isFore ? this._imageListFore : this._imageListBack;
			int num = isFore ? this._selectedImageFore : this._selectedImageBack;
			if (list != null && list.Count > 0)
			{
				if (num > -1 && num < list.Count - 1)
				{
					if (isFore)
					{
						this._selectedImageFore++;
					}
					else
					{
						this._selectedImageBack++;
					}
				}
				else if (isFore)
				{
					this._selectedImageFore = 0;
				}
				else
				{
					this._selectedImageBack = 0;
				}
				this.UpdateImage(isFore);
			}
		}

		private void SetImage(int index, bool isFore)
		{
			List<Image> list = isFore ? this._imageListFore : this._imageListBack;
			if (list != null && list.Count > 0 && index > -1 && index < list.Count)
			{
				if (isFore)
				{
					this._selectedImageFore = index;
				}
				else
				{
					this._selectedImageBack = index;
				}
				this.UpdateImage(isFore);
			}
		}

		private void UpdateImage(bool isFore)
		{
			List<Image> list = isFore ? this._imageListFore : this._imageListBack;
			int num = isFore ? this._selectedImageFore : this._selectedImageBack;
			if (list != null && list.Count > 0 && num > -1 && num < list.Count)
			{
				this.BackgroundImage = list[isFore ? this._selectedImageFore : this._selectedImageBack];
				this.Refresh();
			}
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}
	}
}
