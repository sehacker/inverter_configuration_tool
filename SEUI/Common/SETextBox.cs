using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SEUI.Common
{
	public class SETextBox : TextBox, ISEUICommon, ISEUI
	{
		private IContainer components = null;

		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = true;

		private SETextBoxThreadSafeAccess _tsAccess = null;

		public SETextBoxThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SETextBoxThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}

		public SETextBox()
		{
			this.InitializeComponent();
		}

		public bool IsDataValid(DataValidationType validationType, int keyValue)
		{
			bool result = true;
			string text = this.Text;
			switch (validationType)
			{
			case DataValidationType.IGNORE:
				result = (keyValue == 46 || keyValue == 8 || keyValue == 65536);
				break;
			case DataValidationType.DECIMAL:
				result = (this.IsDataValid(DataValidationType.IGNORE, keyValue) || (keyValue >= 96 && keyValue <= 105) || (keyValue >= 48 && keyValue <= 57));
				break;
			case DataValidationType.HEX:
				result = (this.IsDataValid(DataValidationType.DECIMAL, keyValue) || (keyValue >= 65 && keyValue <= 70));
				break;
			}
			return result;
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}
	}
}
