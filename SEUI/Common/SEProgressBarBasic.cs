using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SEUI.Common
{
	public class SEProgressBarBasic : ProgressBar, ISEUICommon, ISEUI
	{
		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = false;

		private SEProgressBarBasicThreadSafeAccess _tsAccess = null;

		private IContainer components = null;

		public SEProgressBarBasicThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SEProgressBarBasicThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		public SEProgressBarBasic()
		{
			this.InitializeComponent();
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}
	}
}
