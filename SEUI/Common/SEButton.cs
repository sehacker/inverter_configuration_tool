using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SEUI.Common
{
	public class SEButton : Button, ISEUICommon, ISEUI
	{
		private IContainer components = null;

		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = false;

		private SEButtonThreadSafeAccess _tsAccess = null;

		public SEButtonThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SEButtonThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}

		public SEButton()
		{
			this.InitializeComponent();
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}
	}
}
