using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SEUI.Common
{
	public class SEComboBox : ComboBox, ISEUICommon, ISEUI
	{
		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = true;

		private SEComboBoxThreadSafeAccess _tsAccess = null;

		private IContainer components = null;

		public SEComboBoxThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SEComboBoxThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		public SEComboBox()
		{
			this.InitializeComponent();
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}
	}
}
