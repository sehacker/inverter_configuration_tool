using System;

namespace SEUI.Common
{
	public enum DataValidationType
	{
		NONE = -1,
		IGNORE,
		DECIMAL,
		HEX,
		COUNT
	}
}
