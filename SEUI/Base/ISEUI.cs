using System;

namespace SEUI.Base
{
	public interface ISEUI
	{
		bool IsAddColon();

		bool IsUseDictionary();

		void SetUI(bool isAddColon, bool isUseDictionary);

		void Refresh();
	}
}
