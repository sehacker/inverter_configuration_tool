using SEUI.Common;
using SEUI.Containers;
using System;

namespace SEUI.Base
{
	public interface ISEUIProgressBarNotifiable
	{
		void ShowProgressBar(int startValue, int endValue, SEProgressBar.ProgressFormCallBack callBack);

		void ShowProgressBar(int startValue, int endValue, SEProgressBar.ProgressFormCallBack callBack, SEForm owner);

		void UpdateProgressBar(string text, float progress);

		void ResetProgressBar(int startValue, int endValue);

		string GetMessageByKey(string key);
	}
}
