using System;

namespace SEDevices.Records.Jupiter
{
	public class SEPhaseData
	{
		private float _phase1 = 0f;

		private float _phase2 = 0f;

		private float _phase3 = 0f;

		public float Phase_1
		{
			get
			{
				return this._phase1;
			}
			set
			{
				this._phase1 = value;
			}
		}

		public float Phase_2
		{
			get
			{
				return this._phase2;
			}
			set
			{
				this._phase2 = value;
			}
		}

		public float Phase_3
		{
			get
			{
				return this._phase3;
			}
			set
			{
				this._phase3 = value;
			}
		}

		public bool Equals(SEPhaseData objToCompare)
		{
			return objToCompare == null || (this._phase1 == objToCompare.Phase_1 && this._phase1 == objToCompare.Phase_2 && this._phase1 == objToCompare.Phase_3);
		}
	}
}
