using System;

namespace SEDevices.Records.Extra
{
	public class SEEnergyStatus
	{
		private float _accumulatedEnergyDay;

		private float _accumulatedEnergyMonth;

		private float _accumulatedEnergyYear;

		private float _accumulatedEnergyTotal;

		public float TotalAccumulatedEnergyDay
		{
			get
			{
				return this._accumulatedEnergyDay;
			}
		}

		public float TotalAccumulatedEnergyMonth
		{
			get
			{
				return this._accumulatedEnergyMonth;
			}
		}

		public float TotalAccumulatedEnergyYear
		{
			get
			{
				return this._accumulatedEnergyYear;
			}
		}

		public float TotalAccumulatedEnergyTotal
		{
			get
			{
				return this._accumulatedEnergyTotal;
			}
		}

		public SEEnergyStatus(float day, float month, float year, float total)
		{
			this._accumulatedEnergyDay = day;
			this._accumulatedEnergyMonth = month;
			this._accumulatedEnergyYear = year;
			this._accumulatedEnergyTotal = total;
		}
	}
}
