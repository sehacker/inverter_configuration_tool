using SEDevices.Records.Status;
using System;

namespace SEDevices.Records.Extra
{
	public class SETelemetrySystemError
	{
		public SETelemetryHeader Header = new SETelemetryHeader();

		public uint TimeStamp;

		public EVenusEventCodes Error = EVenusEventCodes.VENUSLOG_NO_ERROR;
	}
}
