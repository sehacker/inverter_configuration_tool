using System;

namespace SEDevices.Records.Extra
{
	public class SESampleTest
	{
		private float _vi;

		private float _vo;

		private float _il;

		private float _iIn;

		public float Vi
		{
			get
			{
				return this._vi;
			}
			set
			{
				this._vi = value;
			}
		}

		public float Vo
		{
			get
			{
				return this._vo;
			}
			set
			{
				this._vo = value;
			}
		}

		public float Il
		{
			get
			{
				return this._il;
			}
			set
			{
				this._il = value;
			}
		}

		public float Iin
		{
			get
			{
				return this._iIn;
			}
			set
			{
				this._iIn = value;
			}
		}

		public SESampleTest()
		{
		}

		public SESampleTest(float vi, float vo, float il, float iIn)
		{
			this._vi = vi;
			this._vo = vo;
			this._il = il;
			this._iIn = iIn;
		}
	}
}
