using System;

namespace SEDevices.Records.Extra
{
	public class SEVenusVersion
	{
		private uint _seVer;

		private uint _iseVer;

		public uint SeVer
		{
			get
			{
				return this._seVer;
			}
			set
			{
				this._seVer = value;
			}
		}

		public uint IseVer
		{
			get
			{
				return this._iseVer;
			}
			set
			{
				this._iseVer = value;
			}
		}
	}
}
