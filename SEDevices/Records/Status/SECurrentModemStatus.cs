using System;

namespace SEDevices.Records.Status
{
	public class SECurrentModemStatus
	{
		private float _progress;

		private uint _remainingTime;

		private CurrentModemStatus _state;

		public float Progress
		{
			get
			{
				return this._progress;
			}
			set
			{
				this._progress = value;
			}
		}

		public uint RemainingTime
		{
			get
			{
				return this._remainingTime;
			}
			set
			{
				this._remainingTime = value;
			}
		}

		public CurrentModemStatus State
		{
			get
			{
				return this._state;
			}
			set
			{
				this._state = value;
			}
		}
	}
}
