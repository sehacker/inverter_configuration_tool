using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryHeader
	{
		private ETelemCodes _code;

		private ushort _dataSize;

		private uint _id;

		public ETelemCodes Code
		{
			get
			{
				return this._code;
			}
			set
			{
				this._code = value;
			}
		}

		public ushort DataSize
		{
			get
			{
				return this._dataSize;
			}
			set
			{
				this._dataSize = value;
			}
		}

		public uint Id
		{
			get
			{
				return this._id;
			}
			set
			{
				this._id = value;
			}
		}
	}
}
