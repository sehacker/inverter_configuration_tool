using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryStatusCombi
	{
		private ushort _activeStrings;

		private float _viInv;

		private float _totalCurrent;

		private float _temperature;

		private float _vSupply;

		private float _vCommon;

		private float _vRef1;

		private float _vRef2;

		public ushort ActiveStrings
		{
			get
			{
				return this._activeStrings;
			}
			set
			{
				this._activeStrings = value;
			}
		}

		public float ViInv
		{
			get
			{
				return this._viInv;
			}
			set
			{
				this._viInv = value;
			}
		}

		public float Total_I
		{
			get
			{
				return this._totalCurrent;
			}
			set
			{
				this._totalCurrent = value;
			}
		}

		public float Temperature
		{
			get
			{
				return this._temperature;
			}
			set
			{
				this._temperature = value;
			}
		}

		public float VSupply
		{
			get
			{
				return this._vSupply;
			}
			set
			{
				this._vSupply = value;
			}
		}

		public float VCommon
		{
			get
			{
				return this._vCommon;
			}
			set
			{
				this._vCommon = value;
			}
		}

		public float VRef1
		{
			get
			{
				return this._vRef1;
			}
			set
			{
				this._vRef1 = value;
			}
		}

		public float VRef2
		{
			get
			{
				return this._vRef2;
			}
			set
			{
				this._vRef2 = value;
			}
		}
	}
}
