using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryStatusCombiString
	{
		private int _stringId;

		private int _geminiStringId;

		private ushort _switchesState;

		private float _rcdCurrent;

		private float _stringCurrent;

		private float _stringVoltage;

		private float _hsVoltage;

		private float _lsVoltage;

		private ushort _trippedTests;

		public int StringID
		{
			get
			{
				return this._stringId;
			}
			set
			{
				this._stringId = value;
			}
		}

		public int GeminiStringId
		{
			get
			{
				return this._geminiStringId;
			}
			set
			{
				this._geminiStringId = value;
			}
		}

		public ushort SwitchesState
		{
			get
			{
				return this._switchesState;
			}
			set
			{
				this._switchesState = value;
			}
		}

		public float RCD_I
		{
			get
			{
				return this._rcdCurrent;
			}
			set
			{
				this._rcdCurrent = value;
			}
		}

		public float String_I
		{
			get
			{
				return this._stringCurrent;
			}
			set
			{
				this._stringCurrent = value;
			}
		}

		public float String_V
		{
			get
			{
				return this._stringVoltage;
			}
			set
			{
				this._stringVoltage = value;
			}
		}

		public float HS_V
		{
			get
			{
				return this._hsVoltage;
			}
			set
			{
				this._hsVoltage = value;
			}
		}

		public float LS_V
		{
			get
			{
				return this._lsVoltage;
			}
			set
			{
				this._lsVoltage = value;
			}
		}

		public ushort TrippedTests
		{
			get
			{
				return this._trippedTests;
			}
			set
			{
				this._trippedTests = value;
			}
		}
	}
}
