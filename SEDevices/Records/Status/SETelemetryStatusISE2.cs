using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryStatusISE2
	{
		private float _vfilterrms;

		private float _aprntPwr;

		private float _reactivePwr;

		private float _powerFactor;

		private float _refVolt1V;

		private float _refVolt2V;

		private float _gndFaultResist;

		private bool _phaseIOut;

		private bool _phaseVFilter;

		private bool _phaseVOut;

		private ushort _reserved;

		public float VfilterRms
		{
			get
			{
				return this._vfilterrms;
			}
			set
			{
				this._vfilterrms = value;
			}
		}

		public float AprntPwr
		{
			get
			{
				return this._aprntPwr;
			}
			set
			{
				this._aprntPwr = value;
			}
		}

		public float ReactivePwr
		{
			get
			{
				return this._reactivePwr;
			}
			set
			{
				this._reactivePwr = value;
			}
		}

		public float PowerFactor
		{
			get
			{
				return this._powerFactor;
			}
			set
			{
				this._powerFactor = value;
			}
		}

		public float RefVolt1V
		{
			get
			{
				return this._refVolt1V;
			}
			set
			{
				this._refVolt1V = value;
			}
		}

		public float RefVolt2V
		{
			get
			{
				return this._refVolt2V;
			}
			set
			{
				this._refVolt2V = value;
			}
		}

		public float GndFaultResist
		{
			get
			{
				return this._gndFaultResist;
			}
			set
			{
				this._gndFaultResist = value;
			}
		}

		public bool PhaseIout
		{
			get
			{
				return this._phaseIOut;
			}
			set
			{
				this._phaseIOut = value;
			}
		}

		public bool PhaseVfilter
		{
			get
			{
				return this._phaseVFilter;
			}
			set
			{
				this._phaseVFilter = value;
			}
		}

		public bool PhaseVout
		{
			get
			{
				return this._phaseVOut;
			}
			set
			{
				this._phaseVOut = value;
			}
		}

		public ushort Reserved
		{
			get
			{
				return this._reserved;
			}
			set
			{
				this._reserved = value;
			}
		}
	}
}
