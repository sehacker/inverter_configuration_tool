using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryStatusISE1
	{
		private float _vin;

		private float _vout;

		private float _vl1n;

		private float _vl2n;

		private float _fout;

		private float _iout;

		private float _bridgeTemp;

		private float _ircd;

		private float _ioutdc;

		private float _pout;

		private EInvProtInverterMode _invMode;

		private ushort _reserved;

		public float Vin
		{
			get
			{
				return this._vin;
			}
			set
			{
				this._vin = value;
			}
		}

		public float Vout
		{
			get
			{
				return this._vout;
			}
			set
			{
				this._vout = value;
			}
		}

		public float VL1N
		{
			get
			{
				return this._vl1n;
			}
			set
			{
				this._vl1n = value;
			}
		}

		public float VL2N
		{
			get
			{
				return this._vl2n;
			}
			set
			{
				this._vl2n = value;
			}
		}

		public float Fout
		{
			get
			{
				return this._fout;
			}
			set
			{
				this._fout = value;
			}
		}

		public float Iout
		{
			get
			{
				return this._iout;
			}
			set
			{
				this._iout = value;
			}
		}

		public float BridgeTemp
		{
			get
			{
				return this._bridgeTemp;
			}
			set
			{
				this._bridgeTemp = value;
			}
		}

		public float Ircd
		{
			get
			{
				return this._ircd;
			}
			set
			{
				this._ircd = value;
			}
		}

		public float IoutDc
		{
			get
			{
				return this._ioutdc;
			}
			set
			{
				this._ioutdc = value;
			}
		}

		public float Pout
		{
			get
			{
				return this._pout;
			}
			set
			{
				this._pout = value;
			}
		}

		public EInvProtInverterMode InvMode
		{
			get
			{
				return this._invMode;
			}
			set
			{
				this._invMode = value;
			}
		}

		public ushort Reserved
		{
			get
			{
				return this._reserved;
			}
			set
			{
				this._reserved = value;
			}
		}
	}
}
