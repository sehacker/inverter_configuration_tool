using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryAccumulatedStatus
	{
		public uint Seconds;

		public float AccWattHour;
	}
}
