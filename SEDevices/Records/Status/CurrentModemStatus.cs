using System;

namespace SEDevices.Records.Status
{
	public enum CurrentModemStatus
	{
		CRNTMDM_INIT,
		CRNTMDM_WAIT_VIN_DECREASE,
		CRNTMDM_IDLE,
		CRNTMDM_SENDING,
		CRNTMDM_ERROR,
		CRNTMDM_NOT_IN_PAIRING_STATE
	}
}
