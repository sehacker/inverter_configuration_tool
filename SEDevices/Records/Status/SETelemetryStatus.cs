using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryStatus
	{
		private float _vIn;

		private float _vOut;

		private float _vAmp;

		private float _vl1n;

		private float _vl2n;

		private float _ioutdc;

		private float _fout;

		private float _ircd;

		private float _vref1;

		private float _vref2;

		public float Vin
		{
			get
			{
				return this._vIn;
			}
			set
			{
				this._vIn = value;
			}
		}

		public float Vout
		{
			get
			{
				return this._vOut;
			}
			set
			{
				this._vOut = value;
			}
		}

		public float Vamp
		{
			get
			{
				return this._vAmp;
			}
			set
			{
				this._vAmp = value;
			}
		}

		public float VL1N
		{
			get
			{
				return this._vl1n;
			}
			set
			{
				this._vl1n = value;
			}
		}

		public float VL2N
		{
			get
			{
				return this._vl2n;
			}
			set
			{
				this._vl2n = value;
			}
		}

		public float IoutDc
		{
			get
			{
				return this._ioutdc;
			}
			set
			{
				this._ioutdc = value;
			}
		}

		public float Fout
		{
			get
			{
				return this._fout;
			}
			set
			{
				this._fout = value;
			}
		}

		public float Ircd
		{
			get
			{
				return this._ircd;
			}
			set
			{
				this._ircd = value;
			}
		}

		public float Vref1
		{
			get
			{
				return this._vref1;
			}
			set
			{
				this._vref1 = value;
			}
		}

		public float Vref2
		{
			get
			{
				return this._vref2;
			}
			set
			{
				this._vref2 = value;
			}
		}
	}
}
