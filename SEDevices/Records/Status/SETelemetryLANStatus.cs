using SEUtils.Common;
using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryLANStatus
	{
		private uint _statusIP = 0u;

		private uint _statusSubnetMask = 0u;

		private uint _statusDefaultGateway = 0u;

		private uint _statusDNSIP = 0u;

		public uint IP
		{
			get
			{
				return this._statusIP;
			}
			set
			{
				this._statusIP = value;
			}
		}

		public string IPStr
		{
			get
			{
				return Utils.ConvertToIPString(this._statusIP);
			}
		}

		public uint SubnetMask
		{
			get
			{
				return this._statusSubnetMask;
			}
			set
			{
				this._statusSubnetMask = value;
			}
		}

		public string SubnetMaskStr
		{
			get
			{
				return Utils.ConvertToIPString(this._statusSubnetMask);
			}
		}

		public uint DefaultGateway
		{
			get
			{
				return this._statusDefaultGateway;
			}
			set
			{
				this._statusDefaultGateway = value;
			}
		}

		public string DefaultGatewayStr
		{
			get
			{
				return Utils.ConvertToIPString(this._statusDefaultGateway);
			}
		}

		public uint DNSIP
		{
			get
			{
				return this._statusDNSIP;
			}
			set
			{
				this._statusDNSIP = value;
			}
		}

		public string DNSIPStr
		{
			get
			{
				return Utils.ConvertToIPString(this._statusDNSIP);
			}
		}
	}
}
