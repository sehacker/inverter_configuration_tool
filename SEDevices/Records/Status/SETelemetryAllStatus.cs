using System;

namespace SEDevices.Records.Status
{
	public class SETelemetryAllStatus
	{
		public SETelemetryHeader Header = new SETelemetryHeader();

		public SETelemetryStatus SeStat = new SETelemetryStatus();

		public SETelemetryAccumulatedStatus SeAccStatus = new SETelemetryAccumulatedStatus();

		public SETelemetryStatusISE1 IseStat1 = new SETelemetryStatusISE1();

		public SETelemetryStatusISE2 IseStat2 = new SETelemetryStatusISE2();
	}
}
