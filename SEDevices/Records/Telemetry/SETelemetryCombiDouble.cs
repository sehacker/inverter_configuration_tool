using System;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetryCombiDouble
	{
		private ushort _version;

		private SETelemetryCombiString _combiStringTelem;

		private SETelemetryCombi _combiTelem;

		public ushort Version
		{
			get
			{
				return this._version;
			}
			set
			{
				this._version = value;
			}
		}

		public SETelemetryCombiString Telemetry_CombiString
		{
			get
			{
				return this._combiStringTelem;
			}
			set
			{
				this._combiStringTelem = value;
			}
		}

		public SETelemetryCombi Telemetry_Combi
		{
			get
			{
				return this._combiTelem;
			}
			set
			{
				this._combiTelem = value;
			}
		}
	}
}
