using System;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetryInverter1Phase : SETelemetry
	{
		private uint _secs;

		private uint _secsDelta;

		private float _temperature;

		private float _accPowerAC;

		private float _accPowerDeltaAC;

		private float _voltageAC;

		private float _currentAC;

		private float _freqAC;

		private float _accPowerDC;

		private float _accPowerDeltaDC;

		private float _voltageDC;

		private float _currentDC;

		public uint Seconds
		{
			get
			{
				return this._secs;
			}
			set
			{
				this._secs = value;
			}
		}

		public uint SecondsDelta
		{
			get
			{
				return this._secsDelta;
			}
			set
			{
				this._secsDelta = value;
			}
		}

		public float Temperature
		{
			get
			{
				return this._temperature;
			}
			set
			{
				this._temperature = value;
			}
		}

		public float AccumulatedPowerAC
		{
			get
			{
				return this._accPowerAC;
			}
			set
			{
				this._accPowerAC = value;
			}
		}

		public float AccumulatedDeltaPowerAC
		{
			get
			{
				return this._accPowerDeltaAC;
			}
			set
			{
				this._accPowerDeltaAC = value;
			}
		}

		public float VoltageAC
		{
			get
			{
				return this._voltageAC;
			}
			set
			{
				this._voltageAC = value;
			}
		}

		public float CurrentAC
		{
			get
			{
				return this._currentAC;
			}
			set
			{
				this._currentAC = value;
			}
		}

		public float FreqAC
		{
			get
			{
				return this._freqAC;
			}
			set
			{
				this._freqAC = value;
			}
		}

		public float AccumulatedPowerDC
		{
			get
			{
				return this._accPowerDC;
			}
			set
			{
				this._accPowerDC = value;
			}
		}

		public float AccumulatedDeltaPowerDC
		{
			get
			{
				return this._accPowerDeltaDC;
			}
			set
			{
				this._accPowerDeltaDC = value;
			}
		}

		public float VoltageDC
		{
			get
			{
				return this._voltageDC;
			}
			set
			{
				this._voltageDC = value;
			}
		}

		public float CurrentDC
		{
			get
			{
				return this._currentDC;
			}
			set
			{
				this._currentDC = value;
			}
		}

		public override long ParseRecord(ref byte[] data, long position)
		{
			base.ParseRecord(ref data, position);
			this._secs = base.Reader.ReadUInt32();
			this._secsDelta = base.Reader.ReadUInt32();
			this._temperature = base.Reader.ReadSingle();
			this._accPowerAC = base.Reader.ReadSingle();
			this._accPowerDeltaAC = base.Reader.ReadSingle();
			this._voltageAC = base.Reader.ReadSingle();
			this._currentAC = base.Reader.ReadSingle();
			this._freqAC = base.Reader.ReadSingle();
			this._accPowerDC = base.Reader.ReadSingle();
			this._accPowerDeltaDC = base.Reader.ReadSingle();
			this._voltageDC = base.Reader.ReadSingle();
			this._currentDC = base.Reader.ReadSingle();
			return base.MemStream.Position;
		}
	}
}
