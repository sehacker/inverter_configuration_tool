using System;
using System.IO;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetry
	{
		public const int VI_Q = 4;

		public const int VO_Q = 4;

		public const int POWER_Q = 3;

		public const int TEMP_Q = 2;

		public const int IIN_Q = 8;

		public const int TYPE_PANEL = 0;

		public const int TYPE_STRING = 1;

		public const int TYPE_CLUSTER = 2;

		private MemoryStream _memStream;

		private BinaryReader _reader;

		private RecordType _type;

		private uint _id;

		private uint _timeStamp;

		private ushort _length;

		protected MemoryStream MemStream
		{
			get
			{
				return this._memStream;
			}
		}

		protected BinaryReader Reader
		{
			get
			{
				return this._reader;
			}
		}

		public RecordType Type
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		public uint Id
		{
			get
			{
				return this._id;
			}
			set
			{
				this._id = value;
			}
		}

		public uint TimeStamp
		{
			get
			{
				return this._timeStamp;
			}
			set
			{
				this._timeStamp = value;
			}
		}

		public ushort RecordLength
		{
			get
			{
				return this._length;
			}
			set
			{
				this._length = value;
			}
		}

		private void ClearBuffer()
		{
			if (this._reader != null)
			{
				this._reader = null;
			}
			if (this._memStream != null)
			{
				this._memStream.Dispose();
				this._memStream = null;
			}
		}

		private void SetupBuffer(byte[] data)
		{
			this._memStream = new MemoryStream(data);
			this._reader = new BinaryReader(this._memStream);
		}

		public static SETelemetry GetNewRecord(ref byte[] data, long position)
		{
			SETelemetry sETelemetry = null;
			MemoryStream memoryStream = new MemoryStream(data);
			BinaryReader binaryReader = new BinaryReader(memoryStream);
			memoryStream.Position = position;
			RecordType recordType = (RecordType)binaryReader.ReadUInt16();
			memoryStream.Dispose();
			RecordType recordType2 = recordType;
			if (recordType2 <= RecordType.VENUS)
			{
				switch (recordType2)
				{
				case RecordType.PANEL:
					sETelemetry = new SETelemetryPanel();
					break;
				case RecordType.STRING:
					sETelemetry = new SETelemetryString();
					break;
				case RecordType.CLUSTER:
					sETelemetry = new SETelemetryCluster();
					break;
				default:
					switch (recordType2)
					{
					case RecordType.INVERTER_1PHASE:
						sETelemetry = new SETelemetryInverter1Phase();
						break;
					case RecordType.INVERTER_3PHASE:
						sETelemetry = new SETelemetryInverter3Phase();
						break;
					case RecordType.COMBI_STRING:
						sETelemetry = new SETelemetryCombiString();
						break;
					case RecordType.COMBI:
						sETelemetry = new SETelemetryCombi();
						break;
					case RecordType.UNIFIED_COMBI_STRING:
						sETelemetry = new SETelemetryUnifiedCombiString();
						break;
					case RecordType.UNIFIED_COMBI:
						sETelemetry = new SETelemetryUnifiedCombi();
						break;
					default:
						if (recordType2 != RecordType.VENUS)
						{
						}
						break;
					}
					break;
				}
			}
			else if (recordType2 <= RecordType.SUNTRACER)
			{
				if (recordType2 != RecordType.POLESTAR && recordType2 != RecordType.SUNTRACER)
				{
				}
			}
			else if (recordType2 != RecordType.JUPITER && recordType2 != RecordType.VEGA)
			{
			}
			sETelemetry.Type = recordType;
			return sETelemetry;
		}

		public virtual long ParseRecord(ref byte[] data, long position)
		{
			return this.ParseRecordHeader(ref data, position);
		}

		private long ParseRecordHeader(ref byte[] data, long position)
		{
			this.ClearBuffer();
			this.SetupBuffer(data);
			this._memStream.Position = position + 2L;
			this._id = this._reader.ReadUInt32();
			this._length = this._reader.ReadUInt16();
			this._timeStamp = this._reader.ReadUInt32();
			return this._memStream.Position;
		}
	}
}
