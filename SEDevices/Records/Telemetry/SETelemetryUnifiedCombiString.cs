using System;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetryUnifiedCombiString : SETelemetry
	{
		private uint _secs;

		private uint _secsDelta;

		private uint _initiatorID;

		private ushort _trippedCode;

		private ushort _status;

		private ushort _errorCode;

		private ushort _stringIndex;

		private ushort _geminiStringIndex;

		private ushort _switchesStatus;

		private float _errorMeasurement;

		private float _rcdI;

		private float _stringI;

		private float _stringV;

		private float _hsV;

		private float _lsV;

		private float _energyDelta;

		private float _energy;

		public uint Seconds
		{
			get
			{
				return this._secs;
			}
			set
			{
				this._secs = value;
			}
		}

		public uint SecondsDelta
		{
			get
			{
				return this._secsDelta;
			}
			set
			{
				this._secsDelta = value;
			}
		}

		public uint InitiatorID
		{
			get
			{
				return this._initiatorID;
			}
			set
			{
				this._initiatorID = value;
			}
		}

		public ushort TrippedCode
		{
			get
			{
				return this._trippedCode;
			}
			set
			{
				this._trippedCode = value;
			}
		}

		public ushort Status
		{
			get
			{
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public ushort ErrorCode
		{
			get
			{
				return this._errorCode;
			}
			set
			{
				this._errorCode = value;
			}
		}

		public ushort StringIndex
		{
			get
			{
				return this._stringIndex;
			}
			set
			{
				this._stringIndex = value;
			}
		}

		public ushort StringIndexGemini
		{
			get
			{
				return this._geminiStringIndex;
			}
			set
			{
				this._geminiStringIndex = value;
			}
		}

		public ushort SwitchesStatus
		{
			get
			{
				return this._switchesStatus;
			}
			set
			{
				this._switchesStatus = value;
			}
		}

		public float ErrorMeasurement
		{
			get
			{
				return this._errorMeasurement;
			}
			set
			{
				this._errorMeasurement = value;
			}
		}

		public float RCD_I
		{
			get
			{
				return this._rcdI;
			}
			set
			{
				this._rcdI = value;
			}
		}

		public float String_I
		{
			get
			{
				return this._stringI;
			}
			set
			{
				this._stringI = value;
			}
		}

		public float String_V
		{
			get
			{
				return this._stringV;
			}
			set
			{
				this._stringV = value;
			}
		}

		public float HS_V
		{
			get
			{
				return this._hsV;
			}
			set
			{
				this._hsV = value;
			}
		}

		public float LS_V
		{
			get
			{
				return this._lsV;
			}
			set
			{
				this._lsV = value;
			}
		}

		public float EnergyDelta
		{
			get
			{
				return this._energyDelta;
			}
			set
			{
				this._energyDelta = value;
			}
		}

		public float Energy
		{
			get
			{
				return this._energy;
			}
			set
			{
				this._energy = value;
			}
		}

		public override long ParseRecord(ref byte[] data, long position)
		{
			base.ParseRecord(ref data, position);
			this.Seconds = base.Reader.ReadUInt32();
			this.SecondsDelta = base.Reader.ReadUInt32();
			this.InitiatorID = base.Reader.ReadUInt32();
			this.TrippedCode = base.Reader.ReadUInt16();
			this.Status = base.Reader.ReadUInt16();
			this.ErrorCode = base.Reader.ReadUInt16();
			this.StringIndex = base.Reader.ReadUInt16();
			this.StringIndexGemini = base.Reader.ReadUInt16();
			this.SwitchesStatus = base.Reader.ReadUInt16();
			this.ErrorMeasurement = base.Reader.ReadSingle();
			this.RCD_I = base.Reader.ReadSingle();
			this.String_I = base.Reader.ReadSingle();
			this.String_V = base.Reader.ReadSingle();
			this.HS_V = base.Reader.ReadSingle();
			this.LS_V = base.Reader.ReadSingle();
			this.EnergyDelta = base.Reader.ReadSingle();
			this.Energy = base.Reader.ReadSingle();
			return base.MemStream.Position;
		}
	}
}
