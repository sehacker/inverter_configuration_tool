using System;

namespace SEDevices.Records.Telemetry
{
	public enum RecordType
	{
		PANEL,
		STRING,
		CLUSTER,
		INVERTER_1PHASE = 16,
		INVERTER_3PHASE,
		COMBI_STRING,
		COMBI,
		UNIFIED_COMBI_STRING,
		UNIFIED_COMBI,
		VENUS = 512,
		POLESTAR = 768,
		SUNTRACER = 1024,
		JUPITER = 2048,
		VEGA = 2560
	}
}
