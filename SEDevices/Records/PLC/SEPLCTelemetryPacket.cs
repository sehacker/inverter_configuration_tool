using SEProtocol.Utils;
using System;

namespace SEDevices.Records.PLC
{
	public class SEPLCTelemetryPacket
	{
		private const int TELEM_CONF_BITS = 3;

		private const int TELEM_CONF_MASK = 7;

		private const int TELEM_CONF0 = 0;

		private const int TELEM_CONF1 = 1;

		private const float TELEM_MAX_POWER = 16384f;

		private const float TELEM_MAX_VI = 128f;

		private const float TELEM_MAX_VO = 128f;

		private const float TELEM_MAX_IIN = 25.6f;

		public void TelemMercuryUnpack(ref SEPLCTelemetryGeneral PlcGenTelem, ref SETelemetryMercuryUnPacked TelemUnpacked)
		{
			SETelemetryMercuryPacked sETelemetryMercuryPacked = new SETelemetryMercuryPacked();
			this.TelemGeneralToMercuryPacked(ref PlcGenTelem, ref sETelemetryMercuryPacked);
			switch (sETelemetryMercuryPacked.Id3_Conf & 7)
			{
			case 0:
			{
				ushort num = (ushort)((int)sETelemetryMercuryPacked.Power1 << 8 | (int)sETelemetryMercuryPacked.Power0);
				TelemUnpacked.Id = (uint)(sETelemetryMercuryPacked.Id3_Conf >> 3 << 24 | (int)sETelemetryMercuryPacked.Id2 << 16 | (int)sETelemetryMercuryPacked.Id1 << 8 | (int)sETelemetryMercuryPacked.Id0);
				TelemUnpacked.Power = (float)num * 0.25f;
				TelemUnpacked.Iacc = 0f;
				TelemUnpacked.Vin = (float)sETelemetryMercuryPacked.Vi * 0.5f;
				TelemUnpacked.Vout = (float)sETelemetryMercuryPacked.Vo * 0.5f;
				TelemUnpacked.Iin = (float)sETelemetryMercuryPacked.Iin * 0.1f;
				TelemUnpacked.AvgTemp = 0f;
				TelemUnpacked.Seconds = (ushort)((int)sETelemetryMercuryPacked.Sec1 << 8 | (int)sETelemetryMercuryPacked.Sec0);
				TelemUnpacked.Checksum = (ushort)((int)sETelemetryMercuryPacked.Crc1 << 8 | (int)sETelemetryMercuryPacked.Crc0);
				TelemUnpacked.Type = EPlcTelemType.TELEM_PANEL;
				break;
			}
			case 1:
			{
				ushort num = (ushort)((int)sETelemetryMercuryPacked.Power1 << 8 | (int)sETelemetryMercuryPacked.Power0);
				TelemUnpacked.Id = (uint)(sETelemetryMercuryPacked.Id3_Conf >> 3 << 24 | (int)sETelemetryMercuryPacked.Id2 << 16 | (int)sETelemetryMercuryPacked.Id1 << 8 | (int)sETelemetryMercuryPacked.Id0);
				TelemUnpacked.Iacc = (float)num * 0.25f;
				TelemUnpacked.Power = 0f;
				TelemUnpacked.Vin = (float)sETelemetryMercuryPacked.Vi * 0.5f;
				TelemUnpacked.Vout = (float)sETelemetryMercuryPacked.Vo * 0.5f;
				TelemUnpacked.Iin = (float)sETelemetryMercuryPacked.Iin * 0.1f;
				TelemUnpacked.AvgTemp = 0f;
				TelemUnpacked.Seconds = (ushort)((int)sETelemetryMercuryPacked.Sec1 << 8 | (int)sETelemetryMercuryPacked.Sec0);
				TelemUnpacked.Checksum = (ushort)((int)sETelemetryMercuryPacked.Crc1 << 8 | (int)sETelemetryMercuryPacked.Crc0);
				TelemUnpacked.Type = EPlcTelemType.TELEM_STRING;
				break;
			}
			}
			TelemUnpacked.ChecksumOK = (Crc16.Calc(Crc16.INIT_VAL, PlcGenTelem.Data, 11) == TelemUnpacked.Checksum);
		}

		public void TelemGeneralToMercuryPacked(ref SEPLCTelemetryGeneral PlcGenTelem, ref SETelemetryMercuryPacked TelemPacked)
		{
			TelemPacked.Id3_Conf = PlcGenTelem.Data[0];
			TelemPacked.Id2 = PlcGenTelem.Data[1];
			TelemPacked.Id1 = PlcGenTelem.Data[2];
			TelemPacked.Id0 = PlcGenTelem.Data[3];
			TelemPacked.Power1 = PlcGenTelem.Data[4];
			TelemPacked.Power0 = PlcGenTelem.Data[5];
			TelemPacked.Vi = PlcGenTelem.Data[6];
			TelemPacked.Vo = PlcGenTelem.Data[7];
			TelemPacked.Iin = PlcGenTelem.Data[8];
			TelemPacked.Sec1 = PlcGenTelem.Data[9];
			TelemPacked.Sec0 = PlcGenTelem.Data[10];
			TelemPacked.Crc1 = PlcGenTelem.Data[11];
			TelemPacked.Crc0 = PlcGenTelem.Data[12];
		}
	}
}
