using System;

namespace SEDevices.Records.PLC
{
	public enum EPlcTelemType
	{
		TELEM_PANEL,
		TELEM_STRING,
		TELEM_CLUSTER,
		COUNT
	}
}
