using System;

namespace SEDevices.Records.PLC
{
	public class SEPLCRxTest
	{
		public int G0ResMax;

		public int G1ResMax;

		public int EnergyOldMax;

		public int G0ResMin;

		public int G1ResMin;

		public int EnergyOldMin;

		public int G0ResAvg;

		public int G1ResAvg;

		public int EnergyOldAvg;

		public short Shift;
	}
}
