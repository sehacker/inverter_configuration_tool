using SEDevices.Records.Status;
using System;

namespace SEDevices.Records.PLC
{
	public class SEPLCTelemetryGeneral
	{
		public SETelemetryHeader Header = new SETelemetryHeader();

		public byte[] Data;
	}
}
