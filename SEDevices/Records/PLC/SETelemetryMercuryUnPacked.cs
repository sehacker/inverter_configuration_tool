using System;

namespace SEDevices.Records.PLC
{
	public class SETelemetryMercuryUnPacked
	{
		public const int VI_Q = 4;

		public const int VO_Q = 4;

		public const int POWER_Q = 3;

		public const int TEMP_Q = 2;

		public const int IIN_Q = 8;

		public uint Id;

		public float Iacc;

		public float Power;

		public float Iin;

		public float Vin;

		public float Vout;

		public float AvgTemp;

		public ushort Seconds;

		public ushort Checksum;

		public EPlcTelemType Type = EPlcTelemType.TELEM_PANEL;

		public bool ChecksumOK;
	}
}
