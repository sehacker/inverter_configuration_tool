using System;

namespace SEDevices.Records.PLC
{
	public class SETelemetryMercuryPacked
	{
		public byte Id3_Conf;

		public byte Id2;

		public byte Id1;

		public byte Id0;

		public byte Power1;

		public byte Power0;

		public byte Vi;

		public byte Vo;

		public byte Iin;

		public byte Sec1;

		public byte Sec0;

		public byte Crc1;

		public byte Crc0;
	}
}
