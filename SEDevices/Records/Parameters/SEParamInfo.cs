using System;

namespace SEDevices.Records.Parameter
{
	public class SEParamInfo : SEParam
	{
		private object _min;

		private object _max;

		private object _defValue;

		public object Min
		{
			get
			{
				return this._min;
			}
		}

		public object Max
		{
			get
			{
				return this._max;
			}
		}

		public object Default
		{
			get
			{
				return this._defValue;
			}
		}

		public SEParamInfo(ushort index, string paramName, short paramType, object defaultVal, object max, object min) : base(index, paramName)
		{
			this._min = min;
			this._max = max;
			this._defValue = defaultVal;
		}
	}
}
