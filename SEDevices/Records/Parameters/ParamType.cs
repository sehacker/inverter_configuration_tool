using System;

namespace SEDevices.Records.Parameter
{
	public enum ParamType
	{
		UINT32,
		FLOAT,
		INT,
		STRING,
		COUNT
	}
}
