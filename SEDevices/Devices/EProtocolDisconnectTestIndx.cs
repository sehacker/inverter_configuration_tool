using System;

namespace SEDevices.Devices
{
	public enum EProtocolDisconnectTestIndx
	{
		PRVLG_PARAM_VOUT_MAX,
		PRVLG_PARAM_VOUT_MIN,
		PRVLG_PARAM_FOUT_MAX,
		PRVLG_PARAM_FOUT_MIN
	}
}
