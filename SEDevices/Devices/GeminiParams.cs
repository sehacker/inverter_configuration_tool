using System;

namespace SEDevices.Devices
{
	public enum GeminiParams
	{
		ID,
		VER_MAJOR,
		VER_MINOR,
		CHECKSUM,
		HW_ID,
		ASSEMBLED_STRINGS,
		ACTIVE_STRINGS,
		WD_ENABLE,
		INNER_PROT_ENABLE,
		SYSTEM_PRINT_ENABLE,
		TELEM_AVG_TIME,
		TELEM_SEND_NON_UNIFIED,
		WAKEUP_UART0_DELAY_SEC,
		ADC_GAIN,
		SPIADC_HSV_GAIN,
		SPIADC_LSV_GAIN,
		SPIADC_CUR_GAIN,
		ADC_OFFSET,
		SPIADC_HSV_OFFSET,
		SPIADC_LSV_OFFSET,
		SPIADC_CUR_OFFSET,
		TEMP_GAIN,
		VINV_GAIN,
		VSUPPLY_GAIN,
		VCOMMON_GAIN,
		VREF1_GAIN,
		VREF2_GAIN,
		TEMP_OFFSET,
		VINV_OFFSET,
		VSUPPLY_OFFSET,
		VCOMMON_OFFSET,
		VREF1_OFFSET,
		VREF2_OFFSET,
		COMBI_TRIPPED,
		COMBI_LOG_EVENT_0,
		COMBI_LOG_EVENT_1,
		COMBI_LOG_TIME_0,
		COMBI_LOG_TIME_1,
		RESERVED0,
		RESERVED1,
		RESERVED2,
		RELAY_OVER_VOLTAGE_MAX_CNT,
		RCD_CAL_LAST_TIME,
		RESERVED5,
		RCD_TEST_CURRENT_TH,
		RELAY_TEST_MIN_CUR,
		RELAY_TEST_MAX_CUR,
		RELAY_TEST_MIN_VINV,
		RELAY_TEST_V0_MAX,
		RELAY_TEST_V1_MAX,
		RELAY_TEST_T0,
		RELAY_TEST_T1,
		RELAY_TEST_T2,
		RELAY_TEST_T3,
		RELAY_TEST_AVG_TIME,
		RELAY_MON_CURRENT_TH,
		RCD_MON_ALGORITHM,
		RCD_MON_TIME_TH,
		RCD_MON_ALPHA_FACTOR,
		HSV_GAIN,
		LSV_GAIN,
		HSV_OFFSET,
		LSV_OFFSET,
		RCD_CAL_REF_CURRENT,
		RCD_CAL_PHASE_DURATION,
		STRING0_GEMINI_STRING,
		STRING0_SWITCHES_STATE,
		STRING0_ACTIVE_TESTS,
		STRING0_TRIPPED,
		STRING0_LOG_EVENT_0,
		STRING0_LOG_EVENT_1,
		STRING0_LOG_TIME_0,
		STRING0_LOG_TIME_1,
		STRING0_LOG_MEAS_0,
		STRING0_LOG_MEAS_1,
		STRING0_HSRELAY_TEST_NEXT_TIME,
		STRING0_LSRELAY_TEST_NEXT_TIME,
		STRING0_RCD_MON_CURRENT_TH,
		STRING0_RCD_GAIN,
		STRING0_RCD_ENV_OFFSET_OFF,
		STRING0_RCD_ENV_OFFSET_ON,
		STRING0_CURR_GAIN,
		STRING0_CURR_OFFSET,
		STRING0_RESERVED0,
		STRING0_RESERVED1,
		STRING1_GEMINI_STRING,
		STRING1_SWITCHES_STATE,
		STRING1_ACTIVE_TESTS,
		STRING1_TRIPPED,
		STRING1_LOG_EVENT_0,
		STRING1_LOG_EVENT_1,
		STRING1_LOG_TIME_0,
		STRING1_LOG_TIME_1,
		STRING1_LOG_MEAS_0,
		STRING1_LOG_MEAS_1,
		STRING1_HSRELAY_TEST_NEXT_TIME,
		STRING1_LSRELAY_TEST_NEXT_TIME,
		STRING1_RCD_MON_CURRENT_TH,
		STRING1_RCD_GAIN,
		STRING1_RCD_ENV_OFFSET_OFF,
		STRING1_RCD_ENV_OFFSET_ON,
		STRING1_CURR_GAIN,
		STRING1_CURR_OFFSET,
		STRING1_RESERVED0,
		STRING1_RESERVED1,
		STRING2_GEMINI_STRING,
		STRING2_SWITCHES_STATE,
		STRING2_ACTIVE_TESTS,
		STRING2_TRIPPED,
		STRING2_LOG_EVENT_0,
		STRING2_LOG_EVENT_1,
		STRING2_LOG_TIME_0,
		STRING2_LOG_TIME_1,
		STRING2_LOG_MEAS_0,
		STRING2_LOG_MEAS_1,
		STRING2_HSRELAY_TEST_NEXT_TIME,
		STRING2_LSRELAY_TEST_NEXT_TIME,
		STRING2_RCD_MON_CURRENT_TH,
		STRING2_RCD_GAIN,
		STRING2_RCD_ENV_OFFSET_OFF,
		STRING2_RCD_ENV_OFFSET_ON,
		STRING2_CURR_GAIN,
		STRING2_CURR_OFFSET,
		STRING2_RESERVED0,
		STRING2_RESERVED1,
		STRING3_GEMINI_STRING,
		STRING3_SWITCHES_STATE,
		STRING3_ACTIVE_TESTS,
		STRING3_TRIPPED,
		STRING3_LOG_EVENT_0,
		STRING3_LOG_EVENT_1,
		STRING3_LOG_TIME_0,
		STRING3_LOG_TIME_1,
		STRING3_LOG_MEAS_0,
		STRING3_LOG_MEAS_1,
		STRING3_HSRELAY_TEST_NEXT_TIME,
		STRING3_LSRELAY_TEST_NEXT_TIME,
		STRING3_RCD_MON_CURRENT_TH,
		STRING3_RCD_GAIN,
		STRING3_RCD_ENV_OFFSET_OFF,
		STRING3_RCD_ENV_OFFSET_ON,
		STRING3_CURR_GAIN,
		STRING3_CURR_OFFSET,
		STRING3_RESERVED0,
		STRING3_RESERVED1,
		STRING4_GEMINI_STRING,
		STRING4_SWITCHES_STATE,
		STRING4_ACTIVE_TESTS,
		STRING4_TRIPPED,
		STRING4_LOG_EVENT_0,
		STRING4_LOG_EVENT_1,
		STRING4_LOG_TIME_0,
		STRING4_LOG_TIME_1,
		STRING4_LOG_MEAS_0,
		STRING4_LOG_MEAS_1,
		STRING4_HSRELAY_TEST_NEXT_TIME,
		STRING4_LSRELAY_TEST_NEXT_TIME,
		STRING4_RCD_MON_CURRENT_TH,
		STRING4_RCD_GAIN,
		STRING4_RCD_ENV_OFFSET_OFF,
		STRING4_RCD_ENV_OFFSET_ON,
		STRING4_CURR_GAIN,
		STRING4_CURR_OFFSET,
		STRING4_RESERVED0,
		STRING4_RESERVED1,
		STRING5_GEMINI_STRING,
		STRING5_SWITCHES_STATE,
		STRING5_ACTIVE_TESTS,
		STRING5_TRIPPED,
		STRING5_LOG_EVENT_0,
		STRING5_LOG_EVENT_1,
		STRING5_LOG_TIME_0,
		STRING5_LOG_TIME_1,
		STRING5_LOG_MEAS_0,
		STRING5_LOG_MEAS_1,
		STRING5_HSRELAY_TEST_NEXT_TIME,
		STRING5_LSRELAY_TEST_NEXT_TIME,
		STRING5_RCD_MON_CURRENT_TH,
		STRING5_RCD_GAIN,
		STRING5_RCD_ENV_OFFSET_OFF,
		STRING5_RCD_ENV_OFFSET_ON,
		STRING5_CURR_GAIN,
		STRING5_CURR_OFFSET,
		STRING5_RESERVED0,
		STRING5_RESERVED1,
		STRING6_GEMINI_STRING,
		STRING6_SWITCHES_STATE,
		STRING6_ACTIVE_TESTS,
		STRING6_TRIPPED,
		STRING6_LOG_EVENT_0,
		STRING6_LOG_EVENT_1,
		STRING6_LOG_TIME_0,
		STRING6_LOG_TIME_1,
		STRING6_LOG_MEAS_0,
		STRING6_LOG_MEAS_1,
		STRING6_HSRELAY_TEST_NEXT_TIME,
		STRING6_LSRELAY_TEST_NEXT_TIME,
		STRING6_RCD_MON_CURRENT_TH,
		STRING6_RCD_GAIN,
		STRING6_RCD_ENV_OFFSET_OFF,
		STRING6_RCD_ENV_OFFSET_ON,
		STRING6_CURR_GAIN,
		STRING6_CURR_OFFSET,
		STRING6_RESERVED0,
		STRING6_RESERVED1,
		STRING7_GEMINI_STRING,
		STRING7_SWITCHES_STATE,
		STRING7_ACTIVE_TESTS,
		STRING7_TRIPPED,
		STRING7_LOG_EVENT_0,
		STRING7_LOG_EVENT_1,
		STRING7_LOG_TIME_0,
		STRING7_LOG_TIME_1,
		STRING7_LOG_MEAS_0,
		STRING7_LOG_MEAS_1,
		STRING7_HSRELAY_TEST_NEXT_TIME,
		STRING7_LSRELAY_TEST_NEXT_TIME,
		STRING7_RCD_MON_CURRENT_TH,
		STRING7_RCD_GAIN,
		STRING7_RCD_ENV_OFFSET_OFF,
		STRING7_RCD_ENV_OFFSET_ON,
		STRING7_CURR_GAIN,
		STRING7_CURR_OFFSET,
		STRING7_RESERVED0,
		STRING7_RESERVED1,
		STRING8_GEMINI_STRING,
		STRING8_SWITCHES_STATE,
		STRING8_ACTIVE_TESTS,
		STRING8_TRIPPED,
		STRING8_LOG_EVENT_0,
		STRING8_LOG_EVENT_1,
		STRING8_LOG_TIME_0,
		STRING8_LOG_TIME_1,
		STRING8_LOG_MEAS_0,
		STRING8_LOG_MEAS_1,
		STRING8_HSRELAY_TEST_NEXT_TIME,
		STRING8_LSRELAY_TEST_NEXT_TIME,
		STRING8_RCD_MON_CURRENT_TH,
		STRING8_RCD_GAIN,
		STRING8_RCD_ENV_OFFSET_OFF,
		STRING8_RCD_ENV_OFFSET_ON,
		STRING8_CURR_GAIN,
		STRING8_CURR_OFFSET,
		STRING8_RESERVED0,
		STRING8_RESERVED1,
		STRING9_GEMINI_STRING,
		STRING9_SWITCHES_STATE,
		STRING9_ACTIVE_TESTS,
		STRING9_TRIPPED,
		STRING9_LOG_EVENT_0,
		STRING9_LOG_EVENT_1,
		STRING9_LOG_TIME_0,
		STRING9_LOG_TIME_1,
		STRING9_LOG_MEAS_0,
		STRING9_LOG_MEAS_1,
		STRING9_HSRELAY_TEST_NEXT_TIME,
		STRING9_LSRELAY_TEST_NEXT_TIME,
		STRING9_RCD_MON_CURRENT_TH,
		STRING9_RCD_GAIN,
		STRING9_RCD_ENV_OFFSET_OFF,
		STRING9_RCD_ENV_OFFSET_ON,
		STRING9_CURR_GAIN,
		STRING9_CURR_OFFSET,
		STRING9_RESERVED0,
		STRING9_RESERVED1,
		STRING10_GEMINI_STRING,
		STRING10_SWITCHES_STATE,
		STRING10_ACTIVE_TESTS,
		STRING10_TRIPPED,
		STRING10_LOG_EVENT_0,
		STRING10_LOG_EVENT_1,
		STRING10_LOG_TIME_0,
		STRING10_LOG_TIME_1,
		STRING10_LOG_MEAS_0,
		STRING10_LOG_MEAS_1,
		STRING10_HSRELAY_TEST_NEXT_TIME,
		STRING10_LSRELAY_TEST_NEXT_TIME,
		STRING10_RCD_MON_CURRENT_TH,
		STRING10_RCD_GAIN,
		STRING10_RCD_ENV_OFFSET_OFF,
		STRING10_RCD_ENV_OFFSET_ON,
		STRING10_CURR_GAIN,
		STRING10_CURR_OFFSET,
		STRING10_RESERVED0,
		STRING10_RESERVED1,
		STRING11_GEMINI_STRING,
		STRING11_SWITCHES_STATE,
		STRING11_ACTIVE_TESTS,
		STRING11_TRIPPED,
		STRING11_LOG_EVENT_0,
		STRING11_LOG_EVENT_1,
		STRING11_LOG_TIME_0,
		STRING11_LOG_TIME_1,
		STRING11_LOG_MEAS_0,
		STRING11_LOG_MEAS_1,
		STRING11_HSRELAY_TEST_NEXT_TIME,
		STRING11_LSRELAY_TEST_NEXT_TIME,
		STRING11_RCD_MON_CURRENT_TH,
		STRING11_RCD_GAIN,
		STRING11_RCD_ENV_OFFSET_OFF,
		STRING11_RCD_ENV_OFFSET_ON,
		STRING11_CURR_GAIN,
		STRING11_CURR_OFFSET,
		STRING11_RESERVED0,
		STRING11_RESERVED1,
		STRING12_GEMINI_STRING,
		STRING12_SWITCHES_STATE,
		STRING12_ACTIVE_TESTS,
		STRING12_TRIPPED,
		STRING12_LOG_EVENT_0,
		STRING12_LOG_EVENT_1,
		STRING12_LOG_TIME_0,
		STRING12_LOG_TIME_1,
		STRING12_LOG_MEAS_0,
		STRING12_LOG_MEAS_1,
		STRING12_HSRELAY_TEST_NEXT_TIME,
		STRING12_LSRELAY_TEST_NEXT_TIME,
		STRING12_RCD_MON_CURRENT_TH,
		STRING12_RCD_GAIN,
		STRING12_RCD_ENV_OFFSET_OFF,
		STRING12_RCD_ENV_OFFSET_ON,
		STRING12_CURR_GAIN,
		STRING12_CURR_OFFSET,
		STRING12_RESERVED0,
		STRING12_RESERVED1,
		STRING13_GEMINI_STRING,
		STRING13_SWITCHES_STATE,
		STRING13_ACTIVE_TESTS,
		STRING13_TRIPPED,
		STRING13_LOG_EVENT_0,
		STRING13_LOG_EVENT_1,
		STRING13_LOG_TIME_0,
		STRING13_LOG_TIME_1,
		STRING13_LOG_MEAS_0,
		STRING13_LOG_MEAS_1,
		STRING13_HSRELAY_TEST_NEXT_TIME,
		STRING13_LSRELAY_TEST_NEXT_TIME,
		STRING13_RCD_MON_CURRENT_TH,
		STRING13_RCD_GAIN,
		STRING13_RCD_ENV_OFFSET_OFF,
		STRING13_RCD_ENV_OFFSET_ON,
		STRING13_CURR_GAIN,
		STRING13_CURR_OFFSET,
		STRING13_RESERVED0,
		STRING13_RESERVED1,
		STRING14_GEMINI_STRING,
		STRING14_SWITCHES_STATE,
		STRING14_ACTIVE_TESTS,
		STRING14_TRIPPED,
		STRING14_LOG_EVENT_0,
		STRING14_LOG_EVENT_1,
		STRING14_LOG_TIME_0,
		STRING14_LOG_TIME_1,
		STRING14_LOG_MEAS_0,
		STRING14_LOG_MEAS_1,
		STRING14_HSRELAY_TEST_NEXT_TIME,
		STRING14_LSRELAY_TEST_NEXT_TIME,
		STRING14_RCD_MON_CURRENT_TH,
		STRING14_RCD_GAIN,
		STRING14_RCD_ENV_OFFSET_OFF,
		STRING14_RCD_ENV_OFFSET_ON,
		STRING14_CURR_GAIN,
		STRING14_CURR_OFFSET,
		STRING14_RESERVED0,
		STRING14_RESERVED1,
		STRING15_GEMINI_STRING,
		STRING15_SWITCHES_STATE,
		STRING15_ACTIVE_TESTS,
		STRING15_TRIPPED,
		STRING15_LOG_EVENT_0,
		STRING15_LOG_EVENT_1,
		STRING15_LOG_TIME_0,
		STRING15_LOG_TIME_1,
		STRING15_LOG_MEAS_0,
		STRING15_LOG_MEAS_1,
		STRING15_HSRELAY_TEST_NEXT_TIME,
		STRING15_LSRELAY_TEST_NEXT_TIME,
		STRING15_RCD_MON_CURRENT_TH,
		STRING15_RCD_GAIN,
		STRING15_RCD_ENV_OFFSET_OFF,
		STRING15_RCD_ENV_OFFSET_ON,
		STRING15_CURR_GAIN,
		STRING15_CURR_OFFSET,
		STRING15_RESERVED0,
		STRING15_RESERVED1,
		COMBI_SN0,
		COMBI_SN1,
		COMBI_SN2,
		COMBI_SN3,
		RESERVED6,
		RESERVED7,
		RESERVED8,
		RESERVED9,
		NUM_PARAMS,
		COUNT
	}
}
