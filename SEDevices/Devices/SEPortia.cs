using SEDevices.Data;
using SEDevices.Records.Extra;
using SEDevices.Records.Parameter;
using SEDevices.Records.Status;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace SEDevices.Devices
{
	public class SEPortia : SECommDevice
	{
		private int _lastKeepAliveForPBCommandSendTimeInMiliSec = 0;

		private object _idLockObj = new object();

		private int _checkSum;

		private string _hexID;

		private object _errorListObj = new object();

		private List<uint> _errorList = null;

		private object _errorTableObj = new object();

		private Dictionary<uint, int> _lastErrorTable = null;

		private int _lastErrorTime = 0;

		public int LastKeepAliveForPBCommandSendTime
		{
			get
			{
				return this._lastKeepAliveForPBCommandSendTimeInMiliSec;
			}
		}

		public int LastKeepAliveForPBSendInterval
		{
			get
			{
				return Environment.TickCount - this._lastKeepAliveForPBCommandSendTimeInMiliSec;
			}
		}

		public uint ID
		{
			get
			{
				uint address;
				lock (this._idLockObj)
				{
					address = base.Address;
				}
				return address;
			}
			set
			{
				lock (this._idLockObj)
				{
					if (!base.Address.Equals(value))
					{
						base.Address = value;
						this._baseAddress = base.Address;
						this._checkSum = 0;
						this._hexID = null;
					}
				}
			}
		}

		public int CheckSum
		{
			get
			{
				int checkSum;
				lock (this._idLockObj)
				{
					if (this._checkSum == 0)
					{
						this._checkSum = (int)Utils.GetChecksum(this.ID);
					}
					checkSum = this._checkSum;
				}
				return checkSum;
			}
		}

		public string HexID
		{
			get
			{
				string hexID;
				lock (this._idLockObj)
				{
					if (string.IsNullOrEmpty(this._hexID))
					{
						this._hexID = string.Format("{0} {1}", this.ID.ToString("X8"), this.CheckSum.ToString("X2"));
					}
					hexID = this._hexID;
				}
				return hexID;
			}
		}

		public override uint DeviceMaxDataLength
		{
			get
			{
				return Packet.LENGTH_DATA_BUFFER_DEFAULT;
			}
		}

		public override uint DeviceResetDelay
		{
			get
			{
				return 500u;
			}
		}

		public List<uint> LastErrorsList
		{
			get
			{
				List<uint> errorList;
				lock (this._errorListObj)
				{
					if (this._errorList == null)
					{
						this._errorList = new List<uint>(0);
					}
					errorList = this._errorList;
				}
				return errorList;
			}
		}

		private Dictionary<uint, int> LastErrorTable
		{
			get
			{
				lock (this._errorTableObj)
				{
					if (this._lastErrorTable == null)
					{
						this._lastErrorTable = new Dictionary<uint, int>(0);
					}
				}
				return this._lastErrorTable;
			}
		}

		public SEPortia(CommManager commManager, uint? id, object owner, SWVersion version) : base(commManager, owner, version)
		{
			if (!id.HasValue)
			{
				this.SetSinglecast();
				SEParam sEParam = this.Command_Params_Get(PortiaParams.ID);
				id = new uint?((sEParam != null) ? sEParam.ValueUInt32 : 0u);
			}
			this.ID = id.Value;
			if (version == null)
			{
				this._deviceSWVersion = this.Command_Device_Version();
			}
		}

		protected override Type GetParamType(ushort index)
		{
			return DBParameterVersion.Instance.GetTypeOfParamPortia(index);
		}

		public int? GetNextError()
		{
			int? result = null;
			if (this.LastErrorsList != null && this.LastErrorsList.Count > 0 && base.DeviceSWVersion >= new SWVersion(2u, 38u))
			{
				result = new int?((int)this.LastErrorsList[0]);
				if (this._lastErrorTime > 0)
				{
					int ts = this.LastErrorTable[(uint)result.Value];
					DateTime d = Utils.Timestamp1970ToDate((uint)this._lastErrorTime);
					DateTime d2 = Utils.Timestamp1970ToDate((uint)ts);
					if ((d2 - d).Seconds >= 30)
					{
						result = null;
					}
				}
			}
			return result;
		}

		public void Command_Portia_UartZigbeeBridgeStart()
		{
			Packet command = base.BuildCommand(779, null);
			base.Transmit(command, base.DeviceDefaultTimeout, 3u);
		}

		public void Command_Portia_ConfToolStart(bool isStart)
		{
			this._lastKeepAliveForPBCommandSendTimeInMiliSec = Environment.TickCount;
			Packet command = base.BuildCommand(782, new List<object>(0)
			{
				isStart ? 1 : 0
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Portia_FlashFifoReset()
		{
			Packet command = base.BuildCommand(785, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 10000u, base.DeviceDefaultRetries);
		}

		public void Command_Portia_FlashReset()
		{
			Packet command = base.BuildCommand(786, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 30000u, base.DeviceDefaultRetries);
		}

		public void Command_Portia_FlashFifoEraseFast()
		{
			Packet command = base.BuildCommand(792, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 30000u, base.DeviceDefaultRetries);
		}

		public void Command_Portia_TimeAdvance(uint miliSecs)
		{
			Packet command = base.BuildCommand(791, new List<object>(0)
			{
				miliSecs
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Portia_RAMTest()
		{
			Packet command = base.BuildCommand(772, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 600000u, base.DeviceDefaultRetries);
		}

		public void Command_Portia_FlashTest()
		{
			Packet command = base.BuildCommand(773, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 600000u, base.DeviceDefaultRetries);
		}

		public PING_RESULT Command_Portia_PingTest(string ip, ushort retries)
		{
			PING_RESULT result = PING_RESULT.PR_NO_REPLY;
			Packet command = base.BuildCommand(780, new List<object>(0)
			{
				retries,
				ip
			});
			Packet packet = base.Transcieve(command, 899, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				result = (PING_RESULT)packet.RetrieveInt16();
			}
			return result;
		}

		public void Command_Portia_LcdTest(bool isStart)
		{
			Packet command = base.BuildCommand(781, new List<object>(0)
			{
				isStart ? 1 : 0
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Portia_TcpTest(ushort numPackets, ushort packetsSize)
		{
			Packet command = base.BuildCommand(790, new List<object>(0)
			{
				numPackets,
				packetsSize
			});
			base.Transmit(command, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public override SEParam Command_Params_Get(ushort index)
		{
			return this.Command_Params_Get((PortiaParams)index);
		}

		public SEParam Command_Params_Get(PortiaParams param)
		{
			SEParam sEParam = null;
			if (param == PortiaParams.ID)
			{
				sEParam = base.Command_Params_Get_Real((ushort)param);
			}
			else
			{
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.PORTIA, base.DeviceSWVersion);
				if (paramRealIndex >= 0)
				{
					sEParam = base.Command_Params_Get_Real((ushort)paramRealIndex);
				}
				if (sEParam != null)
				{
					sEParam.Index = (ushort)param;
				}
			}
			return sEParam;
		}

		public override SEParam[] Command_Params_Get(ushort[] indices)
		{
			if (indices == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (indices.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			List<PortiaParams> list = new List<PortiaParams>(0);
			int num = indices.Length;
			for (int i = 0; i < num; i++)
			{
				list.Add((PortiaParams)indices[i]);
			}
			return this.Command_Params_Get(list.ToArray());
		}

		public SEParam[] Command_Params_Get(PortiaParams[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			int num = paramList.Length;
			List<ushort> list = new List<ushort>(0);
			for (int i = 0; i < num; i++)
			{
				PortiaParams portiaParams = paramList[i];
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)portiaParams, DeviceType.PORTIA, base.DeviceSWVersion);
				if (paramRealIndex >= 0)
				{
					list.Add((ushort)paramRealIndex);
				}
			}
			SEParam[] array = base.Command_Params_Get_Real(list.ToArray());
			int num2 = array.Length;
			for (int i = 0; i < num2; i++)
			{
				SEParam sEParam = array[i];
				sEParam.Index = list[i];
			}
			return array;
		}

		public override string Command_Params_Get_Name(ushort index)
		{
			return this.Command_Params_Get_Name((PortiaParams)index);
		}

		public string Command_Params_Get_Name(PortiaParams param)
		{
			string result = null;
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.PORTIA, base.DeviceSWVersion);
			if (paramRealIndex >= 0)
			{
				result = base.Command_Params_Get_Name_Real((ushort)paramRealIndex);
			}
			return result;
		}

		public override SEParamInfo Command_Params_Get_Info(ushort index)
		{
			return this.Command_Params_Get_Info((PortiaParams)index);
		}

		public SEParamInfo Command_Params_Get_Info(PortiaParams param)
		{
			SEParamInfo result = null;
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.PORTIA, base.DeviceSWVersion);
			if (paramRealIndex >= 0)
			{
				result = base.Command_Params_Get_Info_Real((ushort)paramRealIndex);
			}
			return result;
		}

		public override int Command_Params_Get_Count()
		{
			return base.Command_Params_Get_Count_Real();
		}

		public override void Command_Params_Set(SEParam param)
		{
			if (param == null)
			{
				throw new NullReferenceException("No Parameter to send!");
			}
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(param.Index, DeviceType.PORTIA, base.DeviceSWVersion);
			if (paramRealIndex > -1)
			{
				bool flag = DBParameterVersion.Instance.IsResetRequiredForParam(param.Index, DeviceType.PORTIA, base.DeviceSWVersion);
				param.Index = (ushort)paramRealIndex;
				base.Command_Params_Set_Real(param);
				if (flag)
				{
					this.Command_Device_Reset(1000u + this.DeviceResetDelay);
					Thread.Sleep((int)(1000u + this.DeviceResetDelay));
				}
			}
		}

		public override void Command_Params_Set(SEParam[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length > 0)
			{
				int num = paramList.Length;
				for (int i = 0; i < num; i++)
				{
					SEParam sEParam = paramList[i];
					if (sEParam != null)
					{
						int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(sEParam.Index, DeviceType.PORTIA, base.DeviceSWVersion);
						if (paramRealIndex < 0)
						{
							return;
						}
						sEParam.Index = (ushort)paramRealIndex;
					}
				}
				base.Command_Params_Set_Real(paramList);
				this.Command_Device_Reset(this.DeviceResetDelay);
				Thread.Sleep((int)(1000u + this.DeviceResetDelay));
			}
		}

		public ulong Command_Portia_GetMacAddr()
		{
			ulong num = 0uL;
			Packet command = base.BuildCommand(774, null);
			Packet packet = base.Transcieve(command, 897, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				num = (ulong)packet.RetrieveUInt32() << 32;
				num |= (ulong)packet.RetrieveUInt32();
			}
			return num;
		}

		public SETelemetryLANStatus Command_Portia_GetLANStatusDetected()
		{
			SETelemetryLANStatus sETelemetryLANStatus = null;
			Packet command = base.BuildCommand(775, null);
			Packet packet = base.Transcieve(command, 898, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sETelemetryLANStatus = new SETelemetryLANStatus();
				sETelemetryLANStatus.IP = packet.RetrieveUInt32();
				sETelemetryLANStatus.SubnetMask = packet.RetrieveUInt32();
				sETelemetryLANStatus.DefaultGateway = packet.RetrieveUInt32();
				sETelemetryLANStatus.DNSIP = packet.RetrieveUInt32();
			}
			return sETelemetryLANStatus;
		}

		public SETelemetryLANStatus Command_Portia_GetLANStatusParametered()
		{
			SETelemetryLANStatus sETelemetryLANStatus = null;
			SEParam sEParam = this.Command_Params_Get(PortiaParams.MY_IP);
			SEParam sEParam2 = this.Command_Params_Get(PortiaParams.MY_SUBNETMASK_IP);
			SEParam sEParam3 = this.Command_Params_Get(PortiaParams.MY_GATEWAY_IP);
			SEParam sEParam4 = this.Command_Params_Get(PortiaParams.MY_DNS_IP);
			if (sEParam != null && sEParam2 != null && sEParam3 != null && sEParam4 != null)
			{
				sETelemetryLANStatus = new SETelemetryLANStatus();
				sETelemetryLANStatus.IP = Utils.ConvertToIP(sEParam.GetParamAsString());
				sETelemetryLANStatus.SubnetMask = Utils.ConvertToIP(sEParam2.GetParamAsString());
				sETelemetryLANStatus.DefaultGateway = Utils.ConvertToIP(sEParam3.GetParamAsString());
				sETelemetryLANStatus.DNSIP = Utils.ConvertToIP(sEParam4.GetParamAsString());
			}
			return sETelemetryLANStatus;
		}

		public int Command_Portia_EthernetStatus()
		{
			int result = 0;
			Packet command = base.BuildCommand(783, null);
			Packet packet = base.Transcieve(command, 900, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				result = (int)packet.RetrieveInt16();
			}
			return result;
		}

		public string Command_Portia_FlashFifoStatus()
		{
			string result = null;
			Packet command = base.BuildCommand(784, null);
			Packet packet = base.Transcieve(command, 901, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				ushort num = packet.RetrieveUInt16();
				ushort num2 = packet.RetrieveUInt16();
				uint num3 = packet.RetrieveUInt32();
				uint num4 = packet.RetrieveUInt32();
				ushort num5 = packet.RetrieveUInt16();
				ushort num6 = packet.RetrieveUInt16();
				result = string.Concat(new object[]
				{
					"Flash Status: ",
					num,
					", ",
					num2,
					", ",
					num3,
					", ",
					num4,
					", ",
					num5,
					", ",
					num6
				});
			}
			return result;
		}

		public void Command_Portia_SlaveDetect(bool isSetSlaves)
		{
			Packet command = base.BuildCommand(787, new List<object>(0)
			{
				isSetSlaves ? 1 : 0
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public bool Command_Portia_SlaveDetectStatus(ref int numSlaves, ref float progress)
		{
			bool result = false;
			Packet command = base.BuildCommand(788, null);
			Packet packet = base.Transcieve(command, 902, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				ushort num = packet.RetrieveUInt16();
				numSlaves = (int)packet.RetrieveUInt16();
				ushort num2 = packet.RetrieveUInt16();
				ushort num3 = packet.RetrieveUInt16();
				if (num == 1)
				{
					progress = (float)num2 * 100f / (float)num3;
					result = true;
				}
				else
				{
					progress = 100f;
					result = false;
				}
			}
			return result;
		}

		public void Command_Portia_SlaveDetect_Gemini(bool isSetSlaves)
		{
			Packet command = base.BuildCommand(805, new List<object>(0)
			{
				isSetSlaves ? 1 : 0
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public bool Command_Portia_SlaveDetectStatus_Gemini(ref int numSlaves, ref float progress)
		{
			bool result = false;
			Packet command = base.BuildCommand(806, null);
			Packet packet = base.Transcieve(command, 911, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				ushort num = packet.RetrieveUInt16();
				numSlaves = (int)packet.RetrieveUInt16();
				ushort num2 = packet.RetrieveUInt16();
				ushort num3 = packet.RetrieveUInt16();
				if (num == 1)
				{
					progress = (float)num2 * 100f / (float)num3;
					result = true;
				}
				else
				{
					progress = 100f;
					result = false;
				}
			}
			return result;
		}

		public int[] Command_Portia_Get_P_OK()
		{
			int[] array = null;
			int[] result;
			if (base.DeviceSWVersion <= new SWVersion(2u, 38u))
			{
				result = null;
			}
			else
			{
				Packet command = base.BuildCommand(796, null);
				Packet packet = base.Transcieve(command, 904, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
				if (packet != null)
				{
					array = new int[]
					{
						(int)packet.RetrieveUInt16(),
						(int)packet.RetrieveUInt16()
					};
				}
				result = array;
			}
			return result;
		}

		public S_OK_STATUS Command_Portia_Get_S_OK()
		{
			S_OK_STATUS s_OK_STATUS = S_OK_STATUS.UNKNOWN;
			S_OK_STATUS result;
			if (base.DeviceSWVersion <= new SWVersion(2u, 38u))
			{
				result = s_OK_STATUS;
			}
			else
			{
				Packet command = base.BuildCommand(801, null);
				Packet packet = base.Transcieve(command, 908, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
				if (packet != null)
				{
					ushort num = packet.RetrieveUInt16();
					s_OK_STATUS = (S_OK_STATUS)num;
				}
				result = s_OK_STATUS;
			}
			return result;
		}

		public SEEnergyStatus Command_Portia_Get_EnergyStatus()
		{
			SEEnergyStatus sEEnergyStatus = null;
			SEEnergyStatus result;
			if (base.DeviceSWVersion <= new SWVersion(2u, 38u))
			{
				result = sEEnergyStatus;
			}
			else
			{
				Packet command = base.BuildCommand(802, null);
				Packet packet = base.Transcieve(command, 909, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
				if (packet != null)
				{
					float day = packet.RetrieveSingle();
					float month = packet.RetrieveSingle();
					float year = packet.RetrieveSingle();
					float total = packet.RetrieveSingle();
					sEEnergyStatus = new SEEnergyStatus(day, month, year, total);
				}
				result = sEEnergyStatus;
			}
			return result;
		}

		public bool Command_Portia_Gemini_Get_GF_Detected()
		{
			bool flag = false;
			bool result;
			if (base.DeviceSWVersion < new SWVersion(2u, 38u))
			{
				result = false;
			}
			else
			{
				Packet command = base.BuildCommand(807, null);
				Packet packet = base.Transcieve(command, 912, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
				if (packet != null)
				{
					flag = (packet.RetrieveUInt16() == 1);
				}
				result = flag;
			}
			return result;
		}

		public void Command_Portia_Gemini_Update_Error_Log()
		{
			if (!(base.DeviceSWVersion < new SWVersion(2u, 38u)))
			{
				int num = 5;
				string format = "ERROR_LOG_{0}_TIME";
				string format2 = "ERROR_LOG_{0}_CODE";
				List<PortiaParams> list = new List<PortiaParams>(0);
				for (int i = 0; i < num; i++)
				{
					PortiaParams item = (PortiaParams)Enum.Parse(typeof(PortiaParams), string.Format(format, i + 1));
					PortiaParams item2 = (PortiaParams)Enum.Parse(typeof(PortiaParams), string.Format(format2, i + 1));
					list.Add(item);
					list.Add(item2);
				}
				PortiaParams[] paramList = list.ToArray();
				SEParam[] array = this.Command_Params_Get(paramList);
				if (array != null)
				{
					try
					{
						int num2 = array.Length;
						if (num2 % 2 <= 0)
						{
							for (int i = 0; i < num2; i += 2)
							{
								SEParam sEParam = array[i];
								SEParam sEParam2 = array[i + 1];
								uint valueUInt = sEParam2.ValueUInt32;
								int valueInt = sEParam.ValueInt32;
								this.LastErrorsList.Insert(0, valueUInt);
								this._lastErrorTime = valueInt;
								if (this.LastErrorTable.Count > 0 && this.LastErrorTable.ContainsKey(valueUInt))
								{
									this.LastErrorTable[valueUInt] = valueInt;
								}
								else
								{
									this.LastErrorTable.Add(valueUInt, valueInt);
								}
								if (this.LastErrorsList.Count > 5)
								{
									this.LastErrorsList.RemoveAt(5);
								}
							}
						}
					}
					catch (Exception var_14_1B6)
					{
					}
				}
			}
		}

		public bool Command_Portia_Get_ZB_Exists()
		{
			bool flag = true;
			bool result;
			if (base.DeviceSWVersion <= new SWVersion(2u, 23u))
			{
				result = flag;
			}
			else
			{
				Packet command = base.BuildCommand(798, null);
				Packet packet = base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
				flag = (packet != null);
				result = flag;
			}
			return result;
		}

		public Zones Command_Portia_Get_Zone()
		{
			Zones result = Zones.None;
			try
			{
				SEParam sEParam = this.Command_Params_Get(PortiaParams.SETUP_COUNTRY_NORTH_AMERICA_ZONE);
				SEParam sEParam2 = this.Command_Params_Get(PortiaParams.SETUP_COUNTRY_EUROPE_ZONE);
				SEParam sEParam3 = this.Command_Params_Get(PortiaParams.SETUP_COUNTRY_APAC_ZONE);
				SEParam sEParam4 = this.Command_Params_Get(PortiaParams.SETUP_COUNTRY_ALL_ZONES);
				int num = (int)((sEParam != null) ? sEParam.ValueUInt32 : 0u);
				int num2 = (int)((sEParam != null) ? sEParam2.ValueUInt32 : 0u);
				int num3 = (int)((sEParam != null) ? sEParam3.ValueUInt32 : 0u);
				int num4 = (int)((sEParam != null) ? sEParam4.ValueUInt32 : 0u);
				if (num4 == 1)
				{
					result = Zones.All;
				}
				else
				{
					int num5 = num + num2 * 2 + num3 * 4;
					try
					{
						result = (Zones)num5;
					}
					catch (Exception var_10_8C)
					{
					}
				}
			}
			catch (Exception var_11_97)
			{
			}
			return result;
		}

		public void Command_Portia_Lock(bool enabled, uint blockingTimeoutInMiliSec)
		{
			List<object> list = new List<object>(0);
			ushort num = enabled ? 1 : 0;
			list.Add(num);
			list.Add(ProtocolMain.PROT_WEBSERVICE_ADDR);
			list.Add(blockingTimeoutInMiliSec);
			Packet command = base.BuildCommand(809, list);
			base.Transmit(command, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public bool Command_Portia_IsLocked()
		{
			bool result = false;
			Packet command = base.BuildCommand(810, null);
			Packet packet = base.Transcieve(command, 914, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				try
				{
					ushort num = packet.RetrieveUInt16();
					result = (num == 1);
				}
				catch (Exception var_4_44)
				{
				}
			}
			return result;
		}

		public void Command_Portia_SetRTC(DateTime localTime, uint gmtOffsetInSecs)
		{
			List<object> list = new List<object>(0);
			DateTime date = localTime.ToUniversalTime();
			uint num = Utils.DateToTimestamp1970(date);
			list.Add(num);
			MemoryStream memoryStream = new MemoryStream();
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			BinaryReader binaryReader = new BinaryReader(memoryStream);
			binaryWriter.Write(gmtOffsetInSecs);
			memoryStream.Position = 0L;
			int num2 = binaryReader.ReadInt32();
			list.Add(num2);
			Packet command = base.BuildCommand(771, list);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Portia_SetZB(ushort panID, uint scanChannel)
		{
			Packet command = base.BuildCommand(789, new List<object>(0)
			{
				panID,
				scanChannel
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 10000u, base.DeviceDefaultRetries);
		}

		public override SWVersion Command_Device_Version()
		{
			SWVersion sWVersion = new SWVersion();
			SEParam sEParam = base.Command_Params_Get_Real(1);
			SEParam sEParam2 = base.Command_Params_Get_Real(2);
			string text = (sEParam != null) ? sEParam.GetParamAsString() : null;
			string text2 = (sEParam2 != null) ? sEParam2.GetParamAsString() : null;
			if (text != null)
			{
				sWVersion.Major = uint.Parse(text);
			}
			if (text2 != null)
			{
				sWVersion.Minor = uint.Parse(text2);
			}
			return sWVersion;
		}

		public override void UpgradeState_VerifyCompliance(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList)
		{
			if (stateData != null && deviceVersionList != null && deviceUpgradeVersionList != null && deviceCompatabilityList != null)
			{
				PortiaVersionInfo portiaVersionInfo = (PortiaVersionInfo)deviceVersionList[0];
				PortiaVersionInfo portiaVersion = (PortiaVersionInfo)deviceUpgradeVersionList[0];
				List<PortiaCompatibilityItem> list = new List<PortiaCompatibilityItem>(0);
				int count = deviceCompatabilityList.Count;
				for (int i = 0; i < count; i++)
				{
					list.Add((PortiaCompatibilityItem)deviceCompatabilityList[i]);
				}
				bool flag = base.CommManager.ConnectedRemotely.HasValue && base.CommManager.ConnectedRemotely.Value;
				if (portiaVersionInfo.Equals(portiaVersion))
				{
					stateData.UpgradeState = UpgradeState.Finished;
				}
				else
				{
					PortiaCompatibilityItem portiaCompatibilityItem = null;
					foreach (PortiaCompatibilityItem current in list)
					{
						foreach (CompatiblePortiaVersion current2 in current.PortiaVersions)
						{
							if (portiaVersionInfo.Equals(current2.PortiaVersion))
							{
								if (!current2.AllowRemoteUpgrade && flag)
								{
									throw new Exception("ERROR: Cannot upgrade version-" + portiaVersionInfo.ToString() + "remotly");
								}
								portiaCompatibilityItem = current;
								break;
							}
						}
						if (portiaCompatibilityItem != null)
						{
							break;
						}
					}
					if (portiaCompatibilityItem == null)
					{
						throw new Exception("ERROR: SUF not compatible with Portia version - (" + portiaVersionInfo.ToString() + ")");
					}
					stateData.PortiaCompatibiltyItem = portiaCompatibilityItem;
					stateData.OriginalPortiaVersion = portiaVersionInfo;
					stateData.UpgradeState = UpgradeState.SaveParamaeters;
				}
			}
		}

		public override void UpgradeState_SaveParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, uint? sufVersion)
		{
			if (stateData != null && deviceVersionList != null)
			{
				PortiaVersionInfo portiaVersionInfo = (PortiaVersionInfo)deviceVersionList[0];
				if (!portiaVersionInfo.Equals(stateData.OriginalPortiaVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				stateData.SavedParameters = new List<ParamaeterPair>();
				foreach (int current in stateData.PortiaCompatibiltyItem.RestoreParams)
				{
					int num = current;
					if (!sufVersion.HasValue)
					{
						ushort? paramVirtualIndex = DBParameterVersion.Instance.GetParamVirtualIndex(num, DeviceType.PORTIA, base.DeviceSWVersion);
						ushort? num2 = paramVirtualIndex;
						if ((num2.HasValue ? new int?((int)num2.GetValueOrDefault()) : null).HasValue)
						{
							num = (int)paramVirtualIndex.Value;
						}
					}
					SEParam sEParam = this.Command_Params_Get((PortiaParams)num);
					string paramAsString = sEParam.GetParamAsString();
					ParamaeterPair paramaeterPair = new ParamaeterPair();
					paramaeterPair.Index = num;
					paramaeterPair.Value = paramAsString;
					stateData.SavedParameters.Add(paramaeterPair);
				}
				List<SEParam> list = new List<SEParam>(0);
				list.Clear();
				if (stateData.PortiaCompatibiltyItem.ParamatersToSetBeforeUpgrade != null)
				{
					foreach (ParamaeterPair paramaeterPair in stateData.PortiaCompatibiltyItem.ParamatersToSetBeforeUpgrade)
					{
						ushort num3 = (ushort)paramaeterPair.Index;
						Type typeOfParamPortia = DBParameterVersion.Instance.GetTypeOfParamPortia(num3);
						ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamPortia);
						SEParam item = null;
						switch (paramTypeByType)
						{
						case ParamType.UINT32:
							item = new SEParam(num3, uint.Parse(paramaeterPair.Value));
							break;
						case ParamType.FLOAT:
							item = new SEParam(num3, float.Parse(paramaeterPair.Value));
							break;
						case ParamType.INT:
							item = new SEParam(num3, int.Parse(paramaeterPair.Value));
							break;
						case ParamType.STRING:
							item = new SEParam(num3, paramaeterPair.Value);
							break;
						}
						list.Add(item);
					}
				}
				this.Command_Params_Set(list.ToArray());
				stateData.UpgradeState = UpgradeState.UpgradeSoftware;
			}
		}

		public override void UpgradeState_UpgradeSoftwareStatus(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData)
		{
			if (stateData != null && upgradeXmlData != null)
			{
				uint checksum = 0u;
				if (upgradeXmlData.UpgradeData.DataChecksum.HasValue)
				{
					checksum = upgradeXmlData.UpgradeData.DataChecksum.Value;
				}
				uint deviceDefaultTimeout = base.DeviceDefaultTimeout;
				base.DeviceDefaultTimeout = deviceDefaultTimeout * 2u;
				if (stateData.UpgradeBytesCompleted.HasValue)
				{
					int value = stateData.UpgradeBytesCompleted.Value;
				}
				try
				{
					base.DoUpgrade(upgradeXmlData.UpgradeData.Data, upgradeXmlData.UpgradeData.Data.Length, 0u, checksum, 0, base.UpgradeNotifier);
				}
				catch (Exception ex)
				{
					stateData.UpgradeBytesCompleted = new int?((int)ex.Data["BytesCompleted"]);
					throw ex;
				}
				base.DeviceDefaultTimeout = deviceDefaultTimeout;
				stateData.UpgradeState = UpgradeState.VerifyVersionAfterUpgrade;
			}
		}

		public override void UpgradeState_VerifyVersionAfterUpgrade(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraCommDevice)
		{
			if (stateData != null && deviceVersionList != null)
			{
				PortiaVersionInfo portiaVersionInfo = (PortiaVersionInfo)deviceVersionList[0];
				PortiaVersionInfo portiaVersion = (PortiaVersionInfo)deviceVersionList[1];
				bool flag = base.CommManager.ConnectedRemotely.HasValue && base.CommManager.ConnectedRemotely.Value;
				stateData.PreviousPortiaAddress = new uint?(this.ID);
				if (!flag)
				{
					this.SetSinglecast();
				}
				this.Command_Device_Reset(0u);
				int deviceDefaultRetries = (int)base.DeviceDefaultRetries;
				base.DeviceDefaultRetries = 50u;
				this._deviceSWVersion = this.Command_Device_Version();
				portiaVersionInfo.MajorVersion = base.DeviceSWVersion.Major;
				portiaVersionInfo.MinorVersion = base.DeviceSWVersion.Minor;
				if (!portiaVersionInfo.Equals(portiaVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					base.DeviceDefaultRetries = (uint)deviceDefaultRetries;
					foreach (ParamaeterPair current in stateData.SavedParameters)
					{
						if (current.Index == 0)
						{
							SEParam param = new SEParam((ushort)current.Index, uint.Parse(current.Value));
							this.Command_Params_Set(param);
							stateData.PreviousPortiaAddress = new uint?(uint.Parse(current.Value));
							break;
						}
					}
					this.ID = stateData.PreviousPortiaAddress.Value;
					stateData.UpgradeState = UpgradeState.ConfigureParamaeters;
				}
			}
		}

		public override void UpgradeState_ConfigureParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList)
		{
			if (stateData != null && deviceVersionList != null)
			{
				PortiaVersionInfo portiaVersionInfo = (PortiaVersionInfo)deviceVersionList[0];
				PortiaVersionInfo portiaVersion = (PortiaVersionInfo)deviceVersionList[1];
				bool flag = base.CommManager.ConnectedRemotely.HasValue && base.CommManager.ConnectedRemotely.Value;
				if (!portiaVersionInfo.Equals(portiaVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					List<SEParam> list = new List<SEParam>(0);
					foreach (ParamaeterPair current in stateData.SavedParameters)
					{
						ushort num = (ushort)current.Index;
						Type typeOfParamPortia = DBParameterVersion.Instance.GetTypeOfParamPortia(num);
						ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamPortia);
						SEParam item = null;
						switch (paramTypeByType)
						{
						case ParamType.UINT32:
							item = new SEParam(num, uint.Parse(current.Value));
							break;
						case ParamType.FLOAT:
							item = new SEParam(num, float.Parse(current.Value));
							break;
						case ParamType.INT:
							item = new SEParam(num, int.Parse(current.Value));
							break;
						case ParamType.STRING:
							item = new SEParam(num, current.Value);
							break;
						}
						list.Add(item);
					}
					this.Command_Params_Set(list.ToArray());
					list.Clear();
					foreach (ParamaeterPair current in stateData.PortiaCompatibiltyItem.ParamatersToSet)
					{
						ushort num = (ushort)current.Index;
						Type typeOfParamPortia = DBParameterVersion.Instance.GetTypeOfParamPortia(num);
						ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamPortia);
						SEParam item = null;
						switch (paramTypeByType)
						{
						case ParamType.UINT32:
							item = new SEParam(num, uint.Parse(current.Value));
							break;
						case ParamType.FLOAT:
							item = new SEParam(num, float.Parse(current.Value));
							break;
						case ParamType.INT:
							item = new SEParam(num, int.Parse(current.Value));
							break;
						case ParamType.STRING:
							item = new SEParam(num, current.Value);
							break;
						}
						list.Add(item);
					}
					this.Command_Params_Set(list.ToArray());
					stateData.UpgradeState = UpgradeState.Finished;
					if (!flag)
					{
						this.ID = stateData.PreviousPortiaAddress.Value;
					}
				}
			}
		}

		public override void UpgradeState_Finished(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion)
		{
			PortiaVersionInfo portiaVersionInfo = (PortiaVersionInfo)deviceVersion;
			PortiaVersionInfo portiaVersion = (PortiaVersionInfo)upgradeVersion;
			if (!portiaVersionInfo.Equals(portiaVersion))
			{
				stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
		}
	}
}
