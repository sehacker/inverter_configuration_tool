using System;

namespace SEDevices.Devices
{
	public enum PING_RESULT
	{
		PR_CONNECTION_FAILED = -3,
		PR_UNKNOWN_ERROR,
		PR_NO_REPLY,
		PR_OK
	}
}
