using SEDevices.Data;
using SEDevices.Records.Parameter;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEProtocol.PLC;
using SEProtocol.PLC.Packets;
using SEProtocol.PLC.Packets.Base;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets;
using SEProtocol.SDP.Packets.Base;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace SEDevices.Devices
{
	public class SEMercury : SECommDevice
	{
		private ISEPLCCommandable _plcCommandableOwner = null;

		private PLCManager _plcManager = null;

		public override uint DeviceMaxDataLength
		{
			get
			{
				return 20u;
			}
		}

		public override uint DeviceResetDelay
		{
			get
			{
				return 100u;
			}
		}

		public ISEPLCCommandable PLCCommandableOwner
		{
			get
			{
				return this._plcCommandableOwner;
			}
			set
			{
				this._plcCommandableOwner = value;
			}
		}

		public SEMercury(uint address, CommManager commManager, ISEPLCCommandable plcCommandableOwner, object owner, SWVersion version) : base(commManager, owner, version)
		{
			this._address = address;
			this._plcCommandableOwner = plcCommandableOwner;
			base.DeviceDefaultTimeout = 10000u;
			if (this._plcCommandableOwner != null)
			{
				this._plcManager = new PLCManager(this._plcCommandableOwner, address);
			}
			if (this._plcManager != null)
			{
				this._plcManager.StartConnection();
			}
			if (version == null)
			{
				this._deviceSWVersion = this.Command_Device_Version();
			}
		}

		private SEParam[] ParseGetParameterResponse(List<ushort> indices, byte[] data)
		{
			SEParam[] result;
			if (data == null)
			{
				result = null;
			}
			else
			{
				List<SEParam> list = null;
				MemoryStream memoryStream = new MemoryStream(data);
				BinaryReader binaryReader = new BinaryReader(memoryStream);
				byte b = binaryReader.ReadByte();
				byte b2 = binaryReader.ReadByte();
				uint num = binaryReader.ReadUInt32();
				uint num2 = binaryReader.ReadUInt32();
				int num3 = -1;
				while (memoryStream.Position < (long)((ulong)b))
				{
					uint num4 = binaryReader.ReadUInt32();
					short num5 = binaryReader.ReadInt16();
					num3++;
					SEParam item = null;
					ushort index = (ushort)num3;
					switch (num5)
					{
					case 0:
						item = new SEParam(index, num4);
						break;
					case 1:
						item = new SEParam(index, Utils.UInt32AsFloat(num4));
						break;
					case 2:
						item = new SEParam(index, Utils.UInt32AsInt32(num4));
						break;
					}
					if (list == null)
					{
						list = new List<SEParam>(0);
					}
					list.Add(item);
				}
				result = ((list != null) ? list.ToArray() : null);
			}
			return result;
		}

		private PLCPacket parseResponse(Packet response)
		{
			PLCPacket pLCPacket = null;
			PLCPacket result;
			if (response == null)
			{
				result = pLCPacket;
			}
			else
			{
				byte[] data = response.Data;
				if (data == null)
				{
					result = pLCPacket;
				}
				else
				{
					int length = response.Length;
					byte b = response.RetrieveBytes(1)[0];
					byte b2 = response.RetrieveBytes(1)[0];
					ProtocolPLCResponses protocolPLCResponses = (ProtocolPLCResponses)b2;
					ProtocolPLCResponses protocolPLCResponses2 = protocolPLCResponses;
					switch (protocolPLCResponses2)
					{
					case ProtocolPLCResponses.PLC_RESP_MERCURY_TELEM:
					case ProtocolPLCResponses.PLC_RESP_PROBE_TELEM:
						break;
					default:
						switch (protocolPLCResponses2)
						{
						case ProtocolPLCResponses.PLC_RESP_ACK:
						case ProtocolPLCResponses.PLC_RESP_LOG:
						case ProtocolPLCResponses.PLC_RESP_IVTRACE:
							break;
						case ProtocolPLCResponses.PLC_RESP_PARAM:
							break;
						default:
							if (protocolPLCResponses2 != ProtocolPLCResponses.PLC_RESP_PROBE_ALIVE)
							{
							}
							break;
						}
						break;
					}
					uint num = response.RetrieveUInt32();
					uint num2 = response.RetrieveUInt32();
					result = pLCPacket;
				}
			}
			return result;
		}

		public override SEParam Command_Params_Get(ushort index)
		{
			return this.Command_Params_Get((MercuryParams)index);
		}

		public SEParam Command_Params_Get(MercuryParams param)
		{
			SEParam result = null;
			if (param == MercuryParams.ID || param == MercuryParams.VERSION_MAJOR || param == MercuryParams.VERSION_MINOR || param == MercuryParams.TELEM_AVG_TIME_SEC)
			{
				List<ushort> list = new List<ushort>(0);
				list.Add((ushort)param);
				PLCPacketParamGet plcCommand = new PLCPacketParamGet(this.DeviceMaxDataLength, list, ProtocolMain.PROT_CONFTOOL_ADDR, base.Address);
				byte[] data = this._plcManager.SendReceive(plcCommand, base.DeviceDefaultTimeout, false);
				SEParam[] array = this.ParseGetParameterResponse(list, data);
				result = ((array != null) ? array[0] : null);
			}
			else
			{
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.MERCURY, base.DeviceSWVersion);
				if (paramRealIndex >= 0)
				{
					List<ushort> list = new List<ushort>(0);
					list.Add((ushort)paramRealIndex);
					PLCPacketParamGet plcCommand = new PLCPacketParamGet(this.DeviceMaxDataLength, list, ProtocolMain.PROT_CONFTOOL_ADDR, base.Address);
					byte[] data = this._plcManager.SendReceive(plcCommand, base.DeviceDefaultTimeout, false);
					SEParam[] array = this.ParseGetParameterResponse(list, data);
					result = ((array != null) ? array[0] : null);
				}
			}
			return result;
		}

		public override SEParam[] Command_Params_Get(ushort[] indices)
		{
			if (indices == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (indices.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			List<MercuryParams> list = new List<MercuryParams>(0);
			int num = indices.Length;
			for (int i = 0; i < num; i++)
			{
				list.Add((MercuryParams)indices[i]);
			}
			return this.Command_Params_Get(list.ToArray());
		}

		public SEParam[] Command_Params_Get(MercuryParams[] paramList)
		{
			SEParam[] result = null;
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			int num = paramList.Length;
			List<ushort> list = new List<ushort>(0);
			for (int i = 0; i < num; i++)
			{
				MercuryParams mercuryParams = paramList[i];
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)mercuryParams, DeviceType.MERCURY, base.DeviceSWVersion);
				if (paramRealIndex >= 0)
				{
					list.Add((ushort)paramRealIndex);
				}
			}
			PLCPacketParamGet plcCommand = new PLCPacketParamGet(this.DeviceMaxDataLength, list, ProtocolMain.PROT_CONFTOOL_ADDR, base.Address);
			byte[] data = this._plcManager.SendReceive(plcCommand, base.DeviceDefaultTimeout, false);
			SEParam[] array = this.ParseGetParameterResponse(list, data);
			return result;
		}

		public override string Command_Params_Get_Name(ushort index)
		{
			return null;
		}

		public string Command_Params_Get_Name(MercuryParams param)
		{
			return null;
		}

		public override SEParamInfo Command_Params_Get_Info(ushort index)
		{
			return null;
		}

		public SEParamInfo Command_Params_Get_Info(MercuryParams param)
		{
			return null;
		}

		public override int Command_Params_Get_Count()
		{
			return 0;
		}

		public override void Command_Params_Set(SEParam param)
		{
			if (param == null)
			{
				throw new NullReferenceException("No Parameter to send!");
			}
			int num;
			if (param.Index == 1 || param.Index == 2 || param.Index == 3 || param.Index == 61)
			{
				num = (int)param.Index;
			}
			else
			{
				num = DBParameterVersion.Instance.GetParamRealIndex(param.Index, DeviceType.MERCURY, base.DeviceSWVersion);
			}
			if (num > -1)
			{
				param.Index = (ushort)num;
				List<ushort> list = new List<ushort>(0);
				List<uint> list2 = new List<uint>(0);
				list.Add((ushort)num);
				list2.Add(param.ValueUInt32);
				PLCPacketParamSet plcCommand = new PLCPacketParamSet(this.DeviceMaxDataLength, ProtocolMain.PROT_CONFTOOL_ADDR, base.Address, list, list2);
				byte[] data = this._plcManager.SendReceive(plcCommand, base.DeviceDefaultTimeout, false);
				SEParam[] array = this.ParseGetParameterResponse(list, data);
			}
		}

		public void Command_Mercury_Params_Set_Volatile(SEParam param)
		{
			if (param == null)
			{
				throw new NullReferenceException("No Parameter to send!");
			}
			int num;
			if (param.Index == 1 || param.Index == 2 || param.Index == 3 || param.Index == 61)
			{
				num = (int)param.Index;
			}
			else
			{
				num = DBParameterVersion.Instance.GetParamRealIndex(param.Index, DeviceType.MERCURY, base.DeviceSWVersion);
			}
			if (num > -1)
			{
				param.Index = (ushort)num;
				List<ushort> list = new List<ushort>(0);
				List<uint> list2 = new List<uint>(0);
				list.Add((ushort)num);
				list2.Add(param.ValueUInt32);
				PLCPacketParamSetVolatile plcCommand = new PLCPacketParamSetVolatile(this.DeviceMaxDataLength, ProtocolMain.PROT_CONFTOOL_ADDR, base.Address, list, list2);
				byte[] data = this._plcManager.SendReceive(plcCommand, base.DeviceDefaultTimeout, false);
				SEParam[] array = this.ParseGetParameterResponse(list, data);
			}
		}

		public override void Command_Params_Set(SEParam[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			int num = paramList.Length;
			for (int i = 0; i < num; i++)
			{
				SEParam sEParam = paramList[i];
				if (sEParam != null)
				{
					int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(sEParam.Index, DeviceType.PORTIA, base.DeviceSWVersion);
					if (paramRealIndex < 0)
					{
						return;
					}
					sEParam.Index = (ushort)paramRealIndex;
				}
			}
			base.Command_Params_Set_Real(paramList);
			this.Command_Device_Reset(this.DeviceResetDelay);
			Thread.Sleep((int)(1000u + this.DeviceResetDelay));
		}

		public override void Command_Device_Reset()
		{
			PLCPacket pLCPacket = new PLCPacketReset(this.DeviceMaxDataLength, base.Address);
			this.PLCCommandableOwner.Command_PLC_Transmit(pLCPacket.GetTransmition(), false);
		}

		public SWVersion Command_Device_Version(bool shouldOverride)
		{
			SWVersion sWVersion = this.Command_Device_Version();
			if (shouldOverride)
			{
				this._deviceSWVersion = sWVersion;
			}
			return sWVersion;
		}

		public override SWVersion Command_Device_Version()
		{
			SEParam sEParam = this.Command_Params_Get(MercuryParams.VERSION_MINOR);
			uint major = (sEParam != null) ? sEParam.ValueUInt32 : 0u;
			return new SWVersion(major);
		}

		public void Command_Mercury_PLC_Pairing_Start(uint src, byte freqs)
		{
			PLCPacketPairingStart pLCPacketPairingStart = new PLCPacketPairingStart(base.DeviceDefaultRetries, src, base.Address, freqs);
			PLCManager arg_54_0 = this._plcManager;
			PLCPacket arg_54_1 = pLCPacketPairingStart;
			uint arg_54_2 = base.DeviceDefaultTimeout;
			byte? expectedResponse = pLCPacketPairingStart.ExpectedResponse;
			byte[] array = arg_54_0.SendReceive(arg_54_1, arg_54_2, !(expectedResponse.HasValue ? new int?((int)expectedResponse.GetValueOrDefault()) : null).HasValue);
		}

		public void Command_Mercury_PLC_Pairing_Data(uint src)
		{
			PLCPacketPairingData pLCPacketPairingData = new PLCPacketPairingData(base.DeviceDefaultRetries, src);
			PLCManager arg_4D_0 = this._plcManager;
			PLCPacket arg_4D_1 = pLCPacketPairingData;
			uint arg_4D_2 = base.DeviceDefaultTimeout;
			byte? expectedResponse = pLCPacketPairingData.ExpectedResponse;
			byte[] array = arg_4D_0.SendReceive(arg_4D_1, arg_4D_2, !(expectedResponse.HasValue ? new int?((int)expectedResponse.GetValueOrDefault()) : null).HasValue);
		}

		public void Command_Mercury_PLC_Force_Telemetry()
		{
			PLCPacketForceTelem pLCPacketForceTelem = new PLCPacketForceTelem(this.DeviceMaxDataLength, base.Address);
			PLCManager arg_52_0 = this._plcManager;
			PLCPacket arg_52_1 = pLCPacketForceTelem;
			uint arg_52_2 = base.DeviceDefaultTimeout;
			byte? expectedResponse = pLCPacketForceTelem.ExpectedResponse;
			byte[] array = arg_52_0.SendReceive(arg_52_1, arg_52_2, !(expectedResponse.HasValue ? new int?((int)expectedResponse.GetValueOrDefault()) : null).HasValue);
		}

		public static void Command_Mercury_PLC_Set_Telem_Speed(int seconds, ISEPLCCommandable plcCommandableDevice, uint numOfTimesToSend)
		{
			PLCManager pLCManager = new PLCManager(plcCommandableDevice, ProtocolMain.PROT_BROADCAST_ADDR);
			List<ushort> list = new List<ushort>(0);
			List<uint> list2 = new List<uint>(0);
			list.Add(61);
			list2.Add((uint)seconds);
			PLCPacketParamSetVolatile plcCommand = new PLCPacketParamSetVolatile(20u, plcCommandableDevice.GetAddress(), ProtocolMain.PROT_BROADCAST_ADDR, list, list2);
			pLCManager.StartConnection();
			int num = 0;
			while ((long)num < (long)((ulong)numOfTimesToSend))
			{
				pLCManager.SendReceive(plcCommand, 10000u, true);
				num++;
			}
		}

		private static int[] Command_Test_PLC_Broadcast(SEMercury mercuryToTestOn, ISEPLCCommandable plcCommandableDevice)
		{
			SEParam sEParam = mercuryToTestOn.Command_Params_Get(MercuryParams.TELEM_AVG_TIME_SEC);
			uint num = ProtocolMain.PROT_BROADCAST_ADDR;
			PLCManager pLCManager = new PLCManager(plcCommandableDevice, num);
			List<ushort> list = new List<ushort>(0);
			List<uint> list2 = new List<uint>(0);
			list.Add(61);
			uint item = 300u;
			list2.Add(item);
			PLCPacketParamSet plcCommand = new PLCPacketParamSet(20u, plcCommandableDevice.GetAddress(), num, list, list2);
			pLCManager.StartConnection();
			pLCManager.SendReceive(plcCommand, 10000u, true);
			pLCManager.CloseConnection();
			SEParam sEParam2 = mercuryToTestOn.Command_Params_Get(MercuryParams.TELEM_AVG_TIME_SEC);
			return null;
		}

		public override void UpgradeState_VerifyCompliance(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList)
		{
		}

		public override void UpgradeState_SaveParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, uint? sufVersion)
		{
		}

		public override void UpgradeState_UpgradeSoftwareStatus(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData)
		{
		}

		public override void UpgradeState_VerifyVersionAfterUpgrade(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraCommDevice)
		{
		}

		public override void UpgradeState_ConfigureParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList)
		{
		}

		public override void UpgradeState_Finished(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion)
		{
		}
	}
}
