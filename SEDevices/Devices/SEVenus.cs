using SEDevices.Data;
using SEDevices.Records.Extra;
using SEDevices.Records.Parameter;
using SEDevices.Records.PLC;
using SEDevices.Records.Status;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using SEProtocol.PLC;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Threading;

namespace SEDevices.Devices
{
	public class SEVenus : SECommDevice, ISEPLCCommandable
	{
		public const uint PROT_INVERTER_OFFSET = 8388608u;

		protected SWVersion _deviceSWVersionPwr;

		public SWVersion DeviceSWVersionPower
		{
			get
			{
				return this._deviceSWVersionPwr;
			}
		}

		public override uint DeviceMaxDataLength
		{
			get
			{
				return 256u;
			}
		}

		public override uint DeviceResetDelay
		{
			get
			{
				return 200u;
			}
		}

		public SEVenus(uint address, CommManager commManager, object owner, SWVersion version, SWVersion pwrVersion) : base(commManager, owner, version)
		{
			this._address = address;
			if (version == null)
			{
				this._deviceSWVersion = this.Command_Device_Version();
			}
			if (pwrVersion == null)
			{
				this._deviceSWVersionPwr = this.Command_Device_Version_Pwr();
			}
		}

		private SETelemetryStatus ReadSEmeasFromResponse(Packet rsp)
		{
			SETelemetryStatus result;
			if (rsp == null)
			{
				result = null;
			}
			else
			{
				result = new SETelemetryStatus
				{
					Vin = rsp.RetrieveSingle(),
					Vout = rsp.RetrieveSingle(),
					Vamp = rsp.RetrieveSingle(),
					VL1N = rsp.RetrieveSingle(),
					VL2N = rsp.RetrieveSingle(),
					IoutDc = rsp.RetrieveSingle(),
					Fout = rsp.RetrieveSingle(),
					Ircd = rsp.RetrieveSingle(),
					Vref1 = rsp.RetrieveSingle(),
					Vref2 = rsp.RetrieveSingle()
				};
			}
			return result;
		}

		private SETelemetryStatusISE1 ReadIseMeas1FromResponse(Packet rsp)
		{
			SETelemetryStatusISE1 result;
			if (rsp == null)
			{
				result = null;
			}
			else
			{
				result = new SETelemetryStatusISE1
				{
					Vin = rsp.RetrieveSingle(),
					Vout = rsp.RetrieveSingle(),
					VL1N = rsp.RetrieveSingle(),
					VL2N = rsp.RetrieveSingle(),
					Fout = rsp.RetrieveSingle(),
					Iout = rsp.RetrieveSingle(),
					BridgeTemp = rsp.RetrieveSingle(),
					Ircd = rsp.RetrieveSingle(),
					IoutDc = rsp.RetrieveSingle(),
					Pout = rsp.RetrieveSingle(),
					InvMode = (EInvProtInverterMode)rsp.RetrieveUInt16(),
					Reserved = rsp.RetrieveUInt16()
				};
			}
			return result;
		}

		private SETelemetryStatusISE2 ReadIseMeas2FromResponse(Packet rsp)
		{
			SETelemetryStatusISE2 result;
			if (rsp == null)
			{
				result = null;
			}
			else
			{
				SETelemetryStatusISE2 sETelemetryStatusISE = new SETelemetryStatusISE2();
				sETelemetryStatusISE.VfilterRms = rsp.RetrieveSingle();
				sETelemetryStatusISE.AprntPwr = rsp.RetrieveSingle();
				sETelemetryStatusISE.ReactivePwr = rsp.RetrieveSingle();
				sETelemetryStatusISE.PowerFactor = rsp.RetrieveSingle();
				sETelemetryStatusISE.RefVolt1V = rsp.RetrieveSingle();
				sETelemetryStatusISE.RefVolt2V = rsp.RetrieveSingle();
				sETelemetryStatusISE.GndFaultResist = rsp.RetrieveSingle();
				ushort num = rsp.RetrieveUInt16();
				sETelemetryStatusISE.PhaseIout = ((num & 1) == 1);
				sETelemetryStatusISE.PhaseVfilter = ((num & 2) == 2);
				sETelemetryStatusISE.PhaseVout = ((num & 4) == 4);
				sETelemetryStatusISE.Reserved = rsp.RetrieveUInt16();
				result = sETelemetryStatusISE;
			}
			return result;
		}

		private SETelemetryAccumulatedStatus ReadSeAccStatusFromResponse(Packet rsp)
		{
			SETelemetryAccumulatedStatus result;
			if (rsp == null)
			{
				result = null;
			}
			else
			{
				result = new SETelemetryAccumulatedStatus
				{
					Seconds = rsp.RetrieveUInt32(),
					AccWattHour = rsp.RetrieveSingle()
				};
			}
			return result;
		}

		protected override Type GetParamType(ushort index)
		{
			return DBParameterVersion.Instance.GetTypeOfParamVenus(index);
		}

		public void Command_PLC_Transmit(byte[] dataToSend, bool isTransmitOnly)
		{
			if (dataToSend != null)
			{
				List<object> list = new List<object>(0);
				int num = dataToSend.Length;
				list.Add((ushort)num);
				list.Add(dataToSend);
				ushort expectedResponse = isTransmitOnly ? 0 : ProtocolGeneralResponses.PROT_RESP_ACK;
				Packet command = base.BuildCommand(536, list);
				base.Transcieve(command, expectedResponse, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			}
		}

		public uint GetAddress()
		{
			return base.Address;
		}

		public CommManager GetCommManager()
		{
			return base.CommManager;
		}

		public void Command_Venus_StartInverter()
		{
			Packet command = base.BuildCommand(515, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_StartIseDutyCycle(short dutyCycle, float frequency, bool isFixed)
		{
			Packet command = base.BuildCommand(516, new List<object>(0)
			{
				dutyCycle,
				Convert.ToUInt16(isFixed),
				frequency
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_CurrentModemStart()
		{
			Packet command = base.BuildCommand(532, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 8000u, base.DeviceDefaultRetries);
		}

		public void Command_Venus_CurrentModemSend(ushort length, ref ushort[] dataToSend)
		{
			List<object> list = new List<object>(0);
			list.Add(length);
			for (int i = 1; i <= (int)length; i++)
			{
				list.Add(dataToSend[i - 1]);
			}
			Packet command = base.BuildCommand(533, list);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_CurrentModemSendPairing()
		{
			Packet command = base.BuildCommand(534, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_CurrentModemEndPairing()
		{
			Packet command = base.BuildCommand(537, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_StartSupervise()
		{
			Packet command = base.BuildCommand(540, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_PlcRxTestInit()
		{
			Packet command = base.BuildCommand(519, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public SEPLCRxTest Command_Venus_PlcRxTest(float freq0, float freq1, short agcShift, ushort agcLinear, ushort iterNum, int waitTime)
		{
			SEPLCRxTest sEPLCRxTest = null;
			Packet command = base.BuildCommand(520, new List<object>(0)
			{
				freq0,
				freq1,
				agcShift,
				agcLinear,
				iterNum,
				waitTime
			});
			Packet packet = base.Transcieve(command, 645, (uint)(waitTime * (int)iterNum + (int)base.DeviceDefaultTimeout), base.DeviceDefaultRetries);
			if (packet != null)
			{
				sEPLCRxTest = new SEPLCRxTest();
				sEPLCRxTest.G0ResMax = packet.RetrieveInt32();
				sEPLCRxTest.G1ResMax = packet.RetrieveInt32();
				sEPLCRxTest.EnergyOldMax = packet.RetrieveInt32();
				sEPLCRxTest.G0ResMin = packet.RetrieveInt32();
				sEPLCRxTest.G1ResMin = packet.RetrieveInt32();
				sEPLCRxTest.EnergyOldMin = packet.RetrieveInt32();
				sEPLCRxTest.G0ResAvg = packet.RetrieveInt32();
				sEPLCRxTest.G1ResAvg = packet.RetrieveInt32();
				sEPLCRxTest.EnergyOldAvg = packet.RetrieveInt32();
				sEPLCRxTest.Shift = packet.RetrieveInt16();
			}
			return sEPLCRxTest;
		}

		public void Command_Venus_PlcTxTestStart(float freq)
		{
			Packet command = base.BuildCommand(521, new List<object>(0)
			{
				freq
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_PlcTxTestStop()
		{
			Packet command = base.BuildCommand(522, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_TestModeEnableIseWD()
		{
			Packet command = base.BuildCommand(524, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_TestModeDisableIseWD()
		{
			Packet command = base.BuildCommand(525, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public override SEParam Command_Params_Get(ushort index)
		{
			return this.Command_Params_Get((VenusParams)index);
		}

		public SEParam Command_Params_Get(VenusParams param)
		{
			SEParam sEParam;
			if (param == VenusParams.ID)
			{
				sEParam = base.Command_Params_Get_Real((ushort)param);
			}
			else
			{
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.VENUS, base.DeviceSWVersion);
				sEParam = base.Command_Params_Get_Real((ushort)paramRealIndex);
				if (sEParam != null)
				{
					sEParam.Index = (ushort)param;
				}
			}
			return sEParam;
		}

		public override SEParam[] Command_Params_Get(ushort[] indices)
		{
			if (indices == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (indices.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			List<VenusParams> list = new List<VenusParams>(0);
			int num = indices.Length;
			for (int i = 0; i < num; i++)
			{
				list.Add((VenusParams)indices[i]);
			}
			return this.Command_Params_Get(list.ToArray());
		}

		public SEParam[] Command_Params_Get(VenusParams[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			int num = paramList.Length;
			List<ushort> list = new List<ushort>(0);
			for (int i = 0; i < num; i++)
			{
				VenusParams venusParams = paramList[i];
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)venusParams, DeviceType.VENUS, base.DeviceSWVersion);
				list.Add((ushort)paramRealIndex);
			}
			SEParam[] array = base.Command_Params_Get_Real(list.ToArray());
			int num2 = array.Length;
			for (int i = 0; i < num2; i++)
			{
				SEParam sEParam = array[i];
				sEParam.Index = (ushort)paramList[i];
			}
			return array;
		}

		public override string Command_Params_Get_Name(ushort index)
		{
			return this.Command_Params_Get_Name((VenusParams)index);
		}

		public string Command_Params_Get_Name(VenusParams param)
		{
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.VENUS, base.DeviceSWVersion);
			return base.Command_Params_Get_Name_Real((ushort)paramRealIndex);
		}

		public override SEParamInfo Command_Params_Get_Info(ushort index)
		{
			return this.Command_Params_Get_Info((VenusParams)index);
		}

		public SEParamInfo Command_Params_Get_Info(VenusParams param)
		{
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.VENUS, base.DeviceSWVersion);
			return base.Command_Params_Get_Info_Real((ushort)paramRealIndex);
		}

		public override int Command_Params_Get_Count()
		{
			return base.Command_Params_Get_Count_Real();
		}

		public override void Command_Params_Set(SEParam param)
		{
			if (param == null)
			{
				throw new NullReferenceException("No Parameter to send!");
			}
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(param.Index, DeviceType.VENUS, base.DeviceSWVersion);
			if (paramRealIndex > -1)
			{
				this.Command_Device_Stop();
				bool flag = DBParameterVersion.Instance.IsResetRequiredForParam(param.Index, DeviceType.VENUS, base.DeviceSWVersion);
				param.Index = (ushort)paramRealIndex;
				base.Command_Params_Set_Real(param);
				if (flag)
				{
					this.Command_Device_Reset(this.DeviceResetDelay);
					Thread.Sleep((int)(1000u + this.DeviceResetDelay));
				}
			}
		}

		public override void Command_Params_Set(SEParam[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length > 0)
			{
				int num = paramList.Length;
				for (int i = 0; i < num; i++)
				{
					SEParam sEParam = paramList[i];
					if (sEParam != null)
					{
						int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(sEParam.Index, DeviceType.VENUS, base.DeviceSWVersion);
						sEParam.Index = (ushort)paramRealIndex;
					}
				}
				this.Command_Device_Stop();
				base.Command_Params_Set_Real(paramList);
				this.Command_Device_Reset(this.DeviceResetDelay);
				Thread.Sleep((int)(1000u + this.DeviceResetDelay));
			}
		}

		public SETelemetryStatus Command_Venus_GetSEmeas()
		{
			SETelemetryStatus result = null;
			Packet command = base.BuildCommand(514, null);
			Packet packet = base.Transcieve(command, 642, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				result = this.ReadSEmeasFromResponse(packet);
			}
			return result;
		}

		public SEVenusVersion Command_Venus_GetVersions()
		{
			SEVenusVersion sEVenusVersion = null;
			Packet command = base.BuildCommand(51, null);
			Packet packet = base.Transcieve(command, 176, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sEVenusVersion = new SEVenusVersion();
				sEVenusVersion.SeVer = packet.RetrieveUInt32();
				sEVenusVersion.IseVer = packet.RetrieveUInt32();
			}
			return sEVenusVersion;
		}

		public SETelemetryStatusISE1 Command_Venus_GetISEmeas1()
		{
			SETelemetryStatusISE1 result = null;
			Packet command = base.BuildCommand(512, null);
			Packet packet = base.Transcieve(command, 640, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				result = this.ReadIseMeas1FromResponse(packet);
			}
			return result;
		}

		public SETelemetryStatusISE2 Command_Venus_GetISEmeas2()
		{
			SETelemetryStatusISE2 result = null;
			Packet command = base.BuildCommand(513, null);
			Packet packet = base.Transcieve(command, 641, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				result = this.ReadIseMeas2FromResponse(packet);
			}
			return result;
		}

		public SETelemetrySystemStatus Command_Venus_GetSysStatus()
		{
			SETelemetrySystemStatus sETelemetrySystemStatus = null;
			Packet command = base.BuildCommand(517, null);
			Packet packet = base.Transcieve(command, 643, 8000u, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sETelemetrySystemStatus = new SETelemetrySystemStatus();
				sETelemetrySystemStatus.Header.Code = (ETelemCodes)packet.RetrieveUInt16();
				sETelemetrySystemStatus.Header.DataSize = packet.RetrieveUInt16();
				sETelemetrySystemStatus.Header.Id = packet.RetrieveUInt32();
				sETelemetrySystemStatus.Vin = packet.RetrieveSingle();
				sETelemetrySystemStatus.Vout = packet.RetrieveSingle();
				sETelemetrySystemStatus.Power = packet.RetrieveSingle();
				sETelemetrySystemStatus.Freq = packet.RetrieveSingle();
				sETelemetrySystemStatus.Temp = packet.RetrieveSingle();
				sETelemetrySystemStatus.Timer = packet.RetrieveUInt32();
				sETelemetrySystemStatus.InvState = (EInvProcessState)packet.RetrieveUInt16();
				sETelemetrySystemStatus.OnOffSwitch = packet.RetrieveUInt16();
				sETelemetrySystemStatus.LastErr = (EVenusEventCodes)packet.RetrieveInt16();
				sETelemetrySystemStatus.ISEMode = (EInvProtInverterMode)packet.RetrieveInt16();
				sETelemetrySystemStatus.RelayStat = packet.RetrieveInt16();
			}
			return sETelemetrySystemStatus;
		}

		public ETelemCodes Command_Venus_GetTelem(ref SETelemetryAllStatus vTelemAll, ref SETelemetrySystemStatus vTelemSysStat, ref SETelemetrySystemError vTelemSysErr, ref SEPLCTelemetryGeneral plcTelem)
		{
			SETelemetryHeader sETelemetryHeader = new SETelemetryHeader();
			Packet command = base.BuildCommand(518, null);
			Packet packet = base.Transcieve(command, 644, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			ETelemCodes result;
			if (packet == null || packet.Length == 0)
			{
				result = ETelemCodes.NO_TELEM;
			}
			else
			{
				sETelemetryHeader.Code = (ETelemCodes)packet.RetrieveUInt16();
				sETelemetryHeader.DataSize = packet.RetrieveUInt16();
				sETelemetryHeader.Id = packet.RetrieveUInt32();
				vTelemAll.Header.DataSize = 0;
				vTelemSysStat.Header.DataSize = 0;
				vTelemSysErr.Header.DataSize = 0;
				plcTelem.Header.DataSize = 0;
				ETelemCodes code = sETelemetryHeader.Code;
				switch (code)
				{
				case ETelemCodes.VTELEM_ALL_STATUS:
					vTelemAll.Header = sETelemetryHeader;
					vTelemAll.SeStat = this.ReadSEmeasFromResponse(packet);
					vTelemAll.SeAccStatus = this.ReadSeAccStatusFromResponse(packet);
					vTelemAll.IseStat1 = this.ReadIseMeas1FromResponse(packet);
					vTelemAll.IseStat2 = this.ReadIseMeas2FromResponse(packet);
					break;
				case ETelemCodes.VTELEM_SYS_STATUS:
					vTelemSysStat.Header = sETelemetryHeader;
					vTelemSysStat.Vin = packet.RetrieveSingle();
					vTelemSysStat.Vout = packet.RetrieveSingle();
					vTelemSysStat.Power = packet.RetrieveSingle();
					vTelemSysStat.Timer = packet.RetrieveUInt32();
					vTelemSysStat.InvState = (EInvProcessState)packet.RetrieveUInt16();
					vTelemSysStat.OnOffSwitch = packet.RetrieveUInt16();
					break;
				case ETelemCodes.VTELEM_SYS_EVENT:
					vTelemSysErr.Header = sETelemetryHeader;
					vTelemSysErr.TimeStamp = packet.RetrieveUInt32();
					vTelemSysErr.Error = (EVenusEventCodes)packet.RetrieveUInt16();
					break;
				default:
					if (code == ETelemCodes.PLCTELEM_GENERAL)
					{
						plcTelem.Header = sETelemetryHeader;
						plcTelem.Data = packet.RetrieveBytes(13);
					}
					break;
				}
				result = sETelemetryHeader.Code;
			}
			return result;
		}

		public EPrivilegedStates Command_Venus_PrivilegedGetStatus()
		{
			Packet command = base.BuildCommand(531, null);
			Packet packet = base.Transcieve(command, 648, 8000u, base.DeviceDefaultRetries);
			return (EPrivilegedStates)packet.RetrieveUInt16();
		}

		public SECurrentModemStatus Command_Venus_CurrentModemGetStatus()
		{
			SECurrentModemStatus sECurrentModemStatus = null;
			Packet command = base.BuildCommand(535, null);
			Packet packet = base.Transcieve(command, 649, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sECurrentModemStatus = new SECurrentModemStatus();
				sECurrentModemStatus.Progress = packet.RetrieveSingle();
				sECurrentModemStatus.RemainingTime = packet.RetrieveUInt32();
				sECurrentModemStatus.State = (CurrentModemStatus)packet.RetrieveUInt16();
			}
			return sECurrentModemStatus;
		}

		public SETelemetryPrivlidgedEvent Command_Venus_PrivilegedGetEvent()
		{
			SETelemetryPrivlidgedEvent sETelemetryPrivlidgedEvent = null;
			Packet command = base.BuildCommand(530, null);
			Packet packet = base.Transcieve(command, 647, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sETelemetryPrivlidgedEvent.Header = new SETelemetryHeader
				{
					Code = (ETelemCodes)packet.RetrieveUInt16(),
					DataSize = packet.RetrieveUInt16(),
					Id = packet.RetrieveUInt32()
				};
				sETelemetryPrivlidgedEvent.TimeStamp = packet.RetrieveUInt32();
				sETelemetryPrivlidgedEvent.Event = (EVenusEventCodes)packet.RetrieveUInt16();
				sETelemetryPrivlidgedEvent.NumOfEvents = packet.RetrieveUInt16();
			}
			return sETelemetryPrivlidgedEvent;
		}

		public uint Command_Venus_GetCountryCode()
		{
			uint result = 0u;
			Packet command = base.BuildCommand(526, null);
			Packet packet = base.Transcieve(command, 646, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				result = packet.RetrieveUInt32();
			}
			return result;
		}

		public ushort[] Command_Venus_ReadA2DMeas()
		{
			ushort[] array = null;
			Packet command = base.BuildCommand(541, null);
			Packet packet = base.Transcieve(command, 650, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				array = new ushort[16];
				for (int i = 0; i < 16; i++)
				{
					array[i] = packet.RetrieveUInt16();
				}
			}
			return array;
		}

		public EInvModel Command_Venus_GetInverterModel()
		{
			EInvModel result = EInvModel.INV_MODEL_NONE;
			Packet command = base.BuildCommand(544, null);
			Packet packet = base.Transcieve(command, 652, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				result = (EInvModel)packet.RetrieveUInt16();
			}
			return result;
		}

		public uint[] Command_Venus_GetCountryDefaults(uint country, uint paramNum)
		{
			uint[] array = null;
			Packet command = base.BuildCommand(542, new List<object>(0)
			{
				country
			});
			Packet packet = base.Transcieve(command, 651, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				array = new uint[paramNum];
				int num = 0;
				while ((long)num < (long)((ulong)paramNum))
				{
					array[num] = packet.RetrieveUInt32();
					num++;
				}
			}
			return array;
		}

		public uint[] Command_Venus_GetCountryDefaults(uint country)
		{
			uint[] array = null;
			Packet command = base.BuildCommand(542, new List<object>(0)
			{
				country
			});
			Packet packet = base.Transcieve(command, 651, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				int num2;
				int num = Math.DivRem(packet.Length, 4, out num2);
				array = new uint[num];
				for (int i = 0; i < num; i++)
				{
					array[i] = packet.RetrieveUInt32();
				}
			}
			return array;
		}

		public void Command_Venus_PlcTxEnable(bool isEnabled)
		{
			Packet command = base.BuildCommand(523, new List<object>(0)
			{
				isEnabled ? 1 : 0
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_EnablePriviledgedMode()
		{
			Packet command = base.BuildCommand(528, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 8000u, base.DeviceDefaultRetries);
		}

		public void Command_Venus_PriviledgedSetParam(EProtocolDisconnectTestIndx index, ushort val)
		{
			Packet command = base.BuildCommand(529, new List<object>(0)
			{
				(ushort)index,
				val
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_SetCountry(uint countryIndex)
		{
			Packet command = base.BuildCommand(527, new List<object>(0)
			{
				countryIndex
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Venus_SetInverterModel(EInvModel model)
		{
			Packet command = base.BuildCommand(543, new List<object>(0)
			{
				model
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public override SWVersion Command_Device_Version()
		{
			SWVersion sWVersion = new SWVersion();
			SEParam sEParam = base.Command_Params_Get_Real(1);
			SEParam sEParam2 = base.Command_Params_Get_Real(2);
			SEParam sEParam3 = base.Command_Params_Get_Real(3);
			string text = (sEParam != null) ? sEParam.GetParamAsString() : null;
			string text2 = (sEParam2 != null) ? sEParam2.GetParamAsString() : null;
			string text3 = (sEParam3 != null) ? sEParam3.GetParamAsString() : null;
			sWVersion.Major = ((!string.IsNullOrEmpty(text)) ? uint.Parse(text) : 0u);
			sWVersion.Minor = ((!string.IsNullOrEmpty(text2)) ? uint.Parse(text2) : 0u);
			sWVersion.CheckSum = ((!string.IsNullOrEmpty(text3)) ? new uint?(uint.Parse(text3)) : null);
			string venusBuildFromCheckSum = DBParameterVersion.Instance.GetVenusBuildFromCheckSum(uint.Parse(text3));
			if (venusBuildFromCheckSum == null)
			{
				SEParam sEParam4 = base.Command_Params_Get_Real(8);
				string text4 = (sEParam4 != null) ? sEParam4.GetParamAsString() : null;
				if (text4 != null)
				{
					sWVersion.Build = uint.Parse(text4);
				}
			}
			else
			{
				uint build = uint.Parse(venusBuildFromCheckSum.ToString());
				sWVersion.Build = build;
			}
			return sWVersion;
		}

		public SWVersion Command_Device_Version_Pwr()
		{
			SWVersion sWVersion = new SWVersion();
			SEParam sEParam = base.Command_Params_Get_Real(4);
			SEParam sEParam2 = base.Command_Params_Get_Real(5);
			string text = (sEParam != null) ? sEParam.GetParamAsString() : null;
			string text2 = (sEParam2 != null) ? sEParam2.GetParamAsString() : null;
			if (text != null)
			{
				sWVersion.Major = uint.Parse(text);
			}
			if (text2 != null)
			{
				sWVersion.Minor = uint.Parse(text2);
			}
			return sWVersion;
		}

		public override void UpgradeState_VerifyCompliance(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList)
		{
			if (stateData != null && deviceVersionList != null && deviceUpgradeVersionList != null && deviceCompatabilityList != null)
			{
				VenusVersionInfo venusVersionInfo = (VenusVersionInfo)deviceVersionList[0];
				IseVersionInfo iseVersionInfo = (IseVersionInfo)deviceVersionList[1];
				VenusVersionInfo venusVersion = (VenusVersionInfo)deviceUpgradeVersionList[0];
				List<IseCompatibilityItem> list = new List<IseCompatibilityItem>(0);
				int count = deviceCompatabilityList.Count;
				for (int i = 0; i < count; i++)
				{
					list.Add((IseCompatibilityItem)deviceCompatabilityList[i]);
				}
				if (venusVersionInfo.Equals(venusVersion))
				{
					stateData.UpgradeState = UpgradeState.Finished;
				}
				else
				{
					IseCompatibilityItem iseCompatibilityItem = null;
					VenusCompatibilityItem venusCompatibilityItem = null;
					foreach (IseCompatibilityItem current in list)
					{
						foreach (IseVersionInfo current2 in current.IseVersions)
						{
							if (current2.Equals(iseVersionInfo))
							{
								iseCompatibilityItem = current;
								break;
							}
						}
						if (iseCompatibilityItem != null)
						{
							break;
						}
					}
					if (iseCompatibilityItem == null)
					{
						throw new Exception(string.Format("Upgrade file is not compatible to ISE version - ({0})", iseVersionInfo.ToString()));
					}
					foreach (VenusCompatibilityItem current3 in iseCompatibilityItem.VenusCompatabilityList)
					{
						foreach (VenusVersionInfo current4 in current3.VenusVersions)
						{
							if (current4.Equals(venusVersionInfo))
							{
								venusCompatibilityItem = current3;
								break;
							}
						}
						if (venusCompatibilityItem != null)
						{
							break;
						}
					}
					if (venusCompatibilityItem == null)
					{
						throw new Exception("Upgrade file is not compatible to Venus version - (" + venusVersionInfo.ToString() + ")");
					}
					stateData.VenusCompatibiltyItem = venusCompatibilityItem;
					stateData.OriginalVenusVersion = venusVersionInfo;
					stateData.OriginalIseVersion = iseVersionInfo;
					stateData.UpgradeState = UpgradeState.SaveParamaeters;
				}
			}
		}

		public override void UpgradeState_SaveParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, uint? sufVersion)
		{
			if (stateData != null && deviceVersionList != null)
			{
				VenusVersionInfo venusVersionInfo = (VenusVersionInfo)deviceVersionList[0];
				IseVersionInfo iseVersionInfo = (IseVersionInfo)deviceVersionList[1];
				if (!venusVersionInfo.Equals(stateData.OriginalVenusVersion) || !iseVersionInfo.Equals(stateData.OriginalIseVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				stateData.SavedParameters = new List<ParamaeterPair>();
				foreach (int current in stateData.VenusCompatibiltyItem.RestoreParams)
				{
					int num = current;
					if (!sufVersion.HasValue)
					{
						ushort? paramVirtualIndex = DBParameterVersion.Instance.GetParamVirtualIndex(num, DeviceType.VENUS, base.DeviceSWVersion);
						ushort? num2 = paramVirtualIndex;
						if ((num2.HasValue ? new int?((int)num2.GetValueOrDefault()) : null).HasValue)
						{
							num = (int)paramVirtualIndex.Value;
						}
					}
					SEParam sEParam = this.Command_Params_Get((VenusParams)num);
					string paramAsString = sEParam.GetParamAsString();
					ParamaeterPair paramaeterPair = new ParamaeterPair();
					paramaeterPair.Index = num;
					paramaeterPair.Value = paramAsString;
					stateData.SavedParameters.Add(paramaeterPair);
				}
				stateData.UpgradeState = UpgradeState.UpgradeSoftware;
			}
		}

		public override void UpgradeState_UpgradeSoftwareStatus(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData)
		{
			if (stateData != null && upgradeXmlData != null)
			{
				this.Command_Device_Stop();
				uint checksum = 0u;
				if (upgradeXmlData.UpgradeData.DataChecksum.HasValue)
				{
					checksum = upgradeXmlData.UpgradeData.DataChecksum.Value;
				}
				base.DoUpgrade(upgradeXmlData.UpgradeData.Data, upgradeXmlData.UpgradeData.Data.Length, 0u, checksum, 0, base.UpgradeNotifier);
				stateData.UpgradeState = UpgradeState.VerifyVersionAfterUpgrade;
			}
		}

		public override void UpgradeState_VerifyVersionAfterUpgrade(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraCommDevice)
		{
			if (stateData != null && deviceVersionList != null)
			{
				VenusVersionInfo venusVersionInfo = (VenusVersionInfo)deviceVersionList[0];
				VenusVersionInfo venusVersion = (VenusVersionInfo)deviceVersionList[1];
				SEPortia sEPortia = (SEPortia)extraCommDevice;
				sEPortia.Command_Device_Reset(500u);
				int deviceDefaultRetries = (int)base.DeviceDefaultRetries;
				base.DeviceDefaultRetries = 50u;
				this._deviceSWVersion = this.Command_Device_Version();
				this._deviceSWVersionPwr = this.Command_Device_Version_Pwr();
				SWVersion deviceSWVersion = base.DeviceSWVersion;
				venusVersionInfo.MajorVersion = deviceSWVersion.Major;
				venusVersionInfo.MinorVersion = deviceSWVersion.Minor;
				venusVersionInfo.BuildVersion = new uint?(deviceSWVersion.Build);
				venusVersionInfo.Checksum = deviceSWVersion.CheckSum.Value;
				if (!venusVersionInfo.Equals(venusVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					base.DeviceDefaultRetries = (uint)deviceDefaultRetries;
					stateData.UpgradeState = UpgradeState.ConfigureParamaeters;
				}
			}
		}

		public override void UpgradeState_ConfigureParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList)
		{
			if (stateData != null && deviceVersionList != null)
			{
				VenusVersionInfo venusVersionInfo = (VenusVersionInfo)deviceVersionList[0];
				VenusVersionInfo venusVersion = (VenusVersionInfo)deviceVersionList[1];
				if (!venusVersionInfo.Equals(venusVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					this.Command_Device_Stop();
					if (stateData.VenusCompatibiltyItem.SetCountry.HasValue)
					{
						if (stateData.VenusCompatibiltyItem.SetCountry.Value)
						{
							List<SEParam> list = new List<SEParam>(0);
							foreach (ParamaeterPair current in stateData.SavedParameters)
							{
								if (current.Index == 10)
								{
									SEParam param = new SEParam((ushort)current.Index, uint.Parse(current.Value));
									this.Command_Params_Set(param);
									break;
								}
							}
							uint countryIndex = 0u;
							bool flag = false;
							foreach (ParamaeterPair current in stateData.SavedParameters)
							{
								if (current.Index == 7)
								{
									countryIndex = Convert.ToUInt32(current.Value);
									flag = true;
									break;
								}
							}
							if (flag)
							{
								this.Command_Venus_SetCountry(countryIndex);
							}
							else
							{
								SEParam sEParam = this.Command_Params_Get(VenusParams.INV_COUNTRY_CODE);
								uint valueUInt = sEParam.ValueUInt32;
								this.Command_Venus_SetCountry(countryIndex);
							}
						}
					}
					List<SEParam> list2 = new List<SEParam>(0);
					foreach (ParamaeterPair current in stateData.SavedParameters)
					{
						ushort num = (ushort)current.Index;
						Type typeOfParamVenus = DBParameterVersion.Instance.GetTypeOfParamVenus(num);
						ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamVenus);
						SEParam item = null;
						switch (paramTypeByType)
						{
						case ParamType.UINT32:
							item = new SEParam(num, uint.Parse(current.Value));
							break;
						case ParamType.FLOAT:
							item = new SEParam(num, float.Parse(current.Value));
							break;
						case ParamType.INT:
							item = new SEParam(num, int.Parse(current.Value));
							break;
						case ParamType.STRING:
							item = new SEParam(num, current.Value);
							break;
						}
						list2.Add(item);
					}
					if (stateData.VenusCompatibiltyItem != null && stateData.VenusCompatibiltyItem.ParamatersToSet != null)
					{
						foreach (ParamaeterPair current in stateData.VenusCompatibiltyItem.ParamatersToSet)
						{
							ushort num = (ushort)current.Index;
							Type typeOfParamVenus = DBParameterVersion.Instance.GetTypeOfParamVenus(num);
							ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamVenus);
							SEParam item = null;
							switch (paramTypeByType)
							{
							case ParamType.UINT32:
								item = new SEParam(num, uint.Parse(current.Value));
								break;
							case ParamType.FLOAT:
								item = new SEParam(num, float.Parse(current.Value));
								break;
							case ParamType.INT:
								item = new SEParam(num, int.Parse(current.Value));
								break;
							case ParamType.STRING:
								item = new SEParam(num, current.Value);
								break;
							}
							list2.Add(item);
						}
					}
					this.Command_Params_Set(list2.ToArray());
					stateData.UpgradeState = UpgradeState.Finished;
				}
			}
		}

		public override void UpgradeState_Finished(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion)
		{
			VenusVersionInfo venusVersionInfo = (VenusVersionInfo)deviceVersion;
			VenusVersionInfo venusVersion = (VenusVersionInfo)upgradeVersion;
			if (!venusVersionInfo.Equals(venusVersion))
			{
				stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
		}
	}
}
