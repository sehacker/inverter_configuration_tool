using System;

namespace SEDevices.Devices
{
	[Flags]
	public enum enVenusConfigurationAttribute
	{
		NONE = 0,
		RCD = 1,
		ISOLATION_TEST = 2,
		ANTI_ISLANDING = 4,
		NEUTRAL_POINT = 8,
		CHECK_VOUT_MAX2 = 16,
		GRID_FREQUENCY = 32,
		RELAY_TEST = 64,
		ISOLATION_INTERNAL_TEST = 128,
		RCD_TEST = 256
	}
}
