using System;

namespace SEDevices.Devices
{
	public enum Zones
	{
		None,
		North_America,
		Europe,
		APAC = 4,
		All = 8
	}
}
