using SEDevices.Devices.Base;
using SEDevices.Records.Parameter;
using SEDevices.Upgrade.Base;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading;

namespace SEDevices.Devices
{
	public abstract class SECommDevice : ISECommCommandable, ISECommUpgradable
	{
		private const string PARAMETER_TABLE_NAME = "PARAMETER_TABLE_NAME";

		protected const int SLEEP_TIME_AFTER_RESET_DEFAULT = 1000;

		protected const string UPGRADE_EXCEPTION_DATA = "BytesCompleted";

		protected uint _baseAddress;

		protected uint _address;

		protected SWVersion _deviceSWVersion;

		private object _owner = null;

		private ISEUpgradeNotifiable _upgradeNotifier = null;

		private ushort _runningID = 0;

		private int _lastCommandSendTimeInMiliSec = 0;

		private CommManager _commManager;

		private uint _sourceID = ProtocolMain.PROT_CONFTOOL_ADDR;

		private uint _deviceDefaultTimeout = 6000u;

		private uint _deviceDefaultRetries = 3u;

		private DataTable _parameterTable = null;

		private Dictionary<int, DataRow> _parameterTableExistData = null;

		public uint Address
		{
			get
			{
				uint address;
				lock (this)
				{
					address = this._address;
				}
				return address;
			}
			set
			{
				lock (this)
				{
					this._address = value;
				}
			}
		}

		public SWVersion DeviceSWVersion
		{
			get
			{
				return this._deviceSWVersion;
			}
		}

		public object Owner
		{
			get
			{
				return this._owner;
			}
			set
			{
				this._owner = value;
			}
		}

		public ISEUpgradeNotifiable UpgradeNotifier
		{
			get
			{
				return this._upgradeNotifier;
			}
			set
			{
				this._upgradeNotifier = value;
			}
		}

		public int LastCommandSendTime
		{
			get
			{
				return this._lastCommandSendTimeInMiliSec;
			}
		}

		public int LastCommandSendInterval
		{
			get
			{
				return Environment.TickCount - this._lastCommandSendTimeInMiliSec;
			}
		}

		public CommManager CommManager
		{
			get
			{
				return this._commManager;
			}
		}

		public bool? ConnectedRemotely
		{
			get
			{
				return this._commManager.ConnectedRemotely;
			}
		}

		public abstract uint DeviceMaxDataLength
		{
			get;
		}

		public abstract uint DeviceResetDelay
		{
			get;
		}

		public uint DeviceDefaultTimeout
		{
			get
			{
				return this._deviceDefaultTimeout;
			}
			set
			{
				this._deviceDefaultTimeout = value;
			}
		}

		public uint DeviceDefaultRetries
		{
			get
			{
				return this._deviceDefaultRetries;
			}
			set
			{
				this._deviceDefaultRetries = value;
			}
		}

		public DataTable Parameters
		{
			get
			{
				return this._parameterTable;
			}
		}

		public Dictionary<int, DataRow> ParameterTableData
		{
			get
			{
				return this._parameterTableExistData;
			}
		}

		public SECommDevice(CommManager commManager, object owner, SWVersion version)
		{
			this._owner = owner;
			this._deviceSWVersion = version;
			this.SetBroadcast();
			this._baseAddress = this._address;
			this._commManager = commManager;
			this.BuildParametersTable();
		}

		public void Dispose()
		{
			this._commManager = null;
		}

		public virtual void SetBroadcast()
		{
			this._address = ProtocolMain.PROT_BROADCAST_ADDR;
		}

		public virtual void SetSinglecast()
		{
			this._address = ProtocolMain.PROT_SINGLECAST_ADDR;
		}

		public void SetOriginalAddress()
		{
			this._address = this._baseAddress;
		}

		private void ClearParametersTable()
		{
			if (this._parameterTable != null)
			{
				this._parameterTable.Rows.Clear();
				this._parameterTable.Columns.Clear();
				this._parameterTable.Dispose();
				this._parameterTable = null;
			}
		}

		private void BuildParametersTable()
		{
			this.ClearParametersTable();
			this._parameterTable = new DataTable();
			this._parameterTable.TableName = "PARAMETER_TABLE_NAME";
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_INDEX.ToString(), typeof(int)));
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_NAME.ToString(), typeof(string)));
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_TYPE.ToString(), typeof(int)));
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_VALUE.ToString(), typeof(object)));
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_DEFAULT.ToString(), typeof(object)));
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_MIN_VAL.ToString(), typeof(object)));
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_MAX_VAL.ToString(), typeof(object)));
			this._parameterTable.Columns.Add(new DataColumn(ParameterColumns.COL_PARAM_ACCESS_LEVEL.ToString(), typeof(int)));
			this._parameterTableExistData = new Dictionary<int, DataRow>(0);
		}

		public void ClearTableDataAll()
		{
			if (this._parameterTable != null)
			{
				this._parameterTable = null;
			}
			if (this._parameterTableExistData != null)
			{
				this._parameterTableExistData.Clear();
				this._parameterTableExistData = null;
			}
		}

		public void ClearTableData(int virtualIndex)
		{
			if (this._parameterTableExistData != null)
			{
				this.ClearTableData(new int[]
				{
					virtualIndex
				});
			}
		}

		public void ClearTableData(int[] virtualIndices)
		{
			if (this._parameterTableExistData != null)
			{
				int num = virtualIndices.Length;
				for (int i = 0; i < num; i++)
				{
					int key = virtualIndices[i];
					if (this._parameterTableExistData.ContainsKey(key))
					{
						this._parameterTableExistData.Remove(key);
					}
				}
			}
		}

		public SEParam GetParamFromTable(int virtualIndex)
		{
			SEParam sEParam = null;
			SEParam result;
			if (this._parameterTableExistData != null && this._parameterTableExistData.ContainsKey(virtualIndex))
			{
				try
				{
					DataRow dataRow = this._parameterTableExistData[virtualIndex];
					if (dataRow == null)
					{
						result = sEParam;
						return result;
					}
					int num = (int)short.Parse(dataRow.ItemArray[2].ToString());
					string text = dataRow.ItemArray[3].ToString();
					switch (num)
					{
					case 0:
						sEParam = new SEParam((ushort)virtualIndex, uint.Parse(text));
						break;
					case 1:
					{
						float value = float.Parse(text);
						sEParam = new SEParam((ushort)virtualIndex, value);
						break;
					}
					case 2:
					{
						int value2 = int.Parse(text);
						sEParam = new SEParam((ushort)virtualIndex, value2);
						break;
					}
					case 3:
						sEParam = new SEParam((ushort)virtualIndex, text);
						break;
					}
				}
				catch (Exception var_6_D1)
				{
				}
			}
			result = sEParam;
			return result;
		}

		public DataRow GetDataRowFromTable(int virtualIndex)
		{
			DataRow result = null;
			if (this._parameterTableExistData != null && this._parameterTableExistData.Count > 0 && this._parameterTableExistData.ContainsKey(virtualIndex))
			{
				result = this._parameterTableExistData[virtualIndex];
			}
			return result;
		}

		public bool IsParamExist(int virtualIndex)
		{
			return this._parameterTableExistData != null && this._parameterTableExistData.ContainsKey(virtualIndex);
		}

		public void AddParameterToDataTable(int index, string name, int type, object value, object defaultVal, object minVal, object maxVal, int access)
		{
			if (this.Parameters == null)
			{
				this.BuildParametersTable();
			}
			DataRow value2 = this.Parameters.Rows.Add(new object[]
			{
				index,
				name,
				type,
				value,
				defaultVal,
				minVal,
				maxVal,
				access
			});
			this._parameterTableExistData.Add(index, value2);
		}

		public void SetParameterToTable(int index, object value)
		{
			if (this.Parameters != null)
			{
				DataRow dataRow = this._parameterTableExistData[index];
				if (dataRow != null)
				{
					dataRow[3] = value;
				}
			}
		}

		protected virtual Type GetParamType(ushort index)
		{
			return null;
		}

		public virtual SEParam Command_Params_Get(ushort index)
		{
			return null;
		}

		public virtual SEParam[] Command_Params_Get(ushort[] indices)
		{
			return null;
		}

		public virtual int Command_Params_Get_Count()
		{
			return -1;
		}

		public virtual string Command_Params_Get_Name(ushort index)
		{
			return null;
		}

		public virtual SEParamInfo Command_Params_Get_Info(ushort index)
		{
			return null;
		}

		public virtual void Command_Params_Set(SEParam param)
		{
		}

		public virtual void Command_Params_Set(SEParam[] paramList)
		{
		}

		public virtual void Command_Params_Reset()
		{
			Packet command = this.BuildCommand(16, null);
			this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
		}

		public virtual void Command_Params_Save()
		{
			Packet command = this.BuildCommand(25, null);
			this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
		}

		public virtual void Command_Device_Reset()
		{
			Packet command = this.BuildCommand(48, null);
			this.Transmit(command, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
		}

		public virtual void Command_Device_Reset(uint delay)
		{
			Packet command = this.BuildCommand(48, new List<object>(0)
			{
				delay
			});
			if (delay > 5u)
			{
				this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
				Thread.Sleep((int)delay);
			}
			else
			{
				this.Transmit(command, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			}
		}

		public virtual void Command_Device_Stop()
		{
			Packet command = this.BuildCommand(49, null);
			this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
		}

		public virtual SWVersion Command_Device_Version()
		{
			return null;
		}

		public void SetDefaultTimeout(uint defaultTimeout)
		{
			this._deviceDefaultTimeout = defaultTimeout;
		}

		public uint GetDefaultTimeout()
		{
			return this._deviceDefaultTimeout;
		}

		public void SetDefaultRetries(uint defaultRetries)
		{
			this._deviceDefaultRetries = defaultRetries;
		}

		public uint GetDefaultRetries()
		{
			return this._deviceDefaultRetries;
		}

		public void SetNextID(ushort nextID)
		{
			this._runningID = nextID;
		}

		public ushort GetNextID()
		{
			ushort runningID;
			this._runningID = (runningID = this._runningID) + 1;
			return runningID;
		}

		public void SetSourceID(uint sourceID)
		{
			this._sourceID = sourceID;
		}

		public uint GetSourceID()
		{
			return this._sourceID;
		}

		protected void DoUpgrade(byte[] progData, int progBytes, uint progOffset, uint checksum, ushort bank, ISEUpgradeNotifiable notifier)
		{
			this.DoUpgrade(0, progData, progBytes, progOffset, checksum, bank, notifier);
		}

		protected void DoUpgrade(int startOffset, byte[] progData, int progBytes, uint progOffset, uint checksum, ushort bank, ISEUpgradeNotifiable notifier)
		{
			int num = startOffset;
			try
			{
				if (startOffset == 0)
				{
					this.Command_Upgrade_Start(bank);
				}
				DateTime now = DateTime.Now;
				string str = "Copy: started";
				string text = string.Format("[{0:00}:{1:00}:{2:00}] " + str, now.Hour, now.Minute, now.Second);
				int progress = 50;
				notifier.UpdateState(text, progress);
				int i = startOffset;
				while (i < progBytes)
				{
					int num2 = (int)Math.Min((long)(progBytes - i), (long)((ulong)(this.DeviceMaxDataLength - 8u)));
					this.Command_Upgrade_Write(progData, i, (uint)((ulong)progOffset + (ulong)((long)i)), num2);
					i += num2;
					num = i;
					double num3 = (double)i / (double)progBytes;
					int progress2 = (int)(num3 * 100.0);
					notifier.UpdateWritingData(progress2);
				}
				now = DateTime.Now;
				str = "Copy: completed";
				text = string.Format("[{0:00}:{1:00}:{2:00}] " + str, now.Hour, now.Minute, now.Second);
				progress = 60;
				notifier.UpdateState(text, progress);
				this.Command_Upgrade_Finish(checksum);
				now = DateTime.Now;
				str = "Copy: verified";
				text = string.Format("[{0:00}:{1:00}:{2:00}] " + str, now.Hour, now.Minute, now.Second);
				progress = 70;
				notifier.UpdateState(text, progress);
			}
			catch (Exception ex)
			{
				ex.Data.Add("BytesCompleted", num);
				throw ex;
			}
		}

		protected void FullUpgrade(string fileName, int bank, SECommDevice device, ISEUpgradeNotifiable notifier)
		{
			if (bank == -1)
			{
				int progBytes;
				uint checksum;
				byte[] progData = this.UpgradeReadRawFile(fileName, out progBytes, out checksum);
				this.DoUpgrade(progData, progBytes, 0u, checksum, 0, notifier);
			}
			else
			{
				int progBytes;
				byte[] progData = this.UpgradeReadHexFile(fileName, out progBytes);
				this.DoUpgrade(progData, progBytes, (uint)(bank * 65536), 0u, (ushort)((byte)bank), notifier);
			}
		}

		private byte[] UpgradeReadRawFile(string fileName, out int progBytes, out uint progChecksum)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			BinaryReader binaryReader = new BinaryReader(fileStream);
			byte[] array = binaryReader.ReadBytes((int)fileStream.Length);
			progBytes = array.Length - 4;
			if (progBytes <= 0)
			{
				throw new Exception("Upgrade file size is illegal");
			}
			uint num = 0u;
			for (int i = 0; i < array.Length - 4; i += 2)
			{
				num += (uint)((int)array[i] | (int)array[i + 1] << 8);
			}
			fileStream.Position -= 4L;
			uint num2 = binaryReader.ReadUInt32();
			fileStream.Close();
			if (num2 != num)
			{
				throw new Exception("Upgrade file checksum is incorrect");
			}
			progChecksum = num;
			return array;
		}

		private byte[] UpgradeReadHexFile(string fileName, out int progBytes)
		{
			byte[] array = new byte[2097152];
			StreamReader streamReader = new StreamReader(fileName);
			progBytes = 0;
			string value;
			while ((value = streamReader.ReadLine()) != null)
			{
				array[progBytes++] = Convert.ToByte(value, 16);
			}
			return array;
		}

		public virtual void UpgradeState_VerifyCompliance(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList)
		{
		}

		public virtual void UpgradeState_SaveParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, uint? sufVersion)
		{
		}

		public virtual void UpgradeState_UpgradeSoftwareStatus(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData)
		{
		}

		public virtual void UpgradeState_VerifyVersionAfterUpgrade(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraCommDevice)
		{
		}

		public virtual void UpgradeState_ConfigureParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList)
		{
		}

		public virtual void UpgradeState_Finished(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion)
		{
		}

		public virtual void Command_Upgrade_Start(ushort bank)
		{
			Packet command = this.BuildCommand(32, new List<object>(0)
			{
				bank
			});
			this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, 10000u, 3u);
		}

		public virtual void Command_Upgrade_Write(byte[] data, int dataOffset, uint offset, int size)
		{
			List<object> list = new List<object>(0);
			list.Add(offset);
			list.Add((uint)size);
			byte[] array = new byte[size];
			Array.Copy(data, dataOffset, array, 0, size);
			list.Add(array);
			Packet command = this.BuildCommand(33, list);
			this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
		}

		public virtual byte[] Command_Upgrade_Read_Data(int offset, int size)
		{
			byte[] array = null;
			Packet command = this.BuildCommand(35, new List<object>(0)
			{
				offset,
				size
			});
			Packet packet = this.Transcieve(command, 160, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			if (packet != null)
			{
				array = new byte[size];
				Array.Copy(packet.Data, array, size);
			}
			return array;
		}

		public virtual uint Command_Upgrade_Read_Size()
		{
			uint result = 0u;
			Packet command = this.BuildCommand(36, null);
			Packet packet = this.Transcieve(command, 161, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			if (packet != null)
			{
				result = packet.RetrieveUInt32();
			}
			return result;
		}

		public virtual void Command_Upgrade_Finish(uint checksum)
		{
			Packet command = this.BuildCommand(34, new List<object>(0)
			{
				checksum
			});
			this.Transmit(command, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
		}

		protected SEParam Command_Params_Get_Real(ushort realIndex)
		{
			Packet command = this.BuildCommand(18, new List<object>(0)
			{
				realIndex
			});
			Packet packet = this.Transcieve(command, ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			SEParam result = null;
			if (packet != null)
			{
				uint num = packet.RetrieveUInt32();
				short num2 = packet.RetrieveInt16();
				string value = (packet.Length > 6) ? packet.RetrieveString() : null;
				switch (num2)
				{
				case 0:
					result = new SEParam(realIndex, num);
					break;
				case 1:
					result = new SEParam(realIndex, Utils.UInt32AsFloat(num));
					break;
				case 2:
					result = new SEParam(realIndex, Utils.UInt32AsInt32(num));
					break;
				case 3:
					result = new SEParam(realIndex, value);
					break;
				}
			}
			return result;
		}

		protected SEParam[] Command_Params_Get_Real(ushort[] realIndices)
		{
			if (realIndices == null)
			{
				throw new NullReferenceException("Parameter List to get is null!");
			}
			if (realIndices.Length <= 0)
			{
				throw new Exception("Parameter List to get is empty!");
			}
			List<SEParam> list = new List<SEParam>(0);
			int i = 0;
			int num = realIndices.Length;
			bool flag = false;
			while (i < num)
			{
				List<object> list2 = new List<object>(0);
				List<Type> list3 = new List<Type>(0);
				uint num2 = 0u;
				if (!flag)
				{
					for (int j = i; j < num; j++)
					{
						flag = (j == num - 1);
						ushort num3 = realIndices[j];
						Type type = this.GetParamType(num3);
						if (type != null)
						{
							num2 += 2u;
							num2 += 6u;
							if (type.Equals(typeof(string)))
							{
								num2 += Packet.LENGTH_DATA_STRING_VALUE_MAX;
								num2 += 1u;
							}
							list2.Add(num3);
							list3.Add(type);
							if (num2 >= this.DeviceMaxDataLength)
							{
								break;
							}
						}
					}
				}
				if (flag && num2 == 0u)
				{
					break;
				}
				i += list2.Count;
				Packet command = this.BuildCommand(18, list2);
				Packet packet = this.Transcieve(command, ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
				if (packet != null)
				{
					int count = list2.Count;
					for (int j = 0; j < count; j++)
					{
						SEParam item = null;
						try
						{
							ushort num3 = (ushort)list2[j];
							Type type = list3[j];
							uint num4 = packet.RetrieveUInt32();
							short num5 = packet.RetrieveInt16();
							string value = type.Equals(typeof(string)) ? packet.RetrieveString() : null;
							switch (num5)
							{
							case 0:
								item = new SEParam(num3, num4);
								break;
							case 1:
								item = new SEParam(num3, Utils.UInt32AsFloat(num4));
								break;
							case 2:
								item = new SEParam(num3, Utils.UInt32AsInt32(num4));
								break;
							case 3:
								item = new SEParam(num3, value);
								break;
							}
						}
						catch (Exception var_17_223)
						{
						}
						list.Add(item);
					}
				}
			}
			return list.ToArray();
		}

		protected int Command_Params_Get_Count_Real()
		{
			Packet command = this.BuildCommand(21, null);
			Packet packet = this.Transcieve(command, 147, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			int result = 0;
			if (packet != null)
			{
				result = (int)packet.RetrieveUInt16();
			}
			return result;
		}

		protected string Command_Params_Get_Name_Real(ushort realIndex)
		{
			Packet command = this.BuildCommand(20, new List<object>(0)
			{
				realIndex
			});
			Packet packet = this.Transcieve(command, 146, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			string result = "";
			if (packet != null)
			{
				result = packet.RetrieveString();
			}
			return result;
		}

		protected SEParamInfo Command_Params_Get_Info_Real(ushort realIndex)
		{
			Packet command = this.BuildCommand(19, new List<object>(0)
			{
				realIndex
			});
			Packet packet = this.Transcieve(command, 145, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			short paramType = packet.RetrieveInt16();
			SEParamInfo result;
			switch (paramType)
			{
			case 1:
				result = new SEParamInfo(realIndex, null, paramType, packet.RetrieveSingle(), packet.RetrieveSingle(), packet.RetrieveSingle());
				return result;
			}
			result = new SEParamInfo(realIndex, null, paramType, packet.RetrieveInt32(), packet.RetrieveInt32(), packet.RetrieveInt32());
			return result;
		}

		protected void Command_Params_Set_Real(SEParam param)
		{
			if (param == null)
			{
				throw new NullReferenceException("Parameter data to set is null!");
			}
			List<object> list = new List<object>(0);
			list.Add(param.Index);
			list.Add(param.ValueUInt32);
			if (param.ValueString != null)
			{
				list.Add(param.ValueString);
			}
			Packet command = this.BuildCommand(17, list);
			this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
		}

		protected void Command_Params_Set_Real(SEParam[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			List<object> list = null;
			int i = -1;
			int num = paramList.Length - 1;
			int num2 = -1;
			while (i < num)
			{
				uint num3 = 0u;
				uint deviceMaxDataLength = this.DeviceMaxDataLength;
				if (list == null)
				{
					list = new List<object>(0);
				}
				else
				{
					list.Clear();
				}
				while (num3 < deviceMaxDataLength && i < num)
				{
					SEParam sEParam = paramList[++num2];
					int num4 = 6;
					ParamType parameterType = (ParamType)sEParam.ParameterType;
					if (parameterType == ParamType.STRING)
					{
						num4 += sEParam.ValueString.Length + 1;
					}
					if ((ulong)num3 + (ulong)((long)num4) >= (ulong)deviceMaxDataLength)
					{
						num2--;
						break;
					}
					list.Add(sEParam.Index);
					num3 += 2u;
					switch (sEParam.ParameterType)
					{
					case 0:
						list.Add(sEParam.ValueUInt32);
						break;
					case 1:
						list.Add(sEParam.ValueFloat);
						break;
					case 2:
						list.Add(sEParam.ValueInt32);
						break;
					case 3:
						list.Add(sEParam.ValueUInt32);
						break;
					}
					num3 += 4u;
					string valueString = sEParam.ValueString;
					if (valueString != null)
					{
						list.Add(valueString);
						num3 += (uint)valueString.Length;
						num3 += 1u;
					}
					i++;
				}
				Packet command = this.BuildCommand(17, list);
				this.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, this.DeviceDefaultTimeout, this.DeviceDefaultRetries);
			}
		}

		protected Packet BuildCommand(ushort opCode, List<object> data)
		{
			Packet newPacket = Packet.GetNewPacket(PacketVersion.B, this.DeviceMaxDataLength);
			newPacket.OpCode = opCode;
			newPacket.Source = this.GetSourceID();
			newPacket.Destination = this.Address;
			newPacket.ID = this.GetNextID();
			if (data != null)
			{
				int count = data.Count;
				for (int i = 0; i < count; i++)
				{
					newPacket.Append(data[i]);
				}
				newPacket.FlushData();
			}
			return newPacket;
		}

		protected Packet Transcieve(Packet command, ushort expectedResponse, uint timeout, uint retries)
		{
			command.ExpectedOpCode = expectedResponse;
			Packet packet = null;
			bool flag = command.Destination == ProtocolMain.PROT_BROADCAST_ADDR || expectedResponse == 0;
			int num = 0;
			while ((long)num < (long)((ulong)retries))
			{
				packet = this._commManager.SendReceive(command, timeout, flag);
				this._lastCommandSendTimeInMiliSec = Environment.TickCount;
				if (packet != null || flag)
				{
					break;
				}
				num++;
			}
			if (packet == null && !flag)
			{
				throw new TimeoutException("No response was recieved");
			}
			if (packet != null && packet.OpCode == ProtocolGeneralResponses.PROT_RESP_NACK)
			{
				packet = null;
			}
			return packet;
		}

		protected void Transmit(Packet command, uint timeOut, uint retries)
		{
			if (this._commManager != null)
			{
				int num = 0;
				while ((long)num < (long)((ulong)retries))
				{
					this._commManager.SendReceive(command, timeOut, true);
					this._lastCommandSendTimeInMiliSec = Environment.TickCount;
					num++;
				}
			}
		}
	}
}
