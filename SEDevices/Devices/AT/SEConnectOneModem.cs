using SEProtocol.AT;
using SEProtocol.AT.Base;
using SEProtocol.AT.ConnectOne;
using SEProtocol.AT.ConnectOne.Data;
using System;

namespace SEDevices.Devices.AT
{
	public class SEConnectOneModem
	{
		private ATManager _atManager;

		public ATManager ATManager
		{
			get
			{
				return this._atManager;
			}
		}

		public SEConnectOneModem(ATManager atManager)
		{
			this._atManager = atManager;
		}

		public void Dispose()
		{
			this._atManager = null;
		}

		private int GetError(string result)
		{
			int result2 = -1;
			if (!string.IsNullOrEmpty(result) && result.StartsWith("I/ERROR"))
			{
				try
				{
					int num = result.IndexOf("(");
					int num2 = result.IndexOf(")");
					string s = result.Substring(num + 1, num2 - num);
					result2 = int.Parse(s);
				}
				catch (Exception var_4_53)
				{
					throw new ATException("Could not parse error");
				}
			}
			return result2;
		}

		protected string[] Transcieve(ATPacket command)
		{
			return this._atManager.SendReceive(command);
		}

		protected string[] Transcieve(ATPacket command, int? timeOutInMiliSec)
		{
			return this._atManager.SendReceive(command, timeOutInMiliSec);
		}

		public string Command_BD_GetStatus(StatusReport report)
		{
			string result = null;
			Commands arg_1C_0 = Commands.GET_GENERAL_STATUS;
			string[] array = new string[1];
			string[] arg_19_0 = array;
			int arg_19_1 = 0;
			int num = (int)report;
			arg_19_0[arg_19_1] = num.ToString();
			ConnectOnePacket command = new ConnectOnePacket(arg_1C_0, array);
			string[] array2 = this.Transcieve(command);
			if (array2 != null && array2.Length == 2)
			{
				string text = array2[0];
				string text2 = array2[1];
				if (text2.Equals(ConnectOneProtocol.RESPONSES[0]))
				{
					result = text;
				}
			}
			return result;
		}

		public bool Command_BD_RestoreFactoryDefault()
		{
			ConnectOnePacket command = new ConnectOnePacket(Commands.RESTORE_FACTORY_SETTINGS, new string[0]);
			string[] array = this.Transcieve(command);
			return array != null && array.Length == 1 && array[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public bool Command_LL_SetSerialNumber(string snToSetInHex)
		{
			if (snToSetInHex.Length != 0)
			{
				throw new ATException("Value must be an 8 hexadecimal characters");
			}
			ConnectOnePacket command = new ConnectOnePacket(Commands.SET_iChip_SERIAL_NUMBER, new string[]
			{
				snToSetInHex
			});
			string[] array = this.Transcieve(command);
			bool result = false;
			if (array != null && array.Length == 1)
			{
				string text = array[0];
				if (text.Equals(ConnectOneProtocol.RESPONSES[0]))
				{
					result = true;
				}
				else
				{
					ConnectOneProtocol.HandleError(this.GetError(text));
					result = false;
				}
			}
			return result;
		}

		public string Command_LL_GetSerialNumber()
		{
			return this.Command_BD_GetStatus(StatusReport.UNIQUE_SERIAL_NUMBER);
		}

		public bool Command_LL_SetUniqueID(string uidToSetInHex)
		{
			if (uidToSetInHex.Length != 0)
			{
				throw new ATException("Value must be an 8 hexadecimal characters");
			}
			ConnectOnePacket command = new ConnectOnePacket(Commands.SET_iChip_UNIQUE_ID, new string[]
			{
				uidToSetInHex
			});
			string[] array = this.Transcieve(command);
			bool result = false;
			if (array != null && array.Length == 1)
			{
				string text = array[0];
				if (text.Equals(ConnectOneProtocol.RESPONSES[0]))
				{
					result = true;
				}
				else
				{
					ConnectOneProtocol.HandleError(this.GetError(text));
					result = false;
				}
			}
			return result;
		}

		public string Command_LL_GetUniqueID()
		{
			string result = null;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_iChip_UNIQUE_ID, new string[]
			{
				""
			});
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2)
			{
				string text = array[0];
				string text2 = array[1];
				if (text2.Equals(ConnectOneProtocol.RESPONSES[0]))
				{
					result = text;
				}
			}
			return result;
		}

		public iChipDataPartNumber Command_ST_Get_iChipPartNumber()
		{
			iChipDataPartNumber iChipDataPartNumber = null;
			string text = this.Command_BD_GetStatus(StatusReport.PART_NUMBER);
			iChipDataPartNumber result;
			if (!string.IsNullOrEmpty(text))
			{
				iChipDataPartNumber = new iChipDataPartNumber();
				string[] array = text.Split(new string[]
				{
					"-"
				}, StringSplitOptions.None);
				if (array == null || array.Length != 2)
				{
					result = null;
					return result;
				}
				iChipDataPartNumber.Version = int.Parse(array[0].Substring(2, 3));
				iChipDataPartNumber.InterfaceCode = (InterfaceCode)Enum.Parse(typeof(InterfaceCode), array[1].Trim());
			}
			result = iChipDataPartNumber;
			return result;
		}

		public iChipFirmwareData Command_ST_Get_iChipFirmwareData()
		{
			iChipFirmwareData iChipFirmwareData = null;
			string text = this.Command_BD_GetStatus(StatusReport.FIRMWARE_VERSION_AND_DATE);
			iChipFirmwareData result;
			if (!string.IsNullOrEmpty(text))
			{
				iChipFirmwareData = new iChipFirmwareData();
				string[] array = text.Split(new string[]
				{
					" "
				}, StringSplitOptions.None);
				if (array == null || array.Length != 2)
				{
					result = null;
					return result;
				}
				iChipFirmwareData.InterfaceCode = (InterfaceCode)Enum.Parse(typeof(InterfaceCode), array[0].Substring(1, 1));
				iChipFirmwareData.Version = int.Parse(array[0].Substring(2, 3));
				iChipFirmwareData.VersionType = array[0].Substring(5, 1);
				iChipFirmwareData.SubVersion = int.Parse(array[0].Substring(6));
				string[] array2 = array[1].Split(new string[]
				{
					"."
				}, StringSplitOptions.None);
				if (array2 != null && array2.Length == 3)
				{
					iChipFirmwareData.VersionTime = new DateTime(int.Parse(array2[2]), int.Parse(array2[1]), int.Parse(array2[0]), 0, 0, 0);
				}
			}
			result = iChipFirmwareData;
			return result;
		}

		public bool Command_CM_SetBaudRate(BaudRate br)
		{
			ConnectOnePacket command = null;
			if (br != BaudRate.BR_AUTO)
			{
				string text = br.ToString();
				string[] array = text.Split(new string[]
				{
					"_"
				}, StringSplitOptions.None);
				if (array != null && array.Length == 2)
				{
					int num = int.Parse(array[1]);
					Commands arg_79_0 = Commands.SET_BAUD_RATE;
					string[] array2 = new string[1];
					string[] arg_76_0 = array2;
					int arg_76_1 = 0;
					int num2 = (int)br;
					arg_76_0[arg_76_1] = num2.ToString();
					command = new ConnectOnePacket(arg_79_0, array2);
				}
			}
			else
			{
				command = new ConnectOnePacket(Commands.FORCE_AUTO_BAUD_RATE, null);
			}
			string[] array3 = this.Transcieve(command);
			return array3 != null && array3.Length == 1 && array3[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public BaudRate Command_CM_GetBaudRate()
		{
			BaudRate result = BaudRate.BR_AUTO;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_BAUD_RATE, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2 && array[1].Equals(ConnectOneProtocol.RESPONSES[0]))
			{
				result = (BaudRate)int.Parse(array[0]);
			}
			return result;
		}

		public bool Command_CM_StartInternetSession(InternetSessionConnectMode mode)
		{
			bool result = false;
			Commands arg_1C_0 = Commands.START_INTERNET;
			string[] array = new string[1];
			string[] arg_19_0 = array;
			int arg_19_1 = 0;
			int num = (int)mode;
			arg_19_0[arg_19_1] = num.ToString();
			ConnectOnePacket command = new ConnectOnePacket(arg_1C_0, array);
			string[] array2 = this.Transcieve(command);
			if (array2 != null && array2.Length == 1)
			{
				result = array2.Equals(ConnectOneProtocol.RESPONSES[3]);
			}
			return result;
		}

		public bool Command_CM_TriggerInternetSessionInitiation(InternetSessionTriggerMode mode)
		{
			bool result = false;
			Commands arg_1C_0 = Commands.TRIGGER_INTERNET_SESSION_INIT;
			string[] array = new string[1];
			string[] arg_19_0 = array;
			int arg_19_1 = 0;
			int num = (int)mode;
			arg_19_0[arg_19_1] = num.ToString();
			ConnectOnePacket command = new ConnectOnePacket(arg_1C_0, array);
			string[] array2 = this.Transcieve(command);
			if (array2 != null && array2.Length == 1)
			{
				result = array2.Equals(ConnectOneProtocol.RESPONSES[0]);
			}
			return result;
		}

		public string Command_CM_TerminateInternetSession()
		{
			string result = null;
			ConnectOnePacket command = new ConnectOnePacket(Commands.TERMINATE_INTERNET_SESSION, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2)
			{
				result = array[1];
			}
			return result;
		}

		public bool Command_CM_Set_HostInterface(HostInterfaces hif)
		{
			Commands arg_18_0 = Commands.SET_HOST_INTERFACE;
			string[] array = new string[1];
			string[] arg_16_0 = array;
			int arg_16_1 = 0;
			int num = (int)hif;
			arg_16_0[arg_16_1] = num.ToString();
			ConnectOnePacket command = new ConnectOnePacket(arg_18_0, array);
			string[] array2 = this.Transcieve(command);
			return array2 != null && array2.Length == 1 && array2[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public HostInterfaces Command_CM_Get_HostInterface()
		{
			HostInterfaces result = HostInterfaces.ERROR;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_HOST_INTERFACE, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2 && array[1].Equals(ConnectOneProtocol.RESPONSES[0]))
			{
				result = (HostInterfaces)int.Parse(array[0]);
			}
			return result;
		}

		public bool Command_CM_Set_WirelessLANServiceSetID(string ssid)
		{
			ConnectOnePacket command = new ConnectOnePacket(Commands.SET_WIRELESS_LAN_SERVICE_SET_ID, new string[]
			{
				ssid
			});
			string[] array = this.Transcieve(command);
			return array != null && array.Length == 1 && array[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public string Command_CM_Get_WirelessLANServiceSetID()
		{
			string result = null;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_WIRELESS_LAN_SERVICE_SET_ID, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2 && array[1].Equals(ConnectOneProtocol.RESPONSES[0]))
			{
				result = array[0];
			}
			return result;
		}

		public bool Command_CM_Set_WirelessLANSecurity(WirelessLANSecurityType secType)
		{
			Commands arg_20_0 = Commands.SET_WIRELESS_LAN_SECURITY;
			string[] array = new string[2];
			array[0] = "0";
			string[] arg_1E_0 = array;
			int arg_1E_1 = 1;
			int num = (int)secType;
			arg_1E_0[arg_1E_1] = num.ToString();
			ConnectOnePacket command = new ConnectOnePacket(arg_20_0, array);
			string[] array2 = this.Transcieve(command);
			return array2 != null && array2.Length == 1 && array2[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public string Command_CM_Get_WirelessLANSecurity()
		{
			string result = null;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_WIRELESS_LAN_SECURITY, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2 && array[1].Equals(ConnectOneProtocol.RESPONSES[0]))
			{
				result = array[0];
			}
			return result;
		}

		public bool Command_CM_Set_WirelessPassword(WirelessLANPasswordPlaceholder passPlace, string password)
		{
			Commands arg_1C_0 = Commands.SET_WIRELESS_PASSWORD;
			string[] array = new string[2];
			string[] arg_16_0 = array;
			int arg_16_1 = 0;
			int num = (int)passPlace;
			arg_16_0[arg_16_1] = num.ToString();
			array[1] = password;
			ConnectOnePacket command = new ConnectOnePacket(arg_1C_0, array);
			string[] array2 = this.Transcieve(command, new int?(60000));
			return array2 != null && array2.Length == 1 && array2[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public string Command_CM_Get_WirelessPassword()
		{
			string result = null;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_WIRELESS_PASSWORD, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2 && array[1].Equals(ConnectOneProtocol.RESPONSES[0]))
			{
				result = array[0];
			}
			return result;
		}

		public bool Command_CM_Set_HostServerEndPoint(iChipHostServerEndPoint endPoint)
		{
			if (endPoint == null)
			{
				throw new NullReferenceException("Endpoint is NULL!");
			}
			ConnectOnePacket command = new ConnectOnePacket(Commands.SET_HOST_SERVER_ENDPOINT, new string[]
			{
				endPoint.Address,
				endPoint.Port.ToString()
			});
			string[] array = this.Transcieve(command);
			return array != null && array.Length == 1 && array[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public iChipHostServerEndPoint Command_CM_Get_HostServerEndPoint()
		{
			iChipHostServerEndPoint iChipHostServerEndPoint = null;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_HOST_SERVER_ENDPOINT, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2 && array[1].Equals(ConnectOneProtocol.RESPONSES[0]))
			{
				iChipHostServerEndPoint = new iChipHostServerEndPoint();
				string[] array2 = array[0].Split(new string[]
				{
					":"
				}, StringSplitOptions.None);
				iChipHostServerEndPoint.Address = array2[0];
				iChipHostServerEndPoint.Port = int.Parse(array2[1]);
			}
			return iChipHostServerEndPoint;
		}

		public bool Command_CM_Set_SerialNetDeviceSerialInterface(SN_BaudRate br)
		{
			string arg_12_0;
			if (br == SN_BaudRate.SN_BD_IGNORE)
			{
				arg_12_0 = null;
			}
			else
			{
				int num = (int)br;
				arg_12_0 = num.ToString();
			}
			string text = arg_12_0;
			ConnectOnePacket command = new ConnectOnePacket(Commands.SET_SERIALNET_DEVICE_SERIAL_INTERFACE, new string[]
			{
				text
			});
			string[] array = this.Transcieve(command);
			return array != null && array.Length == 1 && array[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public bool Command_CM_Set_SerialNetDeviceSerialInterface(SN_BaudRate br, SN_DataBits db, SN_Parity pr, SN_StopBits sb, SN_Flow fc)
		{
			return false;
		}

		public bool Command_CM_Set_SocketFlushMaxTimeout(int timeout)
		{
			ConnectOnePacket command = new ConnectOnePacket(Commands.SET_SOCKET_FLUSH_MAX_TIMEOUT, new string[]
			{
				timeout.ToString()
			});
			string[] array = this.Transcieve(command);
			return array != null && array.Length == 1 && array[0].Equals(ConnectOneProtocol.RESPONSES[0]);
		}

		public string Command_CM_Get_SocketFlushMaxTimeout()
		{
			string result = null;
			ConnectOnePacket command = new ConnectOnePacket(Commands.GET_SOCKET_FLUSH_MAX_TIMEOUT, null);
			string[] array = this.Transcieve(command);
			if (array != null && array.Length == 2 && array[1].Equals(ConnectOneProtocol.RESPONSES[0]))
			{
				result = array[0];
			}
			return result;
		}
	}
}
