using System;

namespace SEDevices.Devices
{
	public enum WiringTestResult
	{
		WTR_NOT_TESTED = -1,
		WTR_OK,
		WTR_REVERESED,
		WTR_DISCONNECTED_OR_CROSS_BREED,
		WTR_FAILED,
		WTR_COUNT
	}
}
