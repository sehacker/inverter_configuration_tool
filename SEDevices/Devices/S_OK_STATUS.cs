using System;

namespace SEDevices.Devices
{
	public enum S_OK_STATUS
	{
		UNKNOWN = -1,
		NOT_OK,
		OK
	}
}
