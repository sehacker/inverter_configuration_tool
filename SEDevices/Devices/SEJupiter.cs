using SEDevices.Data;
using SEDevices.Records.Jupiter;
using SEDevices.Records.Parameter;
using SEDevices.Records.Status;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using SEProtocol.PLC;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace SEDevices.Devices
{
	public class SEJupiter : SECommDevice, ISEPLCCommandable
	{
		public const uint PROT_JUPITER_OFFSET = 8388608u;

		protected SWVersion _deviceSWVersionPwr;

		public SWVersion DeviceSWVersionPower
		{
			get
			{
				return this._deviceSWVersionPwr;
			}
		}

		public override uint DeviceMaxDataLength
		{
			get
			{
				return 480u;
			}
		}

		public override uint DeviceResetDelay
		{
			get
			{
				return 200u;
			}
		}

		public SEJupiter(uint address, CommManager commManager, object owner, SWVersion version, SWVersion pwrVersion) : base(commManager, owner, version)
		{
			this._address = address;
			if (version == null)
			{
				this._deviceSWVersion = this.Command_Device_Version();
			}
			if (pwrVersion == null)
			{
				this._deviceSWVersionPwr = this.Command_Device_Version_Pwr();
			}
		}

		protected override Type GetParamType(ushort index)
		{
			return DBParameterVersion.Instance.GetTypeOfParamJupiter(index);
		}

		public void Command_PLC_Transmit(byte[] dataToSend, bool isTransmitOnly)
		{
			if (dataToSend != null)
			{
				List<object> list = new List<object>(0);
				int num = dataToSend.Length;
				list.Add((ushort)num);
				list.Add(dataToSend);
				ushort expectedResponse = isTransmitOnly ? 0 : ProtocolGeneralResponses.PROT_RESP_ACK;
				Packet command = base.BuildCommand(536, list);
				base.Transcieve(command, expectedResponse, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			}
		}

		public uint GetAddress()
		{
			return base.Address;
		}

		public CommManager GetCommManager()
		{
			return base.CommManager;
		}

		public override SEParam Command_Params_Get(ushort index)
		{
			return this.Command_Params_Get((JupiterParams)index);
		}

		public SEParam Command_Params_Get(JupiterParams param)
		{
			SEParam sEParam;
			if (param == JupiterParams.ID)
			{
				sEParam = base.Command_Params_Get_Real((ushort)param);
			}
			else
			{
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.JUPITER, base.DeviceSWVersion);
				sEParam = base.Command_Params_Get_Real((ushort)paramRealIndex);
				if (sEParam != null)
				{
					sEParam.Index = (ushort)param;
				}
			}
			return sEParam;
		}

		public override SEParam[] Command_Params_Get(ushort[] indices)
		{
			if (indices == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (indices.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			List<JupiterParams> list = new List<JupiterParams>(0);
			int num = indices.Length;
			for (int i = 0; i < num; i++)
			{
				list.Add((JupiterParams)indices[i]);
			}
			return this.Command_Params_Get(list.ToArray());
		}

		public SEParam[] Command_Params_Get(JupiterParams[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			int num = paramList.Length;
			List<ushort> list = new List<ushort>(0);
			for (int i = 0; i < num; i++)
			{
				JupiterParams jupiterParams = paramList[i];
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)jupiterParams, DeviceType.JUPITER, base.DeviceSWVersion);
				list.Add((ushort)paramRealIndex);
			}
			SEParam[] array = base.Command_Params_Get_Real(list.ToArray());
			int num2 = array.Length;
			for (int i = 0; i < num2; i++)
			{
				SEParam sEParam = array[i];
				sEParam.Index = (ushort)paramList[i];
			}
			return array;
		}

		public override string Command_Params_Get_Name(ushort index)
		{
			return this.Command_Params_Get_Name((JupiterParams)index);
		}

		public string Command_Params_Get_Name(JupiterParams param)
		{
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.JUPITER, base.DeviceSWVersion);
			return base.Command_Params_Get_Name_Real((ushort)paramRealIndex);
		}

		public override SEParamInfo Command_Params_Get_Info(ushort index)
		{
			return this.Command_Params_Get_Info((JupiterParams)index);
		}

		public SEParamInfo Command_Params_Get_Info(JupiterParams param)
		{
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.JUPITER, base.DeviceSWVersion);
			return base.Command_Params_Get_Info_Real((ushort)paramRealIndex);
		}

		public override int Command_Params_Get_Count()
		{
			return base.Command_Params_Get_Count_Real();
		}

		public override void Command_Params_Set(SEParam param)
		{
			if (param == null)
			{
				throw new NullReferenceException("No Parameter to send!");
			}
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(param.Index, DeviceType.JUPITER, base.DeviceSWVersion);
			if (paramRealIndex > -1)
			{
				this.Command_Device_Stop();
				bool flag = DBParameterVersion.Instance.IsResetRequiredForParam(param.Index, DeviceType.JUPITER, base.DeviceSWVersion);
				param.Index = (ushort)paramRealIndex;
				base.Command_Params_Set_Real(param);
				if (flag)
				{
					this.Command_Device_Reset(this.DeviceResetDelay);
					Thread.Sleep((int)(1000u + this.DeviceResetDelay));
				}
			}
		}

		public override void Command_Params_Set(SEParam[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length > 0)
			{
				int num = paramList.Length;
				for (int i = 0; i < num; i++)
				{
					SEParam sEParam = paramList[i];
					if (sEParam != null)
					{
						int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(sEParam.Index, DeviceType.JUPITER, base.DeviceSWVersion);
						sEParam.Index = (ushort)paramRealIndex;
					}
				}
				this.Command_Device_Stop();
				base.Command_Params_Set_Real(paramList);
				this.Command_Device_Reset(this.DeviceResetDelay);
				Thread.Sleep((int)(1000u + this.DeviceResetDelay));
			}
		}

		public SETelemetrySystemStatusJupiter Command_Jupiter_GetSysStatus()
		{
			SETelemetrySystemStatusJupiter sETelemetrySystemStatusJupiter = null;
			Packet command = base.BuildCommand(2054, null);
			Packet packet = base.Transcieve(command, 2182, 8000u, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sETelemetrySystemStatusJupiter = new SETelemetrySystemStatusJupiter();
				sETelemetrySystemStatusJupiter.Header.Code = (ETelemCodes)packet.RetrieveUInt16();
				sETelemetrySystemStatusJupiter.Header.DataSize = packet.RetrieveUInt16();
				sETelemetrySystemStatusJupiter.Header.Id = packet.RetrieveUInt32();
				sETelemetrySystemStatusJupiter.Vin = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Vout = new SEPhaseData();
				sETelemetrySystemStatusJupiter.Vout.Phase_1 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Vout.Phase_2 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Vout.Phase_3 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Power = new SEPhaseData();
				sETelemetrySystemStatusJupiter.Power.Phase_1 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Power.Phase_2 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Power.Phase_3 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Freq = new SEPhaseData();
				sETelemetrySystemStatusJupiter.Freq.Phase_1 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Freq.Phase_2 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Freq.Phase_3 = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Temp = packet.RetrieveSingle();
				sETelemetrySystemStatusJupiter.Timer = packet.RetrieveUInt32();
				sETelemetrySystemStatusJupiter.InvState = (EInvProcessState)packet.RetrieveInt16();
				sETelemetrySystemStatusJupiter.OnOffSwitch = packet.RetrieveUInt16();
				sETelemetrySystemStatusJupiter.LastErr = (EVenusEventCodes)packet.RetrieveInt16();
				sETelemetrySystemStatusJupiter.ISEMode = (EInvProtInverterMode)packet.RetrieveInt16();
				sETelemetrySystemStatusJupiter.RelayStat = packet.RetrieveInt16();
			}
			return sETelemetrySystemStatusJupiter;
		}

		public void Command_Jupiter_SetCountry(uint countryIndex)
		{
			Packet command = base.BuildCommand(2057, new List<object>(0)
			{
				countryIndex
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public override SWVersion Command_Device_Version()
		{
			SWVersion sWVersion = new SWVersion();
			SEParam sEParam = base.Command_Params_Get_Real(1);
			SEParam sEParam2 = base.Command_Params_Get_Real(2);
			SEParam sEParam3 = base.Command_Params_Get_Real(3);
			SEParam sEParam4 = base.Command_Params_Get_Real(4);
			string text = (sEParam != null) ? sEParam.GetParamAsString() : null;
			string text2 = (sEParam2 != null) ? sEParam2.GetParamAsString() : null;
			string text3 = (sEParam3 != null) ? sEParam3.GetParamAsString() : null;
			string text4 = (sEParam4 != null) ? sEParam4.GetParamAsString() : null;
			if (text != null)
			{
				sWVersion.Major = uint.Parse(text);
			}
			if (text2 != null)
			{
				sWVersion.Minor = uint.Parse(text2);
			}
			if (text3 != null)
			{
				sWVersion.Build = uint.Parse(text3);
			}
			if (text4 != null)
			{
				sWVersion.CheckSum = new uint?(uint.Parse(text4));
			}
			return sWVersion;
		}

		public SWVersion Command_Device_Version_Pwr()
		{
			SWVersion sWVersion = new SWVersion();
			SEParam sEParam = base.Command_Params_Get_Real(5);
			SEParam sEParam2 = base.Command_Params_Get_Real(6);
			string text = (sEParam != null) ? sEParam.GetParamAsString() : null;
			string text2 = (sEParam2 != null) ? sEParam2.GetParamAsString() : null;
			if (text != null)
			{
				sWVersion.Major = uint.Parse(text);
			}
			if (text2 != null)
			{
				sWVersion.Minor = uint.Parse(text2);
			}
			return sWVersion;
		}

		public override void UpgradeState_VerifyCompliance(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList)
		{
			if (stateData != null && deviceVersionList != null && deviceUpgradeVersionList != null && deviceCompatabilityList != null)
			{
				JupiterVersionInfo jupiterVersionInfo = (JupiterVersionInfo)deviceVersionList[0];
				JupiterPowerVersionInfo jupiterPowerVersionInfo = (JupiterPowerVersionInfo)deviceVersionList[1];
				JupiterVersionInfo jupiterVersion = (JupiterVersionInfo)deviceUpgradeVersionList[0];
				List<JupiterPowerCompatibilityItem> list = new List<JupiterPowerCompatibilityItem>(0);
				int count = deviceCompatabilityList.Count;
				for (int i = 0; i < count; i++)
				{
					list.Add((JupiterPowerCompatibilityItem)deviceCompatabilityList[i]);
				}
				if (jupiterVersionInfo.Equals(jupiterVersion))
				{
					stateData.UpgradeState = UpgradeState.Finished;
				}
				else
				{
					JupiterPowerCompatibilityItem jupiterPowerCompatibilityItem = null;
					JupiterCompatibilityItem jupiterCompatibilityItem = null;
					foreach (JupiterPowerCompatibilityItem current in list)
					{
						if (current.JupiterPowerVersions == null)
						{
							jupiterPowerCompatibilityItem = current;
						}
						else
						{
							foreach (JupiterPowerVersionInfo current2 in current.JupiterPowerVersions)
							{
								if (current2.Equals(jupiterPowerVersionInfo))
								{
									jupiterPowerCompatibilityItem = current;
									break;
								}
							}
						}
						if (jupiterPowerCompatibilityItem != null)
						{
							break;
						}
					}
					if (jupiterPowerCompatibilityItem == null)
					{
						throw new Exception(string.Format("Upgrade file is not compatible to DSP2 version - ({0})", jupiterPowerVersionInfo.ToString()));
					}
					foreach (JupiterCompatibilityItem current3 in jupiterPowerCompatibilityItem.JupiterCompatability)
					{
						if (current3.JupiterVersions == null)
						{
							jupiterCompatibilityItem = current3;
						}
						else
						{
							foreach (JupiterVersionInfo current4 in current3.JupiterVersions)
							{
								if (current4.Equals(jupiterVersionInfo))
								{
									jupiterCompatibilityItem = current3;
									break;
								}
							}
						}
						if (jupiterCompatibilityItem != null)
						{
							break;
						}
					}
					if (jupiterCompatibilityItem == null)
					{
						throw new Exception("Upgrade file is not compatible to DSP1 version - (" + jupiterVersionInfo.ToString() + ")");
					}
					stateData.JupiterCompatibiltyItem = jupiterCompatibilityItem;
					stateData.OriginalJupiterVersion = jupiterVersionInfo;
					stateData.OriginalJupiterPowerVersion = jupiterPowerVersionInfo;
					stateData.UpgradeState = UpgradeState.SaveParamaeters;
				}
			}
		}

		public override void UpgradeState_SaveParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, uint? sufVersion)
		{
			if (stateData != null && deviceVersionList != null)
			{
				JupiterVersionInfo jupiterVersionInfo = (JupiterVersionInfo)deviceVersionList[0];
				JupiterPowerVersionInfo jupiterPowerVersionInfo = (JupiterPowerVersionInfo)deviceVersionList[1];
				if (!jupiterVersionInfo.Equals(stateData.OriginalJupiterVersion) || !jupiterPowerVersionInfo.Equals(stateData.OriginalJupiterPowerVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				stateData.SavedParameters = new List<ParamaeterPair>();
				foreach (int current in stateData.JupiterCompatibiltyItem.RestoreParams)
				{
					int num = current;
					if (!sufVersion.HasValue)
					{
						ushort? paramVirtualIndex = DBParameterVersion.Instance.GetParamVirtualIndex(num, DeviceType.JUPITER, base.DeviceSWVersion);
						ushort? num2 = paramVirtualIndex;
						if ((num2.HasValue ? new int?((int)num2.GetValueOrDefault()) : null).HasValue)
						{
							num = (int)paramVirtualIndex.Value;
						}
					}
					SEParam sEParam = this.Command_Params_Get((JupiterParams)num);
					string paramAsString = sEParam.GetParamAsString();
					ParamaeterPair paramaeterPair = new ParamaeterPair();
					paramaeterPair.Index = num;
					paramaeterPair.Value = paramAsString;
					stateData.SavedParameters.Add(paramaeterPair);
				}
				stateData.UpgradeState = UpgradeState.UpgradeSoftware;
			}
		}

		public override void UpgradeState_UpgradeSoftwareStatus(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData)
		{
			if (stateData != null && upgradeXmlData != null)
			{
				uint checksum = 0u;
				if (upgradeXmlData.UpgradeData.DataChecksum.HasValue)
				{
					checksum = upgradeXmlData.UpgradeData.DataChecksum.Value;
				}
				base.DoUpgrade(upgradeXmlData.UpgradeData.Data, upgradeXmlData.UpgradeData.Data.Length, 0u, checksum, 0, base.UpgradeNotifier);
				stateData.UpgradeState = UpgradeState.VerifyVersionAfterUpgrade;
			}
		}

		public override void UpgradeState_VerifyVersionAfterUpgrade(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraCommDevice)
		{
			if (stateData != null && deviceVersionList != null)
			{
				JupiterVersionInfo jupiterVersionInfo = (JupiterVersionInfo)deviceVersionList[0];
				JupiterVersionInfo jupiterVersion = (JupiterVersionInfo)deviceVersionList[1];
				this.Command_Device_Reset(0u);
				SEPortia sEPortia = (SEPortia)extraCommDevice;
				sEPortia.Command_Device_Reset(500u);
				int deviceDefaultRetries = (int)base.DeviceDefaultRetries;
				base.DeviceDefaultRetries = 50u;
				this._deviceSWVersion = this.Command_Device_Version();
				this._deviceSWVersionPwr = this.Command_Device_Version_Pwr();
				Trace.WriteLine("D");
				SWVersion deviceSWVersion = base.DeviceSWVersion;
				jupiterVersionInfo.MajorVersion = deviceSWVersion.Major;
				jupiterVersionInfo.MinorVersion = deviceSWVersion.Minor;
				jupiterVersionInfo.BuildVersion = new uint?(deviceSWVersion.Build);
				jupiterVersionInfo.Checksum = deviceSWVersion.CheckSum.Value;
				if (!jupiterVersionInfo.Equals(jupiterVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					base.DeviceDefaultRetries = (uint)deviceDefaultRetries;
					stateData.UpgradeState = UpgradeState.ConfigureParamaeters;
				}
			}
		}

		public override void UpgradeState_ConfigureParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList)
		{
			if (stateData != null && deviceVersionList != null)
			{
				JupiterVersionInfo jupiterVersionInfo = (JupiterVersionInfo)deviceVersionList[0];
				JupiterVersionInfo jupiterVersion = (JupiterVersionInfo)deviceVersionList[1];
				if (!jupiterVersionInfo.Equals(jupiterVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					this.Command_Device_Stop();
					if (stateData.JupiterCompatibiltyItem.SetCountry.HasValue)
					{
						if (stateData.JupiterCompatibiltyItem.SetCountry.Value)
						{
							List<SEParam> list = new List<SEParam>(0);
							foreach (ParamaeterPair current in stateData.SavedParameters)
							{
								if (current.Index == 13)
								{
									SEParam param = new SEParam((ushort)current.Index, uint.Parse(current.Value));
									this.Command_Params_Set(param);
									break;
								}
							}
							uint countryIndex = 0u;
							bool flag = false;
							foreach (ParamaeterPair current in stateData.SavedParameters)
							{
								if (current.Index == 12)
								{
									countryIndex = Convert.ToUInt32(current.Value);
									flag = true;
									break;
								}
							}
							if (flag)
							{
								this.Command_Jupiter_SetCountry(countryIndex);
							}
							else
							{
								SEParam sEParam = this.Command_Params_Get(JupiterParams.INV_COUNTRY_CODE);
								uint valueUInt = sEParam.ValueUInt32;
								this.Command_Jupiter_SetCountry(countryIndex);
							}
						}
					}
					List<SEParam> list2 = new List<SEParam>(0);
					foreach (ParamaeterPair current in stateData.SavedParameters)
					{
						ushort num = (ushort)current.Index;
						Type typeOfParamJupiter = DBParameterVersion.Instance.GetTypeOfParamJupiter(num);
						ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamJupiter);
						SEParam item = null;
						switch (paramTypeByType)
						{
						case ParamType.UINT32:
							item = new SEParam(num, uint.Parse(current.Value));
							break;
						case ParamType.FLOAT:
							item = new SEParam(num, float.Parse(current.Value));
							break;
						case ParamType.INT:
							item = new SEParam(num, int.Parse(current.Value));
							break;
						case ParamType.STRING:
							item = new SEParam(num, current.Value);
							break;
						}
						list2.Add(item);
					}
					if (stateData.JupiterCompatibiltyItem != null && stateData.JupiterCompatibiltyItem.ParamatersToSet != null)
					{
						foreach (ParamaeterPair current in stateData.JupiterCompatibiltyItem.ParamatersToSet)
						{
							ushort num = (ushort)current.Index;
							Type typeOfParamJupiter = DBParameterVersion.Instance.GetTypeOfParamJupiter(num);
							ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamJupiter);
							SEParam item = null;
							switch (paramTypeByType)
							{
							case ParamType.UINT32:
								item = new SEParam(num, uint.Parse(current.Value));
								break;
							case ParamType.FLOAT:
								item = new SEParam(num, float.Parse(current.Value));
								break;
							case ParamType.INT:
								item = new SEParam(num, int.Parse(current.Value));
								break;
							case ParamType.STRING:
								item = new SEParam(num, current.Value);
								break;
							}
							list2.Add(item);
						}
					}
					this.Command_Params_Set(list2.ToArray());
					stateData.UpgradeState = UpgradeState.Finished;
				}
			}
		}

		public override void UpgradeState_Finished(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion)
		{
			JupiterVersionInfo jupiterVersionInfo = (JupiterVersionInfo)deviceVersion;
			JupiterVersionInfo jupiterVersion = (JupiterVersionInfo)upgradeVersion;
			if (!jupiterVersionInfo.Equals(jupiterVersion))
			{
				stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
		}

		public void UpgradeState_VerifyCompliance_Power(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList)
		{
			if (stateData != null && deviceVersionList != null && deviceUpgradeVersionList != null)
			{
				JupiterPowerVersionInfo jupiterPowerVersionInfo = (JupiterPowerVersionInfo)deviceVersionList[0];
				JupiterPowerVersionInfo jupiterPowerVersion = (JupiterPowerVersionInfo)deviceUpgradeVersionList[0];
				if (jupiterPowerVersionInfo.Equals(jupiterPowerVersion))
				{
					stateData.UpgradeState = UpgradeState.Finished;
				}
				else
				{
					stateData.UpgradeState = UpgradeState.UpgradeSoftware;
				}
			}
		}

		public void UpgradeState_UpgradeSoftwareStatus_Power(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData)
		{
			if (stateData != null && upgradeXmlData != null)
			{
				uint checksum = 0u;
				if (upgradeXmlData.UpgradeData.DataChecksum.HasValue)
				{
					checksum = upgradeXmlData.UpgradeData.DataChecksum.Value;
				}
				base.DoUpgrade(upgradeXmlData.UpgradeData.Data, upgradeXmlData.UpgradeData.Data.Length, 2147483648u, checksum, 32768, base.UpgradeNotifier);
				stateData.UpgradeState = UpgradeState.VerifyVersionAfterUpgrade;
			}
		}

		public void UpgradeState_VerifyVersionAfterUpgrade_Power(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraCommDevice)
		{
			if (stateData != null && deviceVersionList != null)
			{
				JupiterPowerVersionInfo jupiterPowerVersionInfo = (JupiterPowerVersionInfo)deviceVersionList[0];
				JupiterPowerVersionInfo jupiterPowerVersion = (JupiterPowerVersionInfo)deviceVersionList[1];
				this.Command_Device_Reset(0u);
				SEPortia sEPortia = (SEPortia)extraCommDevice;
				sEPortia.Command_Device_Reset(500u);
				int deviceDefaultRetries = (int)base.DeviceDefaultRetries;
				base.DeviceDefaultRetries = 50u;
				this._deviceSWVersionPwr = this.Command_Device_Version_Pwr();
				SWVersion deviceSWVersionPower = this.DeviceSWVersionPower;
				jupiterPowerVersionInfo.MajorVersion = deviceSWVersionPower.Major;
				jupiterPowerVersionInfo.MinorVersion = deviceSWVersionPower.Minor;
				if (!jupiterPowerVersionInfo.Equals(jupiterPowerVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					base.DeviceDefaultRetries = (uint)deviceDefaultRetries;
					stateData.UpgradeState = UpgradeState.Finished;
				}
			}
		}

		public void UpgradeState_Finished_Power(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion)
		{
			JupiterPowerVersionInfo jupiterPowerVersionInfo = (JupiterPowerVersionInfo)deviceVersion;
			JupiterPowerVersionInfo jupiterPowerVersion = (JupiterPowerVersionInfo)upgradeVersion;
			if (!jupiterPowerVersionInfo.Equals(jupiterPowerVersion))
			{
				stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
		}
	}
}
