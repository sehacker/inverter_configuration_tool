using System;

namespace SEDevices.Devices
{
	public enum SwitchStates
	{
		DISCONNECTED,
		DISCONNECTED_DONT_KNOW,
		CONNECTED,
		CONNECTED_DONT_KNOW
	}
}
