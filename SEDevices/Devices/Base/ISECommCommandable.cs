using SEDevices.Records.Parameter;
using SEUtils.Common;
using System;

namespace SEDevices.Devices.Base
{
	public interface ISECommCommandable
	{
		SEParam Command_Params_Get(ushort index);

		SEParam[] Command_Params_Get(ushort[] indices);

		int Command_Params_Get_Count();

		string Command_Params_Get_Name(ushort index);

		SEParamInfo Command_Params_Get_Info(ushort index);

		void Command_Params_Set(SEParam param);

		void Command_Params_Set(SEParam[] paramList);

		void Command_Params_Reset();

		void Command_Params_Save();

		void Command_Device_Reset();

		void Command_Device_Reset(uint delay);

		void Command_Device_Stop();

		SWVersion Command_Device_Version();

		void SetDefaultTimeout(uint defaultTimeout);

		uint GetDefaultTimeout();

		void SetDefaultRetries(uint defaultRetries);

		uint GetDefaultRetries();

		void SetNextID(ushort nextID);

		ushort GetNextID();

		void SetSourceID(uint sourceID);

		uint GetSourceID();
	}
}
