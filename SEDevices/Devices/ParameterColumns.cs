using System;

namespace SEDevices.Devices
{
	public enum ParameterColumns
	{
		COL_PARAM_INDEX,
		COL_PARAM_NAME,
		COL_PARAM_TYPE,
		COL_PARAM_VALUE,
		COL_PARAM_DEFAULT,
		COL_PARAM_MIN_VAL,
		COL_PARAM_MAX_VAL,
		COL_PARAM_ACCESS_LEVEL,
		COUNT
	}
}
