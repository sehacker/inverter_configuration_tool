using System;

namespace SEDevices.Data
{
	public enum DeviceType
	{
		PORTIA,
		VENUS,
		MERCURY,
		JUPITER,
		GEMINI,
		COUNT
	}
}
