using SEStorage;
using SEUtils.Common;
using System;
using System.IO;
using System.Reflection;

namespace SEDevices.Data
{
	public class DBParameterVersion : DBExcel
	{
		private const string WS_NAME_DBMAIN = "DBMain";

		private const string WS_NAME_PORTIA_INDICES_PER_VERSION = "Enum_IndicesPerVersion(Portia)";

		private const string WS_NAME_VENUS_INDICES_PER_VERSION = "Enum_IndicesPerVersion(Venus)";

		private const string WS_NAME_MERCURY_INDICES_PER_VERSION = "Enum_IndicesPerVersion(Mercury)";

		private const string WS_NAME_JUPITER_INDICES_PER_VERSION = "Enum_IndicesPerVersion(Jupiter)";

		private const string WS_NAME_GEMINI_INDICES_PER_VERSION = "Enum_IndicesPerVersion(Gemini)";

		private const string WS_NAME_PORTIA_RESET_PER_VERSION = "Enum_ResetPerVersion(Portia)";

		private const string WS_NAME_VENUS_RESET_PER_VERSION = "Enum_ResetPerVersion(Venus)";

		private const string WS_NAME_MERCURY_RESET_PER_VERSION = "Enum_ResetPerVersion(Mercury)";

		private const string WS_NAME_JUPITER_RESET_PER_VERSION = "Enum_ResetPerVersion(Jupiter)";

		private const string WS_NAME_GEMINI_RESET_PER_VERSION = "Enum_ResetPerVersion(Gemini)";

		private const int ROW_DBMAIN_COUNT_PARAMS_PORTIA = 3;

		private const int ROW_DBMAIN_COUNT_PARAMS_VENUS = 4;

		private const int ROW_DBMAIN_COUNT_PARAMS_MERCURY = 5;

		private const int ROW_DBMAIN_COUNT_PARAMS_JUPITER = 6;

		private const int ROW_DBMAIN_COUNT_PARAMS_GEMINI = 7;

		private const int ROW_DBMAIN_PORTIA_VERSION_LIST_START = 11;

		private const int ROW_DBMAIN_VENUS_VERSION_LIST_START = 11;

		private const int ROW_DBMAIN_MERCURY_VERSION_LIST_START = 11;

		private const int ROW_DBMAIN_JUPITER_VERSION_LIST_START = 11;

		private const int ROW_DBMAIN_GEMINI_VERSION_LIST_START = 11;

		private const int ROW_DBMAIN_VENUS_CHECKSUM_LIST_START = 11;

		private const int ROW_DBMAIN_PORTIA_VERSION_LIST_COUNTER = 9;

		private const int ROW_DBMAIN_VENUS_VERSION_LIST_COUNTER = 9;

		private const int ROW_DBMAIN_MERCURY_VERSION_LIST_COUNTER = 9;

		private const int ROW_DBMAIN_JUPITER_VERSION_LIST_COUNTER = 9;

		private const int ROW_DBMAIN_GEMINI_VERSION_LIST_COUNTER = 9;

		private const int ROW_DBMAIN_VENUS_CHECKSUM_LIST_COUNTER = 9;

		private const int COL_DBMAIN_COUNT_PARAMS = 1;

		private const int COL_DBMAIN_PORTIA_VERSION_LIST = 0;

		private const int COL_DBMAIN_PORTIA_VERSION_LIST_INDEX = 1;

		private const int COL_DBMAIN_VENUS_VERSION_LIST = 2;

		private const int COL_DBMAIN_VENUS_VERSION_LIST_INDEX = 3;

		private const int COL_DBMAIN_VENUS_CHECKSUM_LIST_VERSION = 4;

		private const int COL_DBMAIN_VENUS_CHECKSUM_LIST_BUILD = 5;

		private const int COL_DBMAIN_VENUS_CHECKSUM_LIST_CHECKSUM = 6;

		private const int COL_DBMAIN_MERCURY_VERSION_LIST = 7;

		private const int COL_DBMAIN_MERCURY_VERSION_LIST_INDEX = 8;

		private const int COL_DBMAIN_JUPITER_VERSION_LIST = 9;

		private const int COL_DBMAIN_JUPITER_VERSION_LIST_INDEX = 10;

		private const int COL_DBMAIN_GEMINI_VERSION_LIST = 11;

		private const int COL_DBMAIN_GEMINI_VERSION_LIST_INDEX = 12;

		private const int COL_DBMAIN_PORTIA_VERSION_COUNTER = 1;

		private const int COL_DBMAIN_VENUS_VERSION_COUNTER = 3;

		private const int COL_DBMAIN_VENUS_CHECKSUM_COUNTER = 5;

		private const int COL_DBMAIN_MERCURY_VERSION_COUNTER = 8;

		private const int COL_DBMAIN_JUPITER_VERSION_COUNTER = 10;

		private const int COL_DBMAIN_GEMINI_VERSION_COUNTER = 12;

		private const int COL_PARAMS_VERSION_NAME = 0;

		private const int COL_PARAMS_VERSION_TYPE = 1;

		private const int COL_PARAMS_VERSION_VER_START = 2;

		private const int COL_PARAMS_RESET_NAME = 0;

		private const int COL_PARAMS_RESET_VER_START = 1;

		private static DBParameterVersion _instance;

		public static DBParameterVersion Instance
		{
			get
			{
				if (DBParameterVersion._instance == null)
				{
					string name = "SEDevices.Resources.Databases.SEParamVersionTable.xlsm";
					Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
					DBParameterVersion._instance = new DBParameterVersion(manifestResourceStream);
				}
				return DBParameterVersion._instance;
			}
		}

		private DBParameterVersion(Stream fileStream) : base(fileStream, FileTypes.XLSX)
		{
		}

		private DBParameterVersion(string filePath) : base(filePath, FileTypes.XLSX)
		{
		}

		public int GetCountParamsPortia()
		{
			return int.Parse(base.GetCell("DBMain", 3, 1).ToString());
		}

		public int GetCountParamsVenus()
		{
			return int.Parse(base.GetCell("DBMain", 4, 1).ToString());
		}

		public int GetCountParamsMercury()
		{
			return int.Parse(base.GetCell("DBMain", 5, 1).ToString());
		}

		public int GetCountParamsJupiter()
		{
			return int.Parse(base.GetCell("DBMain", 6, 1).ToString());
		}

		public int GetCountParamsGemini()
		{
			return int.Parse(base.GetCell("DBMain", 7, 1).ToString());
		}

		public Type GetTypeOfParamPortia(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_IndicesPerVersion(Portia)", virtualIndex);
		}

		public Type GetTypeOfParamVenus(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_IndicesPerVersion(Venus)", virtualIndex);
		}

		public Type GetTypeOfParamMercury(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_IndicesPerVersion(Mercury)", virtualIndex);
		}

		public Type GetTypeOfParamJupiter(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_IndicesPerVersion(Jupiter)", virtualIndex);
		}

		public Type GetTypeOfParamGemini(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_IndicesPerVersion(Gemini)", virtualIndex);
		}

		public Type GetTypeOfParam(string worksheetName, ushort virtualIndex)
		{
			object cell = base.GetCell(worksheetName, (int)(virtualIndex + 1), 1);
			string text = (cell != null) ? cell.ToString() : null;
			Type type = null;
			Type result;
			if (string.IsNullOrEmpty(text))
			{
				result = type;
			}
			else
			{
				string text2 = text;
				switch (text2)
				{
				case "uint":
				case "bool":
				case "list":
					type = typeof(uint);
					break;
				case "int":
					type = typeof(int);
					break;
				case "float":
				case "fbool":
					type = typeof(float);
					break;
				case "str":
					type = typeof(string);
					break;
				}
				result = type;
			}
			return result;
		}

		private int GetVersionColumnOffset(SWVersion version, DeviceType type)
		{
			int num = -1;
			int result;
			if (type == DeviceType.COUNT)
			{
				result = num;
			}
			else
			{
				int row = 0;
				int column = 0;
				int num2 = 0;
				switch (type)
				{
				case DeviceType.PORTIA:
					row = 9;
					column = 1;
					num2 = 11;
					break;
				case DeviceType.VENUS:
					row = 9;
					column = 3;
					num2 = 11;
					break;
				case DeviceType.MERCURY:
					row = 9;
					column = 8;
					num2 = 11;
					break;
				case DeviceType.JUPITER:
					row = 9;
					column = 10;
					num2 = 11;
					break;
				case DeviceType.GEMINI:
					row = 9;
					column = 12;
					num2 = 11;
					break;
				}
				object cell = base.GetCell("DBMain", row, column);
				string text = (cell != null) ? cell.ToString() : null;
				int num3 = (!string.IsNullOrEmpty(text)) ? int.Parse(text) : -1;
				if (num3 == -1)
				{
					result = num;
				}
				else
				{
					int column2 = 0;
					int column3 = 0;
					switch (type)
					{
					case DeviceType.PORTIA:
						column2 = 0;
						column3 = 1;
						break;
					case DeviceType.VENUS:
						column2 = 2;
						column3 = 3;
						break;
					case DeviceType.MERCURY:
						column2 = 7;
						column3 = 8;
						break;
					case DeviceType.JUPITER:
						column2 = 9;
						column3 = 10;
						break;
					case DeviceType.GEMINI:
						column2 = 11;
						column3 = 12;
						break;
					}
					int num4 = num2 + num3;
					for (int i = num2; i < num4; i++)
					{
						object cell2 = base.GetCell("DBMain", i, column2);
						string text2 = (cell2 != null) ? cell2.ToString() : null;
						SWVersion v = (!string.IsNullOrEmpty(text2)) ? new SWVersion(text2) : null;
						if (v == null || v > version)
						{
							break;
						}
						if (v <= version)
						{
							object cell3 = base.GetCell("DBMain", i, column3);
							string text3 = (cell3 != null) ? cell3.ToString() : null;
							num = ((!string.IsNullOrEmpty(text3)) ? int.Parse(text3) : -1);
						}
					}
					result = num;
				}
			}
			return result;
		}

		public int GetParamRealIndex(ushort virtualIndex, DeviceType type, SWVersion version)
		{
			int num = -1;
			int result;
			if (type == DeviceType.COUNT)
			{
				result = num;
			}
			else
			{
				int versionColumnOffset = this.GetVersionColumnOffset(version, type);
				if (versionColumnOffset == -1)
				{
					result = num;
				}
				else
				{
					string worksheetName = null;
					switch (type)
					{
					case DeviceType.PORTIA:
						worksheetName = "Enum_IndicesPerVersion(Portia)";
						break;
					case DeviceType.VENUS:
						worksheetName = "Enum_IndicesPerVersion(Venus)";
						break;
					case DeviceType.MERCURY:
						worksheetName = "Enum_IndicesPerVersion(Mercury)";
						break;
					case DeviceType.JUPITER:
						worksheetName = "Enum_IndicesPerVersion(Jupiter)";
						break;
					case DeviceType.GEMINI:
						worksheetName = "Enum_IndicesPerVersion(Gemini)";
						break;
					}
					object cell = base.GetCell(worksheetName, (int)(virtualIndex + 1), 2 + versionColumnOffset);
					string text = (cell != null) ? cell.ToString() : null;
					if (!string.IsNullOrEmpty(text) && !text.Equals("null"))
					{
						num = int.Parse(text);
					}
					result = num;
				}
			}
			return result;
		}

		public ushort? GetParamVirtualIndex(int realIndex, DeviceType type, SWVersion version)
		{
			ushort? num = null;
			ushort? result;
			if (realIndex < 0 || type == DeviceType.COUNT)
			{
				result = num;
			}
			else
			{
				int versionColumnOffset = this.GetVersionColumnOffset(version, type);
				if (versionColumnOffset == -1)
				{
					result = num;
				}
				else
				{
					string worksheetName = null;
					int num2 = 0;
					switch (type)
					{
					case DeviceType.PORTIA:
						worksheetName = "Enum_IndicesPerVersion(Portia)";
						num2 = this.GetCountParamsPortia();
						break;
					case DeviceType.VENUS:
						worksheetName = "Enum_IndicesPerVersion(Venus)";
						num2 = this.GetCountParamsVenus();
						break;
					case DeviceType.MERCURY:
						worksheetName = "Enum_IndicesPerVersion(Mercury)";
						num2 = this.GetCountParamsMercury();
						break;
					case DeviceType.JUPITER:
						worksheetName = "Enum_IndicesPerVersion(Jupiter)";
						num2 = this.GetCountParamsJupiter();
						break;
					case DeviceType.GEMINI:
						worksheetName = "Enum_IndicesPerVersion(Gemini)";
						num2 = this.GetCountParamsGemini();
						break;
					}
					num2++;
					for (int i = 1; i < num2; i++)
					{
						object cell = base.GetCell(worksheetName, i, 2 + versionColumnOffset);
						int num3 = (cell != null) ? int.Parse(cell.ToString()) : -1;
						if (num3 >= 0 && num3 == realIndex)
						{
							num = new ushort?((ushort)(i - 1));
							break;
						}
					}
					result = num;
				}
			}
			return result;
		}

		public bool IsResetRequiredForParam(ushort index, DeviceType type, SWVersion version)
		{
			bool flag = false;
			bool result;
			if (index < 0 || type == DeviceType.COUNT)
			{
				result = flag;
			}
			else
			{
				int versionColumnOffset = this.GetVersionColumnOffset(version, type);
				if (versionColumnOffset == -1)
				{
					result = flag;
				}
				else
				{
					string worksheetName = null;
					switch (type)
					{
					case DeviceType.PORTIA:
						worksheetName = "Enum_ResetPerVersion(Portia)";
						break;
					case DeviceType.VENUS:
						worksheetName = "Enum_ResetPerVersion(Venus)";
						break;
					case DeviceType.MERCURY:
						worksheetName = "Enum_ResetPerVersion(Mercury)";
						break;
					case DeviceType.JUPITER:
						worksheetName = "Enum_ResetPerVersion(Jupiter)";
						break;
					case DeviceType.GEMINI:
						worksheetName = "Enum_ResetPerVersion(Gemini)";
						break;
					}
					string text = base.GetCell(worksheetName, (int)index, 1 + versionColumnOffset).ToString();
					if (!string.IsNullOrEmpty(text))
					{
						int num = int.Parse(text);
						flag = (num == 1);
					}
					result = flag;
				}
			}
			return result;
		}

		public string GetVenusVersionFromCheckSum(uint venusChecksum)
		{
			string result = null;
			string value = Convert.ToString((long)((ulong)venusChecksum), 16).ToUpper();
			string text = base.GetCell("DBMain", 9, 5).ToString();
			int num = (!string.IsNullOrEmpty(text)) ? int.Parse(text) : -1;
			if (num > -1)
			{
				int num2 = 11;
				int num3 = num2 + num;
				for (int i = num3 - 1; i >= num2; i--)
				{
					string text2 = base.GetCell("DBMain", i, 6).ToString();
					if (!string.IsNullOrEmpty(text2) && text2.Equals(value))
					{
						result = base.GetCell("DBMain", i, 4).ToString();
						break;
					}
				}
			}
			return result;
		}

		public string GetVenusBuildFromCheckSum(uint venusChecksum)
		{
			string result = null;
			string value = Convert.ToString((long)((ulong)venusChecksum), 16).ToUpper();
			string text = base.GetCell("DBMain", 9, 5).ToString();
			int num = (!string.IsNullOrEmpty(text)) ? int.Parse(text) : -1;
			if (num > -1)
			{
				int num2 = 11;
				int num3 = num2 + num;
				for (int i = num3 - 1; i >= num2; i--)
				{
					string text2 = base.GetCell("DBMain", i, 6).ToString();
					if (!string.IsNullOrEmpty(text2) && text2.Equals(value))
					{
						result = base.GetCell("DBMain", i, 5).ToString();
						break;
					}
				}
			}
			return result;
		}
	}
}
