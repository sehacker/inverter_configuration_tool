using SEDevices.Devices;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using System;
using System.Collections.Generic;

namespace SEDevices.Upgrade.Base
{
	public interface ISECommUpgradable
	{
		void UpgradeState_VerifyCompliance(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList);

		void UpgradeState_SaveParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, uint? sufVersion);

		void UpgradeState_UpgradeSoftwareStatus(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData);

		void UpgradeState_VerifyVersionAfterUpgrade(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraDevice);

		void UpgradeState_ConfigureParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList);

		void UpgradeState_Finished(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion);

		void Command_Upgrade_Start(ushort bank);

		void Command_Upgrade_Write(byte[] data, int dataOffset, uint offset, int size);

		byte[] Command_Upgrade_Read_Data(int offset, int size);

		uint Command_Upgrade_Read_Size();

		void Command_Upgrade_Finish(uint checksum);
	}
}
