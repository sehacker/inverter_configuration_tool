using System;

namespace SEDevices.Upgrade.Base
{
	public interface ISEUpgradeNotifiable
	{
		void NotifyStart(int startState, int endState, int startProgressData, int endProgressData);

		void UpdateState(string text, int progress);

		void UpdateWritingData(int progress);

		void NotifyFinished(string message);
	}
}
