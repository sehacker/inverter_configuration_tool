using SEDevices.Upgrade.DeviceVersions.Base;
using System;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Info
{
	public class JupiterVersionInfo : DeviceVersionInfo
	{
		private uint _majorVersion;

		private uint _minorVersion;

		private uint _checksum;

		private uint? _buildVersion;

		[XmlElement("majorVersion")]
		public uint MajorVersion
		{
			get
			{
				return this._majorVersion;
			}
			set
			{
				this._majorVersion = value;
			}
		}

		[XmlElement("minorVersion")]
		public uint MinorVersion
		{
			get
			{
				return this._minorVersion;
			}
			set
			{
				this._minorVersion = value;
			}
		}

		[XmlElement("checksum")]
		public uint Checksum
		{
			get
			{
				return this._checksum;
			}
			set
			{
				this._checksum = value;
			}
		}

		[XmlElement("buildVersion")]
		public uint? BuildVersion
		{
			get
			{
				return this._buildVersion;
			}
			set
			{
				this._buildVersion = value;
			}
		}

		public bool Equals(JupiterVersionInfo jupiterVersion)
		{
			return jupiterVersion._majorVersion == this._majorVersion && jupiterVersion._minorVersion == this._minorVersion;
		}

		public override string ToString()
		{
			return this._majorVersion + "." + this._minorVersion;
		}
	}
}
