using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Compatibility
{
	public class PortiaCompatibilityItem : DeviceCompatbilityItem
	{
		private List<CompatiblePortiaVersion> _portiaVersions;

		private List<int> _restoreParams;

		private List<ParamaeterPair> _paramatersToSetBeforeUpgrade;

		private List<ParamaeterPair> _paramatersToSet;

		[XmlElement("portiaVersions")]
		public List<CompatiblePortiaVersion> PortiaVersions
		{
			get
			{
				return this._portiaVersions;
			}
			set
			{
				this._portiaVersions = value;
			}
		}

		[XmlElement("restoreParams")]
		public List<int> RestoreParams
		{
			get
			{
				return this._restoreParams;
			}
			set
			{
				this._restoreParams = value;
			}
		}

		[XmlElement("paramatersToSetBeforeUpgrade")]
		public List<ParamaeterPair> ParamatersToSetBeforeUpgrade
		{
			get
			{
				return this._paramatersToSetBeforeUpgrade;
			}
			set
			{
				this._paramatersToSetBeforeUpgrade = value;
			}
		}

		[XmlElement("paramatersToSet")]
		public List<ParamaeterPair> ParamatersToSet
		{
			get
			{
				return this._paramatersToSet;
			}
			set
			{
				this._paramatersToSet = value;
			}
		}
	}
}
