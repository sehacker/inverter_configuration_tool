using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Compatibility
{
	public class GeminiCompatibilityItem : DeviceCompatbilityItem
	{
		private List<GeminiVersionInfo> _geminiVersions;

		private List<int> _restoreParams;

		private List<ParamaeterPair> _paramatersToSet;

		[XmlElement("geminiVersionList")]
		public List<GeminiVersionInfo> GeminiVersions
		{
			get
			{
				return this._geminiVersions;
			}
			set
			{
				this._geminiVersions = value;
			}
		}

		[XmlElement("restoreParams")]
		public List<int> RestoreParams
		{
			get
			{
				return this._restoreParams;
			}
			set
			{
				this._restoreParams = value;
			}
		}

		[XmlElement("paramatersToSet")]
		public List<ParamaeterPair> ParamatersToSet
		{
			get
			{
				return this._paramatersToSet;
			}
			set
			{
				this._paramatersToSet = value;
			}
		}
	}
}
