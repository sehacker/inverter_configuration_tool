using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Compatibility
{
	public class CompatiblePortiaVersion
	{
		private PortiaVersionInfo _portiaVersion;

		private bool _allowRemoteUpgrade;

		[XmlElement("portiaVersion")]
		public PortiaVersionInfo PortiaVersion
		{
			get
			{
				return this._portiaVersion;
			}
			set
			{
				this._portiaVersion = value;
			}
		}

		[XmlElement("allowRemoteUpgrade")]
		public bool AllowRemoteUpgrade
		{
			get
			{
				return this._allowRemoteUpgrade;
			}
			set
			{
				this._allowRemoteUpgrade = value;
			}
		}
	}
}
