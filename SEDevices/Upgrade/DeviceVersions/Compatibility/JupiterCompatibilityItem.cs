using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Compatibility
{
	public class JupiterCompatibilityItem : DeviceCompatbilityItem
	{
		private List<JupiterVersionInfo> _jupiterVersions;

		private List<int> _restoreParams;

		private List<ParamaeterPair> _paramatersToSet;

		private bool? _setCountry;

		[XmlElement("jupiterVersionList")]
		public List<JupiterVersionInfo> JupiterVersions
		{
			get
			{
				return this._jupiterVersions;
			}
			set
			{
				this._jupiterVersions = value;
			}
		}

		[XmlElement("restoreParams")]
		public List<int> RestoreParams
		{
			get
			{
				return this._restoreParams;
			}
			set
			{
				this._restoreParams = value;
			}
		}

		[XmlElement("paramatersToSet")]
		public List<ParamaeterPair> ParamatersToSet
		{
			get
			{
				return this._paramatersToSet;
			}
			set
			{
				this._paramatersToSet = value;
			}
		}

		[XmlElement("setCountry")]
		public bool? SetCountry
		{
			get
			{
				return this._setCountry;
			}
			set
			{
				this._setCountry = value;
			}
		}
	}
}
