using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Compatibility
{
	public class JupiterPowerCompatibilityItem : DeviceCompatbilityItem
	{
		private List<JupiterPowerVersionInfo> _jupiterPowerVersions;

		private List<JupiterCompatibilityItem> _jupiterCompatability;

		[XmlElement("jupiterPowerVersionList")]
		public List<JupiterPowerVersionInfo> JupiterPowerVersions
		{
			get
			{
				return this._jupiterPowerVersions;
			}
			set
			{
				this._jupiterPowerVersions = value;
			}
		}

		[XmlElement("jupiterCompatabilityList")]
		public List<JupiterCompatibilityItem> JupiterCompatability
		{
			get
			{
				return this._jupiterCompatability;
			}
			set
			{
				this._jupiterCompatability = value;
			}
		}
	}
}
