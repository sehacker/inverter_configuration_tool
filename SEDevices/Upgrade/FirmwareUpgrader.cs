using SEDevices.Devices;
using SEDevices.Upgrade.Base;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Info;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace SEDevices.Upgrade
{
	public class FirmwareUpgrader
	{
		private const float PROGRESS_MIN = 0f;

		private const float PROGRESS_MAX = 100f;

		private SEThread _thUpgrade = null;

		private object _commDeviceToUpgradeSyncObject = new object();

		private SECommDevice[] _commDevicesToUpgrade;

		private object _upgradeNotifierSyncObject = new object();

		private ISEUpgradeNotifiable _upgradeNotifier = null;

		private UpgradeStateData _stateData = null;

		private string _fileName = "";

		private byte[] _fileData = null;

		private string _upgradeFilesDirectory = null;

		public SECommDevice[] CommDevices
		{
			get
			{
				SECommDevice[] commDevicesToUpgrade;
				lock (this._commDeviceToUpgradeSyncObject)
				{
					commDevicesToUpgrade = this._commDevicesToUpgrade;
				}
				return commDevicesToUpgrade;
			}
			set
			{
				lock (this._commDeviceToUpgradeSyncObject)
				{
					this._commDevicesToUpgrade = value;
				}
			}
		}

		public ISEUpgradeNotifiable UpgradeNotifier
		{
			get
			{
				ISEUpgradeNotifiable upgradeNotifier;
				lock (this._upgradeNotifierSyncObject)
				{
					upgradeNotifier = this._upgradeNotifier;
				}
				return upgradeNotifier;
			}
			set
			{
				lock (this._upgradeNotifierSyncObject)
				{
					this._upgradeNotifier = value;
				}
			}
		}

		public string UpgradeFileName
		{
			get
			{
				return this._fileName;
			}
			set
			{
				this._fileName = value;
			}
		}

		public byte[] UpgradeFileData
		{
			get
			{
				return this._fileData;
			}
			set
			{
				this._fileData = value;
			}
		}

		public string UpgradeFilesDirectory
		{
			get
			{
				return this._upgradeFilesDirectory;
			}
			set
			{
				this._upgradeFilesDirectory = value;
			}
		}

		public FirmwareUpgrader()
		{
			this.StartThreadControl();
		}

		private UpgradeRawData LoadBinaryFile(string fileName)
		{
			UpgradeRawData upgradeRawData = new UpgradeRawData();
			FileStream fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read);
			BinaryReader binaryReader = new BinaryReader(fileStream);
			int num = (int)fileStream.Length - 4;
			if (num < 0)
			{
				fileStream.Close();
				throw new Exception("Upgrade file size is too small");
			}
			upgradeRawData.Data = binaryReader.ReadBytes(num);
			uint num2 = binaryReader.ReadUInt32();
			binaryReader.Close();
			fileStream.Dispose();
			fileStream.Close();
			uint num3 = 0u;
			for (int i = 0; i < upgradeRawData.Data.Length; i += 2)
			{
				num3 += (uint)((int)upgradeRawData.Data[i] | (int)upgradeRawData.Data[i + 1] << 8);
			}
			if (num2 != num3)
			{
				throw new Exception("Upgrade file checksum is incorrect");
			}
			upgradeRawData.DataChecksum = new uint?(num3);
			return upgradeRawData;
		}

		private UpgradeRawData LoadTextualFile(string fileName)
		{
			UpgradeRawData upgradeRawData = new UpgradeRawData();
			int num = 0;
			StreamReader streamReader = new StreamReader(fileName);
			try
			{
				string value;
				while ((value = streamReader.ReadLine()) != null)
				{
					Convert.ToByte(value, 16);
					num++;
				}
			}
			catch (Exception var_4_39)
			{
				streamReader.Close();
				throw new Exception("Upgrade file checksum is incorrect");
			}
			streamReader.Close();
			streamReader.Dispose();
			streamReader = null;
			streamReader = new StreamReader(fileName);
			upgradeRawData.Data = new byte[num];
			num = 0;
			try
			{
				string value;
				while ((value = streamReader.ReadLine()) != null)
				{
					upgradeRawData.Data[num++] = Convert.ToByte(value, 16);
				}
			}
			catch (Exception var_4_39)
			{
				streamReader.Close();
				streamReader.Dispose();
				streamReader = null;
				throw new Exception("Upgrade file checksum is incorrect");
			}
			if (streamReader != null)
			{
				streamReader.Close();
				streamReader.Dispose();
				streamReader = null;
			}
			upgradeRawData.DataChecksum = null;
			return upgradeRawData;
		}

		public UpgradeRawData ReadUpgradeFile(string fileName, UpgradeFileType upgradeType)
		{
			UpgradeRawData result = new UpgradeRawData();
			switch (upgradeType)
			{
			case UpgradeFileType.BINARY:
				result = this.LoadBinaryFile(fileName);
				break;
			case UpgradeFileType.TEXTUAL:
				result = this.LoadTextualFile(fileName);
				break;
			}
			return result;
		}

		public void WriteSufFile(UpgradeXmlData upgradeXml, string fileName)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(UpgradeXmlData));
			TextWriter textWriter = new StreamWriter(fileName);
			xmlSerializer.Serialize(textWriter, upgradeXml);
			textWriter.Close();
			textWriter.Dispose();
		}

		public UpgradeXmlData ReadSufFile(string fileName)
		{
			FileStream fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read);
			UpgradeXmlData result = this.ReadSufFile(fileStream);
			fileStream.Close();
			fileStream.Dispose();
			return result;
		}

		public UpgradeXmlData ReadSufFile(byte[] fileData)
		{
			MemoryStream memoryStream = new MemoryStream(fileData);
			UpgradeXmlData result = this.ReadSufFile(memoryStream);
			memoryStream.Close();
			memoryStream.Dispose();
			return result;
		}

		private UpgradeXmlData ReadSufFile(Stream dataStream)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(UpgradeXmlData));
			return (UpgradeXmlData)xmlSerializer.Deserialize(dataStream);
		}

		private void UpgradePortiaSoftware(UpgradeXmlData upgradeXmlData, SEPortia portia)
		{
			this.Action_ReportStatus("Reading version data", 0);
			PortiaVersionInfo portiaVersion = upgradeXmlData.PortiaUpgradeScript.PortiaVersion;
			SWVersion deviceSWVersion = portia.DeviceSWVersion;
			PortiaVersionInfo portiaVersionInfo = new PortiaVersionInfo();
			portiaVersionInfo.MajorVersion = deviceSWVersion.Major;
			portiaVersionInfo.MinorVersion = deviceSWVersion.Minor;
			this.Action_ReportStatus("Reading state from file", 5);
			this._stateData = this.ReadPortiaUpgradeState(portia.ID, portiaVersion);
			if (this._stateData == null)
			{
				this.Action_ReportStatus("No state file found - creating a new one", 5);
				this._stateData = new UpgradeStateData();
				this._stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
			this.Action_ReportStatus("Upgrade state machine begins", 10);
			while (true)
			{
				switch (this._stateData.UpgradeState)
				{
				case UpgradeState.VerifyCompliance:
				{
					this.Action_ReportStatus("Stage 1/6 - Verifying compliance", 15);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(portiaVersionInfo);
					List<DeviceVersionInfo> list2 = new List<DeviceVersionInfo>(0);
					list2.Add(portiaVersion);
					List<DeviceCompatbilityItem> list3 = new List<DeviceCompatbilityItem>(0);
					int count = upgradeXmlData.PortiaUpgradeScript.PortiaCompatabilityList.Count;
					for (int i = 0; i < count; i++)
					{
						list3.Add(upgradeXmlData.PortiaUpgradeScript.PortiaCompatabilityList[i]);
					}
					portia.UpgradeState_VerifyCompliance(this._stateData, list, list2, list3);
					this.WritePortiaUpgradeState(portia.ID, portiaVersion, this._stateData);
					break;
				}
				case UpgradeState.SaveParamaeters:
				{
					this.Action_ReportStatus("Stage 2/6 - Saving special parameters", 30);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(portiaVersionInfo);
					portia.UpgradeState_SaveParameters(this._stateData, list, upgradeXmlData.SufVersion);
					this.WritePortiaUpgradeState(portia.ID, portiaVersion, this._stateData);
					break;
				}
				case UpgradeState.UpgradeSoftware:
					this.Action_ReportStatus("Stage 3/6 - Upgrading software", 40);
					try
					{
						portia.UpgradeState_UpgradeSoftwareStatus(this._stateData, upgradeXmlData);
					}
					catch (Exception ex)
					{
						this.WritePortiaUpgradeState(portia.ID, portiaVersion, this._stateData);
						throw ex;
					}
					this.WritePortiaUpgradeState(portia.ID, portiaVersion, this._stateData);
					break;
				case UpgradeState.VerifyVersionAfterUpgrade:
				{
					this.Action_ReportStatus("Stage 4/6 - Verifying new software", 80);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(portiaVersionInfo);
					list.Add(portiaVersion);
					portia.UpgradeState_VerifyVersionAfterUpgrade(this._stateData, list, null);
					this.WritePortiaUpgradeState(portia.ID, portiaVersion, this._stateData);
					break;
				}
				case UpgradeState.ConfigureParamaeters:
				{
					this.Action_ReportStatus("Stage 5/6 - Configuring saved parameters", 90);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(portiaVersionInfo);
					list.Add(portiaVersion);
					portia.UpgradeState_ConfigureParameters(this._stateData, list);
					this.WritePortiaUpgradeState(portia.ID, portiaVersion, this._stateData);
					break;
				}
				case UpgradeState.Finished:
					this.Action_ReportStatus("Stage 6/6 - Finishing Upgrade", 100);
					portia.UpgradeState_Finished(this._stateData, portiaVersionInfo, portiaVersion);
					this.WritePortiaUpgradeState(portia.ID, portiaVersion, this._stateData);
					if (this._stateData.UpgradeState == UpgradeState.Finished)
					{
						return;
					}
					break;
				}
			}
		}

		private void UpgradeVenusSoftware(UpgradeXmlData upgradeXmlData, SEPortia portia, SEVenus venus)
		{
			this.Action_ReportStatus("Reading version data", 0);
			VenusVersionInfo venusVersion = upgradeXmlData.VenusUpgradeScript.VenusVersion;
			SWVersion deviceSWVersion = venus.DeviceSWVersion;
			VenusVersionInfo venusVersionInfo = new VenusVersionInfo();
			venusVersionInfo.MajorVersion = deviceSWVersion.Major;
			venusVersionInfo.MinorVersion = deviceSWVersion.Minor;
			venusVersionInfo.BuildVersion = new uint?(deviceSWVersion.Build);
			venusVersionInfo.Checksum = deviceSWVersion.CheckSum.Value;
			SWVersion deviceSWVersionPower = venus.DeviceSWVersionPower;
			IseVersionInfo iseVersionInfo = new IseVersionInfo();
			iseVersionInfo.MajorVersion = deviceSWVersionPower.Major;
			iseVersionInfo.MinorVersion = deviceSWVersionPower.Minor;
			this.Action_ReportStatus("Reading state from file", 5);
			this._stateData = this.ReadVenusUpgradeState(venus.Address, venusVersion);
			if (this._stateData == null)
			{
				this.Action_ReportStatus("No state file found - creating a new one", 5);
				this._stateData = new UpgradeStateData();
				this._stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
			this.Action_ReportStatus("Upgrade state machine begins", 10);
			while (true)
			{
				switch (this._stateData.UpgradeState)
				{
				case UpgradeState.VerifyCompliance:
				{
					this.Action_ReportStatus("Stage 1/6 - Verifying compliance", 15);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(venusVersionInfo);
					list.Add(iseVersionInfo);
					List<DeviceVersionInfo> list2 = new List<DeviceVersionInfo>(0);
					list2.Add(venusVersion);
					List<DeviceCompatbilityItem> list3 = new List<DeviceCompatbilityItem>(0);
					int count = upgradeXmlData.VenusUpgradeScript.IseCompatability.Count;
					for (int i = 0; i < count; i++)
					{
						list3.Add(upgradeXmlData.VenusUpgradeScript.IseCompatability[i]);
					}
					venus.UpgradeState_VerifyCompliance(this._stateData, list, list2, list3);
					this.WriteVenusUpgradeState(venus.Address, venusVersion, this._stateData);
					break;
				}
				case UpgradeState.SaveParamaeters:
				{
					this.Action_ReportStatus("Stage 2/6 - Saving special parameters", 30);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(venusVersionInfo);
					list.Add(iseVersionInfo);
					venus.UpgradeState_SaveParameters(this._stateData, list, upgradeXmlData.SufVersion);
					this.WriteVenusUpgradeState(venus.Address, venusVersion, this._stateData);
					break;
				}
				case UpgradeState.UpgradeSoftware:
					this.Action_ReportStatus("Stage 3/6 - Upgrading software", 40);
					venus.UpgradeState_UpgradeSoftwareStatus(this._stateData, upgradeXmlData);
					this.WriteVenusUpgradeState(venus.Address, venusVersion, this._stateData);
					break;
				case UpgradeState.VerifyVersionAfterUpgrade:
				{
					this.Action_ReportStatus("Stage 4/6 - Verifying new software", 80);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(venusVersionInfo);
					list.Add(venusVersion);
					venus.UpgradeState_VerifyVersionAfterUpgrade(this._stateData, list, portia);
					this.WriteVenusUpgradeState(venus.Address, venusVersion, this._stateData);
					break;
				}
				case UpgradeState.ConfigureParamaeters:
				{
					this.Action_ReportStatus("Stage 5/6 - Configuring saved parameters", 90);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(venusVersionInfo);
					list.Add(venusVersion);
					venus.UpgradeState_ConfigureParameters(this._stateData, list);
					this.WriteVenusUpgradeState(venus.Address, venusVersion, this._stateData);
					break;
				}
				case UpgradeState.Finished:
					this.Action_ReportStatus("Stage 6/6 - Finishing Upgrade", 100);
					venus.UpgradeState_Finished(this._stateData, venusVersionInfo, venusVersion);
					this.WriteVenusUpgradeState(venus.Address, venusVersion, this._stateData);
					if (this._stateData.UpgradeState == UpgradeState.Finished)
					{
						return;
					}
					break;
				}
			}
		}

		private void UpgradeJupiterSoftware(UpgradeXmlData upgradeXmlData, SEPortia portia, SEJupiter jupiter)
		{
			this.Action_ReportStatus("Reading version data", 0);
			JupiterVersionInfo jupiterVersion = upgradeXmlData.JupiterUpgradeScript.JupiterVersion;
			SWVersion deviceSWVersion = jupiter.DeviceSWVersion;
			JupiterVersionInfo jupiterVersionInfo = new JupiterVersionInfo();
			jupiterVersionInfo.MajorVersion = deviceSWVersion.Major;
			jupiterVersionInfo.MinorVersion = deviceSWVersion.Minor;
			jupiterVersionInfo.BuildVersion = new uint?(deviceSWVersion.Build);
			jupiterVersionInfo.Checksum = deviceSWVersion.CheckSum.Value;
			SWVersion deviceSWVersionPower = jupiter.DeviceSWVersionPower;
			JupiterPowerVersionInfo jupiterPowerVersionInfo = new JupiterPowerVersionInfo();
			jupiterPowerVersionInfo.MajorVersion = deviceSWVersionPower.Major;
			jupiterPowerVersionInfo.MinorVersion = deviceSWVersionPower.Minor;
			this.Action_ReportStatus("Reading state from file", 5);
			this._stateData = this.ReadJupiterUpgradeState(jupiter.Address, jupiterVersion);
			if (this._stateData == null)
			{
				this.Action_ReportStatus("No state file found - creating a new one", 5);
				this._stateData = new UpgradeStateData();
				this._stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
			this.Action_ReportStatus("Upgrade state machine begins", 10);
			while (true)
			{
				switch (this._stateData.UpgradeState)
				{
				case UpgradeState.VerifyCompliance:
				{
					this.Action_ReportStatus("Stage 1/6 - Verifying compliance", 15);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(jupiterVersionInfo);
					list.Add(jupiterPowerVersionInfo);
					List<DeviceVersionInfo> list2 = new List<DeviceVersionInfo>(0);
					list2.Add(jupiterVersion);
					List<DeviceCompatbilityItem> list3 = new List<DeviceCompatbilityItem>(0);
					int count = upgradeXmlData.JupiterUpgradeScript.JupiterPowerCompatability.Count;
					for (int i = 0; i < count; i++)
					{
						list3.Add(upgradeXmlData.JupiterUpgradeScript.JupiterPowerCompatability[i]);
					}
					jupiter.UpgradeState_VerifyCompliance(this._stateData, list, list2, list3);
					this.WriteJupiterUpgradeState(jupiter.Address, jupiterVersion, this._stateData);
					break;
				}
				case UpgradeState.SaveParamaeters:
				{
					this.Action_ReportStatus("Stage 2/6 - Saving special parameters", 30);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(jupiterVersionInfo);
					list.Add(jupiterPowerVersionInfo);
					jupiter.UpgradeState_SaveParameters(this._stateData, list, upgradeXmlData.SufVersion);
					this.WriteJupiterUpgradeState(jupiter.Address, jupiterVersion, this._stateData);
					break;
				}
				case UpgradeState.UpgradeSoftware:
					this.Action_ReportStatus("Stage 3/6 - Upgrading software", 40);
					jupiter.UpgradeState_UpgradeSoftwareStatus(this._stateData, upgradeXmlData);
					this.WriteJupiterUpgradeState(jupiter.Address, jupiterVersion, this._stateData);
					break;
				case UpgradeState.VerifyVersionAfterUpgrade:
				{
					this.Action_ReportStatus("Stage 4/6 - Verifying new software", 80);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(jupiterVersionInfo);
					list.Add(jupiterVersion);
					jupiter.UpgradeState_VerifyVersionAfterUpgrade(this._stateData, list, portia);
					this.WriteJupiterUpgradeState(jupiter.Address, jupiterVersion, this._stateData);
					break;
				}
				case UpgradeState.ConfigureParamaeters:
				{
					this.Action_ReportStatus("Stage 5/6 - Configuring saved parameters", 90);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(jupiterVersionInfo);
					list.Add(jupiterVersion);
					jupiter.UpgradeState_ConfigureParameters(this._stateData, list);
					this.WriteJupiterUpgradeState(jupiter.Address, jupiterVersion, this._stateData);
					break;
				}
				case UpgradeState.Finished:
					this.Action_ReportStatus("Stage 6/6 - Finishing Upgrade", 100);
					jupiter.UpgradeState_Finished(this._stateData, jupiterVersionInfo, jupiterVersion);
					this.WriteJupiterUpgradeState(jupiter.Address, jupiterVersion, this._stateData);
					if (this._stateData.UpgradeState == UpgradeState.Finished)
					{
						return;
					}
					break;
				}
			}
		}

		private void UpgradeJupiterPowerSoftware(UpgradeXmlData upgradeXmlData, SEPortia portia, SEJupiter jupiter)
		{
			this.Action_ReportStatus("Reading version data", 0);
			JupiterPowerVersionInfo jupiterPowerVersion = upgradeXmlData.JupiterPowerUpgradeScript.JupiterPowerVersion;
			SWVersion deviceSWVersionPower = jupiter.DeviceSWVersionPower;
			JupiterPowerVersionInfo jupiterPowerVersionInfo = new JupiterPowerVersionInfo();
			jupiterPowerVersionInfo.MajorVersion = deviceSWVersionPower.Major;
			jupiterPowerVersionInfo.MinorVersion = deviceSWVersionPower.Minor;
			SWVersion deviceSWVersion = jupiter.DeviceSWVersion;
			JupiterVersionInfo jupiterVersionInfo = new JupiterVersionInfo();
			jupiterVersionInfo.MajorVersion = deviceSWVersion.Major;
			jupiterVersionInfo.MinorVersion = deviceSWVersion.Minor;
			this.Action_ReportStatus("Reading state from file", 5);
			this._stateData = this.ReadJupiterPowerUpgradeState(jupiter.Address, jupiterPowerVersion);
			if (this._stateData == null)
			{
				this.Action_ReportStatus("No state file found - creating a new one", 5);
				this._stateData = new UpgradeStateData();
				this._stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
			this.Action_ReportStatus("Upgrade state machine begins", 10);
			while (true)
			{
				switch (this._stateData.UpgradeState)
				{
				case UpgradeState.VerifyCompliance:
				{
					this.Action_ReportStatus("Stage 1/4 - Verifying compliance", 15);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(jupiterPowerVersionInfo);
					List<DeviceVersionInfo> list2 = new List<DeviceVersionInfo>(0);
					list2.Add(jupiterPowerVersion);
					jupiter.UpgradeState_VerifyCompliance_Power(this._stateData, list, list2, null);
					this.WriteJupiterPowerUpgradeState(jupiter.Address, jupiterPowerVersion, this._stateData);
					break;
				}
				case UpgradeState.UpgradeSoftware:
					this.Action_ReportStatus("Stage 2/4 - Upgrading software", 40);
					jupiter.UpgradeState_UpgradeSoftwareStatus_Power(this._stateData, upgradeXmlData);
					this.WriteJupiterPowerUpgradeState(jupiter.Address, jupiterPowerVersion, this._stateData);
					break;
				case UpgradeState.VerifyVersionAfterUpgrade:
				{
					this.Action_ReportStatus("Stage 3/4 - Verifying new software", 80);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(jupiterPowerVersionInfo);
					list.Add(jupiterPowerVersion);
					jupiter.UpgradeState_VerifyVersionAfterUpgrade_Power(this._stateData, list, portia);
					this.WriteJupiterPowerUpgradeState(jupiter.Address, jupiterPowerVersion, this._stateData);
					break;
				}
				case UpgradeState.Finished:
					this.Action_ReportStatus("Stage 4/4 - Finishing Upgrade", 100);
					jupiter.UpgradeState_Finished_Power(this._stateData, jupiterPowerVersionInfo, jupiterPowerVersion);
					this.WriteJupiterPowerUpgradeState(jupiter.Address, jupiterPowerVersion, this._stateData);
					if (this._stateData.UpgradeState == UpgradeState.Finished)
					{
						return;
					}
					break;
				}
			}
		}

		private void UpgradeMercurySoftware(UpgradeXmlData upgradeXmlData, SEPortia portia, SEMercury mercury)
		{
			this.Action_ReportStatus("Stage 1/1 - Upgarde for Mercury not supported", 100);
		}

		private void UpgradeGeminiSoftware(UpgradeXmlData upgradeXmlData, SEPortia portia, List<SEGemini> geminiList)
		{
			int count = geminiList.Count;
			for (int i = 0; i < count; i++)
			{
				this.UpgradeGeminiSoftware(upgradeXmlData, portia, geminiList[i]);
			}
		}

		private void UpgradeGeminiSoftware(UpgradeXmlData upgradeXmlData, SEPortia portia, SEGemini gemini)
		{
			this.Action_ReportStatus("Reading version data", 0);
			GeminiVersionInfo geminiVersion = upgradeXmlData.GeminiUpgradeScript.GeminiVersion;
			SWVersion deviceSWVersion = gemini.DeviceSWVersion;
			GeminiVersionInfo geminiVersionInfo = new GeminiVersionInfo();
			geminiVersionInfo.MajorVersion = deviceSWVersion.Major;
			geminiVersionInfo.MinorVersion = deviceSWVersion.Minor;
			this.Action_ReportStatus("Reading state from file", 5);
			this._stateData = this.ReadGeminiUpgradeState(gemini.Address, geminiVersion);
			if (this._stateData == null)
			{
				this.Action_ReportStatus("No state file found - creating a new one", 5);
				this._stateData = new UpgradeStateData();
				this._stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
			this.Action_ReportStatus("Upgrade state machine begins", 10);
			while (true)
			{
				switch (this._stateData.UpgradeState)
				{
				case UpgradeState.VerifyCompliance:
				{
					this.Action_ReportStatus("Stage 1/6 - Verifying compliance", 15);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(geminiVersionInfo);
					List<DeviceVersionInfo> list2 = new List<DeviceVersionInfo>(0);
					list2.Add(geminiVersion);
					List<DeviceCompatbilityItem> list3 = new List<DeviceCompatbilityItem>(0);
					int count = upgradeXmlData.GeminiUpgradeScript.GeminiCompatability.Count;
					for (int i = 0; i < count; i++)
					{
						list3.Add(upgradeXmlData.GeminiUpgradeScript.GeminiCompatability[i]);
					}
					gemini.UpgradeState_VerifyCompliance(this._stateData, list, list2, list3);
					this.WriteGeminiUpgradeState(gemini.Address, geminiVersion, this._stateData);
					break;
				}
				case UpgradeState.SaveParamaeters:
				{
					this.Action_ReportStatus("Stage 2/6 - Saving special parameters", 30);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(geminiVersionInfo);
					gemini.UpgradeState_SaveParameters(this._stateData, list, upgradeXmlData.SufVersion);
					this.WriteGeminiUpgradeState(gemini.Address, geminiVersion, this._stateData);
					break;
				}
				case UpgradeState.UpgradeSoftware:
					this.Action_ReportStatus("Stage 3/6 - Upgrading software", 40);
					gemini.UpgradeState_UpgradeSoftwareStatus(this._stateData, upgradeXmlData);
					this.WriteGeminiUpgradeState(gemini.Address, geminiVersion, this._stateData);
					break;
				case UpgradeState.VerifyVersionAfterUpgrade:
				{
					this.Action_ReportStatus("Stage 4/6 - Verifying new software", 80);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(geminiVersionInfo);
					list.Add(geminiVersion);
					gemini.UpgradeState_VerifyVersionAfterUpgrade(this._stateData, list, portia);
					this.WriteGeminiUpgradeState(gemini.Address, geminiVersion, this._stateData);
					break;
				}
				case UpgradeState.ConfigureParamaeters:
				{
					this.Action_ReportStatus("Stage 5/6 - Configuring saved parameters", 90);
					List<DeviceVersionInfo> list = new List<DeviceVersionInfo>(0);
					list.Add(geminiVersionInfo);
					list.Add(geminiVersion);
					gemini.UpgradeState_ConfigureParameters(this._stateData, list);
					this.WriteGeminiUpgradeState(gemini.Address, geminiVersion, this._stateData);
					break;
				}
				case UpgradeState.Finished:
					this.Action_ReportStatus("Stage 6/6 - Finishing Upgrade", 100);
					gemini.UpgradeState_Finished(this._stateData, geminiVersionInfo, geminiVersion);
					this.WriteGeminiUpgradeState(gemini.Address, geminiVersion, this._stateData);
					if (this._stateData.UpgradeState == UpgradeState.Finished)
					{
						return;
					}
					break;
				}
			}
		}

		public UpgradeStateData ReadUpgradeStateData(long serialNumber)
		{
			bool flag = !string.IsNullOrEmpty(this.UpgradeFileName) && File.Exists(this.UpgradeFileName);
			bool flag2 = this.UpgradeFileData != null && this.UpgradeFileData.Length > 0;
			bool flag3 = !flag && !flag2;
			if (flag3)
			{
				throw new Exception("ERROR: No Upgrade File to upgrade!");
			}
			UpgradeXmlData upgradeXmlData = (!string.IsNullOrEmpty(this.UpgradeFileName)) ? this.ReadSufFile(this.UpgradeFileName) : this.ReadSufFile(this.UpgradeFileData);
			if (upgradeXmlData == null)
			{
				throw new Exception("ERROR: Cannot convert XML data!");
			}
			switch (upgradeXmlData.SoftwareType)
			{
			case SoftwareType.Venus:
			{
				VenusVersionInfo venusVersion = upgradeXmlData.VenusUpgradeScript.VenusVersion;
				UpgradeStateData result = this.ReadVenusUpgradeState((uint)serialNumber, venusVersion);
				return result;
			}
			case SoftwareType.Portia:
			{
				PortiaVersionInfo portiaVersion = upgradeXmlData.PortiaUpgradeScript.PortiaVersion;
				UpgradeStateData result = this.ReadPortiaUpgradeState((uint)serialNumber, portiaVersion);
				return result;
			}
			case SoftwareType.Jupiter:
			{
				JupiterVersionInfo jupiterVersion = upgradeXmlData.JupiterUpgradeScript.JupiterVersion;
				UpgradeStateData result = this.ReadJupiterUpgradeState((uint)serialNumber, jupiterVersion);
				return result;
			}
			case SoftwareType.JupiterPower:
			{
				JupiterPowerVersionInfo jupiterPowerVersion = upgradeXmlData.JupiterPowerUpgradeScript.JupiterPowerVersion;
				UpgradeStateData result = this.ReadJupiterPowerUpgradeState((uint)serialNumber, jupiterPowerVersion);
				return result;
			}
			case SoftwareType.Gemini:
			{
				GeminiVersionInfo geminiVersion = upgradeXmlData.GeminiUpgradeScript.GeminiVersion;
				UpgradeStateData result = this.ReadGeminiUpgradeState((uint)serialNumber, geminiVersion);
				return result;
			}
			}
			throw new Exception("Unsupported software type - " + upgradeXmlData.SoftwareType.ToString());
		}

		private UpgradeStateData ReadPortiaUpgradeState(uint id, PortiaVersionInfo portiaVersion)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				portiaVersion.MajorVersion,
				"_",
				portiaVersion.MinorVersion,
				".usd"
			});
			return this.ReadDeviceUpgradeState(fileName);
		}

		private UpgradeStateData ReadVenusUpgradeState(uint id, VenusVersionInfo venusVersion)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				venusVersion.MajorVersion,
				"_",
				venusVersion.MinorVersion,
				"_",
				venusVersion.Checksum,
				".usd"
			});
			return this.ReadDeviceUpgradeState(fileName);
		}

		private UpgradeStateData ReadMercuryUpgradeState(uint id, MercuryVersionInfo mercuryVersion)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				mercuryVersion.MajorVersion,
				"_",
				mercuryVersion.MinorVersion,
				".usd"
			});
			return this.ReadDeviceUpgradeState(fileName);
		}

		private UpgradeStateData ReadJupiterUpgradeState(uint id, JupiterVersionInfo jupiterVersion)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				jupiterVersion.MajorVersion,
				"_",
				jupiterVersion.MinorVersion,
				".usd"
			});
			return this.ReadDeviceUpgradeState(fileName);
		}

		private UpgradeStateData ReadJupiterPowerUpgradeState(uint id, JupiterPowerVersionInfo jupiterPowerVersion)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				jupiterPowerVersion.MajorVersion,
				"_",
				jupiterPowerVersion.MinorVersion,
				".usd"
			});
			return this.ReadDeviceUpgradeState(fileName);
		}

		private UpgradeStateData ReadGeminiUpgradeState(uint id, GeminiVersionInfo geminiVersion)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				geminiVersion.MajorVersion,
				"_",
				geminiVersion.MinorVersion,
				".usd"
			});
			return this.ReadDeviceUpgradeState(fileName);
		}

		private UpgradeStateData ReadDeviceUpgradeState(string fileName)
		{
			UpgradeStateData upgradeStateData = null;
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(UpgradeStateData));
			TextReader textReader = null;
			UpgradeStateData result;
			try
			{
				if (this.UpgradeFilesDirectory != null && Directory.Exists(this.UpgradeFilesDirectory))
				{
					fileName = Path.Combine(this.UpgradeFilesDirectory, fileName);
					textReader = new StreamReader(fileName);
					upgradeStateData = (UpgradeStateData)xmlSerializer.Deserialize(textReader);
					textReader.Close();
				}
			}
			catch (Exception var_3_63)
			{
				result = null;
				return result;
			}
			finally
			{
				if (textReader != null)
				{
					textReader.Close();
					textReader.Dispose();
					textReader = null;
				}
			}
			result = upgradeStateData;
			return result;
		}

		private void WritePortiaUpgradeState(uint id, PortiaVersionInfo portiaVersion, UpgradeStateData upgradeStateData)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				portiaVersion.MajorVersion,
				"_",
				portiaVersion.MinorVersion,
				".usd"
			});
			string fileName2 = string.Concat(new object[]
			{
				"0_",
				portiaVersion.MajorVersion,
				"_",
				portiaVersion.MinorVersion,
				".usd"
			});
			this.WriteDeviceUpgradeState(fileName, upgradeStateData);
			this.WriteDeviceUpgradeState(fileName2, upgradeStateData);
		}

		private void WriteVenusUpgradeState(uint id, VenusVersionInfo venusVersion, UpgradeStateData upgradeStateData)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				venusVersion.MajorVersion,
				"_",
				venusVersion.MinorVersion,
				"_",
				venusVersion.Checksum,
				".usd"
			});
			string fileName2 = string.Concat(new object[]
			{
				"0_",
				venusVersion.MajorVersion,
				"_",
				venusVersion.MinorVersion,
				"_",
				venusVersion.Checksum,
				".usd"
			});
			this.WriteDeviceUpgradeState(fileName, upgradeStateData);
			this.WriteDeviceUpgradeState(fileName2, upgradeStateData);
		}

		private void WriteMercuryUpgradeState(uint id, MercuryVersionInfo mercuryVersion, UpgradeStateData upgradeStateData)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				mercuryVersion.MajorVersion,
				"_",
				mercuryVersion.MinorVersion,
				".usd"
			});
			string fileName2 = string.Concat(new object[]
			{
				"0_",
				mercuryVersion.MajorVersion,
				"_",
				mercuryVersion.MinorVersion,
				".usd"
			});
			this.WriteDeviceUpgradeState(fileName, upgradeStateData);
			this.WriteDeviceUpgradeState(fileName2, upgradeStateData);
		}

		private void WriteJupiterUpgradeState(uint id, JupiterVersionInfo jupiterVersion, UpgradeStateData upgradeStateData)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				jupiterVersion.MajorVersion,
				"_",
				jupiterVersion.MinorVersion,
				".usd"
			});
			string fileName2 = string.Concat(new object[]
			{
				"0_",
				jupiterVersion.MajorVersion,
				"_",
				jupiterVersion.MinorVersion,
				".usd"
			});
			this.WriteDeviceUpgradeState(fileName, upgradeStateData);
			this.WriteDeviceUpgradeState(fileName2, upgradeStateData);
		}

		private void WriteJupiterPowerUpgradeState(uint id, JupiterPowerVersionInfo jupiterPowerVersion, UpgradeStateData upgradeStateData)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				jupiterPowerVersion.MajorVersion,
				"_",
				jupiterPowerVersion.MinorVersion,
				".usd"
			});
			string fileName2 = string.Concat(new object[]
			{
				"0_",
				jupiterPowerVersion.MajorVersion,
				"_",
				jupiterPowerVersion.MinorVersion,
				".usd"
			});
			this.WriteDeviceUpgradeState(fileName, upgradeStateData);
			this.WriteDeviceUpgradeState(fileName2, upgradeStateData);
		}

		private void WriteGeminiUpgradeState(uint id, GeminiVersionInfo geminiVersion, UpgradeStateData upgradeStateData)
		{
			string fileName = string.Concat(new object[]
			{
				id.ToString(),
				"_",
				geminiVersion.MajorVersion,
				"_",
				geminiVersion.MinorVersion,
				".usd"
			});
			string fileName2 = string.Concat(new object[]
			{
				"0_",
				geminiVersion.MajorVersion,
				"_",
				geminiVersion.MinorVersion,
				".usd"
			});
			this.WriteDeviceUpgradeState(fileName, upgradeStateData);
			this.WriteDeviceUpgradeState(fileName2, upgradeStateData);
		}

		private void WriteDeviceUpgradeState(string fileName, UpgradeStateData upgradeStateData)
		{
			if (this.UpgradeFilesDirectory != null)
			{
				XmlSerializer xmlSerializer = new XmlSerializer(typeof(UpgradeStateData));
				if (!Directory.Exists(this.UpgradeFilesDirectory))
				{
					Directory.CreateDirectory(this.UpgradeFilesDirectory);
				}
				fileName = Path.Combine(this.UpgradeFilesDirectory, fileName);
				TextWriter textWriter = new StreamWriter(fileName);
				xmlSerializer.Serialize(textWriter, upgradeStateData);
				textWriter.Close();
			}
		}

		public byte[] ReadProgram(SEPortia portia)
		{
			uint num = portia.Command_Upgrade_Read_Size();
			int i = 0;
			int num2 = 40;
			byte[] array = new byte[num];
			while (i < array.Length)
			{
				int num3 = Math.Min(array.Length - i, (int)portia.DeviceMaxDataLength);
				byte[] array2 = portia.Command_Upgrade_Read_Data(i, num3);
				array2.CopyTo(array, i);
				i += num3;
				int incrementSize = (int)((double)i / (double)array.Length * (double)num2);
				this.Action_ReportStatus(incrementSize);
			}
			return array;
		}

		public void ReadProgram(string fileName, SEPortia portia)
		{
			byte[] array = this.ReadProgram(portia);
			FileStream output = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite);
			BinaryWriter binaryWriter = new BinaryWriter(output);
			binaryWriter.Write(array);
			uint num = 0u;
			for (int i = 0; i < array.Length; i += 2)
			{
				num += (uint)((int)array[i] | (int)array[i + 1] << 8);
			}
			binaryWriter.Write(num);
			binaryWriter.Close();
		}

		private void StartThreadControl()
		{
			this._thUpgrade = new SEThread("Upgrade Thread", new SEThread.ThreadFunctionCallBack(this.UpgradeThreadFunc), 500, 0u, 0u, true, true, true);
		}

		public void StopThreadControl()
		{
			try
			{
				if (this._thUpgrade != null)
				{
					this._thUpgrade.Stop = true;
					this._thUpgrade.Dispose();
				}
			}
			catch (Exception var_0_2D)
			{
			}
			this._thUpgrade = null;
		}

		private void UpgradeThreadFunc()
		{
			try
			{
				this.UpgradeNotifier.NotifyStart(0, 100, 0, 100);
				SECommDevice[] commDevices = this.CommDevices;
				this.HandleUpgrade(commDevices);
				this.UpgradeNotifier.NotifyFinished("Upgrade Complete");
				this._thUpgrade.Pause = true;
			}
			catch (Exception ex)
			{
				this.Action_ReportStatus(ex.Message, -1);
				this.UpgradeNotifier.NotifyFinished(ex.Message);
				this._thUpgrade.Pause = true;
			}
		}

		public void Action_SetUpgradeStatus(bool shouldStart)
		{
			this._thUpgrade.Pause = !shouldStart;
		}

		private void Action_ReportException(string status)
		{
			this.Action_ReportStatus(status, -1);
		}

		private void Action_ReportStatus(string status)
		{
			this.Action_ReportStatus(status, -1);
		}

		private void Action_ReportStatus(int incrementSize)
		{
			this.Action_ReportStatus(null, incrementSize);
		}

		private void Action_ReportStatus(string status, int incrementSize)
		{
			string text = null;
			if (status != null)
			{
				DateTime now = DateTime.Now;
				text = string.Format("[{0:00}:{1:00}:{2:00}] " + status, now.Hour, now.Minute, now.Second);
			}
			this.UpgradeNotifier.UpdateState(text, incrementSize);
		}

		private void HandleUpgrade(SECommDevice[] commDevices)
		{
			this.Action_ReportStatus("Starting Pre-Upgrade checks", 0);
			try
			{
				bool flag = !string.IsNullOrEmpty(this.UpgradeFileName) && File.Exists(this.UpgradeFileName);
				bool flag2 = this.UpgradeFileData != null && this.UpgradeFileData.Length > 0;
				bool flag3 = !flag && !flag2;
				if (flag3)
				{
					throw new Exception("ERROR: No Upgrade File to upgrade!");
				}
				UpgradeXmlData upgradeXmlData = (!string.IsNullOrEmpty(this.UpgradeFileName)) ? this.ReadSufFile(this.UpgradeFileName) : this.ReadSufFile(this.UpgradeFileData);
				if (upgradeXmlData == null)
				{
					throw new Exception("ERROR: Cannot convert XML data!");
				}
				this.Action_ReportStatus("Upgrade File (*.suf) loaded", 0);
				switch (upgradeXmlData.SoftwareType)
				{
				case SoftwareType.Venus:
				{
					SEVenus sEVenus = null;
					SEPortia sEPortia = null;
					try
					{
						sEPortia = (SEPortia)commDevices[0];
						sEVenus = (SEVenus)commDevices[1];
					}
					catch (Exception ex)
					{
						throw new Exception("ERROR: Trying to upgrade DSP1 to a Non-DSP1 device", ex);
					}
					this.Action_ReportStatus("Starting DSP1 Upgrade.", 0);
					sEPortia.UpgradeNotifier = this.UpgradeNotifier;
					sEVenus.UpgradeNotifier = this.UpgradeNotifier;
					this.UpgradeVenusSoftware(upgradeXmlData, sEPortia, sEVenus);
					goto IL_37A;
				}
				case SoftwareType.Portia:
				{
					this.Action_ReportStatus("Starting Portia upgrade", 0);
					SEPortia sEPortia = (SEPortia)commDevices[0];
					sEPortia.UpgradeNotifier = this.UpgradeNotifier;
					this.UpgradePortiaSoftware(upgradeXmlData, sEPortia);
					goto IL_37A;
				}
				case SoftwareType.Jupiter:
				{
					SEJupiter sEJupiter = null;
					SEPortia sEPortia = null;
					try
					{
						sEPortia = (SEPortia)commDevices[0];
						sEJupiter = (SEJupiter)commDevices[1];
					}
					catch (Exception ex)
					{
						throw new Exception("ERROR: Trying to upgrade DSP1 to a Non-DSP1 device", ex);
					}
					this.Action_ReportStatus("Starting DSP1 Upgrade.", 0);
					sEPortia.UpgradeNotifier = this.UpgradeNotifier;
					sEJupiter.UpgradeNotifier = this.UpgradeNotifier;
					this.UpgradeJupiterSoftware(upgradeXmlData, sEPortia, sEJupiter);
					goto IL_37A;
				}
				case SoftwareType.JupiterPower:
				{
					SEJupiter sEJupiter = null;
					SEPortia sEPortia = null;
					try
					{
						sEPortia = (SEPortia)commDevices[0];
						sEJupiter = (SEJupiter)commDevices[1];
					}
					catch (Exception ex)
					{
						throw new Exception("ERROR: Trying to upgrade DSP1 to a Non-DSP1 device", ex);
					}
					this.Action_ReportStatus("Starting DSP1 Upgrade.", 0);
					sEPortia.UpgradeNotifier = this.UpgradeNotifier;
					sEJupiter.UpgradeNotifier = this.UpgradeNotifier;
					this.UpgradeJupiterPowerSoftware(upgradeXmlData, sEPortia, sEJupiter);
					goto IL_37A;
				}
				case SoftwareType.Gemini:
				{
					List<SEGemini> list = null;
					SEPortia sEPortia = null;
					try
					{
						sEPortia = (SEPortia)commDevices[0];
						sEPortia.UpgradeNotifier = this.UpgradeNotifier;
						int num = commDevices.Length;
						for (int i = 1; i < num; i++)
						{
							SEGemini sEGemini = (SEGemini)commDevices[i];
							sEGemini.UpgradeNotifier = this.UpgradeNotifier;
							list.Add(sEGemini);
						}
					}
					catch (Exception ex)
					{
						throw new Exception("ERROR: Trying to upgrade DSP1 to a Non-DSP1 device", ex);
					}
					this.Action_ReportStatus("Starting Combi DSP Upgrade.", 0);
					this.UpgradeGeminiSoftware(upgradeXmlData, sEPortia, list);
					goto IL_37A;
				}
				}
				switch (upgradeXmlData.SoftwareType)
				{
				}
				throw new Exception("Unsupported software type - " + upgradeXmlData.SoftwareType.ToString());
				IL_37A:;
			}
			catch (Exception ex)
			{
				this.Action_ReportStatus(ex.Message);
				throw ex;
			}
		}
	}
}
