using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class VenusUpgradeScript
	{
		private List<IseCompatibilityItem> _iseCompatability;

		private VenusVersionInfo _venusVersion;

		[XmlElement("iseCompatabilityList")]
		public List<IseCompatibilityItem> IseCompatability
		{
			get
			{
				return this._iseCompatability;
			}
			set
			{
				this._iseCompatability = value;
			}
		}

		[XmlElement("venusVersion")]
		public VenusVersionInfo VenusVersion
		{
			get
			{
				return this._venusVersion;
			}
			set
			{
				this._venusVersion = value;
			}
		}
	}
}
