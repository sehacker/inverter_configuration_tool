using System;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class UpgradeRawData
	{
		private byte[] _data;

		private uint? _dataChecksum;

		[XmlElement("data")]
		public byte[] Data
		{
			get
			{
				return this._data;
			}
			set
			{
				this._data = value;
			}
		}

		[XmlElement("checksum")]
		public uint? DataChecksum
		{
			get
			{
				return this._dataChecksum;
			}
			set
			{
				this._dataChecksum = value;
			}
		}
	}
}
