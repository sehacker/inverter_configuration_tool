using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class JupiterPowerUpgradeScript
	{
		private JupiterPowerVersionInfo _jupiterPowerVersion;

		private List<JupiterPowerCompatibilityItem> _jupiterPowerCompatability;

		[XmlElement("jupiterPowerVersion")]
		public JupiterPowerVersionInfo JupiterPowerVersion
		{
			get
			{
				return this._jupiterPowerVersion;
			}
			set
			{
				this._jupiterPowerVersion = value;
			}
		}

		[XmlElement("jupiterPowerCompatabilityList")]
		public List<JupiterPowerCompatibilityItem> JupiterPowerCompatability
		{
			get
			{
				return this._jupiterPowerCompatability;
			}
			set
			{
				this._jupiterPowerCompatability = value;
			}
		}
	}
}
