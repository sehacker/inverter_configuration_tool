using System;

namespace SEDevices.Upgrade.Data
{
	public enum SoftwareType
	{
		ISE,
		Venus,
		Portia,
		Jupiter,
		JupiterPower,
		Mercury,
		Gemini,
		COUNT
	}
}
