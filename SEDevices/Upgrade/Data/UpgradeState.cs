using System;

namespace SEDevices.Upgrade.Data
{
	public enum UpgradeState
	{
		VerifyCompliance,
		SaveParamaeters,
		UpgradeSoftware,
		VerifyVersionAfterUpgrade,
		ConfigureParamaeters,
		Finished
	}
}
