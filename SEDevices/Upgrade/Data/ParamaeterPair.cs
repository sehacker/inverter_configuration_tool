using System;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class ParamaeterPair
	{
		private int _index;

		private string _value;

		[XmlElement("index")]
		public int Index
		{
			get
			{
				return this._index;
			}
			set
			{
				this._index = value;
			}
		}

		[XmlElement("value")]
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		public ParamaeterPair()
		{
		}

		public ParamaeterPair(int index, string value)
		{
			this._index = index;
			this._value = value;
		}
	}
}
