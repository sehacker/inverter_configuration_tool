using System;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	[XmlRoot("upgradeData")]
	public class UpgradeXmlData
	{
		private UpgradeRawData _upgradeData;

		private SoftwareType _softwareType;

		private PortiaUpgradeScript _portiaUpgradeScript;

		private VenusUpgradeScript _venusUpgradeScript;

		private MercuryUpgradeScript _mercuryUpgradeScript;

		private JupiterUpgradeScript _jupiterUpgradeScript;

		private JupiterPowerUpgradeScript _jupiterPowerUpgradeScript;

		private GeminiUpgradeScript _geminiUpgradeScript;

		private uint? _sufVersion;

		[XmlElement("upgradeContent")]
		public UpgradeRawData UpgradeData
		{
			get
			{
				return this._upgradeData;
			}
			set
			{
				this._upgradeData = value;
			}
		}

		[XmlElement("softwareType")]
		public SoftwareType SoftwareType
		{
			get
			{
				return this._softwareType;
			}
			set
			{
				this._softwareType = value;
			}
		}

		[XmlElement("portiaUpgradeScript")]
		public PortiaUpgradeScript PortiaUpgradeScript
		{
			get
			{
				return this._portiaUpgradeScript;
			}
			set
			{
				this._portiaUpgradeScript = value;
			}
		}

		[XmlElement("venusUpgradeScript")]
		public VenusUpgradeScript VenusUpgradeScript
		{
			get
			{
				return this._venusUpgradeScript;
			}
			set
			{
				this._venusUpgradeScript = value;
			}
		}

		[XmlElement("mercuryUpgradeScript")]
		public MercuryUpgradeScript MercuryUpgradeScript
		{
			get
			{
				return this._mercuryUpgradeScript;
			}
			set
			{
				this._mercuryUpgradeScript = value;
			}
		}

		[XmlElement("jupiterUpgradeScript")]
		public JupiterUpgradeScript JupiterUpgradeScript
		{
			get
			{
				return this._jupiterUpgradeScript;
			}
			set
			{
				this._jupiterUpgradeScript = value;
			}
		}

		[XmlElement("jupiterPowerUpgradeScript")]
		public JupiterPowerUpgradeScript JupiterPowerUpgradeScript
		{
			get
			{
				return this._jupiterPowerUpgradeScript;
			}
			set
			{
				this._jupiterPowerUpgradeScript = value;
			}
		}

		[XmlElement("geminiUpgradeScript")]
		public GeminiUpgradeScript GeminiUpgradeScript
		{
			get
			{
				return this._geminiUpgradeScript;
			}
			set
			{
				this._geminiUpgradeScript = value;
			}
		}

		[XmlElement("sufVersion")]
		public uint? SufVersion
		{
			get
			{
				return this._sufVersion;
			}
		}
	}
}
