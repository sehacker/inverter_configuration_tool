using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class GeminiUpgradeScript
	{
		private GeminiVersionInfo _geminiVersion;

		private List<GeminiCompatibilityItem> _geminiCompatability;

		[XmlElement("geminiVersion")]
		public GeminiVersionInfo GeminiVersion
		{
			get
			{
				return this._geminiVersion;
			}
			set
			{
				this._geminiVersion = value;
			}
		}

		[XmlElement("geminiCompatabilityList")]
		public List<GeminiCompatibilityItem> GeminiCompatability
		{
			get
			{
				return this._geminiCompatability;
			}
			set
			{
				this._geminiCompatability = value;
			}
		}
	}
}
