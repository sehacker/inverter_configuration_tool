using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class MercuryUpgradeScript
	{
		private MercuryVersionInfo _mercuryVersion;

		private List<MercuryCompatibilityItem> _mercuryCompatability;

		[XmlElement("mercuryVersion")]
		public MercuryVersionInfo MercuryVersion
		{
			get
			{
				return this._mercuryVersion;
			}
			set
			{
				this._mercuryVersion = value;
			}
		}

		[XmlElement("mercuryCompatabilityList")]
		public List<MercuryCompatibilityItem> MercuryCompatability
		{
			get
			{
				return this._mercuryCompatability;
			}
			set
			{
				this._mercuryCompatability = value;
			}
		}
	}
}
