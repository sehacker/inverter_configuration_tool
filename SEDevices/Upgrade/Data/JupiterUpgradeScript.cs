using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class JupiterUpgradeScript
	{
		private JupiterVersionInfo _jupiterVersion;

		private List<JupiterPowerCompatibilityItem> _jupiterPowerCompatability;

		[XmlElement("jupiterVersion")]
		public JupiterVersionInfo JupiterVersion
		{
			get
			{
				return this._jupiterVersion;
			}
			set
			{
				this._jupiterVersion = value;
			}
		}

		[XmlElement("jupiterPowerCompatabilityList")]
		public List<JupiterPowerCompatibilityItem> JupiterPowerCompatability
		{
			get
			{
				return this._jupiterPowerCompatability;
			}
			set
			{
				this._jupiterPowerCompatability = value;
			}
		}
	}
}
