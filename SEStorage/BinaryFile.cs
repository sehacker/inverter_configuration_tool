using SEStorage.Base;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SEStorage
{
	public class BinaryFile : StorageItem
	{
		private const string DEFAULT_FILE_TYPE = "bin";

		public BinaryFile()
		{
			this._defaultFileExtention = "bin";
		}

		protected override void SaveFileProcess(object obj, string path)
		{
			FileStream fileStream = null;
			try
			{
				fileStream = File.Open(path, FileMode.Create, FileAccess.ReadWrite);
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				binaryFormatter.Serialize(fileStream, obj);
				fileStream.Close();
			}
			catch (Exception var_2_26)
			{
				if (fileStream != null)
				{
					fileStream.Close();
					fileStream = null;
				}
			}
		}

		protected override object OpenFileProcess(string path, object[] extraData)
		{
			object result = null;
			FileStream fileStream = null;
			try
			{
				if (File.Exists(path))
				{
					fileStream = File.Open(path, FileMode.Open, FileAccess.Read);
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					result = binaryFormatter.Deserialize(fileStream);
					fileStream.Close();
				}
			}
			catch (Exception var_3_38)
			{
				if (fileStream != null)
				{
					fileStream.Close();
					fileStream = null;
				}
			}
			return result;
		}
	}
}
