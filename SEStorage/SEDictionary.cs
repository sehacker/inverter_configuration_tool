using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SEStorage
{
	public class SEDictionary : DBExcel
	{
		private const string WS_TRANSLATIONS = "TRANSLATIONS";

		private const string WS_LANGUAGES = "LANGUAGES";

		private const string EOF = "*/";

		private string _selectedLanguage = "";

		private Dictionary<string, int> _keyToRowTable = null;

		private Dictionary<string, int> _langToColumnTable = null;

		public string SelectedLanguage
		{
			get
			{
				return this._selectedLanguage;
			}
			set
			{
				this._selectedLanguage = value;
			}
		}

		public string this[string key]
		{
			get
			{
				return this.GetTranslation(key);
			}
		}

		public SEDictionary(Stream fileStream) : base(fileStream, FileTypes.XLSX)
		{
			this.DictionaryInit();
		}

		public SEDictionary(string filePath) : base(filePath, FileTypes.XLSX)
		{
			this.DictionaryInit();
		}

		private void DictionaryInit()
		{
			this.UpdateLanguageList();
			this.UpdateTranslations();
		}

		private void UpdateLanguageList()
		{
			if (this._langToColumnTable == null)
			{
				this._langToColumnTable = new Dictionary<string, int>(0);
			}
			this._langToColumnTable.Clear();
			int num = 1;
			while (true)
			{
				object cell = base.GetCell("LANGUAGES", num, 0);
				object cell2 = base.GetCell("LANGUAGES", num, 1);
				if (cell != null && cell.ToString().StartsWith("*/"))
				{
					break;
				}
				if (cell2 != null && !string.IsNullOrEmpty(cell2.ToString()) && !this._langToColumnTable.ContainsKey(cell2.ToString()))
				{
					this._langToColumnTable.Add(cell2.ToString(), num);
				}
				num++;
			}
		}

		private void UpdateTranslations()
		{
			if (this._langToColumnTable != null && this._langToColumnTable.Count > 0)
			{
				if (this._keyToRowTable == null)
				{
					this._keyToRowTable = new Dictionary<string, int>(0);
				}
				this._keyToRowTable.Clear();
				int num = -1;
				while (true)
				{
					num++;
					object cell = base.GetCell("TRANSLATIONS", num, 0);
					if (cell != null)
					{
						string text = cell.ToString();
						if (text.StartsWith("*/"))
						{
							break;
						}
						if (!text.StartsWith("//"))
						{
							if (!string.IsNullOrEmpty(text))
							{
								if (!this._keyToRowTable.ContainsKey(text))
								{
									this._keyToRowTable.Add(text, num);
								}
							}
						}
					}
				}
			}
		}

		private string GetTranslation(string key)
		{
			string selectedLanguage = this.SelectedLanguage;
			string result;
			if (string.IsNullOrEmpty(selectedLanguage))
			{
				result = key;
			}
			else if (!this._langToColumnTable.ContainsKey(selectedLanguage))
			{
				result = key;
			}
			else
			{
				int column = this._langToColumnTable[selectedLanguage];
				if (key == null || !this._keyToRowTable.ContainsKey(key))
				{
					result = key;
				}
				else
				{
					int row = this._keyToRowTable[key];
					object cell = base.GetCell("TRANSLATIONS", row, column);
					if (cell == null || string.IsNullOrEmpty(cell.ToString()))
					{
						result = key;
					}
					else
					{
						string text = cell.ToString();
						result = text;
					}
				}
			}
			return result;
		}

		public List<string> GetLanguages()
		{
			return this._langToColumnTable.Keys.ToList<string>();
		}
	}
}
