using SEStorage.Base;
using System;
using System.IO;
using System.Xml.Serialization;

namespace SEStorage
{
	public class XmlFile : StorageItem
	{
		private const string DEFAULT_FILE_TYPE = "xml";

		public XmlFile()
		{
			this._defaultFileExtention = "xml";
		}

		protected override void SaveFileProcess(object obj, string path)
		{
			try
			{
				XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
				TextWriter textWriter = new StreamWriter(path);
				xmlSerializer.Serialize(textWriter, obj);
				textWriter.Close();
			}
			catch (Exception var_2_28)
			{
			}
		}

		protected override object OpenFileProcess(string path, object[] extraData)
		{
			object result = null;
			try
			{
				Type type = null;
				if (extraData != null && extraData.Length > 0 && extraData[0] != null)
				{
					type = (Type)extraData[0];
				}
				if (File.Exists(path) || type != null)
				{
					XmlSerializer xmlSerializer = new XmlSerializer(type);
					TextReader textReader = new StreamReader(path);
					result = xmlSerializer.Deserialize(textReader);
					textReader.Close();
				}
			}
			catch (Exception var_4_60)
			{
			}
			return result;
		}
	}
}
