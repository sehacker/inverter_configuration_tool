using System;
using System.Collections.Generic;

namespace SEStorage
{
	[Serializable]
	public class SEUserSettings
	{
		private const string USER_SETTINGS_FILE_NAME = "SEUserSettings";

		private string _dataSourcePath = null;

		private Dictionary<string, object> _dataTable = null;

		public string DataSourcePath
		{
			get
			{
				return this._dataSourcePath;
			}
			set
			{
				this._dataSourcePath = value;
			}
		}

		public object this[string key]
		{
			get
			{
				object result = null;
				if (this._dataTable == null)
				{
					this._dataTable = new Dictionary<string, object>(0);
				}
				if (this._dataTable.ContainsKey(key))
				{
					result = this._dataTable[key];
				}
				return result;
			}
			set
			{
				if (this._dataTable == null)
				{
					this._dataTable = new Dictionary<string, object>(0);
				}
				if (this._dataTable.ContainsKey(key))
				{
					this._dataTable[key] = value;
				}
				else
				{
					this._dataTable.Add(key, value);
				}
				this.SaveUS();
			}
		}

		public SEUserSettings(string dataSourcePath)
		{
			this._dataSourcePath = dataSourcePath;
			this.LoadUS();
		}

		private void SaveUS()
		{
			new BinaryFile
			{
				FileType = "us"
			}.SaveObjectToFile(this, this._dataSourcePath + "\\SEUserSettings");
		}

		private void LoadUS()
		{
			SEUserSettings sEUserSettings = (SEUserSettings)new BinaryFile
			{
				FileType = "us"
			}.OpenFileAsObject(this._dataSourcePath + "\\SEUserSettings");
			if (sEUserSettings != null)
			{
				if (this._dataTable != null)
				{
					Dictionary<string, object>.KeyCollection.Enumerator enumerator = sEUserSettings._dataTable.Keys.GetEnumerator();
					while (enumerator.MoveNext())
					{
						string current = enumerator.Current;
						if (this._dataTable.ContainsKey(current))
						{
							this._dataTable[current] = sEUserSettings[current];
						}
						else
						{
							this._dataTable.Add(current, sEUserSettings[current]);
						}
					}
				}
				else
				{
					this._dataTable = sEUserSettings._dataTable;
				}
			}
		}
	}
}
