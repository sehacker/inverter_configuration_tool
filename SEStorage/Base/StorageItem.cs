using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace SEStorage.Base
{
	[Serializable]
	public class StorageItem
	{
		protected const Binder MEMBER_BINDER_DEFAULT = null;

		protected const string STRING_EMPTY = "";

		protected const string STRING_DOT = ".";

		private const string DEFAULT_FILE_TYPE = "dat";

		private const string DATA_SOURCE_FILE_EXT_CONNECTOR = "|";

		protected string _defaultFileExtention = "dat";

		private static BindingFlags _memberBindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetField | BindingFlags.GetProperty;

		private object[] _memberArgs = new object[0];

		private OpenFileDialog _ofd = null;

		private SaveFileDialog _sfd = null;

		public string FileType
		{
			get
			{
				return this._defaultFileExtention;
			}
			set
			{
				this._defaultFileExtention = value;
			}
		}

		public StorageItem()
		{
		}

		public StorageItem(SerializationInfo info, StreamingContext ctxt) : this()
		{
			Type type = base.GetType();
			MemberInfo[] members = type.GetMembers();
			int num = members.Length;
			for (int i = 0; i < num; i++)
			{
				MemberInfo memberInfo = members[i];
				Type type2 = memberInfo.GetType();
				MemberTypes memberType = memberInfo.MemberType;
				if (memberType == MemberTypes.Field || memberType == MemberTypes.Property)
				{
					try
					{
						string name = memberInfo.Name;
						object obj = type2.InvokeMember(name, StorageItem._memberBindingFlags, null, this, this._memberArgs);
						obj = info.GetValue(name, obj.GetType());
					}
					catch (Exception var_9_84)
					{
					}
				}
			}
		}

		private void SetupSaveDialog()
		{
			this._sfd = new SaveFileDialog();
			this._sfd.AddExtension = true;
			this._sfd.AutoUpgradeEnabled = true;
			this._sfd.InitialDirectory = Application.LocalUserAppDataPath;
			this._sfd.ValidateNames = true;
		}

		private void SetupOpenDialog()
		{
			this._ofd = new OpenFileDialog();
			this._ofd.AddExtension = true;
			this._ofd.AutoUpgradeEnabled = true;
			this._ofd.InitialDirectory = Application.LocalUserAppDataPath;
			this._ofd.ValidateNames = true;
		}

		private DialogResult ShowSaveFileDialog(string title, string fileTypeDescription)
		{
			if (this._sfd == null)
			{
				this.SetupSaveDialog();
			}
			this._sfd.DefaultExt = "." + this.FileType;
			this._sfd.Title = title;
			this._sfd.Filter = fileTypeDescription + "|" + this.FileType;
			return this._sfd.ShowDialog();
		}

		private DialogResult ShowOpenFileDialog(string title, string fileTypeDescription)
		{
			if (this._ofd == null)
			{
				this.SetupOpenDialog();
			}
			this._ofd.DefaultExt = "." + this.FileType;
			this._ofd.Title = title;
			this._ofd.Filter = fileTypeDescription + "|" + this.FileType;
			return this._ofd.ShowDialog();
		}

		private string GetStoragePath(string originalPath)
		{
			string result = "";
			if (!string.IsNullOrEmpty(originalPath))
			{
				string text = "." + this.FileType;
				if (!originalPath.Contains(text))
				{
					originalPath += text;
				}
				result = originalPath;
			}
			return result;
		}

		protected virtual void SaveFileProcess(object obj, string path)
		{
		}

		protected virtual object OpenFileProcess(string path, object[] extraData)
		{
			return null;
		}

		public void SaveObjectToFile(object obj, string title, string fileTypeDescription)
		{
			DialogResult dialogResult = this.ShowSaveFileDialog(title, fileTypeDescription);
			DialogResult dialogResult2 = dialogResult;
			if (dialogResult2 == DialogResult.OK)
			{
				if (obj != null && !string.IsNullOrEmpty(this._sfd.FileName))
				{
					string storagePath = this.GetStoragePath(this._sfd.FileName);
					if (!string.IsNullOrEmpty(storagePath))
					{
						this.SaveFileProcess(obj, storagePath);
					}
				}
			}
		}

		public void SaveObjectToFile(object obj, string filePath)
		{
			if (!string.IsNullOrEmpty(filePath))
			{
				filePath = this.GetStoragePath(filePath);
				if (!string.IsNullOrEmpty(filePath))
				{
					this.SaveFileProcess(obj, filePath);
				}
			}
		}

		public object OpenFileAsObject(string title, string fileTypeDescription)
		{
			return this.OpenFileAsObject(title, fileTypeDescription, null);
		}

		public object OpenFileAsObject(string title, string fileTypeDescription, Type typeOfObjectToLoad)
		{
			object result = null;
			DialogResult dialogResult = this.ShowOpenFileDialog(title, fileTypeDescription);
			DialogResult dialogResult2 = dialogResult;
			if (dialogResult2 == DialogResult.OK)
			{
				if (!string.IsNullOrEmpty(this._ofd.FileName))
				{
					string storagePath = this.GetStoragePath(this._ofd.FileName);
					if (!string.IsNullOrEmpty(storagePath))
					{
						object[] extraData = null;
						if (typeOfObjectToLoad != null)
						{
							extraData = new object[]
							{
								typeOfObjectToLoad
							};
						}
						result = this.OpenFileProcess(storagePath, extraData);
					}
				}
			}
			return result;
		}

		public object OpenFileAsObject(string filePath)
		{
			object result = null;
			if (!string.IsNullOrEmpty(filePath))
			{
				filePath = this.GetStoragePath(filePath);
				if (!string.IsNullOrEmpty(filePath))
				{
					Type type = base.GetType();
					object[] extraData = new object[]
					{
						type
					};
					result = this.OpenFileProcess(filePath, extraData);
				}
			}
			return result;
		}

		public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
		{
			Type type = base.GetType();
			MemberInfo[] members = type.GetMembers();
			int num = members.Length;
			for (int i = 0; i < num; i++)
			{
				MemberInfo memberInfo = members[i];
				Type type2 = memberInfo.GetType();
				MemberTypes memberType = memberInfo.MemberType;
				if (memberType == MemberTypes.Field || memberType == MemberTypes.Property)
				{
					try
					{
						string name = memberInfo.Name;
						object value = type2.InvokeMember(name, StorageItem._memberBindingFlags, null, this, this._memberArgs);
						info.AddValue(name, value);
					}
					catch (Exception var_9_77)
					{
					}
				}
			}
		}
	}
}
