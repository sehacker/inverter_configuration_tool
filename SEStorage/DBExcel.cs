using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;

namespace SEStorage
{
	public class DBExcel
	{
		private const string FILE_DB_TYPE = "xlsx";

		private const string FILE_DBM_TYPE = "xlsm";

		private const string FILE_DB_OLD_TYPE = "xls";

		private const string STRING_EMPTY = "";

		private const string STRING_DOT = ".";

		private const string XL_LICENSE = "EN87-0CVA-I6DR-BN0Q";

		private ExcelFile _dbFile = null;

		private string _dbFilePath = "";

		private bool _writable = false;

		public bool Writable
		{
			get
			{
				return this._writable;
			}
		}

		public DBExcel(object fileInfo, FileTypes type)
		{
			this._writable = (fileInfo == null || fileInfo.GetType().Equals(typeof(string)));
			this.SetupDatabase();
			if (type == FileTypes.XLS)
			{
				this.LoadXLSDatabase(fileInfo);
			}
			else
			{
				this.LoadXLSXDatabase(fileInfo);
			}
		}

		private void SetupDatabase()
		{
			SpreadsheetInfo.SetLicense("EN87-0CVA-I6DR-BN0Q");
			this._dbFile = new ExcelFile();
		}

		protected void LoadXLSDatabase(object path)
		{
			Type type = (path != null) ? path.GetType() : null;
			if (type != null && type.Equals(typeof(string)))
			{
				this._dbFilePath = path.ToString();
				this._dbFile.LoadXls(this._dbFilePath, XlsOptions.None);
			}
			else if (type != null)
			{
				this._dbFile.LoadXls((Stream)path, XlsOptions.None);
			}
		}

		protected void LoadXLSXDatabase(object path)
		{
			Type type = (path != null) ? path.GetType() : null;
			if (type != null && type.Equals(typeof(string)))
			{
				this._dbFilePath = path.ToString();
				this._dbFile.LoadXlsx(this._dbFilePath, XlsxOptions.None);
			}
			else if (type != null)
			{
				this._dbFile.LoadXlsx((Stream)path, XlsxOptions.None);
			}
		}

		protected void SaveXLSDatabase(string path)
		{
			this._dbFile.SaveXls(path);
		}

		protected void SaveXLSXDatabase(string path)
		{
			this._dbFile.SaveXlsx(path);
		}

		private object GetCellValue(ExcelWorksheet ws, int row, int column)
		{
			object result = null;
			if (ws != null && row >= 0 && column >= 0)
			{
				result = ws.Cells[row, column].Value;
			}
			return result;
		}

		private void SetCellValue(ExcelWorksheet ws, int row, int column, object data)
		{
			if (ws != null && row >= 0 && column >= 0 && data != null)
			{
				ws.Cells[row, column].Value = data;
			}
		}

		public void ResetWorkbook()
		{
			this._dbFile = new ExcelFile();
		}

		public bool AddWorkSheet(string worksheetName)
		{
			ExcelWorksheet excelWorksheet = this._dbFile.Worksheets.Add(worksheetName);
			return excelWorksheet != null;
		}

		public object GetCell(int worksheetIndex, int row, int column)
		{
			object result = null;
			if (this._dbFile != null && worksheetIndex >= 0 && worksheetIndex < this._dbFile.Worksheets.Count)
			{
				ExcelWorksheet ws = this._dbFile.Worksheets[worksheetIndex];
				result = this.GetCellValue(ws, row, column);
			}
			return result;
		}

		public object GetCell(string worksheetName, int row, int column)
		{
			object result = null;
			if (this._dbFile != null && !string.IsNullOrEmpty(worksheetName))
			{
				ExcelWorksheet ws = this._dbFile.Worksheets[worksheetName];
				result = this.GetCellValue(ws, row, column);
			}
			return result;
		}

		public object[] GetRow(int worksheetIndex, int row)
		{
			List<object> list = null;
			if (this._dbFile != null && worksheetIndex >= 0 && worksheetIndex < this._dbFile.Worksheets.Count)
			{
				ExcelWorksheet excelWorksheet = this._dbFile.Worksheets[worksheetIndex];
				int column = 0;
				object cell;
				while ((cell = this.GetCell(worksheetIndex, row, column)) != null)
				{
					if (list == null)
					{
						list = new List<object>(0);
					}
					list.Add(cell);
				}
			}
			return (list != null) ? list.ToArray() : null;
		}

		public object[] GetRow(string worksheetName, int row)
		{
			List<object> list = null;
			if (this._dbFile != null && !string.IsNullOrEmpty(worksheetName))
			{
				ExcelWorksheet excelWorksheet = this._dbFile.Worksheets[worksheetName];
				int num = 0;
				object cell;
				while ((cell = this.GetCell(worksheetName, row, num)) != null)
				{
					if (list == null)
					{
						list = new List<object>(0);
					}
					list.Add(cell);
					num++;
				}
			}
			return (list != null) ? list.ToArray() : null;
		}

		public void SetCell(int worksheetIndex, int row, int column, object data)
		{
			if (this._dbFile != null && worksheetIndex >= 0 && worksheetIndex < this._dbFile.Worksheets.Count)
			{
				ExcelWorksheet ws = this._dbFile.Worksheets[worksheetIndex];
				this.SetCellValue(ws, row, column, data);
			}
		}

		public void SetCell(string worksheetName, int row, int column, object data)
		{
			if (this._dbFile != null && !string.IsNullOrEmpty(worksheetName))
			{
				ExcelWorksheet ws = this._dbFile.Worksheets[worksheetName];
				this.SetCellValue(ws, row, column, data);
			}
		}

		public void SetRow(int worksheetIndex, int row, object[] data)
		{
			if (this._dbFile != null && worksheetIndex >= 0 && worksheetIndex < this._dbFile.Worksheets.Count && data != null && data.Length > 0)
			{
				ExcelWorksheet excelWorksheet = this._dbFile.Worksheets[worksheetIndex];
				int num = data.Length;
				if (data.Length <= excelWorksheet.Columns.Count)
				{
					for (int i = 0; i < data.Length; i++)
					{
						this.SetCellValue(excelWorksheet, row, i, data[i]);
					}
				}
			}
		}

		public void SetRow(string worksheetName, int row, object[] data)
		{
			if (this._dbFile != null && !string.IsNullOrEmpty(worksheetName) && data != null && data.Length > 0)
			{
				ExcelWorksheet excelWorksheet = this._dbFile.Worksheets[worksheetName];
				int num = data.Length;
				if (data.Length <= excelWorksheet.Columns.Count)
				{
					for (int i = 0; i < data.Length; i++)
					{
						this.SetCellValue(excelWorksheet, row, i, data[i]);
					}
				}
			}
		}
	}
}
