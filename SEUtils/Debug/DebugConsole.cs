using System;
using System.Diagnostics;
using System.Text;

namespace SEUtils.Debug
{
	public class DebugConsole
	{
		public static void WriteCommand(ushort index, ushort opCode, string name, string action)
		{
			DebugConsole.WriteCommand(index, opCode, name, action, null);
		}

		public static void WriteCommand(ushort index, ushort opCode, string name, string action, ushort? expectedResponseOpcode)
		{
			DateTime now = DateTime.Now;
			int hour = now.Hour;
			int minute = now.Minute;
			int second = now.Second;
			int millisecond = now.Millisecond;
			StringBuilder stringBuilder = new StringBuilder(0);
			stringBuilder.Append(string.Format("[{0:00}:{1:00}:{2:00}:{3:0000}] ", new object[]
			{
				hour,
				minute,
				second,
				millisecond
			}));
			stringBuilder.Append(string.Format("#{0:00000000}", index));
			stringBuilder.Append(" (Request) - ");
			stringBuilder.Append("(Code :");
			stringBuilder.Append(string.Format("{0:0000}", opCode));
			stringBuilder.Append(") - ");
			stringBuilder.Append(name);
			stringBuilder.Append(" - ");
			stringBuilder.Append(action);
			stringBuilder.Append(" - [Rsp: ");
			ushort? num = expectedResponseOpcode;
			if (!(num.HasValue ? new int?((int)num.GetValueOrDefault()) : null).HasValue)
			{
				stringBuilder.Append("None");
			}
			else
			{
				try
				{
					stringBuilder.Append(expectedResponseOpcode.Value);
				}
				catch (Exception var_6_151)
				{
				}
			}
			stringBuilder.Append("]");
			string strToWrite = stringBuilder.ToString();
			DebugConsole.WriteLine(strToWrite);
		}

		public static void WriteAnswer(ushort index, ushort opCode, string name, string action, bool isPushed)
		{
			DateTime now = DateTime.Now;
			int hour = now.Hour;
			int minute = now.Minute;
			int second = now.Second;
			int millisecond = now.Millisecond;
			StringBuilder stringBuilder = new StringBuilder(0);
			stringBuilder.Append(string.Format("[{0:00}:{1:00}:{2:00}:{3:0000}] ", new object[]
			{
				hour,
				minute,
				second,
				millisecond
			}));
			stringBuilder.Append(string.Format("#{0:00000000}", index));
			if (isPushed)
			{
				stringBuilder.Append(" (Pushed) - ");
			}
			else
			{
				stringBuilder.Append(" (Answer) - ");
			}
			stringBuilder.Append("(Code :");
			stringBuilder.Append(string.Format("{0:0000}", opCode));
			stringBuilder.Append(") - ");
			stringBuilder.Append(name);
			stringBuilder.Append(" - ");
			stringBuilder.Append(action);
			string strToWrite = stringBuilder.ToString();
			DebugConsole.WriteLine(strToWrite);
		}

		public static void Write(string strToWrite)
		{
			Trace.Write(strToWrite);
		}

		public static void WriteLine(string strToWrite)
		{
			Trace.WriteLine(strToWrite);
		}
	}
}
