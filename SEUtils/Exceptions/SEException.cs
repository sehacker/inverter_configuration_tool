using System;

namespace SEUtils.Exceptions
{
	public class SEException : Exception
	{
		public SEException()
		{
		}

		public SEException(string message) : base(message)
		{
		}

		public SEException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
