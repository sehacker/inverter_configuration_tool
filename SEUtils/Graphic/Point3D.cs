using System;

namespace SEUtils.Graphic
{
	public class Point3D
	{
		private int _x = 0;

		private int _y = 0;

		private int _z = 0;

		public int X
		{
			get
			{
				return this._x;
			}
			set
			{
				this._x = value;
			}
		}

		public int Y
		{
			get
			{
				return this._y;
			}
			set
			{
				this._y = value;
			}
		}

		public int Z
		{
			get
			{
				return this._z;
			}
			set
			{
				this._z = value;
			}
		}

		public Point3D()
		{
		}

		public Point3D(int x, int y, int z)
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
		}
	}
}
