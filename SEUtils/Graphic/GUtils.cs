using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace SEUtils.Graphic
{
	public class GUtils
	{
		public static Color GetTransparentColor(Bitmap img)
		{
			return GUtils.GetTransparentColor(img, 0, 0);
		}

		public static Color GetTransparentColor(Bitmap img, int x, int y)
		{
			Color result;
			if (img == null || x < 0 || x >= img.Width || y < 0 || y >= img.Height)
			{
				result = Color.Empty;
			}
			else
			{
				result = img.GetPixel(x, y);
			}
			return result;
		}

		public static ImageAttributes GetTransparentAttributes(Color transparentColor)
		{
			ImageAttributes imageAttributes = new ImageAttributes();
			imageAttributes.SetColorKey(transparentColor, transparentColor);
			return imageAttributes;
		}

		public static ImageAttributes GetTransparentAttributes(Bitmap img, int x, int y)
		{
			Color transparentColor = GUtils.GetTransparentColor(img, x, y);
			return GUtils.GetTransparentAttributes(transparentColor);
		}

		public static ImageAttributes GetTransparentAttributes(Bitmap img)
		{
			return GUtils.GetTransparentAttributes(img, 0, 0);
		}
	}
}
