using System;

namespace SEUtils.Common
{
	public enum VersionOptions
	{
		NONE,
		TRIM_TRAIL,
		ADD_CHECKSUM_DECIMAL,
		ADD_CHECKSUM_HEX = 4,
		ADD_BETA_INDICATION = 8,
		MAX = 15
	}
}
