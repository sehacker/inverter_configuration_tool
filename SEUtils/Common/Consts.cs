using System;

namespace SEUtils.Common
{
	public class Consts
	{
		public const string STRING_EMPTY = "";

		public const string STRING_UNDERLINE = "_";

		public const string STRING_SPACE = " ";

		public const string STRING_REMARK = "//";

		public const string STRING_EQUALS = "=";

		public const string STRING_UNICODE_NEWLINE = "\r\n";

		public const string STRING_COLON = ":";

		public const string STRING_DOT = ".";

		public const string STRING_PLUS = "+";

		public const string STRING_MINUS = "-";

		public const string STRING_QUESTION_MARK = "?";

		public const string STRING_DATA_CHANGE = "*";

		public const string STRING_P_RND_L = "(";

		public const string STRING_P_RND_R = ")";

		public const string STRING_P_SQ_L = "[";

		public const string STRING_P_SQ_R = "]";

		public const string STRING_COMMA = ",";

		public const string STRING_DLM = "#";

		public const string STRING_SLASH = "/";

		public const string UNIT_PERCENT = "%";

		public const string UNIT_VOLT = "V";

		public const string UNIT_AMPER = "A";

		public const string UNIT_PERC_TEMP = "%/°C";

		public const string UNIT_W_POWER = "Wp";

		public const string UNIT_K_WATT = "KW";

		public const string UNIT_DEGREES_SPECIAL = "(S = 180)";

		public const string IR_UNIT = "kWh/m²/day";

		public const string EVC_UNIT = "mV/°C";

		public const string EVCP_UNIT = "%/°C";

		public const string EIC_UNIT = "mA/°C";

		public const string EICP_UNIT = "%/°C";

		public const string CABLE_AREA_UNIT = "mm²";

		public const string COMMAND_MAIL_TO = "mailto";

		public const string COMMAND_MAIL_SUBJECT = "subject";

		public const string BETA_NAME = "(beta)";

		public const string VERSION_TAG = "ver";

		public const string SUPPORT_URL = "support@solaredge.com";

		public const string DEFAULT_MAIL_SUBJECT = "New Support Mail";

		public const string XL_LICENSE = "EN87-0CVA-I6DR-BN0Q";

		public const string DATA_SRC_FILE_EXT_CONNECTOR = "|*";

		public const string DATA_SRC_DIC_FILE_TYPE = ".txt";

		public const string DATA_SRC_DEBUG_FILE_TYPE = ".txt";

		public const string DATA_SRC_TESTCASE_FILE_TYPE = ".txt";

		public const string DATA_SRC_XL_FILE_TYPE = ".xlsx";

		public const string DATA_SRC_XLM_FILE_TYPE = ".xlsm";

		public const string DATA_SRC_PRJ_FILE_TYPE = ".sedt";

		public const string IMAGE_SRC_PNG_FILE_TYPE = ".png";

		public const string IMAGE_SRC_JPEG_FILE_TYPE = ".jpeg";

		public const string IMAGE_SRC_BMP_FILE_TYPE = ".bmp";

		public const string IMAGE_SRC_GIF_FILE_TYPE = ".gif";

		public const string OUTPUT_SRC_PDF_FILE_TYPE = ".pdf";

		public const string CULTURE_US = "en-us";

		public const string M_SEP = ".";

		public const string M_RESC = ".Resources";

		public const string M_ICONS = ".Resources.Icons";

		public const string M_LANGS = ".Resources.Languages";

		public const string M_SET = ".Resources.Settings";

		public const string M_VALID = ".Resources.Validations";
	}
}
