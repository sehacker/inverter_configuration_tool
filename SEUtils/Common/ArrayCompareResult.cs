using System;

namespace SEUtils.Common
{
	public enum ArrayCompareResult
	{
		EQUAL,
		A_BIGGER,
		B_BIGGER
	}
}
