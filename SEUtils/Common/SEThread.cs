using System;
using System.Globalization;
using System.Threading;

namespace SEUtils.Common
{
	public class SEThread
	{
		public delegate void ThreadFunctionCallBack();

		public delegate void ThreadFunctionCallBackParams(params object[] data);

		public const uint ITERATION_INFINITE = 0u;

		public const uint TICK_TIME_NONE = 0u;

		public const bool START_PAUSED = true;

		public const bool START_RUNNING = false;

		public const bool RUN_NOW = true;

		public const bool RUN_ON_DEMAND = true;

		public const bool PRIORITY_BACKGROUND = true;

		public const bool PRIORITY_FOREGROUND = false;

		public const bool THREAD_PAUSE = true;

		public const bool THREAD_RESUME = false;

		private Thread _thread = null;

		private bool _shouldThreadStop = false;

		private bool _shouldThreadPause = false;

		private SEThread.ThreadFunctionCallBack _callBack = null;

		private SEThread.ThreadFunctionCallBackParams _callBackWithParams = null;

		private object[] _callBackParams = null;

		private Type[] _callBackParamTypes = null;

		private uint _tickTime = 0u;

		private uint _numOfIterations = 0u;

		private uint _iterationsDone = 0u;

		private ManualResetEvent _notifyTick = null;

		private ManualResetEvent _notifyPause = null;

		private int _sleepDuration = 0;

		public Thread Thread
		{
			get
			{
				return this._thread;
			}
		}

		public bool Stop
		{
			get
			{
				bool shouldThreadStop;
				lock (this)
				{
					shouldThreadStop = this._shouldThreadStop;
				}
				return shouldThreadStop;
			}
			set
			{
				lock (this)
				{
					this._shouldThreadStop = value;
				}
			}
		}

		public bool Pause
		{
			get
			{
				bool shouldThreadPause;
				lock (this)
				{
					shouldThreadPause = this._shouldThreadPause;
				}
				return shouldThreadPause;
			}
			set
			{
				lock (this)
				{
					this._shouldThreadPause = value;
					if (!this._shouldThreadPause)
					{
						try
						{
							this._notifyPause.Set();
						}
						catch (Exception var_0_2D)
						{
						}
					}
				}
			}
		}

		public uint TickTime
		{
			get
			{
				uint tickTime;
				lock (this)
				{
					tickTime = this._tickTime;
				}
				return tickTime;
			}
			set
			{
				lock (this)
				{
					this._tickTime = value;
				}
			}
		}

		public uint Iterations
		{
			get
			{
				uint numOfIterations;
				lock (this)
				{
					numOfIterations = this._numOfIterations;
				}
				return numOfIterations;
			}
			set
			{
				lock (this)
				{
					this._numOfIterations = value;
					this._iterationsDone = this._numOfIterations;
				}
			}
		}

		public SEThread(string name, SEThread.ThreadFunctionCallBack callBack, int sleepDuration, uint tickTime, uint iterations, bool background, bool runNow, bool paused)
		{
			this._thread = new Thread(new ThreadStart(this.ThreadFunc));
			this._thread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
			this._sleepDuration = sleepDuration;
			this._callBack = callBack;
			this._callBackParams = null;
			this._callBackParamTypes = Type.EmptyTypes;
			this._thread.Name = ((!string.IsNullOrEmpty(name)) ? name : "");
			this._tickTime = tickTime;
			this.Iterations = iterations;
			this._thread.IsBackground = background;
			this._shouldThreadPause = paused;
			this._shouldThreadStop = false;
			if (runNow)
			{
				this._thread.Start();
			}
		}

		public SEThread(string name, SEThread.ThreadFunctionCallBackParams callBack, object[] paramList, int sleepDuration, uint tickTime, uint iterations, bool background, bool runNow, bool paused)
		{
			this._thread = new Thread(new ThreadStart(this.ThreadFunc));
			this._thread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
			this._sleepDuration = sleepDuration;
			this._callBackWithParams = callBack;
			this._callBackParams = paramList;
			if (this._callBackParams != null)
			{
				int num = this._callBackParams.Length;
				this._callBackParamTypes = new Type[num];
				for (int i = 0; i < num; i++)
				{
					this._callBackParamTypes[i] = this._callBackParams[i].GetType();
				}
			}
			else
			{
				this._callBackParamTypes = Type.EmptyTypes;
			}
			this._thread.Name = ((!string.IsNullOrEmpty(name)) ? name : "");
			this._tickTime = tickTime;
			this.Iterations = iterations;
			this._thread.IsBackground = background;
			this._shouldThreadPause = paused;
			this._shouldThreadStop = false;
			if (runNow)
			{
				this._thread.Start();
			}
		}

		public void Dispose()
		{
			try
			{
				this.Pause = false;
				this.Stop = true;
				this._thread.Abort();
			}
			catch (Exception var_0_21)
			{
			}
			finally
			{
				this._thread = null;
			}
		}

		public void Interrupt()
		{
			if (!this.Pause)
			{
				this.Pause = true;
				this._thread.Interrupt();
			}
		}

		private void ThreadFunc()
		{
			this._notifyTick = new ManualResetEvent(false);
			this._notifyPause = new ManualResetEvent(false);
			try
			{
				while (!this._shouldThreadStop)
				{
					try
					{
						if (this.TickTime > 0u)
						{
							try
							{
								this._notifyTick.Reset();
								this._notifyTick.WaitOne((int)this.TickTime);
							}
							catch (TimeoutException var_0_54)
							{
							}
						}
						if (this.Pause || (this.Iterations > 0u && this._iterationsDone == 0u))
						{
							this.Pause = true;
							this._notifyPause.Reset();
							this._notifyPause.WaitOne(-1);
						}
						if (this._iterationsDone > 0u)
						{
							this._iterationsDone -= 1u;
						}
						if (this._callBackParams == null)
						{
							this._callBack();
						}
						else
						{
							this._callBackWithParams(this._callBackParams);
						}
					}
					catch (ThreadInterruptedException var_1_FA)
					{
						this.Pause = true;
					}
					catch (Exception var_2_107)
					{
					}
					Thread.Sleep(this._sleepDuration);
				}
			}
			catch (Exception var_2_107)
			{
			}
		}
	}
}
