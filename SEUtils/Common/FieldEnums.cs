using System;

namespace SEUtils.Common
{
	public enum FieldEnums
	{
		MAJOR_ENUM,
		MINOR_ENUM,
		BUILD_ENUM,
		REVISION_ENUM,
		CHECKSUM_ENUM,
		BETA_ENUM,
		COUNT_ENUM
	}
}
