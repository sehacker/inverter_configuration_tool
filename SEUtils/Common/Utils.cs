using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace SEUtils.Common
{
	public class Utils
	{
		private const int MAX_BITS_TO_USE = 32;

		private const int BASE_10 = 10;

		private const int ASCII_DIFF = 48;

		public const int MSECS_IN_SEC = 1000;

		public const int SECS_IN_MIN = 60;

		public const int MINS_IN_HOUR = 60;

		public const int SECS_IN_HOUR = 3600;

		private static char[] HEX_TOP_ARR = new char[]
		{
			'A',
			'B',
			'C',
			'D',
			'E',
			'F'
		};

		private static int[] HEX_TOP_ARR_NUMERIC = new int[]
		{
			10,
			11,
			12,
			13,
			14,
			15
		};

		public static DateTime TIMESTAMP_BASE = new DateTime(2000, 1, 1, 0, 0, 0);

		public static DateTime TIMESTAMP_BASE_1970 = new DateTime(1970, 1, 1, 0, 0, 0);

		public static ArrayCompareResult ArrayCompare(byte[] a, byte[] b)
		{
			ArrayCompareResult result;
			if (a.Length > b.Length)
			{
				result = ArrayCompareResult.A_BIGGER;
			}
			else if (b.Length > a.Length)
			{
				result = ArrayCompareResult.B_BIGGER;
			}
			else
			{
				int i = 0;
				while (i < a.Length)
				{
					if (a[i] == b[i])
					{
						i++;
					}
					else
					{
						if (a[i] > b[i])
						{
							result = ArrayCompareResult.A_BIGGER;
							return result;
						}
						result = ArrayCompareResult.B_BIGGER;
						return result;
					}
				}
				result = ArrayCompareResult.EQUAL;
			}
			return result;
		}

		public static uint FloatAsUInt32(float f)
		{
			byte[] buffer = new byte[4];
			MemoryStream memoryStream = new MemoryStream(buffer);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			BinaryReader binaryReader = new BinaryReader(memoryStream);
			binaryWriter.Write(f);
			memoryStream.Position = 0L;
			return binaryReader.ReadUInt32();
		}

		public static float UInt32AsFloat(uint n)
		{
			byte[] buffer = new byte[4];
			MemoryStream memoryStream = new MemoryStream(buffer);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			BinaryReader binaryReader = new BinaryReader(memoryStream);
			binaryWriter.Write(n);
			memoryStream.Position = 0L;
			return binaryReader.ReadSingle();
		}

		public static uint Int32AsUInt32(int i)
		{
			byte[] buffer = new byte[4];
			MemoryStream memoryStream = new MemoryStream(buffer);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			BinaryReader binaryReader = new BinaryReader(memoryStream);
			binaryWriter.Write(i);
			memoryStream.Position = 0L;
			return binaryReader.ReadUInt32();
		}

		public static int UInt32AsInt32(uint n)
		{
			byte[] buffer = new byte[4];
			MemoryStream memoryStream = new MemoryStream(buffer);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			BinaryReader binaryReader = new BinaryReader(memoryStream);
			binaryWriter.Write(n);
			memoryStream.Position = 0L;
			return binaryReader.ReadInt32();
		}

		public static uint FloatToQ(float f, int Q)
		{
			return (uint)(Math.Pow(2.0, (double)Q) * (double)f);
		}

		public static float QToFloat(uint N, int Q)
		{
			return N / (float)Math.Pow(2.0, (double)Q);
		}

		public static uint DateToTimestamp1970(DateTime Date)
		{
			return (uint)(Date - Utils.TIMESTAMP_BASE_1970).TotalSeconds;
		}

		public static DateTime Timestamp1970ToDate(uint Ts)
		{
			return Utils.TIMESTAMP_BASE_1970 + TimeSpan.FromSeconds(Ts);
		}

		public static DateTime TimestampToDate(uint Ts)
		{
			return Utils.TIMESTAMP_BASE + TimeSpan.FromSeconds(Ts);
		}

		public static uint DateToTimestamp(DateTime Date)
		{
			return (uint)(Date - Utils.TIMESTAMP_BASE).TotalSeconds;
		}

		public static DateTime? StringToTime(string timeStr)
		{
			DateTime? dateTime = null;
			DateTime? result;
			try
			{
				if (string.IsNullOrEmpty(timeStr))
				{
					result = null;
					return result;
				}
				string[] array = timeStr.Split(new string[]
				{
					" "
				}, StringSplitOptions.None);
				if (array.Length != 2)
				{
					result = null;
					return result;
				}
				string text = array[0];
				string[] array2 = text.Split(new string[]
				{
					"-"
				}, StringSplitOptions.None);
				if (array2.Length != 3)
				{
					result = null;
					return result;
				}
				int day = int.Parse(array2[0]);
				int month = int.Parse(array2[1]);
				int year = int.Parse(array2[2]);
				string text2 = array[1];
				string[] array3 = text2.Split(new string[]
				{
					":"
				}, StringSplitOptions.None);
				if (array3.Length != 3)
				{
					result = null;
					return result;
				}
				int hour = int.Parse(array3[0]);
				int minute = int.Parse(array3[1]);
				int second = int.Parse(array3[2]);
				dateTime = new DateTime?(new DateTime(year, month, day, hour, minute, second));
			}
			catch (Exception var_12_139)
			{
			}
			result = dateTime;
			return result;
		}

		public static string GetDateString(DateTime date)
		{
			return Utils.GetDateString(date, true, true);
		}

		public static string GetDateString(DateTime date, bool withDate, bool withTime)
		{
			bool flag = 1 == 0;
			string text = string.Concat(new string[]
			{
				date.Day.ToString(),
				"-",
				date.Month.ToString(),
				"-",
				date.Year.ToString()
			});
			string text2 = (date.Hour < 10) ? ("0" + date.Hour.ToString()) : date.Hour.ToString();
			string text3 = (date.Minute < 10) ? ("0" + date.Minute.ToString()) : date.Minute.ToString();
			string text4 = (date.Second < 10) ? ("0" + date.Second.ToString()) : date.Second.ToString();
			string text5 = string.Concat(new string[]
			{
				text2,
				":",
				text3,
				":",
				text4
			});
			return (withDate ? text : "") + ((withDate && withTime) ? " " : "") + (withTime ? text5 : "");
		}

		public static byte GetChecksum(uint uiValue)
		{
			return (byte)((uiValue >> 24 & 255u) + (uiValue >> 16 & 255u) + (uiValue >> 8 & 255u) + (uiValue & 255u));
		}

		public static string GetCheckSum(uint serial)
		{
			string serial2 = Convert.ToString((long)((ulong)serial), 16).ToUpper();
			return Utils.GetCheckSum(serial2);
		}

		public static string GetCheckSum(string serial)
		{
			string result;
			if (string.IsNullOrEmpty(serial))
			{
				result = null;
			}
			else
			{
				int length = serial.Length;
				if (length <= 0 || length > 9)
				{
					result = null;
				}
				else
				{
					List<string> list = new List<string>(0);
					int num = length;
					StringBuilder stringBuilder = null;
					string value;
					while (!string.IsNullOrEmpty(value = serial[--num].ToString()))
					{
						if (stringBuilder == null || stringBuilder.Length >= 2)
						{
							stringBuilder = new StringBuilder(0);
						}
						switch (stringBuilder.Length)
						{
						case 0:
							stringBuilder.Append(value);
							break;
						case 1:
							stringBuilder.Insert(0, value);
							break;
						}
						if (stringBuilder.Length == 2 || num == 0)
						{
							list.Add(stringBuilder.ToString());
						}
						if (num == 0)
						{
							break;
						}
					}
					int count = list.Count;
					uint num2 = 0u;
					for (int i = 0; i < count; i++)
					{
						string s = list[i];
						uint num3 = uint.Parse(s, NumberStyles.HexNumber);
						num2 += num3;
					}
					string text = Convert.ToString((long)((ulong)num2), 16).ToUpper();
					if (text.Length > 2)
					{
						text = text.Substring(text.Length - 2, 2);
					}
					result = text;
				}
			}
			return result;
		}

		public static DateTime GetDateTimeFromSeconds1970(int seconds)
		{
			DateTime result;
			if (seconds <= 0)
			{
				result = Utils.TIMESTAMP_BASE_1970;
			}
			else
			{
				DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0);
				TimeSpan value = new TimeSpan(0, 0, seconds);
				DateTime dateTime2 = dateTime.Add(value);
				result = dateTime2;
			}
			return result;
		}

		public static double DegreesToRadians(double degrees)
		{
			return degrees * 3.1415926535897931 / 180.0;
		}

		public static double RadiansToDegrees(double radians)
		{
			return radians * 180.0 / 3.1415926535897931;
		}

		public static int GetNumOfOccurencesInString(string strToCheckIn, string valToCheckFor)
		{
			int num = 0;
			if (!string.IsNullOrEmpty(strToCheckIn) && !string.IsNullOrEmpty(valToCheckFor))
			{
				int length = strToCheckIn.Length;
				for (int i = 0; i < length; i++)
				{
					if (strToCheckIn[i].Equals(valToCheckFor.ToCharArray()[0]))
					{
						num++;
					}
				}
			}
			return num;
		}

		public static bool IsTextOnly(string strToCheck)
		{
			bool flag = true;
			int num = Convert.ToInt32('A');
			int num2 = Convert.ToInt32('Z');
			int num3 = Convert.ToInt32('a');
			int num4 = Convert.ToInt32('z');
			bool result;
			if (string.IsNullOrEmpty(strToCheck))
			{
				result = false;
			}
			else
			{
				char[] array = strToCheck.ToCharArray();
				int num5 = array.Length;
				for (int i = 0; i < num5; i++)
				{
					char value = array[i];
					int num6 = Convert.ToInt32(value);
					if ((num6 < num || num6 > num2) && (num6 < num3 || num6 > num4))
					{
						flag = false;
						break;
					}
				}
				result = flag;
			}
			return result;
		}

		public static bool IsNumerical(string strToCheck)
		{
			bool flag = true;
			int num = Convert.ToInt32('0');
			int num2 = Convert.ToInt32('9');
			bool result;
			if (string.IsNullOrEmpty(strToCheck))
			{
				result = false;
			}
			else
			{
				char[] array = strToCheck.ToCharArray();
				int num3 = array.Length;
				for (int i = 0; i < num3; i++)
				{
					char value = array[i];
					int num4 = Convert.ToInt32(value);
					if (num4 < num || num4 > num2)
					{
						flag = false;
						break;
					}
				}
				result = flag;
			}
			return result;
		}

		public static string Reverse(string original)
		{
			string result;
			if (original == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				if (string.IsNullOrEmpty(original))
				{
					result = text;
				}
				else
				{
					char[] array = original.ToCharArray();
					List<char> list = array.ToList<char>();
					list.Reverse();
					array = list.ToArray();
					int num = array.Length;
					for (int i = 0; i < num; i++)
					{
						char c = array[i];
						text += c.ToString();
					}
					result = text;
				}
			}
			return result;
		}

		public static string ConvertToIPString(uint ip)
		{
			return string.Concat(new object[]
			{
				ip >> 24 & 255u,
				".",
				ip >> 16 & 255u,
				".",
				ip >> 8 & 255u,
				".",
				ip & 255u
			});
		}

		public static uint ConvertToIP(string ip)
		{
			uint result = 0u;
			if (!string.IsNullOrEmpty(ip))
			{
				string[] array = ip.Split(new string[]
				{
					"."
				}, StringSplitOptions.None);
				if (array != null && array.Length == 4)
				{
					string s = array[0];
					string s2 = array[1];
					string s3 = array[2];
					string s4 = array[3];
					uint num = uint.Parse(s);
					uint num2 = uint.Parse(s2);
					uint num3 = uint.Parse(s3);
					uint num4 = uint.Parse(s4);
					result = (num << 24) + (num2 << 16) + (num3 << 8) + num4;
				}
			}
			return result;
		}
	}
}
