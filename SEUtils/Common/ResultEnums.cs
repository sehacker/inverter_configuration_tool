using System;

namespace SEUtils.Common
{
	public enum ResultEnums
	{
		SMALL_THAN = -1,
		EQUAL,
		GREATER_THAN
	}
}
