using System;
using System.Windows.Forms;

namespace SEInput.BarCode
{
	public class BarCodeChecker
	{
		private Keys[] _keys = new Keys[]
		{
			Keys.ShiftKey
		};

		private int _lastKeyRetrieved = -1;

		private int _numOfKeysRetreived = 0;

		private bool _didCodeCheckInitialized = false;

		private int _numOfMarkersArrived = 0;

		private string _accumulatedText = "";

		private bool _parsingFinished = false;

		public bool ParsingDone
		{
			get
			{
				return this._parsingFinished;
			}
		}

		public void Reset()
		{
			this._lastKeyRetrieved = -1;
			this._numOfKeysRetreived = 0;
			this._didCodeCheckInitialized = false;
			this._numOfMarkersArrived = 0;
			this._accumulatedText = "";
			this._parsingFinished = false;
		}

		public bool CheckBarCodeForIDAndCheckSum(KeyEventArgs e, ref string[] retVal)
		{
			Keys keyCode = e.KeyCode;
			int keyValue = e.KeyValue;
			if ((keyValue >= 65 && keyValue <= 90) || (keyValue >= 48 && keyValue <= 57) || (keyValue >= 96 && keyValue <= 105) || keyValue == 189)
			{
				string str = Convert.ToChar((int)keyCode).ToString();
				if (keyValue == 189)
				{
					str = "-";
				}
				this._accumulatedText += str;
			}
			if (!this._didCodeCheckInitialized)
			{
				if (this._numOfKeysRetreived < this._keys.Length)
				{
					if (keyCode == this._keys[this._numOfKeysRetreived])
					{
						this._numOfKeysRetreived++;
					}
					else
					{
						this._numOfKeysRetreived = 0;
					}
				}
				else
				{
					this._didCodeCheckInitialized = true;
				}
			}
			if (this._didCodeCheckInitialized)
			{
				if (this._numOfMarkersArrived < 2 && keyCode == Keys.OemMinus)
				{
					this._numOfMarkersArrived++;
				}
				if (this._numOfMarkersArrived == 2 && keyCode == Keys.Return)
				{
					this._didCodeCheckInitialized = false;
					string accumulatedText = this._accumulatedText;
					string[] array = accumulatedText.Split(new string[]
					{
						"-"
					}, StringSplitOptions.None);
					int arg_176_0 = (array != null) ? array.Length : 0;
					string text = array[1];
					string text2 = array[2];
					retVal = new string[]
					{
						text,
						text2
					};
					this._parsingFinished = true;
				}
			}
			return this._didCodeCheckInitialized;
		}
	}
}
