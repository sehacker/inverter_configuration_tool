using InverterConfigurationTool.Src.Model.Data.Base;
using InverterConfigurationTool.Src.Model.Devices.Base;
using SEUtils.Common;
using System;

namespace InverterConfigurationTool.Src.Model.Data
{
	public class SystemStatus : DeviceData
	{
		private string _inverterModel;

		private string _inverterID;

		private SWVersion _cpuVer;

		private SWVersion _dsp1Ver;

		private SWVersion _dsp2Ver;

		private bool _powerBalancingEnabled = false;

		public string InverterModel
		{
			get
			{
				string inverterModel;
				lock (this)
				{
					inverterModel = this._inverterModel;
				}
				return inverterModel;
			}
			set
			{
				lock (this)
				{
					if (this._inverterModel == null || !this._inverterModel.Equals(value))
					{
						this._inverterModel = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string InverterID
		{
			get
			{
				string inverterID;
				lock (this)
				{
					inverterID = this._inverterID;
				}
				return inverterID;
			}
			set
			{
				lock (this)
				{
					if (this._inverterID == null || !this._inverterID.Equals(value))
					{
						this._inverterID = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public SWVersion CPUVersion
		{
			get
			{
				SWVersion cpuVer;
				lock (this)
				{
					cpuVer = this._cpuVer;
				}
				return cpuVer;
			}
			set
			{
				lock (this)
				{
					if (this._cpuVer == null || !this._cpuVer.Equals(value))
					{
						this._cpuVer = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public SWVersion DSP1Version
		{
			get
			{
				SWVersion dsp1Ver;
				lock (this)
				{
					dsp1Ver = this._dsp1Ver;
				}
				return dsp1Ver;
			}
			set
			{
				lock (this)
				{
					if (this._dsp1Ver == null || !this._dsp1Ver.Equals(value))
					{
						this._dsp1Ver = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public SWVersion DSP2Version
		{
			get
			{
				SWVersion dsp2Ver;
				lock (this)
				{
					dsp2Ver = this._dsp2Ver;
				}
				return dsp2Ver;
			}
			set
			{
				lock (this)
				{
					if (this._dsp2Ver == null || !this._dsp2Ver.Equals(value))
					{
						this._dsp2Ver = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool PowerBalancing
		{
			get
			{
				bool powerBalancingEnabled;
				lock (this)
				{
					powerBalancingEnabled = this._powerBalancingEnabled;
				}
				return powerBalancingEnabled;
			}
			set
			{
				lock (this)
				{
					if (this._powerBalancingEnabled != value)
					{
						this._powerBalancingEnabled = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public SystemStatus(Inverter inverter) : base(inverter)
		{
		}
	}
}
