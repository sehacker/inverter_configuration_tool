using System;

namespace InverterConfigurationTool.Src.Model.Data
{
	public enum PowerBoxColumns
	{
		COL_POWER_BOX_PANEL_SERIAL_NUMBER,
		COL_PANEL_VOLTAGE,
		COL_PANEL_PB_VOLTAGE,
		COL_PANEL_CURRENT,
		COL_PANEL_POWER,
		COL_PANEL_TELEM_TIME,
		COL_PANEL_VER,
		COUNT
	}
}
