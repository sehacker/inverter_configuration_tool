using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Data.Base;
using InverterConfigurationTool.Src.Model.Devices.Base;
using InverterConfigurationTool.Src.Ui.Main;
using SEDevices.Devices;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace InverterConfigurationTool.Src.Model.Data
{
	public class PowerBoxData : DeviceData
	{
		private object _pbTableSynchObject = new object();

		private DataTable _pbTable = null;

		private DateTime _lastTelemUpdateTime = DateTime.Now;

		private InverterTelemetrySpeedType _telemTime = InverterTelemetrySpeedType.NORMAL;

		private List<SEMercury> _mercuryList;

		private object _mercuryListSyncObj = new object();

		private SEThread _thAddPB = null;

		private object _lastPBIndexSyncObj = new object();

		private int _lastPBIndexUpdated = -1;

		public int PowerBoxCount
		{
			get
			{
				int result = 0;
				if (this._pbTable != null)
				{
					result = this._pbTable.Rows.Count;
				}
				return result;
			}
		}

		public DateTime LastTelemUpdateTime
		{
			get
			{
				DateTime lastTelemUpdateTime;
				lock (this._pbTableSynchObject)
				{
					lastTelemUpdateTime = this._lastTelemUpdateTime;
				}
				return lastTelemUpdateTime;
			}
		}

		public InverterTelemetrySpeedType TelemetrySpeed
		{
			get
			{
				InverterTelemetrySpeedType telemTime;
				lock (this)
				{
					telemTime = this._telemTime;
				}
				return telemTime;
			}
			set
			{
				lock (this)
				{
					if (this._telemTime != value)
					{
						this._telemTime = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public List<SEMercury> MercuryList
		{
			get
			{
				return this._mercuryList;
			}
		}

		public int LastPBIndexUpdated
		{
			get
			{
				int lastPBIndexUpdated;
				lock (this._lastPBIndexSyncObj)
				{
					lastPBIndexUpdated = this._lastPBIndexUpdated;
				}
				return lastPBIndexUpdated;
			}
			set
			{
				lock (this._lastPBIndexSyncObj)
				{
					this._lastPBIndexUpdated = value;
				}
			}
		}

		public PowerBoxData(Inverter inverter) : base(inverter)
		{
			this._thAddPB = new SEThread("PowerBox add thread", new SEThread.ThreadFunctionCallBack(this.UpdateVersionThreadFunc), 500, 0u, 1u, true, true, true);
		}

		private void BuildPowerBoxTable()
		{
			lock (this._pbTableSynchObject)
			{
				this._pbTable = new DataTable();
				this._pbTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_POWER_BOX_PANEL_SERIAL_NUMBER.ToString(), typeof(string)));
				this._pbTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_VOLTAGE.ToString(), typeof(float)));
				this._pbTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_PB_VOLTAGE.ToString(), typeof(float)));
				this._pbTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_CURRENT.ToString(), typeof(float)));
				this._pbTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_POWER.ToString(), typeof(float)));
				this._pbTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_TELEM_TIME.ToString(), typeof(string)));
				this._pbTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_VER.ToString(), typeof(string)));
			}
		}

		public void Add(string panelSN, float panelV, float panelPBV, float panelI, float panelP, uint telemTime)
		{
			lock (this._pbTableSynchObject)
			{
				bool flag = true;
				if (this._pbTable == null)
				{
					this.BuildPowerBoxTable();
				}
				int index = -1;
				int count = this._pbTable.Rows.Count;
				for (int i = 0; i < count; i++)
				{
					DataRow dataRow = this._pbTable.Rows[i];
					object[] itemArray = dataRow.ItemArray;
					if (itemArray[0].ToString().Equals(panelSN))
					{
						flag = false;
						index = i;
						break;
					}
				}
				string text = "";
				if (telemTime > 0u)
				{
					try
					{
						DateTime date = Utils.Timestamp1970ToDate(telemTime);
						text = Utils.GetDateString(date, false, true);
					}
					catch (Exception var_8_BC)
					{
					}
					this._lastTelemUpdateTime = DateTime.Now;
				}
				string s = panelSN.Split(new string[]
				{
					"-"
				}, StringSplitOptions.None)[0];
				uint mercuryAddress = uint.Parse(s, NumberStyles.HexNumber);
				string text2 = "";
				if (this.IndexOfMercury(mercuryAddress) < 0)
				{
					this.AddMercury(mercuryAddress);
				}
				int num = this.IndexOfMercury(mercuryAddress);
				if (num > -1)
				{
					SEMercury sEMercury = this.MercuryList[num];
					text2 = sEMercury.DeviceSWVersion.ToString(FieldEnums.MINOR_ENUM);
				}
				object[] array = new object[]
				{
					panelSN,
					panelV,
					panelPBV,
					panelI,
					panelP,
					text,
					text2
				};
				if (flag)
				{
					this._pbTable.Rows.Add(array);
				}
				else
				{
					this._pbTable.Rows[index].ItemArray = array;
				}
				this.LastPBIndexUpdated = num;
				base.DataChanged = true;
			}
		}

		public void Add(uint panelSN, float panelV, float panelPBV, float panelI, float panelP)
		{
			this.Add(panelSN, panelV, panelPBV, panelI, panelP, Utils.DateToTimestamp1970(DateTime.Now));
		}

		public void Add(uint panelSN, float panelV, float panelPBV, float panelI, float panelP, uint telemTime)
		{
			string text = Convert.ToString((long)((ulong)panelSN), 16).ToUpper();
			string checkSum = Utils.GetCheckSum(text);
			string panelSN2 = text + "-" + checkSum;
			this.Add(panelSN2, panelV, panelPBV, panelI, panelP, telemTime);
		}

		public void Add(object[] rowData)
		{
			if (rowData != null && rowData.Length == 7)
			{
				try
				{
					string timeStr = (string)rowData[5];
					DateTime? dateTime = Utils.StringToTime(timeStr);
					uint telemTime = dateTime.HasValue ? Utils.DateToTimestamp1970(dateTime.Value) : 0u;
					this.Add((string)rowData[0], (float)rowData[1], (float)rowData[2], (float)rowData[3], (float)rowData[4], telemTime);
				}
				catch (Exception var_3_72)
				{
				}
			}
		}

		public DataTable Get()
		{
			DataTable result = null;
			lock (this._pbTableSynchObject)
			{
				result = ((this._pbTable != null) ? this._pbTable.Copy() : null);
			}
			return result;
		}

		public object[] GetLine(int index)
		{
			object[] result = null;
			if (this._pbTable != null && index > -1 && index < this._pbTable.Rows.Count)
			{
				DataRow dataRow = this._pbTable.Rows[index];
				result = dataRow.ItemArray;
			}
			return result;
		}

		public void Remove(uint panelSN)
		{
			string text = Convert.ToString((long)((ulong)panelSN), 16).ToUpper();
			string checkSum = Utils.GetCheckSum(text);
			text = text + "-" + checkSum;
			this.Remove(text);
		}

		public void Remove(string panelSN)
		{
			lock (this._pbTableSynchObject)
			{
				int count = this._pbTable.Rows.Count;
				int num = -1;
				for (int i = 0; i < count; i++)
				{
					DataRow dataRow = this._pbTable.Rows[i];
					object[] itemArray = dataRow.ItemArray;
					if (itemArray != null && itemArray[0].ToString().Equals(panelSN))
					{
						num = i;
						break;
					}
				}
				if (num > -1)
				{
					this._pbTable.Rows.RemoveAt(num);
					this.MercuryList.RemoveAt(num);
					base.DataChanged = true;
				}
				int num2 = num - 1;
				if (num < 0)
				{
				}
				this.LastPBIndexUpdated = num;
			}
		}

		public void UpdateVersionThreadFunc()
		{
			try
			{
				if (this.MercuryList != null)
				{
					MainForm instance = MainForm.Instance;
					int count = this.MercuryList.Count;
					string text = DataManager.Instance.Dictionary["MsgSendPBTelemSendTo"] + " ";
					instance.UpdateVersionString = text;
					for (int i = 0; i < count; i++)
					{
						object obj = this._pbTable.Rows[i].ItemArray[6];
						string text2 = (obj != null) ? obj.ToString() : null;
						if (string.IsNullOrEmpty(text2) || text2.Equals("0.0"))
						{
							SEMercury sEMercury = this.MercuryList[i];
							SWVersion sWVersion = sEMercury.Command_Device_Version(true);
							uint address = sEMercury.Address;
							string text3 = Convert.ToString((long)((ulong)address), 16).ToUpper();
							string checkSum = Utils.GetCheckSum(text3);
							string text4 = text3 + "-" + checkSum;
							object[] array = null;
							lock (this._pbTableSynchObject)
							{
								array = this._pbTable.Rows[i].ItemArray;
							}
							this._pbTable.Rows[i].ItemArray = new object[]
							{
								array[0],
								array[1],
								array[2],
								array[3],
								array[4],
								array[5],
								sWVersion.ToString(FieldEnums.MINOR_ENUM)
							};
							instance.UpdateVersionString = text + (i + 1).ToString();
						}
					}
					base.DataChanged = true;
					MainForm.Instance.EnableUpdateVersion = true;
					MainForm.Instance.EnableForceTelem = true;
				}
			}
			catch (Exception var_13_1D5)
			{
			}
		}

		public void Action_UpdateVersion()
		{
			if (this.MercuryList != null && this.MercuryList.Count > 0)
			{
				this._thAddPB.Pause = false;
			}
		}

		private void AddMercury(uint mercuryAddress)
		{
			lock (this._mercuryListSyncObj)
			{
				int num = this.IndexOfMercury(mercuryAddress);
				if (num < 0)
				{
					SEMercury item = new SEMercury(mercuryAddress, base.Owner.Portia.CommManager, base.Owner.PLCCommandableDevice, base.Owner, new SWVersion(0u));
					if (this.MercuryList == null)
					{
						this._mercuryList = new List<SEMercury>(0);
					}
					this.MercuryList.Add(item);
				}
			}
		}

		private void RemoveMercury(uint mercuryAddress)
		{
			lock (this._mercuryListSyncObj)
			{
				int num = (this.MercuryList != null) ? this.MercuryList.Count : 0;
				for (int i = 0; i < num; i++)
				{
					SEMercury sEMercury = this.MercuryList[i];
					if (sEMercury != null && sEMercury.Address == mercuryAddress)
					{
						this.MercuryList.Remove(sEMercury);
						break;
					}
				}
			}
		}

		private int IndexOfMercury(SEMercury mercury)
		{
			return this.IndexOfMercury(mercury.Address);
		}

		public int IndexOfMercury(uint mercuryAddress)
		{
			int result = -1;
			lock (this._mercuryListSyncObj)
			{
				if (this._mercuryList != null)
				{
					int count = this._mercuryList.Count;
					for (int i = 0; i < count; i++)
					{
						SEMercury sEMercury = this._mercuryList[i];
						if (sEMercury.Address == mercuryAddress)
						{
							result = i;
							break;
						}
					}
				}
			}
			return result;
		}
	}
}
