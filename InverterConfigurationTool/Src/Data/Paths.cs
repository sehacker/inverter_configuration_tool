using System;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Data
{
	public class Paths
	{
		public const string DATA_SOURCE_FILE_EXT_CONNECTOR = "|*";

		public const string DATA_SOURCE_DIC_FILE_TYPE = ".txt";

		public const string DATA_SOURCE_DEBUG_FILE_TYPE = ".txt";

		public const string DATA_SOURCE_TESTCASE_FILE_TYPE = ".txt";

		public const string DATA_SOURCE_XLS_FILE_TYPE = ".xls";

		public const string DATA_SOURCE_XLSX_FILE_TYPE = ".xlsx";

		public const string DATA_SOURCE_XLSM_FILE_TYPE = ".xlsm";

		public const string DATA_SOURCE_PRJ_FILE_TYPE = ".sedt";

		public const string IMAGE_SOURCE_PNG_FILE_TYPE = ".png";

		public const string IMAGE_SOURCE_JPEG_FILE_TYPE = ".jpeg";

		public const string IMAGE_SOURCE_BMP_FILE_TYPE = ".bmp";

		public const string IMAGE_SOURCE_GIF_FILE_TYPE = ".gif";

		public const string OUTPUT_SOURCE_PDF_FILE_TYPE = ".pdf";

		public const string CABINET_SETUP_FILE = ".csu";

		public const string AT_COMMANDS_CONFIG_FILE = ".atcc";

		public const string DATA_SOURCE_XLS = "InverterConfigurationTool.Resources.STDB.xls";

		public const string DATA_SOURCE_XLSX = "InverterConfigurationTool.Resources.STDB.xlsm";

		public const string DATA_SOURCE_XLSM = "InverterConfigurationTool.Resources.STDBM.xlsm";

		public const string DATA_SOURCE_DIC = "InverterConfigurationTool.Resources.Dictionary.txt";

		public const string DATA_SOURCE_XL_U_DIR = "Data";

		public const string DATA_SOURCE_PRJ_U_DIR = "Projects";

		public const string DATA_SOURCE_DEBUG_DIR = "Debug";

		public const string DATA_SOURCE_REPORT_DIR = "Reports";

		public const string DATA_SOURCE_XL_U = "STDBUser";

		public const string DATA_SOURCE_DEBUG = "DebugVals";

		public const string MANIFEST_RESOURCES = "InverterConfigurationTool.Resources.";

		public const string MANIFEST_DICTIONARY = "Dictionary";

		public static string PATH_APP_COMMON_DATA = Application.CommonAppDataPath;

		public static string PATH_APP_STARTUP = Application.StartupPath;

		public static string PATH_APP_EXEC = Application.ExecutablePath;

		public static string PATH_APP_USER_DATA = Application.UserAppDataPath;

		public static string PATH_APP_USER_DATA_LOCAL = Application.LocalUserAppDataPath;

		public static string DATA_SOURCE_PATH = Paths.PATH_APP_USER_DATA_LOCAL.Substring(0, Paths.PATH_APP_USER_DATA_LOCAL.IndexOf("Inverter Configuration Tool") + "Inverter Configuration Tool".Length);
	}
}
