using InverterConfigurationTool.Src.Model.Devices.Base;
using System;

namespace InverterConfigurationTool.Src.Model.Data.Base
{
	public class DeviceData
	{
		private Inverter _owner;

		private bool _dataChanged = true;

		protected Inverter Owner
		{
			get
			{
				return this._owner;
			}
		}

		public bool DataChanged
		{
			get
			{
				bool dataChanged;
				lock (this)
				{
					dataChanged = this._dataChanged;
				}
				return dataChanged;
			}
			set
			{
				lock (this)
				{
					this._dataChanged = value;
				}
			}
		}

		public DeviceData(Inverter owner)
		{
			this._owner = owner;
		}
	}
}
