using InverterConfigurationTool.Src.Model.Data.Base;
using InverterConfigurationTool.Src.Model.Devices.Base;
using System;

namespace InverterConfigurationTool.Src.Model.Data
{
	public class RegionalData : DeviceData
	{
		private int _langaugeIndex;

		private string _language;

		private string _countryName;

		private string _countryShortName;

		private int _countryNumber;

		private int _zone;

		private DateTime _rtcTime = DateTime.Now;

		private DateTime _currentTime = DateTime.Now;

		private int _gmtOffset = 0;

		public int LanguageIndex
		{
			get
			{
				int langaugeIndex;
				lock (this)
				{
					langaugeIndex = this._langaugeIndex;
				}
				return langaugeIndex;
			}
			set
			{
				lock (this)
				{
					if (this._langaugeIndex != value)
					{
						this._langaugeIndex = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string Language
		{
			get
			{
				string language;
				lock (this)
				{
					language = this._language;
				}
				return language;
			}
			set
			{
				lock (this)
				{
					if (this._language == null || !this._language.Equals(value))
					{
						this._language = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string CountryName
		{
			get
			{
				string countryName;
				lock (this)
				{
					countryName = this._countryName;
				}
				return countryName;
			}
			set
			{
				lock (this)
				{
					if (this._countryName == null || !this._countryName.Equals(value))
					{
						this._countryName = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string CountryShortName
		{
			get
			{
				string countryShortName;
				lock (this)
				{
					countryShortName = this._countryShortName;
				}
				return countryShortName;
			}
			set
			{
				lock (this)
				{
					if (this._countryShortName == null || !this._countryShortName.Equals(value))
					{
						this._countryShortName = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int CountryIndex
		{
			get
			{
				int countryNumber;
				lock (this)
				{
					countryNumber = this._countryNumber;
				}
				return countryNumber;
			}
			set
			{
				lock (this)
				{
					if (this._countryNumber != value)
					{
						this._countryNumber = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int CountryZone
		{
			get
			{
				int zone;
				lock (this)
				{
					zone = this._zone;
				}
				return zone;
			}
			set
			{
				lock (this)
				{
					if (this._zone != value)
					{
						this._zone = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public DateTime RTCTime
		{
			get
			{
				DateTime rtcTime;
				lock (this)
				{
					rtcTime = this._rtcTime;
				}
				return rtcTime;
			}
			set
			{
				lock (this)
				{
					if (this._rtcTime != value)
					{
						this._rtcTime = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public DateTime CurrentTime
		{
			get
			{
				DateTime currentTime;
				lock (this)
				{
					currentTime = this._currentTime;
				}
				return currentTime;
			}
			set
			{
				lock (this)
				{
					if (this._currentTime != value)
					{
						this._currentTime = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int GMTOffset
		{
			get
			{
				int gmtOffset;
				lock (this)
				{
					gmtOffset = this._gmtOffset;
				}
				return gmtOffset;
			}
			set
			{
				lock (this)
				{
					if (this._gmtOffset != value)
					{
						this._gmtOffset = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public RegionalData(Inverter inverter) : base(inverter)
		{
		}

		public void CopyData(RegionalData dataToCopyFrom)
		{
			this._langaugeIndex = dataToCopyFrom.LanguageIndex;
			this._language = dataToCopyFrom.Language;
			this._countryNumber = dataToCopyFrom.CountryIndex;
			this._countryName = dataToCopyFrom.CountryName;
			this._countryShortName = dataToCopyFrom.CountryShortName;
			this._zone = dataToCopyFrom.CountryZone;
			this._currentTime = dataToCopyFrom.CurrentTime;
			this._rtcTime = dataToCopyFrom.RTCTime;
			this._gmtOffset = dataToCopyFrom.GMTOffset;
		}
	}
}
