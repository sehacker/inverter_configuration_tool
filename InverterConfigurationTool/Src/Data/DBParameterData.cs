using SEDevices.Data;
using SEDevices.Devices;
using SESecurity;
using SEStorage;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace InverterConfigurationTool.Src.Model.Data
{
	public class DBParameterData : DBExcel
	{
		private enum ColumnParameters
		{
			NAME,
			DATA_TYPE,
			UNIT_KEY,
			MIN,
			MAX,
			RATE,
			EQ_A,
			EQ_B,
			WRITE_ACCESS,
			VIEW_ACCESS,
			COUNT
		}

		private enum ColumnEnums
		{
			HEADER_NAME = 7,
			HEADER_COUNTER,
			NUM = 1,
			VER_PORTIA,
			VER_VENUS,
			VER_MERCURY,
			VER_JUPITER,
			VER_GEMINI,
			NAME,
			ACCESS_LEVEL_LEVEL,
			SETUP_COUNTRY_SHORT = 8,
			SETUP_COUNTRY_ZONE
		}

		private enum RowStartParameters
		{
			ACCESS_LEVELS_START = 5,
			ETH_CONNECTED_START = 10,
			GSM_MODEM_TYPE_START = 16,
			POLESTAR_MODE_START = 21,
			RS_232_TO_SERVER_MODE_START = 27,
			SERVER_COM_MODE_START = 31,
			SETUP_COUNTRY_START = 35,
			SETUP_LANGUAGE_START = 69,
			SETUP_TEMPERATURE_DISPALY_START = 76,
			STREAM_POLESTARS_START = 80,
			STREAM_SERVER_START = 86,
			ZONE_LIST_START = 93,
			COUNT
		}

		private enum RowCountEnums
		{
			ACCESS_LEVELS = 3,
			ETH_CONNECTED = 8,
			GSM_MODEM_TYPE = 14,
			POLESTAR_MODE = 19,
			RS_232_TO_SERVER_MODE = 25,
			SERVER_COM_MODE = 29,
			SETUP_COUNTRY = 33,
			SETUP_LANGUAGE = 67,
			SETUP_TEMPERATURE_DISPALY = 74,
			STREAM_POLESTARS = 78,
			STREAM_SERVER = 84,
			ZONE_LIST = 91
		}

		private const string WS_NAME_DBMAIN = "DBMain";

		private const string WS_NAME_PORTIA_PARAMS_DATA = "Enum_Params(Portia)";

		private const string WS_NAME_VENUS_PARAMS_DATA = "Enum_Params(Venus)";

		private const string WS_NAME_MERCURY_PARAMS_DATA = "Enum_Params(Mercury)";

		private const string WS_NAME_JUPITER_PARAMS_DATA = "Enum_Params(Jupiter)";

		private const string WS_NAME_GEMINI_PARAMS_DATA = "Enum_Params(Gemini)";

		private const string WS_NAME_ENUM_EXTRA_DATA = "EnumExpandedList";

		private const string START_SUF = "_START";

		private const int ROW_DBMAIN_PARAMS_START = 1;

		private const int ROW_DBMAIN_COUNT_PARAMS_PORTIA = 3;

		private const int ROW_DBMAIN_COUNT_PARAMS_VENUS = 4;

		private const int ROW_DBMAIN_COUNT_PARAMS_MERCURY = 5;

		private const int ROW_DBMAIN_COUNT_PARAMS_JUPITER = 6;

		private const int ROW_DBMAIN_COUNT_PARAMS_GEMINI = 7;

		private const int COL_DBMAIN_COUNT_PARAMS = 1;

		private const int ROW_PARAMS_DATA_START_ROW = 1;

		private static DBParameterData _instance = null;

		public static DBParameterData Instance
		{
			get
			{
				if (DBParameterData._instance == null)
				{
					string name = "InverterConfigurationTool.Resources.Databases.SEParamDataTable.xlsm";
					Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
					DBParameterData._instance = new DBParameterData(manifestResourceStream);
				}
				return DBParameterData._instance;
			}
		}

		private DBParameterData(Stream fileStream) : base(fileStream, FileTypes.XLSX)
		{
		}

		private DBParameterData(string filePath) : base(filePath, FileTypes.XLSX)
		{
		}

		public int GetCountParamsPortia()
		{
			object cell = base.GetCell("DBMain", 3, 1);
			return (cell != null) ? int.Parse(cell.ToString()) : 0;
		}

		public int GetCountParamsVenus()
		{
			object cell = base.GetCell("DBMain", 4, 1);
			return (cell != null) ? int.Parse(cell.ToString()) : 0;
		}

		public int GetCountParamsMercury()
		{
			object cell = base.GetCell("DBMain", 5, 1);
			return (cell != null) ? int.Parse(cell.ToString()) : 0;
		}

		public int GetCountParamsJupiter()
		{
			object cell = base.GetCell("DBMain", 6, 1);
			return (cell != null) ? int.Parse(cell.ToString()) : 0;
		}

		public int GetCountParamsGemini()
		{
			object cell = base.GetCell("DBMain", 7, 1);
			return (cell != null) ? int.Parse(cell.ToString()) : 0;
		}

		public int GetCountInnerEnum(InnerEnumTypes type)
		{
			int result = 0;
			if (type != InnerEnumTypes.COUNT)
			{
				int row = (int)Enum.Parse(typeof(DBParameterData.RowCountEnums), type.ToString());
				object cell = base.GetCell("EnumExpandedList", row, 8);
				string text = (cell != null) ? cell.ToString() : null;
				result = ((!string.IsNullOrEmpty(text)) ? int.Parse(text) : 0);
			}
			return result;
		}

		public List<string> GetEnumListByType(InnerEnumTypes type, SWVersion version, DeviceType deviceType)
		{
			List<string> list = null;
			List<string> result;
			if (type == InnerEnumTypes.COUNT)
			{
				result = list;
			}
			else
			{
				int num = (int)Enum.Parse(typeof(DBParameterData.RowStartParameters), type.ToString() + "_START");
				int countInnerEnum = this.GetCountInnerEnum(type);
				int num2 = num + countInnerEnum;
				int colummByInnerType = this.GetColummByInnerType(type, deviceType);
				for (int i = num; i < num2; i++)
				{
					object cell = base.GetCell("EnumExpandedList", i, 7);
					object cell2 = base.GetCell("EnumExpandedList", i, colummByInnerType);
					string version2 = (cell2 != null) ? cell2.ToString() : null;
					SWVersion v = new SWVersion(version2);
					if (cell != null && v <= version)
					{
						if (list == null)
						{
							list = new List<string>(0);
						}
						string item = cell.ToString();
						list.Add(item);
					}
				}
				result = list;
			}
			return result;
		}

		public int? GetEnumRealIndexByPosition(InnerEnumTypes type, uint position)
		{
			int? num = null;
			int? result;
			if (type == InnerEnumTypes.COUNT || position < 0u)
			{
				result = num;
			}
			else
			{
				int num2 = (int)Enum.Parse(typeof(DBParameterData.RowStartParameters), type.ToString() + "_START");
				int countInnerEnum = this.GetCountInnerEnum(type);
				if ((ulong)position >= (ulong)((long)countInnerEnum))
				{
					result = num;
				}
				else
				{
					object cell = base.GetCell("EnumExpandedList", num2 + (int)position, 1);
					num = ((cell != null) ? new int?(int.Parse(cell.ToString())) : null);
					result = num;
				}
			}
			return result;
		}

		public int? GetEnumRealIndexByName(InnerEnumTypes type, string name)
		{
			int? num = null;
			int? result;
			if (type == InnerEnumTypes.COUNT || string.IsNullOrEmpty(name))
			{
				result = num;
			}
			else
			{
				int num2 = (int)Enum.Parse(typeof(DBParameterData.RowStartParameters), type.ToString() + "_START");
				int countInnerEnum = this.GetCountInnerEnum(type);
				int num3 = num2 + countInnerEnum;
				for (int i = num2; i < num3; i++)
				{
					object cell = base.GetCell("EnumExpandedList", i, 7);
					string text = (cell != null) ? cell.ToString() : null;
					if (!string.IsNullOrEmpty(text) && text.Equals(name))
					{
						object cell2 = base.GetCell("EnumExpandedList", i, 1);
						num = ((cell2 != null) ? new int?(int.Parse(cell2.ToString())) : null);
						break;
					}
				}
				result = num;
			}
			return result;
		}

		public uint GetEnumPositionByRealIndex(InnerEnumTypes type, int realIndex)
		{
			uint num = 0u;
			uint result;
			if (type == InnerEnumTypes.COUNT || realIndex < 0)
			{
				result = num;
			}
			else
			{
				int num2 = (int)Enum.Parse(typeof(DBParameterData.RowStartParameters), type.ToString() + "_START");
				int countInnerEnum = this.GetCountInnerEnum(type);
				int num3 = num2 + countInnerEnum;
				for (int i = num2; i < num3; i++)
				{
					object cell = base.GetCell("EnumExpandedList", i, 1);
					uint? num4 = (cell != null) ? new uint?((uint)int.Parse(cell.ToString())) : null;
					if (num4.HasValue && num4.Value == (uint)realIndex)
					{
						num = (uint)(i - num2);
						break;
					}
				}
				result = num;
			}
			return result;
		}

		public string GetInnerEnumName(InnerEnumTypes type, int realIndex)
		{
			string text = null;
			string result;
			if (type == InnerEnumTypes.COUNT || realIndex < 0)
			{
				result = text;
			}
			else
			{
				uint enumPositionByRealIndex = this.GetEnumPositionByRealIndex(type, realIndex);
				int num = (int)Enum.Parse(typeof(DBParameterData.RowStartParameters), type.ToString() + "_START");
				object cell = base.GetCell("EnumExpandedList", num + (int)enumPositionByRealIndex, 7);
				text = ((cell != null) ? cell.ToString() : null);
				result = text;
			}
			return result;
		}

		public object GetParameterMinValuePortia(ushort virtualIndex)
		{
			return this.GetParameterMinValue("Enum_Params(Portia)", virtualIndex);
		}

		public object GetParameterMinValueVenus(ushort virtualIndex)
		{
			return this.GetParameterMinValue("Enum_Params(Venus)", virtualIndex);
		}

		public object GetParameterMinValueMercury(ushort virtualIndex)
		{
			return this.GetParameterMinValue("Enum_Params(Mercury)", virtualIndex);
		}

		public object GetParameterMinValueJupiter(ushort virtualIndex)
		{
			return this.GetParameterMinValue("Enum_Params(Jupiter)", virtualIndex);
		}

		public object GetParameterMinValueGemini(ushort virtualIndex)
		{
			return this.GetParameterMinValue("Enum_Params(Gemini)", virtualIndex);
		}

		public object GetParameterMaxValuePortia(ushort virtualIndex)
		{
			return this.GetParameterMaxValue("Enum_Params(Portia)", virtualIndex);
		}

		public object GetParameterMaxValueVenus(ushort virtualIndex)
		{
			return this.GetParameterMaxValue("Enum_Params(Venus)", virtualIndex);
		}

		public object GetParameterMaxValueMercury(ushort virtualIndex)
		{
			return this.GetParameterMaxValue("Enum_Params(Mercury)", virtualIndex);
		}

		public object GetParameterMaxValueJupiter(ushort virtualIndex)
		{
			return this.GetParameterMaxValue("Enum_Params(Jupiter)", virtualIndex);
		}

		public object GetParameterMaxValueGemini(ushort virtualIndex)
		{
			return this.GetParameterMaxValue("Enum_Params(Gemini)", virtualIndex);
		}

		public Type GetTypeOfParamPortia(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_Params(Portia)", virtualIndex);
		}

		public Type GetTypeOfParamVenus(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_Params(Venus)", virtualIndex);
		}

		public Type GetTypeOfParamMercury(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_Params(Mercury)", virtualIndex);
		}

		public Type GetTypeOfParamJupiter(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_Params(Jupiter)", virtualIndex);
		}

		public Type GetTypeOfParamGemini(ushort virtualIndex)
		{
			return this.GetTypeOfParam("Enum_Params(Gemini)", virtualIndex);
		}

		public bool IsParameterAllowedForViewPortia(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForView("Enum_Params(Portia)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForViewVenus(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForView("Enum_Params(Venus)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForViewMercury(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForView("Enum_Params(Mercury)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForViewJupiter(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForView("Enum_Params(Jupiter)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForViewGemini(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForView("Enum_Params(Gemini)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForWritingPortia(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForWriting("Enum_Params(Portia)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForWritingVenus(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForWriting("Enum_Params(Venus)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForWritingMercury(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForWriting("Enum_Params(Mercury)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForWritingJupiter(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForWriting("Enum_Params(Jupiter)", virtualIndex, currentLevel);
		}

		public bool IsParameterAllowedForWritingGemini(ushort virtualIndex, AccessLevels currentLevel)
		{
			return this.IsParameterAllowedForWriting("Enum_Params(Gemini)", virtualIndex, currentLevel);
		}

		public List<ushort> GetPortiaParamListToView(AccessLevels currentLevel)
		{
			return this.GetParamListToView(currentLevel, "Enum_Params(Portia)");
		}

		public List<ushort> GetVenusParamListToView(AccessLevels currentLevel)
		{
			return this.GetParamListToView(currentLevel, "Enum_Params(Venus)");
		}

		public List<ushort> GetMercuryParamListToView(AccessLevels currentLevel)
		{
			return this.GetParamListToView(currentLevel, "Enum_Params(Mercury)");
		}

		public List<ushort> GetJupiterParamListToView(AccessLevels currentLevel)
		{
			return this.GetParamListToView(currentLevel, "Enum_Params(Jupiter)");
		}

		public List<ushort> GetGeminiParamListToView(AccessLevels currentLevel)
		{
			return this.GetParamListToView(currentLevel, "Enum_Params(Gemini)");
		}

		public AccessLevels GetPortiaParamAccessLevelForWriting(ushort virtualIndex)
		{
			return this.GetParamAccessLevelForWriting(virtualIndex, "Enum_Params(Portia)");
		}

		public AccessLevels GetVenusParamAccessLevelForWriting(ushort virtualIndex)
		{
			return this.GetParamAccessLevelForWriting(virtualIndex, "Enum_Params(Venus)");
		}

		public AccessLevels GetMercuryParamAccessLevelForWriting(ushort virtualIndex)
		{
			return this.GetParamAccessLevelForWriting(virtualIndex, "Enum_Params(Mercury)");
		}

		public AccessLevels GetJupiterParamAccessLevelForWriting(ushort virtualIndex)
		{
			return this.GetParamAccessLevelForWriting(virtualIndex, "Enum_Params(Jupiter)");
		}

		public AccessLevels GetGeminiParamAccessLevelForWriting(ushort virtualIndex)
		{
			return this.GetParamAccessLevelForWriting(virtualIndex, "Enum_Params(Gemini)");
		}

		private int GetColumnIndexByDeviceType(DeviceType deviceType)
		{
			int result = 0;
			switch (deviceType)
			{
			case DeviceType.PORTIA:
				result = 2;
				break;
			case DeviceType.VENUS:
				result = 3;
				break;
			case DeviceType.MERCURY:
				result = 4;
				break;
			case DeviceType.JUPITER:
				result = 5;
				break;
			case DeviceType.GEMINI:
				result = 6;
				break;
			}
			return result;
		}

		public Dictionary<string, int> GetCountryList(SWVersion dspVersion, DeviceType deviceType, Zones zone)
		{
			Dictionary<string, int> dictionary = null;
			List<int> list = new List<int>(0);
			if ((zone & Zones.North_America) > Zones.None)
			{
				list.Add(1);
			}
			if ((zone & Zones.Europe) > Zones.None)
			{
				list.Add(2);
			}
			if ((zone & Zones.APAC) > Zones.None)
			{
				list.Add(4);
			}
			if ((zone & Zones.All) > Zones.None)
			{
				list.Add(8);
			}
			Dictionary<string, int> result;
			if (list.Count <= 0)
			{
				result = null;
			}
			else
			{
				object cell = base.GetCell("EnumExpandedList", 33, 8);
				int num = (cell != null) ? int.Parse(cell.ToString()) : 0;
				if (num == 0)
				{
					result = null;
				}
				else
				{
					dictionary = new Dictionary<string, int>(0);
					int num2 = 35 + num;
					string version = dspVersion.ToString(FieldEnums.BUILD_ENUM);
					SWVersion v = new SWVersion(version);
					int columnIndexByDeviceType = this.GetColumnIndexByDeviceType(deviceType);
					for (int i = 35; i < num2; i++)
					{
						object cell2 = base.GetCell("EnumExpandedList", i, columnIndexByDeviceType);
						if (cell2 != null)
						{
							string version2 = cell2.ToString();
							SWVersion v2 = new SWVersion(version2);
							if (v2 <= v)
							{
								try
								{
									int item = int.Parse(base.GetCell("EnumExpandedList", i, 9).ToString());
									if (list.Contains(item) || list.Contains(8))
									{
										int value = int.Parse(base.GetCell("EnumExpandedList", i, 1).ToString());
										string key = base.GetCell("EnumExpandedList", i, 7).ToString();
										dictionary.Add(key, value);
									}
								}
								catch (Exception var_15_1A9)
								{
								}
							}
						}
					}
					result = dictionary;
				}
			}
			return result;
		}

		public string GetCountryNameByIndex(int countryIndex)
		{
			string text = null;
			object cell = base.GetCell("EnumExpandedList", 33, 8);
			int num = (cell != null) ? int.Parse(cell.ToString()) : 0;
			string result;
			if (num == 0)
			{
				result = null;
			}
			else
			{
				if (countryIndex >= 0 && countryIndex < num)
				{
					object cell2 = base.GetCell("EnumExpandedList", 35 + countryIndex, 7);
					text = ((cell2 != null) ? cell2.ToString() : null);
				}
				result = text;
			}
			return result;
		}

		public string GetCountryShortNameByIndex(int countryIndex)
		{
			string text = null;
			object cell = base.GetCell("EnumExpandedList", 33, 8);
			int num = (cell != null) ? int.Parse(cell.ToString()) : 0;
			string result;
			if (num == 0)
			{
				result = null;
			}
			else
			{
				if (countryIndex >= 0 && countryIndex < num)
				{
					object cell2 = base.GetCell("EnumExpandedList", 35 + countryIndex, 8);
					text = ((cell2 != null) ? cell2.ToString() : null);
				}
				result = text;
			}
			return result;
		}

		public int GetCountryZoneByIndex(int countryIndex)
		{
			int num = -1;
			object cell = base.GetCell("EnumExpandedList", 33, 8);
			int num2 = (cell != null) ? int.Parse(cell.ToString()) : 0;
			int result;
			if (num2 == 0)
			{
				result = -1;
			}
			else
			{
				if (countryIndex >= 0 && countryIndex < num2)
				{
					object cell2 = base.GetCell("EnumExpandedList", 35 + countryIndex, 9);
					num = ((cell2 != null) ? int.Parse(cell2.ToString()) : -1);
				}
				result = num;
			}
			return result;
		}

		public int GetCountryIndexByName(string countryName)
		{
			int num = -1;
			int result;
			if (string.IsNullOrEmpty(countryName))
			{
				result = num;
			}
			else
			{
				object cell = base.GetCell("EnumExpandedList", 33, 8);
				int num2 = (cell != null) ? int.Parse(cell.ToString()) : 0;
				if (num2 == 0)
				{
					result = -1;
				}
				else
				{
					int num3 = 35;
					int num4 = num3 + num2;
					for (int i = num3; i < num4; i++)
					{
						object cell2 = base.GetCell("EnumExpandedList", i, 7);
						if (cell2 != null && cell2.ToString().Equals(countryName))
						{
							num = i - num3;
							break;
						}
					}
					result = num;
				}
			}
			return result;
		}

		public int GetCountryIndexByShortName(string shortName)
		{
			int num = -1;
			int result;
			if (string.IsNullOrEmpty(shortName))
			{
				result = num;
			}
			else
			{
				object cell = base.GetCell("EnumExpandedList", 33, 8);
				int num2 = (cell != null) ? int.Parse(cell.ToString()) : 0;
				if (num2 == 0)
				{
					result = -1;
				}
				else
				{
					int num3 = 35;
					int num4 = num3 + num2;
					for (int i = num3; i < num4; i++)
					{
						object cell2 = base.GetCell("EnumExpandedList", i, 8);
						if (cell2 != null && cell2.ToString().Equals(shortName))
						{
							num = i - num3;
							break;
						}
					}
					result = num;
				}
			}
			return result;
		}

		public List<string> GetLanguageList(SWVersion version)
		{
			object cell = base.GetCell("EnumExpandedList", 67, 8);
			string text = (cell != null) ? cell.ToString() : null;
			int num = (!string.IsNullOrEmpty(text)) ? int.Parse(text.ToString()) : 0;
			List<string> result;
			if (num == 0)
			{
				result = null;
			}
			else
			{
				List<string> list = new List<string>(0);
				int num2 = 69 + num;
				string version2 = version.ToString(FieldEnums.BUILD_ENUM);
				SWVersion v = new SWVersion(version2);
				for (int i = 69; i < num2; i++)
				{
					string version3 = base.GetCell("EnumExpandedList", i, 2).ToString();
					SWVersion v2 = new SWVersion(version3);
					if (v2 <= v)
					{
						string item = base.GetCell("EnumExpandedList", i, 7).ToString();
						list.Add(item);
					}
				}
				result = list;
			}
			return result;
		}

		public string GetLanguageNameByIndex(int languageIndex)
		{
			string text = null;
			object cell = base.GetCell("EnumExpandedList", 67, 8);
			int num = (cell != null) ? int.Parse(cell.ToString()) : 0;
			string result;
			if (num == 0)
			{
				result = null;
			}
			else
			{
				if (languageIndex >= 0 && languageIndex < num)
				{
					object cell2 = base.GetCell("EnumExpandedList", 69 + languageIndex, 7);
					text = ((cell2 != null) ? cell2.ToString() : null);
				}
				result = text;
			}
			return result;
		}

		public int GetLanguageIndexByName(string languageName)
		{
			int num = -1;
			int result;
			if (string.IsNullOrEmpty(languageName))
			{
				result = num;
			}
			else
			{
				object cell = base.GetCell("EnumExpandedList", 67, 8);
				int num2 = (cell != null) ? int.Parse(cell.ToString()) : -1;
				int num3 = 69;
				int num4 = num3 + num2;
				for (int i = num3; i < num4; i++)
				{
					object cell2 = base.GetCell("EnumExpandedList", i, 7);
					if (cell2 != null && cell2.ToString().Equals(languageName))
					{
						num = i - num3;
						break;
					}
				}
				result = num;
			}
			return result;
		}

		public void PopulateRegionalData(RegionalData data)
		{
			if (data != null)
			{
				int countryIndex = data.CountryIndex;
				int languageIndex = data.LanguageIndex;
				data.Language = this.GetLanguageNameByIndex(languageIndex);
				data.CountryName = this.GetCountryNameByIndex(countryIndex);
				data.CountryShortName = this.GetCountryShortNameByIndex(countryIndex);
				data.CountryZone = this.GetCountryZoneByIndex(countryIndex);
			}
		}

		public float? GetParamConversionRatePortia(ushort virtualIndex)
		{
			return this.GetParamConversionRate("Enum_Params(Portia)", virtualIndex);
		}

		public float? GetParamConversionRateVenus(ushort virtualIndex)
		{
			return this.GetParamConversionRate("Enum_Params(Venus)", virtualIndex);
		}

		public float? GetParamConversionRateMercury(ushort virtualIndex)
		{
			return this.GetParamConversionRate("Enum_Params(Mercury)", virtualIndex);
		}

		public float? GetParamConversionRateJupiter(ushort virtualIndex)
		{
			return this.GetParamConversionRate("Enum_Params(Jupiter)", virtualIndex);
		}

		public float? GetParamConversionRateGemini(ushort virtualIndex)
		{
			return this.GetParamConversionRate("Enum_Params(Gemini)", virtualIndex);
		}

		public float? GetParamEqAPortia(ushort virtualIndex)
		{
			return this.GetParamEqA("Enum_Params(Portia)", virtualIndex);
		}

		public float? GetParamEqAVenus(ushort virtualIndex)
		{
			return this.GetParamEqA("Enum_Params(Venus)", virtualIndex);
		}

		public float? GetParamEqAMercury(ushort virtualIndex)
		{
			return this.GetParamEqA("Enum_Params(Mercury)", virtualIndex);
		}

		public float? GetParamEqAJupiter(ushort virtualIndex)
		{
			return this.GetParamEqA("Enum_Params(Jupiter)", virtualIndex);
		}

		public float? GetParamEqAGemini(ushort virtualIndex)
		{
			return this.GetParamEqA("Enum_Params(Gemini)", virtualIndex);
		}

		public float? GetParamEqBPortia(ushort virtualIndex)
		{
			return this.GetParamEqB("Enum_Params(Portia)", virtualIndex);
		}

		public float? GetParamEqBVenus(ushort virtualIndex)
		{
			return this.GetParamEqB("Enum_Params(Venus)", virtualIndex);
		}

		public float? GetParamEqBMercury(ushort virtualIndex)
		{
			return this.GetParamEqB("Enum_Params(Mercury)", virtualIndex);
		}

		public float? GetParamEqBJupiter(ushort virtualIndex)
		{
			return this.GetParamEqB("Enum_Params(Jupiter)", virtualIndex);
		}

		public float? GetParamEqBGemini(ushort virtualIndex)
		{
			return this.GetParamEqB("Enum_Params(Gemini)", virtualIndex);
		}

		public string GetParamUnitKeyPortia(ushort virtualIndex)
		{
			return this.GetParamUnitKey("Enum_Params(Portia)", virtualIndex);
		}

		public string GetParamUnitKeyVenus(ushort virtualIndex)
		{
			return this.GetParamUnitKey("Enum_Params(Venus)", virtualIndex);
		}

		public string GetParamUnitKeyMercury(ushort virtualIndex)
		{
			return this.GetParamUnitKey("Enum_Params(Mercury)", virtualIndex);
		}

		public string GetParamUnitKeyJupiter(ushort virtualIndex)
		{
			return this.GetParamUnitKey("Enum_Params(Jupiter)", virtualIndex);
		}

		public string GetParamUnitKeyGemini(ushort virtualIndex)
		{
			return this.GetParamUnitKey("Enum_Params(Gemini)", virtualIndex);
		}

		private float? GetParamConversionRate(string wsName, ushort virtualIndex)
		{
			float? num = null;
			object cell = base.GetCell(wsName, (int)(1 + virtualIndex), 5);
			string text = (cell != null) ? cell.ToString() : null;
			return (!string.IsNullOrEmpty(text)) ? new float?(float.Parse(text)) : null;
		}

		private float? GetParamEqA(string wsName, ushort virtualIndex)
		{
			float? num = null;
			object cell = base.GetCell(wsName, (int)(1 + virtualIndex), 6);
			string text = (cell != null) ? cell.ToString() : null;
			return (!string.IsNullOrEmpty(text)) ? new float?(float.Parse(text)) : null;
		}

		private float? GetParamEqB(string wsName, ushort virtualIndex)
		{
			float? num = null;
			object cell = base.GetCell(wsName, (int)(1 + virtualIndex), 7);
			string text = (cell != null) ? cell.ToString() : null;
			return (!string.IsNullOrEmpty(text)) ? new float?(float.Parse(text)) : null;
		}

		private int GetColummByInnerType(InnerEnumTypes type, DeviceType deviceType)
		{
			int result = -1;
			switch (type)
			{
			case InnerEnumTypes.ACCESS_LEVEL:
			case InnerEnumTypes.ETH_CONNECTED:
			case InnerEnumTypes.GSM_MODEM_TYPE:
			case InnerEnumTypes.POLESTAR_MODE:
			case InnerEnumTypes.RS_232_TO_SERVER_MODE:
			case InnerEnumTypes.SERVER_COM_MODE:
			case InnerEnumTypes.SETUP_LANGUAGE:
			case InnerEnumTypes.SETUP_TEMPERATURE_DISPALY:
			case InnerEnumTypes.STREAM_POLESTARS:
			case InnerEnumTypes.STREAM_SERVER:
			case InnerEnumTypes.ZONE_LIST:
				result = this.GetColumnIndexByDeviceType(DeviceType.PORTIA);
				break;
			case InnerEnumTypes.SETUP_COUNTRY:
				result = this.GetColumnIndexByDeviceType(deviceType);
				break;
			}
			return result;
		}

		private object GetObjectByType(object val, Type type)
		{
			object obj = null;
			object result;
			if (val == null)
			{
				result = obj;
			}
			else
			{
				string text = val.ToString();
				if (type.Equals(typeof(string)))
				{
					obj = text;
				}
				else
				{
					obj = double.Parse(text);
				}
				result = obj;
			}
			return result;
		}

		private object GetParameterMinValue(string worksheetName, ushort virtualIndex)
		{
			object cell = base.GetCell(worksheetName, (int)(1 + virtualIndex), 3);
			string text = (cell != null) ? cell.ToString() : null;
			object result;
			if (string.IsNullOrEmpty(text))
			{
				result = cell;
			}
			else
			{
				Type typeOfParam = this.GetTypeOfParam(worksheetName, virtualIndex);
				result = this.GetObjectByType(text, typeOfParam);
			}
			return result;
		}

		private object GetParameterMaxValue(string worksheetName, ushort virtualIndex)
		{
			object cell = base.GetCell(worksheetName, (int)(1 + virtualIndex), 4);
			string text = (cell != null) ? cell.ToString() : null;
			object result;
			if (string.IsNullOrEmpty(text))
			{
				result = cell;
			}
			else
			{
				Type typeOfParam = this.GetTypeOfParam(worksheetName, virtualIndex);
				result = this.GetObjectByType(text, typeOfParam);
			}
			return result;
		}

		private Type GetTypeOfParam(string worksheetName, ushort virtualIndex)
		{
			object cell = base.GetCell(worksheetName, (int)(1 + virtualIndex), 1);
			string text = (cell != null) ? cell.ToString() : null;
			Type type = null;
			Type result;
			if (string.IsNullOrEmpty(text))
			{
				result = type;
			}
			else
			{
				string text2 = text;
				switch (text2)
				{
				case "uint":
				case "bool":
					type = typeof(uint);
					break;
				case "int":
				case "list":
					type = typeof(int);
					break;
				case "float":
				case "fbool":
					type = typeof(float);
					break;
				case "str":
					type = typeof(string);
					break;
				}
				result = type;
			}
			return result;
		}

		private bool IsParameterAllowedForView(string worksheetName, ushort virtualIndex, AccessLevels currentLevel)
		{
			bool result = false;
			object cell = base.GetCell(worksheetName, (int)(1 + virtualIndex), 9);
			string text = (cell != null) ? cell.ToString() : null;
			if (!string.IsNullOrEmpty(text))
			{
				int num = int.Parse(text);
				result = (num <= (int)currentLevel);
			}
			return result;
		}

		private bool IsParameterAllowedForWriting(string worksheetName, ushort virtualIndex, AccessLevels currentLevel)
		{
			AccessLevels paramAccessLevelForWriting = this.GetParamAccessLevelForWriting(virtualIndex, worksheetName);
			return paramAccessLevelForWriting <= currentLevel;
		}

		private List<ushort> GetParamListToView(AccessLevels currentLevel, string worksheetName)
		{
			List<ushort> list = new List<ushort>(0);
			int num = 1;
			int num2 = 0;
			if (worksheetName != null)
			{
				if (!(worksheetName == "Enum_Params(Portia)"))
				{
					if (!(worksheetName == "Enum_Params(Venus)"))
					{
						if (!(worksheetName == "Enum_Params(Mercury)"))
						{
							if (worksheetName == "Enum_Params(Jupiter)")
							{
								num2 = this.GetCountParamsJupiter();
							}
						}
						else
						{
							num2 = this.GetCountParamsMercury();
						}
					}
					else
					{
						num2 = this.GetCountParamsVenus();
					}
				}
				else
				{
					num2 = this.GetCountParamsPortia();
				}
			}
			int num3 = num + num2;
			for (int i = num; i < num3; i++)
			{
				object cell = base.GetCell(worksheetName, i, 9);
				object cell2 = base.GetCell(worksheetName, i, 0);
				int num4 = (cell != null) ? int.Parse(cell.ToString()) : 2;
				if (currentLevel >= (AccessLevels)num4)
				{
					list.Add((ushort)(i - 1));
				}
			}
			return list;
		}

		private AccessLevels GetParamAccessLevelForWriting(ushort virtualIndex, string worksheetName)
		{
			object cell = base.GetCell(worksheetName, (int)(1 + virtualIndex), 8);
			return (AccessLevels)((cell != null) ? int.Parse(cell.ToString()) : 3);
		}

		private string GetParamUnitKey(string worksheetName, ushort virtualIndex)
		{
			object cell = base.GetCell(worksheetName, (int)(1 + virtualIndex), 2);
			return (cell != null) ? cell.ToString() : null;
		}
	}
}
