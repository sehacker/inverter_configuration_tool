using InverterConfigurationTool.Src.Model.Data.Base;
using InverterConfigurationTool.Src.Model.Devices.Base;
using SEDevices.Devices;
using SEDevices.Records.Status;
using System;
using System.Collections.Generic;

namespace InverterConfigurationTool.Src.Model.Data
{
	public class CommData : DeviceData
	{
		private bool _shouldSlavesBeAutoConfigured = true;

		private bool _isConnected = false;

		private int _rs232ToServerConnection = 0;

		private int _rs232GSMModemType = 0;

		private string _rs232GSMAPN = "";

		private string _rs232GSMUsername = "";

		private string _rs232GSMPassword = "";

		private bool _isRS485Enabled = false;

		private bool _isRS485Connected = true;

		private bool _isRS485Master = false;

		private bool _isZigBeeEnabled = false;

		private bool _isZigBeeConnected = true;

		private bool _isZBMaster = false;

		private uint _panID;

		private uint _scanChannel;

		private SETelemetryLANStatus _lanStatusParametered = null;

		private SETelemetryLANStatus _lanStatusDetected = null;

		private uint _invConnectionStatusTCP = 0u;

		private int _invConnectionStatusCable = 0;

		private bool _isLANEnabled = false;

		private bool _isDHCPEnabled = false;

		private ulong _macAddress = 0uL;

		private int _serverCommType = 0;

		private string _serverAddress = "";

		private int _serverPort = 0;

		private S_OK_STATUS _serverStatus = S_OK_STATUS.UNKNOWN;

		private int _iicConnectionType = 0;

		private int _iicCommType = 0;

		public bool AutoConfigureSlaves
		{
			get
			{
				bool shouldSlavesBeAutoConfigured;
				lock (this)
				{
					shouldSlavesBeAutoConfigured = this._shouldSlavesBeAutoConfigured;
				}
				return shouldSlavesBeAutoConfigured;
			}
			set
			{
				if (this._shouldSlavesBeAutoConfigured != value)
				{
					this._shouldSlavesBeAutoConfigured = value;
					base.DataChanged = true;
				}
			}
		}

		public bool Connected
		{
			get
			{
				bool isConnected;
				lock (this)
				{
					isConnected = this._isConnected;
				}
				return isConnected;
			}
			set
			{
				lock (this)
				{
					if (this._isConnected != value)
					{
						this._isConnected = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int RS232ToServerConnection
		{
			get
			{
				int rs232ToServerConnection;
				lock (this)
				{
					rs232ToServerConnection = this._rs232ToServerConnection;
				}
				return rs232ToServerConnection;
			}
			set
			{
				lock (this)
				{
					if (this._rs232ToServerConnection != value)
					{
						this._rs232ToServerConnection = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int RS232GSMModemType
		{
			get
			{
				int rs232GSMModemType;
				lock (this)
				{
					rs232GSMModemType = this._rs232GSMModemType;
				}
				return rs232GSMModemType;
			}
			set
			{
				lock (this)
				{
					if (!this._rs232GSMModemType.Equals(value))
					{
						this._rs232GSMModemType = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string RS232GSMAPN
		{
			get
			{
				string rs232GSMAPN;
				lock (this)
				{
					rs232GSMAPN = this._rs232GSMAPN;
				}
				return rs232GSMAPN;
			}
			set
			{
				lock (this)
				{
					if (this._rs232GSMAPN == null || !this._rs232GSMAPN.Equals(value))
					{
						this._rs232GSMAPN = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string RS232GSMUsername
		{
			get
			{
				string rs232GSMUsername;
				lock (this)
				{
					rs232GSMUsername = this._rs232GSMUsername;
				}
				return rs232GSMUsername;
			}
			set
			{
				lock (this)
				{
					if (this._rs232GSMUsername == null || !this._rs232GSMUsername.Equals(value))
					{
						this._rs232GSMUsername = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string RS232GSMPassword
		{
			get
			{
				string rs232GSMPassword;
				lock (this)
				{
					rs232GSMPassword = this._rs232GSMPassword;
				}
				return rs232GSMPassword;
			}
			set
			{
				lock (this)
				{
					if (this._rs232GSMPassword == null || !this._rs232GSMPassword.Equals(value))
					{
						this._rs232GSMPassword = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool RS485Enabled
		{
			get
			{
				bool isRS485Enabled;
				lock (this)
				{
					isRS485Enabled = this._isRS485Enabled;
				}
				return isRS485Enabled;
			}
			set
			{
				lock (this)
				{
					if (this._isRS485Enabled != value)
					{
						this._isRS485Enabled = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool RS485Connected
		{
			get
			{
				bool isRS485Connected;
				lock (this)
				{
					isRS485Connected = this._isRS485Connected;
				}
				return isRS485Connected;
			}
			set
			{
				lock (this)
				{
					if (this._isRS485Connected != value)
					{
						this._isRS485Connected = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool RS485Master
		{
			get
			{
				bool isRS485Master;
				lock (this)
				{
					isRS485Master = this._isRS485Master;
				}
				return isRS485Master;
			}
			set
			{
				lock (this)
				{
					if (this._isRS485Master != value)
					{
						this._isRS485Master = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool ZigBeeEnabled
		{
			get
			{
				bool isZigBeeEnabled;
				lock (this)
				{
					isZigBeeEnabled = this._isZigBeeEnabled;
				}
				return isZigBeeEnabled;
			}
			set
			{
				lock (this)
				{
					if (this._isZigBeeEnabled != value)
					{
						this._isZigBeeEnabled = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool ZigBeeConnected
		{
			get
			{
				bool isZigBeeConnected;
				lock (this)
				{
					isZigBeeConnected = this._isZigBeeConnected;
				}
				return isZigBeeConnected;
			}
			set
			{
				lock (this)
				{
					if (this._isZigBeeConnected != value)
					{
						this._isZigBeeConnected = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool ZigBeeMaster
		{
			get
			{
				bool isZBMaster;
				lock (this)
				{
					isZBMaster = this._isZBMaster;
				}
				return isZBMaster;
			}
			set
			{
				lock (this)
				{
					if (this._isZBMaster != value)
					{
						this._isZBMaster = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public uint ZigBeePANID
		{
			get
			{
				uint panID;
				lock (this)
				{
					panID = this._panID;
				}
				return panID;
			}
			set
			{
				lock (this)
				{
					if (this._panID != value)
					{
						this._panID = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public uint ZigBeeScanChannel
		{
			get
			{
				uint scanChannel;
				lock (this)
				{
					scanChannel = this._scanChannel;
				}
				return scanChannel;
			}
			set
			{
				lock (this)
				{
					if (this._scanChannel != value)
					{
						this._scanChannel = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public List<int> ZigBeeOnlineScanChannels
		{
			get
			{
				List<int> result;
				lock (this)
				{
					List<int> list = null;
					if (this._scanChannel > 0u && this._scanChannel <= 16u)
					{
						list = new List<int>(0);
						for (int i = 0; i <= 16; i++)
						{
							if (((long)i & (long)((ulong)this._scanChannel)) > 0L)
							{
								list.Add(i);
							}
						}
					}
					result = list;
				}
				return result;
			}
		}

		public SETelemetryLANStatus LANStatusParametered
		{
			get
			{
				SETelemetryLANStatus lanStatusParametered;
				lock (this)
				{
					lanStatusParametered = this._lanStatusParametered;
				}
				return lanStatusParametered;
			}
			set
			{
				lock (this)
				{
					if (this._lanStatusParametered != value)
					{
						this._lanStatusParametered = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public SETelemetryLANStatus LANStatusDetected
		{
			get
			{
				SETelemetryLANStatus lanStatusDetected;
				lock (this)
				{
					lanStatusDetected = this._lanStatusDetected;
				}
				return lanStatusDetected;
			}
			set
			{
				lock (this)
				{
					if (this._lanStatusDetected != value)
					{
						this._lanStatusDetected = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public uint LANConnectionStatusTCP
		{
			get
			{
				uint invConnectionStatusTCP;
				lock (this)
				{
					invConnectionStatusTCP = this._invConnectionStatusTCP;
				}
				return invConnectionStatusTCP;
			}
			set
			{
				lock (this)
				{
					if (this._invConnectionStatusTCP != value)
					{
						this._invConnectionStatusTCP = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int LANConnectionStatusCable
		{
			get
			{
				int invConnectionStatusCable;
				lock (this)
				{
					invConnectionStatusCable = this._invConnectionStatusCable;
				}
				return invConnectionStatusCable;
			}
			set
			{
				lock (this)
				{
					if (this._invConnectionStatusCable != value)
					{
						this._invConnectionStatusCable = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool LANEnabled
		{
			get
			{
				bool isLANEnabled;
				lock (this)
				{
					isLANEnabled = this._isLANEnabled;
				}
				return isLANEnabled;
			}
			set
			{
				lock (this)
				{
					if (this._isLANEnabled != value)
					{
						this._isLANEnabled = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public bool LANDHCPEnabled
		{
			get
			{
				bool isDHCPEnabled;
				lock (this)
				{
					isDHCPEnabled = this._isDHCPEnabled;
				}
				return isDHCPEnabled;
			}
			set
			{
				lock (this)
				{
					if (this._isDHCPEnabled != value)
					{
						this._isDHCPEnabled = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public ulong MACAddress
		{
			get
			{
				ulong macAddress;
				lock (this)
				{
					macAddress = this._macAddress;
				}
				return macAddress;
			}
			set
			{
				lock (this)
				{
					if (this._macAddress != value)
					{
						this._macAddress = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string MACAddressStr
		{
			get
			{
				string result;
				lock (this)
				{
					result = string.Concat(new object[]
					{
						this._macAddress & 255uL,
						".",
						this._macAddress >> 8 & 255uL,
						".",
						this._macAddress >> 16 & 255uL,
						".",
						this._macAddress >> 24 & 255uL,
						".",
						this._macAddress >> 32 & 255uL,
						".",
						this._macAddress >> 40 & 255uL
					});
				}
				return result;
			}
		}

		public string MACAddressStrHex
		{
			get
			{
				string result;
				lock (this)
				{
					long value = (long)(this._macAddress & 255uL);
					string text = Convert.ToString(value, 16).ToUpper();
					long value2 = (long)(this._macAddress >> 8 & 255uL);
					string text2 = Convert.ToString(value2, 16).ToUpper();
					long value3 = (long)(this._macAddress >> 16 & 255uL);
					string text3 = Convert.ToString(value3, 16).ToUpper();
					long value4 = (long)(this._macAddress >> 24 & 255uL);
					string text4 = Convert.ToString(value4, 16).ToUpper();
					long value5 = (long)(this._macAddress >> 32 & 255uL);
					string text5 = Convert.ToString(value5, 16).ToUpper();
					long value6 = (long)(this._macAddress >> 40 & 255uL);
					string text6 = Convert.ToString(value6, 16).ToUpper();
					result = string.Concat(new string[]
					{
						text,
						"-",
						text2,
						"-",
						text3,
						"-",
						text4,
						"-",
						text5,
						"-",
						text6
					});
				}
				return result;
			}
		}

		public int ServerCommunicationType
		{
			get
			{
				int serverCommType;
				lock (this)
				{
					serverCommType = this._serverCommType;
				}
				return serverCommType;
			}
			set
			{
				lock (this)
				{
					if (this._serverCommType != value)
					{
						this._serverCommType = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public string ServerAddress
		{
			get
			{
				string serverAddress;
				lock (this)
				{
					serverAddress = this._serverAddress;
				}
				return serverAddress;
			}
			set
			{
				lock (this)
				{
					if (this._serverAddress == null || !this._serverAddress.Equals(value))
					{
						this._serverAddress = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int ServerPort
		{
			get
			{
				int serverPort;
				lock (this)
				{
					serverPort = this._serverPort;
				}
				return serverPort;
			}
			set
			{
				lock (this)
				{
					if (this._serverPort != value)
					{
						this._serverPort = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public S_OK_STATUS ServerStatus
		{
			get
			{
				S_OK_STATUS serverStatus;
				lock (this)
				{
					serverStatus = this._serverStatus;
				}
				return serverStatus;
			}
			set
			{
				lock (this)
				{
					this._serverStatus = value;
				}
			}
		}

		public int IICConnectionType
		{
			get
			{
				int iicConnectionType;
				lock (this)
				{
					iicConnectionType = this._iicConnectionType;
				}
				return iicConnectionType;
			}
			set
			{
				lock (this)
				{
					if (this._iicConnectionType != value)
					{
						this._iicConnectionType = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public int IICCommunicationType
		{
			get
			{
				int iicCommType;
				lock (this)
				{
					iicCommType = this._iicCommType;
				}
				return iicCommType;
			}
			set
			{
				lock (this)
				{
					if (this._iicCommType != value)
					{
						this._iicCommType = value;
						base.DataChanged = true;
					}
				}
			}
		}

		public CommData(Inverter inverter) : base(inverter)
		{
		}
	}
}
