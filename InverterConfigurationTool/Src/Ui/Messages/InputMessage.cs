using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.Messages
{
	public class InputMessage : SEForm
	{
		private static InputMessage _instance = null;

		private static string _lastInput = "";

		private IContainer components = null;

		private SEPictureBox pbMessageLogo;

		private SETextBox txtMessageInput;

		private SEButton btnMessageCancel;

		private SEButton btnMessageAccept;

		private SELabel lblMessageMessage;

		private SELabel lblMessageCaption;

		public static string LastInput
		{
			get
			{
				return InputMessage._lastInput;
			}
		}

		private InputMessage()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
		}

		private void SetupLabels()
		{
			this.lblMessageCaption.SetUI(false, false);
			this.lblMessageMessage.SetUI(false, false);
			this.txtMessageInput.TSAccess.Text = "";
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (((ISEUI)control).IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_7E)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		public static DialogResult ShowInputMessage(Form owner, string message, string caption, string defaultValue)
		{
			if (InputMessage._instance == null)
			{
				InputMessage._instance = new InputMessage();
			}
			InputMessage._instance.SetForm(message, caption, defaultValue);
			return InputMessage._instance.ShowDialog(owner);
		}

		private void SetForm(string message, string caption, string defaultValue)
		{
			this.lblMessageCaption.TSAccess.Text = caption;
			this.lblMessageMessage.TSAccess.Text = message;
			this.txtMessageInput.TSAccess.Text = defaultValue;
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			this.txtMessageInput.Focus();
			base.OnShown(e);
		}

		private void btnMessageAccept_Click(object sender, EventArgs e)
		{
			InputMessage._lastInput = this.txtMessageInput.TSAccess.Text;
			this.txtMessageInput.TSAccess.Text = "";
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void btnMessageCancel_Click(object sender, EventArgs e)
		{
			this.txtMessageInput.TSAccess.Text = "";
			InputMessage._lastInput = "";
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblMessageCaption.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblMessageCaption_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblMessageCaption.Size.Height);
		}

		private void lblMessageCaption_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblMessageCaption_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(InputMessage));
			this.pbMessageLogo = new SEPictureBox();
			this.txtMessageInput = new SETextBox();
			this.btnMessageCancel = new SEButton();
			this.btnMessageAccept = new SEButton();
			this.lblMessageMessage = new SELabel();
			this.lblMessageCaption = new SELabel();
			((ISupportInitialize)this.pbMessageLogo).BeginInit();
			base.SuspendLayout();
			this.pbMessageLogo.BackColor = Color.Transparent;
			this.pbMessageLogo.BackgroundImage = Resources.SolarEdgeLogo;
			this.pbMessageLogo.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbMessageLogo.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbMessageLogo.ImageListBack");
			this.pbMessageLogo.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbMessageLogo.ImageListFore");
			this.pbMessageLogo.Location = new Point(25, 128);
			this.pbMessageLogo.Name = "pbMessageLogo";
			this.pbMessageLogo.SelectedImageBack = -1;
			this.pbMessageLogo.SelectedImageFore = -1;
			this.pbMessageLogo.Size = new Size(87, 25);
			this.pbMessageLogo.TabIndex = 15;
			this.pbMessageLogo.TabStop = false;
			this.txtMessageInput.Location = new Point(25, 98);
			this.txtMessageInput.Name = "txtMessageInput";
			this.txtMessageInput.Size = new Size(353, 20);
			this.txtMessageInput.TabIndex = 12;
			this.btnMessageCancel.BackColor = Color.Silver;
			this.btnMessageCancel.Location = new Point(315, 128);
			this.btnMessageCancel.Name = "btnMessageCancel";
			this.btnMessageCancel.Size = new Size(63, 29);
			this.btnMessageCancel.TabIndex = 11;
			this.btnMessageCancel.Text = "Cancel";
			this.btnMessageCancel.UseVisualStyleBackColor = true;
			this.btnMessageCancel.Click += new EventHandler(this.btnMessageCancel_Click);
			this.btnMessageAccept.BackColor = Color.Silver;
			this.btnMessageAccept.Location = new Point(246, 128);
			this.btnMessageAccept.Name = "btnMessageAccept";
			this.btnMessageAccept.Size = new Size(63, 29);
			this.btnMessageAccept.TabIndex = 10;
			this.btnMessageAccept.Text = "Accept";
			this.btnMessageAccept.UseVisualStyleBackColor = true;
			this.btnMessageAccept.Click += new EventHandler(this.btnMessageAccept_Click);
			this.lblMessageMessage.BackColor = Color.Transparent;
			this.lblMessageMessage.Location = new Point(27, 41);
			this.lblMessageMessage.Name = "lblMessageMessage";
			this.lblMessageMessage.Size = new Size(351, 44);
			this.lblMessageMessage.TabIndex = 9;
			this.lblMessageMessage.Text = "Message";
			this.lblMessageCaption.BackColor = Color.Transparent;
			this.lblMessageCaption.Location = new Point(27, 3);
			this.lblMessageCaption.Name = "lblMessageCaption";
			this.lblMessageCaption.Size = new Size(351, 30);
			this.lblMessageCaption.TabIndex = 8;
			this.lblMessageCaption.Text = "Caption";
			this.lblMessageCaption.TextAlign = ContentAlignment.MiddleLeft;
			this.lblMessageCaption.MouseMove += new MouseEventHandler(this.lblMessageCaption_MouseMove);
			this.lblMessageCaption.MouseDown += new MouseEventHandler(this.lblMessageCaption_MouseDown);
			this.lblMessageCaption.MouseUp += new MouseEventHandler(this.lblMessageCaption_MouseUp);
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandSmallNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(399, 168);
			base.ControlBox = false;
			base.Controls.Add(this.pbMessageLogo);
			base.Controls.Add(this.txtMessageInput);
			base.Controls.Add(this.btnMessageCancel);
			base.Controls.Add(this.btnMessageAccept);
			base.Controls.Add(this.lblMessageMessage);
			base.Controls.Add(this.lblMessageCaption);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "InputMessage";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			((ISupportInitialize)this.pbMessageLogo).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
