using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.Messages
{
	public class RTCSettingMessage : SEForm
	{
		private delegate void AnimateUIDelegate();

		private IContainer components = null;

		private SEPictureBox sePictureBox1;

		private SEButton btnRTCMessageClose;

		private SELabel lblRTCMessageHeader;

		private SELabel lblRTCMessageLocalTime;

		private SETextBox txtRTCMessageLocalTimeHour;

		private SEButton btnRTCMessageLocalTime;

		private SELabel lblRTCMessageLocalGMT;

		private SETextBox txtRTCMessageLocalTimeMinute;

		private SETextBox txtRTCMessageLocalTimeSecond;

		private SEComboBox cmbRTCMessageLocalGMTHours;

		private SEButton btnRTCMessageAccept;

		private SEComboBox cmbRTCMessageLocalGMTMinutes;

		private SELabel lblColonLeft;

		private SELabel lblColonRight;

		private static Color COLOR_OK = Color.White;

		private static Color COLOR_NOK = Color.LightPink;

		private static RTCSettingMessage _instance = null;

		private int _lastGMTHours;

		private int _lastGMTMinutes;

		private DateTime _lastTime;

		private bool isHourFault = false;

		private bool isMinuteFault = false;

		private bool isSecondFault = false;

		private static DateTime _retValTime = DateTime.Now;

		private static uint _retValGMTSecs = 0u;

		public static DateTime ReturnTime
		{
			get
			{
				return RTCSettingMessage._retValTime;
			}
		}

		public static uint ReturnGMTSecs
		{
			get
			{
				return RTCSettingMessage._retValGMTSecs;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(RTCSettingMessage));
			this.sePictureBox1 = new SEPictureBox();
			this.btnRTCMessageClose = new SEButton();
			this.lblRTCMessageHeader = new SELabel();
			this.lblRTCMessageLocalTime = new SELabel();
			this.txtRTCMessageLocalTimeHour = new SETextBox();
			this.btnRTCMessageLocalTime = new SEButton();
			this.lblRTCMessageLocalGMT = new SELabel();
			this.txtRTCMessageLocalTimeMinute = new SETextBox();
			this.txtRTCMessageLocalTimeSecond = new SETextBox();
			this.cmbRTCMessageLocalGMTHours = new SEComboBox();
			this.btnRTCMessageAccept = new SEButton();
			this.cmbRTCMessageLocalGMTMinutes = new SEComboBox();
			this.lblColonLeft = new SELabel();
			this.lblColonRight = new SELabel();
			((ISupportInitialize)this.sePictureBox1).BeginInit();
			base.SuspendLayout();
			this.sePictureBox1.BackColor = Color.Transparent;
			this.sePictureBox1.BackgroundImage = Resources.SolarEdgeLogo;
			this.sePictureBox1.BackgroundImageLayout = ImageLayout.Zoom;
			this.sePictureBox1.ImageListBack = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListBack");
			this.sePictureBox1.ImageListFore = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListFore");
			this.sePictureBox1.Location = new Point(17, 129);
			this.sePictureBox1.Name = "sePictureBox1";
			this.sePictureBox1.SelectedImageBack = -1;
			this.sePictureBox1.SelectedImageFore = -1;
			this.sePictureBox1.Size = new Size(87, 25);
			this.sePictureBox1.TabIndex = 11;
			this.sePictureBox1.TabStop = false;
			this.btnRTCMessageClose.BackColor = Color.Silver;
			this.btnRTCMessageClose.Location = new Point(318, 127);
			this.btnRTCMessageClose.Name = "btnRTCMessageClose";
			this.btnRTCMessageClose.Size = new Size(63, 29);
			this.btnRTCMessageClose.TabIndex = 10;
			this.btnRTCMessageClose.Text = "Cancel";
			this.btnRTCMessageClose.UseVisualStyleBackColor = true;
			this.btnRTCMessageClose.Click += new EventHandler(this.btnPBMessageClose_Click);
			this.lblRTCMessageHeader.BackColor = Color.Transparent;
			this.lblRTCMessageHeader.Location = new Point(30, 3);
			this.lblRTCMessageHeader.Name = "lblRTCMessageHeader";
			this.lblRTCMessageHeader.Size = new Size(351, 30);
			this.lblRTCMessageHeader.TabIndex = 9;
			this.lblRTCMessageHeader.Text = "Caption";
			this.lblRTCMessageHeader.TextAlign = ContentAlignment.MiddleLeft;
			this.lblRTCMessageHeader.MouseMove += new MouseEventHandler(this.lblPBMessageHeader_MouseMove);
			this.lblRTCMessageHeader.MouseDown += new MouseEventHandler(this.lblPBMessageHeader_MouseDown);
			this.lblRTCMessageHeader.MouseUp += new MouseEventHandler(this.lblPBMessageHeader_MouseUp);
			this.lblRTCMessageLocalTime.BackColor = Color.Transparent;
			this.lblRTCMessageLocalTime.Location = new Point(24, 49);
			this.lblRTCMessageLocalTime.Name = "lblRTCMessageLocalTime";
			this.lblRTCMessageLocalTime.Size = new Size(131, 20);
			this.lblRTCMessageLocalTime.TabIndex = 12;
			this.lblRTCMessageLocalTime.Text = "Current Time To Set:";
			this.lblRTCMessageLocalTime.TextAlign = ContentAlignment.MiddleRight;
			this.txtRTCMessageLocalTimeHour.BackColor = Color.White;
			this.txtRTCMessageLocalTimeHour.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.txtRTCMessageLocalTimeHour.Location = new Point(161, 47);
			this.txtRTCMessageLocalTimeHour.MaxLength = 2;
			this.txtRTCMessageLocalTimeHour.Name = "txtRTCMessageLocalTimeHour";
			this.txtRTCMessageLocalTimeHour.Size = new Size(30, 22);
			this.txtRTCMessageLocalTimeHour.TabIndex = 37;
			this.txtRTCMessageLocalTimeHour.Tag = "";
			this.txtRTCMessageLocalTimeHour.Text = "00";
			this.txtRTCMessageLocalTimeHour.TextAlign = HorizontalAlignment.Center;
			this.txtRTCMessageLocalTimeHour.TextChanged += new EventHandler(this.txtPBMessageLocalTimeHour_TextChanged);
			this.btnRTCMessageLocalTime.BackColor = Color.Silver;
			this.btnRTCMessageLocalTime.Location = new Point(288, 44);
			this.btnRTCMessageLocalTime.Name = "btnRTCMessageLocalTime";
			this.btnRTCMessageLocalTime.Size = new Size(99, 29);
			this.btnRTCMessageLocalTime.TabIndex = 38;
			this.btnRTCMessageLocalTime.Text = "Get Local Time";
			this.btnRTCMessageLocalTime.UseVisualStyleBackColor = true;
			this.btnRTCMessageLocalTime.Click += new EventHandler(this.btnPBMessageLocalTime_Click);
			this.lblRTCMessageLocalGMT.BackColor = Color.Transparent;
			this.lblRTCMessageLocalGMT.Location = new Point(24, 83);
			this.lblRTCMessageLocalGMT.Name = "lblRTCMessageLocalGMT";
			this.lblRTCMessageLocalGMT.Size = new Size(131, 20);
			this.lblRTCMessageLocalGMT.TabIndex = 39;
			this.lblRTCMessageLocalGMT.Text = "GMT Offset To Set:";
			this.lblRTCMessageLocalGMT.TextAlign = ContentAlignment.MiddleRight;
			this.txtRTCMessageLocalTimeMinute.BackColor = Color.White;
			this.txtRTCMessageLocalTimeMinute.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.txtRTCMessageLocalTimeMinute.Location = new Point(201, 47);
			this.txtRTCMessageLocalTimeMinute.MaxLength = 2;
			this.txtRTCMessageLocalTimeMinute.Name = "txtRTCMessageLocalTimeMinute";
			this.txtRTCMessageLocalTimeMinute.Size = new Size(30, 22);
			this.txtRTCMessageLocalTimeMinute.TabIndex = 42;
			this.txtRTCMessageLocalTimeMinute.Tag = "";
			this.txtRTCMessageLocalTimeMinute.Text = "00";
			this.txtRTCMessageLocalTimeMinute.TextAlign = HorizontalAlignment.Center;
			this.txtRTCMessageLocalTimeMinute.TextChanged += new EventHandler(this.txtPBMessageLocalTimeMinute_TextChanged);
			this.txtRTCMessageLocalTimeSecond.BackColor = Color.White;
			this.txtRTCMessageLocalTimeSecond.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.txtRTCMessageLocalTimeSecond.Location = new Point(241, 47);
			this.txtRTCMessageLocalTimeSecond.MaxLength = 2;
			this.txtRTCMessageLocalTimeSecond.Name = "txtRTCMessageLocalTimeSecond";
			this.txtRTCMessageLocalTimeSecond.Size = new Size(30, 22);
			this.txtRTCMessageLocalTimeSecond.TabIndex = 43;
			this.txtRTCMessageLocalTimeSecond.Tag = "";
			this.txtRTCMessageLocalTimeSecond.Text = "00";
			this.txtRTCMessageLocalTimeSecond.TextAlign = HorizontalAlignment.Center;
			this.txtRTCMessageLocalTimeSecond.TextChanged += new EventHandler(this.txtPBMessageLocalTimeSecond_TextChanged);
			this.cmbRTCMessageLocalGMTHours.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbRTCMessageLocalGMTHours.FormattingEnabled = true;
			this.cmbRTCMessageLocalGMTHours.Location = new Point(161, 83);
			this.cmbRTCMessageLocalGMTHours.Name = "cmbRTCMessageLocalGMTHours";
			this.cmbRTCMessageLocalGMTHours.Size = new Size(42, 21);
			this.cmbRTCMessageLocalGMTHours.TabIndex = 44;
			this.cmbRTCMessageLocalGMTHours.SelectedIndexChanged += new EventHandler(this.cmbRTCMessageLocalGMTHours_SelectedIndexChanged);
			this.btnRTCMessageAccept.BackColor = Color.Silver;
			this.btnRTCMessageAccept.Location = new Point(249, 127);
			this.btnRTCMessageAccept.Name = "btnRTCMessageAccept";
			this.btnRTCMessageAccept.Size = new Size(63, 29);
			this.btnRTCMessageAccept.TabIndex = 45;
			this.btnRTCMessageAccept.Text = "Accept";
			this.btnRTCMessageAccept.UseVisualStyleBackColor = true;
			this.btnRTCMessageAccept.Click += new EventHandler(this.btnPBMessageAccept_Click);
			this.cmbRTCMessageLocalGMTMinutes.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbRTCMessageLocalGMTMinutes.FormattingEnabled = true;
			this.cmbRTCMessageLocalGMTMinutes.Location = new Point(209, 83);
			this.cmbRTCMessageLocalGMTMinutes.Name = "cmbRTCMessageLocalGMTMinutes";
			this.cmbRTCMessageLocalGMTMinutes.Size = new Size(42, 21);
			this.cmbRTCMessageLocalGMTMinutes.TabIndex = 46;
			this.cmbRTCMessageLocalGMTMinutes.SelectedIndexChanged += new EventHandler(this.cmbRTCMessageLocalGMTMinutes_SelectedIndexChanged);
			this.lblColonLeft.BackColor = Color.Transparent;
			this.lblColonLeft.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblColonLeft.Location = new Point(191, 47);
			this.lblColonLeft.Name = "lblColonLeft";
			this.lblColonLeft.Size = new Size(12, 20);
			this.lblColonLeft.TabIndex = 47;
			this.lblColonLeft.Text = ":";
			this.lblColonLeft.TextAlign = ContentAlignment.MiddleCenter;
			this.lblColonRight.BackColor = Color.Transparent;
			this.lblColonRight.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblColonRight.Location = new Point(231, 47);
			this.lblColonRight.Name = "lblColonRight";
			this.lblColonRight.Size = new Size(12, 20);
			this.lblColonRight.TabIndex = 48;
			this.lblColonRight.Text = ":";
			this.lblColonRight.TextAlign = ContentAlignment.MiddleCenter;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandSmallNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(399, 168);
			base.ControlBox = false;
			base.Controls.Add(this.cmbRTCMessageLocalGMTMinutes);
			base.Controls.Add(this.btnRTCMessageAccept);
			base.Controls.Add(this.cmbRTCMessageLocalGMTHours);
			base.Controls.Add(this.txtRTCMessageLocalTimeSecond);
			base.Controls.Add(this.txtRTCMessageLocalTimeMinute);
			base.Controls.Add(this.lblRTCMessageLocalGMT);
			base.Controls.Add(this.btnRTCMessageLocalTime);
			base.Controls.Add(this.txtRTCMessageLocalTimeHour);
			base.Controls.Add(this.lblRTCMessageLocalTime);
			base.Controls.Add(this.sePictureBox1);
			base.Controls.Add(this.btnRTCMessageClose);
			base.Controls.Add(this.lblRTCMessageHeader);
			base.Controls.Add(this.lblColonLeft);
			base.Controls.Add(this.lblColonRight);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "RTCSettingMessage";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			((ISupportInitialize)this.sePictureBox1).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private RTCSettingMessage()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
		}

		private void SetupLabels()
		{
			this.lblRTCMessageHeader.SetUI(false, true);
			this.lblRTCMessageLocalTime.SetUI(true, true);
			this.lblRTCMessageLocalGMT.SetUI(true, true);
			this.lblColonLeft.SetUI(false, false);
			this.lblColonRight.SetUI(false, false);
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (iSEUI.IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_79)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		public static DialogResult ShowRTCSettingMessage(SEForm owner, string currentGMT)
		{
			if (RTCSettingMessage._instance == null)
			{
				RTCSettingMessage._instance = new RTCSettingMessage();
			}
			RTCSettingMessage._instance.SetForm(currentGMT);
			return RTCSettingMessage._instance.ShowDialog(owner);
		}

		private void SetForm(string currentGMT)
		{
			if (!string.IsNullOrEmpty(currentGMT))
			{
				string[] array = currentGMT.Split(new string[]
				{
					"."
				}, StringSplitOptions.None);
				if (array != null && array.Length > 0)
				{
					int lastGMTHours = (array.Length >= 1) ? int.Parse(array[0]) : 0;
					int lastGMTMinutes = (array.Length >= 2) ? int.Parse(array[1]) : 0;
					this._lastGMTHours = lastGMTHours;
					this._lastGMTMinutes = lastGMTMinutes;
					this._lastTime = DateTime.Now;
					this.SetComboBoxGMT();
				}
			}
		}

		private void SetComboBoxGMT()
		{
			this.cmbRTCMessageLocalGMTHours.Items.Clear();
			for (int i = -12; i <= 12; i++)
			{
				string text = "";
				if (i >= 0)
				{
					text += "+";
				}
				text += i.ToString();
				this.cmbRTCMessageLocalGMTHours.Items.Add(text);
			}
			this.cmbRTCMessageLocalGMTMinutes.Items.Clear();
			for (int i = 0; i <= 60; i++)
			{
				this.cmbRTCMessageLocalGMTMinutes.Items.Add(i.ToString());
			}
		}

		private void UpdateComboBoxGMT(int gmtHour, int gmtMinute)
		{
			string text = (gmtHour >= 0) ? "+" : "";
			text += gmtHour.ToString();
			int num = this.cmbRTCMessageLocalGMTHours.Items.IndexOf(text);
			this.cmbRTCMessageLocalGMTHours.SelectedIndex = ((num > -1) ? num : 0);
			int num2 = this.cmbRTCMessageLocalGMTMinutes.Items.IndexOf(gmtMinute.ToString());
			this.cmbRTCMessageLocalGMTMinutes.SelectedIndex = ((num2 > -1) ? num2 : 0);
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			this.txtRTCMessageLocalTimeHour.Text = this._lastTime.Hour.ToString();
			this.txtRTCMessageLocalTimeMinute.Text = this._lastTime.Minute.ToString();
			this.txtRTCMessageLocalTimeSecond.Text = this._lastTime.Second.ToString();
			this.UpdateComboBoxGMT(this._lastGMTHours, this._lastGMTMinutes);
			base.OnShown(e);
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblRTCMessageHeader.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblPBMessageHeader_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblRTCMessageHeader.Size.Height);
		}

		private void lblPBMessageHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblPBMessageHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		private void btnPBMessageLocalTime_Click(object sender, EventArgs e)
		{
			DateTime now = DateTime.Now;
			this.txtRTCMessageLocalTimeHour.Text = now.Hour.ToString();
			this.txtRTCMessageLocalTimeMinute.Text = now.Minute.ToString();
			this.txtRTCMessageLocalTimeSecond.Text = now.Second.ToString();
			DateTime d = now.ToUniversalTime();
			TimeSpan timeSpan = now - d;
			int hours = timeSpan.Hours;
			int minutes = timeSpan.Minutes;
			this.UpdateComboBoxGMT(hours, minutes);
		}

		private void txtPBMessageLocalTimeHour_TextChanged(object sender, EventArgs e)
		{
			SETextBox sETextBox = (SETextBox)sender;
			int num = 0;
			int num2 = 23;
			try
			{
				int num3 = int.Parse(sETextBox.Text);
				sETextBox.BackColor = ((num3 > num2 || num3 < num) ? RTCSettingMessage.COLOR_NOK : RTCSettingMessage.COLOR_OK);
				this.isHourFault = (num3 > num2 || num3 < num);
			}
			catch (Exception var_4_4B)
			{
				sETextBox.BackColor = RTCSettingMessage.COLOR_NOK;
			}
			this.btnRTCMessageAccept.Enabled = (!this.isHourFault && !this.isMinuteFault && !this.isSecondFault);
		}

		private void txtPBMessageLocalTimeMinute_TextChanged(object sender, EventArgs e)
		{
			SETextBox sETextBox = (SETextBox)sender;
			int num = 0;
			int num2 = 59;
			try
			{
				int num3 = int.Parse(sETextBox.Text);
				sETextBox.BackColor = ((num3 > num2 || num3 < num) ? RTCSettingMessage.COLOR_NOK : RTCSettingMessage.COLOR_OK);
				this.isMinuteFault = (num3 > num2 || num3 < num);
			}
			catch (Exception var_4_4B)
			{
				sETextBox.BackColor = RTCSettingMessage.COLOR_NOK;
			}
			this.btnRTCMessageAccept.Enabled = (!this.isHourFault && !this.isMinuteFault && !this.isSecondFault);
		}

		private void txtPBMessageLocalTimeSecond_TextChanged(object sender, EventArgs e)
		{
			SETextBox sETextBox = (SETextBox)sender;
			int num = 0;
			int num2 = 59;
			try
			{
				int num3 = int.Parse(sETextBox.Text);
				sETextBox.BackColor = ((num3 > num2 || num3 < num) ? RTCSettingMessage.COLOR_NOK : RTCSettingMessage.COLOR_OK);
				this.isSecondFault = (num3 > num2 || num3 < num);
			}
			catch (Exception var_4_4B)
			{
				sETextBox.BackColor = RTCSettingMessage.COLOR_NOK;
			}
			this.btnRTCMessageAccept.Enabled = (!this.isHourFault && !this.isMinuteFault && !this.isSecondFault);
		}

		private void btnPBMessageAccept_Click(object sender, EventArgs e)
		{
			DateTime retValTime = DateTime.Now;
			uint retValGMTSecs = 0u;
			try
			{
				retValTime = new DateTime(this._lastTime.Year, this._lastTime.Month, this._lastTime.Day, int.Parse(this.txtRTCMessageLocalTimeHour.Text), int.Parse(this.txtRTCMessageLocalTimeMinute.Text), int.Parse(this.txtRTCMessageLocalTimeSecond.Text));
				int num = Math.Abs(this._lastGMTHours) * 3600 + this._lastGMTMinutes * 60;
				if (this._lastGMTHours < 0)
				{
					num *= -1;
					MemoryStream memoryStream = new MemoryStream();
					BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
					BinaryReader binaryReader = new BinaryReader(memoryStream);
					binaryWriter.Write(num);
					memoryStream.Position = 0L;
					retValGMTSecs = binaryReader.ReadUInt32();
				}
				else
				{
					retValGMTSecs = Convert.ToUInt32(num);
				}
			}
			catch (Exception var_6_D3)
			{
				MessageBox.Show(DataManager.Instance.Dictionary["msgRTCError"]);
				base.DialogResult = DialogResult.Cancel;
				return;
			}
			RTCSettingMessage._retValTime = retValTime;
			RTCSettingMessage._retValGMTSecs = retValGMTSecs;
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void btnPBMessageClose_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void cmbRTCMessageLocalGMTHours_SelectedIndexChanged(object sender, EventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;
			string text = comboBox.Items[comboBox.SelectedIndex].ToString();
			string s = text.Contains("+") ? text.Substring(1) : text;
			this._lastGMTHours = int.Parse(s);
		}

		private void cmbRTCMessageLocalGMTMinutes_SelectedIndexChanged(object sender, EventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;
			string s = comboBox.Items[comboBox.SelectedIndex].ToString();
			this._lastGMTMinutes = int.Parse(s);
		}
	}
}
