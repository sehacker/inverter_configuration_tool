using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Devices.Base;
using InverterConfigurationTool.Src.Ui.Main;
using SEDevices.Records.Telemetry;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.Messages
{
	public class PowerBoxTelemChangeMessage : SEForm, ISECommNotifiable
	{
		private delegate void AnimateUIDelegate();

		private const double NUM_OF_MINUTES_TO_WAIT = 4.0;

		private static PowerBoxTelemChangeMessage _instance = null;

		private SEThread _thCheckData = null;

		private List<uint> _idsRetreived = null;

		private object _lockObj = new object();

		private DateTime _timeToEnd = DateTime.Now;

		private ManualResetEvent _thNotifyAnimate = new ManualResetEvent(false);

		private IContainer components = null;

		private SELabel lblPBMessageTimeLeft;

		private SELabel lblPBMessageHeader;

		private SEButton btnPBMessageAbort;

		private SEPictureBox sePictureBox1;

		private SELabel lblPBMessageDetectedSpeed;

		private SELabel lblPBMessagePBRetreived;

		private SELabel lblPBMessageUserMessage;

		private void AddID(uint id)
		{
			lock (this._lockObj)
			{
				if (this._idsRetreived == null)
				{
					this._idsRetreived = new List<uint>(0);
				}
				if (!this._idsRetreived.Contains(id))
				{
					this._idsRetreived.Add(id);
				}
			}
		}

		private int? GetID(uint id)
		{
			int? result;
			lock (this._lockObj)
			{
				if (this._idsRetreived == null || !this._idsRetreived.Contains(id))
				{
					result = null;
				}
				else
				{
					result = new int?(this._idsRetreived.IndexOf(id));
				}
			}
			return result;
		}

		private uint? GetID(int index)
		{
			uint? result;
			lock (this._lockObj)
			{
				if (index < 0 || this._idsRetreived == null || index >= this._idsRetreived.Count)
				{
					result = null;
				}
				else
				{
					result = new uint?(this._idsRetreived[index]);
				}
			}
			return result;
		}

		private int GetCount()
		{
			int result;
			lock (this._lockObj)
			{
				result = ((this._idsRetreived != null) ? this._idsRetreived.Count : 0);
			}
			return result;
		}

		private PowerBoxTelemChangeMessage()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
			this.StartThreadControl();
		}

		private void SetupLabels()
		{
			this.lblPBMessageHeader.SetUI(false, true);
			this.lblPBMessageTimeLeft.SetUI(false, false);
			this.lblPBMessageDetectedSpeed.SetUI(false, true);
			this.lblPBMessagePBRetreived.SetUI(false, false);
			this.lblPBMessageUserMessage.SetUI(false, false);
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (((ISEUI)control).IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_7E)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		public static void ShowPowerBoxTelemChangeMessage()
		{
			if (PowerBoxTelemChangeMessage._instance == null)
			{
				PowerBoxTelemChangeMessage._instance = new PowerBoxTelemChangeMessage();
			}
			PowerBoxTelemChangeMessage._instance.SetForm();
			PowerBoxTelemChangeMessage._instance.Show();
		}

		private void SetForm()
		{
			base.TopMost = true;
			this.lblPBMessageTimeLeft.TSAccess.Text = "";
			this._idsRetreived = new List<uint>(0);
		}

		private void StartThreadControl()
		{
			this._thCheckData = new SEThread("Power Optimizer Telem Change Message Check Data", new SEThread.ThreadFunctionCallBack(this.ThreadFuncCheckTelem), 500, 0u, 0u, true, true, false);
		}

		private void ShutDownThreadControl()
		{
			if (this._thCheckData != null)
			{
				this._thCheckData.Stop = true;
				this._thCheckData.Dispose();
			}
			this._thCheckData = null;
		}

		public void ThreadFuncCheckTelem()
		{
			if (base.InvokeRequired)
			{
				this._thNotifyAnimate.Reset();
				base.BeginInvoke(new PowerBoxTelemChangeMessage.AnimateUIDelegate(this.ThreadFuncCheckTelem));
				this._thNotifyAnimate.WaitOne(2000);
			}
			else
			{
				try
				{
					this.AnimateUI();
				}
				catch (Exception var_0_4E)
				{
				}
				this._thNotifyAnimate.Set();
			}
		}

		private void AnimateUI()
		{
			TimeSpan t = new TimeSpan(0, 4, 0);
			TimeSpan t2 = this._timeToEnd - DateTime.Now;
			TimeSpan timeSpan = t + t2;
			this.lblPBMessageTimeLeft.Text = string.Format(DataManager.Instance.Dictionary[this.lblPBMessageTimeLeft.Name], timeSpan.Minutes, timeSpan.Seconds);
			this.lblPBMessagePBRetreived.Text = string.Format(DataManager.Instance.Dictionary[this.lblPBMessagePBRetreived.Name], this.GetCount());
			Application.DoEvents();
			if (timeSpan.Minutes == 0 && timeSpan.Seconds == 0)
			{
				base.Close();
			}
		}

		public void CommNotify(Packet packet)
		{
			if (packet != null)
			{
				if (packet.OpCode == 1280 && packet.Destination == ProtocolMain.PROT_CONFTOOL_ADDR)
				{
					this.HandlePushTelemetry(packet);
				}
			}
		}

		private void HandlePushTelemetry(Packet packet)
		{
			byte[] data = packet.Data;
			int length = packet.Length;
			long num = 0L;
			while (length > 0 && num < (long)length)
			{
				SETelemetry newRecord = SETelemetry.GetNewRecord(ref data, num);
				if (newRecord != null)
				{
					num = newRecord.ParseRecord(ref data, num);
					RecordType type = newRecord.Type;
					if (type == RecordType.PANEL)
					{
						this.HandleTelemetryFromPanel((SETelemetryPanel)newRecord);
					}
				}
			}
		}

		private void HandleTelemetryFromPanel(SETelemetryPanel panelRecord)
		{
			if (panelRecord.ReceiverId == panelRecord.DstId)
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					uint num = panelRecord.DstId - 8388608u;
					uint id = panelRecord.Id;
					if (this._idsRetreived == null)
					{
						this._idsRetreived = new List<uint>(0);
					}
					if (!this._idsRetreived.Contains(id))
					{
						this._idsRetreived.Add(id);
					}
				}
			}
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			CommManager commManager = selectedInverter.Portia.CommManager;
			this._timeToEnd = DateTime.Now;
			this._timeToEnd.AddMinutes(4.0);
			this.StartThreadControl();
			commManager.RegisterForCommListening(this);
			base.OnShown(e);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this.ShutDownThreadControl();
			MainForm.Instance.SetTelemSpeed(false);
			PowerBoxTelemChangeMessage._instance = null;
			base.OnClosing(e);
		}

		protected override void OnClosed(EventArgs e)
		{
			MessageBox.Show(string.Format(DataManager.Instance.Dictionary["MsgPBMessageEnd"], this.GetCount()));
			base.OnClosed(e);
		}

		private void btnPBMessageAbort_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			CommManager commManager = selectedInverter.Portia.CommManager;
			commManager.UnRegisterFromCommListening(this);
			base.Close();
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblPBMessageHeader.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblPBMessageHeader_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblPBMessageHeader.Size.Height);
		}

		private void lblPBMessageHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblPBMessageHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(PowerBoxTelemChangeMessage));
			this.lblPBMessageTimeLeft = new SELabel();
			this.lblPBMessageHeader = new SELabel();
			this.btnPBMessageAbort = new SEButton();
			this.sePictureBox1 = new SEPictureBox();
			this.lblPBMessageDetectedSpeed = new SELabel();
			this.lblPBMessagePBRetreived = new SELabel();
			this.lblPBMessageUserMessage = new SELabel();
			((ISupportInitialize)this.sePictureBox1).BeginInit();
			base.SuspendLayout();
			this.lblPBMessageTimeLeft.BackColor = Color.Transparent;
			this.lblPBMessageTimeLeft.Location = new Point(25, 58);
			this.lblPBMessageTimeLeft.Name = "lblPBMessageTimeLeft";
			this.lblPBMessageTimeLeft.Size = new Size(351, 20);
			this.lblPBMessageTimeLeft.TabIndex = 3;
			this.lblPBMessageTimeLeft.Text = "Time Left: ## : ## : ##";
			this.lblPBMessageTimeLeft.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPBMessageHeader.BackColor = Color.Transparent;
			this.lblPBMessageHeader.Location = new Point(30, 3);
			this.lblPBMessageHeader.Name = "lblPBMessageHeader";
			this.lblPBMessageHeader.Size = new Size(351, 30);
			this.lblPBMessageHeader.TabIndex = 2;
			this.lblPBMessageHeader.Text = "Caption";
			this.lblPBMessageHeader.TextAlign = ContentAlignment.MiddleLeft;
			this.lblPBMessageHeader.MouseMove += new MouseEventHandler(this.lblPBMessageHeader_MouseMove);
			this.lblPBMessageHeader.MouseDown += new MouseEventHandler(this.lblPBMessageHeader_MouseDown);
			this.lblPBMessageHeader.MouseUp += new MouseEventHandler(this.lblPBMessageHeader_MouseUp);
			this.btnPBMessageAbort.BackColor = Color.Silver;
			this.btnPBMessageAbort.Location = new Point(318, 127);
			this.btnPBMessageAbort.Name = "btnPBMessageAbort";
			this.btnPBMessageAbort.Size = new Size(63, 29);
			this.btnPBMessageAbort.TabIndex = 4;
			this.btnPBMessageAbort.Text = "Abort";
			this.btnPBMessageAbort.UseVisualStyleBackColor = true;
			this.btnPBMessageAbort.Click += new EventHandler(this.btnPBMessageAbort_Click);
			this.sePictureBox1.BackColor = Color.Transparent;
			this.sePictureBox1.BackgroundImage = Resources.SolarEdgeLogo;
			this.sePictureBox1.BackgroundImageLayout = ImageLayout.Zoom;
			this.sePictureBox1.ImageListBack = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListBack");
			this.sePictureBox1.ImageListFore = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListFore");
			this.sePictureBox1.Location = new Point(33, 128);
			this.sePictureBox1.Name = "sePictureBox1";
			this.sePictureBox1.SelectedImageBack = -1;
			this.sePictureBox1.SelectedImageFore = -1;
			this.sePictureBox1.Size = new Size(87, 25);
			this.sePictureBox1.TabIndex = 8;
			this.sePictureBox1.TabStop = false;
			this.lblPBMessageDetectedSpeed.BackColor = Color.Transparent;
			this.lblPBMessageDetectedSpeed.Location = new Point(25, 35);
			this.lblPBMessageDetectedSpeed.Name = "lblPBMessageDetectedSpeed";
			this.lblPBMessageDetectedSpeed.Size = new Size(351, 20);
			this.lblPBMessageDetectedSpeed.TabIndex = 9;
			this.lblPBMessageDetectedSpeed.Text = "Current Speed: High";
			this.lblPBMessageDetectedSpeed.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPBMessagePBRetreived.BackColor = Color.Transparent;
			this.lblPBMessagePBRetreived.Location = new Point(25, 80);
			this.lblPBMessagePBRetreived.Name = "lblPBMessagePBRetreived";
			this.lblPBMessagePBRetreived.Size = new Size(351, 20);
			this.lblPBMessagePBRetreived.TabIndex = 10;
			this.lblPBMessagePBRetreived.Text = "PowerBoxes retreived: 0";
			this.lblPBMessagePBRetreived.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPBMessageUserMessage.BackColor = Color.Transparent;
			this.lblPBMessageUserMessage.Location = new Point(25, 104);
			this.lblPBMessageUserMessage.Name = "lblPBMessageUserMessage";
			this.lblPBMessageUserMessage.Size = new Size(351, 20);
			this.lblPBMessageUserMessage.TabIndex = 11;
			this.lblPBMessageUserMessage.TextAlign = ContentAlignment.MiddleCenter;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandSmallNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(399, 168);
			base.ControlBox = false;
			base.Controls.Add(this.lblPBMessageUserMessage);
			base.Controls.Add(this.lblPBMessagePBRetreived);
			base.Controls.Add(this.lblPBMessageDetectedSpeed);
			base.Controls.Add(this.sePictureBox1);
			base.Controls.Add(this.btnPBMessageAbort);
			base.Controls.Add(this.lblPBMessageTimeLeft);
			base.Controls.Add(this.lblPBMessageHeader);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "PowerBoxTelemChangeMessage";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			((ISupportInitialize)this.sePictureBox1).EndInit();
			base.ResumeLayout(false);
		}
	}
}
