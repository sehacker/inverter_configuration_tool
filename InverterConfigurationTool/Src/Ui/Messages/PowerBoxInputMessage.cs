using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using SEInput.BarCode;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.Messages
{
	public class PowerBoxInputMessage : SEForm
	{
		private static PowerBoxInputMessage _instance = null;

		private static string _lastInput = "";

		private BarCodeChecker _barCodeChecker = new BarCodeChecker();

		private string[] retValData;

		private IContainer components = null;

		private SELabel lblPBMessageHeader;

		private SELabel lblPBMessageMessage;

		private SEButton btnPBMessageAccept;

		private SEButton btnPBMessageCancel;

		private SELabel lblPBMessageMarker;

		private SETextBox txtPBMessageInputCheckSum;

		private SEPictureBox sePictureBox1;

		private SETextBox txtPBMessageInputSerial;

		private SECheckBox chkPBMessageUseBarcode;

		public static string LastInput
		{
			get
			{
				return PowerBoxInputMessage._lastInput;
			}
		}

		private PowerBoxInputMessage()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
		}

		private void SetupLabels()
		{
			this.lblPBMessageHeader.SetUI(false, false);
			this.lblPBMessageMessage.SetUI(false, false);
			this.lblPBMessageMarker.SetUI(false, false);
			this.txtPBMessageInputSerial.TSAccess.Text = "";
			this.txtPBMessageInputCheckSum.TSAccess.Text = "";
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (((ISEUI)control).IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_7E)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		public static DialogResult ShowPowerBoxInputMessage(Form owner, string message, string caption)
		{
			if (PowerBoxInputMessage._instance == null)
			{
				PowerBoxInputMessage._instance = new PowerBoxInputMessage();
			}
			PowerBoxInputMessage._instance.SetForm(message, caption);
			return PowerBoxInputMessage._instance.ShowDialog(owner);
		}

		private void SetForm(string message, string caption)
		{
			this.lblPBMessageHeader.TSAccess.Text = caption;
			this.lblPBMessageMessage.TSAccess.Text = message;
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			this.txtPBMessageInputSerial.Focus();
			base.OnShown(e);
		}

		private void btnPBMessageCancel_Click(object sender, EventArgs e)
		{
			this.txtPBMessageInputSerial.TSAccess.Text = "";
			this.txtPBMessageInputCheckSum.TSAccess.Text = "";
			PowerBoxInputMessage._lastInput = "";
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void btnPBMessageAccept_Click(object sender, EventArgs e)
		{
			string text = this.txtPBMessageInputSerial.TSAccess.Text;
			string text2 = this.txtPBMessageInputCheckSum.TSAccess.Text;
			try
			{
				uint num = uint.Parse(text, NumberStyles.HexNumber);
				uint num2 = uint.Parse(text2, NumberStyles.HexNumber);
				string checkSum = Utils.GetCheckSum(text);
				if (!checkSum.Equals(text2))
				{
					throw new Exception(DataManager.Instance.Dictionary["msgPBDataAddChecksumError"]);
				}
				base.DialogResult = DialogResult.OK;
				PowerBoxInputMessage._lastInput = this.txtPBMessageInputSerial.TSAccess.Text + "-" + this.txtPBMessageInputCheckSum.TSAccess.Text;
				this.txtPBMessageInputSerial.TSAccess.Text = "";
				this.txtPBMessageInputCheckSum.TSAccess.Text = "";
				base.Close();
			}
			catch (FormatException var_5_D9)
			{
				MessageBox.Show(DataManager.Instance.Dictionary["msgPBDataAddFormatErrorMessage"], DataManager.Instance.Dictionary["msgPBDataAddFormatErrorTitle"], MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, DataManager.Instance.Dictionary["msgPBDataAddGeneralErrorTitle"], MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblPBMessageHeader.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblPBMessageHeader_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblPBMessageHeader.Size.Height);
		}

		private void lblPBMessageHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblPBMessageHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		private void txtPBMessageInputSerial_KeyDown(object sender, KeyEventArgs e)
		{
			if (!this.chkPBMessageUseBarcode.Checked)
			{
				if (!this.txtPBMessageInputSerial.IsDataValid(DataValidationType.HEX, e.KeyValue))
				{
					e.SuppressKeyPress = true;
				}
			}
			else
			{
				if (!this._barCodeChecker.CheckBarCodeForIDAndCheckSum(e, ref this.retValData))
				{
					SETextBox sETextBox = (SETextBox)sender;
					if (this.retValData != null && this.retValData.Length >= 2)
					{
						this.txtPBMessageInputSerial.Text = this.retValData[0];
						this.txtPBMessageInputCheckSum.Text = this.retValData[1];
						this.retValData = null;
					}
					else if (!this.txtPBMessageInputSerial.IsDataValid(DataValidationType.HEX, e.KeyValue))
					{
						e.SuppressKeyPress = true;
					}
				}
				else
				{
					this.txtPBMessageInputSerial.Text = "";
					this.txtPBMessageInputCheckSum.Text = "";
				}
				if (this._barCodeChecker.ParsingDone)
				{
					this._barCodeChecker.Reset();
				}
			}
		}

		private void txtPBMessageInputCheckSum_KeyDown(object sender, KeyEventArgs e)
		{
			int keyValue = e.KeyValue;
			if ((keyValue < 65 || keyValue > 70) && (keyValue < 48 || keyValue > 57) && (keyValue < 96 || keyValue > 105) && keyValue != 46 && keyValue != 8 && keyValue != 16)
			{
				e.SuppressKeyPress = true;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(PowerBoxInputMessage));
			this.lblPBMessageHeader = new SELabel();
			this.lblPBMessageMessage = new SELabel();
			this.btnPBMessageAccept = new SEButton();
			this.btnPBMessageCancel = new SEButton();
			this.txtPBMessageInputSerial = new SETextBox();
			this.lblPBMessageMarker = new SELabel();
			this.txtPBMessageInputCheckSum = new SETextBox();
			this.sePictureBox1 = new SEPictureBox();
			this.chkPBMessageUseBarcode = new SECheckBox();
			((ISupportInitialize)this.sePictureBox1).BeginInit();
			base.SuspendLayout();
			this.lblPBMessageHeader.BackColor = Color.Transparent;
			this.lblPBMessageHeader.Location = new Point(30, 3);
			this.lblPBMessageHeader.Name = "lblPBMessageHeader";
			this.lblPBMessageHeader.Size = new Size(351, 30);
			this.lblPBMessageHeader.TabIndex = 0;
			this.lblPBMessageHeader.Text = "Caption";
			this.lblPBMessageHeader.TextAlign = ContentAlignment.MiddleLeft;
			this.lblPBMessageHeader.MouseMove += new MouseEventHandler(this.lblPBMessageHeader_MouseMove);
			this.lblPBMessageHeader.MouseDown += new MouseEventHandler(this.lblPBMessageHeader_MouseDown);
			this.lblPBMessageHeader.MouseUp += new MouseEventHandler(this.lblPBMessageHeader_MouseUp);
			this.lblPBMessageMessage.BackColor = Color.Transparent;
			this.lblPBMessageMessage.Location = new Point(30, 36);
			this.lblPBMessageMessage.Name = "lblPBMessageMessage";
			this.lblPBMessageMessage.Size = new Size(351, 44);
			this.lblPBMessageMessage.TabIndex = 1;
			this.lblPBMessageMessage.Text = "Message";
			this.btnPBMessageAccept.BackColor = Color.Silver;
			this.btnPBMessageAccept.Location = new Point(249, 127);
			this.btnPBMessageAccept.Name = "btnPBMessageAccept";
			this.btnPBMessageAccept.Size = new Size(63, 29);
			this.btnPBMessageAccept.TabIndex = 2;
			this.btnPBMessageAccept.Text = "Accept";
			this.btnPBMessageAccept.UseVisualStyleBackColor = true;
			this.btnPBMessageAccept.Click += new EventHandler(this.btnPBMessageAccept_Click);
			this.btnPBMessageCancel.BackColor = Color.Silver;
			this.btnPBMessageCancel.Location = new Point(318, 127);
			this.btnPBMessageCancel.Name = "btnPBMessageCancel";
			this.btnPBMessageCancel.Size = new Size(63, 29);
			this.btnPBMessageCancel.TabIndex = 3;
			this.btnPBMessageCancel.Text = "Cancel";
			this.btnPBMessageCancel.UseVisualStyleBackColor = true;
			this.btnPBMessageCancel.Click += new EventHandler(this.btnPBMessageCancel_Click);
			this.txtPBMessageInputSerial.CharacterCasing = CharacterCasing.Upper;
			this.txtPBMessageInputSerial.Location = new Point(62, 84);
			this.txtPBMessageInputSerial.MaxLength = 9;
			this.txtPBMessageInputSerial.Name = "txtPBMessageInputSerial";
			this.txtPBMessageInputSerial.Size = new Size(202, 20);
			this.txtPBMessageInputSerial.TabIndex = 4;
			this.txtPBMessageInputSerial.KeyDown += new KeyEventHandler(this.txtPBMessageInputSerial_KeyDown);
			this.lblPBMessageMarker.BackColor = Color.Transparent;
			this.lblPBMessageMarker.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPBMessageMarker.Location = new Point(256, 79);
			this.lblPBMessageMarker.Name = "lblPBMessageMarker";
			this.lblPBMessageMarker.Size = new Size(40, 30);
			this.lblPBMessageMarker.TabIndex = 5;
			this.lblPBMessageMarker.Text = "-";
			this.lblPBMessageMarker.TextAlign = ContentAlignment.MiddleCenter;
			this.txtPBMessageInputCheckSum.CharacterCasing = CharacterCasing.Upper;
			this.txtPBMessageInputCheckSum.Location = new Point(287, 84);
			this.txtPBMessageInputCheckSum.MaxLength = 6;
			this.txtPBMessageInputCheckSum.Name = "txtPBMessageInputCheckSum";
			this.txtPBMessageInputCheckSum.Size = new Size(45, 20);
			this.txtPBMessageInputCheckSum.TabIndex = 6;
			this.txtPBMessageInputCheckSum.KeyDown += new KeyEventHandler(this.txtPBMessageInputCheckSum_KeyDown);
			this.sePictureBox1.BackColor = Color.Transparent;
			this.sePictureBox1.BackgroundImage = Resources.SolarEdgeLogo;
			this.sePictureBox1.BackgroundImageLayout = ImageLayout.Zoom;
			this.sePictureBox1.ImageListBack = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListBack");
			this.sePictureBox1.ImageListFore = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListFore");
			this.sePictureBox1.Location = new Point(26, 130);
			this.sePictureBox1.Name = "sePictureBox1";
			this.sePictureBox1.SelectedImageBack = -1;
			this.sePictureBox1.SelectedImageFore = -1;
			this.sePictureBox1.Size = new Size(87, 25);
			this.sePictureBox1.TabIndex = 7;
			this.sePictureBox1.TabStop = false;
			this.chkPBMessageUseBarcode.AutoSize = true;
			this.chkPBMessageUseBarcode.BackColor = Color.Transparent;
			this.chkPBMessageUseBarcode.Location = new Point(27, 109);
			this.chkPBMessageUseBarcode.Name = "chkPBMessageUseBarcode";
			this.chkPBMessageUseBarcode.Size = new Size(126, 17);
			this.chkPBMessageUseBarcode.TabIndex = 8;
			this.chkPBMessageUseBarcode.Text = "Use Barcode Reader";
			this.chkPBMessageUseBarcode.UseVisualStyleBackColor = false;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandSmallNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(399, 168);
			base.ControlBox = false;
			base.Controls.Add(this.chkPBMessageUseBarcode);
			base.Controls.Add(this.sePictureBox1);
			base.Controls.Add(this.txtPBMessageInputCheckSum);
			base.Controls.Add(this.txtPBMessageInputSerial);
			base.Controls.Add(this.btnPBMessageCancel);
			base.Controls.Add(this.btnPBMessageAccept);
			base.Controls.Add(this.lblPBMessageMessage);
			base.Controls.Add(this.lblPBMessageHeader);
			base.Controls.Add(this.lblPBMessageMarker);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "PowerBoxInputMessage";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			((ISupportInitialize)this.sePictureBox1).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
