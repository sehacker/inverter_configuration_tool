using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using SESecurity;
using SEStorage;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.Main
{
	public class OptionsForm : SEForm
	{
		private const string PASSWORD_MESSAGE_CAPTION = "PasswordMessageCaption";

		private const string PASSWORD_MESSAGE_LEVEL_ACHIEVED = "PasswordMessageHighLevelAchieved";

		private const string PASSWORD_MESSAGE_MONITORING = "PasswordMessageMonitoring";

		private const string PASSWORD_MESSAGE_TECHNICIAN = "PasswordMessageTechnician";

		private const string PASSWORD_MESSAGE_CERTIFICATE = "PasswordMessageCertificate";

		private const string PASSWORD_MESSAGE_ADMIN = "PasswordMessageAdmin";

		private const string USER_DATA_NOT_SAVED_MESSAGE = "UserDataNotSavedMessage";

		private IContainer components = null;

		private SECheckBox chkOptionsDisplayConnectionOnStart;

		private SEGroupBox grbOptionsAccessControl;

		private SEButton btnOptionsLogin;

		private SELabel lblAccessLevelName;

		private SELabel lblAccessLevel;

		private SELabel lblPassword;

		private SETextBox txtPassword;

		private SELabel lblOptionsLanguage;

		private SEComboBox cmbOptionsLanguage;

		private SEButton btnOptionsApply;

		private SELabel lblOptionsHeader;

		private SEButton btnOptionsBack;

		private SELabel lblPasswordHeader;

		private SELabel lblUsernameHeader;

		private SETextBox txtUsername;

		private SECheckBox chkOptionsHidePassword;

		private static OptionsForm _instance = null;

		private static bool _shouldRefreshAfterClose = false;

		private bool _shouldOnClosingHandle = true;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.chkOptionsDisplayConnectionOnStart = new SECheckBox();
			this.grbOptionsAccessControl = new SEGroupBox();
			this.chkOptionsHidePassword = new SECheckBox();
			this.lblPasswordHeader = new SELabel();
			this.lblUsernameHeader = new SELabel();
			this.txtUsername = new SETextBox();
			this.btnOptionsLogin = new SEButton();
			this.lblAccessLevelName = new SELabel();
			this.lblAccessLevel = new SELabel();
			this.lblPassword = new SELabel();
			this.txtPassword = new SETextBox();
			this.lblOptionsLanguage = new SELabel();
			this.cmbOptionsLanguage = new SEComboBox();
			this.btnOptionsApply = new SEButton();
			this.lblOptionsHeader = new SELabel();
			this.btnOptionsBack = new SEButton();
			this.grbOptionsAccessControl.SuspendLayout();
			base.SuspendLayout();
			this.chkOptionsDisplayConnectionOnStart.BackColor = Color.Transparent;
			this.chkOptionsDisplayConnectionOnStart.Checked = true;
			this.chkOptionsDisplayConnectionOnStart.CheckState = CheckState.Checked;
			this.chkOptionsDisplayConnectionOnStart.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.chkOptionsDisplayConnectionOnStart.Location = new Point(16, 206);
			this.chkOptionsDisplayConnectionOnStart.Name = "chkOptionsDisplayConnectionOnStart";
			this.chkOptionsDisplayConnectionOnStart.Size = new Size(371, 21);
			this.chkOptionsDisplayConnectionOnStart.TabIndex = 9;
			this.chkOptionsDisplayConnectionOnStart.Text = "Display Connection Window on Start";
			this.chkOptionsDisplayConnectionOnStart.UseVisualStyleBackColor = false;
			this.chkOptionsDisplayConnectionOnStart.CheckedChanged += new EventHandler(this.chkOptionsDisplayConnectionOnStart_CheckedChanged);
			this.grbOptionsAccessControl.BackColor = Color.Transparent;
			this.grbOptionsAccessControl.Controls.Add(this.chkOptionsHidePassword);
			this.grbOptionsAccessControl.Controls.Add(this.lblPasswordHeader);
			this.grbOptionsAccessControl.Controls.Add(this.lblUsernameHeader);
			this.grbOptionsAccessControl.Controls.Add(this.txtUsername);
			this.grbOptionsAccessControl.Controls.Add(this.btnOptionsLogin);
			this.grbOptionsAccessControl.Controls.Add(this.lblAccessLevelName);
			this.grbOptionsAccessControl.Controls.Add(this.lblAccessLevel);
			this.grbOptionsAccessControl.Controls.Add(this.lblPassword);
			this.grbOptionsAccessControl.Controls.Add(this.txtPassword);
			this.grbOptionsAccessControl.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.grbOptionsAccessControl.Location = new Point(12, 44);
			this.grbOptionsAccessControl.Name = "grbOptionsAccessControl";
			this.grbOptionsAccessControl.Size = new Size(565, 120);
			this.grbOptionsAccessControl.TabIndex = 2;
			this.grbOptionsAccessControl.TabStop = false;
			this.grbOptionsAccessControl.Text = "Access Control";
			this.chkOptionsHidePassword.BackColor = Color.Transparent;
			this.chkOptionsHidePassword.Checked = true;
			this.chkOptionsHidePassword.CheckState = CheckState.Checked;
			this.chkOptionsHidePassword.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.chkOptionsHidePassword.Location = new Point(437, 90);
			this.chkOptionsHidePassword.Name = "chkOptionsHidePassword";
			this.chkOptionsHidePassword.Size = new Size(122, 21);
			this.chkOptionsHidePassword.TabIndex = 7;
			this.chkOptionsHidePassword.Text = "Hide Password";
			this.chkOptionsHidePassword.UseVisualStyleBackColor = false;
			this.chkOptionsHidePassword.CheckedChanged += new EventHandler(this.chkOptionsHidePassword_CheckedChanged);
			this.lblPasswordHeader.BackColor = Color.Transparent;
			this.lblPasswordHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPasswordHeader.ForeColor = Color.Black;
			this.lblPasswordHeader.Location = new Point(96, 90);
			this.lblPasswordHeader.Name = "lblPasswordHeader";
			this.lblPasswordHeader.Size = new Size(77, 18);
			this.lblPasswordHeader.TabIndex = 43;
			this.lblPasswordHeader.Tag = "";
			this.lblPasswordHeader.Text = "Password";
			this.lblPasswordHeader.TextAlign = ContentAlignment.MiddleRight;
			this.lblUsernameHeader.BackColor = Color.Transparent;
			this.lblUsernameHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblUsernameHeader.ForeColor = Color.Black;
			this.lblUsernameHeader.Location = new Point(105, 62);
			this.lblUsernameHeader.Name = "lblUsernameHeader";
			this.lblUsernameHeader.Size = new Size(68, 18);
			this.lblUsernameHeader.TabIndex = 42;
			this.lblUsernameHeader.Tag = "";
			this.lblUsernameHeader.Text = "Portia ID";
			this.lblUsernameHeader.TextAlign = ContentAlignment.MiddleRight;
			this.txtUsername.BackColor = Color.White;
			this.txtUsername.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.txtUsername.Location = new Point(177, 60);
			this.txtUsername.MaxLength = 100;
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new Size(254, 22);
			this.txtUsername.TabIndex = 4;
			this.txtUsername.Tag = "";
			this.btnOptionsLogin.BackColor = Color.Silver;
			this.btnOptionsLogin.Location = new Point(437, 57);
			this.btnOptionsLogin.Name = "btnOptionsLogin";
			this.btnOptionsLogin.Size = new Size(122, 29);
			this.btnOptionsLogin.TabIndex = 6;
			this.btnOptionsLogin.Text = "Log In";
			this.btnOptionsLogin.UseVisualStyleBackColor = true;
			this.btnOptionsLogin.Click += new EventHandler(this.btnOptionsLogin_Click);
			this.lblAccessLevelName.BackColor = Color.Transparent;
			this.lblAccessLevelName.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblAccessLevelName.ForeColor = Color.Black;
			this.lblAccessLevelName.Location = new Point(7, 23);
			this.lblAccessLevelName.Name = "lblAccessLevelName";
			this.lblAccessLevelName.Size = new Size(152, 29);
			this.lblAccessLevelName.TabIndex = 38;
			this.lblAccessLevelName.Tag = "";
			this.lblAccessLevelName.Text = "Current Access Level:";
			this.lblAccessLevelName.TextAlign = ContentAlignment.MiddleLeft;
			this.lblAccessLevel.BackColor = Color.White;
			this.lblAccessLevel.BorderStyle = BorderStyle.FixedSingle;
			this.lblAccessLevel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblAccessLevel.ForeColor = Color.Black;
			this.lblAccessLevel.Location = new Point(177, 25);
			this.lblAccessLevel.Name = "lblAccessLevel";
			this.lblAccessLevel.Size = new Size(129, 26);
			this.lblAccessLevel.TabIndex = 3;
			this.lblAccessLevel.Tag = "";
			this.lblAccessLevel.Text = "Monitoring";
			this.lblAccessLevel.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPassword.BackColor = Color.Transparent;
			this.lblPassword.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPassword.ForeColor = Color.Black;
			this.lblPassword.Location = new Point(8, 57);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new Size(101, 29);
			this.lblPassword.TabIndex = 35;
			this.lblPassword.Tag = "";
			this.lblPassword.Text = "Set New Level:";
			this.lblPassword.TextAlign = ContentAlignment.MiddleLeft;
			this.txtPassword.BackColor = Color.White;
			this.txtPassword.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.txtPassword.Location = new Point(177, 88);
			this.txtPassword.MaxLength = 100;
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new Size(254, 22);
			this.txtPassword.TabIndex = 5;
			this.txtPassword.Tag = "";
			this.txtPassword.UseSystemPasswordChar = true;
			this.lblOptionsLanguage.BackColor = Color.Transparent;
			this.lblOptionsLanguage.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblOptionsLanguage.Location = new Point(15, 175);
			this.lblOptionsLanguage.Name = "lblOptionsLanguage";
			this.lblOptionsLanguage.Size = new Size(149, 27);
			this.lblOptionsLanguage.TabIndex = 45;
			this.lblOptionsLanguage.Text = "Application Language:";
			this.lblOptionsLanguage.TextAlign = ContentAlignment.MiddleLeft;
			this.cmbOptionsLanguage.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbOptionsLanguage.FormattingEnabled = true;
			this.cmbOptionsLanguage.Location = new Point(165, 178);
			this.cmbOptionsLanguage.Name = "cmbOptionsLanguage";
			this.cmbOptionsLanguage.Size = new Size(111, 21);
			this.cmbOptionsLanguage.TabIndex = 8;
			this.cmbOptionsLanguage.SelectedIndexChanged += new EventHandler(this.cmbOptionsLanguage_SelectedIndexChanged);
			this.btnOptionsApply.BackColor = Color.Silver;
			this.btnOptionsApply.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.btnOptionsApply.Location = new Point(393, 206);
			this.btnOptionsApply.Name = "btnOptionsApply";
			this.btnOptionsApply.Size = new Size(89, 34);
			this.btnOptionsApply.TabIndex = 10;
			this.btnOptionsApply.Text = "Apply";
			this.btnOptionsApply.UseVisualStyleBackColor = true;
			this.btnOptionsApply.Click += new EventHandler(this.btnOptionsApply_Click);
			this.lblOptionsHeader.BackColor = Color.Transparent;
			this.lblOptionsHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblOptionsHeader.Location = new Point(31, 2);
			this.lblOptionsHeader.Name = "lblOptionsHeader";
			this.lblOptionsHeader.Size = new Size(527, 36);
			this.lblOptionsHeader.TabIndex = 42;
			this.lblOptionsHeader.Text = "Options";
			this.lblOptionsHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblOptionsHeader.MouseMove += new MouseEventHandler(this.lblOptionsHeader_MouseMove);
			this.lblOptionsHeader.MouseDown += new MouseEventHandler(this.lblOptionsHeader_MouseDown);
			this.lblOptionsHeader.MouseUp += new MouseEventHandler(this.lblOptionsHeader_MouseUp);
			this.btnOptionsBack.BackColor = Color.Silver;
			this.btnOptionsBack.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.btnOptionsBack.Location = new Point(488, 206);
			this.btnOptionsBack.Name = "btnOptionsBack";
			this.btnOptionsBack.Size = new Size(89, 34);
			this.btnOptionsBack.TabIndex = 11;
			this.btnOptionsBack.Text = "Back";
			this.btnOptionsBack.UseVisualStyleBackColor = true;
			this.btnOptionsBack.Click += new EventHandler(this.btnOptionsBack_Click);
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(589, 255);
			base.ControlBox = false;
			base.Controls.Add(this.btnOptionsBack);
			base.Controls.Add(this.chkOptionsDisplayConnectionOnStart);
			base.Controls.Add(this.grbOptionsAccessControl);
			base.Controls.Add(this.lblOptionsLanguage);
			base.Controls.Add(this.cmbOptionsLanguage);
			base.Controls.Add(this.btnOptionsApply);
			base.Controls.Add(this.lblOptionsHeader);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "OptionsForm";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			this.grbOptionsAccessControl.ResumeLayout(false);
			this.grbOptionsAccessControl.PerformLayout();
			base.ResumeLayout(false);
		}

		private OptionsForm()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
			this.SetLanguageComboBox();
		}

		private void SetupLabels()
		{
			this.lblOptionsHeader.SetUI(false, true);
			this.lblOptionsLanguage.SetUI(true, true);
			this.lblAccessLevelName.SetUI(true, true);
			this.lblPassword.SetUI(true, true);
			this.lblUsernameHeader.SetUI(false, true);
			this.lblPasswordHeader.SetUI(false, true);
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (iSEUI.IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_79)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		private void SetLanguageComboBox()
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			List<string> languages = dictionary.GetLanguages();
			int num = (languages != null) ? languages.Count : 0;
			if (num > 0)
			{
				this.cmbOptionsLanguage.Items.Clear();
			}
			for (int i = 0; i < num; i++)
			{
				string item = languages[i];
				this.cmbOptionsLanguage.Items.Add(item);
			}
			object obj = DataManager.Instance.UserSettings["SelectedLanguage"];
			string text = (obj != null) ? ((string)obj) : null;
			if (string.IsNullOrEmpty(text))
			{
				text = "English";
			}
			int selectedIndex = 0;
			int count = this.cmbOptionsLanguage.Items.Count;
			for (int i = 0; i < count; i++)
			{
				string value = (string)this.cmbOptionsLanguage.Items[i];
				if (text.Equals(value))
				{
					selectedIndex = i;
					break;
				}
			}
			this.cmbOptionsLanguage.SelectedIndex = selectedIndex;
		}

		public static void ShowOptionsForm(Form owner, bool shouldRefreshAfterClose)
		{
			if (OptionsForm._instance == null)
			{
				OptionsForm._instance = new OptionsForm();
			}
			OptionsForm._shouldRefreshAfterClose = shouldRefreshAfterClose;
			OptionsForm._instance.ShowDialog(owner);
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			object obj = DataManager.Instance.UserSettings["ShowConnectionFormOnStart"];
			this.chkOptionsDisplayConnectionOnStart.Checked = (obj == null || (bool)obj);
			string text = DataManager.Instance.Dictionary[DataManager.Instance.LoginItem.AccessLevel.ToString()];
			this.lblAccessLevel.TSAccess.Text = text;
			string str = "txtPassword";
			for (int i = 1; i <= 5; i++)
			{
				try
				{
					string key = str + i.ToString();
					TextBox textBox = (TextBox)this.grbOptionsAccessControl.Controls[key];
					textBox.Text = "";
				}
				catch (Exception var_6_B5)
				{
				}
			}
			object obj2 = DataManager.Instance.UserSettings["SelectedLanguage"];
			string key2 = (obj2 != null) ? ((string)obj2) : null;
			string text2 = DataManager.Instance.Dictionary[key2];
			if (!string.IsNullOrEmpty(text2))
			{
				this.cmbOptionsLanguage.SelectedItem = text2;
			}
			this.btnOptionsApply.Enabled = false;
			base.OnShown(e);
		}

		protected override void OnClosed(EventArgs e)
		{
			OptionsForm._shouldRefreshAfterClose = false;
			base.OnClosed(e);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			if (this._shouldOnClosingHandle)
			{
				if (this.btnOptionsApply.Enabled)
				{
					string text = DataManager.Instance.Dictionary["UserDataNotSavedMessage"];
					string caption = "";
					DialogResult dialogResult = MessageBox.Show(text, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button3);
					DialogResult dialogResult2 = dialogResult;
					if (dialogResult2 != DialogResult.Cancel)
					{
						if (dialogResult2 == DialogResult.Yes)
						{
							this.btnOptionsApply_Click(this.btnOptionsApply, null);
						}
					}
					else
					{
						e.Cancel = true;
					}
				}
				base.OnClosing(e);
			}
		}

		private void btnOptionsLogin_Click(object sender, EventArgs e)
		{
			DataManager instance = DataManager.Instance;
			string text = "";
			string caption = instance.Dictionary["PasswordMessageCaption"];
			string text2 = this.txtUsername.Text;
			string text3 = this.txtPassword.Text;
			LoginData loginData = new LoginData(new LoginCredentials(null, text2, DateTime.Now), text3, instance.LoginItem.AccessLevel);
			loginData.LevelRequested = AccessLevels.TECHNICIAN;
			loginData = instance.LoginItem.QuerryCredentials(loginData);
			if (loginData.LevelAttained == AccessLevels.TECHNICIAN)
			{
				string caption2 = DataManager.Instance.Dictionary["MsgDisclaimerTechnicianCaption"];
				string text4 = DataManager.Instance.Dictionary["MsgDisclaimerTechnicianMessage"];
				DialogResult dialogResult = MessageBox.Show(text4, caption2, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
				DialogResult dialogResult2 = dialogResult;
				if (dialogResult2 == DialogResult.No)
				{
					instance.LoginItem.AccessLevel = AccessLevels.MONITORING;
					return;
				}
			}
			else
			{
				loginData.LevelRequested = AccessLevels.CERTIFICATE;
				loginData = instance.LoginItem.QuerryCredentials(loginData);
				if (loginData.LevelAttained == AccessLevels.CERTIFICATE)
				{
					string caption2 = DataManager.Instance.Dictionary["MsgDisclaimerCertificateCaption"];
					string text4 = DataManager.Instance.Dictionary["MsgDisclaimerCertificateMessage"];
					DialogResult dialogResult = MessageBox.Show(text4, caption2, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
					DialogResult dialogResult2 = dialogResult;
					if (dialogResult2 == DialogResult.No)
					{
						instance.LoginItem.AccessLevel = AccessLevels.MONITORING;
						return;
					}
				}
				else
				{
					instance.LoginItem.ShouldValidateAdminOffline = true;
					loginData.LevelRequested = AccessLevels.ADMIN;
					loginData.Credentials.UserName = loginData.Credentials.DeviceID;
					loginData = instance.LoginItem.QuerryCredentials(loginData);
					instance.LoginItem.AccessLevel = loginData.LevelAttained;
				}
			}
			bool flag = loginData.LevelAttained == loginData.LevelRequested && loginData.LevelAttained != AccessLevels.MONITORING;
			MessageBoxIcon icon = flag ? MessageBoxIcon.Asterisk : MessageBoxIcon.Hand;
			if (flag)
			{
				instance.LoginItem.AccessLevel = loginData.LevelAttained;
				this.lblAccessLevel.TSAccess.Text = DataManager.Instance.Dictionary[DataManager.Instance.LoginItem.AccessLevel.ToString()];
				switch (instance.LoginItem.AccessLevel)
				{
				case AccessLevels.MONITORING:
					text = DataManager.Instance.Dictionary["PasswordMessageMonitoring"];
					break;
				case AccessLevels.TECHNICIAN:
					text = DataManager.Instance.Dictionary["PasswordMessageTechnician"];
					break;
				case AccessLevels.CERTIFICATE:
					text = DataManager.Instance.Dictionary["PasswordMessageCertificate"];
					break;
				case AccessLevels.ADMIN:
					text = DataManager.Instance.Dictionary["PasswordMessageAdmin"];
					break;
				}
			}
			else
			{
				text = DataManager.Instance.Dictionary["PasswordMessageMonitoring"];
			}
			if (text != null)
			{
				MessageBox.Show(text, caption, MessageBoxButtons.OK, icon, MessageBoxDefaultButton.Button1);
				if (instance.LoginItem.AccessLevel == AccessLevels.CERTIFICATE)
				{
					instance.LoginItem.AccessLevel = AccessLevels.ADMIN;
				}
			}
			if (instance.LoginItem.AccessLevel != AccessLevels.MONITORING)
			{
				base.Close();
			}
		}

		private void btnOptionsApply_Click(object sender, EventArgs e)
		{
			SEUserSettings userSettings = DataManager.Instance.UserSettings;
			userSettings["ShowConnectionFormOnStart"] = this.chkOptionsDisplayConnectionOnStart.Checked;
			if (this.cmbOptionsLanguage.SelectedItem != null)
			{
				string value = this.cmbOptionsLanguage.SelectedItem.ToString();
				userSettings["SelectedLanguage"] = value;
			}
			else
			{
				userSettings["SelectedLanguage"] = "";
			}
			string selectedLanguage = (string)userSettings["SelectedLanguage"];
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			dictionary.SelectedLanguage = selectedLanguage;
			MainForm.Instance.UpdateUI(MainForm.Instance);
			MainForm.Instance.UpdateSpecialUI();
			if (e != null)
			{
				this._shouldOnClosingHandle = false;
				base.Close();
				this._shouldOnClosingHandle = true;
			}
		}

		private void btnOptionsBack_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void chkOptionsDisplayConnectionOnStart_CheckedChanged(object sender, EventArgs e)
		{
			object obj = DataManager.Instance.UserSettings["ShowConnectionFormOnStart"];
			this.btnOptionsApply.TSAccess.Enabled = (obj == null || (bool)obj != this.chkOptionsDisplayConnectionOnStart.Checked);
		}

		private void cmbOptionsLanguage_SelectedIndexChanged(object sender, EventArgs e)
		{
			object obj = DataManager.Instance.UserSettings["SelectedLanguage"];
			string text = null;
			if (obj != null)
			{
				text = (string)obj;
			}
			string value = (this.cmbOptionsLanguage.SelectedItem != null) ? this.cmbOptionsLanguage.SelectedItem.ToString() : "";
			this.btnOptionsApply.TSAccess.Enabled = (text == null || !text.Equals(value));
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblOptionsHeader.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblOptionsHeader_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblOptionsHeader.Size.Height);
		}

		private void lblOptionsHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblOptionsHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		private void chkOptionsHidePassword_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox checkBox = (CheckBox)sender;
			bool @checked = checkBox.Checked;
			this.txtPassword.UseSystemPasswordChar = @checked;
		}
	}
}
