using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Data;
using InverterConfigurationTool.Src.Model.Devices;
using InverterConfigurationTool.Src.Model.Devices.Base;
using InverterConfigurationTool.Src.Ui.DataEntry;
using InverterConfigurationTool.Src.Ui.Messages;
using SEDevices.Data;
using SEDevices.Devices;
using SEDevices.Records.Parameter;
using SEDevices.Records.Status;
using SEDevices.Upgrade.Base;
using SESecurity;
using SEStorage;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using SEUI.Data;
using SEUI.Graphic;
using SEUI.ThreadSafeAccess;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.Main
{
	public class MainForm : SEForm, ISEUpgradeNotifiable
	{
		private delegate void AnimateUIDelegate();

		private const bool IS_BETA = false;

		private const int TELEM_SPEED_SLOW = 600;

		private const int TELEM_SPEED_NORMAL = 300;

		private const int TELEM_SPEED_FAST = 31;

		private const int TIME_TO_WAIT_BETWEEN_FORCE_TELEM_COMMANDS = 2000;

		private const uint NUM_OF_TELEM_BROADCAST_CMDS_TO_SEND = 3u;

		private IContainer components = null;

		private SEDataGridView dgvInverterList;

		private SEPanel pnlLineH;

		private SEPictureBox pbLogo;

		private SEPanel pnlInvListBackground;

		private SETabControl tbcMain;

		private SETabPage tpStatus;

		private SETabPage tpSettings;

		private SETabPage tpPBData;

		private SETabPage tpTools;

		private SEPanel pnlInverterStat;

		private SEPanel pnlTSSS;

		private SELabel lblInvStatFaultHeader;

		private SELabel lblInvStatModuleCommHeader;

		private SELabel lblInvStatPowerProductionHeader;

		private SELabel lblInvStatSEURLHeader;

		private SELabel lblTSSSDSP2Version;

		private SELabel lblTSSSDSP1Version;

		private SELabel lblTSSSCPUVersion;

		private SELabel lblTSSSInverterID;

		private SELabel lblTSSSInverterModel;

		private SELabel lblTSSSHeader;

		private SELabel lblTSCSInterInverterConfig;

		private SELabel lblTSCSServerConfig;

		private SEPanel pnlInverterList;

		private SELabel lblInvListHeader;

		private SEPanel pnlLineV;

		private SEDataGridView dgvPBParam;

		private SEPanel pnlPBLPTop;

		private SELabel lblPBParamHeader;

		private SETabControl tbcTools;

		private SETabPage tpFirmwareUpgrade;

		private SEPanel pnlMenuLineRight;

		private SEButton btnConnect;

		private SEButton btnRefresh;

		private SEPictureBox pbLCDButton;

		private SEButton btnSupport;

		private SEButton btnExit;

		private SEButton btnOptions;

		private SEPanel pnlTSRS;

		private SELabel lblTSRSCountry;

		private SELabel lblTSRSLanguage;

		private SELabel lblTSSETHeader;

		private SELCD lcdInverter;

		private SETabControl tbcSettings;

		private SETabPage tpRegionalSettings;

		private SEPanel pnlRSDSPParams;

		private SELabel lblRSIPVenusParam;

		private SEPanel pnlRSCPUParams;

		private SEDataGridView dgvPortiaParams;

		private SELabel lblRSIPPortiaParam;

		private SEPanel pnlRSRSTop;

		private SEComboBox cmbSettingsRSRSCountry;

		private SEComboBox cmbSettingsRSRSLanguage;

		private SELabel lblSettingsRSRSCountry;

		private SELabel lblSettingsRSRSLanguage;

		private SELabel lblSettingsRSRSHeader;

		private SEButton btnSettingsRSApply;

		private TabPage tpCommSettings;

		private SEPictureBox pbSettingsRS485;

		private SEPanel pnlSettingsRS485;

		private SEPictureBox pbSettingsRS232;

		private SELabel lblSettingsLANStatusCable;

		private SELabel lblSettingsLANStatusTCP;

		private SEPanel pnlSettingsIIC;

		private SEPanel pnlSettingsServer;

		private SEPanel pnlSettingsLAN;

		private SEPanel pnlSettingsZB;

		private SEPanel pnlSettingsRS232;

		private SEPanel pnlSettingsIICLine2;

		private SEPictureBox pbSettingsLAN;

		private SEPictureBox pbSettingsZB;

		private SEPictureBox pbSettingsIIC;

		private SEPictureBox pbSettingsServerCloud;

		private SEPictureBox pbSettingsInverter;

		private SEPanel pnlSettingsRS232Line1;

		private SEPanel pnlSettingsZBLine1;

		private SEPanel pnlSettingsLANLineTCPStatus;

		private SEPanel pnlSettingsRS232Line2;

		private SEPanel pnlSettingsLANLineEthernetCableStatus2;

		private SEPanel pnlSettingsServerLine1;

		private SEPanel pnlSettingsServerLine2;

		private SEPanel pnlSettingsIICLine1;

		private SEPanel pnlSettingsIICLine3;

		private SEPanel pnlSettingsLANLineEthernetCableStatus1;

		private SEPanel pnlSettingsRS485Line1;

		private SELabel lblSettingsRS232CommType;

		private SELabel lblSettingsRS232Header;

		private SELabel lblSettingsZBHeader;

		private SELabel lblSettingsLANCurrentIP;

		private SELabel lblSettingsLANDHCPStatus;

		private SELabel lblSettingsLANHeader;

		private SELabel lblSettingsLANSubnetMask;

		private SELabel lblSettingsLANGateway;

		private SELabel lblSettingsServerAddress;

		private SELabel lblSettingsServerPort;

		private SELabel lblSettingsServerVia;

		private SELabel lblSettingsServerHeader;

		private SEButton btnSettingsServerPingTest;

		private SELabel lblSettingsIICCommType;

		private SELabel lblSettingsIICHeader;

		private SEButton btnSettingsIICDetectSlaves;

		private SELabel lblSettingsRS485Header;

		private SELabel lblPBLastTelemTimeValue;

		private SELabel lblPBLastTelemTime;

		private SELabel lblTSLCD;

		private TabPage tpMisc;

		private SEPanel pnlTMRTC;

		private SELabel lblTMRTCGMTOffset;

		private SEPanel pnlTMPwrBalance;

		private SEButton btnTMPwrBalanceStatus;

		private SELabel lblTMPwrBalanceStatus;

		private SELabel lblTMPwrBalanceHeader;

		private SEButton btnTMRTCSet;

		private SELabel lblTMRTCHeader;

		private SEPanel pnlTUDetails;

		private SEPanel pnlTUUpdateControl;

		private SEButton btnTUUpdateControlBeginUpdate;

		private SETextBox txtTUUpdateControlFile;

		private SELabel lblTUUpdateControlFile;

		private SELabel lblTUUpdateControlHeader;

		private SEButton btnTUUpdateControlFile;

		private SELabel lblTUDetailsDSP1Value;

		private SELabel lblTUDetailsCPUValue;

		private SELabel lblTUDetailsDSP1;

		private SELabel lblTUDetailsCPU;

		private SELabel lblTUDetailsHeader;

		private SELabel lblTUDetailsUpgradeStatusHeader;

		private SELabel lblTUDetailsDSP2Value;

		private SELabel lblTUDetailsDSP2;

		private SEPanel pnlPBBtns;

		private SEGroupBox grbPBData;

		private SEButton btnPBDataLoad;

		private SEButton btnPBRecordsExport;

		private SEButton btnPBDataAdd;

		private SEButton btnPBDataRemove;

		private SEGroupBox grbPBTelemetry;

		private SEGroupBox grbPBRecords;

		private SECheckBox chkPBTelemetryNormal;

		private SECheckBox chkPBTelemetryFast;

		private SELabel lblPBBtnsHeader;

		private SELabel lblConnectionStatus;

		private SEPanel pnlMenuLineLeft;

		private SELabel lblConnectionStatusHeader;

		private SEButton btnPBForceTelem;

		private SELabel lblTSCSInterInverterConfigValue;

		private SELabel lblTSCSServerConfigValue;

		private SELabel lblTSRSCountryValue;

		private SELabel lblTSRSLanguageValue;

		private SELabel lblTSSSDSP2VersionValue;

		private SELabel lblTSSSDSP1VersionValue;

		private SELabel lblTSSSCPUVersionValue;

		private SELabel lblTSSSInverterIDValue;

		private SELabel lblTSSSInverterModelValue;

		private DataGridViewTextBoxColumn colID;

		private DataGridViewTextBoxColumn colHexID;

		private SELabel lblSettingsRS232CommTypeValue;

		private SELabel lblSettingsRS485Value;

		private SELabel lblSettingsLANGatewayValue;

		private SELabel lblSettingsLANSubnetMaskValue;

		private SELabel lblSettingsLANCurrentIPValue;

		private SELabel lblSettingsLANDHCPStatusValue;

		private SELabel lblSettingsZBValue;

		private SELabel lblSettingsServerViaValue;

		private SELabel lblSettingsServerPortValue;

		private SELabel lblSettingsServerAddressValue;

		private SELabel lblSettingsIICCommTypeValue;

		private SECheckBox chkTSInverterPanelShouldUpdate;

		private SELabel lblTMRTCGMTOffsetValue;

		private SEComboBox cmbTMPwrBalanceStatus;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;

		private DataGridViewTextBoxColumn colSerial;

		private DataGridViewTextBoxColumn colVin;

		private DataGridViewTextBoxColumn colVOut;

		private DataGridViewTextBoxColumn colIIn;

		private DataGridViewTextBoxColumn colEnergy;

		private DataGridViewTextBoxColumn colTime;

		private DataGridViewTextBoxColumn colVersion;

		private SELabel lblUpgradeProgressProgram;

		private SELabel lblUpgradeProgressTotal;

		private SETextBox txtTUDetailsStatus;

		private SELabel lblPBInverterWarning;

		private SEButton btnPBDataVersionUpdate;

		private SEDataGridView dgvVenusParams;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;

		private DataGridViewTextBoxColumn colIndex;

		private DataGridViewTextBoxColumn colName;

		private DataGridViewTextBoxColumn colValue;

		private DataGridViewTextBoxColumn colUnit;

		private SELabel lblTMRTCGMTCurrentValue;

		private SELabel lblTMRTCGMTCurrent;

		private SEProgressBarBasic pbUpgradeProgress;

		private SEProgressBarBasic pbUpgradeProgressProgram;

		public static string[] SCREEN_NAMES = new string[]
		{
			"tpStatus",
			"tpSettings",
			"tpRegionalSettings",
			"tpCommSettings",
			"tpPBData",
			"tpTools",
			"tpFirmwareUpgrade",
			"tpMisc"
		};

		private static Color COLOR_CONNECTED = Color.Lime;

		private static Color COLOR_DISCONNECTED = Color.Red;

		private static Color COLOR_UNAVAILABLE = Color.Gray;

		private static Color COLOR_NOT_SUPPOTED = Color.Black;

		private static LCDScreenStates[] LCD_SCREENS_1_5082 = new LCDScreenStates[]
		{
			LCDScreenStates.SCREEN_STATUS_1,
			LCDScreenStates.SCREEN_STATUS_2
		};

		private static LCDScreenStates[] LCD_SCREENS_2_0023 = new LCDScreenStates[]
		{
			LCDScreenStates.SCREEN_STATUS_1,
			LCDScreenStates.SCREEN_STATUS_2,
			LCDScreenStates.SCREEN_SPEC,
			LCDScreenStates.SCREEN_IIC_STATUS,
			LCDScreenStates.SCREEN_COMM_STATUS
		};

		private static LCDScreenStates[] LCD_SCREENS_2_0038 = new LCDScreenStates[]
		{
			LCDScreenStates.SCREEN_STATUS_1,
			LCDScreenStates.SCREEN_STATUS_2,
			LCDScreenStates.SCREEN_ENERGY,
			LCDScreenStates.SCREEN_SPEC,
			LCDScreenStates.SCREEN_IIC_STATUS,
			LCDScreenStates.SCREEN_COMM_STATUS
		};

		private static MainForm _instance = null;

		private SEProgressBar _progressBar = null;

		private SWVersion _appVersion = null;

		private bool _enableForceTelem = true;

		private string _forceTelemString = "";

		private bool _enableUpdateVersion = true;

		private string _updateVersionString = "";

		private ScreenName _selectedScreenName = ScreenName.NONE;

		private object _monitoringPauseObj = new object();

		private bool _shouldMonitoringPause = false;

		private object _shouldAnimateWorkKLockObj = new object();

		private bool _shouldAnimteObject = true;

		private object _shouldUpdateInverterListObject = new object();

		private bool _shouldUpdateInverterList = false;

		private SEThread _thUIAnimate = null;

		private SEThread _thForceTelem = null;

		private Dictionary<uint, int> _inverterListLog = null;

		public bool IsScreenWaitingForTransmition = false;

		private object _syncObjShouldUpdate = new object();

		private bool[] _screenStatusShouldUpdate = null;

		private object _rsDataChangedSynchObject = new object();

		private bool _lastRSChangedStatus = false;

		private bool _isCountryChanged = false;

		private bool _isLanguageChanged = false;

		private Dictionary<int, object> _parameterChangedPortia = new Dictionary<int, object>(0);

		private Dictionary<int, object> _parameterChangedVenus = new Dictionary<int, object>(0);

		private Dictionary<int, object> _parameterChangedJupiter = new Dictionary<int, object>(0);

		private object _csDataChangedSynchObject = new object();

		private bool _isCsDataRequireUpdate = false;

		private object _toolsMiscDataChangedSynchObject = new object();

		private bool _isRTCChanged = false;

		private Dictionary<string, int> _countryTable = null;

		private Dictionary<string, int> _languageTable = null;

		private static ScreenName _tabName = ScreenName.NONE;

		private static bool _isOverrideRemote = false;

		private ManualResetEvent _thNotifyAnimate = new ManualResetEvent(false);

		private bool _shouldTBCMainEventFire = true;

		private bool _shouldCheckEventFastBeActive = true;

		private bool _shouldCheckEventNormalBeActive = true;

		public static MainForm Instance
		{
			get
			{
				if (MainForm._instance == null)
				{
					MainForm._instance = new MainForm();
				}
				return MainForm._instance;
			}
		}

		public SWVersion ApplicationVersion
		{
			get
			{
				if (this._appVersion == null)
				{
					this._appVersion = new SWVersion();
					Version version = Assembly.GetExecutingAssembly().GetName().Version;
					this._appVersion.Major = (uint)version.Major;
					this._appVersion.Minor = (uint)version.Minor;
					this._appVersion.Build = (uint)version.Build;
					this._appVersion.Revision = (uint)version.Revision;
					this._appVersion.Beta = false;
				}
				return this._appVersion;
			}
		}

		public bool EnableForceTelem
		{
			get
			{
				return this._enableForceTelem;
			}
			set
			{
				this._enableForceTelem = value;
			}
		}

		public string ForceTelemString
		{
			get
			{
				return this._forceTelemString;
			}
			set
			{
				this._forceTelemString = value;
			}
		}

		public bool EnableUpdateVersion
		{
			get
			{
				return this._enableUpdateVersion;
			}
			set
			{
				this._enableUpdateVersion = value;
			}
		}

		public string UpdateVersionString
		{
			get
			{
				return this._updateVersionString;
			}
			set
			{
				this._updateVersionString = value;
			}
		}

		public ScreenName SelectedScreen
		{
			get
			{
				return this._selectedScreenName;
			}
			set
			{
				this.SwitchScreen(value);
			}
		}

		public bool PauseMonitoring
		{
			get
			{
				bool shouldMonitoringPause;
				lock (this._monitoringPauseObj)
				{
					shouldMonitoringPause = this._shouldMonitoringPause;
				}
				return shouldMonitoringPause;
			}
			set
			{
				lock (this._monitoringPauseObj)
				{
					this._shouldMonitoringPause = value;
				}
			}
		}

		public bool AnimateObjects
		{
			get
			{
				bool shouldAnimteObject;
				lock (this._shouldAnimateWorkKLockObj)
				{
					shouldAnimteObject = this._shouldAnimteObject;
				}
				return shouldAnimteObject;
			}
			set
			{
				lock (this._shouldAnimateWorkKLockObj)
				{
					this._shouldAnimteObject = value;
				}
			}
		}

		public bool UpdateInverterList
		{
			get
			{
				bool shouldUpdateInverterList;
				lock (this._shouldUpdateInverterListObject)
				{
					shouldUpdateInverterList = this._shouldUpdateInverterList;
				}
				return shouldUpdateInverterList;
			}
			set
			{
				lock (this._shouldUpdateInverterListObject)
				{
					this._shouldUpdateInverterList = value;
				}
			}
		}

		private bool LastRSChangedStatus
		{
			get
			{
				bool lastRSChangedStatus;
				lock (this._rsDataChangedSynchObject)
				{
					lastRSChangedStatus = this._lastRSChangedStatus;
				}
				return lastRSChangedStatus;
			}
			set
			{
				lock (this._rsDataChangedSynchObject)
				{
					this._lastRSChangedStatus = value;
				}
			}
		}

		private bool CountryChanged
		{
			get
			{
				bool isCountryChanged;
				lock (this._rsDataChangedSynchObject)
				{
					isCountryChanged = this._isCountryChanged;
				}
				return isCountryChanged;
			}
			set
			{
				lock (this._rsDataChangedSynchObject)
				{
					this._isCountryChanged = value;
				}
			}
		}

		private bool LanguageChanged
		{
			get
			{
				bool isLanguageChanged;
				lock (this._rsDataChangedSynchObject)
				{
					isLanguageChanged = this._isLanguageChanged;
				}
				return isLanguageChanged;
			}
			set
			{
				lock (this._rsDataChangedSynchObject)
				{
					this._isLanguageChanged = value;
				}
			}
		}

		private Dictionary<int, object> ParameterChangedPortia
		{
			get
			{
				Dictionary<int, object> parameterChangedPortia;
				lock (this._rsDataChangedSynchObject)
				{
					parameterChangedPortia = this._parameterChangedPortia;
				}
				return parameterChangedPortia;
			}
			set
			{
				lock (this._rsDataChangedSynchObject)
				{
					this._parameterChangedPortia = value;
				}
			}
		}

		private Dictionary<int, object> ParameterChangedVenus
		{
			get
			{
				Dictionary<int, object> parameterChangedVenus;
				lock (this._rsDataChangedSynchObject)
				{
					parameterChangedVenus = this._parameterChangedVenus;
				}
				return parameterChangedVenus;
			}
			set
			{
				lock (this._rsDataChangedSynchObject)
				{
					this._parameterChangedVenus = value;
				}
			}
		}

		private Dictionary<int, object> ParameterChangedJupiter
		{
			get
			{
				Dictionary<int, object> parameterChangedJupiter;
				lock (this._rsDataChangedSynchObject)
				{
					parameterChangedJupiter = this._parameterChangedJupiter;
				}
				return parameterChangedJupiter;
			}
			set
			{
				lock (this._rsDataChangedSynchObject)
				{
					this._parameterChangedJupiter = value;
				}
			}
		}

		public bool IsCSDataRequireUpdate
		{
			get
			{
				bool isCsDataRequireUpdate;
				lock (this._csDataChangedSynchObject)
				{
					isCsDataRequireUpdate = this._isCsDataRequireUpdate;
				}
				return isCsDataRequireUpdate;
			}
			set
			{
				lock (this._csDataChangedSynchObject)
				{
					this._isCsDataRequireUpdate = value;
				}
			}
		}

		private bool RTCChanged
		{
			get
			{
				bool isRTCChanged;
				lock (this._toolsMiscDataChangedSynchObject)
				{
					isRTCChanged = this._isRTCChanged;
				}
				return isRTCChanged;
			}
			set
			{
				lock (this._toolsMiscDataChangedSynchObject)
				{
					this._isRTCChanged = value;
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle6 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle7 = new DataGridViewCellStyle();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(MainForm));
			DataGridViewCellStyle dataGridViewCellStyle8 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle9 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle10 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle11 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle12 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle13 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle14 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle15 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle16 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle17 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle18 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle19 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle20 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle21 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle22 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle23 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle24 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle25 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle26 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle27 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle28 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle29 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle30 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle31 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle32 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle33 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle34 = new DataGridViewCellStyle();
			this.dgvInverterList = new SEDataGridView();
			this.colID = new DataGridViewTextBoxColumn();
			this.colHexID = new DataGridViewTextBoxColumn();
			this.pnlLineH = new SEPanel();
			this.pbLogo = new SEPictureBox();
			this.pnlInvListBackground = new SEPanel();
			this.pnlInverterList = new SEPanel();
			this.lblInvListHeader = new SELabel();
			this.tbcMain = new SETabControl();
			this.tpStatus = new SETabPage();
			this.pnlTSRS = new SEPanel();
			this.lblTSCSInterInverterConfigValue = new SELabel();
			this.lblTSCSServerConfigValue = new SELabel();
			this.lblTSRSCountryValue = new SELabel();
			this.lblTSRSLanguageValue = new SELabel();
			this.lblTSRSCountry = new SELabel();
			this.lblTSRSLanguage = new SELabel();
			this.lblTSSETHeader = new SELabel();
			this.lblTSCSServerConfig = new SELabel();
			this.lblTSCSInterInverterConfig = new SELabel();
			this.pnlTSSS = new SEPanel();
			this.lblTSSSDSP2VersionValue = new SELabel();
			this.lblTSSSDSP1VersionValue = new SELabel();
			this.lblTSSSCPUVersionValue = new SELabel();
			this.lblTSSSInverterIDValue = new SELabel();
			this.lblTSSSInverterModelValue = new SELabel();
			this.lblTSSSDSP2Version = new SELabel();
			this.lblTSSSDSP1Version = new SELabel();
			this.lblTSSSCPUVersion = new SELabel();
			this.lblTSSSInverterID = new SELabel();
			this.lblTSSSInverterModel = new SELabel();
			this.lblTSSSHeader = new SELabel();
			this.pnlInverterStat = new SEPanel();
			this.chkTSInverterPanelShouldUpdate = new SECheckBox();
			this.lblTSLCD = new SELabel();
			this.lcdInverter = new SELCD();
			this.pbLCDButton = new SEPictureBox();
			this.lblInvStatSEURLHeader = new SELabel();
			this.lblInvStatFaultHeader = new SELabel();
			this.lblInvStatModuleCommHeader = new SELabel();
			this.lblInvStatPowerProductionHeader = new SELabel();
			this.tpSettings = new SETabPage();
			this.tbcSettings = new SETabControl();
			this.tpCommSettings = new TabPage();
			this.lblSettingsLANStatusCable = new SELabel();
			this.lblSettingsLANStatusTCP = new SELabel();
			this.pnlSettingsRS232 = new SEPanel();
			this.lblSettingsRS232CommTypeValue = new SELabel();
			this.lblSettingsRS232Header = new SELabel();
			this.lblSettingsRS232CommType = new SELabel();
			this.pbSettingsRS485 = new SEPictureBox();
			this.pnlSettingsRS485 = new SEPanel();
			this.lblSettingsRS485Value = new SELabel();
			this.lblSettingsRS485Header = new SELabel();
			this.pbSettingsRS232 = new SEPictureBox();
			this.pnlSettingsIIC = new SEPanel();
			this.lblSettingsIICCommTypeValue = new SELabel();
			this.lblSettingsIICHeader = new SELabel();
			this.lblSettingsIICCommType = new SELabel();
			this.btnSettingsIICDetectSlaves = new SEButton();
			this.pnlSettingsServer = new SEPanel();
			this.lblSettingsServerViaValue = new SELabel();
			this.lblSettingsServerPortValue = new SELabel();
			this.lblSettingsServerAddressValue = new SELabel();
			this.lblSettingsServerHeader = new SELabel();
			this.lblSettingsServerAddress = new SELabel();
			this.lblSettingsServerPort = new SELabel();
			this.lblSettingsServerVia = new SELabel();
			this.btnSettingsServerPingTest = new SEButton();
			this.pnlSettingsLAN = new SEPanel();
			this.lblSettingsLANGatewayValue = new SELabel();
			this.lblSettingsLANSubnetMaskValue = new SELabel();
			this.lblSettingsLANCurrentIPValue = new SELabel();
			this.lblSettingsLANDHCPStatusValue = new SELabel();
			this.lblSettingsLANHeader = new SELabel();
			this.lblSettingsLANDHCPStatus = new SELabel();
			this.lblSettingsLANCurrentIP = new SELabel();
			this.lblSettingsLANGateway = new SELabel();
			this.lblSettingsLANSubnetMask = new SELabel();
			this.pnlSettingsZB = new SEPanel();
			this.lblSettingsZBValue = new SELabel();
			this.lblSettingsZBHeader = new SELabel();
			this.pnlSettingsIICLine2 = new SEPanel();
			this.pbSettingsLAN = new SEPictureBox();
			this.pbSettingsZB = new SEPictureBox();
			this.pbSettingsIIC = new SEPictureBox();
			this.pbSettingsServerCloud = new SEPictureBox();
			this.pbSettingsInverter = new SEPictureBox();
			this.pnlSettingsRS232Line1 = new SEPanel();
			this.pnlSettingsZBLine1 = new SEPanel();
			this.pnlSettingsLANLineTCPStatus = new SEPanel();
			this.pnlSettingsRS232Line2 = new SEPanel();
			this.pnlSettingsLANLineEthernetCableStatus2 = new SEPanel();
			this.pnlSettingsServerLine1 = new SEPanel();
			this.pnlSettingsServerLine2 = new SEPanel();
			this.pnlSettingsIICLine1 = new SEPanel();
			this.pnlSettingsIICLine3 = new SEPanel();
			this.pnlSettingsLANLineEthernetCableStatus1 = new SEPanel();
			this.pnlSettingsRS485Line1 = new SEPanel();
			this.tpRegionalSettings = new SETabPage();
			this.pnlRSDSPParams = new SEPanel();
			this.dgvVenusParams = new SEDataGridView();
			this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new DataGridViewTextBoxColumn();
			this.lblRSIPVenusParam = new SELabel();
			this.pnlRSCPUParams = new SEPanel();
			this.dgvPortiaParams = new SEDataGridView();
			this.colIndex = new DataGridViewTextBoxColumn();
			this.colName = new DataGridViewTextBoxColumn();
			this.colValue = new DataGridViewTextBoxColumn();
			this.colUnit = new DataGridViewTextBoxColumn();
			this.lblRSIPPortiaParam = new SELabel();
			this.pnlRSRSTop = new SEPanel();
			this.cmbSettingsRSRSCountry = new SEComboBox();
			this.cmbSettingsRSRSLanguage = new SEComboBox();
			this.lblSettingsRSRSCountry = new SELabel();
			this.lblSettingsRSRSLanguage = new SELabel();
			this.lblSettingsRSRSHeader = new SELabel();
			this.btnSettingsRSApply = new SEButton();
			this.tpPBData = new SETabPage();
			this.lblPBLastTelemTimeValue = new SELabel();
			this.lblPBInverterWarning = new SELabel();
			this.pnlPBBtns = new SEPanel();
			this.lblPBBtnsHeader = new SELabel();
			this.grbPBTelemetry = new SEGroupBox();
			this.btnPBForceTelem = new SEButton();
			this.chkPBTelemetryNormal = new SECheckBox();
			this.chkPBTelemetryFast = new SECheckBox();
			this.grbPBRecords = new SEGroupBox();
			this.btnPBDataLoad = new SEButton();
			this.btnPBRecordsExport = new SEButton();
			this.grbPBData = new SEGroupBox();
			this.btnPBDataVersionUpdate = new SEButton();
			this.btnPBDataAdd = new SEButton();
			this.btnPBDataRemove = new SEButton();
			this.lblPBLastTelemTime = new SELabel();
			this.pnlPBLPTop = new SEPanel();
			this.dgvPBParam = new SEDataGridView();
			this.dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
			this.colSerial = new DataGridViewTextBoxColumn();
			this.colVin = new DataGridViewTextBoxColumn();
			this.colVOut = new DataGridViewTextBoxColumn();
			this.colIIn = new DataGridViewTextBoxColumn();
			this.colEnergy = new DataGridViewTextBoxColumn();
			this.colTime = new DataGridViewTextBoxColumn();
			this.colVersion = new DataGridViewTextBoxColumn();
			this.lblPBParamHeader = new SELabel();
			this.tpTools = new SETabPage();
			this.tbcTools = new SETabControl();
			this.tpFirmwareUpgrade = new SETabPage();
			this.pnlTUDetails = new SEPanel();
			this.txtTUDetailsStatus = new SETextBox();
			this.lblTUDetailsUpgradeStatusHeader = new SELabel();
			this.lblTUDetailsDSP2Value = new SELabel();
			this.lblTUDetailsDSP2 = new SELabel();
			this.lblTUDetailsDSP1Value = new SELabel();
			this.lblTUDetailsCPUValue = new SELabel();
			this.lblTUDetailsDSP1 = new SELabel();
			this.lblTUDetailsCPU = new SELabel();
			this.lblTUDetailsHeader = new SELabel();
			this.pnlTUUpdateControl = new SEPanel();
			this.pbUpgradeProgress = new SEProgressBarBasic();
			this.pbUpgradeProgressProgram = new SEProgressBarBasic();
			this.lblUpgradeProgressProgram = new SELabel();
			this.lblUpgradeProgressTotal = new SELabel();
			this.btnTUUpdateControlFile = new SEButton();
			this.btnTUUpdateControlBeginUpdate = new SEButton();
			this.txtTUUpdateControlFile = new SETextBox();
			this.lblTUUpdateControlFile = new SELabel();
			this.lblTUUpdateControlHeader = new SELabel();
			this.tpMisc = new TabPage();
			this.pnlTMPwrBalance = new SEPanel();
			this.cmbTMPwrBalanceStatus = new SEComboBox();
			this.btnTMPwrBalanceStatus = new SEButton();
			this.lblTMPwrBalanceStatus = new SELabel();
			this.lblTMPwrBalanceHeader = new SELabel();
			this.pnlTMRTC = new SEPanel();
			this.lblTMRTCGMTCurrentValue = new SELabel();
			this.lblTMRTCGMTCurrent = new SELabel();
			this.lblTMRTCGMTOffsetValue = new SELabel();
			this.btnTMRTCSet = new SEButton();
			this.lblTMRTCGMTOffset = new SELabel();
			this.lblTMRTCHeader = new SELabel();
			this.pnlLineV = new SEPanel();
			this.pnlMenuLineRight = new SEPanel();
			this.btnConnect = new SEButton();
			this.btnRefresh = new SEButton();
			this.btnSupport = new SEButton();
			this.btnExit = new SEButton();
			this.btnOptions = new SEButton();
			this.lblConnectionStatus = new SELabel();
			this.pnlMenuLineLeft = new SEPanel();
			this.lblConnectionStatusHeader = new SELabel();
			((ISupportInitialize)this.dgvInverterList).BeginInit();
			((ISupportInitialize)this.pbLogo).BeginInit();
			this.pnlInvListBackground.SuspendLayout();
			this.pnlInverterList.SuspendLayout();
			this.tbcMain.SuspendLayout();
			this.tpStatus.SuspendLayout();
			this.pnlTSRS.SuspendLayout();
			this.pnlTSSS.SuspendLayout();
			this.pnlInverterStat.SuspendLayout();
			((ISupportInitialize)this.lcdInverter).BeginInit();
			((ISupportInitialize)this.pbLCDButton).BeginInit();
			this.tpSettings.SuspendLayout();
			this.tbcSettings.SuspendLayout();
			this.tpCommSettings.SuspendLayout();
			this.pnlSettingsRS232.SuspendLayout();
			((ISupportInitialize)this.pbSettingsRS485).BeginInit();
			this.pnlSettingsRS485.SuspendLayout();
			((ISupportInitialize)this.pbSettingsRS232).BeginInit();
			this.pnlSettingsIIC.SuspendLayout();
			this.pnlSettingsServer.SuspendLayout();
			this.pnlSettingsLAN.SuspendLayout();
			this.pnlSettingsZB.SuspendLayout();
			((ISupportInitialize)this.pbSettingsLAN).BeginInit();
			((ISupportInitialize)this.pbSettingsZB).BeginInit();
			((ISupportInitialize)this.pbSettingsIIC).BeginInit();
			((ISupportInitialize)this.pbSettingsServerCloud).BeginInit();
			((ISupportInitialize)this.pbSettingsInverter).BeginInit();
			this.tpRegionalSettings.SuspendLayout();
			this.pnlRSDSPParams.SuspendLayout();
			((ISupportInitialize)this.dgvVenusParams).BeginInit();
			this.pnlRSCPUParams.SuspendLayout();
			((ISupportInitialize)this.dgvPortiaParams).BeginInit();
			this.pnlRSRSTop.SuspendLayout();
			this.tpPBData.SuspendLayout();
			this.pnlPBBtns.SuspendLayout();
			this.grbPBTelemetry.SuspendLayout();
			this.grbPBRecords.SuspendLayout();
			this.grbPBData.SuspendLayout();
			this.pnlPBLPTop.SuspendLayout();
			((ISupportInitialize)this.dgvPBParam).BeginInit();
			this.tpTools.SuspendLayout();
			this.tbcTools.SuspendLayout();
			this.tpFirmwareUpgrade.SuspendLayout();
			this.pnlTUDetails.SuspendLayout();
			this.pnlTUUpdateControl.SuspendLayout();
			this.tpMisc.SuspendLayout();
			this.pnlTMPwrBalance.SuspendLayout();
			this.pnlTMRTC.SuspendLayout();
			base.SuspendLayout();
			this.dgvInverterList.AllowUserToAddRows = false;
			this.dgvInverterList.AllowUserToDeleteRows = false;
			this.dgvInverterList.AllowUserToResizeColumns = false;
			this.dgvInverterList.AllowUserToResizeRows = false;
			dataGridViewCellStyle.BackColor = Color.FromArgb(224, 224, 224);
			dataGridViewCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = Color.Black;
			dataGridViewCellStyle.Padding = new Padding(0, 4, 0, 4);
			dataGridViewCellStyle.SelectionBackColor = Color.FromArgb(64, 64, 64);
			dataGridViewCellStyle.SelectionForeColor = Color.White;
			this.dgvInverterList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle;
			this.dgvInverterList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
			this.dgvInverterList.BackgroundColor = Color.Silver;
			this.dgvInverterList.BorderStyle = BorderStyle.Fixed3D;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = Color.FromArgb(224, 224, 224);
			dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = Color.Black;
			dataGridViewCellStyle2.Padding = new Padding(0, 4, 0, 4);
			dataGridViewCellStyle2.SelectionBackColor = Color.FromArgb(64, 64, 64);
			dataGridViewCellStyle2.SelectionForeColor = Color.White;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
			this.dgvInverterList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
			this.dgvInverterList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvInverterList.ColumnHeadersVisible = false;
			this.dgvInverterList.Columns.AddRange(new DataGridViewColumn[]
			{
				this.colID,
				this.colHexID
			});
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = Color.FromArgb(224, 224, 224);
			dataGridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle3.ForeColor = Color.Black;
			dataGridViewCellStyle3.Padding = new Padding(0, 4, 0, 4);
			dataGridViewCellStyle3.SelectionBackColor = Color.FromArgb(64, 64, 64);
			dataGridViewCellStyle3.SelectionForeColor = Color.White;
			dataGridViewCellStyle3.WrapMode = DataGridViewTriState.False;
			this.dgvInverterList.DefaultCellStyle = dataGridViewCellStyle3;
			this.dgvInverterList.Location = new Point(12, 43);
			this.dgvInverterList.Name = "dgvInverterList";
			this.dgvInverterList.ReadOnly = true;
			dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = Color.FromArgb(224, 224, 224);
			dataGridViewCellStyle4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle4.ForeColor = Color.Black;
			dataGridViewCellStyle4.Padding = new Padding(0, 4, 0, 4);
			dataGridViewCellStyle4.SelectionBackColor = Color.FromArgb(64, 64, 64);
			dataGridViewCellStyle4.SelectionForeColor = Color.White;
			dataGridViewCellStyle4.WrapMode = DataGridViewTriState.True;
			this.dgvInverterList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this.dgvInverterList.RowHeadersVisible = false;
			this.dgvInverterList.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
			dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle5.BackColor = Color.FromArgb(224, 224, 224);
			dataGridViewCellStyle5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle5.ForeColor = Color.Black;
			dataGridViewCellStyle5.Padding = new Padding(0, 4, 0, 4);
			dataGridViewCellStyle5.SelectionBackColor = Color.FromArgb(64, 64, 64);
			dataGridViewCellStyle5.SelectionForeColor = Color.White;
			dataGridViewCellStyle5.WrapMode = DataGridViewTriState.True;
			this.dgvInverterList.RowsDefaultCellStyle = dataGridViewCellStyle5;
			this.dgvInverterList.RowTemplate.DefaultCellStyle.BackColor = Color.FromArgb(224, 224, 224);
			this.dgvInverterList.RowTemplate.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.dgvInverterList.RowTemplate.DefaultCellStyle.ForeColor = Color.Black;
			this.dgvInverterList.RowTemplate.DefaultCellStyle.Padding = new Padding(0, 4, 0, 4);
			this.dgvInverterList.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.FromArgb(64, 64, 64);
			this.dgvInverterList.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.White;
			this.dgvInverterList.ScrollBars = ScrollBars.Vertical;
			this.dgvInverterList.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgvInverterList.Size = new Size(159, 339);
			this.dgvInverterList.TabIndex = 0;
			this.dgvInverterList.Tag = "";
			this.dgvInverterList.CellClick += new DataGridViewCellEventHandler(this.dgvInverterList_CellClick);
			dataGridViewCellStyle6.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle6.BackColor = Color.White;
			dataGridViewCellStyle6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle6.ForeColor = Color.Black;
			dataGridViewCellStyle6.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle6.SelectionForeColor = Color.White;
			dataGridViewCellStyle6.WrapMode = DataGridViewTriState.True;
			this.colID.DefaultCellStyle = dataGridViewCellStyle6;
			this.colID.HeaderText = "ID";
			this.colID.Name = "colID";
			this.colID.ReadOnly = true;
			this.colID.Resizable = DataGridViewTriState.False;
			this.colID.Visible = false;
			this.colID.Width = 5;
			dataGridViewCellStyle7.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle7.BackColor = Color.White;
			dataGridViewCellStyle7.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle7.ForeColor = Color.Black;
			dataGridViewCellStyle7.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle7.SelectionForeColor = Color.White;
			dataGridViewCellStyle7.WrapMode = DataGridViewTriState.True;
			this.colHexID.DefaultCellStyle = dataGridViewCellStyle7;
			this.colHexID.HeaderText = "HexID";
			this.colHexID.Name = "colHexID";
			this.colHexID.ReadOnly = true;
			this.colHexID.Resizable = DataGridViewTriState.False;
			this.colHexID.Width = 158;
			this.pnlLineH.BackColor = SystemColors.Control;
			this.pnlLineH.Location = new Point(-9, 59);
			this.pnlLineH.Name = "pnlLineH";
			this.pnlLineH.Size = new Size(1011, 30);
			this.pnlLineH.TabIndex = 18;
			this.pbLogo.BackColor = Color.Transparent;
			this.pbLogo.BackgroundImage = Resources.SolarEdgeLogo;
			this.pbLogo.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbLogo.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbLogo.ImageListBack");
			this.pbLogo.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbLogo.ImageListFore");
			this.pbLogo.Location = new Point(20, 12);
			this.pbLogo.Name = "pbLogo";
			this.pbLogo.SelectedImageBack = -1;
			this.pbLogo.SelectedImageFore = -1;
			this.pbLogo.Size = new Size(159, 36);
			this.pbLogo.TabIndex = 19;
			this.pbLogo.TabStop = false;
			this.pnlInvListBackground.BackColor = Color.Transparent;
			this.pnlInvListBackground.BackgroundImage = Resources.BackGrad;
			this.pnlInvListBackground.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlInvListBackground.Controls.Add(this.pnlInverterList);
			this.pnlInvListBackground.Location = new Point(-2, 84);
			this.pnlInvListBackground.Name = "pnlInvListBackground";
			this.pnlInvListBackground.Size = new Size(214, 425);
			this.pnlInvListBackground.TabIndex = 20;
			this.pnlInverterList.BackgroundImage = (Image)componentResourceManager.GetObject("pnlInverterList.BackgroundImage");
			this.pnlInverterList.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlInverterList.Controls.Add(this.dgvInverterList);
			this.pnlInverterList.Controls.Add(this.lblInvListHeader);
			this.pnlInverterList.Location = new Point(12, 10);
			this.pnlInverterList.Name = "pnlInverterList";
			this.pnlInverterList.Size = new Size(183, 393);
			this.pnlInverterList.TabIndex = 0;
			this.lblInvListHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblInvListHeader.ForeColor = Color.Black;
			this.lblInvListHeader.Location = new Point(10, 4);
			this.lblInvListHeader.Name = "lblInvListHeader";
			this.lblInvListHeader.Size = new Size(157, 29);
			this.lblInvListHeader.TabIndex = 4;
			this.lblInvListHeader.Text = "Inverter List";
			this.lblInvListHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.tbcMain.Appearance = TabAppearance.Buttons;
			this.tbcMain.Controls.Add(this.tpStatus);
			this.tbcMain.Controls.Add(this.tpSettings);
			this.tbcMain.Controls.Add(this.tpPBData);
			this.tbcMain.Controls.Add(this.tpTools);
			this.tbcMain.Cursor = Cursors.Hand;
			this.tbcMain.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.tbcMain.ItemSize = new Size(187, 19);
			this.tbcMain.Location = new Point(229, 64);
			this.tbcMain.Name = "tbcMain";
			this.tbcMain.SelectedIndex = 0;
			this.tbcMain.Size = new Size(773, 442);
			this.tbcMain.SizeMode = TabSizeMode.Fixed;
			this.tbcMain.TabIndex = 1;
			this.tbcMain.SelectedIndexChanged += new EventHandler(this.tbcMain_SelectedIndexChanged);
			this.tpStatus.BackColor = Color.Transparent;
			this.tpStatus.BackgroundImage = Resources.BackGrad;
			this.tpStatus.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpStatus.Controls.Add(this.pnlTSRS);
			this.tpStatus.Controls.Add(this.pnlTSSS);
			this.tpStatus.Controls.Add(this.pnlInverterStat);
			this.tpStatus.Cursor = Cursors.Arrow;
			this.tpStatus.Location = new Point(4, 23);
			this.tpStatus.Name = "tpStatus";
			this.tpStatus.Padding = new Padding(3);
			this.tpStatus.Size = new Size(765, 415);
			this.tpStatus.TabIndex = 1;
			this.tpStatus.Text = "Status";
			this.pnlTSRS.BackgroundImage = (Image)componentResourceManager.GetObject("pnlTSRS.BackgroundImage");
			this.pnlTSRS.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlTSRS.Controls.Add(this.lblTSCSInterInverterConfigValue);
			this.pnlTSRS.Controls.Add(this.lblTSCSServerConfigValue);
			this.pnlTSRS.Controls.Add(this.lblTSRSCountryValue);
			this.pnlTSRS.Controls.Add(this.lblTSRSLanguageValue);
			this.pnlTSRS.Controls.Add(this.lblTSRSCountry);
			this.pnlTSRS.Controls.Add(this.lblTSRSLanguage);
			this.pnlTSRS.Controls.Add(this.lblTSSETHeader);
			this.pnlTSRS.Controls.Add(this.lblTSCSServerConfig);
			this.pnlTSRS.Controls.Add(this.lblTSCSInterInverterConfig);
			this.pnlTSRS.Location = new Point(368, 5);
			this.pnlTSRS.Name = "pnlTSRS";
			this.pnlTSRS.Size = new Size(379, 209);
			this.pnlTSRS.TabIndex = 0;
			this.lblTSCSInterInverterConfigValue.BackColor = Color.White;
			this.lblTSCSInterInverterConfigValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSCSInterInverterConfigValue.ForeColor = Color.Black;
			this.lblTSCSInterInverterConfigValue.Location = new Point(152, 139);
			this.lblTSCSInterInverterConfigValue.Name = "lblTSCSInterInverterConfigValue";
			this.lblTSCSInterInverterConfigValue.Size = new Size(207, 22);
			this.lblTSCSInterInverterConfigValue.TabIndex = 22;
			this.lblTSCSInterInverterConfigValue.Tag = "";
			this.lblTSCSInterInverterConfigValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSCSServerConfigValue.BackColor = Color.White;
			this.lblTSCSServerConfigValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSCSServerConfigValue.ForeColor = Color.Black;
			this.lblTSCSServerConfigValue.Location = new Point(152, 107);
			this.lblTSCSServerConfigValue.Name = "lblTSCSServerConfigValue";
			this.lblTSCSServerConfigValue.Size = new Size(207, 22);
			this.lblTSCSServerConfigValue.TabIndex = 21;
			this.lblTSCSServerConfigValue.Tag = "";
			this.lblTSCSServerConfigValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSRSCountryValue.BackColor = Color.White;
			this.lblTSRSCountryValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSRSCountryValue.ForeColor = Color.Black;
			this.lblTSRSCountryValue.Location = new Point(152, 74);
			this.lblTSRSCountryValue.Name = "lblTSRSCountryValue";
			this.lblTSRSCountryValue.Size = new Size(207, 22);
			this.lblTSRSCountryValue.TabIndex = 20;
			this.lblTSRSCountryValue.Tag = "";
			this.lblTSRSCountryValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSRSLanguageValue.BackColor = Color.White;
			this.lblTSRSLanguageValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSRSLanguageValue.ForeColor = Color.Black;
			this.lblTSRSLanguageValue.Location = new Point(152, 42);
			this.lblTSRSLanguageValue.Name = "lblTSRSLanguageValue";
			this.lblTSRSLanguageValue.Size = new Size(207, 22);
			this.lblTSRSLanguageValue.TabIndex = 19;
			this.lblTSRSLanguageValue.Tag = "";
			this.lblTSRSLanguageValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSRSCountry.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSRSCountry.ForeColor = Color.Black;
			this.lblTSRSCountry.Location = new Point(11, 71);
			this.lblTSRSCountry.Name = "lblTSRSCountry";
			this.lblTSRSCountry.Size = new Size(132, 29);
			this.lblTSRSCountry.TabIndex = 13;
			this.lblTSRSCountry.Tag = "";
			this.lblTSRSCountry.Text = "Country:";
			this.lblTSRSCountry.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSRSLanguage.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSRSLanguage.ForeColor = Color.Black;
			this.lblTSRSLanguage.Location = new Point(11, 37);
			this.lblTSRSLanguage.Name = "lblTSRSLanguage";
			this.lblTSRSLanguage.Size = new Size(132, 29);
			this.lblTSRSLanguage.TabIndex = 12;
			this.lblTSRSLanguage.Tag = "";
			this.lblTSRSLanguage.Text = "Language:";
			this.lblTSRSLanguage.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSSETHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSETHeader.ForeColor = Color.Black;
			this.lblTSSETHeader.Location = new Point(25, 3);
			this.lblTSSETHeader.Name = "lblTSSETHeader";
			this.lblTSSETHeader.Size = new Size(334, 29);
			this.lblTSSETHeader.TabIndex = 5;
			this.lblTSSETHeader.Text = "Settings";
			this.lblTSSETHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSCSServerConfig.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSCSServerConfig.ForeColor = Color.Black;
			this.lblTSCSServerConfig.Location = new Point(14, 104);
			this.lblTSCSServerConfig.Name = "lblTSCSServerConfig";
			this.lblTSCSServerConfig.Size = new Size(134, 29);
			this.lblTSCSServerConfig.TabIndex = 12;
			this.lblTSCSServerConfig.Tag = "";
			this.lblTSCSServerConfig.Text = "Server Configuration:";
			this.lblTSCSServerConfig.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSCSInterInverterConfig.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSCSInterInverterConfig.ForeColor = Color.Black;
			this.lblTSCSInterInverterConfig.Location = new Point(14, 135);
			this.lblTSCSInterInverterConfig.Name = "lblTSCSInterInverterConfig";
			this.lblTSCSInterInverterConfig.Size = new Size(134, 29);
			this.lblTSCSInterInverterConfig.TabIndex = 13;
			this.lblTSCSInterInverterConfig.Tag = "";
			this.lblTSCSInterInverterConfig.Text = "Inter-Inverter Config.:";
			this.lblTSCSInterInverterConfig.TextAlign = ContentAlignment.MiddleRight;
			this.pnlTSSS.BackgroundImage = (Image)componentResourceManager.GetObject("pnlTSSS.BackgroundImage");
			this.pnlTSSS.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlTSSS.Controls.Add(this.lblTSSSDSP2VersionValue);
			this.pnlTSSS.Controls.Add(this.lblTSSSDSP1VersionValue);
			this.pnlTSSS.Controls.Add(this.lblTSSSCPUVersionValue);
			this.pnlTSSS.Controls.Add(this.lblTSSSInverterIDValue);
			this.pnlTSSS.Controls.Add(this.lblTSSSInverterModelValue);
			this.pnlTSSS.Controls.Add(this.lblTSSSDSP2Version);
			this.pnlTSSS.Controls.Add(this.lblTSSSDSP1Version);
			this.pnlTSSS.Controls.Add(this.lblTSSSCPUVersion);
			this.pnlTSSS.Controls.Add(this.lblTSSSInverterID);
			this.pnlTSSS.Controls.Add(this.lblTSSSInverterModel);
			this.pnlTSSS.Controls.Add(this.lblTSSSHeader);
			this.pnlTSSS.Location = new Point(11, 5);
			this.pnlTSSS.Name = "pnlTSSS";
			this.pnlTSSS.Size = new Size(344, 209);
			this.pnlTSSS.TabIndex = 0;
			this.lblTSSSDSP2VersionValue.BackColor = Color.White;
			this.lblTSSSDSP2VersionValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSDSP2VersionValue.ForeColor = Color.Black;
			this.lblTSSSDSP2VersionValue.Location = new Point(124, 169);
			this.lblTSSSDSP2VersionValue.Name = "lblTSSSDSP2VersionValue";
			this.lblTSSSDSP2VersionValue.Size = new Size(197, 22);
			this.lblTSSSDSP2VersionValue.TabIndex = 19;
			this.lblTSSSDSP2VersionValue.Tag = "";
			this.lblTSSSDSP2VersionValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSSSDSP1VersionValue.BackColor = Color.White;
			this.lblTSSSDSP1VersionValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSDSP1VersionValue.ForeColor = Color.Black;
			this.lblTSSSDSP1VersionValue.Location = new Point(124, 139);
			this.lblTSSSDSP1VersionValue.Name = "lblTSSSDSP1VersionValue";
			this.lblTSSSDSP1VersionValue.Size = new Size(197, 22);
			this.lblTSSSDSP1VersionValue.TabIndex = 18;
			this.lblTSSSDSP1VersionValue.Tag = "";
			this.lblTSSSDSP1VersionValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSSSCPUVersionValue.BackColor = Color.White;
			this.lblTSSSCPUVersionValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSCPUVersionValue.ForeColor = Color.Black;
			this.lblTSSSCPUVersionValue.Location = new Point(124, 107);
			this.lblTSSSCPUVersionValue.Name = "lblTSSSCPUVersionValue";
			this.lblTSSSCPUVersionValue.Size = new Size(197, 22);
			this.lblTSSSCPUVersionValue.TabIndex = 17;
			this.lblTSSSCPUVersionValue.Tag = "";
			this.lblTSSSCPUVersionValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSSSInverterIDValue.BackColor = Color.White;
			this.lblTSSSInverterIDValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSInverterIDValue.ForeColor = Color.Black;
			this.lblTSSSInverterIDValue.Location = new Point(124, 74);
			this.lblTSSSInverterIDValue.Name = "lblTSSSInverterIDValue";
			this.lblTSSSInverterIDValue.Size = new Size(197, 22);
			this.lblTSSSInverterIDValue.TabIndex = 16;
			this.lblTSSSInverterIDValue.Tag = "";
			this.lblTSSSInverterIDValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSSSInverterModelValue.BackColor = Color.White;
			this.lblTSSSInverterModelValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSInverterModelValue.ForeColor = Color.Black;
			this.lblTSSSInverterModelValue.Location = new Point(124, 42);
			this.lblTSSSInverterModelValue.Name = "lblTSSSInverterModelValue";
			this.lblTSSSInverterModelValue.Size = new Size(197, 22);
			this.lblTSSSInverterModelValue.TabIndex = 15;
			this.lblTSSSInverterModelValue.Tag = "";
			this.lblTSSSInverterModelValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTSSSDSP2Version.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSDSP2Version.ForeColor = Color.Black;
			this.lblTSSSDSP2Version.Location = new Point(25, 166);
			this.lblTSSSDSP2Version.Name = "lblTSSSDSP2Version";
			this.lblTSSSDSP2Version.Size = new Size(93, 29);
			this.lblTSSSDSP2Version.TabIndex = 9;
			this.lblTSSSDSP2Version.Tag = "";
			this.lblTSSSDSP2Version.Text = "DSP2 Version:";
			this.lblTSSSDSP2Version.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSSSDSP1Version.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSDSP1Version.ForeColor = Color.Black;
			this.lblTSSSDSP1Version.Location = new Point(25, 135);
			this.lblTSSSDSP1Version.Name = "lblTSSSDSP1Version";
			this.lblTSSSDSP1Version.Size = new Size(93, 29);
			this.lblTSSSDSP1Version.TabIndex = 8;
			this.lblTSSSDSP1Version.Tag = "";
			this.lblTSSSDSP1Version.Text = "DSP1 Version:";
			this.lblTSSSDSP1Version.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSSSCPUVersion.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSCPUVersion.ForeColor = Color.Black;
			this.lblTSSSCPUVersion.Location = new Point(25, 102);
			this.lblTSSSCPUVersion.Name = "lblTSSSCPUVersion";
			this.lblTSSSCPUVersion.Size = new Size(93, 29);
			this.lblTSSSCPUVersion.TabIndex = 7;
			this.lblTSSSCPUVersion.Tag = "";
			this.lblTSSSCPUVersion.Text = "CPU Version:";
			this.lblTSSSCPUVersion.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSSSInverterID.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSInverterID.ForeColor = Color.Black;
			this.lblTSSSInverterID.Location = new Point(25, 70);
			this.lblTSSSInverterID.Name = "lblTSSSInverterID";
			this.lblTSSSInverterID.Size = new Size(93, 29);
			this.lblTSSSInverterID.TabIndex = 6;
			this.lblTSSSInverterID.Tag = "";
			this.lblTSSSInverterID.Text = "Inverter ID:";
			this.lblTSSSInverterID.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSSSInverterModel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSInverterModel.ForeColor = Color.Black;
			this.lblTSSSInverterModel.Location = new Point(25, 38);
			this.lblTSSSInverterModel.Name = "lblTSSSInverterModel";
			this.lblTSSSInverterModel.Size = new Size(93, 29);
			this.lblTSSSInverterModel.TabIndex = 5;
			this.lblTSSSInverterModel.Tag = "";
			this.lblTSSSInverterModel.Text = "Inverter Model:";
			this.lblTSSSInverterModel.TextAlign = ContentAlignment.MiddleRight;
			this.lblTSSSHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSSSHeader.ForeColor = Color.Black;
			this.lblTSSSHeader.Location = new Point(33, 2);
			this.lblTSSSHeader.Name = "lblTSSSHeader";
			this.lblTSSSHeader.Size = new Size(281, 29);
			this.lblTSSSHeader.TabIndex = 4;
			this.lblTSSSHeader.Text = "System Specifications";
			this.lblTSSSHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlInverterStat.BackgroundImage = Resources.InverterPanel3;
			this.pnlInverterStat.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlInverterStat.Controls.Add(this.chkTSInverterPanelShouldUpdate);
			this.pnlInverterStat.Controls.Add(this.lblTSLCD);
			this.pnlInverterStat.Controls.Add(this.lcdInverter);
			this.pnlInverterStat.Controls.Add(this.pbLCDButton);
			this.pnlInverterStat.Controls.Add(this.lblInvStatSEURLHeader);
			this.pnlInverterStat.Controls.Add(this.lblInvStatFaultHeader);
			this.pnlInverterStat.Controls.Add(this.lblInvStatModuleCommHeader);
			this.pnlInverterStat.Controls.Add(this.lblInvStatPowerProductionHeader);
			this.pnlInverterStat.Location = new Point(9, 222);
			this.pnlInverterStat.Name = "pnlInverterStat";
			this.pnlInverterStat.Size = new Size(741, 181);
			this.pnlInverterStat.TabIndex = 0;
			this.pnlInverterStat.Tag = "";
			this.chkTSInverterPanelShouldUpdate.Checked = true;
			this.chkTSInverterPanelShouldUpdate.CheckState = CheckState.Checked;
			this.chkTSInverterPanelShouldUpdate.Location = new Point(297, 152);
			this.chkTSInverterPanelShouldUpdate.Name = "chkTSInverterPanelShouldUpdate";
			this.chkTSInverterPanelShouldUpdate.Size = new Size(205, 22);
			this.chkTSInverterPanelShouldUpdate.TabIndex = 22;
			this.chkTSInverterPanelShouldUpdate.Text = "Update Panel";
			this.chkTSInverterPanelShouldUpdate.UseVisualStyleBackColor = true;
			this.lblTSLCD.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTSLCD.ForeColor = Color.Black;
			this.lblTSLCD.Location = new Point(118, 63);
			this.lblTSLCD.Name = "lblTSLCD";
			this.lblTSLCD.Size = new Size(40, 29);
			this.lblTSLCD.TabIndex = 21;
			this.lblTSLCD.Text = "LCD";
			this.lblTSLCD.TextAlign = ContentAlignment.MiddleCenter;
			this.lcdInverter.ImageListBack = (List<Image>)componentResourceManager.GetObject("lcdInverter.ImageListBack");
			this.lcdInverter.ImageListFore = (List<Image>)componentResourceManager.GetObject("lcdInverter.ImageListFore");
			this.lcdInverter.Lines = null;
			this.lcdInverter.Location = new Point(177, 41);
			this.lcdInverter.Name = "lcdInverter";
			this.lcdInverter.SelectedImageBack = -1;
			this.lcdInverter.SelectedImageFore = -1;
			this.lcdInverter.Size = new Size(320, 97);
			this.lcdInverter.TabIndex = 20;
			this.lcdInverter.TabStop = false;
			this.pbLCDButton.BackgroundImage = Resources.buttonOff;
			this.pbLCDButton.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbLCDButton.Cursor = Cursors.Hand;
			this.pbLCDButton.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbLCDButton.ImageListBack");
			this.pbLCDButton.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbLCDButton.ImageListFore");
			this.pbLCDButton.Location = new Point(52, 49);
			this.pbLCDButton.Name = "pbLCDButton";
			this.pbLCDButton.SelectedImageBack = -1;
			this.pbLCDButton.SelectedImageFore = -1;
			this.pbLCDButton.Size = new Size(66, 57);
			this.pbLCDButton.TabIndex = 19;
			this.pbLCDButton.TabStop = false;
			this.pbLCDButton.Click += new EventHandler(this.pbLCDButton_Click);
			this.pbLCDButton.MouseDown += new MouseEventHandler(this.pbLCDButton_MouseDown);
			this.pbLCDButton.MouseUp += new MouseEventHandler(this.pbLCDButton_MouseUp);
			this.lblInvStatSEURLHeader.Font = new Font("Arial Narrow", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.lblInvStatSEURLHeader.ForeColor = Color.White;
			this.lblInvStatSEURLHeader.Location = new Point(537, 136);
			this.lblInvStatSEURLHeader.Name = "lblInvStatSEURLHeader";
			this.lblInvStatSEURLHeader.Size = new Size(196, 39);
			this.lblInvStatSEURLHeader.TabIndex = 18;
			this.lblInvStatSEURLHeader.Text = "www.solaredge.com";
			this.lblInvStatSEURLHeader.TextAlign = ContentAlignment.MiddleRight;
			this.lblInvStatFaultHeader.Font = new Font("Arial Narrow", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.lblInvStatFaultHeader.ForeColor = Color.White;
			this.lblInvStatFaultHeader.Location = new Point(572, 104);
			this.lblInvStatFaultHeader.Name = "lblInvStatFaultHeader";
			this.lblInvStatFaultHeader.Size = new Size(172, 29);
			this.lblInvStatFaultHeader.TabIndex = 17;
			this.lblInvStatFaultHeader.Text = "Fault";
			this.lblInvStatFaultHeader.TextAlign = ContentAlignment.MiddleLeft;
			this.lblInvStatModuleCommHeader.Font = new Font("Arial Narrow", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.lblInvStatModuleCommHeader.ForeColor = Color.White;
			this.lblInvStatModuleCommHeader.Location = new Point(571, 75);
			this.lblInvStatModuleCommHeader.Name = "lblInvStatModuleCommHeader";
			this.lblInvStatModuleCommHeader.Size = new Size(172, 29);
			this.lblInvStatModuleCommHeader.TabIndex = 16;
			this.lblInvStatModuleCommHeader.Text = "Module Communication";
			this.lblInvStatModuleCommHeader.TextAlign = ContentAlignment.MiddleLeft;
			this.lblInvStatPowerProductionHeader.Font = new Font("Arial Narrow", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.lblInvStatPowerProductionHeader.ForeColor = Color.White;
			this.lblInvStatPowerProductionHeader.Location = new Point(571, 48);
			this.lblInvStatPowerProductionHeader.Name = "lblInvStatPowerProductionHeader";
			this.lblInvStatPowerProductionHeader.Size = new Size(172, 29);
			this.lblInvStatPowerProductionHeader.TabIndex = 15;
			this.lblInvStatPowerProductionHeader.Text = "Power Production";
			this.lblInvStatPowerProductionHeader.TextAlign = ContentAlignment.MiddleLeft;
			this.tpSettings.BackgroundImage = Resources.BackGrad;
			this.tpSettings.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpSettings.Controls.Add(this.tbcSettings);
			this.tpSettings.Location = new Point(4, 23);
			this.tpSettings.Name = "tpSettings";
			this.tpSettings.Padding = new Padding(3);
			this.tpSettings.Size = new Size(765, 415);
			this.tpSettings.TabIndex = 2;
			this.tpSettings.Text = "Settings";
			this.tpSettings.UseVisualStyleBackColor = true;
			this.tbcSettings.Appearance = TabAppearance.Buttons;
			this.tbcSettings.Controls.Add(this.tpCommSettings);
			this.tbcSettings.Controls.Add(this.tpRegionalSettings);
			this.tbcSettings.ItemSize = new Size(376, 21);
			this.tbcSettings.Location = new Point(-2, 0);
			this.tbcSettings.Name = "tbcSettings";
			this.tbcSettings.SelectedIndex = 0;
			this.tbcSettings.Size = new Size(769, 422);
			this.tbcSettings.SizeMode = TabSizeMode.Fixed;
			this.tbcSettings.TabIndex = 0;
			this.tbcSettings.SelectedIndexChanged += new EventHandler(this.tbcSettings_SelectedIndexChanged);
			this.tpCommSettings.BackColor = Color.FromArgb(224, 224, 224);
			this.tpCommSettings.BackgroundImage = Resources.BackGrad;
			this.tpCommSettings.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpCommSettings.Controls.Add(this.lblSettingsLANStatusCable);
			this.tpCommSettings.Controls.Add(this.lblSettingsLANStatusTCP);
			this.tpCommSettings.Controls.Add(this.pnlSettingsRS232);
			this.tpCommSettings.Controls.Add(this.pbSettingsRS485);
			this.tpCommSettings.Controls.Add(this.pnlSettingsRS485);
			this.tpCommSettings.Controls.Add(this.pbSettingsRS232);
			this.tpCommSettings.Controls.Add(this.pnlSettingsIIC);
			this.tpCommSettings.Controls.Add(this.pnlSettingsServer);
			this.tpCommSettings.Controls.Add(this.pnlSettingsLAN);
			this.tpCommSettings.Controls.Add(this.pnlSettingsZB);
			this.tpCommSettings.Controls.Add(this.pnlSettingsIICLine2);
			this.tpCommSettings.Controls.Add(this.pbSettingsLAN);
			this.tpCommSettings.Controls.Add(this.pbSettingsZB);
			this.tpCommSettings.Controls.Add(this.pbSettingsIIC);
			this.tpCommSettings.Controls.Add(this.pbSettingsServerCloud);
			this.tpCommSettings.Controls.Add(this.pbSettingsInverter);
			this.tpCommSettings.Controls.Add(this.pnlSettingsRS232Line1);
			this.tpCommSettings.Controls.Add(this.pnlSettingsZBLine1);
			this.tpCommSettings.Controls.Add(this.pnlSettingsLANLineTCPStatus);
			this.tpCommSettings.Controls.Add(this.pnlSettingsRS232Line2);
			this.tpCommSettings.Controls.Add(this.pnlSettingsLANLineEthernetCableStatus2);
			this.tpCommSettings.Controls.Add(this.pnlSettingsServerLine1);
			this.tpCommSettings.Controls.Add(this.pnlSettingsServerLine2);
			this.tpCommSettings.Controls.Add(this.pnlSettingsIICLine1);
			this.tpCommSettings.Controls.Add(this.pnlSettingsIICLine3);
			this.tpCommSettings.Controls.Add(this.pnlSettingsLANLineEthernetCableStatus1);
			this.tpCommSettings.Controls.Add(this.pnlSettingsRS485Line1);
			this.tpCommSettings.Cursor = Cursors.Arrow;
			this.tpCommSettings.Location = new Point(4, 25);
			this.tpCommSettings.Name = "tpCommSettings";
			this.tpCommSettings.Padding = new Padding(3);
			this.tpCommSettings.Size = new Size(761, 393);
			this.tpCommSettings.TabIndex = 2;
			this.tpCommSettings.Text = "Comm Settings";
			this.tpCommSettings.UseVisualStyleBackColor = true;
			this.lblSettingsLANStatusCable.BackColor = Color.Transparent;
			this.lblSettingsLANStatusCable.ForeColor = Color.White;
			this.lblSettingsLANStatusCable.Location = new Point(311, 332);
			this.lblSettingsLANStatusCable.Name = "lblSettingsLANStatusCable";
			this.lblSettingsLANStatusCable.Size = new Size(106, 22);
			this.lblSettingsLANStatusCable.TabIndex = 53;
			this.lblSettingsLANStatusCable.Tag = "";
			this.lblSettingsLANStatusCable.Text = "ETHERNET CABLE";
			this.lblSettingsLANStatusCable.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANStatusTCP.BackColor = Color.Transparent;
			this.lblSettingsLANStatusTCP.ForeColor = Color.White;
			this.lblSettingsLANStatusTCP.Location = new Point(108, 334);
			this.lblSettingsLANStatusTCP.Name = "lblSettingsLANStatusTCP";
			this.lblSettingsLANStatusTCP.Size = new Size(34, 22);
			this.lblSettingsLANStatusTCP.TabIndex = 52;
			this.lblSettingsLANStatusTCP.Tag = "";
			this.lblSettingsLANStatusTCP.Text = "TCP";
			this.lblSettingsLANStatusTCP.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlSettingsRS232.BackColor = Color.Silver;
			this.pnlSettingsRS232.BorderStyle = BorderStyle.Fixed3D;
			this.pnlSettingsRS232.Controls.Add(this.lblSettingsRS232CommTypeValue);
			this.pnlSettingsRS232.Controls.Add(this.lblSettingsRS232Header);
			this.pnlSettingsRS232.Controls.Add(this.lblSettingsRS232CommType);
			this.pnlSettingsRS232.Cursor = Cursors.Hand;
			this.pnlSettingsRS232.Location = new Point(142, 19);
			this.pnlSettingsRS232.Name = "pnlSettingsRS232";
			this.pnlSettingsRS232.Size = new Size(169, 52);
			this.pnlSettingsRS232.TabIndex = 17;
			this.pnlSettingsRS232.Click += new EventHandler(this.settingsRS232_Click);
			this.lblSettingsRS232CommTypeValue.BackColor = Color.White;
			this.lblSettingsRS232CommTypeValue.Cursor = Cursors.Hand;
			this.lblSettingsRS232CommTypeValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsRS232CommTypeValue.ForeColor = Color.Black;
			this.lblSettingsRS232CommTypeValue.Location = new Point(75, 22);
			this.lblSettingsRS232CommTypeValue.Name = "lblSettingsRS232CommTypeValue";
			this.lblSettingsRS232CommTypeValue.Size = new Size(83, 20);
			this.lblSettingsRS232CommTypeValue.TabIndex = 60;
			this.lblSettingsRS232CommTypeValue.Tag = "";
			this.lblSettingsRS232CommTypeValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsRS232CommTypeValue.Click += new EventHandler(this.settingsRS232_Click);
			this.lblSettingsRS232Header.Cursor = Cursors.Hand;
			this.lblSettingsRS232Header.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblSettingsRS232Header.Location = new Point(7, -1);
			this.lblSettingsRS232Header.Name = "lblSettingsRS232Header";
			this.lblSettingsRS232Header.Size = new Size(151, 22);
			this.lblSettingsRS232Header.TabIndex = 59;
			this.lblSettingsRS232Header.Tag = "";
			this.lblSettingsRS232Header.Text = "RS 232 Status";
			this.lblSettingsRS232Header.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsRS232Header.Click += new EventHandler(this.settingsRS232_Click);
			this.lblSettingsRS232CommType.Cursor = Cursors.Hand;
			this.lblSettingsRS232CommType.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsRS232CommType.ForeColor = Color.Black;
			this.lblSettingsRS232CommType.Location = new Point(4, 20);
			this.lblSettingsRS232CommType.Name = "lblSettingsRS232CommType";
			this.lblSettingsRS232CommType.Size = new Size(69, 23);
			this.lblSettingsRS232CommType.TabIndex = 16;
			this.lblSettingsRS232CommType.Tag = "";
			this.lblSettingsRS232CommType.Text = "Comm Type:";
			this.lblSettingsRS232CommType.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsRS232CommType.Click += new EventHandler(this.settingsRS232_Click);
			this.pbSettingsRS485.BackColor = Color.Transparent;
			this.pbSettingsRS485.BackgroundImage = Resources.RS485;
			this.pbSettingsRS485.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSettingsRS485.Cursor = Cursors.Hand;
			this.pbSettingsRS485.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSettingsRS485.ImageListBack");
			this.pbSettingsRS485.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSettingsRS485.ImageListFore");
			this.pbSettingsRS485.Location = new Point(31, 91);
			this.pbSettingsRS485.Name = "pbSettingsRS485";
			this.pbSettingsRS485.SelectedImageBack = -1;
			this.pbSettingsRS485.SelectedImageFore = -1;
			this.pbSettingsRS485.Size = new Size(72, 66);
			this.pbSettingsRS485.TabIndex = 57;
			this.pbSettingsRS485.TabStop = false;
			this.pbSettingsRS485.Click += new EventHandler(this.settingsRS485_Click);
			this.pnlSettingsRS485.BackColor = Color.Silver;
			this.pnlSettingsRS485.BorderStyle = BorderStyle.Fixed3D;
			this.pnlSettingsRS485.Controls.Add(this.lblSettingsRS485Value);
			this.pnlSettingsRS485.Controls.Add(this.lblSettingsRS485Header);
			this.pnlSettingsRS485.Cursor = Cursors.Hand;
			this.pnlSettingsRS485.Location = new Point(141, 89);
			this.pnlSettingsRS485.Name = "pnlSettingsRS485";
			this.pnlSettingsRS485.Size = new Size(169, 55);
			this.pnlSettingsRS485.TabIndex = 56;
			this.pnlSettingsRS485.Click += new EventHandler(this.settingsRS485_Click);
			this.lblSettingsRS485Value.BackColor = Color.White;
			this.lblSettingsRS485Value.Cursor = Cursors.Hand;
			this.lblSettingsRS485Value.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsRS485Value.ForeColor = Color.Black;
			this.lblSettingsRS485Value.Location = new Point(7, 24);
			this.lblSettingsRS485Value.Name = "lblSettingsRS485Value";
			this.lblSettingsRS485Value.Size = new Size(152, 20);
			this.lblSettingsRS485Value.TabIndex = 61;
			this.lblSettingsRS485Value.Tag = "";
			this.lblSettingsRS485Value.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsRS485Value.Click += new EventHandler(this.settingsRS485_Click);
			this.lblSettingsRS485Header.Cursor = Cursors.Hand;
			this.lblSettingsRS485Header.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblSettingsRS485Header.Location = new Point(8, -2);
			this.lblSettingsRS485Header.Name = "lblSettingsRS485Header";
			this.lblSettingsRS485Header.Size = new Size(151, 22);
			this.lblSettingsRS485Header.TabIndex = 58;
			this.lblSettingsRS485Header.Tag = "";
			this.lblSettingsRS485Header.Text = "RS 485 Status";
			this.lblSettingsRS485Header.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsRS485Header.Click += new EventHandler(this.settingsRS485_Click);
			this.pbSettingsRS232.BackColor = Color.Transparent;
			this.pbSettingsRS232.BackgroundImage = Resources.c9PinSerialPlug;
			this.pbSettingsRS232.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSettingsRS232.Cursor = Cursors.Hand;
			this.pbSettingsRS232.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSettingsRS232.ImageListBack");
			this.pbSettingsRS232.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSettingsRS232.ImageListFore");
			this.pbSettingsRS232.Location = new Point(16, 4);
			this.pbSettingsRS232.Name = "pbSettingsRS232";
			this.pbSettingsRS232.SelectedImageBack = -1;
			this.pbSettingsRS232.SelectedImageFore = -1;
			this.pbSettingsRS232.Size = new Size(96, 74);
			this.pbSettingsRS232.TabIndex = 54;
			this.pbSettingsRS232.TabStop = false;
			this.pbSettingsRS232.Click += new EventHandler(this.settingsRS232_Click);
			this.pnlSettingsIIC.BackColor = Color.Silver;
			this.pnlSettingsIIC.BorderStyle = BorderStyle.Fixed3D;
			this.pnlSettingsIIC.Controls.Add(this.lblSettingsIICCommTypeValue);
			this.pnlSettingsIIC.Controls.Add(this.lblSettingsIICHeader);
			this.pnlSettingsIIC.Controls.Add(this.lblSettingsIICCommType);
			this.pnlSettingsIIC.Controls.Add(this.btnSettingsIICDetectSlaves);
			this.pnlSettingsIIC.Cursor = Cursors.Hand;
			this.pnlSettingsIIC.Location = new Point(555, 157);
			this.pnlSettingsIIC.Name = "pnlSettingsIIC";
			this.pnlSettingsIIC.Size = new Size(189, 83);
			this.pnlSettingsIIC.TabIndex = 21;
			this.pnlSettingsIIC.Click += new EventHandler(this.settingsIIC_Click);
			this.lblSettingsIICCommTypeValue.BackColor = Color.White;
			this.lblSettingsIICCommTypeValue.Cursor = Cursors.Hand;
			this.lblSettingsIICCommTypeValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsIICCommTypeValue.ForeColor = Color.Black;
			this.lblSettingsIICCommTypeValue.Location = new Point(80, 22);
			this.lblSettingsIICCommTypeValue.Name = "lblSettingsIICCommTypeValue";
			this.lblSettingsIICCommTypeValue.Size = new Size(97, 20);
			this.lblSettingsIICCommTypeValue.TabIndex = 65;
			this.lblSettingsIICCommTypeValue.Tag = "";
			this.lblSettingsIICCommTypeValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsIICCommTypeValue.Click += new EventHandler(this.settingsIIC_Click);
			this.lblSettingsIICHeader.Cursor = Cursors.Hand;
			this.lblSettingsIICHeader.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblSettingsIICHeader.Location = new Point(9, -2);
			this.lblSettingsIICHeader.Name = "lblSettingsIICHeader";
			this.lblSettingsIICHeader.Size = new Size(167, 22);
			this.lblSettingsIICHeader.TabIndex = 59;
			this.lblSettingsIICHeader.Tag = "";
			this.lblSettingsIICHeader.Text = "Inter-Inverter Comm Status";
			this.lblSettingsIICHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsIICHeader.Click += new EventHandler(this.settingsIIC_Click);
			this.lblSettingsIICCommType.Cursor = Cursors.Hand;
			this.lblSettingsIICCommType.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsIICCommType.ForeColor = Color.Black;
			this.lblSettingsIICCommType.Location = new Point(5, 16);
			this.lblSettingsIICCommType.Name = "lblSettingsIICCommType";
			this.lblSettingsIICCommType.Size = new Size(76, 29);
			this.lblSettingsIICCommType.TabIndex = 16;
			this.lblSettingsIICCommType.Tag = "";
			this.lblSettingsIICCommType.Text = "Comm Type:";
			this.lblSettingsIICCommType.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsIICCommType.Click += new EventHandler(this.settingsIIC_Click);
			this.btnSettingsIICDetectSlaves.Cursor = Cursors.Hand;
			this.btnSettingsIICDetectSlaves.Location = new Point(21, 47);
			this.btnSettingsIICDetectSlaves.Name = "btnSettingsIICDetectSlaves";
			this.btnSettingsIICDetectSlaves.Size = new Size(143, 27);
			this.btnSettingsIICDetectSlaves.TabIndex = 60;
			this.btnSettingsIICDetectSlaves.Tag = "";
			this.btnSettingsIICDetectSlaves.Text = "Detect Slaves";
			this.btnSettingsIICDetectSlaves.UseVisualStyleBackColor = true;
			this.btnSettingsIICDetectSlaves.Click += new EventHandler(this.btnSettingsIICDetectSlaves_Click);
			this.pnlSettingsServer.BackColor = Color.Silver;
			this.pnlSettingsServer.BorderStyle = BorderStyle.Fixed3D;
			this.pnlSettingsServer.Controls.Add(this.lblSettingsServerViaValue);
			this.pnlSettingsServer.Controls.Add(this.lblSettingsServerPortValue);
			this.pnlSettingsServer.Controls.Add(this.lblSettingsServerAddressValue);
			this.pnlSettingsServer.Controls.Add(this.lblSettingsServerHeader);
			this.pnlSettingsServer.Controls.Add(this.lblSettingsServerAddress);
			this.pnlSettingsServer.Controls.Add(this.lblSettingsServerPort);
			this.pnlSettingsServer.Controls.Add(this.lblSettingsServerVia);
			this.pnlSettingsServer.Controls.Add(this.btnSettingsServerPingTest);
			this.pnlSettingsServer.Cursor = Cursors.Hand;
			this.pnlSettingsServer.Location = new Point(554, 15);
			this.pnlSettingsServer.Name = "pnlSettingsServer";
			this.pnlSettingsServer.Size = new Size(189, 132);
			this.pnlSettingsServer.TabIndex = 20;
			this.pnlSettingsServer.Click += new EventHandler(this.settingsServer_Click);
			this.lblSettingsServerViaValue.BackColor = Color.White;
			this.lblSettingsServerViaValue.Cursor = Cursors.Hand;
			this.lblSettingsServerViaValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsServerViaValue.ForeColor = Color.Black;
			this.lblSettingsServerViaValue.Location = new Point(58, 74);
			this.lblSettingsServerViaValue.Name = "lblSettingsServerViaValue";
			this.lblSettingsServerViaValue.Size = new Size(120, 20);
			this.lblSettingsServerViaValue.TabIndex = 67;
			this.lblSettingsServerViaValue.Tag = "";
			this.lblSettingsServerViaValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsServerViaValue.Click += new EventHandler(this.settingsServer_Click);
			this.lblSettingsServerPortValue.BackColor = Color.White;
			this.lblSettingsServerPortValue.Cursor = Cursors.Hand;
			this.lblSettingsServerPortValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsServerPortValue.ForeColor = Color.Black;
			this.lblSettingsServerPortValue.Location = new Point(58, 48);
			this.lblSettingsServerPortValue.Name = "lblSettingsServerPortValue";
			this.lblSettingsServerPortValue.Size = new Size(120, 20);
			this.lblSettingsServerPortValue.TabIndex = 66;
			this.lblSettingsServerPortValue.Tag = "";
			this.lblSettingsServerPortValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsServerPortValue.Click += new EventHandler(this.settingsServer_Click);
			this.lblSettingsServerAddressValue.BackColor = Color.White;
			this.lblSettingsServerAddressValue.Cursor = Cursors.Hand;
			this.lblSettingsServerAddressValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsServerAddressValue.ForeColor = Color.Black;
			this.lblSettingsServerAddressValue.Location = new Point(58, 22);
			this.lblSettingsServerAddressValue.Name = "lblSettingsServerAddressValue";
			this.lblSettingsServerAddressValue.Size = new Size(120, 20);
			this.lblSettingsServerAddressValue.TabIndex = 65;
			this.lblSettingsServerAddressValue.Tag = "";
			this.lblSettingsServerAddressValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsServerAddressValue.Click += new EventHandler(this.settingsServer_Click);
			this.lblSettingsServerHeader.Cursor = Cursors.Hand;
			this.lblSettingsServerHeader.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblSettingsServerHeader.Location = new Point(5, -2);
			this.lblSettingsServerHeader.Name = "lblSettingsServerHeader";
			this.lblSettingsServerHeader.Size = new Size(177, 22);
			this.lblSettingsServerHeader.TabIndex = 58;
			this.lblSettingsServerHeader.Tag = "";
			this.lblSettingsServerHeader.Text = "Server Connection Status";
			this.lblSettingsServerHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsServerHeader.Click += new EventHandler(this.settingsServer_Click);
			this.lblSettingsServerAddress.Cursor = Cursors.Hand;
			this.lblSettingsServerAddress.Location = new Point(2, 19);
			this.lblSettingsServerAddress.Name = "lblSettingsServerAddress";
			this.lblSettingsServerAddress.Size = new Size(53, 22);
			this.lblSettingsServerAddress.TabIndex = 43;
			this.lblSettingsServerAddress.Tag = "";
			this.lblSettingsServerAddress.Text = "Address:";
			this.lblSettingsServerAddress.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsServerAddress.Click += new EventHandler(this.settingsServer_Click);
			this.lblSettingsServerPort.Cursor = Cursors.Hand;
			this.lblSettingsServerPort.Location = new Point(2, 46);
			this.lblSettingsServerPort.Name = "lblSettingsServerPort";
			this.lblSettingsServerPort.Size = new Size(53, 22);
			this.lblSettingsServerPort.TabIndex = 45;
			this.lblSettingsServerPort.Tag = "";
			this.lblSettingsServerPort.Text = "Port:";
			this.lblSettingsServerPort.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsServerPort.Click += new EventHandler(this.settingsServer_Click);
			this.lblSettingsServerVia.Cursor = Cursors.Hand;
			this.lblSettingsServerVia.Location = new Point(3, 72);
			this.lblSettingsServerVia.Name = "lblSettingsServerVia";
			this.lblSettingsServerVia.Size = new Size(52, 22);
			this.lblSettingsServerVia.TabIndex = 47;
			this.lblSettingsServerVia.Tag = "";
			this.lblSettingsServerVia.Text = "Via:";
			this.lblSettingsServerVia.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsServerVia.Click += new EventHandler(this.settingsServer_Click);
			this.btnSettingsServerPingTest.BackColor = Color.Silver;
			this.btnSettingsServerPingTest.Location = new Point(21, 98);
			this.btnSettingsServerPingTest.Name = "btnSettingsServerPingTest";
			this.btnSettingsServerPingTest.Size = new Size(143, 27);
			this.btnSettingsServerPingTest.TabIndex = 61;
			this.btnSettingsServerPingTest.Tag = "";
			this.btnSettingsServerPingTest.Text = "Ping Test";
			this.btnSettingsServerPingTest.UseVisualStyleBackColor = true;
			this.btnSettingsServerPingTest.Click += new EventHandler(this.btnSettingsServerPingTest_Click);
			this.pnlSettingsLAN.BackColor = Color.Silver;
			this.pnlSettingsLAN.BorderStyle = BorderStyle.Fixed3D;
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANGatewayValue);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANSubnetMaskValue);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANCurrentIPValue);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANDHCPStatusValue);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANHeader);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANDHCPStatus);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANCurrentIP);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANGateway);
			this.pnlSettingsLAN.Controls.Add(this.lblSettingsLANSubnetMask);
			this.pnlSettingsLAN.Cursor = Cursors.Hand;
			this.pnlSettingsLAN.Location = new Point(142, 244);
			this.pnlSettingsLAN.Name = "pnlSettingsLAN";
			this.pnlSettingsLAN.Size = new Size(167, 129);
			this.pnlSettingsLAN.TabIndex = 19;
			this.pnlSettingsLAN.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANGatewayValue.BackColor = Color.White;
			this.lblSettingsLANGatewayValue.Cursor = Cursors.Hand;
			this.lblSettingsLANGatewayValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsLANGatewayValue.ForeColor = Color.Black;
			this.lblSettingsLANGatewayValue.Location = new Point(59, 99);
			this.lblSettingsLANGatewayValue.Name = "lblSettingsLANGatewayValue";
			this.lblSettingsLANGatewayValue.Size = new Size(97, 20);
			this.lblSettingsLANGatewayValue.TabIndex = 67;
			this.lblSettingsLANGatewayValue.Tag = "";
			this.lblSettingsLANGatewayValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANGatewayValue.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANSubnetMaskValue.BackColor = Color.White;
			this.lblSettingsLANSubnetMaskValue.Cursor = Cursors.Hand;
			this.lblSettingsLANSubnetMaskValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsLANSubnetMaskValue.ForeColor = Color.Black;
			this.lblSettingsLANSubnetMaskValue.Location = new Point(59, 73);
			this.lblSettingsLANSubnetMaskValue.Name = "lblSettingsLANSubnetMaskValue";
			this.lblSettingsLANSubnetMaskValue.Size = new Size(97, 20);
			this.lblSettingsLANSubnetMaskValue.TabIndex = 66;
			this.lblSettingsLANSubnetMaskValue.Tag = "";
			this.lblSettingsLANSubnetMaskValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANSubnetMaskValue.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANCurrentIPValue.BackColor = Color.White;
			this.lblSettingsLANCurrentIPValue.Cursor = Cursors.Hand;
			this.lblSettingsLANCurrentIPValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsLANCurrentIPValue.ForeColor = Color.Black;
			this.lblSettingsLANCurrentIPValue.Location = new Point(59, 48);
			this.lblSettingsLANCurrentIPValue.Name = "lblSettingsLANCurrentIPValue";
			this.lblSettingsLANCurrentIPValue.Size = new Size(97, 20);
			this.lblSettingsLANCurrentIPValue.TabIndex = 65;
			this.lblSettingsLANCurrentIPValue.Tag = "";
			this.lblSettingsLANCurrentIPValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANCurrentIPValue.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANDHCPStatusValue.BackColor = Color.White;
			this.lblSettingsLANDHCPStatusValue.Cursor = Cursors.Hand;
			this.lblSettingsLANDHCPStatusValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsLANDHCPStatusValue.ForeColor = Color.Black;
			this.lblSettingsLANDHCPStatusValue.Location = new Point(59, 22);
			this.lblSettingsLANDHCPStatusValue.Name = "lblSettingsLANDHCPStatusValue";
			this.lblSettingsLANDHCPStatusValue.Size = new Size(97, 20);
			this.lblSettingsLANDHCPStatusValue.TabIndex = 64;
			this.lblSettingsLANDHCPStatusValue.Tag = "";
			this.lblSettingsLANDHCPStatusValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANDHCPStatusValue.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANHeader.Cursor = Cursors.Hand;
			this.lblSettingsLANHeader.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblSettingsLANHeader.Location = new Point(6, -1);
			this.lblSettingsLANHeader.Name = "lblSettingsLANHeader";
			this.lblSettingsLANHeader.Size = new Size(152, 22);
			this.lblSettingsLANHeader.TabIndex = 59;
			this.lblSettingsLANHeader.Tag = "";
			this.lblSettingsLANHeader.Text = "LAN Status";
			this.lblSettingsLANHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANHeader.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANDHCPStatus.Cursor = Cursors.Hand;
			this.lblSettingsLANDHCPStatus.Location = new Point(4, 19);
			this.lblSettingsLANDHCPStatus.Name = "lblSettingsLANDHCPStatus";
			this.lblSettingsLANDHCPStatus.Size = new Size(53, 22);
			this.lblSettingsLANDHCPStatus.TabIndex = 53;
			this.lblSettingsLANDHCPStatus.Tag = "";
			this.lblSettingsLANDHCPStatus.Text = "DHCP:";
			this.lblSettingsLANDHCPStatus.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANDHCPStatus.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANCurrentIP.Cursor = Cursors.Hand;
			this.lblSettingsLANCurrentIP.Location = new Point(3, 45);
			this.lblSettingsLANCurrentIP.Name = "lblSettingsLANCurrentIP";
			this.lblSettingsLANCurrentIP.Size = new Size(53, 22);
			this.lblSettingsLANCurrentIP.TabIndex = 51;
			this.lblSettingsLANCurrentIP.Tag = "";
			this.lblSettingsLANCurrentIP.Text = "IP:";
			this.lblSettingsLANCurrentIP.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANCurrentIP.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANGateway.Cursor = Cursors.Hand;
			this.lblSettingsLANGateway.Location = new Point(4, 95);
			this.lblSettingsLANGateway.Name = "lblSettingsLANGateway";
			this.lblSettingsLANGateway.Size = new Size(53, 22);
			this.lblSettingsLANGateway.TabIndex = 62;
			this.lblSettingsLANGateway.Tag = "";
			this.lblSettingsLANGateway.Text = "Gateway:";
			this.lblSettingsLANGateway.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANGateway.Click += new EventHandler(this.settingsLAN_Click);
			this.lblSettingsLANSubnetMask.Cursor = Cursors.Hand;
			this.lblSettingsLANSubnetMask.Location = new Point(3, 70);
			this.lblSettingsLANSubnetMask.Name = "lblSettingsLANSubnetMask";
			this.lblSettingsLANSubnetMask.Size = new Size(53, 22);
			this.lblSettingsLANSubnetMask.TabIndex = 60;
			this.lblSettingsLANSubnetMask.Tag = "";
			this.lblSettingsLANSubnetMask.Text = "Subnet:";
			this.lblSettingsLANSubnetMask.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsLANSubnetMask.Click += new EventHandler(this.settingsLAN_Click);
			this.pnlSettingsZB.BackColor = Color.Silver;
			this.pnlSettingsZB.BorderStyle = BorderStyle.Fixed3D;
			this.pnlSettingsZB.Controls.Add(this.lblSettingsZBValue);
			this.pnlSettingsZB.Controls.Add(this.lblSettingsZBHeader);
			this.pnlSettingsZB.Cursor = Cursors.Hand;
			this.pnlSettingsZB.Location = new Point(141, 167);
			this.pnlSettingsZB.Name = "pnlSettingsZB";
			this.pnlSettingsZB.Size = new Size(169, 57);
			this.pnlSettingsZB.TabIndex = 18;
			this.pnlSettingsZB.Click += new EventHandler(this.settingsZB_Click);
			this.lblSettingsZBValue.BackColor = Color.White;
			this.lblSettingsZBValue.Cursor = Cursors.Hand;
			this.lblSettingsZBValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsZBValue.ForeColor = Color.Black;
			this.lblSettingsZBValue.Location = new Point(7, 26);
			this.lblSettingsZBValue.Name = "lblSettingsZBValue";
			this.lblSettingsZBValue.Size = new Size(152, 20);
			this.lblSettingsZBValue.TabIndex = 62;
			this.lblSettingsZBValue.Tag = "";
			this.lblSettingsZBValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsZBValue.Click += new EventHandler(this.settingsZB_Click);
			this.lblSettingsZBHeader.Cursor = Cursors.Hand;
			this.lblSettingsZBHeader.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblSettingsZBHeader.Location = new Point(8, -2);
			this.lblSettingsZBHeader.Name = "lblSettingsZBHeader";
			this.lblSettingsZBHeader.Size = new Size(151, 22);
			this.lblSettingsZBHeader.TabIndex = 58;
			this.lblSettingsZBHeader.Tag = "";
			this.lblSettingsZBHeader.Text = "ZigBee Status";
			this.lblSettingsZBHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSettingsZBHeader.Click += new EventHandler(this.settingsZB_Click);
			this.pnlSettingsIICLine2.BackColor = Color.Lime;
			this.pnlSettingsIICLine2.Location = new Point(512, 193);
			this.pnlSettingsIICLine2.Name = "pnlSettingsIICLine2";
			this.pnlSettingsIICLine2.Size = new Size(5, 112);
			this.pnlSettingsIICLine2.TabIndex = 15;
			this.pbSettingsLAN.BackColor = Color.Transparent;
			this.pbSettingsLAN.BackgroundImage = Resources.LAN2;
			this.pbSettingsLAN.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSettingsLAN.Cursor = Cursors.Hand;
			this.pbSettingsLAN.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSettingsLAN.ImageListBack");
			this.pbSettingsLAN.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSettingsLAN.ImageListFore");
			this.pbSettingsLAN.Location = new Point(6, 274);
			this.pbSettingsLAN.Name = "pbSettingsLAN";
			this.pbSettingsLAN.SelectedImageBack = -1;
			this.pbSettingsLAN.SelectedImageFore = -1;
			this.pbSettingsLAN.Size = new Size(102, 90);
			this.pbSettingsLAN.TabIndex = 3;
			this.pbSettingsLAN.TabStop = false;
			this.pbSettingsLAN.Click += new EventHandler(this.settingsLAN_Click);
			this.pbSettingsZB.BackColor = Color.Transparent;
			this.pbSettingsZB.BackgroundImage = Resources.ZigBee;
			this.pbSettingsZB.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSettingsZB.Cursor = Cursors.Hand;
			this.pbSettingsZB.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSettingsZB.ImageListBack");
			this.pbSettingsZB.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSettingsZB.ImageListFore");
			this.pbSettingsZB.Location = new Point(16, 169);
			this.pbSettingsZB.Name = "pbSettingsZB";
			this.pbSettingsZB.SelectedImageBack = -1;
			this.pbSettingsZB.SelectedImageFore = -1;
			this.pbSettingsZB.Size = new Size(96, 74);
			this.pbSettingsZB.TabIndex = 1;
			this.pbSettingsZB.TabStop = false;
			this.pbSettingsZB.Click += new EventHandler(this.settingsZB_Click);
			this.pbSettingsIIC.BackColor = Color.Transparent;
			this.pbSettingsIIC.BackgroundImage = Resources.Inverters;
			this.pbSettingsIIC.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSettingsIIC.Cursor = Cursors.Hand;
			this.pbSettingsIIC.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSettingsIIC.ImageListBack");
			this.pbSettingsIIC.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSettingsIIC.ImageListFore");
			this.pbSettingsIIC.Location = new Point(613, 242);
			this.pbSettingsIIC.Name = "pbSettingsIIC";
			this.pbSettingsIIC.SelectedImageBack = -1;
			this.pbSettingsIIC.SelectedImageFore = -1;
			this.pbSettingsIIC.Size = new Size(137, 139);
			this.pbSettingsIIC.TabIndex = 5;
			this.pbSettingsIIC.TabStop = false;
			this.pbSettingsIIC.Click += new EventHandler(this.settingsIIC_Click);
			this.pbSettingsServerCloud.BackColor = Color.Transparent;
			this.pbSettingsServerCloud.BackgroundImage = Resources.Server;
			this.pbSettingsServerCloud.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSettingsServerCloud.Cursor = Cursors.Hand;
			this.pbSettingsServerCloud.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSettingsServerCloud.ImageListBack");
			this.pbSettingsServerCloud.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSettingsServerCloud.ImageListFore");
			this.pbSettingsServerCloud.Location = new Point(456, 4);
			this.pbSettingsServerCloud.Name = "pbSettingsServerCloud";
			this.pbSettingsServerCloud.SelectedImageBack = -1;
			this.pbSettingsServerCloud.SelectedImageFore = -1;
			this.pbSettingsServerCloud.Size = new Size(68, 90);
			this.pbSettingsServerCloud.TabIndex = 4;
			this.pbSettingsServerCloud.TabStop = false;
			this.pbSettingsServerCloud.Click += new EventHandler(this.settingsServer_Click);
			this.pbSettingsInverter.BackColor = Color.Transparent;
			this.pbSettingsInverter.BackgroundImage = Resources._1Inv;
			this.pbSettingsInverter.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSettingsInverter.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSettingsInverter.ImageListBack");
			this.pbSettingsInverter.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSettingsInverter.ImageListFore");
			this.pbSettingsInverter.Location = new Point(340, 88);
			this.pbSettingsInverter.Name = "pbSettingsInverter";
			this.pbSettingsInverter.SelectedImageBack = -1;
			this.pbSettingsInverter.SelectedImageFore = -1;
			this.pbSettingsInverter.Size = new Size(122, 189);
			this.pbSettingsInverter.TabIndex = 0;
			this.pbSettingsInverter.TabStop = false;
			this.pnlSettingsRS232Line1.BackColor = Color.Lime;
			this.pnlSettingsRS232Line1.Location = new Point(63, 44);
			this.pnlSettingsRS232Line1.Name = "pnlSettingsRS232Line1";
			this.pnlSettingsRS232Line1.Size = new Size(320, 5);
			this.pnlSettingsRS232Line1.TabIndex = 7;
			this.pnlSettingsZBLine1.BackColor = Color.Lime;
			this.pnlSettingsZBLine1.Location = new Point(57, 191);
			this.pnlSettingsZBLine1.Name = "pnlSettingsZBLine1";
			this.pnlSettingsZBLine1.Size = new Size(313, 5);
			this.pnlSettingsZBLine1.TabIndex = 8;
			this.pnlSettingsLANLineTCPStatus.BackColor = Color.Lime;
			this.pnlSettingsLANLineTCPStatus.Location = new Point(73, 328);
			this.pnlSettingsLANLineTCPStatus.Name = "pnlSettingsLANLineTCPStatus";
			this.pnlSettingsLANLineTCPStatus.Size = new Size(200, 5);
			this.pnlSettingsLANLineTCPStatus.TabIndex = 9;
			this.pnlSettingsRS232Line2.BackColor = Color.Lime;
			this.pnlSettingsRS232Line2.Location = new Point(379, 47);
			this.pnlSettingsRS232Line2.Name = "pnlSettingsRS232Line2";
			this.pnlSettingsRS232Line2.Size = new Size(5, 144);
			this.pnlSettingsRS232Line2.TabIndex = 10;
			this.pnlSettingsLANLineEthernetCableStatus2.BackColor = Color.Lime;
			this.pnlSettingsLANLineEthernetCableStatus2.Location = new Point(384, 188);
			this.pnlSettingsLANLineEthernetCableStatus2.Name = "pnlSettingsLANLineEthernetCableStatus2";
			this.pnlSettingsLANLineEthernetCableStatus2.Size = new Size(5, 144);
			this.pnlSettingsLANLineEthernetCableStatus2.TabIndex = 11;
			this.pnlSettingsServerLine1.BackColor = Color.Lime;
			this.pnlSettingsServerLine1.Location = new Point(410, 44);
			this.pnlSettingsServerLine1.Name = "pnlSettingsServerLine1";
			this.pnlSettingsServerLine1.Size = new Size(5, 144);
			this.pnlSettingsServerLine1.TabIndex = 12;
			this.pnlSettingsServerLine2.BackColor = Color.Lime;
			this.pnlSettingsServerLine2.Location = new Point(410, 44);
			this.pnlSettingsServerLine2.Name = "pnlSettingsServerLine2";
			this.pnlSettingsServerLine2.Size = new Size(290, 5);
			this.pnlSettingsServerLine2.TabIndex = 13;
			this.pnlSettingsIICLine1.BackColor = Color.Lime;
			this.pnlSettingsIICLine1.Location = new Point(367, 192);
			this.pnlSettingsIICLine1.Name = "pnlSettingsIICLine1";
			this.pnlSettingsIICLine1.Size = new Size(150, 5);
			this.pnlSettingsIICLine1.TabIndex = 14;
			this.pnlSettingsIICLine3.BackColor = Color.Lime;
			this.pnlSettingsIICLine3.Location = new Point(515, 300);
			this.pnlSettingsIICLine3.Name = "pnlSettingsIICLine3";
			this.pnlSettingsIICLine3.Size = new Size(150, 5);
			this.pnlSettingsIICLine3.TabIndex = 16;
			this.pnlSettingsLANLineEthernetCableStatus1.BackColor = Color.Lime;
			this.pnlSettingsLANLineEthernetCableStatus1.Location = new Point(289, 327);
			this.pnlSettingsLANLineEthernetCableStatus1.Name = "pnlSettingsLANLineEthernetCableStatus1";
			this.pnlSettingsLANLineEthernetCableStatus1.Size = new Size(100, 5);
			this.pnlSettingsLANLineEthernetCableStatus1.TabIndex = 24;
			this.pnlSettingsRS485Line1.BackColor = Color.Lime;
			this.pnlSettingsRS485Line1.Location = new Point(57, 114);
			this.pnlSettingsRS485Line1.Name = "pnlSettingsRS485Line1";
			this.pnlSettingsRS485Line1.Size = new Size(313, 5);
			this.pnlSettingsRS485Line1.TabIndex = 55;
			this.tpRegionalSettings.BackColor = Color.Gray;
			this.tpRegionalSettings.BackgroundImage = Resources.BackGrad;
			this.tpRegionalSettings.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpRegionalSettings.Controls.Add(this.pnlRSDSPParams);
			this.tpRegionalSettings.Controls.Add(this.pnlRSCPUParams);
			this.tpRegionalSettings.Controls.Add(this.pnlRSRSTop);
			this.tpRegionalSettings.Controls.Add(this.btnSettingsRSApply);
			this.tpRegionalSettings.Location = new Point(4, 25);
			this.tpRegionalSettings.Name = "tpRegionalSettings";
			this.tpRegionalSettings.Padding = new Padding(3);
			this.tpRegionalSettings.Size = new Size(761, 393);
			this.tpRegionalSettings.TabIndex = 0;
			this.tpRegionalSettings.Text = "Regional Settings";
			this.tpRegionalSettings.UseVisualStyleBackColor = true;
			this.pnlRSDSPParams.BackColor = Color.Transparent;
			this.pnlRSDSPParams.BackgroundImage = (Image)componentResourceManager.GetObject("pnlRSDSPParams.BackgroundImage");
			this.pnlRSDSPParams.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlRSDSPParams.Controls.Add(this.dgvVenusParams);
			this.pnlRSDSPParams.Controls.Add(this.lblRSIPVenusParam);
			this.pnlRSDSPParams.Location = new Point(383, 113);
			this.pnlRSDSPParams.Name = "pnlRSDSPParams";
			this.pnlRSDSPParams.Size = new Size(365, 224);
			this.pnlRSDSPParams.TabIndex = 6;
			this.pnlRSDSPParams.Visible = false;
			this.dgvVenusParams.AllowUserToAddRows = false;
			this.dgvVenusParams.AllowUserToDeleteRows = false;
			this.dgvVenusParams.AllowUserToResizeColumns = false;
			this.dgvVenusParams.AllowUserToResizeRows = false;
			this.dgvVenusParams.BorderStyle = BorderStyle.Fixed3D;
			dataGridViewCellStyle8.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle8.BackColor = SystemColors.Control;
			dataGridViewCellStyle8.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle8.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle8.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle8.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle8.WrapMode = DataGridViewTriState.True;
			this.dgvVenusParams.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
			this.dgvVenusParams.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvVenusParams.ColumnHeadersVisible = false;
			this.dgvVenusParams.Columns.AddRange(new DataGridViewColumn[]
			{
				this.dataGridViewTextBoxColumn1,
				this.dataGridViewTextBoxColumn2,
				this.dataGridViewTextBoxColumn3,
				this.dataGridViewTextBoxColumn5
			});
			dataGridViewCellStyle9.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle9.BackColor = SystemColors.Window;
			dataGridViewCellStyle9.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle9.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle9.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle9.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle9.WrapMode = DataGridViewTriState.True;
			this.dgvVenusParams.DefaultCellStyle = dataGridViewCellStyle9;
			this.dgvVenusParams.Location = new Point(6, 36);
			this.dgvVenusParams.Name = "dgvVenusParams";
			dataGridViewCellStyle10.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle10.BackColor = SystemColors.Control;
			dataGridViewCellStyle10.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle10.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle10.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle10.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle10.WrapMode = DataGridViewTriState.True;
			this.dgvVenusParams.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
			this.dgvVenusParams.RowHeadersVisible = false;
			dataGridViewCellStyle11.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle11.BackColor = Color.White;
			dataGridViewCellStyle11.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle11.ForeColor = Color.Black;
			dataGridViewCellStyle11.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle11.SelectionForeColor = Color.White;
			dataGridViewCellStyle11.WrapMode = DataGridViewTriState.True;
			this.dgvVenusParams.RowsDefaultCellStyle = dataGridViewCellStyle11;
			this.dgvVenusParams.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvVenusParams.RowTemplate.DefaultCellStyle.BackColor = Color.White;
			this.dgvVenusParams.RowTemplate.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.dgvVenusParams.RowTemplate.DefaultCellStyle.ForeColor = Color.Black;
			this.dgvVenusParams.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.DodgerBlue;
			this.dgvVenusParams.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.White;
			this.dgvVenusParams.RowTemplate.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvVenusParams.ScrollBars = ScrollBars.Vertical;
			this.dgvVenusParams.ShowCellErrors = false;
			this.dgvVenusParams.ShowRowErrors = false;
			this.dgvVenusParams.Size = new Size(354, 172);
			this.dgvVenusParams.TabIndex = 7;
			this.dgvVenusParams.Tag = "";
			this.dgvVenusParams.CellMouseEnter += new DataGridViewCellEventHandler(this.dgvVenusParams_CellMouseEnter);
			this.dgvVenusParams.CellEndEdit += new DataGridViewCellEventHandler(this.dgvVenusParams_CellEndEdit);
			dataGridViewCellStyle12.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle12.BackColor = Color.White;
			dataGridViewCellStyle12.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle12.ForeColor = Color.Black;
			dataGridViewCellStyle12.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle12.SelectionForeColor = Color.White;
			dataGridViewCellStyle12.WrapMode = DataGridViewTriState.True;
			this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle12;
			this.dataGridViewTextBoxColumn1.HeaderText = "Index";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn1.Width = 30;
			dataGridViewCellStyle13.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle13.BackColor = Color.White;
			dataGridViewCellStyle13.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle13.ForeColor = Color.Black;
			dataGridViewCellStyle13.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle13.SelectionForeColor = Color.White;
			dataGridViewCellStyle13.WrapMode = DataGridViewTriState.True;
			this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle13;
			this.dataGridViewTextBoxColumn2.HeaderText = "Parameter Name";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn2.Width = 163;
			dataGridViewCellStyle14.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle14.BackColor = Color.White;
			dataGridViewCellStyle14.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle14.ForeColor = Color.Black;
			dataGridViewCellStyle14.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle14.SelectionForeColor = Color.White;
			dataGridViewCellStyle14.WrapMode = DataGridViewTriState.True;
			this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
			this.dataGridViewTextBoxColumn3.HeaderText = "Parameter Value";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Resizable = DataGridViewTriState.False;
			dataGridViewCellStyle15.BackColor = Color.White;
			dataGridViewCellStyle15.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle15.ForeColor = Color.Black;
			dataGridViewCellStyle15.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle15.SelectionForeColor = Color.White;
			this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle15;
			this.dataGridViewTextBoxColumn5.HeaderText = "Units";
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.ReadOnly = true;
			this.dataGridViewTextBoxColumn5.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn5.Width = 50;
			this.lblRSIPVenusParam.BackColor = Color.Transparent;
			this.lblRSIPVenusParam.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblRSIPVenusParam.ForeColor = Color.Black;
			this.lblRSIPVenusParam.Location = new Point(16, 3);
			this.lblRSIPVenusParam.Name = "lblRSIPVenusParam";
			this.lblRSIPVenusParam.Size = new Size(337, 29);
			this.lblRSIPVenusParam.TabIndex = 5;
			this.lblRSIPVenusParam.Text = "Venus Parameters";
			this.lblRSIPVenusParam.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlRSCPUParams.BackColor = Color.Transparent;
			this.pnlRSCPUParams.BackgroundImage = (Image)componentResourceManager.GetObject("pnlRSCPUParams.BackgroundImage");
			this.pnlRSCPUParams.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlRSCPUParams.Controls.Add(this.dgvPortiaParams);
			this.pnlRSCPUParams.Controls.Add(this.lblRSIPPortiaParam);
			this.pnlRSCPUParams.Location = new Point(9, 114);
			this.pnlRSCPUParams.Name = "pnlRSCPUParams";
			this.pnlRSCPUParams.Size = new Size(365, 224);
			this.pnlRSCPUParams.TabIndex = 0;
			this.pnlRSCPUParams.Visible = false;
			this.dgvPortiaParams.AllowUserToAddRows = false;
			this.dgvPortiaParams.AllowUserToDeleteRows = false;
			this.dgvPortiaParams.AllowUserToResizeColumns = false;
			this.dgvPortiaParams.AllowUserToResizeRows = false;
			this.dgvPortiaParams.BorderStyle = BorderStyle.Fixed3D;
			dataGridViewCellStyle16.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle16.BackColor = SystemColors.Control;
			dataGridViewCellStyle16.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle16.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle16.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle16.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle16.WrapMode = DataGridViewTriState.True;
			this.dgvPortiaParams.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
			this.dgvPortiaParams.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPortiaParams.ColumnHeadersVisible = false;
			this.dgvPortiaParams.Columns.AddRange(new DataGridViewColumn[]
			{
				this.colIndex,
				this.colName,
				this.colValue,
				this.colUnit
			});
			dataGridViewCellStyle17.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle17.BackColor = SystemColors.Window;
			dataGridViewCellStyle17.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle17.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle17.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle17.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle17.WrapMode = DataGridViewTriState.True;
			this.dgvPortiaParams.DefaultCellStyle = dataGridViewCellStyle17;
			this.dgvPortiaParams.Location = new Point(5, 36);
			this.dgvPortiaParams.Name = "dgvPortiaParams";
			dataGridViewCellStyle18.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle18.BackColor = SystemColors.Control;
			dataGridViewCellStyle18.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle18.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle18.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle18.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle18.WrapMode = DataGridViewTriState.True;
			this.dgvPortiaParams.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
			this.dgvPortiaParams.RowHeadersVisible = false;
			dataGridViewCellStyle19.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle19.BackColor = Color.White;
			dataGridViewCellStyle19.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle19.ForeColor = Color.Black;
			dataGridViewCellStyle19.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle19.SelectionForeColor = Color.White;
			dataGridViewCellStyle19.WrapMode = DataGridViewTriState.True;
			this.dgvPortiaParams.RowsDefaultCellStyle = dataGridViewCellStyle19;
			this.dgvPortiaParams.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvPortiaParams.RowTemplate.DefaultCellStyle.BackColor = Color.White;
			this.dgvPortiaParams.RowTemplate.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.dgvPortiaParams.RowTemplate.DefaultCellStyle.ForeColor = Color.Black;
			this.dgvPortiaParams.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.DodgerBlue;
			this.dgvPortiaParams.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.White;
			this.dgvPortiaParams.RowTemplate.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvPortiaParams.ScrollBars = ScrollBars.Vertical;
			this.dgvPortiaParams.ShowCellErrors = false;
			this.dgvPortiaParams.ShowRowErrors = false;
			this.dgvPortiaParams.Size = new Size(354, 172);
			this.dgvPortiaParams.TabIndex = 6;
			this.dgvPortiaParams.Tag = "";
			this.dgvPortiaParams.CellMouseEnter += new DataGridViewCellEventHandler(this.dgvPortiaParams_CellMouseEnter);
			this.dgvPortiaParams.CellEndEdit += new DataGridViewCellEventHandler(this.dgvPortiaParams_CellEndEdit);
			dataGridViewCellStyle20.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle20.BackColor = Color.White;
			dataGridViewCellStyle20.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle20.ForeColor = Color.Black;
			dataGridViewCellStyle20.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle20.SelectionForeColor = Color.White;
			dataGridViewCellStyle20.WrapMode = DataGridViewTriState.True;
			this.colIndex.DefaultCellStyle = dataGridViewCellStyle20;
			this.colIndex.HeaderText = "Index";
			this.colIndex.Name = "colIndex";
			this.colIndex.ReadOnly = true;
			this.colIndex.Resizable = DataGridViewTriState.False;
			this.colIndex.Width = 30;
			dataGridViewCellStyle21.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle21.BackColor = Color.White;
			dataGridViewCellStyle21.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle21.ForeColor = Color.Black;
			dataGridViewCellStyle21.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle21.SelectionForeColor = Color.White;
			dataGridViewCellStyle21.WrapMode = DataGridViewTriState.True;
			this.colName.DefaultCellStyle = dataGridViewCellStyle21;
			this.colName.HeaderText = "Parameter Name";
			this.colName.Name = "colName";
			this.colName.ReadOnly = true;
			this.colName.Resizable = DataGridViewTriState.False;
			this.colName.Width = 163;
			dataGridViewCellStyle22.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle22.BackColor = Color.White;
			dataGridViewCellStyle22.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle22.ForeColor = Color.Black;
			dataGridViewCellStyle22.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle22.SelectionForeColor = Color.White;
			dataGridViewCellStyle22.WrapMode = DataGridViewTriState.True;
			this.colValue.DefaultCellStyle = dataGridViewCellStyle22;
			this.colValue.HeaderText = "Parameter Value";
			this.colValue.Name = "colValue";
			this.colValue.ReadOnly = true;
			this.colValue.Resizable = DataGridViewTriState.False;
			dataGridViewCellStyle23.BackColor = Color.White;
			dataGridViewCellStyle23.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle23.ForeColor = Color.Black;
			dataGridViewCellStyle23.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle23.SelectionForeColor = Color.White;
			this.colUnit.DefaultCellStyle = dataGridViewCellStyle23;
			this.colUnit.HeaderText = "Units";
			this.colUnit.Name = "colUnit";
			this.colUnit.ReadOnly = true;
			this.colUnit.Resizable = DataGridViewTriState.False;
			this.colUnit.Width = 50;
			this.lblRSIPPortiaParam.BackColor = Color.Transparent;
			this.lblRSIPPortiaParam.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblRSIPPortiaParam.ForeColor = Color.Black;
			this.lblRSIPPortiaParam.Location = new Point(19, 3);
			this.lblRSIPPortiaParam.Name = "lblRSIPPortiaParam";
			this.lblRSIPPortiaParam.Size = new Size(335, 29);
			this.lblRSIPPortiaParam.TabIndex = 5;
			this.lblRSIPPortiaParam.Text = "Portia Parameters";
			this.lblRSIPPortiaParam.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlRSRSTop.BackColor = Color.Transparent;
			this.pnlRSRSTop.BackgroundImage = Resources.BackLandTinyNew;
			this.pnlRSRSTop.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlRSRSTop.Controls.Add(this.cmbSettingsRSRSCountry);
			this.pnlRSRSTop.Controls.Add(this.cmbSettingsRSRSLanguage);
			this.pnlRSRSTop.Controls.Add(this.lblSettingsRSRSCountry);
			this.pnlRSRSTop.Controls.Add(this.lblSettingsRSRSLanguage);
			this.pnlRSRSTop.Controls.Add(this.lblSettingsRSRSHeader);
			this.pnlRSRSTop.Location = new Point(199, 7);
			this.pnlRSRSTop.Name = "pnlRSRSTop";
			this.pnlRSRSTop.Size = new Size(359, 101);
			this.pnlRSRSTop.TabIndex = 1;
			this.cmbSettingsRSRSCountry.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbSettingsRSRSCountry.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.cmbSettingsRSRSCountry.FormattingEnabled = true;
			this.cmbSettingsRSRSCountry.Location = new Point(89, 67);
			this.cmbSettingsRSRSCountry.Name = "cmbSettingsRSRSCountry";
			this.cmbSettingsRSRSCountry.Size = new Size(239, 21);
			this.cmbSettingsRSRSCountry.TabIndex = 13;
			this.cmbSettingsRSRSCountry.Tag = "";
			this.cmbSettingsRSRSCountry.SelectedIndexChanged += new EventHandler(this.cmbSettingsRSRSCountry_SelectedIndexChanged);
			this.cmbSettingsRSRSLanguage.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbSettingsRSRSLanguage.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.cmbSettingsRSRSLanguage.FormattingEnabled = true;
			this.cmbSettingsRSRSLanguage.Location = new Point(89, 37);
			this.cmbSettingsRSRSLanguage.Name = "cmbSettingsRSRSLanguage";
			this.cmbSettingsRSRSLanguage.Size = new Size(239, 21);
			this.cmbSettingsRSRSLanguage.TabIndex = 11;
			this.cmbSettingsRSRSLanguage.Tag = "";
			this.cmbSettingsRSRSLanguage.SelectedIndexChanged += new EventHandler(this.cmbSettingsRSRSLanguage_SelectedIndexChanged);
			this.lblSettingsRSRSCountry.BackColor = Color.Transparent;
			this.lblSettingsRSRSCountry.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsRSRSCountry.ForeColor = Color.Black;
			this.lblSettingsRSRSCountry.Location = new Point(9, 62);
			this.lblSettingsRSRSCountry.Name = "lblSettingsRSRSCountry";
			this.lblSettingsRSRSCountry.Size = new Size(73, 29);
			this.lblSettingsRSRSCountry.TabIndex = 10;
			this.lblSettingsRSRSCountry.Tag = "";
			this.lblSettingsRSRSCountry.Text = "Country:";
			this.lblSettingsRSRSCountry.TextAlign = ContentAlignment.MiddleRight;
			this.lblSettingsRSRSLanguage.BackColor = Color.Transparent;
			this.lblSettingsRSRSLanguage.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsRSRSLanguage.ForeColor = Color.Black;
			this.lblSettingsRSRSLanguage.Location = new Point(10, 35);
			this.lblSettingsRSRSLanguage.Name = "lblSettingsRSRSLanguage";
			this.lblSettingsRSRSLanguage.Size = new Size(73, 29);
			this.lblSettingsRSRSLanguage.TabIndex = 8;
			this.lblSettingsRSRSLanguage.Tag = "";
			this.lblSettingsRSRSLanguage.Text = "Language:";
			this.lblSettingsRSRSLanguage.TextAlign = ContentAlignment.MiddleRight;
			this.lblSettingsRSRSHeader.BackColor = Color.Transparent;
			this.lblSettingsRSRSHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSettingsRSRSHeader.ForeColor = Color.Black;
			this.lblSettingsRSRSHeader.Location = new Point(19, 2);
			this.lblSettingsRSRSHeader.Name = "lblSettingsRSRSHeader";
			this.lblSettingsRSRSHeader.Size = new Size(326, 29);
			this.lblSettingsRSRSHeader.TabIndex = 5;
			this.lblSettingsRSRSHeader.Text = "Regional Settings";
			this.lblSettingsRSRSHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.btnSettingsRSApply.Enabled = false;
			this.btnSettingsRSApply.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.btnSettingsRSApply.Location = new Point(307, 346);
			this.btnSettingsRSApply.Name = "btnSettingsRSApply";
			this.btnSettingsRSApply.Size = new Size(143, 29);
			this.btnSettingsRSApply.TabIndex = 5;
			this.btnSettingsRSApply.Text = "APPLY";
			this.btnSettingsRSApply.UseVisualStyleBackColor = true;
			this.btnSettingsRSApply.Click += new EventHandler(this.btnSettingsRSApply_Click);
			this.tpPBData.BackgroundImage = Resources.BackGrad;
			this.tpPBData.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpPBData.Controls.Add(this.lblPBLastTelemTimeValue);
			this.tpPBData.Controls.Add(this.lblPBInverterWarning);
			this.tpPBData.Controls.Add(this.pnlPBBtns);
			this.tpPBData.Controls.Add(this.lblPBLastTelemTime);
			this.tpPBData.Controls.Add(this.pnlPBLPTop);
			this.tpPBData.Location = new Point(4, 23);
			this.tpPBData.Name = "tpPBData";
			this.tpPBData.Padding = new Padding(3);
			this.tpPBData.Size = new Size(765, 415);
			this.tpPBData.TabIndex = 3;
			this.tpPBData.Text = "PowerBoxes Data";
			this.tpPBData.UseVisualStyleBackColor = true;
			this.lblPBLastTelemTimeValue.BackColor = Color.Transparent;
			this.lblPBLastTelemTimeValue.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPBLastTelemTimeValue.ForeColor = Color.White;
			this.lblPBLastTelemTimeValue.Location = new Point(161, 382);
			this.lblPBLastTelemTimeValue.Name = "lblPBLastTelemTimeValue";
			this.lblPBLastTelemTimeValue.Size = new Size(111, 22);
			this.lblPBLastTelemTimeValue.TabIndex = 7;
			this.lblPBLastTelemTimeValue.TextAlign = ContentAlignment.MiddleLeft;
			this.lblPBInverterWarning.BackColor = Color.Transparent;
			this.lblPBInverterWarning.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.lblPBInverterWarning.ForeColor = Color.Red;
			this.lblPBInverterWarning.Location = new Point(276, 382);
			this.lblPBInverterWarning.Name = "lblPBInverterWarning";
			this.lblPBInverterWarning.Size = new Size(478, 22);
			this.lblPBInverterWarning.TabIndex = 11;
			this.lblPBInverterWarning.Text = "No Data received. 'Force Telem' and 'Export' are disabled";
			this.lblPBInverterWarning.TextAlign = ContentAlignment.MiddleLeft;
			this.pnlPBBtns.BackColor = Color.Transparent;
			this.pnlPBBtns.BackgroundImage = Resources.BackPortNew;
			this.pnlPBBtns.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlPBBtns.Controls.Add(this.lblPBBtnsHeader);
			this.pnlPBBtns.Controls.Add(this.grbPBTelemetry);
			this.pnlPBBtns.Controls.Add(this.grbPBRecords);
			this.pnlPBBtns.Controls.Add(this.grbPBData);
			this.pnlPBBtns.Location = new Point(571, 8);
			this.pnlPBBtns.Name = "pnlPBBtns";
			this.pnlPBBtns.Size = new Size(174, 368);
			this.pnlPBBtns.TabIndex = 10;
			this.lblPBBtnsHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPBBtnsHeader.ForeColor = Color.Black;
			this.lblPBBtnsHeader.Location = new Point(15, 3);
			this.lblPBBtnsHeader.Name = "lblPBBtnsHeader";
			this.lblPBBtnsHeader.Size = new Size(146, 29);
			this.lblPBBtnsHeader.TabIndex = 6;
			this.lblPBBtnsHeader.Text = "Data Control";
			this.lblPBBtnsHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.grbPBTelemetry.Controls.Add(this.btnPBForceTelem);
			this.grbPBTelemetry.Controls.Add(this.chkPBTelemetryNormal);
			this.grbPBTelemetry.Controls.Add(this.chkPBTelemetryFast);
			this.grbPBTelemetry.Location = new Point(9, 230);
			this.grbPBTelemetry.Name = "grbPBTelemetry";
			this.grbPBTelemetry.Size = new Size(158, 92);
			this.grbPBTelemetry.TabIndex = 4;
			this.grbPBTelemetry.TabStop = false;
			this.grbPBTelemetry.Text = "Telemetry Control";
			this.btnPBForceTelem.BackColor = Color.Silver;
			this.btnPBForceTelem.Location = new Point(9, 51);
			this.btnPBForceTelem.Name = "btnPBForceTelem";
			this.btnPBForceTelem.Size = new Size(140, 34);
			this.btnPBForceTelem.TabIndex = 4;
			this.btnPBForceTelem.Text = "Force Telemetry";
			this.btnPBForceTelem.UseVisualStyleBackColor = true;
			this.btnPBForceTelem.Click += new EventHandler(this.btnPBForceTelem_Click);
			this.chkPBTelemetryNormal.Appearance = Appearance.Button;
			this.chkPBTelemetryNormal.BackColor = Color.Silver;
			this.chkPBTelemetryNormal.CheckAlign = ContentAlignment.MiddleCenter;
			this.chkPBTelemetryNormal.Checked = true;
			this.chkPBTelemetryNormal.CheckState = CheckState.Checked;
			this.chkPBTelemetryNormal.Location = new Point(10, 17);
			this.chkPBTelemetryNormal.Name = "chkPBTelemetryNormal";
			this.chkPBTelemetryNormal.Size = new Size(67, 32);
			this.chkPBTelemetryNormal.TabIndex = 8;
			this.chkPBTelemetryNormal.Text = "Normal";
			this.chkPBTelemetryNormal.TextAlign = ContentAlignment.MiddleCenter;
			this.chkPBTelemetryNormal.UseVisualStyleBackColor = true;
			this.chkPBTelemetryNormal.CheckedChanged += new EventHandler(this.chkPBTelemetryNormal_CheckedChanged);
			this.chkPBTelemetryFast.Appearance = Appearance.Button;
			this.chkPBTelemetryFast.BackColor = Color.Silver;
			this.chkPBTelemetryFast.CheckAlign = ContentAlignment.MiddleCenter;
			this.chkPBTelemetryFast.Location = new Point(80, 17);
			this.chkPBTelemetryFast.Name = "chkPBTelemetryFast";
			this.chkPBTelemetryFast.Size = new Size(71, 32);
			this.chkPBTelemetryFast.TabIndex = 9;
			this.chkPBTelemetryFast.Text = "Fast";
			this.chkPBTelemetryFast.TextAlign = ContentAlignment.MiddleCenter;
			this.chkPBTelemetryFast.UseVisualStyleBackColor = true;
			this.chkPBTelemetryFast.CheckedChanged += new EventHandler(this.chkPBTelemetryFast_CheckedChanged);
			this.grbPBRecords.Controls.Add(this.btnPBDataLoad);
			this.grbPBRecords.Controls.Add(this.btnPBRecordsExport);
			this.grbPBRecords.Location = new Point(9, 37);
			this.grbPBRecords.Name = "grbPBRecords";
			this.grbPBRecords.Size = new Size(158, 94);
			this.grbPBRecords.TabIndex = 3;
			this.grbPBRecords.TabStop = false;
			this.grbPBRecords.Text = "PowerBox Records";
			this.btnPBDataLoad.BackColor = Color.Silver;
			this.btnPBDataLoad.Location = new Point(9, 52);
			this.btnPBDataLoad.Name = "btnPBDataLoad";
			this.btnPBDataLoad.Size = new Size(140, 34);
			this.btnPBDataLoad.TabIndex = 1;
			this.btnPBDataLoad.Text = "Load List";
			this.btnPBDataLoad.UseVisualStyleBackColor = true;
			this.btnPBDataLoad.Click += new EventHandler(this.btnPBDataLoad_Click);
			this.btnPBRecordsExport.BackColor = Color.Silver;
			this.btnPBRecordsExport.Location = new Point(9, 16);
			this.btnPBRecordsExport.Name = "btnPBRecordsExport";
			this.btnPBRecordsExport.Size = new Size(140, 34);
			this.btnPBRecordsExport.TabIndex = 0;
			this.btnPBRecordsExport.Text = "Export";
			this.btnPBRecordsExport.UseVisualStyleBackColor = true;
			this.btnPBRecordsExport.Click += new EventHandler(this.btnPBRecordsExport_Click);
			this.grbPBData.Controls.Add(this.btnPBDataVersionUpdate);
			this.grbPBData.Controls.Add(this.btnPBDataAdd);
			this.grbPBData.Controls.Add(this.btnPBDataRemove);
			this.grbPBData.Location = new Point(9, 134);
			this.grbPBData.Name = "grbPBData";
			this.grbPBData.Size = new Size(158, 92);
			this.grbPBData.TabIndex = 2;
			this.grbPBData.TabStop = false;
			this.grbPBData.Text = "PowerBox Data";
			this.btnPBDataVersionUpdate.BackColor = Color.Silver;
			this.btnPBDataVersionUpdate.Enabled = false;
			this.btnPBDataVersionUpdate.Location = new Point(9, 85);
			this.btnPBDataVersionUpdate.Name = "btnPBDataVersionUpdate";
			this.btnPBDataVersionUpdate.Size = new Size(140, 34);
			this.btnPBDataVersionUpdate.TabIndex = 4;
			this.btnPBDataVersionUpdate.Text = "Update Versions";
			this.btnPBDataVersionUpdate.UseVisualStyleBackColor = true;
			this.btnPBDataVersionUpdate.Visible = false;
			this.btnPBDataVersionUpdate.Click += new EventHandler(this.btnPBDataVersionUpdate_Click);
			this.btnPBDataAdd.BackColor = Color.Silver;
			this.btnPBDataAdd.Location = new Point(9, 15);
			this.btnPBDataAdd.Name = "btnPBDataAdd";
			this.btnPBDataAdd.Size = new Size(140, 34);
			this.btnPBDataAdd.TabIndex = 3;
			this.btnPBDataAdd.Text = "Add PowerBox";
			this.btnPBDataAdd.UseVisualStyleBackColor = true;
			this.btnPBDataAdd.Click += new EventHandler(this.btnPBDataAdd_Click);
			this.btnPBDataRemove.BackColor = Color.Silver;
			this.btnPBDataRemove.Location = new Point(9, 50);
			this.btnPBDataRemove.Name = "btnPBDataRemove";
			this.btnPBDataRemove.Size = new Size(140, 34);
			this.btnPBDataRemove.TabIndex = 2;
			this.btnPBDataRemove.Text = "Remove PowerBox";
			this.btnPBDataRemove.UseVisualStyleBackColor = true;
			this.btnPBDataRemove.Click += new EventHandler(this.btnPBDataRemove_Click);
			this.lblPBLastTelemTime.BackColor = Color.Transparent;
			this.lblPBLastTelemTime.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPBLastTelemTime.ForeColor = Color.White;
			this.lblPBLastTelemTime.Location = new Point(6, 382);
			this.lblPBLastTelemTime.Name = "lblPBLastTelemTime";
			this.lblPBLastTelemTime.Size = new Size(152, 22);
			this.lblPBLastTelemTime.TabIndex = 6;
			this.lblPBLastTelemTime.Text = "Last PowerBox telemetry time:";
			this.lblPBLastTelemTime.TextAlign = ContentAlignment.MiddleLeft;
			this.pnlPBLPTop.BackColor = Color.Transparent;
			this.pnlPBLPTop.BackgroundImage = Resources.BackLandBigNew;
			this.pnlPBLPTop.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlPBLPTop.Controls.Add(this.dgvPBParam);
			this.pnlPBLPTop.Controls.Add(this.lblPBParamHeader);
			this.pnlPBLPTop.Location = new Point(6, 10);
			this.pnlPBLPTop.Name = "pnlPBLPTop";
			this.pnlPBLPTop.Size = new Size(559, 366);
			this.pnlPBLPTop.TabIndex = 0;
			this.dgvPBParam.AllowUserToAddRows = false;
			this.dgvPBParam.AllowUserToDeleteRows = false;
			this.dgvPBParam.AllowUserToResizeColumns = false;
			this.dgvPBParam.AllowUserToResizeRows = false;
			dataGridViewCellStyle24.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle24.BackColor = Color.White;
			dataGridViewCellStyle24.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle24.ForeColor = Color.Black;
			dataGridViewCellStyle24.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle24.SelectionForeColor = Color.White;
			dataGridViewCellStyle24.WrapMode = DataGridViewTriState.True;
			this.dgvPBParam.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
			this.dgvPBParam.BorderStyle = BorderStyle.Fixed3D;
			dataGridViewCellStyle25.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle25.BackColor = SystemColors.Control;
			dataGridViewCellStyle25.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle25.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle25.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle25.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle25.WrapMode = DataGridViewTriState.True;
			this.dgvPBParam.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
			this.dgvPBParam.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.dgvPBParam.Columns.AddRange(new DataGridViewColumn[]
			{
				this.dataGridViewTextBoxColumn4,
				this.colSerial,
				this.colVin,
				this.colVOut,
				this.colIIn,
				this.colEnergy,
				this.colTime,
				this.colVersion
			});
			dataGridViewCellStyle26.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle26.BackColor = SystemColors.Window;
			dataGridViewCellStyle26.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle26.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle26.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle26.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle26.WrapMode = DataGridViewTriState.True;
			this.dgvPBParam.DefaultCellStyle = dataGridViewCellStyle26;
			this.dgvPBParam.Location = new Point(2, 28);
			this.dgvPBParam.Name = "dgvPBParam";
			this.dgvPBParam.ReadOnly = true;
			this.dgvPBParam.RowHeadersVisible = false;
			this.dgvPBParam.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvPBParam.RowTemplate.DefaultCellStyle.BackColor = Color.White;
			this.dgvPBParam.RowTemplate.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.dgvPBParam.RowTemplate.DefaultCellStyle.ForeColor = Color.Black;
			this.dgvPBParam.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.Gray;
			this.dgvPBParam.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.White;
			this.dgvPBParam.RowTemplate.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvPBParam.RowTemplate.ReadOnly = true;
			this.dgvPBParam.RowTemplate.Resizable = DataGridViewTriState.False;
			this.dgvPBParam.ScrollBars = ScrollBars.Vertical;
			this.dgvPBParam.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.dgvPBParam.Size = new Size(554, 326);
			this.dgvPBParam.TabIndex = 6;
			this.dgvPBParam.Tag = "";
			dataGridViewCellStyle27.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle27.BackColor = Color.White;
			dataGridViewCellStyle27.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle27.ForeColor = Color.Black;
			dataGridViewCellStyle27.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle27.SelectionForeColor = Color.White;
			dataGridViewCellStyle27.WrapMode = DataGridViewTriState.True;
			this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle27;
			this.dataGridViewTextBoxColumn4.HeaderText = "#";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.ReadOnly = true;
			this.dataGridViewTextBoxColumn4.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn4.Width = 25;
			dataGridViewCellStyle28.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle28.BackColor = Color.White;
			dataGridViewCellStyle28.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle28.ForeColor = Color.Black;
			dataGridViewCellStyle28.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle28.SelectionForeColor = Color.White;
			dataGridViewCellStyle28.WrapMode = DataGridViewTriState.True;
			this.colSerial.DefaultCellStyle = dataGridViewCellStyle28;
			this.colSerial.HeaderText = "ID";
			this.colSerial.Name = "colSerial";
			this.colSerial.ReadOnly = true;
			this.colSerial.Resizable = DataGridViewTriState.False;
			this.colSerial.Width = 120;
			dataGridViewCellStyle29.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle29.BackColor = Color.White;
			dataGridViewCellStyle29.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle29.ForeColor = Color.Black;
			dataGridViewCellStyle29.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle29.SelectionForeColor = Color.White;
			dataGridViewCellStyle29.WrapMode = DataGridViewTriState.True;
			this.colVin.DefaultCellStyle = dataGridViewCellStyle29;
			this.colVin.HeaderText = "VIn [v]";
			this.colVin.Name = "colVin";
			this.colVin.ReadOnly = true;
			this.colVin.Resizable = DataGridViewTriState.False;
			this.colVin.Width = 80;
			dataGridViewCellStyle30.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle30.BackColor = Color.White;
			dataGridViewCellStyle30.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle30.ForeColor = Color.Black;
			dataGridViewCellStyle30.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle30.SelectionForeColor = Color.White;
			dataGridViewCellStyle30.WrapMode = DataGridViewTriState.True;
			this.colVOut.DefaultCellStyle = dataGridViewCellStyle30;
			this.colVOut.HeaderText = "VOut [v]";
			this.colVOut.Name = "colVOut";
			this.colVOut.ReadOnly = true;
			this.colVOut.Resizable = DataGridViewTriState.False;
			this.colVOut.Width = 80;
			dataGridViewCellStyle31.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle31.BackColor = Color.White;
			dataGridViewCellStyle31.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle31.ForeColor = Color.Black;
			dataGridViewCellStyle31.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle31.SelectionForeColor = Color.White;
			dataGridViewCellStyle31.WrapMode = DataGridViewTriState.True;
			this.colIIn.DefaultCellStyle = dataGridViewCellStyle31;
			this.colIIn.HeaderText = "IIn [A]";
			this.colIIn.Name = "colIIn";
			this.colIIn.ReadOnly = true;
			this.colIIn.Resizable = DataGridViewTriState.False;
			this.colIIn.Width = 60;
			dataGridViewCellStyle32.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle32.BackColor = Color.White;
			dataGridViewCellStyle32.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle32.ForeColor = Color.Black;
			dataGridViewCellStyle32.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle32.SelectionForeColor = Color.White;
			dataGridViewCellStyle32.WrapMode = DataGridViewTriState.True;
			this.colEnergy.DefaultCellStyle = dataGridViewCellStyle32;
			this.colEnergy.HeaderText = "Energy [Wh]";
			this.colEnergy.Name = "colEnergy";
			this.colEnergy.ReadOnly = true;
			this.colEnergy.Resizable = DataGridViewTriState.False;
			this.colEnergy.Visible = false;
			this.colEnergy.Width = 90;
			dataGridViewCellStyle33.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle33.BackColor = Color.White;
			dataGridViewCellStyle33.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle33.ForeColor = Color.Black;
			dataGridViewCellStyle33.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle33.SelectionForeColor = Color.White;
			dataGridViewCellStyle33.WrapMode = DataGridViewTriState.True;
			this.colTime.DefaultCellStyle = dataGridViewCellStyle33;
			this.colTime.HeaderText = "Time";
			this.colTime.Name = "colTime";
			this.colTime.ReadOnly = true;
			this.colTime.Resizable = DataGridViewTriState.False;
			this.colTime.Width = 120;
			dataGridViewCellStyle34.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle34.BackColor = Color.White;
			dataGridViewCellStyle34.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle34.ForeColor = Color.Black;
			dataGridViewCellStyle34.SelectionBackColor = Color.FromArgb(64, 64, 64);
			dataGridViewCellStyle34.SelectionForeColor = Color.White;
			dataGridViewCellStyle34.WrapMode = DataGridViewTriState.True;
			this.colVersion.DefaultCellStyle = dataGridViewCellStyle34;
			this.colVersion.HeaderText = "Version";
			this.colVersion.Name = "colVersion";
			this.colVersion.ReadOnly = true;
			this.colVersion.Resizable = DataGridViewTriState.False;
			this.colVersion.Width = 60;
			this.lblPBParamHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPBParamHeader.ForeColor = Color.Black;
			this.lblPBParamHeader.Location = new Point(15, 2);
			this.lblPBParamHeader.Name = "lblPBParamHeader";
			this.lblPBParamHeader.Size = new Size(514, 29);
			this.lblPBParamHeader.TabIndex = 5;
			this.lblPBParamHeader.Text = "Connected PowerBoxes Live Parameters";
			this.lblPBParamHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.tpTools.BackgroundImage = Resources.BackGrad;
			this.tpTools.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpTools.Controls.Add(this.tbcTools);
			this.tpTools.Location = new Point(4, 23);
			this.tpTools.Name = "tpTools";
			this.tpTools.Padding = new Padding(3);
			this.tpTools.Size = new Size(765, 415);
			this.tpTools.TabIndex = 4;
			this.tpTools.Text = "Tools";
			this.tpTools.UseVisualStyleBackColor = true;
			this.tbcTools.Appearance = TabAppearance.Buttons;
			this.tbcTools.Controls.Add(this.tpFirmwareUpgrade);
			this.tbcTools.Controls.Add(this.tpMisc);
			this.tbcTools.ItemSize = new Size(376, 21);
			this.tbcTools.Location = new Point(-1, -1);
			this.tbcTools.Name = "tbcTools";
			this.tbcTools.SelectedIndex = 0;
			this.tbcTools.Size = new Size(769, 420);
			this.tbcTools.SizeMode = TabSizeMode.Fixed;
			this.tbcTools.TabIndex = 1;
			this.tbcTools.SelectedIndexChanged += new EventHandler(this.tbcTools_SelectedIndexChanged);
			this.tpFirmwareUpgrade.BackColor = Color.Gray;
			this.tpFirmwareUpgrade.BackgroundImage = Resources.BackGrad;
			this.tpFirmwareUpgrade.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpFirmwareUpgrade.Controls.Add(this.pnlTUDetails);
			this.tpFirmwareUpgrade.Controls.Add(this.pnlTUUpdateControl);
			this.tpFirmwareUpgrade.Cursor = Cursors.Arrow;
			this.tpFirmwareUpgrade.Location = new Point(4, 25);
			this.tpFirmwareUpgrade.Name = "tpFirmwareUpgrade";
			this.tpFirmwareUpgrade.Padding = new Padding(3);
			this.tpFirmwareUpgrade.Size = new Size(761, 391);
			this.tpFirmwareUpgrade.TabIndex = 0;
			this.tpFirmwareUpgrade.Text = "Firmware Update";
			this.pnlTUDetails.BackColor = Color.Transparent;
			this.pnlTUDetails.BackgroundImage = Resources.BackPortNew;
			this.pnlTUDetails.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlTUDetails.Controls.Add(this.txtTUDetailsStatus);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsUpgradeStatusHeader);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsDSP2Value);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsDSP2);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsDSP1Value);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsCPUValue);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsDSP1);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsCPU);
			this.pnlTUDetails.Controls.Add(this.lblTUDetailsHeader);
			this.pnlTUDetails.Location = new Point(401, 10);
			this.pnlTUDetails.Name = "pnlTUDetails";
			this.pnlTUDetails.Size = new Size(340, 350);
			this.pnlTUDetails.TabIndex = 3;
			this.txtTUDetailsStatus.BackColor = SystemColors.Control;
			this.txtTUDetailsStatus.ForeColor = Color.Black;
			this.txtTUDetailsStatus.Location = new Point(15, 171);
			this.txtTUDetailsStatus.Multiline = true;
			this.txtTUDetailsStatus.Name = "txtTUDetailsStatus";
			this.txtTUDetailsStatus.ReadOnly = true;
			this.txtTUDetailsStatus.ScrollBars = ScrollBars.Vertical;
			this.txtTUDetailsStatus.Size = new Size(312, 165);
			this.txtTUDetailsStatus.TabIndex = 21;
			this.lblTUDetailsUpgradeStatusHeader.BackColor = Color.Transparent;
			this.lblTUDetailsUpgradeStatusHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblTUDetailsUpgradeStatusHeader.ForeColor = Color.Black;
			this.lblTUDetailsUpgradeStatusHeader.Location = new Point(109, 146);
			this.lblTUDetailsUpgradeStatusHeader.Name = "lblTUDetailsUpgradeStatusHeader";
			this.lblTUDetailsUpgradeStatusHeader.Size = new Size(124, 20);
			this.lblTUDetailsUpgradeStatusHeader.TabIndex = 20;
			this.lblTUDetailsUpgradeStatusHeader.Tag = "";
			this.lblTUDetailsUpgradeStatusHeader.Text = "Upgrade Status:";
			this.lblTUDetailsUpgradeStatusHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTUDetailsDSP2Value.BackColor = Color.White;
			this.lblTUDetailsDSP2Value.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUDetailsDSP2Value.ForeColor = Color.Black;
			this.lblTUDetailsDSP2Value.Location = new Point(97, 112);
			this.lblTUDetailsDSP2Value.Name = "lblTUDetailsDSP2Value";
			this.lblTUDetailsDSP2Value.Size = new Size(228, 23);
			this.lblTUDetailsDSP2Value.TabIndex = 18;
			this.lblTUDetailsDSP2Value.Tag = "";
			this.lblTUDetailsDSP2Value.Text = "1.5";
			this.lblTUDetailsDSP2Value.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTUDetailsDSP2.BackColor = Color.Transparent;
			this.lblTUDetailsDSP2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUDetailsDSP2.ForeColor = Color.Black;
			this.lblTUDetailsDSP2.Location = new Point(10, 108);
			this.lblTUDetailsDSP2.Name = "lblTUDetailsDSP2";
			this.lblTUDetailsDSP2.Size = new Size(84, 29);
			this.lblTUDetailsDSP2.TabIndex = 17;
			this.lblTUDetailsDSP2.Tag = "";
			this.lblTUDetailsDSP2.Text = "DSP2 Version:";
			this.lblTUDetailsDSP2.TextAlign = ContentAlignment.MiddleRight;
			this.lblTUDetailsDSP1Value.BackColor = Color.White;
			this.lblTUDetailsDSP1Value.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUDetailsDSP1Value.ForeColor = Color.Black;
			this.lblTUDetailsDSP1Value.Location = new Point(97, 79);
			this.lblTUDetailsDSP1Value.Name = "lblTUDetailsDSP1Value";
			this.lblTUDetailsDSP1Value.Size = new Size(228, 23);
			this.lblTUDetailsDSP1Value.TabIndex = 16;
			this.lblTUDetailsDSP1Value.Tag = "";
			this.lblTUDetailsDSP1Value.Text = "1.210.22";
			this.lblTUDetailsDSP1Value.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTUDetailsCPUValue.BackColor = Color.White;
			this.lblTUDetailsCPUValue.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUDetailsCPUValue.ForeColor = Color.Black;
			this.lblTUDetailsCPUValue.Location = new Point(97, 45);
			this.lblTUDetailsCPUValue.Name = "lblTUDetailsCPUValue";
			this.lblTUDetailsCPUValue.Size = new Size(228, 23);
			this.lblTUDetailsCPUValue.TabIndex = 15;
			this.lblTUDetailsCPUValue.Tag = "";
			this.lblTUDetailsCPUValue.Text = "1.5082";
			this.lblTUDetailsCPUValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTUDetailsDSP1.BackColor = Color.Transparent;
			this.lblTUDetailsDSP1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUDetailsDSP1.ForeColor = Color.Black;
			this.lblTUDetailsDSP1.Location = new Point(10, 75);
			this.lblTUDetailsDSP1.Name = "lblTUDetailsDSP1";
			this.lblTUDetailsDSP1.Size = new Size(84, 29);
			this.lblTUDetailsDSP1.TabIndex = 13;
			this.lblTUDetailsDSP1.Tag = "";
			this.lblTUDetailsDSP1.Text = "DSP1 Version:";
			this.lblTUDetailsDSP1.TextAlign = ContentAlignment.MiddleRight;
			this.lblTUDetailsCPU.BackColor = Color.Transparent;
			this.lblTUDetailsCPU.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUDetailsCPU.ForeColor = Color.Black;
			this.lblTUDetailsCPU.Location = new Point(10, 41);
			this.lblTUDetailsCPU.Name = "lblTUDetailsCPU";
			this.lblTUDetailsCPU.Size = new Size(84, 29);
			this.lblTUDetailsCPU.TabIndex = 11;
			this.lblTUDetailsCPU.Tag = "";
			this.lblTUDetailsCPU.Text = "CPU Version:";
			this.lblTUDetailsCPU.TextAlign = ContentAlignment.MiddleRight;
			this.lblTUDetailsHeader.BackColor = Color.Transparent;
			this.lblTUDetailsHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUDetailsHeader.ForeColor = Color.Black;
			this.lblTUDetailsHeader.Location = new Point(32, 3);
			this.lblTUDetailsHeader.Name = "lblTUDetailsHeader";
			this.lblTUDetailsHeader.Size = new Size(281, 29);
			this.lblTUDetailsHeader.TabIndex = 5;
			this.lblTUDetailsHeader.Text = "Details";
			this.lblTUDetailsHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlTUUpdateControl.BackColor = Color.Transparent;
			this.pnlTUUpdateControl.BackgroundImage = Resources.BackLandSmallNew;
			this.pnlTUUpdateControl.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlTUUpdateControl.Controls.Add(this.pbUpgradeProgress);
			this.pnlTUUpdateControl.Controls.Add(this.pbUpgradeProgressProgram);
			this.pnlTUUpdateControl.Controls.Add(this.lblUpgradeProgressProgram);
			this.pnlTUUpdateControl.Controls.Add(this.lblUpgradeProgressTotal);
			this.pnlTUUpdateControl.Controls.Add(this.btnTUUpdateControlFile);
			this.pnlTUUpdateControl.Controls.Add(this.btnTUUpdateControlBeginUpdate);
			this.pnlTUUpdateControl.Controls.Add(this.txtTUUpdateControlFile);
			this.pnlTUUpdateControl.Controls.Add(this.lblTUUpdateControlFile);
			this.pnlTUUpdateControl.Controls.Add(this.lblTUUpdateControlHeader);
			this.pnlTUUpdateControl.Location = new Point(12, 13);
			this.pnlTUUpdateControl.Name = "pnlTUUpdateControl";
			this.pnlTUUpdateControl.Size = new Size(381, 144);
			this.pnlTUUpdateControl.TabIndex = 2;
			this.pbUpgradeProgress.Location = new Point(91, 85);
			this.pbUpgradeProgress.Name = "pbUpgradeProgress";
			this.pbUpgradeProgress.Size = new Size(280, 20);
			this.pbUpgradeProgress.Step = 1;
			this.pbUpgradeProgress.Style = ProgressBarStyle.Continuous;
			this.pbUpgradeProgress.TabIndex = 18;
			this.pbUpgradeProgress.Visible = false;
			this.pbUpgradeProgressProgram.Location = new Point(91, 110);
			this.pbUpgradeProgressProgram.Name = "pbUpgradeProgressProgram";
			this.pbUpgradeProgressProgram.Size = new Size(280, 20);
			this.pbUpgradeProgressProgram.Step = 1;
			this.pbUpgradeProgressProgram.Style = ProgressBarStyle.Continuous;
			this.pbUpgradeProgressProgram.TabIndex = 17;
			this.pbUpgradeProgressProgram.Visible = false;
			this.lblUpgradeProgressProgram.BackColor = Color.Transparent;
			this.lblUpgradeProgressProgram.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblUpgradeProgressProgram.ForeColor = Color.Black;
			this.lblUpgradeProgressProgram.Location = new Point(6, 108);
			this.lblUpgradeProgressProgram.Name = "lblUpgradeProgressProgram";
			this.lblUpgradeProgressProgram.Size = new Size(84, 20);
			this.lblUpgradeProgressProgram.TabIndex = 16;
			this.lblUpgradeProgressProgram.Tag = "";
			this.lblUpgradeProgressProgram.Text = "Data writing:";
			this.lblUpgradeProgressProgram.TextAlign = ContentAlignment.MiddleRight;
			this.lblUpgradeProgressProgram.Visible = false;
			this.lblUpgradeProgressTotal.BackColor = Color.Transparent;
			this.lblUpgradeProgressTotal.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblUpgradeProgressTotal.ForeColor = Color.Black;
			this.lblUpgradeProgressTotal.Location = new Point(5, 84);
			this.lblUpgradeProgressTotal.Name = "lblUpgradeProgressTotal";
			this.lblUpgradeProgressTotal.Size = new Size(84, 20);
			this.lblUpgradeProgressTotal.TabIndex = 15;
			this.lblUpgradeProgressTotal.Tag = "";
			this.lblUpgradeProgressTotal.Text = "Total Progress:";
			this.lblUpgradeProgressTotal.TextAlign = ContentAlignment.MiddleRight;
			this.lblUpgradeProgressTotal.Visible = false;
			this.btnTUUpdateControlFile.BackColor = Color.Silver;
			this.btnTUUpdateControlFile.Cursor = Cursors.Hand;
			this.btnTUUpdateControlFile.Location = new Point(272, 44);
			this.btnTUUpdateControlFile.Name = "btnTUUpdateControlFile";
			this.btnTUUpdateControlFile.Size = new Size(102, 27);
			this.btnTUUpdateControlFile.TabIndex = 13;
			this.btnTUUpdateControlFile.Text = "Browse";
			this.btnTUUpdateControlFile.UseVisualStyleBackColor = true;
			this.btnTUUpdateControlFile.Click += new EventHandler(this.btnTUUpdateControlFile_Click);
			this.btnTUUpdateControlBeginUpdate.BackColor = Color.Silver;
			this.btnTUUpdateControlBeginUpdate.Cursor = Cursors.Hand;
			this.btnTUUpdateControlBeginUpdate.Location = new Point(136, 92);
			this.btnTUUpdateControlBeginUpdate.Name = "btnTUUpdateControlBeginUpdate";
			this.btnTUUpdateControlBeginUpdate.Size = new Size(104, 32);
			this.btnTUUpdateControlBeginUpdate.TabIndex = 12;
			this.btnTUUpdateControlBeginUpdate.Text = "Begin Update";
			this.btnTUUpdateControlBeginUpdate.UseVisualStyleBackColor = true;
			this.btnTUUpdateControlBeginUpdate.Click += new EventHandler(this.btnTUUpdateControlBeginUpdate_Click);
			this.txtTUUpdateControlFile.BackColor = Color.White;
			this.txtTUUpdateControlFile.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.txtTUUpdateControlFile.Location = new Point(91, 46);
			this.txtTUUpdateControlFile.Name = "txtTUUpdateControlFile";
			this.txtTUUpdateControlFile.ReadOnly = true;
			this.txtTUUpdateControlFile.Size = new Size(175, 22);
			this.txtTUUpdateControlFile.TabIndex = 10;
			this.txtTUUpdateControlFile.Tag = "";
			this.txtTUUpdateControlFile.TextAlign = HorizontalAlignment.Center;
			this.lblTUUpdateControlFile.BackColor = Color.Transparent;
			this.lblTUUpdateControlFile.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUUpdateControlFile.ForeColor = Color.Black;
			this.lblTUUpdateControlFile.Location = new Point(5, 43);
			this.lblTUUpdateControlFile.Name = "lblTUUpdateControlFile";
			this.lblTUUpdateControlFile.Size = new Size(84, 29);
			this.lblTUUpdateControlFile.TabIndex = 5;
			this.lblTUUpdateControlFile.Tag = "";
			this.lblTUUpdateControlFile.Text = "Upgrade File:";
			this.lblTUUpdateControlFile.TextAlign = ContentAlignment.MiddleRight;
			this.lblTUUpdateControlHeader.BackColor = Color.Transparent;
			this.lblTUUpdateControlHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTUUpdateControlHeader.ForeColor = Color.Black;
			this.lblTUUpdateControlHeader.Location = new Point(48, 0);
			this.lblTUUpdateControlHeader.Name = "lblTUUpdateControlHeader";
			this.lblTUUpdateControlHeader.Size = new Size(281, 29);
			this.lblTUUpdateControlHeader.TabIndex = 4;
			this.lblTUUpdateControlHeader.Text = "Update Control";
			this.lblTUUpdateControlHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.tpMisc.BackColor = Color.Transparent;
			this.tpMisc.BackgroundImage = Resources.BackGrad;
			this.tpMisc.BackgroundImageLayout = ImageLayout.Stretch;
			this.tpMisc.Controls.Add(this.pnlTMPwrBalance);
			this.tpMisc.Controls.Add(this.pnlTMRTC);
			this.tpMisc.Location = new Point(4, 25);
			this.tpMisc.Name = "tpMisc";
			this.tpMisc.Padding = new Padding(3);
			this.tpMisc.Size = new Size(761, 391);
			this.tpMisc.TabIndex = 2;
			this.tpMisc.Text = "Miscellaneous";
			this.pnlTMPwrBalance.BackgroundImage = (Image)componentResourceManager.GetObject("pnlTMPwrBalance.BackgroundImage");
			this.pnlTMPwrBalance.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlTMPwrBalance.Controls.Add(this.cmbTMPwrBalanceStatus);
			this.pnlTMPwrBalance.Controls.Add(this.btnTMPwrBalanceStatus);
			this.pnlTMPwrBalance.Controls.Add(this.lblTMPwrBalanceStatus);
			this.pnlTMPwrBalance.Controls.Add(this.lblTMPwrBalanceHeader);
			this.pnlTMPwrBalance.Location = new Point(380, 19);
			this.pnlTMPwrBalance.Name = "pnlTMPwrBalance";
			this.pnlTMPwrBalance.Size = new Size(349, 111);
			this.pnlTMPwrBalance.TabIndex = 2;
			this.cmbTMPwrBalanceStatus.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbTMPwrBalanceStatus.FormattingEnabled = true;
			this.cmbTMPwrBalanceStatus.Location = new Point(110, 42);
			this.cmbTMPwrBalanceStatus.Name = "cmbTMPwrBalanceStatus";
			this.cmbTMPwrBalanceStatus.Size = new Size(211, 21);
			this.cmbTMPwrBalanceStatus.TabIndex = 12;
			this.cmbTMPwrBalanceStatus.SelectedIndexChanged += new EventHandler(this.cmbTMPwrBalanceStatus_SelectedIndexChanged);
			this.btnTMPwrBalanceStatus.BackColor = Color.Silver;
			this.btnTMPwrBalanceStatus.Location = new Point(130, 70);
			this.btnTMPwrBalanceStatus.Name = "btnTMPwrBalanceStatus";
			this.btnTMPwrBalanceStatus.Size = new Size(109, 32);
			this.btnTMPwrBalanceStatus.TabIndex = 11;
			this.btnTMPwrBalanceStatus.Text = "Set Status";
			this.btnTMPwrBalanceStatus.UseVisualStyleBackColor = true;
			this.btnTMPwrBalanceStatus.Click += new EventHandler(this.btnTMPwrBalanceEnableDisable_Click);
			this.lblTMPwrBalanceStatus.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTMPwrBalanceStatus.ForeColor = Color.Black;
			this.lblTMPwrBalanceStatus.Location = new Point(36, 38);
			this.lblTMPwrBalanceStatus.Name = "lblTMPwrBalanceStatus";
			this.lblTMPwrBalanceStatus.Size = new Size(64, 29);
			this.lblTMPwrBalanceStatus.TabIndex = 5;
			this.lblTMPwrBalanceStatus.Tag = "";
			this.lblTMPwrBalanceStatus.Text = "Status:";
			this.lblTMPwrBalanceStatus.TextAlign = ContentAlignment.MiddleRight;
			this.lblTMPwrBalanceHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTMPwrBalanceHeader.ForeColor = Color.Black;
			this.lblTMPwrBalanceHeader.Location = new Point(33, 2);
			this.lblTMPwrBalanceHeader.Name = "lblTMPwrBalanceHeader";
			this.lblTMPwrBalanceHeader.Size = new Size(281, 29);
			this.lblTMPwrBalanceHeader.TabIndex = 4;
			this.lblTMPwrBalanceHeader.Text = "Power Balancing";
			this.lblTMPwrBalanceHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlTMRTC.BackgroundImage = (Image)componentResourceManager.GetObject("pnlTMRTC.BackgroundImage");
			this.pnlTMRTC.BackgroundImageLayout = ImageLayout.Stretch;
			this.pnlTMRTC.Controls.Add(this.lblTMRTCGMTCurrentValue);
			this.pnlTMRTC.Controls.Add(this.lblTMRTCGMTCurrent);
			this.pnlTMRTC.Controls.Add(this.lblTMRTCGMTOffsetValue);
			this.pnlTMRTC.Controls.Add(this.btnTMRTCSet);
			this.pnlTMRTC.Controls.Add(this.lblTMRTCGMTOffset);
			this.pnlTMRTC.Controls.Add(this.lblTMRTCHeader);
			this.pnlTMRTC.Location = new Point(22, 19);
			this.pnlTMRTC.Name = "pnlTMRTC";
			this.pnlTMRTC.Size = new Size(344, 111);
			this.pnlTMRTC.TabIndex = 1;
			this.lblTMRTCGMTCurrentValue.BackColor = Color.White;
			this.lblTMRTCGMTCurrentValue.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTMRTCGMTCurrentValue.ForeColor = Color.Black;
			this.lblTMRTCGMTCurrentValue.Location = new Point(93, 72);
			this.lblTMRTCGMTCurrentValue.Name = "lblTMRTCGMTCurrentValue";
			this.lblTMRTCGMTCurrentValue.Size = new Size(173, 23);
			this.lblTMRTCGMTCurrentValue.TabIndex = 20;
			this.lblTMRTCGMTCurrentValue.Tag = "";
			this.lblTMRTCGMTCurrentValue.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTMRTCGMTCurrent.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTMRTCGMTCurrent.ForeColor = Color.Black;
			this.lblTMRTCGMTCurrent.Location = new Point(7, 69);
			this.lblTMRTCGMTCurrent.Name = "lblTMRTCGMTCurrent";
			this.lblTMRTCGMTCurrent.Size = new Size(83, 29);
			this.lblTMRTCGMTCurrent.TabIndex = 19;
			this.lblTMRTCGMTCurrent.Tag = "";
			this.lblTMRTCGMTCurrent.Text = "Current RTC:";
			this.lblTMRTCGMTCurrent.TextAlign = ContentAlignment.MiddleRight;
			this.lblTMRTCGMTOffsetValue.BackColor = Color.White;
			this.lblTMRTCGMTOffsetValue.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTMRTCGMTOffsetValue.ForeColor = Color.Black;
			this.lblTMRTCGMTOffsetValue.Location = new Point(93, 41);
			this.lblTMRTCGMTOffsetValue.Name = "lblTMRTCGMTOffsetValue";
			this.lblTMRTCGMTOffsetValue.Size = new Size(173, 23);
			this.lblTMRTCGMTOffsetValue.TabIndex = 17;
			this.lblTMRTCGMTOffsetValue.Tag = "";
			this.lblTMRTCGMTOffsetValue.TextAlign = ContentAlignment.MiddleCenter;
			this.btnTMRTCSet.BackColor = Color.Silver;
			this.btnTMRTCSet.Location = new Point(272, 50);
			this.btnTMRTCSet.Name = "btnTMRTCSet";
			this.btnTMRTCSet.Size = new Size(62, 32);
			this.btnTMRTCSet.TabIndex = 12;
			this.btnTMRTCSet.Text = "Set RTC";
			this.btnTMRTCSet.UseVisualStyleBackColor = true;
			this.btnTMRTCSet.Click += new EventHandler(this.btnTMRTCSet_Click);
			this.lblTMRTCGMTOffset.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTMRTCGMTOffset.ForeColor = Color.Black;
			this.lblTMRTCGMTOffset.Location = new Point(7, 38);
			this.lblTMRTCGMTOffset.Name = "lblTMRTCGMTOffset";
			this.lblTMRTCGMTOffset.Size = new Size(83, 29);
			this.lblTMRTCGMTOffset.TabIndex = 6;
			this.lblTMRTCGMTOffset.Tag = "";
			this.lblTMRTCGMTOffset.Text = "GMT Offset:";
			this.lblTMRTCGMTOffset.TextAlign = ContentAlignment.MiddleRight;
			this.lblTMRTCHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblTMRTCHeader.ForeColor = Color.Black;
			this.lblTMRTCHeader.Location = new Point(33, 2);
			this.lblTMRTCHeader.Name = "lblTMRTCHeader";
			this.lblTMRTCHeader.Size = new Size(281, 29);
			this.lblTMRTCHeader.TabIndex = 4;
			this.lblTMRTCHeader.Text = "Real-Time Clock (RTC)";
			this.lblTMRTCHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlLineV.BackColor = SystemColors.Control;
			this.pnlLineV.Location = new Point(204, -4);
			this.pnlLineV.Name = "pnlLineV";
			this.pnlLineV.Size = new Size(26, 513);
			this.pnlLineV.TabIndex = 17;
			this.pnlMenuLineRight.BackColor = Color.White;
			this.pnlMenuLineRight.Location = new Point(769, 5);
			this.pnlMenuLineRight.Name = "pnlMenuLineRight";
			this.pnlMenuLineRight.Size = new Size(2, 45);
			this.pnlMenuLineRight.TabIndex = 28;
			this.pnlMenuLineRight.Tag = "";
			this.btnConnect.BackColor = Color.Transparent;
			this.btnConnect.BackgroundImageLayout = ImageLayout.Stretch;
			this.btnConnect.FlatAppearance.BorderSize = 0;
			this.btnConnect.FlatStyle = FlatStyle.Flat;
			this.btnConnect.ForeColor = Color.White;
			this.btnConnect.Image = Resources.Connect;
			this.btnConnect.Location = new Point(511, 5);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new Size(100, 49);
			this.btnConnect.TabIndex = 29;
			this.btnConnect.Tag = "";
			this.btnConnect.Text = "Connect";
			this.btnConnect.TextImageRelation = TextImageRelation.ImageAboveText;
			this.btnConnect.UseVisualStyleBackColor = false;
			this.btnConnect.Click += new EventHandler(this.btnConnect_Click);
			this.btnRefresh.BackColor = Color.Transparent;
			this.btnRefresh.BackgroundImageLayout = ImageLayout.Stretch;
			this.btnRefresh.Enabled = false;
			this.btnRefresh.FlatAppearance.BorderSize = 0;
			this.btnRefresh.FlatStyle = FlatStyle.Flat;
			this.btnRefresh.ForeColor = Color.White;
			this.btnRefresh.Image = Resources.Refresh;
			this.btnRefresh.Location = new Point(624, 5);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new Size(139, 49);
			this.btnRefresh.TabIndex = 30;
			this.btnRefresh.Tag = "";
			this.btnRefresh.Text = "Refresh";
			this.btnRefresh.TextImageRelation = TextImageRelation.ImageAboveText;
			this.btnRefresh.UseVisualStyleBackColor = false;
			this.btnRefresh.Click += new EventHandler(this.btnRefresh_Click);
			this.btnSupport.BackColor = Color.Transparent;
			this.btnSupport.BackgroundImageLayout = ImageLayout.Stretch;
			this.btnSupport.FlatAppearance.BorderSize = 0;
			this.btnSupport.FlatStyle = FlatStyle.Flat;
			this.btnSupport.ForeColor = Color.White;
			this.btnSupport.Image = Resources.Help;
			this.btnSupport.Location = new Point(776, 5);
			this.btnSupport.Name = "btnSupport";
			this.btnSupport.Size = new Size(68, 49);
			this.btnSupport.TabIndex = 35;
			this.btnSupport.Tag = "";
			this.btnSupport.Text = "Support";
			this.btnSupport.TextImageRelation = TextImageRelation.ImageAboveText;
			this.btnSupport.UseVisualStyleBackColor = false;
			this.btnSupport.Click += new EventHandler(this.btnSupport_Click);
			this.btnExit.BackColor = Color.Transparent;
			this.btnExit.BackgroundImageLayout = ImageLayout.Stretch;
			this.btnExit.FlatAppearance.BorderSize = 0;
			this.btnExit.FlatStyle = FlatStyle.Flat;
			this.btnExit.ForeColor = Color.White;
			this.btnExit.Image = Resources.Exit;
			this.btnExit.Location = new Point(919, 5);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new Size(68, 49);
			this.btnExit.TabIndex = 37;
			this.btnExit.Tag = "";
			this.btnExit.Text = "Exit";
			this.btnExit.TextImageRelation = TextImageRelation.ImageAboveText;
			this.btnExit.UseVisualStyleBackColor = false;
			this.btnExit.Click += new EventHandler(this.btnExit_Click);
			this.btnOptions.BackColor = Color.Transparent;
			this.btnOptions.BackgroundImageLayout = ImageLayout.Stretch;
			this.btnOptions.FlatAppearance.BorderSize = 0;
			this.btnOptions.FlatStyle = FlatStyle.Flat;
			this.btnOptions.ForeColor = Color.White;
			this.btnOptions.Image = Resources.Options;
			this.btnOptions.Location = new Point(848, 5);
			this.btnOptions.Name = "btnOptions";
			this.btnOptions.Size = new Size(68, 49);
			this.btnOptions.TabIndex = 36;
			this.btnOptions.Tag = "";
			this.btnOptions.Text = "Options";
			this.btnOptions.TextImageRelation = TextImageRelation.ImageAboveText;
			this.btnOptions.UseVisualStyleBackColor = false;
			this.btnOptions.Click += new EventHandler(this.btnOptions_Click);
			this.lblConnectionStatus.BackColor = Color.Transparent;
			this.lblConnectionStatus.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblConnectionStatus.ForeColor = Color.White;
			this.lblConnectionStatus.Location = new Point(418, 22);
			this.lblConnectionStatus.Name = "lblConnectionStatus";
			this.lblConnectionStatus.Size = new Size(87, 18);
			this.lblConnectionStatus.TabIndex = 32;
			this.lblConnectionStatus.Tag = "";
			this.lblConnectionStatus.Text = "OFFLINE";
			this.lblConnectionStatus.TextAlign = ContentAlignment.MiddleCenter;
			this.pnlMenuLineLeft.BackColor = Color.White;
			this.pnlMenuLineLeft.Location = new Point(616, 6);
			this.pnlMenuLineLeft.Name = "pnlMenuLineLeft";
			this.pnlMenuLineLeft.Size = new Size(2, 45);
			this.pnlMenuLineLeft.TabIndex = 27;
			this.pnlMenuLineLeft.Tag = "";
			this.lblConnectionStatusHeader.BackColor = Color.Transparent;
			this.lblConnectionStatusHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblConnectionStatusHeader.ForeColor = Color.White;
			this.lblConnectionStatusHeader.Location = new Point(236, 22);
			this.lblConnectionStatusHeader.Name = "lblConnectionStatusHeader";
			this.lblConnectionStatusHeader.Size = new Size(175, 18);
			this.lblConnectionStatusHeader.TabIndex = 38;
			this.lblConnectionStatusHeader.Tag = "";
			this.lblConnectionStatusHeader.Text = "CONNECTION   STATUS:";
			this.lblConnectionStatusHeader.TextAlign = ContentAlignment.MiddleCenter;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackGridDark;
			base.ClientSize = new Size(990, 496);
			base.Controls.Add(this.lblConnectionStatusHeader);
			base.Controls.Add(this.btnExit);
			base.Controls.Add(this.btnOptions);
			base.Controls.Add(this.btnSupport);
			base.Controls.Add(this.lblConnectionStatus);
			base.Controls.Add(this.btnRefresh);
			base.Controls.Add(this.btnConnect);
			base.Controls.Add(this.pnlMenuLineRight);
			base.Controls.Add(this.tbcMain);
			base.Controls.Add(this.pbLogo);
			base.Controls.Add(this.pnlLineV);
			base.Controls.Add(this.pnlLineH);
			base.Controls.Add(this.pnlInvListBackground);
			base.Controls.Add(this.pnlMenuLineLeft);
			base.FormBorderStyle = FormBorderStyle.Fixed3D;
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.Name = "MainForm";
			this.Text = "SolarEdge Inverter Configuration Tool v2.0";
			((ISupportInitialize)this.dgvInverterList).EndInit();
			((ISupportInitialize)this.pbLogo).EndInit();
			this.pnlInvListBackground.ResumeLayout(false);
			this.pnlInverterList.ResumeLayout(false);
			this.tbcMain.ResumeLayout(false);
			this.tpStatus.ResumeLayout(false);
			this.pnlTSRS.ResumeLayout(false);
			this.pnlTSSS.ResumeLayout(false);
			this.pnlInverterStat.ResumeLayout(false);
			((ISupportInitialize)this.lcdInverter).EndInit();
			((ISupportInitialize)this.pbLCDButton).EndInit();
			this.tpSettings.ResumeLayout(false);
			this.tbcSettings.ResumeLayout(false);
			this.tpCommSettings.ResumeLayout(false);
			this.pnlSettingsRS232.ResumeLayout(false);
			((ISupportInitialize)this.pbSettingsRS485).EndInit();
			this.pnlSettingsRS485.ResumeLayout(false);
			((ISupportInitialize)this.pbSettingsRS232).EndInit();
			this.pnlSettingsIIC.ResumeLayout(false);
			this.pnlSettingsServer.ResumeLayout(false);
			this.pnlSettingsLAN.ResumeLayout(false);
			this.pnlSettingsZB.ResumeLayout(false);
			((ISupportInitialize)this.pbSettingsLAN).EndInit();
			((ISupportInitialize)this.pbSettingsZB).EndInit();
			((ISupportInitialize)this.pbSettingsIIC).EndInit();
			((ISupportInitialize)this.pbSettingsServerCloud).EndInit();
			((ISupportInitialize)this.pbSettingsInverter).EndInit();
			this.tpRegionalSettings.ResumeLayout(false);
			this.pnlRSDSPParams.ResumeLayout(false);
			((ISupportInitialize)this.dgvVenusParams).EndInit();
			this.pnlRSCPUParams.ResumeLayout(false);
			((ISupportInitialize)this.dgvPortiaParams).EndInit();
			this.pnlRSRSTop.ResumeLayout(false);
			this.tpPBData.ResumeLayout(false);
			this.pnlPBBtns.ResumeLayout(false);
			this.grbPBTelemetry.ResumeLayout(false);
			this.grbPBRecords.ResumeLayout(false);
			this.grbPBData.ResumeLayout(false);
			this.pnlPBLPTop.ResumeLayout(false);
			((ISupportInitialize)this.dgvPBParam).EndInit();
			this.tpTools.ResumeLayout(false);
			this.tbcTools.ResumeLayout(false);
			this.tpFirmwareUpgrade.ResumeLayout(false);
			this.pnlTUDetails.ResumeLayout(false);
			this.pnlTUDetails.PerformLayout();
			this.pnlTUUpdateControl.ResumeLayout(false);
			this.pnlTUUpdateControl.PerformLayout();
			this.tpMisc.ResumeLayout(false);
			this.pnlTMPwrBalance.ResumeLayout(false);
			this.pnlTMRTC.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		public bool StatusShouldUpdatedGet(UpdateDataType type)
		{
			bool result;
			lock (this._syncObjShouldUpdate)
			{
				result = this._screenStatusShouldUpdate[(int)type];
			}
			return result;
		}

		public void StatusShouldUpdatedSet(bool updated, UpdateDataType type)
		{
			lock (this._syncObjShouldUpdate)
			{
				this._screenStatusShouldUpdate[(int)type] = updated;
			}
		}

		public void StatusShouldUpdatedSetAll(bool shouldUpdate)
		{
			lock (this._syncObjShouldUpdate)
			{
				int num = 14;
				for (int i = 0; i < num; i++)
				{
					this._screenStatusShouldUpdate[i] = shouldUpdate;
				}
			}
		}

		private MainForm()
		{
			this.InitializeComponent();
			Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
			DataManager instance = DataManager.Instance;
			this.SetupLabels();
			this.UpdateUI(this);
			this.UpdateSpecialUI();
			int num = 14;
			this._screenStatusShouldUpdate = new bool[num];
			for (int i = 0; i < num; i++)
			{
				this._screenStatusShouldUpdate[i] = false;
			}
			this._thUIAnimate = new SEThread("Main Form UI Animation Thread", new SEThread.ThreadFunctionCallBack(this.ThreadFuncAnimateUI), 500, 0u, 0u, true, true, false);
			this._thForceTelem = new SEThread("Main Form Force Telem Thread", new SEThread.ThreadFunctionCallBack(this.ThreadFuncForceTelem), 500, 0u, 1u, true, true, true);
		}

		private void HandleClosing()
		{
			if (this._thUIAnimate != null)
			{
				this._thUIAnimate.Stop = true;
				this._thUIAnimate.Dispose();
			}
			this._thUIAnimate = null;
			DataManager.Instance.Task_CloseComm();
		}

		protected void SetupLabels()
		{
			this.lblTSLCD.SetUI(false, false);
			this.lblConnectionStatusHeader.SetUI(true, true);
			this.lblConnectionStatus.SetUI(false, false);
			this.lblTSRSCountry.SetUI(true, true);
			this.lblTSRSCountryValue.SetUI(false, false);
			this.lblTSRSLanguage.SetUI(true, true);
			this.lblTSRSLanguageValue.SetUI(false, false);
			this.lblTSCSServerConfig.SetUI(true, true);
			this.lblTSCSServerConfigValue.SetUI(false, false);
			this.lblTSCSInterInverterConfig.SetUI(true, true);
			this.lblTSCSInterInverterConfigValue.SetUI(false, false);
			this.lblTSSSInverterModel.SetUI(true, true);
			this.lblTSSSInverterID.SetUI(true, true);
			this.lblTSSSCPUVersion.SetUI(true, true);
			this.lblTSSSDSP1Version.SetUI(true, true);
			this.lblTSSSDSP2Version.SetUI(true, true);
			this.lblTSSSInverterModelValue.SetUI(false, false);
			this.lblTSSSInverterIDValue.SetUI(false, false);
			this.lblTSSSCPUVersionValue.SetUI(false, false);
			this.lblTSSSDSP1VersionValue.SetUI(false, false);
			this.lblTSSSDSP2VersionValue.SetUI(false, false);
			this.lblSettingsRSRSCountry.SetUI(true, true);
			this.lblSettingsRSRSLanguage.SetUI(true, true);
			this.lblRSIPPortiaParam.SetUI(false, true);
			this.lblRSIPVenusParam.SetUI(false, true);
			this.lblSettingsRS232Header.SetUI(false, true);
			this.lblSettingsRS232CommType.SetUI(true, true);
			this.lblSettingsRS232CommTypeValue.SetUI(false, false);
			this.lblSettingsRS485Header.SetUI(false, true);
			this.lblSettingsRS485Value.SetUI(false, false);
			this.lblSettingsZBHeader.SetUI(false, true);
			this.lblSettingsZBValue.SetUI(false, false);
			this.lblSettingsLANHeader.SetUI(false, true);
			this.lblSettingsLANDHCPStatus.SetUI(true, true);
			this.lblSettingsLANDHCPStatusValue.SetUI(false, false);
			this.lblSettingsLANCurrentIP.SetUI(true, true);
			this.lblSettingsLANCurrentIPValue.SetUI(false, false);
			this.lblSettingsLANSubnetMask.SetUI(true, true);
			this.lblSettingsLANSubnetMaskValue.SetUI(false, false);
			this.lblSettingsLANGateway.SetUI(true, true);
			this.lblSettingsLANGatewayValue.SetUI(false, false);
			this.lblSettingsServerHeader.SetUI(false, true);
			this.lblSettingsServerAddress.SetUI(true, true);
			this.lblSettingsServerAddressValue.SetUI(false, false);
			this.lblSettingsServerPort.SetUI(true, true);
			this.lblSettingsServerPortValue.SetUI(false, false);
			this.lblSettingsServerVia.SetUI(true, true);
			this.lblSettingsServerViaValue.SetUI(false, false);
			this.lblSettingsIICHeader.SetUI(false, true);
			this.lblSettingsIICCommType.SetUI(true, true);
			this.lblSettingsIICCommTypeValue.SetUI(false, false);
			this.lblPBLastTelemTime.SetUI(true, true);
			this.lblPBLastTelemTimeValue.SetUI(false, false);
			this.lblPBInverterWarning.SetUI(false, false);
			this.lblTUUpdateControlHeader.SetUI(false, true);
			this.lblTUUpdateControlFile.SetUI(true, true);
			this.lblUpgradeProgressTotal.SetUI(true, true);
			this.lblUpgradeProgressProgram.SetUI(true, true);
			this.lblTUDetailsHeader.SetUI(false, true);
			this.lblTUDetailsCPU.SetUI(true, true);
			this.lblTUDetailsCPUValue.SetUI(false, false);
			this.lblTUDetailsDSP1.SetUI(true, true);
			this.lblTUDetailsDSP1Value.SetUI(false, false);
			this.lblTUDetailsDSP2.SetUI(true, true);
			this.lblTUDetailsDSP2Value.SetUI(false, false);
			this.lblTUDetailsUpgradeStatusHeader.SetUI(true, true);
			this.lblTMRTCHeader.SetUI(false, true);
			this.lblTMRTCGMTOffset.SetUI(true, true);
			this.lblTMRTCGMTOffsetValue.SetUI(false, false);
			this.lblTMPwrBalanceHeader.SetUI(false, true);
			this.lblTMPwrBalanceStatus.SetUI(true, true);
		}

		public void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		protected string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (((ISEUI)control).IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_7E)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		public void UpdateSpecialUI()
		{
			this.Text += this.ApplicationVersion.ToString(FieldEnums.BETA_ENUM, null, (VersionOptions)9);
			this.lblConnectionStatus.Text = DataManager.Instance.Dictionary[this.lblConnectionStatus.Name + "Offline"];
		}

		private void SetupCountryCombo(Inverter inverter)
		{
			if (inverter != null)
			{
				List<string> list = null;
				Zones zone = inverter.Portia.Command_Portia_Get_Zone();
				try
				{
					DeviceType deviceType = (inverter is Inverter1Phase) ? DeviceType.VENUS : DeviceType.JUPITER;
					SWVersion dspVersion = (inverter is Inverter1Phase) ? ((Inverter1Phase)inverter).Venus.DeviceSWVersion : ((Inverter3Phase)inverter).Jupiter.DeviceSWVersion;
					this._countryTable = DBParameterData.Instance.GetCountryList(dspVersion, deviceType, zone);
					if (this._countryTable == null)
					{
						return;
					}
					list = new List<string>(0);
					Dictionary<string, int>.KeyCollection.Enumerator enumerator = this._countryTable.Keys.GetEnumerator();
					while (enumerator.MoveNext())
					{
						string current = enumerator.Current;
						list.Add(current);
					}
				}
				catch (Exception var_6_C3)
				{
				}
				if (list != null)
				{
					this.cmbSettingsRSRSCountry.TSAccess.DataSource = null;
					this.cmbSettingsRSRSCountry.TSAccess.Items.Clear();
					list.Sort();
					int count = list.Count;
					for (int i = 0; i < count; i++)
					{
						this.cmbSettingsRSRSCountry.TSAccess.Items.Add(list[i]);
					}
				}
			}
		}

		private void SetupLanguageCombo(Inverter inverter)
		{
			if (inverter != null)
			{
				List<string> languageList = DBParameterData.Instance.GetLanguageList(inverter.Portia.DeviceSWVersion);
				if (languageList != null)
				{
					this._languageTable = new Dictionary<string, int>(0);
					this.cmbSettingsRSRSLanguage.TSAccess.DataSource = null;
					this.cmbSettingsRSRSLanguage.TSAccess.Items.Clear();
					int count = languageList.Count;
					try
					{
						for (int i = 0; i < count; i++)
						{
							this._languageTable.Add(languageList[i], i);
						}
					}
					catch (Exception var_3_A2)
					{
					}
					languageList.Sort();
					for (int i = 0; i < count; i++)
					{
						this.cmbSettingsRSRSLanguage.TSAccess.Items.Add(languageList[i]);
					}
				}
			}
		}

		private void SetupPowerBalanceCombo(Inverter inverter)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			this.cmbTMPwrBalanceStatus.TSAccess.DataSource = null;
			this.cmbTMPwrBalanceStatus.TSAccess.Items.Clear();
			this.cmbTMPwrBalanceStatus.TSAccess.Items.Add(dictionary["Disable"]);
			this.cmbTMPwrBalanceStatus.TSAccess.Items.Add(dictionary["Enable"]);
		}

		private void ClearData(Control control)
		{
			if (control != null)
			{
				control.Text = "";
				Control.ControlCollection controls = control.Controls;
				if (controls != null)
				{
					int count = controls.Count;
					for (int i = 0; i < count; i++)
					{
						Control control2 = controls[i];
						this.ClearData(control2);
					}
				}
			}
		}

		private void ClearScreenData()
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			this._shouldTBCMainEventFire = false;
			this.tbcMain.TSAccess.SelectedIndex = 0;
			this._shouldTBCMainEventFire = true;
			this.lblTSSSInverterIDValue.TSAccess.Text = "";
			this.lblTSSSInverterModelValue.TSAccess.Text = "";
			this.lblTSSSCPUVersionValue.TSAccess.Text = "";
			this.lblTSSSDSP1VersionValue.TSAccess.Text = "";
			this.lblTSSSDSP2VersionValue.TSAccess.Text = "";
			this.lblTSRSCountryValue.TSAccess.Text = "";
			this.lblTSRSLanguageValue.TSAccess.Text = "";
			this.lblTSCSInterInverterConfigValue.TSAccess.Text = "";
			this.lblTSCSServerConfigValue.TSAccess.Text = "";
			selectedInverter.CurrentLCDScreenState = LCDScreenStates.SCREEN_STATUS_1;
			this.lblSettingsRS232CommTypeValue.TSAccess.Text = "";
			this.lblSettingsRS485Value.TSAccess.Text = "";
			this.lblSettingsZBValue.TSAccess.Text = "";
			this.lblSettingsLANDHCPStatusValue.TSAccess.Text = "";
			this.lblSettingsLANCurrentIPValue.TSAccess.Text = "";
			this.lblSettingsLANSubnetMaskValue.TSAccess.Text = "";
			this.lblSettingsLANGatewayValue.TSAccess.Text = "";
			this.lblSettingsServerAddressValue.TSAccess.Text = "";
			this.lblSettingsServerPortValue.TSAccess.Text = "";
			this.lblSettingsServerViaValue.TSAccess.Text = "";
			this.cmbSettingsRSRSCountry.TSAccess.Items.Clear();
			this.cmbSettingsRSRSLanguage.TSAccess.Items.Clear();
			this.dgvPortiaParams.TSAccess.Rows.Clear();
			this.dgvVenusParams.TSAccess.Rows.Clear();
			this.dgvPBParam.TSAccess.Rows.Clear();
			this.txtTUUpdateControlFile.TSAccess.Text = "";
			this.lblTUDetailsCPUValue.TSAccess.Text = "";
			this.lblTUDetailsDSP1Value.TSAccess.Text = "";
			this.lblTUDetailsDSP2Value.TSAccess.Text = "";
			this.txtTUDetailsStatus.TSAccess.Text = "";
			this.lblTMRTCGMTOffsetValue.TSAccess.Text = "";
			this.ClearInverterData();
			this.ClearInverterPanel();
		}

		public void EnableUI()
		{
			this.tbcMain.TSAccess.Enabled = true;
		}

		public void SetLCDUpdate(bool isEnabled)
		{
			this.chkTSInverterPanelShouldUpdate.TSAccess.Checked = isEnabled;
		}

		private void UpdateInverterData(uint id, bool isMaster)
		{
			if (id != ProtocolMain.PROT_BROADCAST_ADDR)
			{
				if (this._inverterListLog == null)
				{
					this._inverterListLog = new Dictionary<uint, int>(0);
				}
				if (!this._inverterListLog.ContainsKey(id))
				{
					string text = string.Format("{0:x8}", id).ToUpper();
					string checkSum = Utils.GetCheckSum(text);
					string text2 = text + "-" + checkSum + (isMaster ? " (M)" : "");
					int rowIndex = this.dgvInverterList.TSAccess.Rows.Add(new object[]
					{
						id,
						text2
					});
				}
				else
				{
					int rowIndex = this._inverterListLog[id];
					string text3 = this.dgvInverterList.TSAccess.Rows[rowIndex].Cells[1].Value.ToString();
					bool flag = text3.EndsWith("(M)");
					if ((!flag || !isMaster) && (flag || isMaster))
					{
						string text = string.Format("{0:x8}", id).ToUpper();
						string checkSum = Utils.GetCheckSum(text);
						string text2 = text + "-" + checkSum + (isMaster ? " (M)" : "");
						this.dgvInverterList.TSAccess.Rows[rowIndex].Cells[1].Value = text2;
					}
				}
			}
		}

		private void ClearInverterData()
		{
			this.dgvInverterList.TSAccess.Rows.Clear();
			if (this._inverterListLog != null)
			{
				this._inverterListLog.Clear();
			}
		}

		public void UpdateInverterParameters(Dictionary<int, string[]> cpuParams, Dictionary<int, string[]> dspParams)
		{
			SEPanel sEPanel = null;
			this.pnlRSCPUParams.TSAccess.Visible = (cpuParams != null && cpuParams.Count > 0);
			this.pnlRSDSPParams.TSAccess.Visible = (dspParams != null && dspParams.Count > 0);
			if (!this.pnlRSCPUParams.TSAccess.Visible && this.pnlRSDSPParams.TSAccess.Visible)
			{
				sEPanel = this.pnlRSDSPParams;
			}
			if (this.pnlRSCPUParams.TSAccess.Visible && !this.pnlRSDSPParams.TSAccess.Visible)
			{
				sEPanel = this.pnlRSCPUParams;
			}
			if (sEPanel != null)
			{
				int width = this.tpRegionalSettings.TSAccess.Size.Width;
				int width2 = sEPanel.TSAccess.Size.Width;
				sEPanel.TSAccess.Location = new Point((width - width2) / 2, sEPanel.TSAccess.Location.Y);
			}
			else
			{
				this.pnlRSCPUParams.TSAccess.Location = new Point(9, 114);
				this.pnlRSDSPParams.TSAccess.Location = new Point(383, 113);
			}
			this.dgvPortiaParams.TSAccess.Rows.Clear();
			if (cpuParams != null && cpuParams.Count > 0)
			{
				Dictionary<int, string[]>.KeyCollection.Enumerator enumerator = cpuParams.Keys.GetEnumerator();
				while (enumerator.MoveNext())
				{
					int current = enumerator.Current;
					string[] array = cpuParams[current];
					if (array != null && array.Length == 3)
					{
						string text = array[0];
						string text2 = array[1];
						string text3 = string.IsNullOrEmpty(array[2]) ? "" : array[2];
						this.dgvPortiaParams.TSAccess.Rows.Add(new object[]
						{
							current,
							text,
							text2,
							text3
						});
					}
				}
			}
			this.dgvVenusParams.TSAccess.Rows.Clear();
			if (dspParams != null)
			{
				Dictionary<int, string[]>.KeyCollection.Enumerator enumerator2 = dspParams.Keys.GetEnumerator();
				while (enumerator2.MoveNext())
				{
					int current = enumerator2.Current;
					string[] array = dspParams[current];
					if (array != null && array.Length == 3)
					{
						string text = array[0];
						string text2 = array[1];
						string text3 = string.IsNullOrEmpty(array[2]) ? "" : array[2];
						this.dgvVenusParams.TSAccess.Rows.Add(new object[]
						{
							current,
							text,
							text2,
							text3
						});
					}
				}
			}
		}

		public void UpdatePowerBoxData(object[] values)
		{
			if (values != null && values.Length == 7)
			{
				this.UpdatePowerBoxData((string)values[0], (float)values[1], (float)values[2], (float)values[3], (float)values[4], (string)values[5], (string)values[6]);
			}
		}

		private void UpdatePowerBoxData(string panelSN, float panelV, float panelPBV, float panelI, float panelE, string timeToSet, string version)
		{
			int num = -1;
			int count = this.dgvPBParam.TSAccess.Rows.Count;
			for (int i = 0; i < count; i++)
			{
				string text = this.dgvPBParam.TSAccess.Rows[i].Cells[1].Value.ToString();
				if (text.Equals(panelSN))
				{
					num = i;
					break;
				}
			}
			if (num > -1)
			{
				this.dgvPBParam.TSAccess.Rows[num].Cells[0].Value = num + 1;
				this.dgvPBParam.TSAccess.Rows[num].Cells[1].Value = panelSN;
				this.dgvPBParam.TSAccess.Rows[num].Cells[2].Value = panelV;
				this.dgvPBParam.TSAccess.Rows[num].Cells[3].Value = panelPBV;
				this.dgvPBParam.TSAccess.Rows[num].Cells[4].Value = panelI;
				this.dgvPBParam.TSAccess.Rows[num].Cells[5].Value = panelE;
				this.dgvPBParam.TSAccess.Rows[num].Cells[6].Value = timeToSet;
				this.dgvPBParam.TSAccess.Rows[num].Cells[7].Value = version;
			}
			else
			{
				int count2 = this.dgvPBParam.TSAccess.Rows.Count;
				int num2 = this.dgvPBParam.TSAccess.Rows.Add(new object[]
				{
					count2 + 1,
					panelSN,
					panelV,
					panelPBV,
					panelI,
					panelE,
					timeToSet
				});
			}
			for (int i = 0; i < count; i++)
			{
				this.dgvPBParam.TSAccess.Rows[i].Cells[0].Value = i + 1;
			}
		}

		public void SetTelemSpeed(bool isFast)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				if (isFast)
				{
					string caption = DataManager.Instance.Dictionary["MsgPBTelemWarningCaption"];
					string text = DataManager.Instance.Dictionary["MsgPBTelemWarningMessage"];
					DialogResult dialogResult = MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
					if (dialogResult == DialogResult.Yes)
					{
						SEMercury.Command_Mercury_PLC_Set_Telem_Speed(31, selectedInverter.PLCCommandableDevice, 3u);
						PowerBoxTelemChangeMessage.ShowPowerBoxTelemChangeMessage();
					}
					else
					{
						this._shouldCheckEventNormalBeActive = false;
						this._shouldCheckEventFastBeActive = false;
						this.chkPBTelemetryNormal.Checked = true;
						this.chkPBTelemetryFast.Checked = false;
						this._shouldCheckEventNormalBeActive = true;
						this._shouldCheckEventFastBeActive = true;
					}
				}
				else
				{
					SEMercury.Command_Mercury_PLC_Set_Telem_Speed(300, selectedInverter.PLCCommandableDevice, 3u);
					this._shouldCheckEventNormalBeActive = false;
					this._shouldCheckEventFastBeActive = false;
					this.chkPBTelemetryNormal.Checked = true;
					this.chkPBTelemetryFast.Checked = false;
					this._shouldCheckEventNormalBeActive = true;
					this._shouldCheckEventFastBeActive = true;
				}
			}
		}

		private LCDScreenStates[] GetAvailableLCDScreenStates(SWVersion version)
		{
			LCDScreenStates[] array = null;
			LCDScreenStates[] result;
			if (version == null)
			{
				result = array;
			}
			else
			{
				if (version < DataManager.PORTIA_SW_VERSION_2_0023)
				{
					array = MainForm.LCD_SCREENS_1_5082;
				}
				if (version < DataManager.PORTIA_SW_VERSION_2_0038)
				{
					array = MainForm.LCD_SCREENS_2_0023;
				}
				else
				{
					array = MainForm.LCD_SCREENS_2_0038;
				}
				result = array;
			}
			return result;
		}

		private void NextScreen(Inverter inv)
		{
			SWVersion deviceSWVersion = inv.Portia.DeviceSWVersion;
			if (!(deviceSWVersion == null))
			{
				LCDScreenStates[] availableLCDScreenStates = this.GetAvailableLCDScreenStates(deviceSWVersion);
				int currentLCDScreenState = (int)inv.CurrentLCDScreenState;
				int num = currentLCDScreenState + 1;
				if (num >= availableLCDScreenStates.Length)
				{
					num = 0;
				}
				LCDScreenStates lCDScreenStates = availableLCDScreenStates[num];
				while (!this.IsNextScreenOKForDisplay(lCDScreenStates, inv))
				{
					num++;
					if (num >= availableLCDScreenStates.Length)
					{
						num = 0;
					}
					lCDScreenStates = availableLCDScreenStates[num];
				}
				inv.CurrentLCDScreenState = lCDScreenStates;
			}
		}

		private bool IsNextScreenOKForDisplay(LCDScreenStates state, Inverter inv)
		{
			bool result = true;
			if (state == LCDScreenStates.SKIP)
			{
				result = false;
			}
			return result;
		}

		private List<string> GetLCDScreenLinesByState(LCDScreenStates state, Inverter inv)
		{
			return inv.LCDLines;
		}

		public void ClearInverterPanel()
		{
			this.lcdInverter.Lines = null;
			this.lcdInverter.TSAccess.Refresh();
		}

		public void MarkInverterPanelWaitingTransmition()
		{
			List<string> lines = new List<string>(0);
			lines = Inverter.GetScreenWaiting();
			this.lcdInverter.Lines = lines;
			this.lcdInverter.Refresh();
		}

		public void MarkInverterPanelOffline()
		{
			List<string> lines = new List<string>(0);
			lines = Inverter.GetScreenWaiting();
			this.lcdInverter.Lines = lines;
			this.lcdInverter.Refresh();
		}

		private ScreenName GetScreenNameByName(string name)
		{
			ScreenName result;
			if (string.IsNullOrEmpty(name))
			{
				result = ScreenName.NONE;
			}
			else
			{
				List<string> list = MainForm.SCREEN_NAMES.ToList<string>();
				int num = list.IndexOf(name);
				if (num < 0)
				{
					result = ScreenName.NONE;
				}
				else
				{
					ScreenName screenName = (ScreenName)num;
					result = screenName;
				}
			}
			return result;
		}

		public void RefreshScreen()
		{
			this.SwitchScreen(this.SelectedScreen);
		}

		public void SwitchScreen(ScreenName tabName)
		{
			this.SwitchScreen(tabName, true);
		}

		public void SwitchScreen(ScreenName tabName, bool isOverrideRemote)
		{
			if (tabName != ScreenName.NONE)
			{
				string name = MainForm.SCREEN_NAMES[(int)tabName];
				ScreenName screenName = tabName;
				if (screenName != ScreenName.SE_TAB_PAGE_SETTINGS_NAME)
				{
					if (screenName == ScreenName.SE_TAB_PAGE_TOOLS_NAME)
					{
						name = this.tbcTools.SelectedTab.Name;
					}
				}
				else
				{
					name = this.tbcSettings.SelectedTab.Name;
				}
				tabName = this.GetScreenNameByName(name);
				this.HandleTabChanged(tabName, isOverrideRemote);
			}
		}

		private void HandleTabChanged(ScreenName tabName, bool isOverrideRemote)
		{
			MainForm._tabName = tabName;
			MainForm._isOverrideRemote = isOverrideRemote;
			this.ShowProgressBar(0, 100, new SEProgressBar.ProgressFormCallBack(this.HandleTabChangedCallback));
		}

		public void HandleTabChangedCallback()
		{
			try
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					bool flag = false;
					this._selectedScreenName = MainForm._tabName;
					switch (MainForm._tabName)
					{
					case ScreenName.SE_TAB_PAGE_STATUS_NAME:
						selectedInverter.Action_Set_LCD_Update(this.chkTSInverterPanelShouldUpdate.TSAccess.Checked, true);
						this.UpdateProgressBar("Updating...", 0f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.SPEC))
						{
							selectedInverter.UpdateStatusSpec();
						}
						this.UpdateProgressBar("Updating...", 20f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.REGIONAL_DATA))
						{
							selectedInverter.UpdateSettingsRegional();
						}
						this.UpdateProgressBar("Updating...", 40f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_RS485))
						{
							selectedInverter.UpdateSettingsCommRS485();
						}
						this.UpdateProgressBar("Updating...", 50f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_ZB))
						{
							selectedInverter.UpdateSettingsCommZigBee();
						}
						this.UpdateProgressBar("Updating...", 60f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_SERVER))
						{
							selectedInverter.UpdateSettingsCommServer();
						}
						this.UpdateProgressBar("Updating...", 80f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_IIC))
						{
							selectedInverter.UpdateSettingsCommIIC();
						}
						this.UpdateProgressBar("Updating...", 100f);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.SPEC);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.REGIONAL_DATA);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_RS485);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_ZB);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_SERVER);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_IIC);
						break;
					case ScreenName.SE_TAB_PAGE_SETTINGS_REGIONAL_NAME:
						selectedInverter.Action_Set_LCD_Update(false, false);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.REGIONAL_DATA))
						{
							selectedInverter.UpdateSettingsRegional();
							this.UpdateUIRegional(selectedInverter);
							selectedInverter.StatusUpdatedSet(true, UpdateDataType.REGIONAL_DATA);
						}
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.REGIONAL_PARAMS))
						{
							selectedInverter.Action_UpdateParameterTable(0, false);
							selectedInverter.StatusUpdatedSet(true, UpdateDataType.REGIONAL_PARAMS);
						}
						else
						{
							this.UpdateProgressBar("Updating...", 100f);
						}
						break;
					case ScreenName.SE_TAB_PAGE_SETTINGS_COMM_NAME:
						selectedInverter.Action_Set_LCD_Update(false, false);
						this.UpdateProgressBar("Updating...", 0f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_RS232))
						{
							selectedInverter.UpdateSettingsCommRS232();
						}
						this.UpdateProgressBar("Updating...", 20f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_RS485))
						{
							selectedInverter.UpdateSettingsCommRS485();
						}
						this.UpdateProgressBar("Updating...", 40f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_ZB))
						{
							selectedInverter.UpdateSettingsCommZigBee();
						}
						this.UpdateProgressBar("Updating...", 50f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_LAN))
						{
							selectedInverter.UpdateSettingsCommLAN();
						}
						this.UpdateProgressBar("Updating...", 70f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_SERVER))
						{
							selectedInverter.UpdateSettingsCommServer();
						}
						this.UpdateProgressBar("Updating...", 80f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.COMM_IIC))
						{
							selectedInverter.UpdateSettingsCommIIC();
						}
						this.UpdateProgressBar("Updating...", 100f);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_RS232);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_RS485);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_ZB);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_LAN);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_SERVER);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.COMM_IIC);
						break;
					case ScreenName.SE_TAB_PAGE_PB_NAME:
					{
						selectedInverter.Action_Set_LCD_Update(true, false);
						this.UpdateProgressBar("Updating...", 0f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.LCD))
						{
							selectedInverter.StatusUpdatedSet(true, UpdateDataType.LCD);
						}
						this.UpdateProgressBar("Updating...", 50f);
						InverterTelemetrySpeedType telemetrySpeed = selectedInverter.DataPowerBox.TelemetrySpeed;
						this.UpdateProgressBar("Updating...", 85f);
						this._shouldCheckEventNormalBeActive = false;
						this._shouldCheckEventFastBeActive = false;
						this.chkPBTelemetryNormal.Checked = (telemetrySpeed == InverterTelemetrySpeedType.NORMAL);
						this.chkPBTelemetryFast.Checked = (telemetrySpeed == InverterTelemetrySpeedType.FAST);
						this._shouldCheckEventNormalBeActive = true;
						this._shouldCheckEventFastBeActive = true;
						this.UpdateProgressBar("Updating...", 100f);
						if (this.AnimateObjects && DataManager.Instance.LoginItem.AccessLevel == AccessLevels.ADMIN)
						{
							this.grbPBTelemetry.TSAccess.Location = new Point(this.grbPBTelemetry.Location.X, this.grbPBTelemetry.Location.Y + 34);
							this.grbPBData.TSAccess.Size = new Size(this.grbPBData.Size.Width, this.grbPBData.Size.Height + 34);
							this.btnPBDataVersionUpdate.TSAccess.Visible = true;
							this.AnimateObjects = false;
						}
						break;
					}
					case ScreenName.SE_TAB_PAGE_TOOLS_UPGRADE_NAME:
						selectedInverter.Action_Set_LCD_Update(false, false);
						this.UpdateProgressBar("Updating...", 0f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.SPEC))
						{
							selectedInverter.UpdateStatusSpec();
						}
						this.UpdateProgressBar("Updating...", 30f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.REGIONAL_DATA))
						{
							selectedInverter.UpdateSettingsRegional();
						}
						this.UpdateProgressBar("Updating...", 60f);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.SPEC);
						selectedInverter.StatusUpdatedSet(true, UpdateDataType.REGIONAL_DATA);
						this.txtTUDetailsStatus.TSAccess.Text = "";
						this.UpdateProgressBar("Updating...", 100f);
						break;
					case ScreenName.SE_TAB_PAGE_TOOLS_MISC_NAME:
					{
						selectedInverter.Action_Set_LCD_Update(false, false);
						bool visible = selectedInverter is Inverter1Phase;
						this.pnlTMPwrBalance.TSAccess.Visible = visible;
						this.UpdateProgressBar("Updating...", 0f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.SPEC))
						{
							selectedInverter.UpdateStatusSpec();
						}
						this.UpdateProgressBar("Updating...", 40f);
						if (!flag && !selectedInverter.StatusUpdatedGet(UpdateDataType.REGIONAL_DATA))
						{
							selectedInverter.UpdateSettingsRegional();
						}
						this.UpdateProgressBar("Updating...", 80f);
						this.UpdateUIMiscData(selectedInverter);
						this.UpdateProgressBar("Updating...", 100f);
						DateTime rTCTime = selectedInverter.DataRegional.RTCTime;
						this.lblTMRTCGMTCurrentValue.TSAccess.Text = Utils.GetDateString(rTCTime);
						double num = (double)selectedInverter.DataRegional.GMTOffset / 3600.0;
						string text = (num >= 0.0) ? "+" : "";
						text += num.ToString();
						this.lblTMRTCGMTOffsetValue.TSAccess.Text = text;
						break;
					}
					}
				}
			}
			catch (Exception var_7_6F2)
			{
			}
			finally
			{
				this.UpdateProgressBar("Updating...", 100f);
			}
		}

		public void ShowProgressBar(int startValue, int endValue, SEProgressBar.ProgressFormCallBack callBack)
		{
			this.ShowProgressBar(startValue, endValue, callBack, this);
		}

		public void ShowProgressBar(int startValue, int endValue, SEProgressBar.ProgressFormCallBack callBack, SEForm owner)
		{
			if (this._progressBar != null)
			{
				this._progressBar.Dispose();
				this._progressBar = null;
			}
			this._progressBar = new SEProgressBar(owner, null, ImageLayout.Zoom, Resources.LightProgressBar, ImageLayout.Stretch, new Image[]
			{
				Resources.PanelRegular,
				Resources.PanelLight
			}, ImageLayout.Zoom, Resources.ProgressFormBack, ImageLayout.Zoom);
			this._progressBar.ShowProgressBar(startValue, endValue, callBack);
		}

		public void UpdateProgressBar(string text, float progress)
		{
			this._progressBar.Update(text, progress);
		}

		public void ResetProgressBar(int startValue, int endValue)
		{
			this._progressBar.ResetBarValues(startValue, endValue);
		}

		private void UpdateUIMenuBar(Inverter inverter)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			if (inverter != null)
			{
				this.lblConnectionStatus.TSAccess.Text = dictionary[this.lblConnectionStatus.TSAccess.Name + "Online"];
				this.btnConnect.TSAccess.Text = dictionary[this.btnConnect.TSAccess.Name + "Disconnect"];
				this.btnRefresh.TSAccess.Enabled = (this.SelectedScreen != ScreenName.SE_TAB_PAGE_PB_NAME);
			}
			else
			{
				this.lblConnectionStatus.TSAccess.Text = dictionary[this.lblConnectionStatus.TSAccess.Name + "Offline"];
				this.btnConnect.TSAccess.Text = dictionary[this.btnConnect.TSAccess.Name];
				this.btnRefresh.TSAccess.Enabled = false;
			}
		}

		private void UpdateUIInverterList(Inverter inverter)
		{
			if (this.UpdateInverterList)
			{
				this.UpdateInverterList = false;
				int rowIndex = 0;
				if (this.dgvInverterList.TSAccess.Rows.Count > 0)
				{
					DataGridViewSelectedCellCollection selectedCells = this.dgvInverterList.TSAccess.SelectedCells;
					if (selectedCells != null && selectedCells.Count > 0)
					{
						rowIndex = ((selectedCells[0].RowIndex >= 0) ? selectedCells[0].RowIndex : 0);
					}
				}
				this.ClearInverterData();
				List<Inverter> inverters = DataManager.Instance.Inverters;
				int count = inverters.Count;
				for (int i = 0; i < count; i++)
				{
					Inverter inverter2 = inverters[i];
					if (inverter2 != null && DataManager.Instance.Connected)
					{
						uint iD = inverter2.Portia.ID;
						this.UpdateInverterData(iD, inverter2.IsMaster);
					}
				}
				this.dgvInverterList.TSAccess.CurrentCell.Selected = false;
				this.dgvInverterList.TSAccess.Rows[rowIndex].Cells[1].Selected = true;
				this.dgvInverterList.TSAccess.CurrentCell = this.dgvInverterList.TSAccess.SelectedCells[0];
			}
		}

		private void UpdateUISpec(Inverter inverter)
		{
			if (inverter != null && inverter.DataSystem != null && inverter.DataRegional != null && inverter.DataComm != null)
			{
				this.lblTSSSInverterModelValue.TSAccess.Text = inverter.DataSystem.InverterModel;
				this.lblTSSSInverterIDValue.TSAccess.Text = inverter.DataSystem.InverterID;
				this.lblTSSSCPUVersionValue.TSAccess.Text = ((inverter.DataSystem.CPUVersion != null) ? inverter.DataSystem.CPUVersion.ToString(FieldEnums.MINOR_ENUM, null, VersionOptions.NONE) : "");
				FieldEnums upToVersionPart = (DataManager.Instance.LoginItem.AccessLevel == AccessLevels.ADMIN) ? FieldEnums.BUILD_ENUM : FieldEnums.MINOR_ENUM;
				this.lblTSSSDSP1VersionValue.TSAccess.Text = ((inverter.DataSystem.DSP1Version != null) ? inverter.DataSystem.DSP1Version.ToString(upToVersionPart, null, (VersionOptions)5) : "");
				this.lblTSSSDSP2VersionValue.TSAccess.Text = ((inverter.DataSystem.DSP2Version != null) ? inverter.DataSystem.DSP2Version.ToString(FieldEnums.MINOR_ENUM, null, VersionOptions.NONE) : "");
				inverter.DataSystem.DataChanged = false;
				this.lblTSRSCountryValue.TSAccess.Text = string.Format("{0} ({1} / {2})", inverter.DataRegional.CountryName, inverter.DataRegional.CountryShortName, inverter.DataRegional.CountryIndex);
				this.lblTSRSLanguageValue.TSAccess.Text = inverter.DataRegional.Language;
				inverter.DataSystem.DataChanged = false;
				string text = "";
				switch (inverter.DataComm.ServerCommunicationType)
				{
				case -1:
					text = "None";
					break;
				case 0:
					text = "ZigBee";
					break;
				case 1:
					text = "RS232";
					break;
				case 2:
					text = "RS485";
					break;
				case 3:
					text = "TCP";
					break;
				}
				this.lblTSCSServerConfigValue.TSAccess.Text = text;
				string text2 = "";
				int iICConnectionType = inverter.DataComm.IICConnectionType;
				switch (iICConnectionType)
				{
				case -1:
					text2 = "None";
					break;
				case 0:
				case 1:
					text2 = "ZigBee";
					break;
				case 2:
					text2 = "RS485";
					break;
				}
				if (iICConnectionType > -1)
				{
					int iICCommunicationType = inverter.DataComm.IICCommunicationType;
					bool rS485Master = inverter.DataComm.RS485Master;
					bool zigBeeMaster = inverter.DataComm.ZigBeeMaster;
					switch (iICCommunicationType)
					{
					case 0:
						if (iICConnectionType < 2)
						{
							text2 += (zigBeeMaster ? " (Master)" : " (Slave)");
						}
						else
						{
							text2 += (rS485Master ? " (Master)" : " (Slave)");
						}
						break;
					case 1:
						text2 += " (P2P)";
						break;
					}
				}
				this.lblTSCSInterInverterConfigValue.TSAccess.Text = text2;
			}
		}

		private void UpdateUILCD(Inverter inverter)
		{
			if (this.chkTSInverterPanelShouldUpdate.TSAccess.Checked)
			{
				if (inverter != null)
				{
					if (this.lcdInverter.Lines == null)
					{
						this.lcdInverter.Lines = new List<string>(0);
					}
					if (inverter.CurrentLCDStateChanged)
					{
						inverter.CurrentLCDStateChanged = false;
						this.ClearInverterPanel();
					}
					if (inverter is Inverter1Phase)
					{
						if (inverter.SystemStatus == null)
						{
							return;
						}
					}
					else if (inverter is Inverter3Phase)
					{
						Inverter3Phase inverter3Phase = (Inverter3Phase)inverter;
						if (inverter3Phase.SystemStatusJupiter == null)
						{
							return;
						}
					}
					this.lcdInverter.Lines = this.GetLCDScreenLinesByState(inverter.CurrentLCDScreenState, inverter);
				}
				else if (this.IsScreenWaitingForTransmition)
				{
					this.lcdInverter.Lines = Inverter.GetScreenWaiting();
				}
				else
				{
					this.lcdInverter.Lines = Inverter.GetScreenOffline();
				}
				this.lcdInverter.TSAccess.Refresh();
			}
			else
			{
				this.lcdInverter.Lines = Inverter.GetScreenNotUpdating();
				this.lcdInverter.TSAccess.Refresh();
			}
		}

		private void UpdateUIRegional(Inverter inverter)
		{
			if (inverter != null && inverter.DataRegional != null && inverter.DataRegional.DataChanged)
			{
				this.UpdateUIRegionalLanguage(inverter);
				this.UpdateUIRegionalCountry(inverter);
			}
		}

		private void UpdateUIRegionalLanguage(Inverter inverter)
		{
			if (this.cmbSettingsRSRSLanguage == null || this.cmbSettingsRSRSLanguage.TSAccess.Items.Count == 0)
			{
				this.SetupLanguageCombo(inverter);
			}
			string language = inverter.DataRegional.Language;
			if (!string.IsNullOrEmpty(language))
			{
				int num = this.cmbSettingsRSRSLanguage.TSAccess.Items.IndexOf(language);
				if (num < 0)
				{
					num = 0;
				}
				this.cmbSettingsRSRSLanguage.TSAccess.SelectedIndex = num;
			}
		}

		private void UpdateUIRegionalCountry(Inverter inverter)
		{
			if (this.cmbSettingsRSRSCountry == null || this.cmbSettingsRSRSCountry.TSAccess.Items.Count == 0)
			{
				this.SetupCountryCombo(inverter);
			}
			string countryName = inverter.DataRegional.CountryName;
			if (!string.IsNullOrEmpty(countryName))
			{
				int num = this.cmbSettingsRSRSCountry.TSAccess.Items.IndexOf(countryName);
				if (num >= 0)
				{
					this.cmbSettingsRSRSCountry.TSAccess.SelectedIndex = num;
				}
			}
		}

		private void UpdateUICommMain(Inverter inverter)
		{
			if (inverter != null && inverter.DataComm != null)
			{
				this.pbSettingsInverter.TSAccess.BackgroundImage = ((inverter is Inverter1Phase) ? Resources._1Inv : Resources._3Inv);
			}
		}

		private void UpdateUICommRS232(Inverter inverter)
		{
			if (inverter != null && inverter.DataComm != null)
			{
				string text = "";
				switch (inverter.DataComm.RS232ToServerConnection)
				{
				case -1:
				case 0:
					text = "Regular";
					break;
				case 1:
					text = "GSM";
					break;
				}
				this.lblSettingsRS232CommTypeValue.TSAccess.Text = text;
				this.pbSettingsRS232.TSAccess.BackgroundImage = Resources.c9PinSerialPlug;
				this.pnlSettingsRS232Line1.TSAccess.BackColor = MainForm.COLOR_CONNECTED;
				this.pnlSettingsRS232Line2.TSAccess.BackColor = MainForm.COLOR_CONNECTED;
			}
		}

		private void UpdateUICommRS485(Inverter inverter)
		{
			if (inverter != null && inverter.DataComm != null)
			{
				string text = inverter.DataComm.RS485Enabled ? "" : "Disabled";
				string text2 = inverter.DataComm.RS485Connected ? "" : "Disconnected";
				string text3 = inverter.DataComm.RS485Master ? "Master" : "Slave";
				string text4;
				if (!string.IsNullOrEmpty(text))
				{
					text4 = text;
				}
				else
				{
					text4 = (string.IsNullOrEmpty(text2) ? text3 : text2);
				}
				this.lblSettingsRS485Value.TSAccess.Text = text4;
				this.pbSettingsRS485.TSAccess.BackgroundImage = (inverter.DataComm.RS485Enabled ? Resources.RS485 : Resources.RS485Disabled);
				this.pnlSettingsRS485Line1.TSAccess.BackColor = (inverter.DataComm.RS485Enabled ? (inverter.DataComm.RS485Connected ? MainForm.COLOR_CONNECTED : MainForm.COLOR_DISCONNECTED) : MainForm.COLOR_UNAVAILABLE);
			}
		}

		private void UpdateUICommZigBee(Inverter inverter)
		{
			if (inverter != null && inverter.DataComm != null)
			{
				string text = inverter.DataComm.ZigBeeEnabled ? "" : "Disabled";
				string text2 = inverter.DataComm.ZigBeeConnected ? "" : "Disconnected";
				string text3 = inverter.DataComm.ZigBeeMaster ? "Master" : "Slave";
				string text4;
				if (!string.IsNullOrEmpty(text))
				{
					text4 = text;
				}
				else
				{
					text4 = (string.IsNullOrEmpty(text2) ? text3 : text2);
				}
				this.lblSettingsZBValue.TSAccess.Text = text4;
				this.pbSettingsZB.TSAccess.BackgroundImage = (inverter.DataComm.ZigBeeEnabled ? Resources.ZigBee : Resources.ZigBeeDisabled);
				this.pnlSettingsZBLine1.TSAccess.BackColor = (inverter.DataComm.ZigBeeEnabled ? (inverter.DataComm.ZigBeeConnected ? MainForm.COLOR_CONNECTED : MainForm.COLOR_DISCONNECTED) : MainForm.COLOR_UNAVAILABLE);
			}
		}

		private void UpdateUICommLAN(Inverter inverter)
		{
			if (inverter != null && inverter.DataComm != null)
			{
				bool lANDHCPEnabled = inverter.DataComm.LANDHCPEnabled;
				SETelemetryLANStatus sETelemetryLANStatus = lANDHCPEnabled ? inverter.DataComm.LANStatusDetected : inverter.DataComm.LANStatusParametered;
				bool flag = inverter.DataComm.LANConnectionStatusCable == 1;
				bool flag2 = inverter.DataComm.LANConnectionStatusTCP == 3u;
				string text = flag ? (lANDHCPEnabled ? "Enabled" : "Disabled") : "Data Unavailable";
				string text2 = flag ? sETelemetryLANStatus.IPStr : "Data Unavailable";
				string text3 = flag ? sETelemetryLANStatus.SubnetMaskStr : "Data Unavailable";
				string text4 = flag ? sETelemetryLANStatus.DefaultGatewayStr : "Data Unavailable";
				this.lblSettingsLANDHCPStatusValue.TSAccess.Text = text;
				this.lblSettingsLANCurrentIPValue.TSAccess.Text = text2;
				this.lblSettingsLANSubnetMaskValue.TSAccess.Text = text3;
				this.lblSettingsLANGatewayValue.TSAccess.Text = text4;
				this.pbSettingsLAN.TSAccess.BackgroundImage = (inverter.DataComm.LANEnabled ? Resources.LAN2 : Resources.LAN2Disabled);
				this.pnlSettingsLANLineEthernetCableStatus1.TSAccess.BackColor = (flag ? MainForm.COLOR_CONNECTED : MainForm.COLOR_DISCONNECTED);
				this.pnlSettingsLANLineEthernetCableStatus2.TSAccess.BackColor = this.pnlSettingsLANLineEthernetCableStatus1.TSAccess.BackColor;
				this.pnlSettingsLANLineTCPStatus.TSAccess.BackColor = (flag2 ? MainForm.COLOR_CONNECTED : MainForm.COLOR_DISCONNECTED);
			}
		}

		private void UpdateUICommServer(Inverter inverter)
		{
			if (inverter != null && inverter.DataComm != null)
			{
				bool flag = inverter.Portia.DeviceSWVersion > DataManager.PORTIA_SW_VERSION_2_0038;
				string serverAddress = inverter.DataComm.ServerAddress;
				int serverPort = inverter.DataComm.ServerPort;
				string text = "";
				switch (inverter.DataComm.ServerCommunicationType)
				{
				case -1:
					text = "None";
					break;
				case 0:
					text = "ZigBee";
					break;
				case 1:
					text = "RS232";
					break;
				case 2:
					text = "RS485";
					break;
				case 3:
					text = "TCP";
					break;
				}
				this.lblSettingsServerAddressValue.TSAccess.Text = serverAddress;
				this.lblSettingsServerPortValue.TSAccess.Text = serverPort.ToString();
				this.lblSettingsServerViaValue.TSAccess.Text = text;
				this.pbSettingsServerCloud.TSAccess.BackgroundImage = Resources.Server;
				Color backColor = Color.Black;
				S_OK_STATUS arg_11D_0 = flag ? inverter.DataComm.ServerStatus : S_OK_STATUS.UNKNOWN;
				switch (inverter.DataComm.ServerStatus)
				{
				case S_OK_STATUS.UNKNOWN:
					backColor = Color.Gray;
					break;
				case S_OK_STATUS.NOT_OK:
					backColor = Color.Red;
					break;
				case S_OK_STATUS.OK:
					backColor = Color.Lime;
					break;
				}
				this.pnlSettingsServerLine1.TSAccess.BackColor = backColor;
				this.pnlSettingsServerLine2.TSAccess.BackColor = backColor;
			}
		}

		private void UpdateUICommIIC(Inverter inverter)
		{
			if (inverter != null && inverter.DataComm != null)
			{
				string text = "";
				int iICConnectionType = inverter.DataComm.IICConnectionType;
				switch (iICConnectionType)
				{
				case -1:
					text = "None";
					break;
				case 0:
				case 1:
					text = "ZigBee";
					break;
				case 2:
					text = "RS485";
					break;
				}
				bool flag = false;
				bool flag2 = false;
				if (iICConnectionType > -1)
				{
					int iICCommunicationType = inverter.DataComm.IICCommunicationType;
					flag = inverter.DataComm.RS485Master;
					flag2 = inverter.DataComm.ZigBeeMaster;
					switch (iICCommunicationType)
					{
					case 0:
						text += ((iICConnectionType == 0 || iICConnectionType == 1) ? " / (M/S)" : "");
						break;
					case 1:
						text += " / (P2P)";
						break;
					}
				}
				this.lblSettingsIICCommTypeValue.TSAccess.Text = text;
				int num = (inverter.Slaves != null) ? inverter.Slaves.Count : 0;
				bool flag3 = iICConnectionType > -1 && num > 0;
				if (inverter is Inverter1Phase)
				{
					this.pbSettingsIIC.TSAccess.BackgroundImage = (flag3 ? Resources.Inverters : Resources.InvertersDisabled);
				}
				else if (inverter is Inverter3Phase)
				{
					this.pbSettingsIIC.TSAccess.BackgroundImage = (flag3 ? Resources.Inverters3Phase : Resources.Inverters3PhaseDisabled);
				}
				this.pnlSettingsIICLine1.TSAccess.BackColor = (flag3 ? MainForm.COLOR_CONNECTED : MainForm.COLOR_UNAVAILABLE);
				this.pnlSettingsIICLine2.TSAccess.BackColor = this.pnlSettingsIICLine1.TSAccess.BackColor;
				this.pnlSettingsIICLine3.TSAccess.BackColor = this.pnlSettingsIICLine1.TSAccess.BackColor;
				this.btnSettingsIICDetectSlaves.TSAccess.Enabled = (((iICConnectionType == 0 || iICConnectionType == 1) && flag2) || (iICConnectionType == 2 && flag));
			}
		}

		private void UpdateUIPowerBox(Inverter inverter)
		{
			if (inverter != null && inverter.DataPowerBox != null)
			{
				if (DataManager.Instance.LoginItem.AccessLevel == AccessLevels.ADMIN)
				{
					this.dgvPBParam.TSAccess.Columns["colTime"].Width = 110;
					this.dgvPBParam.TSAccess.Columns["colVersion"].Width = 60;
					this.dgvPBParam.TSAccess.Columns["colVersion"].Visible = true;
				}
				else
				{
					this.dgvPBParam.TSAccess.Columns["colTime"].Width = 170;
					this.dgvPBParam.TSAccess.Columns["colVersion"].Width = 10;
					this.dgvPBParam.TSAccess.Columns["colVersion"].Visible = false;
				}
				if (inverter.DataPowerBox.DataChanged)
				{
					DataTable dataTable = inverter.DataPowerBox.Get();
					if (dataTable != null)
					{
						int count = dataTable.Rows.Count;
						if (count <= 0)
						{
							this.dgvPBParam.TSAccess.Rows.Clear();
						}
						else
						{
							for (int i = 0; i < count; i++)
							{
								DataRow dataRow = dataTable.Rows[i];
								this.UpdatePowerBoxData(dataRow.ItemArray);
							}
						}
					}
					int lastPBIndexUpdated = inverter.DataPowerBox.LastPBIndexUpdated;
					if (lastPBIndexUpdated > -1)
					{
						int count = this.dgvPBParam.TSAccess.Rows.Count;
						for (int i = 0; i < count; i++)
						{
							this.dgvPBParam.TSAccess.Rows[i].Selected = false;
							if (i == lastPBIndexUpdated)
							{
								this.dgvPBParam.TSAccess.Rows[i].Selected = true;
							}
						}
					}
					inverter.DataPowerBox.DataChanged = false;
					this.lblPBLastTelemTimeValue.TSAccess.Text = inverter.DataPowerBox.LastTelemUpdateTime.ToString();
				}
				bool flag = this.dgvPBParam.TSAccess.Rows.Count > 0;
				bool flag2 = (inverter is Inverter1Phase) ? (inverter.SystemStatus != null && inverter.SystemStatus.OnOffSwitch == 1) : (((Inverter3Phase)inverter).SystemStatusJupiter != null && ((Inverter3Phase)inverter).SystemStatusJupiter.OnOffSwitch == 1);
				bool flag3 = DataManager.Instance.LoginItem.AccessLevel >= AccessLevels.TECHNICIAN;
				this.btnPBRecordsExport.TSAccess.Enabled = flag;
				this.btnPBForceTelem.TSAccess.Enabled = (flag && flag2 && this.EnableForceTelem);
				this.lblPBInverterWarning.TSAccess.Visible = (!flag2 || !flag);
				if (flag2 && !flag)
				{
					this.lblPBInverterWarning.TSAccess.ForeColor = Color.Yellow;
					this.lblPBInverterWarning.TSAccess.Text = DataManager.Instance.Dictionary[this.lblPBInverterWarning.TSAccess.Name + "NoData"];
				}
				else if (!flag2)
				{
					this.lblPBInverterWarning.TSAccess.ForeColor = Color.Red;
					this.lblPBInverterWarning.TSAccess.Text = DataManager.Instance.Dictionary[this.lblPBInverterWarning.TSAccess.Name + "Off"];
				}
				if (!this.EnableForceTelem)
				{
					this.btnPBForceTelem.TSAccess.Text = this.ForceTelemString;
				}
				else
				{
					this.btnPBForceTelem.TSAccess.Text = DataManager.Instance.Dictionary[this.btnPBForceTelem.TSAccess.Name];
				}
				if (!this.EnableUpdateVersion)
				{
					this.btnPBDataVersionUpdate.TSAccess.Text = this.UpdateVersionString;
				}
				else
				{
					this.btnPBDataVersionUpdate.TSAccess.Text = DataManager.Instance.Dictionary[this.btnPBDataVersionUpdate.TSAccess.Name];
				}
				this.btnPBDataRemove.TSAccess.Enabled = flag;
				this.chkPBTelemetryNormal.TSAccess.Enabled = flag2;
				this.chkPBTelemetryFast.TSAccess.Enabled = flag2;
				this.btnPBDataVersionUpdate.TSAccess.Enabled = (flag2 && flag && this.EnableUpdateVersion);
			}
		}

		private void UpdateUIUpgrade(Inverter inverter)
		{
			if (inverter != null)
			{
				this.lblTUDetailsCPUValue.TSAccess.Text = ((inverter.DataSystem.CPUVersion != null) ? inverter.DataSystem.CPUVersion.ToString(FieldEnums.MINOR_ENUM, null, VersionOptions.NONE) : "");
				FieldEnums upToVersionPart = (DataManager.Instance.LoginItem.AccessLevel == AccessLevels.ADMIN) ? FieldEnums.BUILD_ENUM : FieldEnums.MINOR_ENUM;
				this.lblTUDetailsDSP1Value.TSAccess.Text = ((inverter.DataSystem.DSP1Version != null) ? inverter.DataSystem.DSP1Version.ToString(upToVersionPart, null, (VersionOptions)5) : "");
				this.lblTUDetailsDSP2Value.TSAccess.Text = ((inverter.DataSystem.DSP2Version != null) ? inverter.DataSystem.DSP2Version.ToString(FieldEnums.MINOR_ENUM, null, VersionOptions.NONE) : "");
				string text = this.txtTUUpdateControlFile.TSAccess.Text;
				this.btnTUUpdateControlBeginUpdate.TSAccess.Enabled = (!string.IsNullOrEmpty(text) && File.Exists(text));
			}
		}

		private void UpdateUIItalyTest(Inverter inverter)
		{
			if (inverter == null)
			{
			}
		}

		private void UpdateUIMisc(Inverter inverter)
		{
			if (inverter != null)
			{
				bool flag = DataManager.Instance.LoginItem.AccessLevel == AccessLevels.ADMIN;
				this.lblTMRTCGMTCurrent.TSAccess.Visible = flag;
				this.lblTMRTCGMTCurrentValue.TSAccess.Visible = flag;
				this.lblTMRTCGMTOffset.TSAccess.Location = (flag ? new Point(7, 38) : new Point(7, 50));
				this.lblTMRTCGMTOffsetValue.TSAccess.Location = (flag ? new Point(93, 41) : new Point(93, 53));
			}
		}

		private void UpdateUIMiscData(Inverter inverter)
		{
			double num = (double)inverter.DataRegional.GMTOffset / 3600.0;
			string text = (num >= 0.0) ? "+" : "";
			text += num.ToString();
			this.lblTMRTCGMTOffsetValue.TSAccess.Text = text;
			if (this.cmbTMPwrBalanceStatus == null || this.cmbTMPwrBalanceStatus.Items.Count == 0)
			{
				this.SetupPowerBalanceCombo(inverter);
			}
			this.cmbTMPwrBalanceStatus.TSAccess.SelectedIndex = (inverter.DataSystem.PowerBalancing ? 1 : 0);
			bool flag = this.cmbTMPwrBalanceStatus.TSAccess.SelectedIndex == 1;
			this.btnTMPwrBalanceStatus.TSAccess.Enabled = (flag != inverter.DataSystem.PowerBalancing);
		}

		private void SetParametersPortia()
		{
			DataManager instance = DataManager.Instance;
			if (this.ParameterChangedPortia != null && this.ParameterChangedPortia.Count != 0)
			{
				Inverter selectedInverter = instance.GetSelectedInverter();
				int count = this.ParameterChangedPortia.Count;
				Dictionary<int, object>.KeyCollection.Enumerator enumerator = this.ParameterChangedPortia.Keys.GetEnumerator();
				List<PortiaParams> list = new List<PortiaParams>(0);
				List<object> list2 = new List<object>(0);
				while (enumerator.MoveNext())
				{
					int current = enumerator.Current;
					object item = this.ParameterChangedPortia[current];
					list.Add((PortiaParams)current);
					list2.Add(item);
				}
				selectedInverter.SetParamPortia(list.ToArray(), list2.ToArray());
			}
		}

		private void SetParametersVenus()
		{
			DataManager instance = DataManager.Instance;
			if (this.ParameterChangedVenus != null && this.ParameterChangedVenus.Count != 0)
			{
				Inverter selectedInverter = instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					Inverter1Phase inverter1Phase = (Inverter1Phase)selectedInverter;
					int count = this.ParameterChangedVenus.Count;
					Dictionary<int, object>.KeyCollection.Enumerator enumerator = this.ParameterChangedVenus.Keys.GetEnumerator();
					List<VenusParams> list = new List<VenusParams>(0);
					List<object> list2 = new List<object>(0);
					while (enumerator.MoveNext())
					{
						int current = enumerator.Current;
						object item = this.ParameterChangedVenus[current];
						list.Add((VenusParams)current);
						list2.Add(item);
					}
					inverter1Phase.SetParamVenus(list.ToArray(), list2.ToArray());
				}
			}
		}

		private void SetParametersJupiter()
		{
			DataManager instance = DataManager.Instance;
			if (this.ParameterChangedJupiter != null && this.ParameterChangedJupiter.Count != 0)
			{
				Inverter selectedInverter = instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					Inverter3Phase inverter3Phase = (Inverter3Phase)selectedInverter;
					int count = this.ParameterChangedJupiter.Count;
					Dictionary<int, object>.KeyCollection.Enumerator enumerator = this.ParameterChangedJupiter.Keys.GetEnumerator();
					List<JupiterParams> list = new List<JupiterParams>(0);
					List<object> list2 = new List<object>(0);
					while (enumerator.MoveNext())
					{
						int current = enumerator.Current;
						object item = this.ParameterChangedJupiter[current];
						list.Add((JupiterParams)current);
						list2.Add(item);
					}
					inverter3Phase.SetParamJupiter(list.ToArray(), list2.ToArray());
				}
			}
		}

		public void SetRSDataNoUpdateRequired()
		{
			this.CountryChanged = false;
			this.LanguageChanged = false;
			this.ParameterChangedPortia.Clear();
			this.ParameterChangedVenus.Clear();
			this.ParameterChangedJupiter.Clear();
		}

		public bool IsRSDataRequireUpdating()
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			bool flag = selectedInverter is Inverter1Phase;
			return this.CountryChanged || this.LanguageChanged || (this.ParameterChangedPortia != null && this.ParameterChangedPortia.Count > 0) || (flag && this.ParameterChangedVenus != null && this.ParameterChangedVenus.Count > 0) || (!flag && this.ParameterChangedJupiter != null && this.ParameterChangedJupiter.Count > 0);
		}

		public void UpdateUIInverterDataChangedMark(Inverter inverter, bool shouldMarkAsUpdateNeeded)
		{
			this.UpdateUIInverterChangedStatus(inverter.Index, shouldMarkAsUpdateNeeded);
			this.btnSettingsRSApply.TSAccess.Enabled = (this.tbcMain.TSAccess.SelectedTab.Name.Equals(MainForm.SCREEN_NAMES[1]) && this.tbcSettings.TSAccess.SelectedTab.Name.Equals(MainForm.SCREEN_NAMES[2]) && shouldMarkAsUpdateNeeded);
			this._lastRSChangedStatus = shouldMarkAsUpdateNeeded;
		}

		private void UpdateUIInverterChangedStatus(int selectedInverterIndex, bool isChanged)
		{
			this.dgvInverterList.TSAccess.Rows[selectedInverterIndex].DefaultCellStyle.Font = new Font(this.dgvInverterList.TSAccess.Rows[selectedInverterIndex].DefaultCellStyle.Font.FontFamily, this.dgvInverterList.TSAccess.Rows[selectedInverterIndex].DefaultCellStyle.Font.Size, isChanged ? FontStyle.Italic : FontStyle.Regular);
		}

		private void UpdateUIParamTableChangedStatus(DataGridView dgv, int row, bool isChanged)
		{
			DataGridViewCellStyle defaultCellStyle = dgv.Rows[row].DefaultCellStyle;
			defaultCellStyle.ForeColor = (isChanged ? Color.Blue : Color.Black);
			defaultCellStyle.Font = new Font(defaultCellStyle.Font.FontFamily, defaultCellStyle.Font.Size, isChanged ? FontStyle.Bold : FontStyle.Regular);
		}

		private void ThreadFuncAnimateUI()
		{
			if (!this.PauseMonitoring)
			{
				try
				{
					this.AnimateUI();
				}
				catch (Exception var_0_1B)
				{
				}
			}
		}

		private void AnimateUI()
		{
			try
			{
				Application.DoEvents();
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				ScreenName screenNameByName = this.GetScreenNameByName(this.tbcMain.TSAccess.SelectedTab.Name);
				if (selectedInverter != null && !selectedInverter.DataReady)
				{
					this.ClearScreenData();
				}
				else
				{
					this.UpdateUIMenuBar(selectedInverter);
					this.UpdateUIInverterList(selectedInverter);
					switch (screenNameByName)
					{
					case ScreenName.SE_TAB_PAGE_STATUS_NAME:
						this.UpdateUISpec(selectedInverter);
						this.UpdateUILCD(selectedInverter);
						this.UpdateUIRegional(selectedInverter);
						this.UpdateUICommRS485(selectedInverter);
						this.UpdateUICommZigBee(selectedInverter);
						this.UpdateUICommServer(selectedInverter);
						this.UpdateUICommIIC(selectedInverter);
						this.chkTSInverterPanelShouldUpdate.TSAccess.Visible = (DataManager.Instance.LoginItem.AccessLevel == AccessLevels.ADMIN);
						break;
					case ScreenName.SE_TAB_PAGE_SETTINGS_NAME:
						switch (this.GetScreenNameByName(this.tbcSettings.TSAccess.SelectedTab.Name))
						{
						case ScreenName.SE_TAB_PAGE_SETTINGS_REGIONAL_NAME:
						{
							bool flag = this.IsRSDataRequireUpdating();
							this.btnSettingsRSApply.TSAccess.Enabled = flag;
							this.UpdateUIInverterDataChangedMark(selectedInverter, flag);
							break;
						}
						case ScreenName.SE_TAB_PAGE_SETTINGS_COMM_NAME:
							this.UpdateUICommMain(selectedInverter);
							this.UpdateUICommRS232(selectedInverter);
							this.UpdateUICommRS485(selectedInverter);
							this.UpdateUICommZigBee(selectedInverter);
							this.UpdateUICommLAN(selectedInverter);
							this.UpdateUICommServer(selectedInverter);
							this.UpdateUICommIIC(selectedInverter);
							break;
						}
						break;
					case ScreenName.SE_TAB_PAGE_PB_NAME:
						this.UpdateUIPowerBox(selectedInverter);
						break;
					case ScreenName.SE_TAB_PAGE_TOOLS_NAME:
						switch (this.GetScreenNameByName(this.tbcTools.TSAccess.SelectedTab.Name))
						{
						case ScreenName.SE_TAB_PAGE_TOOLS_UPGRADE_NAME:
							this.UpdateUIUpgrade(selectedInverter);
							break;
						case ScreenName.SE_TAB_PAGE_TOOLS_MISC_NAME:
							this.UpdateUIMisc(selectedInverter);
							break;
						}
						break;
					}
				}
			}
			catch (Exception var_5_1D6)
			{
			}
		}

		private void MarkCursor(bool isWorking)
		{
			this.MarkCursor(isWorking, Cursors.Arrow);
		}

		private void MarkCursor(bool isWorking, Cursor defaultCursor)
		{
			base.TSAccess.Cursor = (isWorking ? Cursors.WaitCursor : defaultCursor);
		}

		private void ThreadFuncForceTelem()
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			string text = dictionary["msgThreadPBDataForceTelemButtonMessage"];
			this.MarkCursor(true);
			this.EnableForceTelem = false;
			this.ForceTelemString = text;
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				int num = (selectedInverter.DataPowerBox.MercuryList != null) ? selectedInverter.DataPowerBox.MercuryList.Count : 0;
				for (int i = 0; i < num; i++)
				{
					SEMercury sEMercury = selectedInverter.DataPowerBox.MercuryList[i];
					sEMercury.Command_Mercury_PLC_Force_Telemetry();
					this.ForceTelemString = text + " " + (i + 1).ToString();
					Thread.Sleep(2000);
				}
				this.EnableForceTelem = true;
				this._thForceTelem.Pause = true;
				this.MarkCursor(false, Cursors.Hand);
			}
		}

		public void NotifyStart(int startState, int endState, int startProgressData, int endProgressData)
		{
			this.btnTUUpdateControlFile.TSAccess.Enabled = false;
			this.btnTUUpdateControlBeginUpdate.TSAccess.Visible = false;
			this.lblUpgradeProgressTotal.TSAccess.Visible = true;
			this.pbUpgradeProgress.TSAccess.Minimum = startState;
			this.pbUpgradeProgress.TSAccess.Maximum = endState;
			this.pbUpgradeProgress.TSAccess.Value = startState;
			this.pbUpgradeProgress.TSAccess.Visible = true;
			this.lblUpgradeProgressProgram.TSAccess.Visible = true;
			this.pbUpgradeProgressProgram.TSAccess.Minimum = startState;
			this.pbUpgradeProgressProgram.TSAccess.Maximum = endState;
			this.pbUpgradeProgressProgram.TSAccess.Value = startState;
			this.pbUpgradeProgressProgram.TSAccess.Visible = true;
			this.PauseMonitoring = true;
		}

		public void UpdateState(string text, int progress)
		{
			if (!string.IsNullOrEmpty(text))
			{
				SETextBoxThreadSafeAccess expr_17 = this.txtTUDetailsStatus.TSAccess;
				expr_17.Text = expr_17.Text + "\r\n" + text;
				this.txtTUDetailsStatus.TSAccess.SelectionStart = this.txtTUDetailsStatus.TSAccess.Text.Length;
				this.txtTUDetailsStatus.TSAccess.ScrollToCaret();
			}
			if (progress >= this.pbUpgradeProgress.TSAccess.Minimum && progress <= this.pbUpgradeProgress.TSAccess.Maximum)
			{
				this.pbUpgradeProgress.TSAccess.Value = progress;
				this.pbUpgradeProgress.TSAccess.Refresh();
				Application.DoEvents();
				if (this.pbUpgradeProgress.TSAccess.Value == this.pbUpgradeProgress.TSAccess.Maximum)
				{
					Thread.Sleep(1000);
				}
			}
		}

		public void UpdateWritingData(int progress)
		{
			this.pbUpgradeProgressProgram.TSAccess.Value = progress;
			this.pbUpgradeProgressProgram.TSAccess.Refresh();
			Application.DoEvents();
		}

		public void NotifyFinished(string message)
		{
			this.UpdateProgressBar("", 100f);
			if (!string.IsNullOrEmpty(message))
			{
				MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
			}
			this.btnTUUpdateControlFile.TSAccess.Enabled = true;
			this.btnTUUpdateControlBeginUpdate.TSAccess.Visible = true;
			this.lblUpgradeProgressTotal.TSAccess.Visible = false;
			this.pbUpgradeProgress.TSAccess.Minimum = 0;
			this.pbUpgradeProgress.TSAccess.Maximum = 1;
			this.pbUpgradeProgress.TSAccess.Value = 0;
			this.pbUpgradeProgress.TSAccess.Visible = false;
			this.lblUpgradeProgressProgram.TSAccess.Visible = false;
			this.pbUpgradeProgressProgram.TSAccess.Minimum = 0;
			this.pbUpgradeProgressProgram.TSAccess.Maximum = 1;
			this.pbUpgradeProgressProgram.TSAccess.Value = 0;
			this.pbUpgradeProgressProgram.TSAccess.Visible = false;
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			selectedInverter.StatusUpdatedSet(false, UpdateDataType.SPEC);
			selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_DATA);
			this.SwitchScreen(this.SelectedScreen, true);
			this.PauseMonitoring = false;
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			this.tbcMain.Enabled = false;
			DataManager instance = DataManager.Instance;
			instance.UIOwner = this;
			object obj = instance.UserSettings["ShowConnectionFormOnStart"];
			bool flag = obj == null || (bool)obj;
			if (flag)
			{
				ConnectionForm.Instance.BarShow();
			}
			base.OnShown(e);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this.HandleClosing();
			base.OnClosing(e);
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			DataManager instance = DataManager.Instance;
			Inverter selectedInverter = instance.GetSelectedInverter();
			bool flag = selectedInverter != null && selectedInverter.DataComm.Connected;
			if (flag)
			{
				DataManager.Instance.Disconnect();
				this.ClearScreenData();
				this.tbcMain.Enabled = false;
			}
			else
			{
				ConnectionForm.Instance.BarShow();
			}
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			this.UpdateInverterList = true;
			ScreenName screenNameByName = this.GetScreenNameByName(this.tbcMain.SelectedTab.Name);
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			switch (screenNameByName)
			{
			case ScreenName.SE_TAB_PAGE_STATUS_NAME:
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.SPEC);
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_DATA);
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_RS485);
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_ZB);
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_SERVER);
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_IIC);
				break;
			case ScreenName.SE_TAB_PAGE_SETTINGS_NAME:
				switch (this.GetScreenNameByName(this.tbcSettings.SelectedTab.Name))
				{
				case ScreenName.SE_TAB_PAGE_SETTINGS_REGIONAL_NAME:
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_DATA);
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_PARAMS);
					break;
				case ScreenName.SE_TAB_PAGE_SETTINGS_COMM_NAME:
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_RS232);
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_RS485);
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_ZB);
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_LAN);
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_SERVER);
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.COMM_IIC);
					break;
				}
				break;
			case ScreenName.SE_TAB_PAGE_PB_NAME:
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.PB);
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.LCD);
				break;
			case ScreenName.SE_TAB_PAGE_TOOLS_NAME:
				switch (this.GetScreenNameByName(this.tbcTools.SelectedTab.Name))
				{
				case ScreenName.SE_TAB_PAGE_TOOLS_UPGRADE_NAME:
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.SPEC);
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_DATA);
					break;
				case ScreenName.SE_TAB_PAGE_TOOLS_MISC_NAME:
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_DATA);
					break;
				}
				break;
			}
			this.SwitchScreen(screenNameByName, true);
		}

		private void btnSupport_Click(object sender, EventArgs e)
		{
			SupportForm.ShowSupportForm(this);
		}

		private void btnOptions_Click(object sender, EventArgs e)
		{
			bool flag = false;
			switch (this.SelectedScreen)
			{
			case ScreenName.SE_TAB_PAGE_SETTINGS_REGIONAL_NAME:
			case ScreenName.SE_TAB_PAGE_PB_NAME:
				flag = true;
				break;
			}
			OptionsForm.ShowOptionsForm(this, flag);
			Application.DoEvents();
			if (flag)
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					selectedInverter.StatusUpdatedSetAll(false);
					this.SwitchScreen(MainForm.Instance.SelectedScreen);
				}
			}
		}

		private void btnExit_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void tbcMain_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this._shouldTBCMainEventFire)
			{
				SETabControl sETabControl = (SETabControl)sender;
				SETabPage sETabPage = (SETabPage)sETabControl.SelectedTab;
				string name = sETabPage.Name;
				ScreenName screenNameByName = this.GetScreenNameByName(name);
				this.SwitchScreen(screenNameByName);
			}
		}

		private void tbcSettings_SelectedIndexChanged(object sender, EventArgs e)
		{
			TabControl tabControl = (TabControl)sender;
			TabPage selectedTab = tabControl.SelectedTab;
			string name = selectedTab.Name;
			ScreenName screenNameByName = this.GetScreenNameByName(name);
			this.SwitchScreen(screenNameByName);
		}

		private void tbcTools_SelectedIndexChanged(object sender, EventArgs e)
		{
			TabControl tabControl = (TabControl)sender;
			TabPage selectedTab = tabControl.SelectedTab;
			string name = selectedTab.Name;
			ScreenName screenNameByName = this.GetScreenNameByName(name);
			this.SwitchScreen(screenNameByName);
		}

		private void dgvInverterList_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			int selectedInverterIndex = DataManager.Instance.SelectedInverterIndex;
			DataManager.Instance.SelectedInverterIndex = e.RowIndex;
			string name = this.tbcMain.SelectedTab.Name;
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter == null)
			{
				DataManager.Instance.SelectedInverterIndex = selectedInverterIndex;
			}
			else
			{
				selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_DATA);
				this.dgvPBParam.Rows.Clear();
				selectedInverter.DataPowerBox.DataChanged = true;
				ScreenName screenNameByName = this.GetScreenNameByName(name);
				this.SwitchScreen(screenNameByName, true);
			}
		}

		private void pbLCDButton_MouseDown(object sender, MouseEventArgs e)
		{
			SEPictureBox sEPictureBox = (SEPictureBox)sender;
			sEPictureBox.BackgroundImage = Resources.buttonOn;
		}

		private void pbLCDButton_MouseUp(object sender, MouseEventArgs e)
		{
			SEPictureBox sEPictureBox = (SEPictureBox)sender;
			sEPictureBox.BackgroundImage = Resources.buttonOff;
		}

		private void pbLCDButton_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				SWVersion deviceSWVersion = selectedInverter.Portia.DeviceSWVersion;
				this.NextScreen(selectedInverter);
			}
		}

		private void btnSettingsRSApply_Click(object sender, EventArgs e)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			if (this.IsRSDataRequireUpdating())
			{
				this.Cursor = Cursors.WaitCursor;
				SECommDevice sECommDevice = null;
				uint delay = 0u;
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				bool flag = selectedInverter is Inverter1Phase;
				bool flag2 = false;
				if (selectedInverter != null)
				{
					bool flag3 = false;
					bool flag4 = false;
					bool flag5 = false;
					bool flag6 = false;
					string text = (this.cmbSettingsRSRSLanguage.SelectedItem != null) ? this.cmbSettingsRSRSLanguage.SelectedItem.ToString() : null;
					string text2 = (this.cmbSettingsRSRSCountry.SelectedItem != null) ? this.cmbSettingsRSRSCountry.SelectedItem.ToString() : null;
					int num = (!string.IsNullOrEmpty(text)) ? this._languageTable[text] : -1;
					int num2 = (!string.IsNullOrEmpty(text2)) ? this._countryTable[text2] : -1;
					if (this.LanguageChanged && num > -1)
					{
						selectedInverter.SetParamPortia(PortiaParams.SETUP_LANGUAGE, (uint)num);
						flag2 = true;
						this.LanguageChanged = false;
						flag4 = true;
						sECommDevice = selectedInverter.Portia;
					}
					if (this.CountryChanged && num2 > -1)
					{
						flag2 = false;
						selectedInverter.Action_Set_Country((uint)num2);
						this.CountryChanged = false;
						flag3 = true;
					}
					if (this.ParameterChangedPortia != null && this.ParameterChangedPortia.Count > 0)
					{
						this.SetParametersPortia();
						this.ParameterChangedPortia.Clear();
						flag2 = true;
						flag5 = true;
						sECommDevice = selectedInverter.Portia;
						delay = 200u;
						flag6 = true;
					}
					if (flag && this.ParameterChangedVenus != null && this.ParameterChangedVenus.Count > 0)
					{
						this.SetParametersVenus();
						this.ParameterChangedVenus.Clear();
						flag2 = true;
						flag5 = true;
						sECommDevice = ((Inverter1Phase)selectedInverter).Venus;
						delay = 500u;
						flag6 = true;
					}
					if (!flag && this.ParameterChangedJupiter != null && this.ParameterChangedJupiter.Count > 0)
					{
						this.SetParametersJupiter();
						this.ParameterChangedJupiter.Clear();
						flag2 = true;
						flag5 = true;
						sECommDevice = ((Inverter3Phase)selectedInverter).Jupiter;
						delay = 500u;
						flag6 = true;
					}
					if (flag2)
					{
						sECommDevice.Command_Device_Reset(delay);
					}
					selectedInverter.UpdateSettingsRegional();
					if (flag6)
					{
						selectedInverter.StatusUpdatedSetAll(false);
					}
					else
					{
						selectedInverter.StatusUpdatedSet(false, UpdateDataType.REGIONAL_DATA);
					}
					string text3 = dictionary["msgParamUpdateDoneMessage"] + ".";
					if (flag4)
					{
						string text4 = text3;
						text3 = string.Concat(new string[]
						{
							text4,
							"\n\n",
							dictionary["msgParamUpdateNewLanguageLine"],
							" ",
							(this.cmbSettingsRSRSLanguage.SelectedItem != null) ? this.cmbSettingsRSRSLanguage.SelectedItem.ToString() : "",
							"."
						});
					}
					if (flag3)
					{
						string text4 = text3;
						text3 = string.Concat(new string[]
						{
							text4,
							"\n\n",
							dictionary["msgParamUpdateNewCountryLine"],
							" ",
							(this.cmbSettingsRSRSCountry.SelectedItem != null) ? this.cmbSettingsRSRSCountry.SelectedItem.ToString() : "",
							"."
						});
					}
					if (flag5)
					{
						text3 = text3 + "\n\n" + dictionary["msgParamUpdateNewParamsLine"] + ".";
					}
					this.Cursor = Cursors.Arrow;
					MessageBox.Show(text3, dictionary["msgParamUpdateDoneCaption"], MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
					if (flag6)
					{
						this.RefreshScreen();
					}
				}
			}
		}

		private void cmbSettingsRSRSLanguage_SelectedIndexChanged(object sender, EventArgs e)
		{
			SEComboBox sEComboBox = (SEComboBox)sender;
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				string text = (sEComboBox.SelectedItem != null) ? sEComboBox.SelectedItem.ToString() : null;
				string language = selectedInverter.DataRegional.Language;
				this.LanguageChanged = (text != null && !text.Equals(language));
			}
		}

		private void cmbSettingsRSRSCountry_SelectedIndexChanged(object sender, EventArgs e)
		{
			SEComboBox sEComboBox = (SEComboBox)sender;
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				string text = (sEComboBox.SelectedItem != null) ? sEComboBox.SelectedItem.ToString() : null;
				string countryName = selectedInverter.DataRegional.CountryName;
				this.CountryChanged = (text != null && !text.Equals(countryName));
			}
		}

		private void dgvPortiaParams_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				this.HandleDataChanged(this.dgvPortiaParams, selectedInverter.Portia.ParameterTableData, e.RowIndex, e.ColumnIndex, this.ParameterChangedPortia, DeviceType.PORTIA);
			}
			catch (Exception var_1_3B)
			{
			}
		}

		private void dgvPortiaParams_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				DataManager instance = DataManager.Instance;
				Inverter selectedInverter = instance.GetSelectedInverter();
				Dictionary<int, DataRow> parameterTableData = selectedInverter.Portia.ParameterTableData;
				this.HandleDataAccess(this.dgvPortiaParams, e.RowIndex, e.ColumnIndex, parameterTableData);
			}
			catch (Exception var_3_38)
			{
			}
		}

		private void dgvVenusParams_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				bool flag = selectedInverter is Inverter1Phase;
				DeviceType device = flag ? DeviceType.VENUS : DeviceType.JUPITER;
				Dictionary<int, object> paramChangedTable = flag ? this.ParameterChangedVenus : this.ParameterChangedJupiter;
				Dictionary<int, DataRow> paramTable = flag ? ((Inverter1Phase)selectedInverter).Venus.ParameterTableData : ((Inverter3Phase)selectedInverter).Jupiter.ParameterTableData;
				this.HandleDataChanged(this.dgvVenusParams, paramTable, e.RowIndex, e.ColumnIndex, paramChangedTable, device);
			}
			catch (Exception var_5_78)
			{
			}
		}

		private void dgvVenusParams_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					Dictionary<int, DataRow> paramTable = (selectedInverter is Inverter1Phase) ? ((Inverter1Phase)selectedInverter).Venus.ParameterTableData : ((Inverter3Phase)selectedInverter).Jupiter.ParameterTableData;
					this.HandleDataAccess(this.dgvVenusParams, e.RowIndex, e.ColumnIndex, paramTable);
				}
			}
			catch (Exception var_2_62)
			{
			}
		}

		private void HandleDataAccess(DataGridView dgv, int row, int column, Dictionary<int, DataRow> paramTable)
		{
			if (row >= 0 && row < dgv.Rows.Count && column == 2)
			{
				DataGridViewCell dataGridViewCell = dgv[0, row];
				DataGridViewCell dataGridViewCell2 = dgv[column, row];
				int key = int.Parse(dataGridViewCell.Value.ToString());
				DataRow dataRow = paramTable[key];
				if (dataRow != null)
				{
					object obj = dataRow.ItemArray[0];
					object obj2 = dataRow.ItemArray[7];
					int num = (obj != null) ? int.Parse(obj.ToString()) : -1;
					if (num >= 0)
					{
						int num2 = (obj2 != null) ? int.Parse(obj2.ToString()) : 2;
						int accessLevel = (int)DataManager.Instance.LoginItem.AccessLevel;
						dataGridViewCell2.ReadOnly = (accessLevel < num2);
					}
				}
			}
		}

		private void HandleDataChanged(DataGridView dgv, Dictionary<int, DataRow> paramTable, int row, int column, Dictionary<int, object> paramChangedTable, DeviceType device)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			if (dgv != null && paramTable != null && row >= 0 && column == 2)
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					DataGridViewCell dataGridViewCell = dgv[0, row];
					DataGridViewCell dataGridViewCell2 = dgv[column, row];
					object value = dataGridViewCell.Value;
					object value2 = dataGridViewCell2.Value;
					int key = int.Parse(value.ToString());
					string text = value2.ToString();
					DataRow dataRow = paramTable[key];
					if (dataRow != null)
					{
						object[] itemArray = dataRow.ItemArray;
						int num = int.Parse(itemArray[0].ToString());
						string text2 = itemArray[3].ToString();
						Type type = typeof(string);
						switch (device)
						{
						case DeviceType.PORTIA:
							type = DBParameterData.Instance.GetTypeOfParamPortia((ushort)num);
							break;
						case DeviceType.VENUS:
							type = DBParameterData.Instance.GetTypeOfParamVenus((ushort)num);
							break;
						case DeviceType.MERCURY:
							type = DBParameterData.Instance.GetTypeOfParamMercury((ushort)num);
							break;
						case DeviceType.JUPITER:
							type = DBParameterData.Instance.GetTypeOfParamJupiter((ushort)num);
							break;
						case DeviceType.GEMINI:
							type = DBParameterData.Instance.GetTypeOfParamGemini((ushort)num);
							break;
						}
						ParamType paramTypeByType = SEParam.GetParamTypeByType(type);
						bool flag = false;
						object value3 = null;
						if (paramTypeByType != ParamType.STRING)
						{
							try
							{
								double num2 = double.Parse(text2);
								num2 = selectedInverter.ConvertValue(num, num2, device);
								double num3 = double.Parse(text);
								object obj = itemArray[5];
								object obj2 = itemArray[6];
								double? num4 = null;
								double? num5 = null;
								try
								{
									num4 = new double?((obj != null) ? double.Parse(obj.ToString()) : num3);
									num5 = new double?((obj2 != null) ? double.Parse(obj2.ToString()) : num3);
								}
								catch (Exception var_22_1DB)
								{
								}
								bool flag2 = !num4.HasValue || !num5.HasValue || (num3 >= num4.Value && num3 <= num5.Value);
								flag = (num3 != num2);
								if (!flag2)
								{
									string caption = dictionary["msgParamEntryErrorTitle"];
									MessageBoxButtons buttons = MessageBoxButtons.OK;
									MessageBoxIcon icon = MessageBoxIcon.Hand;
									MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1;
									MessageBox.Show(string.Format(dictionary["msgParamEntryErrorMessage"], num4, num5), caption, buttons, icon, defaultButton);
									dataGridViewCell2.Value = num2;
									return;
								}
								value3 = num3;
							}
							catch (Exception var_28_28D)
							{
								return;
							}
							if (flag && !paramChangedTable.ContainsKey(key))
							{
								paramChangedTable.Add(key, value3);
							}
							else if (!flag && paramChangedTable.ContainsKey(key))
							{
								paramChangedTable.Remove(key);
							}
							this.UpdateUIParamTableChangedStatus(dgv, row, flag);
						}
						else
						{
							try
							{
								flag = !text.Equals(text2);
								value3 = text;
							}
							catch (Exception var_29_2AD)
							{
							}
						}
					}
				}
			}
		}

		private void btnSettingsServerPingTest_Click(object sender, EventArgs e)
		{
			HWSettings.ShowHWSettings(HWType.PingTest, this);
		}

		private void btnSettingsIICDetectSlaves_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				DataManager.Instance.ClearDevices(true);
				selectedInverter.FreeSlaves();
				selectedInverter.Action_DetectSlaves();
				this.UpdateInverterList = true;
			}
		}

		private void settingsRS232_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			bool flag = selectedInverter != null && selectedInverter.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
			if (flag)
			{
				DialogResult dialogResult = HWSettings.ShowHWSettings(HWType.RS232, this);
				if (dialogResult == DialogResult.OK)
				{
					selectedInverter.UpdateSettingsCommRS232();
				}
			}
		}

		private void settingsRS485_Click(object sender, EventArgs e)
		{
			DialogResult dialogResult = HWSettings.ShowHWSettings(HWType.RS485, this);
			if (dialogResult == DialogResult.OK)
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				selectedInverter.UpdateSettingsCommRS485();
			}
		}

		private void settingsZB_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			bool flag = selectedInverter != null && selectedInverter.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
			if (flag)
			{
				DialogResult dialogResult = HWSettings.ShowHWSettings(HWType.ZB, this);
				if (dialogResult == DialogResult.OK)
				{
					selectedInverter.UpdateSettingsCommZigBee();
				}
			}
		}

		private void settingsLAN_Click(object sender, EventArgs e)
		{
			DialogResult dialogResult = HWSettings.ShowHWSettings(HWType.LAN, this);
			if (dialogResult == DialogResult.OK)
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				selectedInverter.UpdateSettingsCommLAN();
			}
		}

		private void settingsServer_Click(object sender, EventArgs e)
		{
			DialogResult dialogResult = HWSettings.ShowHWSettings(HWType.Server, this);
			if (dialogResult == DialogResult.OK)
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				selectedInverter.UpdateSettingsCommServer();
			}
		}

		private void settingsIIC_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			bool flag = selectedInverter != null && selectedInverter.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
			if (flag)
			{
				DialogResult dialogResult = HWSettings.ShowHWSettings(HWType.IIC, this);
				if (dialogResult == DialogResult.OK)
				{
					selectedInverter.UpdateSettingsCommIIC();
				}
			}
		}

		private void btnPBDataAdd_Click(object sender, EventArgs e)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			string text = "";
			DialogResult dialogResult = PowerBoxInputMessage.ShowPowerBoxInputMessage(this, dictionary["msgPBDataAddMessage"], dictionary["msgPBDataAddCaption"]);
			DialogResult dialogResult2 = dialogResult;
			if (dialogResult2 == DialogResult.OK)
			{
				text = PowerBoxInputMessage.LastInput;
			}
			if (!string.IsNullOrEmpty(text))
			{
				try
				{
					Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
					if (selectedInverter != null)
					{
						selectedInverter.DataPowerBox.Add(text, 0f, 0f, 0f, 0f, 0u);
					}
				}
				catch (FormatException var_4_8E)
				{
					MessageBox.Show(dictionary["msgPBDataAddInputError"]);
				}
			}
		}

		private void btnPBDataRemove_Click(object sender, EventArgs e)
		{
			DataGridViewRow dataGridViewRow = (this.dgvPBParam != null && this.dgvPBParam.SelectedRows != null && this.dgvPBParam.SelectedRows.Count > 0) ? this.dgvPBParam.SelectedRows[0] : null;
			if (dataGridViewRow != null)
			{
				string panelSN = dataGridViewRow.Cells[1].Value.ToString();
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					selectedInverter.DataPowerBox.Remove(panelSN);
					this.dgvPBParam.Rows.Remove(dataGridViewRow);
					int count = this.dgvPBParam.Rows.Count;
					for (int i = 0; i < count; i++)
					{
						DataGridViewRow dataGridViewRow2 = this.dgvPBParam.Rows[i];
						dataGridViewRow2.Cells[0].Value = i + 1;
					}
				}
			}
		}

		private void btnPBDataLoad_Click(object sender, EventArgs e)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			DBPowerBoxData dBPowerBoxData = new DBPowerBoxData();
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.DefaultExt = ".xls";
			openFileDialog.AddExtension = true;
			openFileDialog.AutoUpgradeEnabled = true;
			openFileDialog.Title = dictionary["msgLoadPBDataTitle"];
			openFileDialog.ValidateNames = true;
			openFileDialog.Filter = dictionary["msgLoadPBDataFilterName"] + "|*.xls";
			DialogResult dialogResult = openFileDialog.ShowDialog();
			DataTable dataTable = null;
			if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
			{
				string fileName = openFileDialog.FileName;
				try
				{
					dataTable = dBPowerBoxData.LoadWorksheet(fileName);
				}
				catch (Exception var_6_9F)
				{
					MessageBox.Show(dictionary["msgLoadPBDataError"]);
				}
			}
			if (dataTable != null)
			{
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				int count = dataTable.Rows.Count;
				for (int i = 0; i < count; i++)
				{
					DataRow dataRow = dataTable.Rows[i];
					object[] itemArray = dataRow.ItemArray;
					selectedInverter.DataPowerBox.Add(itemArray);
				}
			}
		}

		private void btnPBDataVersionUpdate_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				this.EnableForceTelem = false;
				selectedInverter.DataPowerBox.Action_UpdateVersion();
			}
		}

		private void btnPBForceTelem_Click(object sender, EventArgs e)
		{
			this._thForceTelem.Pause = false;
		}

		private void chkPBTelemetryFast_CheckedChanged(object sender, EventArgs e)
		{
			if (this._shouldCheckEventFastBeActive)
			{
				if (!this.chkPBTelemetryFast.Checked)
				{
					this._shouldCheckEventFastBeActive = false;
					this.chkPBTelemetryFast.Checked = true;
					this._shouldCheckEventFastBeActive = true;
				}
				else
				{
					Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
					if (selectedInverter != null)
					{
						selectedInverter.DataPowerBox.TelemetrySpeed = InverterTelemetrySpeedType.FAST;
						selectedInverter.DataPowerBox.DataChanged = false;
					}
					this._shouldCheckEventNormalBeActive = false;
					this.chkPBTelemetryNormal.Checked = false;
					this._shouldCheckEventNormalBeActive = true;
				}
				if (this.chkPBTelemetryFast.Checked)
				{
					this.SetTelemSpeed(true);
				}
			}
		}

		private void chkPBTelemetryNormal_CheckedChanged(object sender, EventArgs e)
		{
			if (this._shouldCheckEventNormalBeActive)
			{
				if (!this.chkPBTelemetryNormal.Checked)
				{
					this._shouldCheckEventNormalBeActive = false;
					this.chkPBTelemetryNormal.Checked = true;
					this._shouldCheckEventNormalBeActive = true;
				}
				else
				{
					Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
					if (selectedInverter != null)
					{
						selectedInverter.DataPowerBox.TelemetrySpeed = InverterTelemetrySpeedType.NORMAL;
						selectedInverter.DataPowerBox.DataChanged = false;
					}
					this._shouldCheckEventFastBeActive = false;
					this.chkPBTelemetryFast.Checked = false;
					this._shouldCheckEventFastBeActive = true;
				}
				if (this.chkPBTelemetryNormal.Checked)
				{
					this.SetTelemSpeed(false);
				}
			}
		}

		private void btnPBRecordsExport_Click(object sender, EventArgs e)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				DataTable dataTable = (selectedInverter.DataPowerBox != null) ? selectedInverter.DataPowerBox.Get() : null;
				if (dataTable != null)
				{
					DBPowerBoxData dBPowerBoxData = new DBPowerBoxData();
					dBPowerBoxData.ExportDataToWorksheet(dataTable);
					SaveFileDialog saveFileDialog = new SaveFileDialog();
					saveFileDialog.DefaultExt = ".xls";
					saveFileDialog.AddExtension = true;
					saveFileDialog.AutoUpgradeEnabled = true;
					saveFileDialog.Title = dictionary["msgSavePBDataTitle"];
					saveFileDialog.ValidateNames = true;
					saveFileDialog.Filter = dictionary["msgSavePBDataFilterName"] + "|*.xls";
					DialogResult dialogResult = saveFileDialog.ShowDialog(this);
					if ((dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes) && !string.IsNullOrEmpty(saveFileDialog.FileName))
					{
						dBPowerBoxData.SaveWorksheet(saveFileDialog.FileName);
					}
				}
			}
		}

		private void btnTUUpdateControlFile_Click(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			DialogResult dialogResult = openFileDialog.ShowDialog(this);
			if (!string.IsNullOrEmpty(openFileDialog.FileName))
			{
				this.txtTUUpdateControlFile.TSAccess.Text = openFileDialog.FileName;
			}
		}

		private void btnTUUpdateControlBeginUpdate_Click(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			if (selectedInverter != null)
			{
				SECommDevice sECommDevice = null;
				try
				{
					sECommDevice = ((Inverter1Phase)selectedInverter).Venus;
				}
				catch (Exception var_2_2E)
				{
				}
				try
				{
					sECommDevice = ((Inverter3Phase)selectedInverter).Jupiter;
				}
				catch (Exception var_2_2E)
				{
				}
				this.txtTUDetailsStatus.Clear();
				DataManager.Instance.FirmwareUpgrader.UpgradeFileName = this.txtTUUpdateControlFile.TSAccess.Text;
				DataManager.Instance.FirmwareUpgrader.UpgradeNotifier = this;
				DataManager.Instance.FirmwareUpgrader.CommDevices = new SECommDevice[]
				{
					selectedInverter.Portia,
					sECommDevice
				};
				DataManager.Instance.FirmwareUpgrader.Action_SetUpgradeStatus(true);
			}
		}

		private void btnTMRTCSet_Click(object sender, EventArgs e)
		{
			try
			{
				this.Cursor = Cursors.WaitCursor;
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				if (selectedInverter != null)
				{
					string text = this.lblTMRTCGMTOffsetValue.Text;
					if (text.Contains("+"))
					{
						text = text.Substring(1);
					}
					DialogResult dialogResult = RTCSettingMessage.ShowRTCSettingMessage(this, text);
					if (dialogResult == DialogResult.OK)
					{
						DateTime returnTime = RTCSettingMessage.ReturnTime;
						uint returnGMTSecs = RTCSettingMessage.ReturnGMTSecs;
						selectedInverter.Portia.Command_Portia_SetRTC(returnTime, returnGMTSecs);
						selectedInverter.DataRegional.GMTOffset = Utils.UInt32AsInt32(returnGMTSecs);
					}
					selectedInverter.StatusUpdatedSet(false, UpdateDataType.TOOLS_MISC);
					this.SwitchScreen(this.SelectedScreen);
				}
			}
			catch (Exception var_5_B3)
			{
			}
			finally
			{
				this.Cursor = Cursors.Arrow;
			}
		}

		private void btnTMPwrBalanceEnableDisable_Click(object sender, EventArgs e)
		{
			Cursor cursor = this.Cursor;
			try
			{
				this.Cursor = Cursors.WaitCursor;
				Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
				bool powerBalancing = selectedInverter.DataSystem.PowerBalancing;
				this.cmbTMPwrBalanceStatus.SelectedIndex = (powerBalancing ? 0 : 1);
				selectedInverter.SetParamPortia(PortiaParams.POWER_BALANCE_SHUTDOWN, powerBalancing ? 0u : 1u);
				selectedInverter.Portia.Command_Device_Reset();
				selectedInverter.DataSystem.PowerBalancing = !powerBalancing;
				this.btnTMPwrBalanceStatus.Enabled = false;
			}
			catch (Exception var_3_83)
			{
			}
			this.Cursor = cursor;
		}

		private void cmbTMPwrBalanceStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
			bool flag = this.cmbTMPwrBalanceStatus.SelectedIndex == 1;
			this.btnTMPwrBalanceStatus.Enabled = (flag != selectedInverter.DataSystem.PowerBalancing);
		}
	}
}
