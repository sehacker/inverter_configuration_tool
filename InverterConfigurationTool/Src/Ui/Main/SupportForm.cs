using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.Main
{
	public class SupportForm : SEForm
	{
		private const string BUILD_NUM = "";

		private IContainer components = null;

		private SELabel lblSupportHeader;

		private SEPictureBox pbSupportLogo;

		private SEButton btnSupportMessageClose;

		private SEButton btnSupportContactUs;

		private SELabel lblSupportApplication;

		private SELabel lblSupportVersion;

		private SELabel lblSupportCopyRight;

		private SELabel lblSupportSE;

		private SELabel lblSupportApplicationValue;

		private SELabel lblSupportVersionValue;

		private SETextBox txtSupportContactInfo;

		private LinkLabel llSESiteMain;

		private LinkLabel llSESiteSupport;

		private SELabel lblSESite;

		private SELabel lblSESiteSupport;

		private static SupportForm _instance = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(SupportForm));
			this.lblSupportHeader = new SELabel();
			this.pbSupportLogo = new SEPictureBox();
			this.btnSupportMessageClose = new SEButton();
			this.btnSupportContactUs = new SEButton();
			this.lblSupportApplication = new SELabel();
			this.lblSupportVersion = new SELabel();
			this.lblSupportCopyRight = new SELabel();
			this.lblSupportSE = new SELabel();
			this.lblSupportApplicationValue = new SELabel();
			this.lblSupportVersionValue = new SELabel();
			this.txtSupportContactInfo = new SETextBox();
			this.llSESiteMain = new LinkLabel();
			this.llSESiteSupport = new LinkLabel();
			this.lblSESite = new SELabel();
			this.lblSESiteSupport = new SELabel();
			((ISupportInitialize)this.pbSupportLogo).BeginInit();
			base.SuspendLayout();
			this.lblSupportHeader.BackColor = Color.Transparent;
			this.lblSupportHeader.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSupportHeader.Location = new Point(26, 3);
			this.lblSupportHeader.Name = "lblSupportHeader";
			this.lblSupportHeader.Size = new Size(484, 27);
			this.lblSupportHeader.TabIndex = 0;
			this.lblSupportHeader.Text = "SolarEdge Inverter Configuration Tool Support";
			this.lblSupportHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSupportHeader.MouseMove += new MouseEventHandler(this.lblPBMessageHeader_MouseMove);
			this.lblSupportHeader.MouseDown += new MouseEventHandler(this.lblPBMessageHeader_MouseDown);
			this.lblSupportHeader.MouseUp += new MouseEventHandler(this.lblPBMessageHeader_MouseUp);
			this.pbSupportLogo.BackColor = Color.Transparent;
			this.pbSupportLogo.BackgroundImage = Resources.SolarEdgeLogo;
			this.pbSupportLogo.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbSupportLogo.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbSupportLogo.ImageListBack");
			this.pbSupportLogo.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbSupportLogo.ImageListFore");
			this.pbSupportLogo.Location = new Point(413, 43);
			this.pbSupportLogo.Name = "pbSupportLogo";
			this.pbSupportLogo.SelectedImageBack = -1;
			this.pbSupportLogo.SelectedImageFore = -1;
			this.pbSupportLogo.Size = new Size(114, 32);
			this.pbSupportLogo.TabIndex = 8;
			this.pbSupportLogo.TabStop = false;
			this.btnSupportMessageClose.BackColor = Color.Silver;
			this.btnSupportMessageClose.Location = new Point(453, 360);
			this.btnSupportMessageClose.Name = "btnSupportMessageClose";
			this.btnSupportMessageClose.Size = new Size(63, 29);
			this.btnSupportMessageClose.TabIndex = 9;
			this.btnSupportMessageClose.Text = "Close";
			this.btnSupportMessageClose.UseVisualStyleBackColor = true;
			this.btnSupportMessageClose.Click += new EventHandler(this.btnSupportMessageClose_Click);
			this.btnSupportContactUs.BackColor = Color.Silver;
			this.btnSupportContactUs.Location = new Point(209, 361);
			this.btnSupportContactUs.Name = "btnSupportContactUs";
			this.btnSupportContactUs.Size = new Size(120, 29);
			this.btnSupportContactUs.TabIndex = 10;
			this.btnSupportContactUs.Text = "Contact Us";
			this.btnSupportContactUs.UseVisualStyleBackColor = true;
			this.btnSupportContactUs.Click += new EventHandler(this.btnSupportContactUs_Click);
			this.lblSupportApplication.BackColor = Color.Transparent;
			this.lblSupportApplication.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSupportApplication.Location = new Point(17, 42);
			this.lblSupportApplication.Name = "lblSupportApplication";
			this.lblSupportApplication.Size = new Size(110, 22);
			this.lblSupportApplication.TabIndex = 11;
			this.lblSupportApplication.Text = "Application:";
			this.lblSupportApplication.TextAlign = ContentAlignment.MiddleLeft;
			this.lblSupportVersion.BackColor = Color.Transparent;
			this.lblSupportVersion.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSupportVersion.Location = new Point(17, 65);
			this.lblSupportVersion.Name = "lblSupportVersion";
			this.lblSupportVersion.Size = new Size(76, 22);
			this.lblSupportVersion.TabIndex = 12;
			this.lblSupportVersion.Text = "Version:";
			this.lblSupportVersion.TextAlign = ContentAlignment.MiddleLeft;
			this.lblSupportCopyRight.BackColor = Color.Transparent;
			this.lblSupportCopyRight.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSupportCopyRight.Location = new Point(17, 87);
			this.lblSupportCopyRight.Name = "lblSupportCopyRight";
			this.lblSupportCopyRight.Size = new Size(167, 22);
			this.lblSupportCopyRight.TabIndex = 13;
			this.lblSupportCopyRight.Text = "Copyright (c) 2010";
			this.lblSupportCopyRight.TextAlign = ContentAlignment.MiddleLeft;
			this.lblSupportSE.BackColor = Color.Transparent;
			this.lblSupportSE.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSupportSE.Location = new Point(17, 111);
			this.lblSupportSE.Name = "lblSupportSE";
			this.lblSupportSE.Size = new Size(167, 22);
			this.lblSupportSE.TabIndex = 14;
			this.lblSupportSE.Text = "SolarEdge Technologies";
			this.lblSupportSE.TextAlign = ContentAlignment.MiddleLeft;
			this.lblSupportApplicationValue.BackColor = Color.Transparent;
			this.lblSupportApplicationValue.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSupportApplicationValue.Location = new Point(131, 43);
			this.lblSupportApplicationValue.Name = "lblSupportApplicationValue";
			this.lblSupportApplicationValue.Size = new Size(277, 22);
			this.lblSupportApplicationValue.TabIndex = 15;
			this.lblSupportApplicationValue.Text = "SolarEdge Inverter Configuration Tool";
			this.lblSupportApplicationValue.TextAlign = ContentAlignment.MiddleLeft;
			this.lblSupportVersionValue.BackColor = Color.Transparent;
			this.lblSupportVersionValue.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSupportVersionValue.Location = new Point(131, 65);
			this.lblSupportVersionValue.Name = "lblSupportVersionValue";
			this.lblSupportVersionValue.Size = new Size(277, 22);
			this.lblSupportVersionValue.TabIndex = 16;
			this.lblSupportVersionValue.Text = "1.0.0.0";
			this.lblSupportVersionValue.TextAlign = ContentAlignment.MiddleLeft;
			this.txtSupportContactInfo.Location = new Point(20, 212);
			this.txtSupportContactInfo.Multiline = true;
			this.txtSupportContactInfo.Name = "txtSupportContactInfo";
			this.txtSupportContactInfo.ReadOnly = true;
			this.txtSupportContactInfo.ScrollBars = ScrollBars.Vertical;
			this.txtSupportContactInfo.Size = new Size(498, 137);
			this.txtSupportContactInfo.TabIndex = 17;
			this.txtSupportContactInfo.Text = "Contact Information:\r\n\r\nWorldwide: +972.73.2403116\r\nUSA: +1.650.319.8843\r\nGermany: +49.89.23513100\r\nFrance: +33.(0)970.465.662\r\n\r\nFax: +972.73.2403117";
			this.llSESiteMain.BackColor = Color.Transparent;
			this.llSESiteMain.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.llSESiteMain.Location = new Point(132, 139);
			this.llSESiteMain.Name = "llSESiteMain";
			this.llSESiteMain.Size = new Size(387, 24);
			this.llSESiteMain.TabIndex = 18;
			this.llSESiteMain.TabStop = true;
			this.llSESiteMain.Text = "www.solaredge.com";
			this.llSESiteMain.TextAlign = ContentAlignment.MiddleLeft;
			this.llSESiteMain.LinkClicked += new LinkLabelLinkClickedEventHandler(this.llSESiteMain_LinkClicked);
			this.llSESiteSupport.BackColor = Color.Transparent;
			this.llSESiteSupport.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.llSESiteSupport.Location = new Point(132, 176);
			this.llSESiteSupport.Name = "llSESiteSupport";
			this.llSESiteSupport.Size = new Size(383, 27);
			this.llSESiteSupport.TabIndex = 19;
			this.llSESiteSupport.TabStop = true;
			this.llSESiteSupport.Text = "http://www.solaredge.com/groups/support/overview";
			this.llSESiteSupport.TextAlign = ContentAlignment.MiddleLeft;
			this.llSESiteSupport.LinkClicked += new LinkLabelLinkClickedEventHandler(this.llSESiteSupport_LinkClicked);
			this.lblSESite.BackColor = Color.Transparent;
			this.lblSESite.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSESite.Location = new Point(17, 136);
			this.lblSESite.Name = "lblSESite";
			this.lblSESite.Size = new Size(115, 32);
			this.lblSESite.TabIndex = 20;
			this.lblSESite.Text = "SolarEdge Site:";
			this.lblSESite.TextAlign = ContentAlignment.MiddleLeft;
			this.lblSESiteSupport.BackColor = Color.Transparent;
			this.lblSESiteSupport.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSESiteSupport.Location = new Point(17, 173);
			this.lblSESiteSupport.Name = "lblSESiteSupport";
			this.lblSESiteSupport.Size = new Size(115, 32);
			this.lblSESiteSupport.TabIndex = 21;
			this.lblSESiteSupport.Text = "Support Site:";
			this.lblSESiteSupport.TextAlign = ContentAlignment.MiddleLeft;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandBigNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(537, 402);
			base.ControlBox = false;
			base.Controls.Add(this.lblSESiteSupport);
			base.Controls.Add(this.lblSESite);
			base.Controls.Add(this.llSESiteSupport);
			base.Controls.Add(this.llSESiteMain);
			base.Controls.Add(this.txtSupportContactInfo);
			base.Controls.Add(this.lblSupportVersionValue);
			base.Controls.Add(this.lblSupportApplicationValue);
			base.Controls.Add(this.lblSupportSE);
			base.Controls.Add(this.lblSupportCopyRight);
			base.Controls.Add(this.lblSupportVersion);
			base.Controls.Add(this.lblSupportApplication);
			base.Controls.Add(this.btnSupportContactUs);
			base.Controls.Add(this.btnSupportMessageClose);
			base.Controls.Add(this.pbSupportLogo);
			base.Controls.Add(this.lblSupportHeader);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "SupportForm";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			((ISupportInitialize)this.pbSupportLogo).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private SupportForm()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
		}

		private void SetupLabels()
		{
			this.lblSupportHeader.SetUI(false, true);
			this.lblSupportApplication.SetUI(true, true);
			this.lblSupportApplicationValue.SetUI(false, false);
			this.lblSupportVersion.SetUI(true, true);
			this.lblSupportVersionValue.SetUI(false, false);
			this.lblSupportCopyRight.SetUI(false, true);
			this.lblSupportSE.SetUI(false, true);
			this.lblSESite.SetUI(true, true);
			this.lblSESiteSupport.SetUI(true, true);
			this.txtSupportContactInfo.Text = "";
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (iSEUI.IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_79)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		public static void ShowSupportForm(Form owner)
		{
			if (SupportForm._instance == null)
			{
				SupportForm._instance = new SupportForm();
			}
			SupportForm._instance.SetForm();
			SupportForm._instance.ShowDialog(owner);
		}

		private void SetForm()
		{
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			this.lblSupportVersionValue.Text = MainForm.Instance.ApplicationVersion.ToString(FieldEnums.BETA_ENUM, null, (VersionOptions)9);
			string text = DataManager.Instance.Dictionary[this.txtSupportContactInfo.Name];
			string[] lines = text.Split(new string[]
			{
				"\\n"
			}, StringSplitOptions.None);
			this.txtSupportContactInfo.Lines = lines;
			base.OnShown(e);
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblSupportHeader.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblPBMessageHeader_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblSupportHeader.Size.Height);
		}

		private void lblPBMessageHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblPBMessageHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		private void btnSupportContactUs_Click(object sender, EventArgs e)
		{
			string fileName = "mailto:support@solaredge.com?subject:New Support Mail";
			Process.Start(fileName);
		}

		private void btnSupportMessageClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void llSESiteMain_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			LinkLabel linkLabel = (LinkLabel)sender;
			Process.Start(linkLabel.Text);
		}

		private void llSESiteSupport_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			LinkLabel linkLabel = (LinkLabel)sender;
			Process.Start(linkLabel.Text);
		}
	}
}
