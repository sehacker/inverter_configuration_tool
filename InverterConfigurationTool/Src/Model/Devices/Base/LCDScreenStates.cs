using System;

namespace InverterConfigurationTool.Src.Model.Devices.Base
{
	public enum LCDScreenStates
	{
		SKIP = -1,
		SCREEN_STATUS_1,
		SCREEN_STATUS_2,
		SCREEN_ENERGY,
		SCREEN_SPEC,
		SCREEN_IIC_STATUS,
		SCREEN_COMM_STATUS,
		SCREEN_ZB_SETTINGS,
		COUNT
	}
}
