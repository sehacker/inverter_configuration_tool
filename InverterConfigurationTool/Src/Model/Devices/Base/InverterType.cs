using System;

namespace InverterConfigurationTool.Src.Model.Devices.Base
{
	public enum InverterType
	{
		NONE,
		SE3300,
		SE4000,
		SE5000,
		SE5000DE,
		SE6000,
		SE8k,
		SE10k,
		SE12k,
		SE3300US208V,
		SE4000US208V,
		SE5000US208V,
		SE6000US208V,
		SE3300US240V,
		SE4000US240V,
		SE5000US240V,
		SE6000US240V,
		SE4000POR,
		COUNT
	}
}
