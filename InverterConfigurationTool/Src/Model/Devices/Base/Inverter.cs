using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Data;
using SEDevices.Data;
using SEDevices.Devices;
using SEDevices.Records.Extra;
using SEDevices.Records.Parameter;
using SEDevices.Records.Status;
using SEDevices.Records.Telemetry;
using SEProtocol.PLC;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using SESecurity;
using SEStorage.Base;
using SEUI.Common;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace InverterConfigurationTool.Src.Model.Devices.Base
{
	[Serializable]
	public class Inverter : StorageItem, ISECommNotifiable
	{
		private delegate void ThreadFunc();

		private const uint IS_ALIVE_PERIOD = 60000u;

		private const int PARAM_ACCURACY = 2;

		private const bool NO_CONVERTION = false;

		private const bool CONVERT = true;

		private const int PARAMS_PORTIA_MIN = 0;

		private const int PARAMS_PORTIA_MAX = 569;

		private const int PARAMS_VENUS_MIN = 0;

		private const int PARAMS_VENUS_DEF_SPECIAL_MIN = 24;

		private const int PARAMS_VENUS_DEF_SPECIAL_MAX = 55;

		private const int PARAMS_VENUS_MAX = 245;

		private const uint EXCLUSIVE_LOCK_TIMEOUT = 60000u;

		private static SWVersion MINIMUM_DEVICE_VERSION_FOR_LOCK = new SWVersion(3u, 39u);

		private bool _isInvSelected = false;

		private object _isExclusivlyControlingDeviceObject = new object();

		private bool _isExclusivelyControlling = true;

		private object _shouldControlExclusivelyObject = new object();

		private bool _shouldControlExclusively = false;

		private object _lastExclusiveControlKeepAliveSentTimeObject = new object();

		private long _lastExclusiveControlKeepAliveSentTime = 0L;

		private int _index = -1;

		private object _dataReadySyncObj = new object();

		private bool _dataReady = false;

		private SEPortia _portia;

		protected ISEPLCCommandable _plcCommandableDevice = null;

		private Inverter _master;

		private List<Inverter> _slaves;

		private CommManager _commManager;

		private SETelemetrySystemStatus _lastSysStat = null;

		private SystemStatus _statusData = null;

		private RegionalData _regionalData = null;

		private CommData _commData = null;

		private PowerBoxData _pbData = null;

		private EnergyData _energyData = null;

		private bool _shouldUpdateLCDWithEnergy = false;

		protected object _syncObjUpdated = new object();

		private bool[] _screenStatusUpdated = null;

		protected Dictionary<int, string[]> _lastUpdatedCPUData = null;

		protected Dictionary<int, string[]> _lastUpdatedDSPData = null;

		private SEThread _thParamUpdate;

		private SEThread _thKeepAlive;

		private SEThread _thLANUpdate;

		private SEThread _thUpdateLCD;

		private SEThread _thDeviceLock;

		private bool _wasThreadParamRunning = false;

		private bool _wasThreadKARunning = false;

		private bool _wasThreadLANRunning = false;

		private bool _wasThreadLCDRunning = false;

		private bool _wasThreadDeviceLockRunning = false;

		private List<string> _lcdLines = null;

		private bool _currentLCDStateChanged = false;

		private LCDScreenStates _currentLCDScreenState = LCDScreenStates.SCREEN_STATUS_1;

		public bool IsSelected
		{
			get
			{
				bool isInvSelected;
				lock (this)
				{
					isInvSelected = this._isInvSelected;
				}
				return isInvSelected;
			}
			set
			{
				lock (this)
				{
					this._isInvSelected = value;
				}
			}
		}

		public bool IsExclusivelyControling
		{
			get
			{
				bool isExclusivelyControlling;
				lock (this._isExclusivlyControlingDeviceObject)
				{
					isExclusivelyControlling = this._isExclusivelyControlling;
				}
				return isExclusivelyControlling;
			}
			set
			{
				lock (this._isExclusivlyControlingDeviceObject)
				{
					this._isExclusivelyControlling = value;
				}
			}
		}

		private bool ControlExclusively
		{
			get
			{
				bool shouldControlExclusively;
				lock (this._shouldControlExclusivelyObject)
				{
					shouldControlExclusively = this._shouldControlExclusively;
				}
				return shouldControlExclusively;
			}
			set
			{
				lock (this._shouldControlExclusivelyObject)
				{
					this._shouldControlExclusively = value;
				}
			}
		}

		private long LastExclusiveControlKeepAliveSent
		{
			get
			{
				long lastExclusiveControlKeepAliveSentTime;
				lock (this._lastExclusiveControlKeepAliveSentTimeObject)
				{
					lastExclusiveControlKeepAliveSentTime = this._lastExclusiveControlKeepAliveSentTime;
				}
				return lastExclusiveControlKeepAliveSentTime;
			}
			set
			{
				lock (this._lastExclusiveControlKeepAliveSentTimeObject)
				{
					this._lastExclusiveControlKeepAliveSentTime = value;
				}
			}
		}

		public int Index
		{
			get
			{
				int index;
				lock (this)
				{
					index = this._index;
				}
				return index;
			}
			set
			{
				lock (this)
				{
					this._index = value;
				}
			}
		}

		public bool DataReady
		{
			get
			{
				bool dataReady;
				lock (this._dataReadySyncObj)
				{
					dataReady = this._dataReady;
				}
				return dataReady;
			}
			set
			{
				lock (this._dataReadySyncObj)
				{
					this._dataReady = value;
				}
			}
		}

		public SEPortia Portia
		{
			get
			{
				return this._portia;
			}
		}

		public ISEPLCCommandable PLCCommandableDevice
		{
			get
			{
				return this._plcCommandableDevice;
			}
		}

		public Inverter Master
		{
			get
			{
				return this._master;
			}
			set
			{
				this._master = value;
			}
		}

		public bool IsMaster
		{
			get
			{
				return this.Master == null && ((this.DataComm.IICConnectionType == 2 && this.DataComm.RS485Connected && this.DataComm.RS485Enabled && this.DataComm.RS485Master) || ((this.DataComm.IICConnectionType == 0 || this.DataComm.IICConnectionType == 1) && this.DataComm.ZigBeeConnected && this.DataComm.ZigBeeEnabled && this.DataComm.ZigBeeMaster));
			}
		}

		public List<Inverter> Slaves
		{
			get
			{
				List<Inverter> slaves;
				lock (this)
				{
					if (this._slaves == null && this.Master == null)
					{
						this.UpdateSlaves();
					}
					slaves = this._slaves;
				}
				return slaves;
			}
		}

		public SETelemetrySystemStatus SystemStatus
		{
			get
			{
				return this._lastSysStat;
			}
			set
			{
				this._lastSysStat = value;
			}
		}

		public SystemStatus DataSystem
		{
			get
			{
				if (this._statusData == null)
				{
					this._statusData = new SystemStatus(this);
				}
				return this._statusData;
			}
		}

		public RegionalData DataRegional
		{
			get
			{
				if (this._regionalData == null)
				{
					this._regionalData = new RegionalData(this);
				}
				return this._regionalData;
			}
		}

		public CommData DataComm
		{
			get
			{
				if (this._commData == null)
				{
					this._commData = new CommData(this);
				}
				return this._commData;
			}
		}

		public PowerBoxData DataPowerBox
		{
			get
			{
				if (this._pbData == null)
				{
					this._pbData = new PowerBoxData(this);
				}
				return this._pbData;
			}
		}

		public EnergyData DataEnergy
		{
			get
			{
				if (this._energyData == null)
				{
					this._energyData = new EnergyData(this);
				}
				return this._energyData;
			}
		}

		public Dictionary<int, string[]> LastUpdatedCPUData
		{
			get
			{
				return this._lastUpdatedCPUData;
			}
		}

		public Dictionary<int, string[]> LastUpdatedDSPData
		{
			get
			{
				return this._lastUpdatedDSPData;
			}
		}

		public List<string> LCDLines
		{
			get
			{
				return this._lcdLines;
			}
			set
			{
				this._lcdLines = value;
			}
		}

		public bool CurrentLCDStateChanged
		{
			get
			{
				return this._currentLCDStateChanged;
			}
			set
			{
				this._currentLCDStateChanged = value;
			}
		}

		public LCDScreenStates CurrentLCDScreenState
		{
			get
			{
				return this._currentLCDScreenState;
			}
			set
			{
				if (this._currentLCDScreenState != value)
				{
					this._currentLCDScreenState = value;
					this._currentLCDStateChanged = true;
				}
			}
		}

		public bool StatusUpdatedGet(UpdateDataType type)
		{
			bool result;
			lock (this._syncObjUpdated)
			{
				result = this._screenStatusUpdated[(int)type];
			}
			return result;
		}

		public virtual void StatusUpdatedSet(bool updated, UpdateDataType type)
		{
			lock (this._syncObjUpdated)
			{
				this._screenStatusUpdated[(int)type] = updated;
				if (!updated)
				{
					this.Portia.ClearTableDataAll();
				}
			}
		}

		public virtual void StatusUpdatedSetAll(bool updated)
		{
			lock (this._syncObjUpdated)
			{
				int num = 14;
				for (int i = 0; i < num; i++)
				{
					this._screenStatusUpdated[i] = updated;
				}
				if (!updated)
				{
					this.Portia.ClearTableDataAll();
				}
			}
		}

		public Inverter(SEPortia portia)
		{
			this._portia = portia;
			this._portia.Owner = this;
			int num = 14;
			this._screenStatusUpdated = new bool[num];
			for (int i = 0; i < num; i++)
			{
				this._screenStatusUpdated[i] = false;
			}
		}

		public virtual void StartInverter()
		{
			if (this.Portia != null)
			{
				this._commManager = this.Portia.CommManager;
				this._commManager.RegisterForCommListening(this);
				this.DataComm.Connected = true;
				this.SetThreadControl();
			}
		}

		public virtual void ShutdownInverter()
		{
			this.DataComm.Connected = false;
			this._commManager.UnRegisterFromCommListening(this);
			this.CloseThreadControl();
			if (this._portia != null)
			{
				this._portia.Dispose();
				this._portia = null;
			}
		}

		private void SetThreadControl()
		{
			string str = "Inverter " + this.Portia.Address.ToString();
			this._thKeepAlive = new SEThread(str + " Keep-Alive Thread", new SEThread.ThreadFunctionCallBack(this.ThreadFuncKeepAlive), 500, 0u, 0u, true, true, false);
			this._thLANUpdate = new SEThread(str + " LAN Update Thread", new SEThread.ThreadFunctionCallBack(this.ThreadFuncLANUpdate), 500, 0u, 0u, true, true, false);
			this._thUpdateLCD = new SEThread(str + " Update LCD Thread", new SEThread.ThreadFunctionCallBack(this.ThreadFuncUpdateLCD), 500, 0u, 0u, true, true, true);
			this._thDeviceLock = new SEThread(str + "Device Lock Thread", new SEThread.ThreadFunctionCallBack(this.ThreadFuncDeviceLock), 500, 0u, 0u, true, true, false);
		}

		private void StoreThreadStatus()
		{
			this._wasThreadKARunning = this._thKeepAlive.Pause;
			this._wasThreadLANRunning = this._thLANUpdate.Pause;
			this._wasThreadLCDRunning = this._thUpdateLCD.Pause;
			this._wasThreadDeviceLockRunning = this._thDeviceLock.Pause;
		}

		public void PauseAllThreadControl()
		{
			this.StoreThreadStatus();
			this._thKeepAlive.Pause = true;
			this._thLANUpdate.Pause = true;
			this._thUpdateLCD.Pause = true;
			this._thDeviceLock.Pause = true;
		}

		public void ResumeLastThreadControlStatus()
		{
			this._thKeepAlive.Pause = this._wasThreadKARunning;
			this._thLANUpdate.Pause = this._wasThreadLANRunning;
			this._thUpdateLCD.Pause = this._wasThreadLCDRunning;
			this._thDeviceLock.Pause = this._wasThreadDeviceLockRunning;
		}

		private void CloseThreadControl()
		{
			try
			{
				this._thLANUpdate.Stop = true;
				this._thLANUpdate.Dispose();
				this._thKeepAlive.Stop = true;
				this._thKeepAlive.Dispose();
				this._thUpdateLCD.Stop = true;
				this._thUpdateLCD.Dispose();
				this._thDeviceLock.Stop = true;
				this._thDeviceLock.Dispose();
			}
			catch (Exception var_0_69)
			{
			}
			finally
			{
				this._thLANUpdate = null;
				this._thKeepAlive = null;
				this._thUpdateLCD = null;
				this._thDeviceLock = null;
			}
		}

		public void CommNotify(Packet packet)
		{
			if (packet != null)
			{
				if (packet.OpCode == 1280 && packet.Destination == ProtocolMain.PROT_CONFTOOL_ADDR)
				{
					this.HandlePushTelemetry(packet);
				}
			}
		}

		private void HandlePushTelemetry(Packet packet)
		{
			byte[] data = packet.Data;
			int length = packet.Length;
			long num = 0L;
			while (length > 0 && num < (long)length)
			{
				SETelemetry newRecord = SETelemetry.GetNewRecord(ref data, num);
				string text = newRecord.Type.ToString();
				if (newRecord != null)
				{
					num = newRecord.ParseRecord(ref data, num);
					RecordType type = newRecord.Type;
					if (type == RecordType.PANEL)
					{
						this.HandleTelemetryFromPanel((SETelemetryPanel)newRecord);
					}
				}
			}
		}

		private void HandleTelemetryFromPanel(SETelemetryPanel panelRecord)
		{
			if (panelRecord.ReceiverId == panelRecord.DstId)
			{
				Trace.WriteLine("Packet Received");
				uint num = panelRecord.DstId - 8388608u;
				if (num == this.Portia.Address)
				{
					uint id = panelRecord.Id;
					this.DataPowerBox.Add(id, panelRecord.Vin, panelRecord.Vout, panelRecord.Iin, panelRecord.AccPower);
				}
			}
		}

		public double ConvertValue(int index, double value, DeviceType device)
		{
			double num = value;
			try
			{
				DBParameterData instance = DBParameterData.Instance;
				float? num2 = null;
				float? num3 = null;
				float? num4 = null;
				switch (device)
				{
				case DeviceType.PORTIA:
					num2 = instance.GetParamConversionRatePortia((ushort)index);
					num3 = instance.GetParamEqAPortia((ushort)index);
					num4 = instance.GetParamEqBPortia((ushort)index);
					break;
				case DeviceType.VENUS:
					num2 = instance.GetParamConversionRateVenus((ushort)index);
					num3 = instance.GetParamEqAVenus((ushort)index);
					num4 = instance.GetParamEqBVenus((ushort)index);
					break;
				case DeviceType.MERCURY:
					num2 = instance.GetParamConversionRateMercury((ushort)index);
					num3 = instance.GetParamEqAMercury((ushort)index);
					num4 = instance.GetParamEqBMercury((ushort)index);
					break;
				case DeviceType.JUPITER:
					num2 = instance.GetParamConversionRateJupiter((ushort)index);
					num3 = instance.GetParamEqAJupiter((ushort)index);
					num4 = instance.GetParamEqBJupiter((ushort)index);
					break;
				}
				double num5 = num2.HasValue ? ((double)num2.Value) : 1.0;
				double num6 = num3.HasValue ? ((double)num3.Value) : 1.0;
				double num7 = num4.HasValue ? ((double)num4.Value) : 0.0;
				num = num6 * (value / num5) + num7;
				string s = string.Format("{0:0.00}", num);
				num = double.Parse(s);
			}
			catch (Exception var_9_13F)
			{
			}
			return num;
		}

		public double RevertValue(int index, double value, DeviceType device)
		{
			double result = value;
			try
			{
				DBParameterData instance = DBParameterData.Instance;
				float? num = null;
				float? num2 = null;
				float? num3 = null;
				switch (device)
				{
				case DeviceType.PORTIA:
					num = instance.GetParamConversionRatePortia((ushort)index);
					num2 = instance.GetParamEqAPortia((ushort)index);
					num3 = instance.GetParamEqBPortia((ushort)index);
					break;
				case DeviceType.VENUS:
					num = instance.GetParamConversionRateVenus((ushort)index);
					num2 = instance.GetParamEqAVenus((ushort)index);
					num3 = instance.GetParamEqBVenus((ushort)index);
					break;
				case DeviceType.MERCURY:
					num = instance.GetParamConversionRateMercury((ushort)index);
					num2 = instance.GetParamEqAMercury((ushort)index);
					num3 = instance.GetParamEqBMercury((ushort)index);
					break;
				case DeviceType.JUPITER:
					num = instance.GetParamConversionRateJupiter((ushort)index);
					num2 = instance.GetParamEqAJupiter((ushort)index);
					num3 = instance.GetParamEqBJupiter((ushort)index);
					break;
				}
				double num4 = num.HasValue ? ((double)num.Value) : 1.0;
				double num5 = num2.HasValue ? ((double)num2.Value) : 1.0;
				double num6 = num3.HasValue ? ((double)num3.Value) : 0.0;
				result = num4 * ((value - num6) / num5);
			}
			catch (Exception var_8_125)
			{
			}
			return result;
		}

		public string GetParamPortia(PortiaParams portiaParam)
		{
			return this.GetParamPortia(portiaParam, true);
		}

		public string GetParamPortia(PortiaParams portiaParam, bool isConverted)
		{
			string result = null;
			SEParam sEParam = null;
			if (this.Portia.IsParamExist((int)portiaParam))
			{
				sEParam = this.Portia.GetParamFromTable((int)portiaParam);
			}
			if (sEParam == null)
			{
				sEParam = this.Portia.Command_Params_Get(portiaParam);
			}
			if (sEParam != null)
			{
				if (this.Portia.IsParamExist((int)portiaParam))
				{
					this.Portia.SetParameterToTable((int)portiaParam, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValuePortia = instance.GetParameterMinValuePortia((ushort)index);
					object parameterMaxValuePortia = instance.GetParameterMaxValuePortia((ushort)index);
					AccessLevels portiaParamAccessLevelForWriting = instance.GetPortiaParamAccessLevelForWriting((ushort)index);
					this.Portia.AddParameterToDataTable(index, ((PortiaParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValuePortia, parameterMaxValuePortia, (int)portiaParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRatePortia = instance2.GetParamConversionRatePortia(index2);
					float? paramEqAPortia = instance2.GetParamEqAPortia(index2);
					float? paramEqBPortia = instance2.GetParamEqBPortia(index2);
					float rate = paramConversionRatePortia.HasValue ? paramConversionRatePortia.Value : 1f;
					float a = paramEqAPortia.HasValue ? paramEqAPortia.Value : 1f;
					float b = paramEqBPortia.HasValue ? paramEqBPortia.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				result = sEParam.GetParamAsString(isConverted);
			}
			return result;
		}

		public string[] GetParamPortia(PortiaParams[] portiaParams)
		{
			return this.GetParamPortia(portiaParams, true);
		}

		public string[] GetParamPortia(PortiaParams[] portiaParams, bool isConverted)
		{
			List<string> list = null;
			int num = portiaParams.Length;
			List<PortiaParams> list2 = new List<PortiaParams>(0);
			List<SEParam> list3 = new List<SEParam>(0);
			for (int i = 0; i < num; i++)
			{
				PortiaParams portiaParams2 = portiaParams[i];
				SEParam paramFromTable = this.Portia.GetParamFromTable((int)portiaParams2);
				if (paramFromTable == null)
				{
					list2.Add(portiaParams2);
				}
				else
				{
					list3.Add(paramFromTable);
				}
			}
			if (list2 != null && list2.Count > 0)
			{
				SEParam[] array = this.Portia.Command_Params_Get(list2.ToArray());
				if (array != null)
				{
					int num2 = array.Length;
					for (int i = 0; i < num2; i++)
					{
						SEParam item = array[i];
						list3.Add(item);
					}
				}
			}
			int num3 = (list3 != null) ? list3.Count : 0;
			for (int i = 0; i < num3; i++)
			{
				SEParam sEParam = list3[i];
				if (this.Portia.IsParamExist((int)sEParam.Index))
				{
					this.Portia.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValuePortia = instance.GetParameterMinValuePortia((ushort)index);
					object parameterMaxValuePortia = instance.GetParameterMaxValuePortia((ushort)index);
					AccessLevels portiaParamAccessLevelForWriting = instance.GetPortiaParamAccessLevelForWriting((ushort)index);
					this.Portia.AddParameterToDataTable(index, ((PortiaParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValuePortia, parameterMaxValuePortia, (int)portiaParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRatePortia = instance2.GetParamConversionRatePortia(index2);
					float? paramEqAPortia = instance2.GetParamEqAPortia(index2);
					float? paramEqBPortia = instance2.GetParamEqBPortia(index2);
					float rate = paramConversionRatePortia.HasValue ? paramConversionRatePortia.Value : 1f;
					float a = paramEqAPortia.HasValue ? paramEqAPortia.Value : 1f;
					float b = paramEqBPortia.HasValue ? paramEqBPortia.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				string paramAsString = sEParam.GetParamAsString(isConverted);
				if (list == null)
				{
					list = new List<string>(0);
				}
				list.Add(paramAsString);
			}
			return list.ToArray();
		}

		public string GetParamMercury(MercuryParams mercuryParam, SEMercury merc)
		{
			return this.GetParamMercury(mercuryParam, merc, true);
		}

		public string GetParamMercury(MercuryParams mercuryParam, SEMercury merc, bool isConvert)
		{
			string result = null;
			SEParam sEParam = null;
			if (merc.IsParamExist((int)mercuryParam))
			{
				sEParam = merc.GetParamFromTable((int)mercuryParam);
			}
			if (sEParam == null)
			{
				sEParam = merc.Command_Params_Get(mercuryParam);
			}
			if (sEParam != null)
			{
				if (merc.IsParamExist((int)mercuryParam))
				{
					merc.SetParameterToTable((int)mercuryParam, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueMercury = instance.GetParameterMinValueMercury((ushort)index);
					object parameterMaxValueMercury = instance.GetParameterMaxValueMercury((ushort)index);
					AccessLevels mercuryParamAccessLevelForWriting = instance.GetMercuryParamAccessLevelForWriting((ushort)index);
					merc.AddParameterToDataTable(index, ((PortiaParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueMercury, parameterMaxValueMercury, (int)mercuryParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRateMercury = instance2.GetParamConversionRateMercury(index2);
					float? paramEqAMercury = instance2.GetParamEqAMercury(index2);
					float? paramEqBMercury = instance2.GetParamEqBMercury(index2);
					float rate = paramConversionRateMercury.HasValue ? paramConversionRateMercury.Value : 1f;
					float a = paramEqAMercury.HasValue ? paramEqAMercury.Value : 1f;
					float b = paramEqBMercury.HasValue ? paramEqBMercury.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				result = sEParam.GetParamAsString(isConvert);
			}
			return result;
		}

		public string[] GetParamMercury(MercuryParams[] mercuryParams, SEMercury merc)
		{
			return this.GetParamMercury(mercuryParams, merc, true);
		}

		public string[] GetParamMercury(MercuryParams[] mercuryParams, SEMercury merc, bool isConvert)
		{
			List<string> list = null;
			int num = mercuryParams.Length;
			List<MercuryParams> list2 = new List<MercuryParams>(0);
			List<SEParam> list3 = new List<SEParam>(0);
			for (int i = 0; i < num; i++)
			{
				MercuryParams mercuryParams2 = mercuryParams[i];
				SEParam paramFromTable = merc.GetParamFromTable((int)mercuryParams2);
				if (paramFromTable == null)
				{
					list2.Add(mercuryParams2);
				}
				else
				{
					list3.Add(paramFromTable);
				}
			}
			if (list2 != null && list2.Count > 0)
			{
				SEParam[] array = merc.Command_Params_Get(list2.ToArray());
				if (array != null)
				{
					int num2 = array.Length;
					for (int i = 0; i < num2; i++)
					{
						SEParam item = array[i];
						list3.Add(item);
					}
				}
			}
			int num3 = (list3 != null) ? list3.Count : 0;
			for (int i = 0; i < num3; i++)
			{
				SEParam sEParam = list3[i];
				if (merc.IsParamExist((int)sEParam.Index))
				{
					merc.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueMercury = instance.GetParameterMinValueMercury((ushort)index);
					object parameterMaxValueMercury = instance.GetParameterMaxValueMercury((ushort)index);
					AccessLevels mercuryParamAccessLevelForWriting = instance.GetMercuryParamAccessLevelForWriting((ushort)index);
					merc.AddParameterToDataTable(index, ((MercuryParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueMercury, parameterMaxValueMercury, (int)mercuryParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRateMercury = instance2.GetParamConversionRateMercury(index2);
					float? paramEqAMercury = instance2.GetParamEqAMercury(index2);
					float? paramEqBMercury = instance2.GetParamEqBMercury(index2);
					float rate = paramConversionRateMercury.HasValue ? paramConversionRateMercury.Value : 1f;
					float a = paramEqAMercury.HasValue ? paramEqAMercury.Value : 1f;
					float b = paramEqBMercury.HasValue ? paramEqBMercury.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				string paramAsString = sEParam.GetParamAsString(isConvert);
				if (list == null)
				{
					list = new List<string>(0);
				}
				list.Add(paramAsString);
			}
			return list.ToArray();
		}

		public virtual SWVersion GetDSPVersion()
		{
			return null;
		}

		public virtual SWVersion GetDSPPwrVersion()
		{
			return null;
		}

		public void SetParamPortia(PortiaParams index, object value)
		{
			this.SetParamPortia(index, value, true);
		}

		public void SetParamPortia(PortiaParams index, object value, bool shouldValueBeConverted)
		{
			DBParameterData instance = DBParameterData.Instance;
			Type typeOfParamPortia = instance.GetTypeOfParamPortia((ushort)index);
			SEParam sEParam = new SEParam((ushort)index, value, typeOfParamPortia);
			if (shouldValueBeConverted && sEParam.ParameterType != 3)
			{
				sEParam.SetValueConverted(this.RevertValue((int)sEParam.Index, double.Parse(value.ToString()), DeviceType.PORTIA));
			}
			if (this.Portia.IsParamExist((int)sEParam.Index))
			{
				this.Portia.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
			}
			else
			{
				object defaultVal = null;
				object parameterMinValuePortia = instance.GetParameterMinValuePortia(sEParam.Index);
				object parameterMaxValuePortia = instance.GetParameterMaxValuePortia(sEParam.Index);
				AccessLevels portiaParamAccessLevelForWriting = instance.GetPortiaParamAccessLevelForWriting(sEParam.Index);
				this.Portia.AddParameterToDataTable((int)sEParam.Index, ((PortiaParams)sEParam.Index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValuePortia, parameterMaxValuePortia, (int)portiaParamAccessLevelForWriting);
			}
			this.Portia.Command_Params_Set(sEParam);
		}

		public void SetParamPortia(PortiaParams[] indices, object[] values)
		{
			this.SetParamPortia(indices, values, true);
		}

		public void SetParamPortia(PortiaParams[] indices, object[] values, bool shouldValuesBeConverted)
		{
			List<SEParam> list = new List<SEParam>(0);
			int num = indices.Length;
			DBParameterData instance = DBParameterData.Instance;
			for (int i = 0; i < num; i++)
			{
				ushort num2 = (ushort)indices[i];
				object obj = values[i];
				Type typeOfParamPortia = DBParameterData.Instance.GetTypeOfParamPortia(num2);
				SEParam sEParam = new SEParam(num2, obj, typeOfParamPortia);
				if (shouldValuesBeConverted && sEParam.ParameterType != 3)
				{
					sEParam.SetValueConverted(this.RevertValue((int)sEParam.Index, double.Parse(obj.ToString()), DeviceType.PORTIA));
				}
				if (this.Portia.IsParamExist((int)sEParam.Index))
				{
					this.Portia.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
				}
				else
				{
					DBParameterData instance2 = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValuePortia = instance2.GetParameterMinValuePortia(sEParam.Index);
					object parameterMaxValuePortia = instance2.GetParameterMaxValuePortia(sEParam.Index);
					AccessLevels portiaParamAccessLevelForWriting = instance2.GetPortiaParamAccessLevelForWriting(sEParam.Index);
					this.Portia.AddParameterToDataTable((int)sEParam.Index, ((PortiaParams)sEParam.Index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValuePortia, parameterMaxValuePortia, (int)portiaParamAccessLevelForWriting);
				}
				list.Add(sEParam);
			}
			this.Portia.Command_Params_Set(list.ToArray());
		}

		public virtual void UpdateStatusSpec()
		{
			try
			{
				string paramPortia = this.GetParamPortia(PortiaParams.INV_MODEL_NUMBER);
				string paramPortia2 = this.GetParamPortia(PortiaParams.INV_SERIAL_NUMBER);
				this.DataSystem.InverterModel = ((paramPortia != null) ? paramPortia : "");
				this.DataSystem.InverterID = ((paramPortia2 != null) ? paramPortia2 : "");
				this.DataSystem.CPUVersion = this.Portia.DeviceSWVersion;
				string paramPortia3 = this.GetParamPortia(PortiaParams.POWER_BALANCE_SHUTDOWN);
				this.DataSystem.PowerBalancing = (paramPortia3 != null && paramPortia3 == "1");
			}
			catch (Exception var_3_83)
			{
			}
		}

		public virtual void UpdateStatusLCD(bool withEnergy)
		{
			if (withEnergy)
			{
				try
				{
					SEEnergyStatus sEEnergyStatus = this.Portia.Command_Portia_Get_EnergyStatus();
					if (sEEnergyStatus != null)
					{
						this.DataEnergy.EnergyDay = sEEnergyStatus.TotalAccumulatedEnergyDay;
						this.DataEnergy.EnergyMonth = sEEnergyStatus.TotalAccumulatedEnergyMonth;
						this.DataEnergy.EnergyYear = sEEnergyStatus.TotalAccumulatedEnergyYear;
						this.DataEnergy.EnergyTotal = sEEnergyStatus.TotalAccumulatedEnergyTotal;
					}
				}
				catch (Exception var_1_6A)
				{
				}
			}
		}

		public virtual void UpdateSettingsRegional()
		{
		}

		public virtual void UpdateSettingsCommRS232()
		{
			try
			{
				string paramPortia = this.GetParamPortia(PortiaParams.ENABLE_GSM);
				this.DataComm.RS232ToServerConnection = ((!string.IsNullOrEmpty(paramPortia)) ? int.Parse(paramPortia) : -1);
				this.DataComm.RS232GSMAPN = this.GetParamPortia(PortiaParams.GSM_ACCESS_POINT_NAME);
				string paramPortia2 = this.GetParamPortia(PortiaParams.GSM_MODEM_TYPE);
				int rS232GSMModemType = string.IsNullOrEmpty(paramPortia2) ? 0 : int.Parse(paramPortia2);
				this.DataComm.RS232GSMModemType = rS232GSMModemType;
				this.DataComm.RS232GSMUsername = this.GetParamPortia(PortiaParams.GSM_USER_NAME);
				this.DataComm.RS232GSMPassword = this.GetParamPortia(PortiaParams.GSM_PASSWORD);
			}
			catch (Exception var_3_9E)
			{
			}
		}

		public virtual void UpdateSettingsCommRS485()
		{
			try
			{
				string paramPortia = this.GetParamPortia(PortiaParams.ENABLE_RS485);
				this.DataComm.RS485Enabled = (paramPortia != null && paramPortia.Equals("1"));
				this.DataComm.RS485Connected = true;
				string paramPortia2 = this.GetParamPortia(PortiaParams.RS485_MASTER);
				this.DataComm.RS485Master = (paramPortia2 != null && paramPortia2.Equals("1"));
			}
			catch (Exception var_2_5D)
			{
			}
		}

		public virtual void UpdateSettingsCommZigBee()
		{
			try
			{
				string paramPortia = this.GetParamPortia(PortiaParams.ENABLE_ZB);
				this.DataComm.ZigBeeEnabled = (paramPortia != null && paramPortia.Equals("1"));
				this.DataComm.ZigBeeConnected = this.Portia.Command_Portia_Get_ZB_Exists();
				string paramPortia2 = this.GetParamPortia(PortiaParams.ZIGBEE_MASTER);
				this.DataComm.ZigBeeMaster = (paramPortia2 != null && paramPortia2.Equals("1"));
				string paramPortia3 = this.GetParamPortia(PortiaParams.ZIGBEE_PAN_ID);
				this.DataComm.ZigBeePANID = ((paramPortia3 != null) ? uint.Parse(paramPortia3) : 0u);
				string paramPortia4 = this.GetParamPortia(PortiaParams.ZIGBEE_SCAN_CHANNELS_MASK);
				this.DataComm.ZigBeeScanChannel = ((paramPortia4 != null) ? uint.Parse(paramPortia4) : 0u);
			}
			catch (Exception var_4_A9)
			{
			}
		}

		public virtual void UpdateSettingsCommLAN()
		{
			try
			{
				string paramPortia = this.GetParamPortia(PortiaParams.ENABLE_TCP);
				this.DataComm.LANEnabled = paramPortia.Equals("1");
				string paramPortia2 = this.GetParamPortia(PortiaParams.ENABLE_DHCP);
				this.DataComm.LANDHCPEnabled = (!string.IsNullOrEmpty(paramPortia2) && int.Parse(paramPortia2) == 1);
				this.DataComm.LANStatusDetected = this.Portia.Command_Portia_GetLANStatusDetected();
				this.DataComm.LANStatusParametered = this.Portia.Command_Portia_GetLANStatusParametered();
				this.DataComm.MACAddress = this.Portia.Command_Portia_GetMacAddr();
				string paramAsString = this.Portia.Command_Params_Get(PortiaParams.ETH_CONNECTED).GetParamAsString();
				if (paramAsString != null)
				{
					this.DataComm.LANConnectionStatusTCP = uint.Parse(paramAsString);
				}
				this.DataComm.LANConnectionStatusCable = this.Portia.Command_Portia_EthernetStatus();
			}
			catch (Exception var_3_D8)
			{
			}
		}

		public virtual void UpdateSettingsCommServer()
		{
			try
			{
				string paramPortia = this.GetParamPortia(PortiaParams.SERVER_IP1);
				this.DataComm.ServerAddress = ((paramPortia != null) ? paramPortia : "");
				string paramPortia2 = this.GetParamPortia(PortiaParams.TCP_SERVER_PORT);
				this.DataComm.ServerPort = ((!string.IsNullOrEmpty(paramPortia2)) ? int.Parse(paramPortia2) : 0);
				string paramPortia3 = this.GetParamPortia(PortiaParams.STREAM_SERVER);
				int serverCommunicationType = string.IsNullOrEmpty(paramPortia3) ? 0 : int.Parse(paramPortia3);
				this.DataComm.ServerCommunicationType = serverCommunicationType;
				S_OK_STATUS serverStatus = this.Portia.Command_Portia_Get_S_OK();
				this.DataComm.ServerStatus = serverStatus;
			}
			catch (Exception var_5_8E)
			{
			}
		}

		public virtual void UpdateSettingsCommIIC()
		{
			try
			{
				string paramPortia = this.GetParamPortia(PortiaParams.STREAM_POLESTARS);
				this.DataComm.IICConnectionType = (string.IsNullOrEmpty(paramPortia) ? 2 : int.Parse(paramPortia));
				string paramPortia2 = this.GetParamPortia(PortiaParams.SERVER_COM_MODE);
				this.DataComm.IICCommunicationType = (string.IsNullOrEmpty(paramPortia2) ? 0 : int.Parse(paramPortia2));
			}
			catch (Exception var_2_57)
			{
			}
		}

		private void CheckIfStatusShouldChange(long now)
		{
			if (this.IsExclusivelyControling && !this.ControlExclusively)
			{
				this.Portia.Command_Portia_Lock(false, 60000u);
			}
			else if (!this.IsExclusivelyControling && this.ControlExclusively)
			{
				this.Portia.Command_Portia_Lock(true, 60000u);
				this.LastExclusiveControlKeepAliveSent = now;
			}
		}

		private void UpdateExclusiveLockStatus()
		{
			this.IsExclusivelyControling = this.Portia.Command_Portia_IsLocked();
		}

		private void HandleExclusiveKeepAlive(long now)
		{
			if (this.IsExclusivelyControling && this.LastExclusiveControlKeepAliveSent - now > 60000L)
			{
				this.Portia.Command_Portia_Lock(true, 60000u);
				this.LastExclusiveControlKeepAliveSent = now;
			}
		}

		private void ThreadFuncKeepAlive()
		{
			try
			{
				if ((long)this.Portia.LastKeepAliveForPBSendInterval > 60000L)
				{
					this.Portia.SetBroadcast();
					this.Portia.Command_Portia_ConfToolStart(true);
					this.Portia.SetOriginalAddress();
				}
			}
			catch (Exception var_0_47)
			{
			}
		}

		protected virtual void UpdateParameterTableCallBack()
		{
		}

		private void SlaveDetectionCallBack()
		{
			DataManager instance = DataManager.Instance;
			try
			{
				if (this.Portia != null)
				{
					this.Portia.Command_Portia_SlaveDetect(this.DataComm.AutoConfigureSlaves);
					int num = 0;
					float num2 = 0f;
					int num3 = -1;
					float num4 = -1f;
					while (num2 < 100f)
					{
						bool flag = this.Portia.Command_Portia_SlaveDetectStatus(ref num, ref num2);
						if (num2 != num4 || num != num3)
						{
							instance.UIOwner.UpdateProgressBar(string.Format(DataManager.Instance.Dictionary["MsgDetectSlaves"], new object[]
							{
								num
							}), num2);
						}
					}
					this.UpdateSlaves();
					DataManager.Instance.DetectSlavesForDevice(this);
					DataManager.Instance.UIOwner.UpdateInverterList = true;
				}
			}
			catch (Exception var_6_DF)
			{
				instance.UIOwner.UpdateProgressBar("", 100f);
			}
		}

		private void ThreadFuncLANUpdate()
		{
			try
			{
				this.UpdateSettingsCommLAN();
				S_OK_STATUS serverStatus = this.Portia.Command_Portia_Get_S_OK();
				this.DataComm.ServerStatus = serverStatus;
			}
			catch (Exception var_1_25)
			{
			}
		}

		private void ThreadFuncUpdateLCD()
		{
			try
			{
				this.UpdateStatusLCD(this._shouldUpdateLCDWithEnergy);
				this.UpdateLCDScreensByState();
			}
			catch (Exception var_0_19)
			{
			}
		}

		private void ThreadFuncDeviceLock()
		{
			try
			{
				if (this.Portia != null && this.Portia.DeviceSWVersion >= Inverter.MINIMUM_DEVICE_VERSION_FOR_LOCK)
				{
					long ticks = DateTime.Now.Ticks;
					this.CheckIfStatusShouldChange(ticks);
					this.UpdateExclusiveLockStatus();
					this.HandleExclusiveKeepAlive(ticks);
				}
			}
			catch (Exception var_1_53)
			{
			}
		}

		protected virtual void UpdateParams(List<ushort> cpuIndexList, List<ushort> dspIndexList, Dictionary<int, string[]> cpuDataList, Dictionary<int, string[]> dspDataList)
		{
		}

		protected void UpdateCPUParams(List<ushort> cpuList, Dictionary<int, string[]> cpuDataTable, ref int numParamsRetreived)
		{
			DataManager instance = DataManager.Instance;
			int num = (cpuList != null) ? cpuList.Count : 0;
			List<PortiaParams> list = new List<PortiaParams>(0);
			for (int i = 0; i < num; i++)
			{
				int num2 = (int)cpuList[i];
				list.Add((PortiaParams)num2);
				this.Portia.ClearTableData(num2);
			}
			if (list != null && list.Count > 0)
			{
				string[] paramPortia = this.GetParamPortia(list.ToArray());
				int num3 = list.Count;
				num3 = Math.Min(num3, paramPortia.Length);
				for (int i = 0; i < num3; i++)
				{
					ushort num4 = cpuList[i];
					string text = list[i].ToString();
					string text2 = paramPortia[i];
					string paramUnitKeyPortia = DBParameterData.Instance.GetParamUnitKeyPortia(num4);
					cpuDataTable.Add((int)num4, new string[]
					{
						text,
						text2,
						paramUnitKeyPortia
					});
				}
				int num5 = paramPortia.Length;
				numParamsRetreived = num5;
				instance.UIOwner.UpdateProgressBar(string.Format(DataManager.Instance.Dictionary["MsgUpdateParamCPU"], new object[]
				{
					num5
				}), (float)numParamsRetreived);
			}
		}

		public void ReleaseSlaves()
		{
			if (this._slaves != null)
			{
				int count = this._slaves.Count;
				for (int i = 0; i < count; i++)
				{
					Inverter inverter = this._slaves[i];
					inverter.ShutdownInverter();
				}
				this._slaves.Clear();
			}
		}

		public bool CanSustainSlaves(bool realCheck)
		{
			bool flag = this.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
			bool flag2 = false;
			if (flag)
			{
				int num = this.DataComm.IICConnectionType;
				if (realCheck)
				{
					string paramPortia = this.GetParamPortia(PortiaParams.STREAM_POLESTARS);
					num = ((paramPortia != null) ? int.Parse(paramPortia) : num);
					this.DataComm.IICConnectionType = num;
				}
				switch (num)
				{
				case 0:
				case 1:
					flag2 = this.DataComm.ZigBeeMaster;
					if (realCheck)
					{
						string paramPortia = this.GetParamPortia(PortiaParams.ZIGBEE_MASTER);
						flag2 = ((paramPortia != null) ? (int.Parse(paramPortia) == 1) : flag2);
						this.DataComm.ZigBeeMaster = flag2;
					}
					break;
				case 2:
					flag2 = this.DataComm.RS485Master;
					if (realCheck)
					{
						string paramPortia = this.GetParamPortia(PortiaParams.RS485_MASTER);
						flag2 = ((paramPortia != null) ? (int.Parse(paramPortia) == 1) : flag2);
						this.DataComm.RS485Master = flag2;
					}
					break;
				}
			}
			else
			{
				flag2 = true;
			}
			return flag2;
		}

		public void FreeSlaves()
		{
			if (this._slaves != null && this._slaves.Count > 0)
			{
				this.ReleaseSlaves();
			}
		}

		public void UpdateSlaves()
		{
			this._slaves = new List<Inverter>(0);
			bool flag = this.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
			int iICConnectionType = this.DataComm.IICConnectionType;
			bool rS485Master = this.DataComm.RS485Master;
			bool zigBeeMaster = this.DataComm.ZigBeeMaster;
			bool flag2 = !flag && !rS485Master;
			bool flag3 = flag && iICConnectionType == -1;
			bool flag4 = flag && iICConnectionType == 2 && !rS485Master;
			bool flag5 = flag && (iICConnectionType == 0 || iICConnectionType == 1) && !zigBeeMaster;
			if (!flag2 && (!flag3 && !flag4) && !flag5)
			{
				string paramPortia = this.GetParamPortia(PortiaParams.NUM_OF_PS_SLV);
				int num = (!string.IsNullOrEmpty(paramPortia)) ? int.Parse(paramPortia) : 0;
				if (this._slaves == null)
				{
					this._slaves = new List<Inverter>(0);
				}
				else
				{
					this.FreeSlaves();
				}
				uint deviceDefaultTimeout = this.Portia.DeviceDefaultTimeout;
				this.Portia.DeviceDefaultTimeout = 12000u;
				this.Portia.Command_Portia_ConfToolStart(true);
				Thread.Sleep(2000);
				int i = 0;
				while (i < num)
				{
					try
					{
						int portiaParam = 59 + i;
						string paramPortia2 = this.GetParamPortia((PortiaParams)portiaParam);
						if (paramPortia2 != null)
						{
							uint value = uint.Parse(paramPortia2);
							SEPortia sEPortia = new SEPortia(this._commManager, new uint?(value), null, null);
							SEParam sEParam = sEPortia.Command_Params_Get(PortiaParams.POLESTAR_MODE);
							Inverter inverter;
							switch ((sEParam != null) ? sEParam.ValueUInt32 : 0u)
							{
							case 1u:
								inverter = new Inverter3Phase(sEPortia);
								goto IL_1BF;
							}
							inverter = new Inverter1Phase(sEPortia);
							IL_1BF:
							sEPortia.Owner = inverter;
							inverter.Master = this;
							inverter.StartInverter();
							this._slaves.Add(inverter);
						}
					}
					catch (Exception var_19_1EB)
					{
					}
					IL_1F3:
					i++;
					continue;
					goto IL_1F3;
				}
				this.Portia.DeviceDefaultTimeout = deviceDefaultTimeout;
			}
		}

		private void UpdateLCDScreensByState()
		{
			switch (this.CurrentLCDScreenState)
			{
			case LCDScreenStates.SCREEN_STATUS_1:
				this.LCDLines = this.GetScreenStatus1();
				break;
			case LCDScreenStates.SCREEN_STATUS_2:
				this.LCDLines = this.GetScreenStatus2();
				break;
			case LCDScreenStates.SCREEN_ENERGY:
				this.LCDLines = this.GetScreenEnergy();
				break;
			case LCDScreenStates.SCREEN_SPEC:
				this.LCDLines = this.GetScreenSpec();
				break;
			case LCDScreenStates.SCREEN_IIC_STATUS:
				this.LCDLines = this.GetScreenIIC();
				break;
			case LCDScreenStates.SCREEN_COMM_STATUS:
				this.LCDLines = this.GetScreenComm();
				break;
			case LCDScreenStates.SCREEN_ZB_SETTINGS:
				this.LCDLines = this.GetScreenZBSettings();
				break;
			}
		}

		private List<string> GetScreenStatus1()
		{
			return new List<string>(0)
			{
				this.GetLineStatusHeader(),
				this.GetLineStatusValue(),
				this.GetLinePBCommStatus(),
				this.GetLinePowerOnOffSwitch()
			};
		}

		private List<string> GetScreenStatus2()
		{
			return new List<string>(0)
			{
				this.GetLineStatusHeader(),
				this.GetLineStatusValue(),
				this.GetLineFacTempStatusHeader(),
				this.GetLineFacTempStatusValue()
			};
		}

		private List<string> GetScreenEnergy()
		{
			return new List<string>(0)
			{
				this.GetLineEnergyDay(),
				this.GetLineEnergyMonth(),
				this.GetLineEnergyYear(),
				this.GetLineEnergyTotal()
			};
		}

		private List<string> GetScreenModule()
		{
			return new List<string>(0)
			{
				this.GetLineModuleSerialNumber(),
				this.GetLineModuleEnergy(),
				this.GetLineModuleVOut(),
				this.GetLineModuleVIn()
			};
		}

		private List<string> GetScreenSpec()
		{
			return new List<string>(0)
			{
				this.GetLineInverterID(),
				this.GetLineInverterDSPVersions(),
				this.GetLineInverterCPUVersion(),
				this.GetLineInverterCountry()
			};
		}

		private List<string> GetScreenIIC()
		{
			return new List<string>(0)
			{
				this.GetLineCommStatusValue(),
				this.GetLineCommStatusHeader(),
				this.GetLineCommStatusAdd(),
				this.GetLineIICCounters()
			};
		}

		private List<string> GetScreenComm()
		{
			return new List<string>(0)
			{
				this.GetLineIP(),
				this.GetLineSN(),
				this.GetLineGW(),
				this.GetLineMAC()
			};
		}

		private List<string> GetScreenZBSettings()
		{
			return new List<string>(0)
			{
				this.GetLineZBPAN(),
				this.GetLineZBChannel(),
				this.GetLineZBID(),
				this.GetLineZBMID()
			};
		}

		public static List<string> GetScreenOffline()
		{
			return new List<string>(0)
			{
				"",
				Inverter.GetLineOffline(),
				"",
				""
			};
		}

		public static List<string> GetScreenWaiting()
		{
			return new List<string>(0)
			{
				"",
				Inverter.GetLineWaiting(),
				"",
				""
			};
		}

		public static List<string> GetScreenNotUpdating()
		{
			return new List<string>(0)
			{
				"",
				Inverter.GetLineNotUpdating(),
				"",
				""
			};
		}

		private static string GetLineWaiting()
		{
			return "#Receiving Data...##";
		}

		private static string GetLineOffline()
		{
			return "###Panel#Offline####";
		}

		private static string GetLineNotUpdating()
		{
			return "##Updating Disabled#";
		}

		public string GetLineStatusHeader()
		{
			return "Vac[v]#Vdc[v]#Pac[w]";
		}

		public virtual string GetLineStatusValue()
		{
			return "";
		}

		public string GetLinePBCommStatus()
		{
			StringBuilder stringBuilder = new StringBuilder();
			int[] array = this.Portia.Command_Portia_Get_P_OK();
			if (array != null && array.Length > 1 && array[0] > 0)
			{
				stringBuilder.Append(string.Format("<P_OK {0:##}>", array[0]));
			}
			else
			{
				stringBuilder.Append("");
			}
			S_OK_STATUS s_OK_STATUS = this.Portia.Command_Portia_Get_S_OK();
			if (s_OK_STATUS == S_OK_STATUS.OK)
			{
				string text = "<S_OK>";
				int length = text.Length;
				int num = 20 - stringBuilder.Length - length;
				for (int i = 0; i < num; i++)
				{
					stringBuilder.Append("#");
				}
				stringBuilder.Append(text);
			}
			return stringBuilder.ToString();
		}

		public virtual string GetLinePowerOnOffSwitch()
		{
			return "";
		}

		public string GetLineFacTempStatusHeader()
		{
			return "Fac[Hz]########Temp#";
		}

		public virtual string GetLineFacTempStatusValue()
		{
			return "";
		}

		public string GetLineEnergyDay()
		{
			StringBuilder stringBuilder = new StringBuilder(0);
			float num = this.DataEnergy.EnergyDay;
			string str;
			if (num > 1000000f)
			{
				num /= 1000000f;
				str = "[MWh]";
			}
			else if (num > 1000f)
			{
				num /= 1000f;
				str = "[KWh]";
			}
			else
			{
				str = "[Wh]";
			}
			string text = "Day" + str + ":";
			string text2 = string.Format("{0:0.0}", num);
			stringBuilder.Append(text);
			int num2 = 20 - text.Length - text2.Length;
			for (int i = 0; i < num2; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			return stringBuilder.ToString();
		}

		public string GetLineEnergyMonth()
		{
			StringBuilder stringBuilder = new StringBuilder(0);
			float num = this.DataEnergy.EnergyMonth;
			string str;
			if (num > 1000000f)
			{
				num /= 1000000f;
				str = "[MWh]";
			}
			else if (num > 1000f)
			{
				num /= 1000f;
				str = "[KWh]";
			}
			else
			{
				str = "[Wh]";
			}
			string text = "Month" + str + ":";
			string text2 = string.Format("{0:0.0}", num);
			stringBuilder.Append(text);
			int num2 = 20 - text.Length - text2.Length;
			for (int i = 0; i < num2; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			return stringBuilder.ToString();
		}

		public string GetLineEnergyYear()
		{
			StringBuilder stringBuilder = new StringBuilder(0);
			float num = this.DataEnergy.EnergyYear;
			string str;
			if (num > 1000000f)
			{
				num /= 1000000f;
				str = "[MWh]";
			}
			else if (num > 1000f)
			{
				num /= 1000f;
				str = "[KWh]";
			}
			else
			{
				str = "[Wh]";
			}
			string text = "Year" + str + ":";
			string text2 = string.Format("{0:0.0}", num);
			stringBuilder.Append(text);
			int num2 = 20 - text.Length - text2.Length;
			for (int i = 0; i < num2; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			return stringBuilder.ToString();
		}

		public string GetLineEnergyTotal()
		{
			StringBuilder stringBuilder = new StringBuilder(0);
			float num = this.DataEnergy.EnergyTotal;
			string str;
			if (num > 1000000f)
			{
				num /= 1000000f;
				str = "[MWh]";
			}
			else if (num > 1000f)
			{
				num /= 1000f;
				str = "[KWh]";
			}
			else
			{
				str = "[Wh]";
			}
			string text = "Total" + str + ":";
			string text2 = string.Format("{0:0.0}", num);
			stringBuilder.Append(text);
			int num2 = 20 - text.Length - text2.Length;
			for (int i = 0; i < num2; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			return stringBuilder.ToString();
		}

		public string GetLineModuleSerialNumber()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Module:");
			if (this.DataPowerBox != null && this.DataPowerBox.PowerBoxCount > 0)
			{
				int index = this.DataPowerBox.PowerBoxCount - 1;
				object[] line = this.DataPowerBox.GetLine(index);
				if (line != null)
				{
					string text = line[0].ToString();
					if (string.IsNullOrEmpty(text))
					{
						throw new Exception();
					}
					int num = 20 - "Module:".Length - text.Length;
					for (int i = 0; i < num; i++)
					{
						stringBuilder.Append(" ");
					}
					stringBuilder.Append(text);
				}
			}
			return stringBuilder.ToString();
		}

		public string GetLineModuleEnergy()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Energy[Wh]:");
			if (this.DataPowerBox != null && this.DataPowerBox.PowerBoxCount > 0)
			{
				int index = this.DataPowerBox.PowerBoxCount - 1;
				object[] line = this.DataPowerBox.GetLine(index);
				if (line != null)
				{
					string text = line[4].ToString();
					if (string.IsNullOrEmpty(text))
					{
						throw new Exception();
					}
					int num = 20 - "Energy[Wh]:".Length - text.Length;
					for (int i = 0; i < num; i++)
					{
						stringBuilder.Append(" ");
					}
					stringBuilder.Append(text);
				}
			}
			return stringBuilder.ToString();
		}

		public string GetLineModuleVOut()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Vdc_o[v]:");
			if (this.DataPowerBox != null && this.DataPowerBox.PowerBoxCount > 0)
			{
				int index = this.DataPowerBox.PowerBoxCount - 1;
				object[] line = this.DataPowerBox.GetLine(index);
				if (line != null)
				{
					string text = line[2].ToString();
					if (string.IsNullOrEmpty(text))
					{
						throw new Exception();
					}
					int num = 20 - "Vdc_o[v]:".Length - text.Length;
					for (int i = 0; i < num; i++)
					{
						stringBuilder.Append(" ");
					}
					stringBuilder.Append(text);
				}
			}
			return stringBuilder.ToString();
		}

		public string GetLineModuleVIn()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Vdc_I[v]:");
			if (this.DataPowerBox != null && this.DataPowerBox.PowerBoxCount > 0)
			{
				int index = this.DataPowerBox.PowerBoxCount - 1;
				object[] line = this.DataPowerBox.GetLine(index);
				if (line != null)
				{
					string text = line[1].ToString();
					if (string.IsNullOrEmpty(text))
					{
						throw new Exception();
					}
					int num = 20 - "Vdc_I[v]:".Length - text.Length;
					for (int i = 0; i < num; i++)
					{
						stringBuilder.Append(" ");
					}
					stringBuilder.Append(text);
				}
			}
			return stringBuilder.ToString();
		}

		public string GetLineInverterID()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string hexID = this.Portia.HexID;
			string value = "ID : " + hexID;
			stringBuilder.Append(value);
			return stringBuilder.ToString();
		}

		public string GetLineInverterDSPVersions()
		{
			StringBuilder stringBuilder = new StringBuilder();
			SWVersion dSPVersion = this.GetDSPVersion();
			SWVersion dSPPwrVersion = this.GetDSPPwrVersion();
			stringBuilder.Append("DSP1/2:");
			stringBuilder.Append(dSPVersion.ToString(FieldEnums.MINOR_ENUM, new int[]
			{
				0,
				4
			}, VersionOptions.NONE));
			stringBuilder.Append("/");
			stringBuilder.Append(dSPPwrVersion.ToString(FieldEnums.MINOR_ENUM, new int[]
			{
				0,
				4
			}, VersionOptions.NONE));
			return stringBuilder.ToString();
		}

		public string GetLineInverterCPUVersion()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("CPU:");
			SWVersion deviceSWVersion = this.Portia.DeviceSWVersion;
			stringBuilder.Append(deviceSWVersion.ToString(FieldEnums.MINOR_ENUM, new int[]
			{
				4,
				4
			}, VersionOptions.NONE));
			return stringBuilder.ToString();
		}

		public string GetLineInverterCountry()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Country:");
			stringBuilder.Append(this.DataRegional.CountryShortName);
			int countryIndex = this.DataRegional.CountryIndex;
			int num = 0;
			string text = "";
			if (countryIndex < 100)
			{
				num = 1;
			}
			if (countryIndex < 10)
			{
				num = 2;
			}
			if (countryIndex <= 0)
			{
				num = 3;
			}
			for (int i = 0; i < num; i++)
			{
				text += "0";
			}
			stringBuilder.Append(" (" + text + countryIndex.ToString() + ")");
			return stringBuilder.ToString();
		}

		public string GetLineCommStatusValue()
		{
			DBParameterData instance = DBParameterData.Instance;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Comm:");
			string innerEnumName = instance.GetInnerEnumName(InnerEnumTypes.STREAM_SERVER, this.DataComm.ServerCommunicationType);
			string innerEnumName2 = instance.GetInnerEnumName(InnerEnumTypes.STREAM_POLESTARS, this.DataComm.IICConnectionType);
			string innerEnumName3 = instance.GetInnerEnumName(InnerEnumTypes.SERVER_COM_MODE, this.DataComm.IICCommunicationType);
			if (innerEnumName.Equals("ZigBee"))
			{
				stringBuilder.Append(innerEnumName + " " + (innerEnumName3.Equals("M/S") ? "M/S" : "P2P"));
			}
			else if (innerEnumName.Equals("RS 232"))
			{
				stringBuilder.Append(innerEnumName);
			}
			else if (innerEnumName.Equals("RS 485"))
			{
				stringBuilder.Append(innerEnumName + " " + (innerEnumName3.Equals("M/S") ? "M/S" : "P2P"));
			}
			else if (innerEnumName.Equals("TCP"))
			{
				stringBuilder.Append(innerEnumName);
			}
			S_OK_STATUS s_OK_STATUS = this.Portia.Command_Portia_Get_S_OK();
			if (s_OK_STATUS == S_OK_STATUS.OK)
			{
				string text = " <S-OK>";
				int num = 20 - stringBuilder.Length - text.Length;
				for (int i = 0; i < num; i++)
				{
					stringBuilder.Append("#");
				}
				stringBuilder.Append(text);
			}
			return stringBuilder.ToString();
		}

		public string GetLineCommStatusHeader()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Status:");
			return stringBuilder.ToString();
		}

		public string GetLineCommStatusAdd()
		{
			return "";
		}

		public string GetLineIICCounters()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string innerEnumName = DBParameterData.Instance.GetInnerEnumName(InnerEnumTypes.STREAM_POLESTARS, this.DataComm.IICConnectionType);
			int num = (this.Slaves != null) ? this.Slaves.Count : 0;
			int num2 = 0;
			if (num < 100)
			{
				num2 = 1;
			}
			if (num < 10)
			{
				num2 = 2;
			}
			if (num <= 0)
			{
				num2 = 3;
			}
			string str = "";
			for (int i = 0; i < num2; i++)
			{
				str += "0";
			}
			stringBuilder.Append("RS485<");
			if (innerEnumName.Equals("RS 485") && num > 0)
			{
				stringBuilder.Append(str + num.ToString());
			}
			else
			{
				stringBuilder.Append("---");
			}
			stringBuilder.Append(">  ZB<");
			if (innerEnumName.Equals("ZigBee") && num > 0)
			{
				stringBuilder.Append(str + num.ToString());
			}
			else
			{
				stringBuilder.Append("---");
			}
			stringBuilder.Append(">");
			return stringBuilder.ToString();
		}

		public string GetLineIP()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string iPStr = this.DataComm.LANStatusDetected.IPStr;
			stringBuilder.Append("IP  " + iPStr);
			return stringBuilder.ToString();
		}

		public string GetLineSN()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string subnetMaskStr = this.DataComm.LANStatusDetected.SubnetMaskStr;
			stringBuilder.Append("MSK " + subnetMaskStr);
			return stringBuilder.ToString();
		}

		public string GetLineGW()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string defaultGatewayStr = this.DataComm.LANStatusDetected.DefaultGatewayStr;
			stringBuilder.Append("GW  " + defaultGatewayStr);
			return stringBuilder.ToString();
		}

		public string GetLineMAC()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string mACAddressStrHex = this.DataComm.MACAddressStrHex;
			stringBuilder.Append("MAC " + mACAddressStrHex);
			return stringBuilder.ToString();
		}

		public string GetLineZBPAN()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string text = this.DataComm.ZigBeePANID.ToString();
			int num = 16 - text.Length;
			string text2 = "";
			for (int i = 0; i < num; i++)
			{
				text2 += "0";
			}
			text2 += text;
			stringBuilder.Append("PAN:" + text2);
			return stringBuilder.ToString();
		}

		public string GetLineZBChannel()
		{
			StringBuilder stringBuilder = new StringBuilder();
			uint zigBeeScanChannel = this.DataComm.ZigBeeScanChannel;
			string text = "";
			if (zigBeeScanChannel < 10u)
			{
				text += "0";
			}
			text += zigBeeScanChannel;
			stringBuilder.Append("Channel:          " + text);
			return stringBuilder.ToString();
		}

		public string GetLineZBID()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string str = "";
			stringBuilder.Append("ID: " + str);
			return stringBuilder.ToString();
		}

		public string GetLineZBMID()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string str = "";
			stringBuilder.Append("MID: " + str);
			return stringBuilder.ToString();
		}

		public virtual void Action_UpdateParameterTable(int numOfParams, bool shouldStartNewProgressBar)
		{
		}

		public void Action_DetectSlaves()
		{
			DataManager.Instance.UIOwner.ShowProgressBar(0, 100, new SEProgressBar.ProgressFormCallBack(this.SlaveDetectionCallBack));
		}

		public virtual void Action_Set_Country(uint newCountryIndex)
		{
		}

		public void Action_Set_LCD_Update(bool shouldRun, bool updateWithEnergy)
		{
			this._shouldUpdateLCDWithEnergy = updateWithEnergy;
			this._thUpdateLCD.Pause = !shouldRun;
		}

		public void Action_Set_Exclusive_Control(bool shouldLock)
		{
			if (this.Portia != null && !(this.Portia.DeviceSWVersion < Inverter.MINIMUM_DEVICE_VERSION_FOR_LOCK) && this._thDeviceLock != null)
			{
				this.ControlExclusively = shouldLock;
				this._thDeviceLock.Pause = false;
			}
		}
	}
}
