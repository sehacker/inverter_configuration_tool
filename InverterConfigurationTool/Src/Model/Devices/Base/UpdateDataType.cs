using System;

namespace InverterConfigurationTool.Src.Model.Devices.Base
{
	public enum UpdateDataType
	{
		INVERTER_LIST,
		SPEC,
		LCD,
		COMM_RS232,
		COMM_RS485,
		COMM_ZB,
		COMM_LAN,
		COMM_SERVER,
		COMM_IIC,
		REGIONAL_DATA,
		REGIONAL_PARAMS,
		PB,
		TOOLS_UPGRADE,
		TOOLS_MISC,
		COUNT
	}
}
