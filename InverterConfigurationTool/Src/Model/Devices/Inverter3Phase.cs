using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Data;
using InverterConfigurationTool.Src.Model.Devices.Base;
using SEDevices.Data;
using SEDevices.Devices;
using SEDevices.Records.Parameter;
using SEDevices.Records.Status;
using SESecurity;
using SEUI.Common;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace InverterConfigurationTool.Src.Model.Devices
{
	public class Inverter3Phase : Inverter
	{
		private delegate void ThreadFunc();

		private SEJupiter _jupiter;

		private SETelemetrySystemStatusJupiter _lastSysStatJupiter = null;

		public SEJupiter Jupiter
		{
			get
			{
				return this._jupiter;
			}
		}

		public SETelemetrySystemStatusJupiter SystemStatusJupiter
		{
			get
			{
				return this._lastSysStatJupiter;
			}
			set
			{
				this._lastSysStatJupiter = value;
			}
		}

		public override void StatusUpdatedSet(bool updated, UpdateDataType type)
		{
			lock (this._syncObjUpdated)
			{
				base.StatusUpdatedSet(updated, type);
				if (!updated)
				{
					this.Jupiter.ClearTableDataAll();
				}
			}
		}

		public override void StatusUpdatedSetAll(bool updated)
		{
			lock (this._syncObjUpdated)
			{
				base.StatusUpdatedSetAll(updated);
				if (!updated)
				{
					this.Jupiter.ClearTableDataAll();
				}
			}
		}

		public Inverter3Phase(SEPortia portia) : base(portia)
		{
		}

		public override void StartInverter()
		{
			if (base.Portia != null && base.Portia.CommManager.IsConnected)
			{
				this._jupiter = new SEJupiter(base.Portia.Address + 8388608u, base.Portia.CommManager, this, null, null);
				this._plcCommandableDevice = this._jupiter;
			}
			base.StartInverter();
		}

		public override void ShutdownInverter()
		{
			base.ShutdownInverter();
			if (this._jupiter != null)
			{
				this._jupiter.Dispose();
				this._jupiter = null;
			}
		}

		public string GetParamJupiter(JupiterParams jupiterParam)
		{
			return this.GetParamJupiter(jupiterParam, true);
		}

		public string GetParamJupiter(JupiterParams jupiterParam, bool isConvert)
		{
			string result = null;
			SEParam sEParam = null;
			if (this.Jupiter.IsParamExist((int)jupiterParam))
			{
				sEParam = this.Jupiter.GetParamFromTable((int)jupiterParam);
			}
			if (sEParam == null)
			{
				sEParam = this.Jupiter.Command_Params_Get(jupiterParam);
			}
			if (sEParam != null)
			{
				if (this.Jupiter.IsParamExist((int)jupiterParam))
				{
					this.Jupiter.SetParameterToTable((int)jupiterParam, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueJupiter = instance.GetParameterMinValueJupiter((ushort)index);
					object parameterMaxValueJupiter = instance.GetParameterMaxValueJupiter((ushort)index);
					AccessLevels jupiterParamAccessLevelForWriting = instance.GetJupiterParamAccessLevelForWriting((ushort)index);
					this.Jupiter.AddParameterToDataTable(index, ((JupiterParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueJupiter, parameterMaxValueJupiter, (int)jupiterParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRateJupiter = instance2.GetParamConversionRateJupiter(index2);
					float? paramEqAJupiter = instance2.GetParamEqAJupiter(index2);
					float? paramEqBJupiter = instance2.GetParamEqBJupiter(index2);
					float rate = paramConversionRateJupiter.HasValue ? paramConversionRateJupiter.Value : 1f;
					float a = paramEqAJupiter.HasValue ? paramEqAJupiter.Value : 1f;
					float b = paramEqBJupiter.HasValue ? paramEqBJupiter.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				result = sEParam.GetParamAsString(isConvert);
			}
			return result;
		}

		public string[] GetParamJupiter(JupiterParams[] jupiterParams)
		{
			return this.GetParamJupiter(jupiterParams, true);
		}

		public string[] GetParamJupiter(JupiterParams[] jupiterParams, bool isConvert)
		{
			List<string> list = null;
			int num = jupiterParams.Length;
			List<JupiterParams> list2 = new List<JupiterParams>(0);
			List<SEParam> list3 = new List<SEParam>(0);
			for (int i = 0; i < num; i++)
			{
				JupiterParams jupiterParams2 = jupiterParams[i];
				SEParam paramFromTable = this.Jupiter.GetParamFromTable((int)jupiterParams2);
				if (paramFromTable == null)
				{
					list2.Add(jupiterParams2);
				}
				else
				{
					list3.Add(paramFromTable);
				}
			}
			if (list2 != null && list2.Count > 0)
			{
				SEParam[] array = this.Jupiter.Command_Params_Get(list2.ToArray());
				if (array != null)
				{
					int num2 = array.Length;
					for (int i = 0; i < num2; i++)
					{
						SEParam item = array[i];
						list3.Add(item);
					}
				}
			}
			int num3 = (list3 != null) ? list3.Count : 0;
			for (int i = 0; i < num3; i++)
			{
				SEParam sEParam = list3[i];
				if (this.Jupiter.IsParamExist((int)sEParam.Index))
				{
					this.Jupiter.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueJupiter = instance.GetParameterMinValueJupiter((ushort)index);
					object parameterMaxValueJupiter = instance.GetParameterMaxValueJupiter((ushort)index);
					AccessLevels jupiterParamAccessLevelForWriting = instance.GetJupiterParamAccessLevelForWriting((ushort)index);
					this.Jupiter.AddParameterToDataTable(index, ((JupiterParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueJupiter, parameterMaxValueJupiter, (int)jupiterParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRateJupiter = instance2.GetParamConversionRateJupiter(index2);
					float? paramEqAJupiter = instance2.GetParamEqAJupiter(index2);
					float? paramEqBJupiter = instance2.GetParamEqBJupiter(index2);
					float rate = paramConversionRateJupiter.HasValue ? paramConversionRateJupiter.Value : 1f;
					float a = paramEqAJupiter.HasValue ? paramEqAJupiter.Value : 1f;
					float b = paramEqBJupiter.HasValue ? paramEqBJupiter.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				string paramAsString = sEParam.GetParamAsString(isConvert);
				if (list == null)
				{
					list = new List<string>(0);
				}
				list.Add(paramAsString);
			}
			return list.ToArray();
		}

		public override SWVersion GetDSPVersion()
		{
			return this.Jupiter.DeviceSWVersion;
		}

		public override SWVersion GetDSPPwrVersion()
		{
			return this.Jupiter.DeviceSWVersionPower;
		}

		public void SetParamJupiter(JupiterParams index, object value)
		{
			this.SetParamJupiter(index, value, true);
		}

		public void SetParamJupiter(JupiterParams index, object value, bool shouldValueBeConverted)
		{
			DBParameterData instance = DBParameterData.Instance;
			Type typeOfParamJupiter = instance.GetTypeOfParamJupiter((ushort)index);
			SEParam sEParam = new SEParam((ushort)index, value, typeOfParamJupiter);
			if (shouldValueBeConverted && sEParam.ParameterType != 3)
			{
				sEParam.SetValueConverted(base.RevertValue((int)sEParam.Index, double.Parse(value.ToString()), DeviceType.JUPITER));
			}
			if (this.Jupiter.IsParamExist((int)sEParam.Index))
			{
				this.Jupiter.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
			}
			else
			{
				object defaultVal = null;
				object parameterMinValueJupiter = instance.GetParameterMinValueJupiter(sEParam.Index);
				object parameterMaxValueJupiter = instance.GetParameterMaxValueJupiter(sEParam.Index);
				AccessLevels jupiterParamAccessLevelForWriting = instance.GetJupiterParamAccessLevelForWriting(sEParam.Index);
				this.Jupiter.AddParameterToDataTable((int)sEParam.Index, ((JupiterParams)sEParam.Index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueJupiter, parameterMaxValueJupiter, (int)jupiterParamAccessLevelForWriting);
			}
			this.Jupiter.Command_Params_Set(sEParam);
		}

		public void SetParamJupiter(JupiterParams[] indices, object[] values)
		{
			this.SetParamJupiter(indices, values, true);
		}

		public void SetParamJupiter(JupiterParams[] indices, object[] values, bool shouldValuesBeConverted)
		{
			List<SEParam> list = new List<SEParam>(0);
			int num = indices.Length;
			DBParameterData instance = DBParameterData.Instance;
			for (int i = 0; i < num; i++)
			{
				ushort num2 = (ushort)indices[i];
				object obj = values[i];
				Type typeOfParamJupiter = DBParameterData.Instance.GetTypeOfParamJupiter(num2);
				SEParam sEParam = new SEParam(num2, obj, typeOfParamJupiter);
				if (shouldValuesBeConverted && sEParam.ParameterType != 3)
				{
					sEParam.SetValueConverted(base.RevertValue((int)sEParam.Index, double.Parse(obj.ToString()), DeviceType.JUPITER));
				}
				if (this.Jupiter.IsParamExist((int)sEParam.Index))
				{
					this.Jupiter.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
				}
				else
				{
					DBParameterData instance2 = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueJupiter = instance2.GetParameterMinValueJupiter(sEParam.Index);
					object parameterMaxValueJupiter = instance2.GetParameterMaxValueJupiter(sEParam.Index);
					AccessLevels jupiterParamAccessLevelForWriting = instance2.GetJupiterParamAccessLevelForWriting(sEParam.Index);
					this.Jupiter.AddParameterToDataTable((int)sEParam.Index, ((JupiterParams)sEParam.Index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueJupiter, parameterMaxValueJupiter, (int)jupiterParamAccessLevelForWriting);
				}
				list.Add(sEParam);
			}
			this.Jupiter.Command_Params_Set(list.ToArray());
		}

		public override void UpdateStatusSpec()
		{
			base.UpdateStatusSpec();
			try
			{
				base.DataSystem.DSP1Version = this.Jupiter.DeviceSWVersion;
				base.DataSystem.DSP2Version = this.Jupiter.DeviceSWVersionPower;
			}
			catch (Exception var_0_3A)
			{
			}
		}

		public override void UpdateStatusLCD(bool withEnergy)
		{
			try
			{
				SETelemetrySystemStatusJupiter sETelemetrySystemStatusJupiter = this.Jupiter.Command_Jupiter_GetSysStatus();
				if (sETelemetrySystemStatusJupiter != null)
				{
					this.SystemStatusJupiter = sETelemetrySystemStatusJupiter;
				}
			}
			catch (Exception var_1_21)
			{
			}
			base.UpdateStatusLCD(withEnergy);
		}

		public override void UpdateSettingsRegional()
		{
			try
			{
				DBParameterData instance = DBParameterData.Instance;
				string paramJupiter = this.GetParamJupiter(JupiterParams.INV_COUNTRY_CODE);
				string paramPortia = base.GetParamPortia(PortiaParams.SETUP_LANGUAGE);
				int num = (!string.IsNullOrEmpty(paramJupiter)) ? int.Parse(paramJupiter) : -1;
				int num2 = (!string.IsNullOrEmpty(paramPortia)) ? int.Parse(paramPortia) : -1;
				DateTime currentTime = new DateTime(1970, 1, 1, 0, 0, 0);
				try
				{
					string paramPortia2 = base.GetParamPortia(PortiaParams.SYSTEM_LOCAL_TIME);
					int seconds = (!string.IsNullOrEmpty(paramPortia2)) ? int.Parse(paramPortia2) : 0;
					TimeSpan value = new TimeSpan(0, 0, seconds);
					currentTime = currentTime.Add(value);
				}
				catch (Exception var_9_8E)
				{
				}
				DateTime rTCTime = new DateTime(1970, 1, 1, 0, 0, 0);
				try
				{
					string paramPortia3 = base.GetParamPortia(PortiaParams.RTC_TIME);
					int seconds2 = (!string.IsNullOrEmpty(paramPortia3)) ? int.Parse(paramPortia3) : 0;
					TimeSpan value2 = new TimeSpan(0, 0, seconds2);
					rTCTime = rTCTime.Add(value2);
				}
				catch (Exception var_9_8E)
				{
				}
				SEParam sEParam = base.Portia.Command_Params_Get(PortiaParams.RTC_GMT_OFFSET);
				int gMTOffset = 0;
				try
				{
					gMTOffset = sEParam.ValueInt32;
				}
				catch (Exception var_16_10A)
				{
				}
				base.DataRegional.GMTOffset = gMTOffset;
				if (num > -1 || num2 > -1)
				{
					RegionalData regionalData = new RegionalData(this);
					regionalData.CurrentTime = currentTime;
					regionalData.RTCTime = rTCTime;
					regionalData.GMTOffset = gMTOffset;
					regionalData.CountryIndex = num;
					regionalData.LanguageIndex = num2;
					instance.PopulateRegionalData(regionalData);
					base.DataRegional.CopyData(regionalData);
				}
			}
			catch (Exception var_16_10A)
			{
			}
		}

		protected override void UpdateParameterTableCallBack()
		{
			DBParameterData instance = DBParameterData.Instance;
			try
			{
				if (base.Portia != null && this.Jupiter != null)
				{
					DataManager instance2 = DataManager.Instance;
					List<ushort> portiaParamListToView = instance.GetPortiaParamListToView(instance2.LoginItem.AccessLevel);
					List<ushort> jupiterParamListToView = instance.GetJupiterParamListToView(instance2.LoginItem.AccessLevel);
					Dictionary<int, string[]> dictionary = new Dictionary<int, string[]>(0);
					Dictionary<int, string[]> dictionary2 = new Dictionary<int, string[]>(0);
					this.UpdateParams(portiaParamListToView, jupiterParamListToView, dictionary, dictionary2);
					this._lastUpdatedCPUData = dictionary;
					this._lastUpdatedDSPData = dictionary2;
					DataManager.Instance.UIOwner.UpdateInverterParameters(dictionary, dictionary2);
				}
			}
			catch (Exception var_6_95)
			{
			}
		}

		protected override void UpdateParams(List<ushort> cpuIndexList, List<ushort> dspIndexList, Dictionary<int, string[]> cpuDataList, Dictionary<int, string[]> dspDataList)
		{
			int num = ((cpuIndexList != null) ? cpuIndexList.Count : 0) + ((dspIndexList != null) ? dspIndexList.Count : 0);
			int num2 = 0;
			base.UpdateCPUParams(cpuIndexList, cpuDataList, ref num2);
			this.UpdateDSPParams(dspIndexList, dspDataList, ref num2);
			DataManager.Instance.UIOwner.UpdateProgressBar("", (float)num);
		}

		private void UpdateDSPParams(List<ushort> dspList, Dictionary<int, string[]> dspDataList, ref int numParamsRetreived)
		{
			DataManager instance = DataManager.Instance;
			int num = (dspList != null) ? dspList.Count : 0;
			List<JupiterParams> list = new List<JupiterParams>(0);
			for (int i = 0; i < num; i++)
			{
				int num2 = (int)dspList[i];
				list.Add((JupiterParams)num2);
				this.Jupiter.ClearTableData(num2);
			}
			if (list != null && list.Count > 0)
			{
				string[] paramJupiter = this.GetParamJupiter(list.ToArray());
				int num3 = list.Count;
				num3 = Math.Min(num3, paramJupiter.Length);
				for (int i = 0; i < num3; i++)
				{
					ushort num4 = dspList[i];
					string text = list[i].ToString();
					string text2 = paramJupiter[i];
					string paramUnitKeyJupiter = DBParameterData.Instance.GetParamUnitKeyJupiter(num4);
					dspDataList.Add((int)num4, new string[]
					{
						text,
						text2,
						paramUnitKeyJupiter
					});
				}
				int num5 = paramJupiter.Length;
				numParamsRetreived += num5;
				instance.UIOwner.UpdateProgressBar(string.Format(DataManager.Instance.Dictionary["MsgUpdateParamDSP"], new object[]
				{
					num5
				}), (float)numParamsRetreived);
			}
		}

		public override string GetLineStatusValue()
		{
			float vin = this.SystemStatusJupiter.Vin;
			float num = (this.SystemStatusJupiter.Vout.Phase_1 + this.SystemStatusJupiter.Vout.Phase_2 + this.SystemStatusJupiter.Vout.Phase_3) / 3f;
			float num2 = this.SystemStatusJupiter.Power.Phase_1 + this.SystemStatusJupiter.Power.Phase_2 + this.SystemStatusJupiter.Power.Phase_3;
			string text = string.Format("{0:0.0}", (double)num);
			string text2 = string.Format("{0:0.0}", (double)vin);
			string text3 = string.Format("{0:0.0}", (double)num2);
			int length = text.Length;
			int length2 = text2.Length;
			int length3 = text3.Length;
			int num3 = 6 - text.Length;
			int num4 = 6 - text2.Length;
			int num5 = 6 - text3.Length;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(text);
			for (int i = 0; i < num3; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append("#");
			int num6 = num4 / 2;
			for (int i = 0; i < num6; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			if (num6 > 0)
			{
				int num7 = num4 - num6;
				for (int i = 0; i < num7; i++)
				{
					stringBuilder.Append("#");
				}
			}
			stringBuilder.Append("#");
			for (int i = 0; i < num5; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text3);
			return stringBuilder.ToString();
		}

		public override string GetLinePowerOnOffSwitch()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string text = (this.SystemStatusJupiter.OnOffSwitch == 0) ? "OFF" : "ON";
			int length = text.Length;
			int num = 20 - length;
			for (int i = 0; i < num; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text);
			return stringBuilder.ToString();
		}

		public override string GetLineFacTempStatusValue()
		{
			StringBuilder stringBuilder = new StringBuilder();
			float num = (this.SystemStatusJupiter.Freq.Phase_1 + this.SystemStatusJupiter.Freq.Phase_2 + this.SystemStatusJupiter.Freq.Phase_3) / 3f;
			float temp = this.SystemStatusJupiter.Temp;
			string text = (num > -3.40282347E+38f && num < 3.40282347E+38f) ? string.Format("{0:0.##}", (double)num) : "---";
			string text2 = (temp > -3.40282347E+38f && temp < 3.40282347E+38f) ? string.Format("{0:0.##}", (double)temp) : "---";
			int length = text.Length;
			int length2 = text2.Length;
			int num2 = 7 - length;
			int num3 = 5 - length2;
			stringBuilder.Append(text);
			for (int i = 0; i < num2; i++)
			{
				stringBuilder.Append("#");
			}
			int num4 = 20 - length - num2 - length2;
			for (int i = 0; i < num4; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			return stringBuilder.ToString();
		}

		public override void Action_UpdateParameterTable(int numOfParams, bool shouldStartNewProgressBar)
		{
			DataManager instance = DataManager.Instance;
			DBParameterData instance2 = DBParameterData.Instance;
			List<ushort> portiaParamListToView = instance2.GetPortiaParamListToView(instance.LoginItem.AccessLevel);
			List<ushort> jupiterParamListToView = instance2.GetJupiterParamListToView(instance.LoginItem.AccessLevel);
			int endValue = ((portiaParamListToView != null) ? portiaParamListToView.Count : 0) + ((jupiterParamListToView != null) ? jupiterParamListToView.Count : 0);
			if (shouldStartNewProgressBar)
			{
				instance.UIOwner.ShowProgressBar(0, endValue, new SEProgressBar.ProgressFormCallBack(this.UpdateParameterTableCallBack), instance.UIOwner);
			}
			else
			{
				this.UpdateParameterTableCallBack();
			}
		}

		public override void Action_Set_Country(uint newCountryIndex)
		{
			this.Jupiter.Command_Jupiter_SetCountry(newCountryIndex);
			this.Jupiter.Command_Device_Reset(0u);
			base.SetParamPortia(PortiaParams.RESERVED1, newCountryIndex);
			base.Portia.Command_Device_Reset(0u);
			this.Action_UpdateParameterTable(0, true);
		}
	}
}
