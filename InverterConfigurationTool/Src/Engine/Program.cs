using InverterConfigurationTool.Src.Ui.Main;
using System;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Engine
{
	internal static class Program
	{
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			MainForm instance = MainForm.Instance;
			Application.Run(instance);
		}
	}
}
