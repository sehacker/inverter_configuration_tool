using InverterConfigurationTool.Src.Data;
using InverterConfigurationTool.Src.Model.Devices;
using InverterConfigurationTool.Src.Model.Devices.Base;
using InverterConfigurationTool.Src.Ui.Main;
using SEComm.Connections;
using SEComm.Connections.Types;
using SEDevices.Devices;
using SEDevices.Records.Parameter;
using SEDevices.Upgrade;
using SEProtocol.SDP;
using SESecurity;
using SEStorage;
using SEUI.Common;
using SEUtils.Common;
using SEUtils.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Engine
{
	public class DataManager
	{
		private const string MASTER_INDICATOR = " ( M )";

		private const string LOCALE_EN_US = "EN_US";

		public const string US_KEY_SHOW_CON_ON_START = "ShowConnectionFormOnStart";

		public const string US_KEY_LAST_CONN_PARAMS = "LastConnectionParams";

		public const string US_KEY_CONF_CAB = "ShouldConfigureCabinet";

		public const string US_KEY_SELECTED_LANG = "SelectedLanguage";

		private const int TIMER_GENERAL_INTERVAL_IN_MILISECS = 1000;

		public const string TABLE_NAME_INVERTER_LIST = "tableNameInverterList";

		public const string COLUMN_NAME_INVERTER_LIST_ID = "colInverterID";

		public const string COLUMN_NAME_INVERTER_LIST_HEXID = "colInverterHEXID";

		public const string COLUMN_NAME_INVERTER_LIST_CHECKSUM = "colInverterCHECKSUM";

		private const int DATA_TABLE_PORTIA = 0;

		private const int DATA_TABLE_VENUS = 1;

		public static SWVersion PORTIA_SW_VERSION_2_0023 = new SWVersion(2u, 23u);

		public static SWVersion PORTIA_SW_VERSION_2_0038 = new SWVersion(2u, 38u);

		public static SWVersion PORTIA_SW_VERSION_3_0080 = new SWVersion(3u, 80u);

		private ManualResetEvent _notifyTaskArrived = null;

		private static DataManager _instance;

		private SWVersion _version = null;

		private SEUserSettings _userSettings = null;

		private SEDictionary _dictionary;

		private FirmwareUpgrader _upgrader = null;

		private MainForm _control = null;

		private LoginItem _loginItem;

		private SEThread _thTimerTask = null;

		private List<Dictionary<TimerTasks, object[]>> _taskQueue = new List<Dictionary<TimerTasks, object[]>>(0);

		private int _selectedInverterIndex = -1;

		private List<Inverter> _inverters = null;

		private Dictionary<string, CommManager> _commManagers;

		private object _countryDataSynchObj = new object();

		private DataTable _countryData = null;

		private object _languageDataSynchObj = new object();

		private DataTable _languageData = null;

		private ConnectionParams _cp = null;

		public static DataManager Instance
		{
			get
			{
				if (DataManager._instance == null)
				{
					DataManager._instance = new DataManager();
				}
				return DataManager._instance;
			}
		}

		public SWVersion AppVersion
		{
			get
			{
				if (this._version == null)
				{
					this._version = new SWVersion();
					Version version = Assembly.GetExecutingAssembly().GetName().Version;
					this._version.Major = (uint)version.Major;
					this._version.Minor = (uint)version.Minor;
					this._version.Build = (uint)version.Build;
					this._version.Revision = (uint)version.Revision;
				}
				return this._version;
			}
		}

		public SEUserSettings UserSettings
		{
			get
			{
				if (this._userSettings == null)
				{
					this._userSettings = new SEUserSettings(Paths.DATA_SOURCE_PATH);
				}
				return this._userSettings;
			}
		}

		public SEDictionary Dictionary
		{
			get
			{
				return this._dictionary;
			}
		}

		public FirmwareUpgrader FirmwareUpgrader
		{
			get
			{
				if (this._upgrader == null)
				{
					this._upgrader = new FirmwareUpgrader();
					this._upgrader.UpgradeFilesDirectory = Paths.DATA_SOURCE_PATH + "\\UpgradeData";
				}
				return this._upgrader;
			}
		}

		public MainForm UIOwner
		{
			get
			{
				return this._control;
			}
			set
			{
				this._control = value;
			}
		}

		public LoginItem LoginItem
		{
			get
			{
				return this._loginItem;
			}
			set
			{
				this._loginItem = value;
			}
		}

		public int SelectedInverterIndex
		{
			get
			{
				return this._selectedInverterIndex;
			}
			set
			{
				if (this._selectedInverterIndex != value)
				{
					Inverter selectedInverter = this.GetSelectedInverter();
					if (selectedInverter != null)
					{
						selectedInverter.IsSelected = false;
						selectedInverter.DataReady = false;
					}
					this._selectedInverterIndex = value;
					selectedInverter = this.GetSelectedInverter();
					if (selectedInverter != null)
					{
						selectedInverter.IsSelected = true;
						selectedInverter.DataReady = true;
					}
				}
			}
		}

		public List<Inverter> Inverters
		{
			get
			{
				return this._inverters;
			}
		}

		public bool Connected
		{
			get
			{
				bool flag = false;
				bool flag2 = this._commManagers != null && this._commManagers.Count > 0;
				if (flag2)
				{
					int count = this._commManagers.Count;
					Dictionary<string, CommManager>.KeyCollection.Enumerator enumerator = this._commManagers.Keys.GetEnumerator();
					while (enumerator.MoveNext())
					{
						string current = enumerator.Current;
						CommManager commManager = this._commManagers[current];
						flag = (commManager != null && commManager.IsConnected);
						if (flag)
						{
							break;
						}
					}
				}
				return flag;
			}
		}

		public DataTable CountryData
		{
			get
			{
				DataTable countryData;
				lock (this._countryDataSynchObj)
				{
					countryData = this._countryData;
				}
				return countryData;
			}
		}

		public DataTable LanguageData
		{
			get
			{
				DataTable languageData;
				lock (this._languageDataSynchObj)
				{
					languageData = this._languageData;
				}
				return languageData;
			}
		}

		private DataManager()
		{
			this.StartData();
			this.StartThreadControl();
		}

		private void StartThreadControl()
		{
			this._thTimerTask = new SEThread("DataManager's Timer Task Thread", new SEThread.ThreadFunctionCallBack(this.TimerTaskTick), 500, 0u, 0u, true, true, false);
		}

		private void ShutDownThreadControl()
		{
			try
			{
				this._thTimerTask.Stop = true;
				this._thTimerTask.Dispose();
			}
			catch (Exception var_0_1E)
			{
			}
			finally
			{
				this._thTimerTask = null;
			}
		}

		private void StartData()
		{
			string name = "InverterConfigurationTool.Resources.Languages.TranslationFile.xlsm";
			Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
			this._dictionary = new SEDictionary(manifestResourceStream);
			string text = (this.UserSettings["SelectedLanguage"] != null) ? ((string)this.UserSettings["SelectedLanguage"]) : null;
			this._dictionary.SelectedLanguage = (string.IsNullOrEmpty(text) ? "English" : text);
			this.LoginItem = new LoginItem(AccessLevels.MONITORING);
			this.LoginItem.ShouldValidateAdminOffline = true;
		}

		private void CloseData()
		{
			this._dictionary = null;
		}

		private void StartCommThreadFunc()
		{
			this.StartComm(this._cp);
			this._cp = null;
		}

		public void Connect(ConnectionParams cp)
		{
			this.Task_StartComm(cp);
		}

		public void Disconnect()
		{
			this.Task_CloseComm();
		}

		private void StartComm(ConnectionParams cp)
		{
			try
			{
				SEDictionary dictionary = DataManager.Instance.Dictionary;
				if (cp == null)
				{
					this.UIOwner.UpdateProgressBar("", 100f);
				}
				else
				{
					this.UIOwner.UpdateProgressBar(dictionary["prgrsFormStartCommStage1Begin"], 10f);
					this.UIOwner.IsScreenWaitingForTransmition = true;
					this.AddNewConnectionManager(cp);
					this.UIOwner.UpdateProgressBar(dictionary["prgrsFormStartCommStage1Done"], 20f);
					this.UIOwner.SetLCDUpdate(!cp.RemoteConnection);
					this.UIOwner.UpdateProgressBar(dictionary["prgrsFormStartCommStage2Begin"], 30f);
					this.DetectDevices();
					this.UIOwner.UpdateProgressBar(dictionary["prgrsFormStartCommStage2Done"], 80f);
					Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
					if (selectedInverter != null)
					{
						selectedInverter.Action_Set_Exclusive_Control(true);
					}
					this.UIOwner.UpdateProgressBar("", 100f);
					MainForm.Instance.EnableUI();
					Application.DoEvents();
					this.UIOwner.IsScreenWaitingForTransmition = false;
					this.UIOwner.SwitchScreen(ScreenName.SE_TAB_PAGE_STATUS_NAME, true);
				}
			}
			catch (Exception ex)
			{
				try
				{
					this.UIOwner.UpdateProgressBar("", 100f);
					this.Disconnect();
					if (ex.Message.StartsWith("Access to the port"))
					{
						int num = ex.Message.IndexOf("'");
						int num2 = ex.Message.IndexOf("'", num + 1);
						int length = num2 - num - 1;
						string arg = ex.Message.Substring(num + 1, length);
						throw new SEException(string.Format(DataManager.Instance.Dictionary["MsgExcepAccessToPortDenied"], arg));
					}
					throw new SEException(DataManager.Instance.Dictionary["MsgExcepUnableToEstablishConnection"]);
				}
				catch (SEException)
				{
					string message = ex.Message;
					string caption = this.Dictionary["MsgCaptionError"];
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void CloseComm()
		{
			this.ClearDevices(false);
			if (this._commManagers != null)
			{
				Dictionary<string, CommManager>.KeyCollection.Enumerator enumerator = this._commManagers.Keys.GetEnumerator();
				while (enumerator.MoveNext())
				{
					string current = enumerator.Current;
					this.ShutDownConnectionManager(current);
				}
				this._commManagers.Clear();
				this._commManagers = null;
			}
			try
			{
				this.UIOwner.IsScreenWaitingForTransmition = false;
			}
			catch (Exception var_2_6B)
			{
			}
		}

		private void AddNewConnectionManager(ConnectionParams cp)
		{
			if (cp != null)
			{
				if (this._commManagers == null)
				{
					this._commManagers = new Dictionary<string, CommManager>(0);
				}
				CommManager commManager = new CommManager(cp);
				commManager.StartConnection();
				this._commManagers.Add(cp.Name, commManager);
			}
		}

		private void ShutDownConnectionManager(string name)
		{
			if (name != null && this._commManagers != null && this._commManagers.Count != 0)
			{
				CommManager commManager = null;
				if (this._commManagers.ContainsKey(name))
				{
					commManager = this._commManagers[name];
				}
				if (commManager != null)
				{
					commManager.CloseConnection();
				}
			}
		}

		public Inverter GetSelectedInverter()
		{
			return this.GetInverterByIndex(this.SelectedInverterIndex);
		}

		public Inverter GetInverterByIndex(int inverterIndex)
		{
			Inverter result = null;
			if (this.Inverters != null && inverterIndex >= 0 && inverterIndex < this.Inverters.Count)
			{
				result = this.Inverters[inverterIndex];
			}
			return result;
		}

		public Inverter GetInverterByID(int invID)
		{
			Inverter result = null;
			if (this.Inverters != null)
			{
				int count = this.Inverters.Count;
				for (int i = 0; i < count; i++)
				{
					Inverter inverter = this.Inverters[i];
					if (inverter != null && inverter.Portia != null && (ulong)inverter.Portia.Address == (ulong)((long)invID))
					{
						result = inverter;
						break;
					}
				}
			}
			return result;
		}

		public int GetInverterIndexByID(int invID)
		{
			int result = -1;
			if (this.Inverters != null)
			{
				int count = this.Inverters.Count;
				for (int i = 0; i < count; i++)
				{
					Inverter inverter = this.Inverters[0];
					if (inverter != null && inverter.Portia != null && (ulong)inverter.Portia.Address == (ulong)((long)invID))
					{
						result = i;
						break;
					}
				}
			}
			return result;
		}

		public bool DoesInverterExists(uint inverterID)
		{
			bool result = false;
			int num = (this.Inverters != null) ? this.Inverters.Count : 0;
			for (int i = 0; i < num; i++)
			{
				Inverter inverter = this.Inverters[i];
				if (inverter.Portia != null && inverterID == inverter.Portia.Address)
				{
					result = true;
					break;
				}
			}
			return result;
		}

		public void DetectDevices()
		{
			if (this.Inverters == null)
			{
				this._inverters = new List<Inverter>(0);
			}
			Dictionary<string, CommManager>.ValueCollection.Enumerator enumerator = this._commManagers.Values.GetEnumerator();
			while (enumerator.MoveNext())
			{
				CommManager current = enumerator.Current;
				uint? id = null;
				object obj = DataManager.Instance.UserSettings["LastConnectionParams"];
				ConnectionParams connectionParams = (obj != null) ? ((ConnectionParams)obj) : null;
				if (connectionParams != null && connectionParams.ConnectionType.Equals(typeof(PortReplicatorConnection)))
				{
					id = (uint?)connectionParams.Parameters[2];
				}
				SEPortia sEPortia = new SEPortia(current, id, null, null);
				sEPortia.SetBroadcast();
				sEPortia.Command_Portia_ConfToolStart(true);
				sEPortia.SetOriginalAddress();
				SEParam sEParam = sEPortia.Command_Params_Get(PortiaParams.POLESTAR_MODE);
				Inverter inverter;
				switch ((sEParam != null) ? sEParam.ValueInt32 : 0)
				{
				case 0:
					goto IL_104;
				case 1:
					inverter = new Inverter3Phase(sEPortia);
					break;
				default:
					goto IL_104;
				}
				IL_110:
				sEPortia.Owner = inverter;
				if (inverter != null)
				{
					inverter.StartInverter();
					if (inverter.Portia != null)
					{
						this.Inverters.Add(inverter);
						inverter.Index = this.Inverters.Count - 1;
						bool flag = inverter.CanSustainSlaves(true);
						if (flag)
						{
							this.DetectSlavesForDevice(inverter);
						}
					}
				}
				continue;
				IL_104:
				inverter = new Inverter1Phase(sEPortia);
				goto IL_110;
			}
			this.UIOwner.UpdateInverterList = true;
			if (this.Inverters != null && this.Inverters.Count > 0)
			{
				this.SelectedInverterIndex = 0;
			}
		}

		public void DetectSlavesForDevice(Inverter inverter)
		{
			inverter.PauseAllThreadControl();
			List<Inverter> slaves = inverter.Slaves;
			int num = (slaves != null) ? slaves.Count : 0;
			for (int i = 0; i < num; i++)
			{
				Inverter inverter2 = inverter.Slaves[i];
				if (inverter2.Portia != null && !this.DoesInverterExists(inverter2.Portia.Address))
				{
					this.Inverters.Add(inverter2);
					inverter2.Index = this.Inverters.Count - 1;
				}
			}
			inverter.ResumeLastThreadControlStatus();
		}

		public void ClearDevices(bool slaveOnly)
		{
			if (this.Inverters != null)
			{
				int count = this.Inverters.Count;
				for (int i = 0; i < count; i++)
				{
					try
					{
						Inverter inverter = this._inverters[i];
						if (inverter.Slaves != null)
						{
							try
							{
								inverter.ReleaseSlaves();
							}
							catch (Exception var_3_52)
							{
							}
						}
						if (!slaveOnly)
						{
							try
							{
								inverter.ShutdownInverter();
								inverter = null;
							}
							catch (Exception var_3_52)
							{
							}
						}
					}
					catch (Exception var_3_52)
					{
					}
				}
				if (slaveOnly)
				{
					this.SelectedInverterIndex = 0;
					while (this.Inverters.Count > 1)
					{
						this.Inverters.RemoveAt(1);
					}
				}
				else
				{
					this.SelectedInverterIndex = -1;
					this.Inverters.Clear();
					this._inverters = null;
				}
			}
		}

		private void AddTask(TimerTasks task, object[] dataAssociated, bool withPerform)
		{
			lock (this._taskQueue)
			{
				Dictionary<TimerTasks, object[]> dictionary = new Dictionary<TimerTasks, object[]>(0);
				dictionary.Add(task, dataAssociated);
				this._taskQueue.Add(dictionary);
				if (withPerform)
				{
					this._notifyTaskArrived.Set();
				}
			}
		}

		private void TimerTaskTick()
		{
			if (this._notifyTaskArrived == null)
			{
				this._notifyTaskArrived = new ManualResetEvent(false);
			}
			this._notifyTaskArrived.Reset();
			this._notifyTaskArrived.WaitOne(-1);
			lock (this._taskQueue)
			{
				while (this._taskQueue.Count > 0)
				{
					try
					{
						Dictionary<TimerTasks, object[]> dictionary = this._taskQueue[0];
						this._taskQueue.RemoveAt(0);
						TimerTasks timerTasks = dictionary.Keys.ElementAt(0);
						this.HandleTask(timerTasks, dictionary[timerTasks]);
					}
					catch (SEException ex)
					{
						string message = ex.Message;
						string caption = this.Dictionary["MsgCaptionError"];
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
					}
					catch (Exception var_5_B0)
					{
					}
				}
			}
		}

		private void HandleTask(TimerTasks task, object[] data)
		{
			switch (task)
			{
			case TimerTasks.COMM_START:
				try
				{
					ConnectionParams cp = (ConnectionParams)data[0];
					this._cp = cp;
					this.UIOwner.ShowProgressBar(0, 100, new SEProgressBar.ProgressFormCallBack(this.StartCommThreadFunc));
				}
				catch (Exception ex)
				{
					this.UIOwner.UpdateProgressBar("", 100f);
					this.Disconnect();
					if (ex.Message.StartsWith("Access to the port"))
					{
						int num = ex.Message.IndexOf("'");
						int num2 = ex.Message.IndexOf("'", num + 1);
						int length = num2 - num - 1;
						string arg = ex.Message.Substring(num + 1, length);
						throw new SEException(string.Format(DataManager.Instance.Dictionary["MsgExcepAccessToPortDenied"], arg));
					}
					throw new SEException(DataManager.Instance.Dictionary["MsgExcepUnableToEstablishConnection"]);
				}
				break;
			case TimerTasks.COMM_CLOSE:
				this.CloseComm();
				break;
			case TimerTasks.WAIT_500:
			case TimerTasks.WAIT_1000:
			case TimerTasks.WAIT_2000:
			{
				string text = task.ToString();
				string s = text.Substring(text.IndexOf("_") + 1);
				int millisecondsTimeout = int.Parse(s);
				Thread.Sleep(millisecondsTimeout);
				break;
			}
			}
		}

		public void Task_StartComm(ConnectionParams cp)
		{
			this.AddTask(TimerTasks.COMM_START, new object[]
			{
				cp
			}, true);
		}

		public void Task_CloseComm()
		{
			this.AddTask(TimerTasks.COMM_CLOSE, null, true);
		}
	}
}
