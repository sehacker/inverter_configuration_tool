using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace InverterConfigurationTool.Properties
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	internal class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(Resources.resourceMan, null))
				{
					ResourceManager resourceManager = new ResourceManager("InverterConfigurationTool.Properties.Resources", typeof(Resources).Assembly);
					Resources.resourceMan = resourceManager;
				}
				return Resources.resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Resources.resourceCulture;
			}
			set
			{
				Resources.resourceCulture = value;
			}
		}

		internal static Bitmap _1Inv
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("_1Inv", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap _3Inv
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("_3Inv", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackGrad
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackGrad", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackGridDark
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackGridDark", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackGridLight
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackGridLight", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackLandBigNew
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackLandBigNew", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackLandMidNew
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackLandMidNew", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackLandNew
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackLandNew", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackLandSmallNew
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackLandSmallNew", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackLandTinyNew
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackLandTinyNew", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BackPortNew
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("BackPortNew", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap buttonOff
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("buttonOff", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap buttonOn
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("buttonOn", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap c9PinSerialPlug
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("c9PinSerialPlug", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap c9PinSerialPlugDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("c9PinSerialPlugDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Cloud
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Cloud", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap CloudDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("CloudDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Connect
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Connect", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap DataBase
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("DataBase", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Disconnect
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Disconnect", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Exit
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Exit", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap File
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("File", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap gsm_antenna_1
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("gsm_antenna_1", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Help
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Help", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Icon ICTIcon
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ICTIcon", Resources.resourceCulture);
				return (Icon)@object;
			}
		}

		internal static Bitmap InverterPanel3
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("InverterPanel3", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Inverters
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Inverters", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Inverters3Phase
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Inverters3Phase", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Inverters3PhaseDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Inverters3PhaseDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap InvertersDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("InvertersDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap LAN
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("LAN", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap LAN2
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("LAN2", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap LAN2Disabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("LAN2Disabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap LANDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("LANDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap LightProgressBar
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("LightProgressBar", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Options
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Options", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap PanelLight
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("PanelLight", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap PanelRegular
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("PanelRegular", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Password
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Password", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap PowerLightErr
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("PowerLightErr", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap PowerLightOK
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("PowerLightOK", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap PowerLightWarn
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("PowerLightWarn", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ProgressFormBack
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ProgressFormBack", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Refresh
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Refresh", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap RS485
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("RS485", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap RS485Disabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("RS485Disabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap SELogo
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("SELogo", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Server
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Server", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ServerDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ServerDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap SolarEdgeLogo
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("SolarEdgeLogo", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap StatusConnected
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("StatusConnected", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap StatusDisconnected
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("StatusDisconnected", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap StatusOffline
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("StatusOffline", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap StatusOnline
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("StatusOnline", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap USB
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("USB", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap USBDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("USBDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ValidationError
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ValidationError", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ValidationOK
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ValidationOK", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ValidationWarning
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ValidationWarning", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ZigBee
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ZigBee", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ZigBeeDisabled
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("ZigBeeDisabled", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal Resources()
		{
		}
	}
}
