using SEProtocol.Utils;
using System;
using System.IO;

namespace SEProtocol.PLC.Packets.Base
{
	public abstract class PLCPacket
	{
		private ushort _opCode = 0;

		protected uint _maxDataBufferLength = 0u;

		private MemoryStream _buffer = null;

		private BinaryWriter _writer = null;

		protected ushort OpCode
		{
			get
			{
				return this._opCode;
			}
			set
			{
				this._opCode = value;
			}
		}

		protected MemoryStream Buffer
		{
			get
			{
				return this._buffer;
			}
		}

		protected BinaryWriter Writer
		{
			get
			{
				return this._writer;
			}
		}

		public virtual byte? ExpectedResponse
		{
			get
			{
				return null;
			}
		}

		public PLCPacket(ushort opCode, uint maxDataBufferLength)
		{
			this._maxDataBufferLength = maxDataBufferLength;
			this._opCode = opCode;
		}

		public byte[] GetTransmition()
		{
			this._buffer = new MemoryStream(0);
			this._writer = new BinaryWriter(this._buffer);
			this._writer.Write(0);
			this._writer.Write((byte)this.OpCode);
			byte[] dataTransmition = this.GetDataTransmition();
			this._writer.Write(dataTransmition, 0, dataTransmition.Length);
			ushort checkSum = this.GetCheckSum(this.Buffer.ToArray(), 1, this._buffer.Position);
			this._writer.Write(checkSum);
			this._writer.Flush();
			byte[] array = this._buffer.ToArray();
			array[0] = this.GetLength(dataTransmition.Length);
			return array;
		}

		protected abstract byte[] GetDataTransmition();

		protected byte GetHeaderLength()
		{
			return 1;
		}

		protected virtual byte GetAvailableDataLength()
		{
			return Convert.ToByte(this._maxDataBufferLength) - this.GetHeaderLength() - this.GetFooterLength();
		}

		private byte GetFooterLength()
		{
			return 2;
		}

		private byte GetLength(int dataLength)
		{
			return (byte)((int)this.GetHeaderLength() + dataLength + (int)this.GetFooterLength());
		}

		private ushort GetCheckSum(byte[] dataToCheck, int from, long to)
		{
			ushort num = Crc16.INIT_VAL;
			int num2 = from;
			while ((long)num2 < to)
			{
				num = Crc16.CalcByte(num, dataToCheck[num2]);
				num2++;
			}
			return num;
		}
	}
}
