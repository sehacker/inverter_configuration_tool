using System;
using System.Threading;

namespace SEProtocol.PLC.Packets.Base
{
	public class PLCDirectoryCard
	{
		private ManualResetEvent _wakeEvent = null;

		private byte[] _response;

		public ManualResetEvent WakeEvent
		{
			get
			{
				return this._wakeEvent;
			}
			set
			{
				this._wakeEvent = value;
			}
		}

		public byte[] Response
		{
			get
			{
				return this._response;
			}
			set
			{
				this._response = value;
			}
		}
	}
}
