using SEProtocol.PLC.Packets.Base;
using System;
using System.Collections.Generic;
using System.IO;

namespace SEProtocol.PLC.Packets
{
	public class PLCPacketParamGet : PLCPacket
	{
		private List<ushort> _indices = null;

		private uint _src = 0u;

		private uint _dst = 0u;

		protected List<ushort> Indices
		{
			get
			{
				return this._indices;
			}
		}

		protected uint Source
		{
			get
			{
				return this._src;
			}
		}

		protected uint Destination
		{
			get
			{
				return this._dst;
			}
		}

		public override byte? ExpectedResponse
		{
			get
			{
				return new byte?(17);
			}
		}

		public PLCPacketParamGet(uint maxDataBufferLength, List<ushort> indices, uint src, uint dst) : this(3, maxDataBufferLength, indices, src, dst)
		{
		}

		protected PLCPacketParamGet(ushort opCode, uint maxDataBufferLength, List<ushort> indices, uint src, uint dst) : base(opCode, maxDataBufferLength)
		{
			this._indices = indices;
			this._src = src;
			this._dst = dst;
		}

		protected override byte[] GetDataTransmition()
		{
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(this.Source);
			binaryWriter.Write(this.Destination);
			int num = (this.Indices != null) ? this.Indices.Count : 0;
			byte availableDataLength = this.GetAvailableDataLength();
			int val = (int)(availableDataLength / 2);
			num = Math.Min(num, val);
			for (int i = 0; i < num; i++)
			{
				binaryWriter.Write(this.Indices[i]);
			}
			binaryWriter.Flush();
			return memoryStream.ToArray();
		}
	}
}
