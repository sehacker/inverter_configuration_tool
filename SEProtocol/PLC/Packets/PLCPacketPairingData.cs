using SEProtocol.PLC.Packets.Base;
using System;
using System.IO;

namespace SEProtocol.PLC.Packets
{
	public class PLCPacketPairingData : PLCPacket
	{
		private uint _src = 0u;

		protected uint Source
		{
			get
			{
				return this._src;
			}
		}

		public PLCPacketPairingData(uint maxDataBufferLength, uint src) : base(9, maxDataBufferLength)
		{
			this._src = src;
		}

		protected override byte[] GetDataTransmition()
		{
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(this.Source);
			binaryWriter.Flush();
			return memoryStream.ToArray();
		}
	}
}
