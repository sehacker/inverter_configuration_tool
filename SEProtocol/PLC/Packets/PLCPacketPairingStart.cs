using SEProtocol.PLC.Packets.Base;
using System;
using System.IO;

namespace SEProtocol.PLC.Packets
{
	public class PLCPacketPairingStart : PLCPacket
	{
		private uint _src = 0u;

		private uint _dst = 0u;

		private byte _freqs = 0;

		protected uint Source
		{
			get
			{
				return this._src;
			}
		}

		protected uint Destination
		{
			get
			{
				return this._dst;
			}
		}

		protected byte Frequencies
		{
			get
			{
				return this._freqs;
			}
		}

		public PLCPacketPairingStart(uint maxDataBufferLength, uint src, uint dst, byte freqs) : base(8, maxDataBufferLength)
		{
			this._src = src;
			this._dst = dst;
			this._freqs = freqs;
		}

		protected override byte[] GetDataTransmition()
		{
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(this.Source);
			binaryWriter.Write(this.Destination);
			binaryWriter.Write(this.Frequencies);
			binaryWriter.Flush();
			return memoryStream.ToArray();
		}
	}
}
