using SEProtocol.SDP;
using System;

namespace SEProtocol.PLC
{
	public interface ISEPLCCommandable
	{
		void Command_PLC_Transmit(byte[] dataToSend, bool isTransmitOnly);

		uint GetAddress();

		CommManager GetCommManager();
	}
}
