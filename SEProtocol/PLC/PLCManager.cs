using SEProtocol.PLC.Packets.Base;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace SEProtocol.PLC
{
	public class PLCManager : ISECommNotifiable
	{
		private const bool ADD_COPY_TO_QUEUE = true;

		private const bool ADD_REAL_TO_QUEUE = false;

		private const int GENERAL_COUNTER_LIMIT = 10000;

		private uint _mercuryAddress;

		private ISEPLCCommandable _plcCommandableDevice;

		private Dictionary<PLCPacket, PLCDirectoryCard> _plcDirectory;

		private List<Packet> _plcDirectoryGeneral;

		public PLCManager(ISEPLCCommandable plcCommandableDevice, uint mercuryAddress)
		{
			this._plcCommandableDevice = plcCommandableDevice;
			this._mercuryAddress = mercuryAddress;
			this._plcDirectory = new Dictionary<PLCPacket, PLCDirectoryCard>(0);
		}

		public void StartConnection()
		{
			this._plcCommandableDevice.GetCommManager().RegisterForCommListening(this);
		}

		public void CloseConnection()
		{
			this._plcCommandableDevice.GetCommManager().UnRegisterFromCommListening(this);
			this.ShutDownCommDirectory();
		}

		public byte[] SendReceive(PLCPacket plcCommand, uint receiveTimeOut, bool isTransmitOnly)
		{
			ManualResetEvent manualResetEvent = new ManualResetEvent(false);
			this.RegisterPLCCommandToComm(plcCommand, manualResetEvent, isTransmitOnly);
			byte[] array = null;
			if (!isTransmitOnly)
			{
				try
				{
					manualResetEvent.Reset();
					manualResetEvent.WaitOne((int)receiveTimeOut);
					PLCDirectoryCard pLCDirectoryCard = this.GetPLCDirectoryCard(plcCommand);
					array = ((pLCDirectoryCard != null) ? pLCDirectoryCard.Response : null);
					if (array != null)
					{
						this.RemovePLCPacketFromCommRegistry(plcCommand);
					}
				}
				finally
				{
					this.RemovePLCPacketFromCommRegistry(plcCommand);
				}
			}
			return array;
		}

		private void Send(PLCPacket plcCmd, bool isTransmitOnly)
		{
			if (plcCmd != null)
			{
				this._plcCommandableDevice.Command_PLC_Transmit(plcCmd.GetTransmition(), isTransmitOnly);
			}
		}

		public void CommNotify(Packet packet)
		{
			if (packet != null)
			{
				if (packet.OpCode == 1664)
				{
					ManualResetEvent manualResetEvent = this.RegisterPLCResponseDataToComm(packet.Data);
					if (manualResetEvent != null)
					{
						try
						{
							manualResetEvent.Set();
						}
						catch (Exception var_1_42)
						{
						}
					}
				}
			}
		}

		private void RegisterPLCCommandToComm(PLCPacket plcCommand, ManualResetEvent commWakeEvent, bool isTransmitOnly)
		{
			if (!isTransmitOnly)
			{
				lock (this._plcDirectory)
				{
					PLCDirectoryCard pLCDirectoryCard = new PLCDirectoryCard();
					pLCDirectoryCard.WakeEvent = commWakeEvent;
					if (!this._plcDirectory.ContainsKey(plcCommand))
					{
						this._plcDirectory.Add(plcCommand, pLCDirectoryCard);
					}
				}
			}
			this.Send(plcCommand, isTransmitOnly);
		}

		private ManualResetEvent RegisterPLCResponseDataToComm(byte[] plcResponseData)
		{
			ManualResetEvent result;
			if (plcResponseData == null)
			{
				result = null;
			}
			else
			{
				ManualResetEvent manualResetEvent = null;
				lock (this._plcDirectory)
				{
					MemoryStream input = new MemoryStream(plcResponseData);
					BinaryReader binaryReader = new BinaryReader(input);
					byte b = binaryReader.ReadByte();
					byte responseOpCode = binaryReader.ReadByte();
					uint rspSource = binaryReader.ReadUInt32();
					uint rspDestination = binaryReader.ReadUInt32();
					Dictionary<PLCPacket, PLCDirectoryCard>.KeyCollection.Enumerator enumerator = this._plcDirectory.Keys.GetEnumerator();
					while (enumerator.MoveNext())
					{
						PLCPacket current = enumerator.Current;
						if (this.DoesPLCResponseMatchCommand(responseOpCode, rspSource, rspDestination, current))
						{
							PLCDirectoryCard pLCDirectoryCard = this._plcDirectory[current];
							pLCDirectoryCard.Response = plcResponseData;
							manualResetEvent = pLCDirectoryCard.WakeEvent;
							break;
						}
					}
				}
				result = manualResetEvent;
			}
			return result;
		}

		private PLCDirectoryCard GetPLCDirectoryCard(PLCPacket plcCommand)
		{
			PLCDirectoryCard result = null;
			lock (this._plcDirectory)
			{
				if (plcCommand != null && this._plcDirectory != null && this._plcDirectory.ContainsKey(plcCommand))
				{
					result = this._plcDirectory[plcCommand];
				}
			}
			return result;
		}

		private bool DoesPLCResponseMatchCommand(byte responseOpCode, uint rspSource, uint rspDestination, PLCPacket plcCommand)
		{
			return plcCommand != null && plcCommand.ExpectedResponse == responseOpCode && rspDestination == ProtocolMain.PROT_CONFTOOL_ADDR && this._mercuryAddress == rspSource;
		}

		private void RemovePLCPacketFromCommRegistry(PLCPacket plcCommand)
		{
			lock (this._plcDirectory)
			{
				if (this._plcDirectory.ContainsKey(plcCommand))
				{
					this._plcDirectory.Remove(plcCommand);
				}
			}
		}

		private void ShutDownCommDirectory()
		{
			lock (this._plcDirectory)
			{
				Dictionary<PLCPacket, PLCDirectoryCard>.ValueCollection.Enumerator enumerator = this._plcDirectory.Values.GetEnumerator();
				while (enumerator.MoveNext())
				{
					PLCDirectoryCard current = enumerator.Current;
					ManualResetEvent wakeEvent = current.WakeEvent;
					try
					{
						if (wakeEvent != null)
						{
							wakeEvent.Set();
						}
					}
					catch (Exception var_3_49)
					{
					}
				}
				this._plcDirectory.Clear();
			}
			this._plcDirectory = null;
		}
	}
}
