using System;

namespace SEProtocol.AT.Base
{
	public class ATException : Exception
	{
		public ATException(string message) : base(message)
		{
		}
	}
}
