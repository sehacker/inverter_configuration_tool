using System;

namespace SEProtocol.AT.Base
{
	public class ATPacket
	{
		private string _body = "";

		private string _expectedEndResponse = null;

		protected virtual string Prefix
		{
			get
			{
				return "";
			}
		}

		public string Body
		{
			get
			{
				return this._body;
			}
		}

		protected virtual string Terminator
		{
			get
			{
				return "\r\n";
			}
		}

		public string ExpectedEndResponse
		{
			get
			{
				return this._expectedEndResponse;
			}
			set
			{
				this._expectedEndResponse = value;
			}
		}

		public ATPacket(string command, string expectedEndResponse)
		{
			this._body = command;
			this._expectedEndResponse = expectedEndResponse;
		}

		public string GetTransmition()
		{
			return this.Prefix + this.Body + this.Terminator;
		}
	}
}
