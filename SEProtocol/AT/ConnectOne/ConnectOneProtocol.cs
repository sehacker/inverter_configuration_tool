using SEProtocol.AT.Base;
using System;

namespace SEProtocol.AT.ConnectOne
{
	public class ConnectOneProtocol
	{
		public static string[] PARITY_TYPE = new string[]
		{
			"N",
			"E",
			"O"
		};

		public static string[] COMMANDS = new string[]
		{
			"+++",
			"---",
			"SNUM={0}",
			"UID={0}",
			"UID?",
			"RP{0}",
			"BDRA",
			"UP={0}",
			"TUP={0}",
			"DOWN",
			"FD",
			"HIF={0}",
			"HIF?",
			"BDRF={0}",
			"BDRF?",
			"WLSI={0}",
			"WLSI?",
			"WST{0}={1}",
			"WST0?",
			"WPP{0}={1}",
			"WPP0?",
			"HSRV={0}:{1}",
			"HSRV?",
			"SNSI={0}",
			"SNSI?",
			"MTTF={0}",
			"MTTF?"
		};

		public static string[] RESPONSES = new string[]
		{
			"I/OK",
			"I/BUSY",
			"I/DONE",
			"I/ONLINE",
			"I/OFFLINE",
			"I/RCV",
			"I/PART",
			"I/EOP",
			"I/MBE",
			"I/UPDATE",
			"I/ERROR",
			""
		};

		public static void HandleError(int errorCode)
		{
			string text = null;
			if (errorCode <= 308)
			{
				switch (errorCode)
				{
				case 41:
					text = "Illegal delimiter";
					break;
				case 42:
					text = "Illegal value";
					break;
				case 43:
					text = "CR expected";
					break;
				case 44:
					text = "Number expected";
					break;
				case 45:
					text = "CR or ',' expected";
					break;
				case 46:
					text = "DNS expected";
					break;
				case 47:
					text = "':' or '~' expected";
					break;
				case 48:
					text = "String expected";
					break;
				case 49:
					text = "':' or '=' expected";
					break;
				case 50:
					text = "Text expected";
					break;
				case 51:
					text = "Syntax Error";
					break;
				case 52:
					text = "',' expected";
					break;
				case 53:
					text = "Illegal command code";
					break;
				case 54:
					text = "Error when setting parameter";
					break;
				case 55:
					text = "Error when getting parameter value";
					break;
				case 56:
					text = "User abort";
					break;
				case 57:
					text = "Error when trying to establish PPP";
					break;
				case 58:
					text = "Error when trying to establish SMTP";
					break;
				case 59:
					text = "Error when trying to establish POP3";
					break;
				case 60:
					text = "Single session body for MIME exceeds the maximum allowed";
					break;
				case 61:
					text = "Internal memory failure";
					break;
				case 62:
					text = "User aborted the system";
					break;
				case 63:
					text = "~CTSH needs to be LOW to change to hardware flow control";
					break;
				case 64:
					text = "User aborted last command using '---'";
					break;
				case 65:
					text = "Unique ID already exists (not all FF..)";
					break;
				case 66:
					text = "RESERVED";
					break;
				case 67:
					text = "Command ignored as irrelevant";
					break;
				case 68:
					text = "iChip serial number already exists";
					break;
				case 69:
					text = "Timeout on host communication";
					break;
				case 70:
					text = "Modem failed to respond";
					break;
				case 71:
					text = "No dial tone response";
					break;
				case 72:
					text = "No carrier modem response";
					break;
				case 73:
					text = "Dial failed";
					break;
				case 74:
					text = "Modem connection with ISP lost OR LAN connection lost OR WLAN connection lost";
					break;
				case 75:
					text = "Access denied to ISP server";
					break;
				case 76:
					text = "Unable to locate POP3 server";
					break;
				case 77:
					text = "POP3 server timed out";
					break;
				case 78:
					text = "Access denied to POP3 server";
					break;
				case 79:
					text = "POP failed";
					break;
				case 80:
					text = "No suitable message in mailbox";
					break;
				case 81:
					text = "Unable to locate SMTP server";
					break;
				case 82:
					text = "SMTP server timed out";
					break;
				case 83:
					text = "SMTP failed";
					break;
				case 84:
					text = "RESERVED";
					break;
				case 85:
					text = "RESERVED";
					break;
				case 86:
					text = "Writing to internal non-volatile parameters database failed";
					break;
				case 87:
					text = "Web server IP registration failed";
					break;
				case 88:
					text = "Socket IP registration failed";
					break;
				case 89:
					text = "E-mail IP registration failed";
					break;
				case 90:
					text = "IP registration failed for all methods specified";
					break;
				case 91:
					text = "RESERVED";
					break;
				case 92:
					text = "RESERVED";
					break;
				case 93:
					text = "RESERVED";
					break;
				case 94:
					text = "In Always Online mode, connection was lost and re-established";
					break;
				case 95:
				case 97:
				case 125:
				case 126:
				case 127:
				case 128:
				case 129:
				case 130:
				case 131:
				case 132:
				case 133:
				case 134:
				case 135:
				case 136:
				case 137:
				case 138:
				case 139:
				case 140:
				case 141:
				case 142:
				case 143:
				case 144:
				case 145:
				case 146:
				case 147:
				case 148:
				case 149:
				case 150:
				case 151:
				case 152:
				case 153:
				case 154:
				case 155:
				case 156:
				case 157:
				case 158:
				case 159:
				case 160:
				case 161:
				case 162:
				case 163:
				case 164:
				case 165:
				case 166:
				case 167:
				case 168:
				case 169:
				case 170:
				case 171:
				case 172:
				case 173:
				case 174:
				case 175:
				case 176:
				case 177:
				case 178:
				case 179:
				case 180:
				case 181:
				case 182:
				case 183:
				case 184:
				case 185:
				case 186:
				case 187:
				case 188:
				case 189:
				case 190:
				case 191:
				case 192:
				case 193:
				case 194:
				case 195:
				case 196:
				case 197:
				case 198:
				case 199:
				case 205:
				case 211:
				case 213:
				case 214:
					break;
				case 96:
					text = "A remote host, which had taken over iChip through the LATI port, was disconnected";
					break;
				case 98:
					text = "RESERVED";
					break;
				case 99:
					text = "RESERVED";
					break;
				case 100:
					text = "Error restoring default parameters";
					break;
				case 101:
					text = "No ISP access numbers defined";
					break;
				case 102:
					text = "No USRN defined";
					break;
				case 103:
					text = "No PWD entered";
					break;
				case 104:
					text = "No DNS defined";
					break;
				case 105:
					text = "POP3 server not defined";
					break;
				case 106:
					text = "MBX (mailbox) not defined";
					break;
				case 107:
					text = "MPWD (mailbox password) not defined";
					break;
				case 108:
					text = "TOA (addressee) not defined";
					break;
				case 109:
					text = "REA (return e-mail address) not defined";
					break;
				case 110:
					text = "SMTP server not defined";
					break;
				case 111:
					text = "Serial data overflow";
					break;
				case 112:
					text = "Illegal command when modem online";
					break;
				case 113:
					text = "E-mail firmware update attempted but not completed. The original firmware remained intact.";
					break;
				case 114:
					text = "E-mail parameters update rejected";
					break;
				case 115:
					text = "SerialNET could not be started due to missing parameters";
					break;
				case 116:
					text = "Error parsing a new trusted CA certificate";
					break;
				case 117:
					text = "RESERVED";
					break;
				case 118:
					text = "Protocol specified in the USRV parameter does not exist or is unknown";
					break;
				case 119:
					text = "WPA passphrase too short - has to be 8-63 chars";
					break;
				case 120:
					text = "RESERVED";
					break;
				case 121:
					text = "RESERVED";
					break;
				case 122:
					text = "SerialNET error: Host Interface undefined (HIF=0)";
					break;
				case 123:
					text = "SerialNET mode error: Host baud rate cannot be determined";
					break;
				case 124:
					text = "SerialNET over TELNET error: HIF parameter must be set to 1 or 2";
					break;
				case 200:
					text = "Socket does not exist";
					break;
				case 201:
					text = "Socket empty on receive";
					break;
				case 202:
					text = "Socket not in use";
					break;
				case 203:
					text = "Socket down";
					break;
				case 204:
					text = "No available sockets";
					break;
				case 206:
					text = "PPP open failed for socket";
					break;
				case 207:
					text = "Error creating socket";
					break;
				case 208:
					text = "Socket send error";
					break;
				case 209:
					text = "Socket receive error";
					break;
				case 210:
					text = "PPP down for socket";
					break;
				case 212:
					text = "Socket flush error";
					break;
				case 215:
					text = "No carrier error on socket operation";
					break;
				case 216:
					text = "General exception";
					break;
				case 217:
					text = "Out of memory";
					break;
				case 218:
					text = "An STCP (Open Socket) command specified a local port number that is already in use";
					break;
				case 219:
					text = "SSL initialization/internal CA certificate loading error";
					break;
				case 220:
					text = "SSL3 negotiation error";
					break;
				case 221:
					text = "Illegal SSL socket handle. Must be an open and active TCP socket.";
					break;
				case 222:
					text = "Trusted CA certificate does not exist";
					break;
				case 223:
					text = "RESERVED";
					break;
				case 224:
					text = "Decoding error on incoming SSL data";
					break;
				case 225:
					text = "No additional SSL sockets available";
					break;
				case 226:
					text = "Maximum SSL packet size (2K) exceeded";
					break;
				case 227:
					text = "AT+iSSND command failed because size of stream sent exceeded 2048 bytes";
					break;
				case 228:
					text = "AT+iSSND command failed because checksum calculated does not match checksum sent by host";
					break;
				default:
					switch (errorCode)
					{
					case 300:
						text = "HTTP server unknown";
						break;
					case 301:
						text = "HTTP server timeout";
						break;
					case 302:
						text = "HTTP failure";
						break;
					case 303:
						text = "No URL specified";
						break;
					case 304:
						text = "Illegal HTTP host name";
						break;
					case 305:
						text = "Illegal HTTP port number";
						break;
					case 306:
						text = "Illegal URL address";
						break;
					case 307:
						text = "URL address too long";
						break;
					case 308:
						text = "The AT+iWWW command failed because iChip does not contain a home page";
						break;
					}
					break;
				}
			}
			else
			{
				switch (errorCode)
				{
				case 400:
					text = "MAC address exists";
					break;
				case 401:
					text = "No IP address";
					break;
				case 402:
					text = "Wireless LAN power set failed";
					break;
				case 403:
					text = "Wireless LAN radio control failed";
					break;
				case 404:
					text = "Wireless LAN reset failed";
					break;
				case 405:
					text = "Wireless LAN hardware setup failed";
					break;
				case 406:
					text = "Command failed because WiFi module is currently busy";
					break;
				case 407:
					text = "Illegal WiFi channel";
					break;
				case 408:
					text = "Illegal SNR threshold";
					break;
				default:
					switch (errorCode)
					{
					case 500:
						text = "RESERVED";
						break;
					case 501:
						text = "Communications platform already active";
						break;
					case 502:
						text = "RESERVED";
						break;
					case 503:
						text = "RESERVED";
						break;
					case 504:
						text = "RESERVED";
						break;
					case 505:
						text = "Cannot open additional FTP session – all FTP handles in use";
						break;
					case 506:
						text = "Not an FTP session handle";
						break;
					case 507:
						text = "FTP server not found";
						break;
					case 508:
						text = "Timeout when connecting to FTP server";
						break;
					case 509:
						text = "Failed to login to FTP server (bad username or password or account)";
						break;
					case 510:
						text = "FTP command could not be completed";
						break;
					case 511:
						text = "FTP data socket could not be opened";
						break;
					case 512:
						text = "Failed to send data on FTP data socket";
						break;
					case 513:
						text = "FTP shutdown by remote server";
						break;
					case 514:
						text = "RESERVED";
						break;
					default:
						switch (errorCode)
						{
						case 550:
							text = "Telnet server not found";
							break;
						case 551:
							text = "Timeout when connecting to Telnet server";
							break;
						case 552:
							text = "Telnet command could not be completed";
							break;
						case 553:
							text = "Telnet session shutdown by remote server";
							break;
						case 554:
							text = "A Telnet session is not currently active";
							break;
						case 555:
							text = "A Telnet session is already open";
							break;
						case 556:
							text = "Telnet server refused to switch to BINARY mode";
							break;
						case 557:
							text = "Telnet server refused to switch to ASCII mode";
							break;
						case 558:
							text = "RESERVED";
							break;
						case 559:
							text = "RESERVED";
							break;
						case 560:
							text = "Client could not retrieve a ring response e-mail";
							break;
						case 561:
							text = "Remote peer closed the SerialNET socket";
							break;
						case 570:
							text = "PING destination not found";
							break;
						case 571:
							text = "No reply to PING request";
							break;
						}
						break;
					}
					break;
				}
			}
			if (!string.IsNullOrEmpty(text))
			{
				throw new ATException(text);
			}
		}
	}
}
