using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum BaudRate
	{
		BR_2400,
		BR_4800,
		BR_9600,
		BR_19200,
		BR_38400,
		BR_57600,
		BR_115200,
		BR_AUTO
	}
}
