using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum HostInterfaces
	{
		ERROR = -1,
		AUTO,
		USART0,
		USART1,
		USART2,
		USB_DEVICE,
		USB_HOST,
		CHECK
	}
}
