using System;

namespace SEProtocol.AT.ConnectOne.Data
{
	public class iChipFirmwareData
	{
		private InterfaceCode _interfaceCode = InterfaceCode.D;

		private int _majorVersion = 0;

		private string _versionType = null;

		private int _subVersion = 0;

		private DateTime _verTime = DateTime.Now;

		public InterfaceCode InterfaceCode
		{
			get
			{
				return this._interfaceCode;
			}
			set
			{
				this._interfaceCode = value;
			}
		}

		public int Version
		{
			get
			{
				return this._majorVersion;
			}
			set
			{
				this._majorVersion = value;
			}
		}

		public string VersionType
		{
			get
			{
				return this._versionType;
			}
			set
			{
				this._versionType = value;
			}
		}

		public int SubVersion
		{
			get
			{
				return this._subVersion;
			}
			set
			{
				this._subVersion = value;
			}
		}

		public DateTime VersionTime
		{
			get
			{
				return this._verTime;
			}
			set
			{
				this._verTime = value;
			}
		}
	}
}
