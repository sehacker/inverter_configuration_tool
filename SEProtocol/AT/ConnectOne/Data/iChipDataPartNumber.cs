using System;

namespace SEProtocol.AT.ConnectOne.Data
{
	public class iChipDataPartNumber
	{
		private int _versionNumber = 0;

		private InterfaceCode _interfaceCode = InterfaceCode.D;

		public int Version
		{
			get
			{
				return this._versionNumber;
			}
			set
			{
				this._versionNumber = value;
			}
		}

		public InterfaceCode InterfaceCode
		{
			get
			{
				return this._interfaceCode;
			}
			set
			{
				this._interfaceCode = value;
			}
		}
	}
}
