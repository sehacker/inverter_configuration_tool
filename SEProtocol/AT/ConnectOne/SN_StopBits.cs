using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum SN_StopBits
	{
		SN_SB_IGNORE = -1,
		SN_SB_1 = 1
	}
}
