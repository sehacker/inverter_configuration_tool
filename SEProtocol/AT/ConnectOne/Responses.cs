using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum Responses
	{
		I_OK,
		I_BUSY,
		I_DONE,
		I_ONLINE,
		I_OFFLINE,
		I_RCV,
		I_PART,
		I_EOP,
		I_MBE,
		I_UPDATE,
		I_ERROR,
		COUNT
	}
}
