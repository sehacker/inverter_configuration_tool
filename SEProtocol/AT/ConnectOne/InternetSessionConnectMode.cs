using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum InternetSessionConnectMode
	{
		ONLINE,
		ONLINE_AND_REGISTER
	}
}
