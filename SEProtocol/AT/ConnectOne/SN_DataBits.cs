using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum SN_DataBits
	{
		SN_DB_IGNORE = -1,
		SN_DB_7 = 7,
		SN_DB_8
	}
}
