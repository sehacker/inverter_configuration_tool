using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum SN_BaudRate
	{
		SN_BD_IGNORE = -1,
		SN_BD_BY_BDRD,
		SN_BD_600,
		SN_BD_1200,
		SN_BD_2400,
		SN_BD_4800,
		SN_BD_9600,
		SN_BD_19200,
		SN_BD_38400,
		SN_BD_57600,
		SN_BD_115200
	}
}
