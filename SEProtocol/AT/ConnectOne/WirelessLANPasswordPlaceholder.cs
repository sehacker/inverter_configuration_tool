using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum WirelessLANPasswordPlaceholder
	{
		PASS_0,
		PASS_1,
		PASS_2,
		PASS_3,
		PASS_4,
		PASS_5,
		PASS_6,
		PASS_7,
		PASS_8,
		PASS_9
	}
}
