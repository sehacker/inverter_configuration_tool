using SEProtocol.AT.Base;
using System;

namespace SEProtocol.AT.ConnectOne
{
	public class ConnectOnePacket : ATPacket
	{
		protected override string Prefix
		{
			get
			{
				return "AT+i";
			}
		}

		public ConnectOnePacket(Commands command, params string[] values) : this(command, Responses.I_OK, values)
		{
		}

		public ConnectOnePacket(Commands command, Responses expectedEndResponse, params string[] values) : base(string.Format(ConnectOneProtocol.COMMANDS[(int)command], values), ConnectOneProtocol.RESPONSES[(int)expectedEndResponse])
		{
		}
	}
}
