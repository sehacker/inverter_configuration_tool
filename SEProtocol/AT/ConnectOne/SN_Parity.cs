using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum SN_Parity
	{
		SN_P_IGNORE = -1,
		SN_P_NONE,
		SE_P_EQUAL,
		SE_P_ODD
	}
}
