using SEProtocol.SDP.Packets.Base;
using System;

namespace SEProtocol.SDP
{
	public interface ISECommNotifiable
	{
		void CommNotify(Packet response);
	}
}
