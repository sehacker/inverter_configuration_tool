using SEComm;
using SEComm.Connections;
using SEProtocol.SDP.Packets.Base;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace SEProtocol.SDP
{
	public class CommManager
	{
		private const bool ADD_COPY_TO_QUEUE = true;

		private const bool ADD_REAL_TO_QUEUE = false;

		private const int GENERAL_COUNTER_LIMIT = 10000;

		private Connection _connection;

		private ConnectionAccess _connectionAccess;

		private bool? _connectedRemotely = null;

		private Thread _thCommRecieveListener;

		private bool _shouldCommRecieveListenerBeActive = false;

		private List<ISECommNotifiable> _commNotifyListeners;

		private Thread _thCommNotify;

		private bool _shouldCommNotifyBeActive;

		private Semaphore _commNotifyGeneralListener;

		private Dictionary<Packet, CommDirectoryCard> _commDirectory;

		private List<Packet> _commDirectoryGeneral;

		public bool? ConnectedRemotely
		{
			get
			{
				bool? result = null;
				if (this._connection != null)
				{
					result = new bool?(this._connection.RemoteConnection);
				}
				return result;
			}
		}

		public bool IsConnected
		{
			get
			{
				return this._connection != null && this._connection.IsConnected;
			}
		}

		public CommManager(ConnectionParams cp)
		{
			ConstructorInfo constructor = cp.ConnectionType.GetConstructor(cp.ParameterTypes);
			this._connection = (Connection)constructor.Invoke(cp.Parameters);
			this._commNotifyListeners = new List<ISECommNotifiable>(0);
			this._commDirectory = new Dictionary<Packet, CommDirectoryCard>(0);
			this._commDirectoryGeneral = new List<Packet>(0);
			this._thCommRecieveListener = new Thread(new ThreadStart(this.CommReceiveListener));
			this._thCommRecieveListener.Name = "Comm Receive Listener (Sync) Thread";
			this._thCommRecieveListener.IsBackground = true;
			this._thCommNotify = new Thread(new ThreadStart(this.CommNotifyGeneral));
			this._thCommNotify.Name = "Comm Receive Listener (ASync) Thread";
			this._thCommNotify.IsBackground = true;
		}

		public bool StartConnection()
		{
			bool result = false;
			if (this._connection != null)
			{
				this._connection.OpenConnection();
				if (this._connection.IsConnected)
				{
					this._connectionAccess = new ConnectionAccess(this._connection);
					this._shouldCommRecieveListenerBeActive = true;
					this._thCommRecieveListener.Start();
					this._shouldCommNotifyBeActive = true;
					this._thCommNotify.Start();
				}
			}
			return result;
		}

		public void CloseConnection()
		{
			this._shouldCommNotifyBeActive = false;
			this._shouldCommRecieveListenerBeActive = false;
			this.ShutDownCommDirectory();
			this._commNotifyGeneralListener.Release();
			try
			{
				this._thCommNotify.Abort();
			}
			catch (ThreadAbortException var_0_32)
			{
			}
			this._thCommNotify = null;
			if (this._connection != null)
			{
				try
				{
					this._connection.CloseConnection();
				}
				catch (Exception var_1_5D)
				{
				}
				this._connection = null;
			}
			if (this._thCommRecieveListener != null && this._thCommRecieveListener.IsAlive)
			{
				try
				{
					this._thCommRecieveListener.Abort();
				}
				catch (ThreadAbortException var_0_32)
				{
				}
			}
			this._thCommRecieveListener = null;
			if (this._connectionAccess != null)
			{
				this._connectionAccess = null;
			}
		}

		public void RegisterForCommListening(ISECommNotifiable listener)
		{
			if (listener != null)
			{
				lock (this._commNotifyListeners)
				{
					this._commNotifyListeners.Add(listener);
				}
			}
		}

		public void UnRegisterFromCommListening(ISECommNotifiable listener)
		{
			if (listener != null)
			{
				lock (this._commNotifyListeners)
				{
					if (this._commNotifyListeners.Contains(listener))
					{
						this._commNotifyListeners.Remove(listener);
					}
				}
			}
		}

		public Packet SendReceive(Packet command, uint receiveTimeOut, bool isTransmitOnly)
		{
			ManualResetEvent manualResetEvent = new ManualResetEvent(false);
			this.RegisterCommandToComm(command, manualResetEvent, isTransmitOnly, receiveTimeOut);
			Packet packet = null;
			if (!isTransmitOnly)
			{
				try
				{
					manualResetEvent.Reset();
					manualResetEvent.WaitOne((int)receiveTimeOut);
					CommDirectoryCard commDirectoryCard = this.GetCommDirectoryCard(command);
					packet = ((commDirectoryCard != null) ? commDirectoryCard.Response : null);
					if (packet != null)
					{
						this.RemovePacketFromCommRegistry(command);
					}
				}
				finally
				{
					this.RemovePacketFromCommRegistry(command);
				}
			}
			return packet;
		}

		private void CommNotifyGeneral()
		{
			this._commNotifyGeneralListener = new Semaphore(0, 10000, "CommManager_General_Notify_Semaphore");
			while (this._shouldCommNotifyBeActive)
			{
				this._commNotifyGeneralListener.WaitOne(-1);
				try
				{
					Packet nextResponseFromGeneralComm = this.GetNextResponseFromGeneralComm();
					if (nextResponseFromGeneralComm != null)
					{
						ushort opCode = nextResponseFromGeneralComm.OpCode;
						if (opCode <= ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE)
						{
							switch (opCode)
							{
							case ProtocolGeneralResponses.PROT_RESP_ACK:
								break;
							case ProtocolGeneralResponses.PROT_RESP_NACK:
								break;
							default:
								if (opCode != ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE)
								{
								}
								break;
							}
						}
						else
						{
							switch (opCode)
							{
							case 897:
								break;
							case 898:
								break;
							case 899:
								break;
							case 900:
								break;
							default:
								switch (opCode)
								{
								case 1280:
									break;
								case 1281:
									break;
								default:
									switch (opCode)
									{
									}
									break;
								}
								break;
							}
						}
						if (nextResponseFromGeneralComm.OpCode != ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE && nextResponseFromGeneralComm.OpCode != 897 && nextResponseFromGeneralComm.OpCode != 898 && nextResponseFromGeneralComm.OpCode != 900)
						{
						}
					}
					if (nextResponseFromGeneralComm != null)
					{
						lock (this._commNotifyListeners)
						{
							int count = this._commNotifyListeners.Count;
							for (int i = 0; i < count; i++)
							{
								ISECommNotifiable iSECommNotifiable = this._commNotifyListeners[i];
								Packet newPacket = Packet.GetNewPacket(nextResponseFromGeneralComm);
								iSECommNotifiable.CommNotify(newPacket);
							}
						}
					}
				}
				catch (Exception var_6_1C3)
				{
				}
			}
		}

		private void RegisterCommandToComm(Packet command, ManualResetEvent commWakeEvent, bool isTransmitOnly, uint timeout)
		{
			if (!isTransmitOnly)
			{
				lock (this._commDirectory)
				{
					CommDirectoryCard commDirectoryCard = new CommDirectoryCard();
					commDirectoryCard.WakeEvent = commWakeEvent;
					if (!this._commDirectory.ContainsKey(command))
					{
						this._commDirectory.Add(command, commDirectoryCard);
					}
				}
			}
			this.Send(command, timeout);
		}

		private ManualResetEvent RegisterResponseToComm(Packet response)
		{
			ManualResetEvent manualResetEvent = null;
			lock (this._commDirectory)
			{
				Dictionary<Packet, CommDirectoryCard>.KeyCollection.Enumerator enumerator = this._commDirectory.Keys.GetEnumerator();
				while (enumerator.MoveNext())
				{
					Packet current = enumerator.Current;
					if (this.DoesResponseMatchCommand(response, current))
					{
						CommDirectoryCard commDirectoryCard = this._commDirectory[current];
						commDirectoryCard.Response = response;
						manualResetEvent = commDirectoryCard.WakeEvent;
						break;
					}
				}
			}
			lock (this._commDirectoryGeneral)
			{
				if (manualResetEvent == null)
				{
					this._commDirectoryGeneral.Add(response);
				}
			}
			return manualResetEvent;
		}

		private Packet GetNextResponseFromGeneralComm()
		{
			Packet packet = null;
			lock (this._commDirectoryGeneral)
			{
				if (this._commDirectoryGeneral != null && this._commDirectoryGeneral.Count > 0)
				{
					packet = this._commDirectoryGeneral[0];
					this._commDirectoryGeneral.Remove(packet);
				}
			}
			return packet;
		}

		private CommDirectoryCard GetCommDirectoryCard(Packet command)
		{
			CommDirectoryCard result = null;
			lock (this._commDirectory)
			{
				if (command != null && this._commDirectory != null && this._commDirectory.ContainsKey(command))
				{
					result = this._commDirectory[command];
				}
			}
			return result;
		}

		private bool DoesResponseMatchCommand(Packet response, Packet command)
		{
			return command != null && command.ID == response.ID && this.IsDestinationValid(response, command) && (command.ExpectedOpCode == response.OpCode || response.OpCode == ProtocolGeneralResponses.PROT_RESP_NACK);
		}

		private bool IsDestinationValid(Packet response, Packet command)
		{
			return command.Destination.Equals(ProtocolMain.PROT_SINGLECAST_ADDR) || command.Destination.Equals(ProtocolMain.PROT_BROADCAST_ADDR) || command.Destination == response.Source;
		}

		private bool IsOpCodeValid(Packet response, Packet command)
		{
			return command.ExpectedOpCode == response.OpCode || command.ExpectedOpCode == ProtocolGeneralResponses.PROT_RESP_ACK;
		}

		private void RemovePacketFromCommRegistry(Packet command)
		{
			lock (this._commDirectory)
			{
				if (this._commDirectory.ContainsKey(command))
				{
					this._commDirectory.Remove(command);
				}
			}
		}

		private void ShutDownCommDirectory()
		{
			lock (this._commDirectory)
			{
				Dictionary<Packet, CommDirectoryCard>.ValueCollection.Enumerator enumerator = this._commDirectory.Values.GetEnumerator();
				while (enumerator.MoveNext())
				{
					CommDirectoryCard current = enumerator.Current;
					ManualResetEvent wakeEvent = current.WakeEvent;
					try
					{
						if (wakeEvent != null)
						{
							wakeEvent.Set();
						}
					}
					catch (Exception var_3_49)
					{
					}
				}
				this._commDirectory.Clear();
			}
			this._commDirectory = null;
		}

		private void Send(Packet packet, uint timeout)
		{
			if (packet != null)
			{
				if (packet != null)
				{
					ushort opCode = packet.OpCode;
					if (opCode <= 775)
					{
						if (opCode != 18)
						{
							switch (opCode)
							{
							}
						}
					}
					else
					{
						switch (opCode)
						{
						case 782:
							break;
						case 783:
							break;
						default:
							switch (opCode)
							{
							}
							break;
						}
					}
					if (packet.OpCode != 18 && packet.OpCode != 783 && packet.OpCode != 774 && packet.OpCode != 775)
					{
					}
				}
				this._connectionAccess.Write(packet.GetTransmition(), (int)timeout);
			}
		}

		private void CommReceiveListener()
		{
			Packet newPacket = Packet.GetNewPacket(PacketVersion.B, Packet.LENGTH_DATA_BUFFER_DEFAULT);
			while (this._shouldCommRecieveListenerBeActive)
			{
				try
				{
					byte b = this._connectionAccess.ReadByte(-1);
					if (newPacket.Parse(b))
					{
						Packet newPacket2 = Packet.GetNewPacket(newPacket);
						if (newPacket2 != null)
						{
							ushort opCode = newPacket2.OpCode;
							if (opCode <= ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE)
							{
								switch (opCode)
								{
								case ProtocolGeneralResponses.PROT_RESP_ACK:
									break;
								case ProtocolGeneralResponses.PROT_RESP_NACK:
									break;
								default:
									if (opCode != ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE)
									{
									}
									break;
								}
							}
							else
							{
								switch (opCode)
								{
								case 897:
									break;
								case 898:
									break;
								case 899:
									break;
								case 900:
									break;
								default:
									switch (opCode)
									{
									case 1280:
										break;
									case 1281:
										break;
									default:
										switch (opCode)
										{
										}
										break;
									}
									break;
								}
							}
							if (newPacket2.OpCode != ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE && newPacket2.OpCode != 1280 && newPacket2.OpCode != 1281 && newPacket2.OpCode != 897 && newPacket2.OpCode != 898 && newPacket2.OpCode != 900)
							{
							}
						}
						newPacket.Reset();
						ManualResetEvent manualResetEvent = this.RegisterResponseToComm(newPacket2);
						if (manualResetEvent != null)
						{
							manualResetEvent.Set();
						}
						else
						{
							this._commNotifyGeneralListener.Release();
						}
					}
				}
				catch (Exception var_5_1B4)
				{
				}
			}
		}
	}
}
