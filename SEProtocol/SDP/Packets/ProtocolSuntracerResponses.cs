using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolSuntracerResponses
	{
		PROT_RESP_SUNTRACER_TRACE = 0x0480,
		PROT_RESP_SUNTRACER_FLASH = 0x0481,
		COUNT			  = 0x0482,
	}
}
