using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolUpgradeCommands
	{
		PROT_CMD_UPGRADE_START		= 0x0020,
		PROT_CMD_UPGRADE_WRITE		= 0x0021,
		PROT_CMD_UPGRADE_FINISH		= 0x0022,
		PROT_CMD_UPGRADE_READ_DATA	= 0x0023,
		PROT_CMD_UPGRADE_READ_SIZE	= 0x0024,
		COUNT				= 0x0025,
	}
}
