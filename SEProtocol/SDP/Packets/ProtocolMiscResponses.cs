using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolMiscResponses
	{
		PROT_RESP_MISC_GET_VER		= 0x00b0,
		PROT_RESP_MISC_GET_TYPE		= 0x00b1,
		PROT_RESP_MISC_PAYLOAD		= 0x00b2,
		PROT_RESP_MISC_READ_MEMORY	= 0x00b3,
		COUNT				= 0x00b4,
	}
}
