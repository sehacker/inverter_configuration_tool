using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolPLCResponses
	{
		PLC_RESP_MERCURY_TELEM	= 0x0000,
		PLC_RESP_PROBE_TELEM	= 0x0001,
		PLC_RESP_ACK		= 0x0010,
		PLC_RESP_PARAM		= 0x0011,
		PLC_RESP_LOG		= 0x0012,
		PLC_RESP_IVTRACE	= 0x0013,
		PLC_RESP_PROBE_ALIVE	= 0x0080,
	}
}
