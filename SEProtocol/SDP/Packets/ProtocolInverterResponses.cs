using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolInverterResponses
	{
		PROT_RESP_INVERTER_DYNAMIC_POWER_LIMIT = 0x0980,
		COUNT				       = 0x0981,
	}
}
