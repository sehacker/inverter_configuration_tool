using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolVegaCommands
	{
		PROT_CMD_VEGA_READ_MEAS		= 0x0a00,
		PROT_CMD_VEGA_GET_SYS_STATUS	= 0x0a01,
		PROT_CMD_VEGA_GET_TELEM		= 0x0a02,
		PROT_CMD_VEGA_GET_MAX_VDC_VALUE	= 0x0a03,
		PROT_CMD_VEGA_SET_MAX_VDC_VALUE	= 0x0a04,
		PROT_CMD_VEGA_RELAY_SET		= 0x0a05,
		COUNT				= 0x0a06,
	}
}
