using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolConfigToolResponses
	{
		PROT_RESP_CONFTOOL_PLC_DATA = 0x0680,
		COUNT			    = 0x0681,
	}
}
