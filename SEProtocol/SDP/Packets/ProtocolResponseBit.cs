using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolResponseBit
	{
		PROT_RESP_BIT = 0x0080,
		COUNT	      = 0x0081,
	}
}
