using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolServerResponses
	{
		PROT_RESP_SERVER_GMT	= 0x0580,
		PROT_RESP_SERVER_NAME	= 0x0581,
		COUNT			= 0x0582,
	}
}
