using SEProtocol.SDP.Packets.Base;
using System;
using System.IO;
using System.Linq;

namespace SEProtocol.SDP.Packets
{
	public class PacketA : Packet
	{
		public PacketA(uint maxDataBufferLength) : base(PacketVersion.A, new byte[]
		{
			0x12, 0x34, 0x56, 0x78
		}, maxDataBufferLength)
		{
		}

		public PacketA(Packet packet) : base(packet)
		{
		}

		public override byte[] GetTransmition()
		{
			ushort opCode = base.OpCode;
			base.OpCode = ProtocolConverter.Ver2ToVer1Conv(base.OpCode);
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(base.Barker.ToArray<byte>(), 0, base.BarkerLength);
			ushort num = (ushort)base.Length;
			binaryWriter.Write(num);
			binaryWriter.Write(~num);
			binaryWriter.Write(base.ID);
			binaryWriter.Write(base.Destination);
			binaryWriter.Write(base.OpCode);
			binaryWriter.Write(base.Data, 0, (int)num);
			binaryWriter.Write(this.GetCheckSum());
			base.OpCode = opCode;
			return memoryStream.ToArray();
		}

		public override bool Parse(byte b)
		{
			Packet.ReceivePacketState receiveState = base.ReceiveState;
			bool result;
			if (receiveState != Packet.ReceivePacketState.PACKET_READ_SRC4)
			{
				result = base.Parse(b);
			}
			else
			{
				base.Source |= (uint)((uint)b << 24);
				base.Destination = base.Source;
				base.StateSkip(4);
				result = base.ParsingDone;
			}
			return result;
		}

		public override ushort GetCheckSum()
		{
			ushort num = 0;
			num += base.ID;
			num += base.OpCode;
			num += (ushort)(base.Destination >> 16);
			num += (ushort)(base.Destination & ushort.MaxValue);
			for (int i = 0; i < base.Length; i++)
			{
				num += (ushort)base.Data[i];
			}
			return num;
		}
	}
}
