using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolVegaResponses
	{
		PROT_RESP_VEGA_READ_MEAS		= 0x0a80,
		PROT_RESP_VEGA_GET_SYS_STATUS		= 0x0a81,
		PROT_RESP_VEGA_GET_TELEM		= 0x0a82,
		PROT_RESP_VEGA_GET_MAX_VDC_VALUE	= 0x0a83,
		COUNT					= 0x0a84,
	}
}
