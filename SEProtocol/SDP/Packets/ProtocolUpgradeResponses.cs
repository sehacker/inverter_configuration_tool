using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolUpgradeResponses
	{
		PROT_RESP_UPGRADE_DATA = 0x00a0,
		PROT_RESP_UPGRADE_SIZE = 0x00a1,
		COUNT		       = 0x00a2,
	}
}
