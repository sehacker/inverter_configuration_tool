using System;
using System.Threading;

namespace SEProtocol.SDP.Packets.Base
{
	public class CommDirectoryCard
	{
		private ManualResetEvent _wakeEvent = null;

		private Packet _response;

		public ManualResetEvent WakeEvent
		{
			get
			{
				return this._wakeEvent;
			}
			set
			{
				this._wakeEvent = value;
			}
		}

		public Packet Response
		{
			get
			{
				return this._response;
			}
			set
			{
				this._response = value;
			}
		}
	}
}
