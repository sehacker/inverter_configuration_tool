using SEProtocol.SDP.Packets.Base;
using SEProtocol.Utils;
using System;
using System.IO;
using System.Linq;

namespace SEProtocol.SDP.Packets
{
	public class PacketB : Packet
	{
		public PacketB(uint maxDataBufferLength) : base(PacketVersion.B, new byte[]
		{
			0x12, 0x34, 0x56, 0x79
		}, maxDataBufferLength)
		{
		}

		public PacketB(Packet packet) : base(packet)
		{
		}

		public override byte[] GetTransmition()
		{
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(base.Barker.ToArray<byte>(), 0, base.BarkerLength);
			ushort num = (ushort)base.Length;
			binaryWriter.Write(num);
			binaryWriter.Write(~num);
			binaryWriter.Write(base.ID);
			binaryWriter.Write(base.Source);
			binaryWriter.Write(base.Destination);
			binaryWriter.Write(base.OpCode);
			binaryWriter.Write(base.Data, 0, (int)num);
			binaryWriter.Write(this.GetCheckSum());
			return memoryStream.ToArray();
		}

		public override bool Parse(byte b)
		{
			Packet.ReceivePacketState receiveState = base.ReceiveState;
			bool result;
			if (receiveState != Packet.ReceivePacketState.PACKET_READ_SRC4)
			{
				result = base.Parse(b);
			}
			else
			{
				base.Source |= (uint)((uint)b << 24);
				base.StateNext();
				result = base.ParsingDone;
			}
			return result;
		}

		public override ushort GetCheckSum()
		{
			ushort crc = Crc16.INIT_VAL;
			crc = Crc16.CalcUint16MsbFirst(crc, base.ID);
			crc = Crc16.CalcUint32MsbFirst(crc, base.Source);
			crc = Crc16.CalcUint32MsbFirst(crc, base.Destination);
			crc = Crc16.CalcUint16MsbFirst(crc, base.OpCode);
			return Crc16.Calc(crc, base.Data, base.Length);
		}
	}
}
