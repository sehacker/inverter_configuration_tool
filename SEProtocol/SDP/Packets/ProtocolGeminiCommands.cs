using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolGeminiCommands
	{
		PROT_CMD_COMBI_PAUSE_MONITORING			= 0x0b00,
		PROT_CMD_COMBI_SET_TIME_STAMP			= 0x0b01,
		PROT_CMD_COMBI_RCD_CALIBRATION			= 0x0b02,
		PROT_CMD_COMBI_GET_TELEM			= 0x0b03,
		PROT_CMD_COMBI_FORCE_TELEM			= 0x0b04,
		PROT_CMD_COMBI_SWITCHES_CONNECT			= 0x0b05,
		PROT_CMD_COMBI_SWITCHES_DISCONNECT		= 0x0b06,
		PROT_CMD_COMBI_SWITCHES_CONNECT_ALL		= 0x0b07,
		PROT_CMD_COMBI_SWITCHES_DISCONNECT_ALL		= 0x0b08,
		PROT_CMD_COMBI_RCD_TEST_EXECUTE			= 0x0b09,
		PROT_CMD_COMBI_RELAYS_TEST_EXECUTE		= 0x0b0a,
		PROT_CMD_COMBI_GET_COMBISTRING_PARAM		= 0x0b0b,
		PROT_CMD_COMBI_SET_COMBISTRING_PARAM		= 0x0b0c,
		PROT_CMD_COMBI_GET_ALL_COMBISTRING_PARAMS	= 0x0b0d,
		PROT_CMD_COMBI_GET_ALL_COMBI_PARAMS		= 0x0b0e,
		PROT_CMD_COMBI_READ_MEASUREMENTS		= 0x0b0f,
		PROT_CMD_COMBI_GET_STRING_STATUS		= 0x0b10,
		PROT_CMD_COMBI_GET_COMBI_STATUS			= 0x0b11,
		PROT_CMD_COMBI_GET_ACTIVE_STRINGS		= 0x0b12,
		PROT_CMD_COMBI_FWD_STRING_TELEM			= 0x0b13,
		PROT_CMD_COMBI_FWD_COMBI_TELEM			= 0x0b14,
		PROT_CMD_COMBI_GET_UNIFIED_STRING_STATUS	= 0x0b15,
		PROT_CMD_COMBI_GET_UNIFIED_COMBI_STATUS		= 0x0b16,
		PROT_CMD_COMBI_CHECK_INNER_PROTOCOL		= 0x0b17,
		PROT_CMD_COMBI_SWITCHES_CONNECT_RELAY		= 0x0b18,
		PROT_CMD_COMBI_SWITCHES_DISCONNECT_RELAY	= 0x0b19,
		PROT_CMD_COMBI_GET_GEMINI_STRING_IDS		= 0x0b1a,
		PROT_CMD_COMBI_GET_ALL_SWITCHES_STATUS		= 0x0b1b,
		COUNT						= 0x0b1c,
	}
}
