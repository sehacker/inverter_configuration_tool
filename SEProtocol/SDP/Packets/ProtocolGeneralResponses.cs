using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolGeneralResponses
	{
		PROT_RESP_ACK	= 0x0080,
		PROT_RESP_NACK	= 0x0081,
		COUNT		= 0x0082,
	}
}
