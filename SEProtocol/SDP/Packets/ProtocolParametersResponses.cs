using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolParametersResponses
	{
		PROT_RESP_PARAMS_SINGLE			= 0x0090,
		PROT_RESP_PARAMS_INFO			= 0x0091,
		PROT_RESP_PARAMS_NAME			= 0x0092,
		PROT_RESP_PARAMS_NUM			= 0x0093,
		PROT_RESP_PARAMS_ALL			= 0x0094,
		PROT_RESP_PARAMS_INCORRECT_PASSWORD	= 0x0095,
		COUNT					= 0x0096,
	}
}
