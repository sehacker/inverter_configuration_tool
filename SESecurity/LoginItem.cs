using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;

namespace SESecurity
{
	public class LoginItem
	{
		private const int LOGON32_LOGON_NETWORK = 3;

		private const string PASSWORD_DLM = "#";

		private const string PASSWORD_SPACER = "-";

		private const int PASSWORD_BATCH_SIZE = 6;

		private const string KEYS = "ABCDEFGHIJKLM0123456789NOPQRSTUVWXYZ";

		private const string DOMAIN = "solaredge";

		private const string LOCAL = "local";

		private const string LDAP_FULL = "LDAP://DC=solaredge,DC=local";

		private const string LDAP_PATH = "LDAP://DC=solaredge,DC=local";

		private const string GROUP_SID = "010400000000000515000000268E37D8499D3A1470726BB8";

		private AccessLevels _accessLevel = AccessLevels.MONITORING;

		private bool _shouldValidateAdminOffline = false;

		public AccessLevels AccessLevel
		{
			get
			{
				return this._accessLevel;
			}
			set
			{
				this._accessLevel = value;
			}
		}

		public bool ShouldValidateAdminOffline
		{
			get
			{
				return this._shouldValidateAdminOffline;
			}
			set
			{
				this._shouldValidateAdminOffline = value;
			}
		}

		[DllImport("ADVAPI32.DLL")]
		public static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, out int phToken);

		public LoginItem(AccessLevels startingLevel)
		{
			this._accessLevel = startingLevel;
		}

		public LoginData QuerryCredentials(LoginData loginData)
		{
			LoginData loginData2 = new LoginData(loginData);
			LoginData result;
			if (loginData == null)
			{
				result = loginData2;
			}
			else
			{
				switch (loginData.LevelRequested)
				{
				case AccessLevels.MONITORING:
					loginData2 = this.QuerryCredentialsMonitoring(loginData);
					break;
				case AccessLevels.TECHNICIAN:
					loginData2 = this.QuerryCredentialsTechnician(loginData);
					break;
				case AccessLevels.CERTIFICATE:
					loginData2 = this.QuerryCredentialsCertification(loginData);
					break;
				case AccessLevels.ADMIN:
					loginData2 = this.QuerryCredentialsAdmin(loginData);
					break;
				}
				result = loginData2;
			}
			return result;
		}

		private LoginData QuerryCredentialsMonitoring(LoginData loginData)
		{
			return new LoginData(loginData)
			{
				LevelAttained = AccessLevels.MONITORING,
				Successful = true
			};
		}

		private LoginData QuerryCredentialsTechnician(LoginData loginData)
		{
			LoginData loginData2 = new LoginData(loginData);
			LoginData result;
			if (loginData == null)
			{
				result = loginData2;
			}
			else
			{
				byte keyAlgorithm = this.GetKeyAlgorithm(loginData.Credentials.DeviceID);
				LoginCredentials credentialsTechnician = this.GetCredentialsTechnician(loginData.Password, keyAlgorithm);
				if (credentialsTechnician == null)
				{
					result = loginData2;
				}
				else
				{
					bool flag = string.IsNullOrEmpty(loginData.Credentials.DeviceID) || credentialsTechnician.DeviceID.Equals(loginData.Credentials.DeviceID);
					bool flag2 = DateTime.Now < credentialsTechnician.Time;
					loginData2.Successful = (flag && flag2);
					if (loginData2.Successful)
					{
						loginData2.LevelAttained = AccessLevels.TECHNICIAN;
					}
					result = loginData2;
				}
			}
			return result;
		}

		private LoginData QuerryCredentialsCertification(LoginData loginData)
		{
			LoginData loginData2 = new LoginData(loginData);
			LoginData result;
			if (loginData == null)
			{
				result = loginData2;
			}
			else
			{
				byte keyAlgorithm = this.GetKeyAlgorithm(loginData.Credentials.DeviceID);
				LoginCredentials credentialsCertificate = this.GetCredentialsCertificate(loginData.Password, keyAlgorithm);
				if (credentialsCertificate == null)
				{
					result = loginData2;
				}
				else
				{
					bool arg_88_0 = !string.IsNullOrEmpty(loginData.Credentials.UserName) && loginData.Credentials.UserName.Equals(credentialsCertificate.UserName);
					bool flag = string.IsNullOrEmpty(loginData.Credentials.DeviceID) || credentialsCertificate.DeviceID.Equals(loginData.Credentials.DeviceID);
					bool flag2 = DateTime.Now < credentialsCertificate.Time;
					loginData2.Successful = (flag && flag2);
					if (loginData2.Successful)
					{
						loginData2.LevelAttained = AccessLevels.CERTIFICATE;
					}
					result = loginData2;
				}
			}
			return result;
		}

		private LoginData QuerryCredentialsAdmin(LoginData loginData)
		{
			LoginData loginData2 = new LoginData(loginData);
			LoginData result;
			if (loginData == null)
			{
				result = loginData2;
			}
			else
			{
				bool flag = this.IsUserValidOnline(loginData.Credentials.UserName, loginData.Password, "solaredge");
				if (flag)
				{
					loginData2.LevelAttained = AccessLevels.ADMIN;
				}
				else if (this.ShouldValidateAdminOffline)
				{
					bool flag2 = this.IsUserValidOffline(loginData.Credentials.UserName, loginData.Password, "solaredge");
					if (flag2)
					{
						loginData2.LevelAttained = AccessLevels.ADMIN;
					}
				}
				loginData2.Successful = (loginData2.LevelAttained == AccessLevels.ADMIN);
				result = loginData2;
			}
			return result;
		}

		private byte GetKeyAlgorithm(string deviceID)
		{
			byte b = 0;
			byte result;
			if (string.IsNullOrEmpty(deviceID))
			{
				result = b;
			}
			else
			{
				int length = deviceID.Length;
				for (int i = 0; i < length; i++)
				{
					char value = deviceID[i];
					byte b2 = Convert.ToByte(value);
					b ^= b2;
				}
				result = b;
			}
			return result;
		}

		private LoginCredentials GetCredentialsTechnician(string password, byte key)
		{
			LoginCredentials result = null;
			try
			{
				string text = this.ParsePassword(password, key);
				string[] array = text.Split(new string[]
				{
					"#"
				}, StringSplitOptions.None);
				result = new LoginCredentials(null, array[0], this.GetTimeFromString(array[1]));
			}
			catch (Exception var_3_3E)
			{
			}
			return result;
		}

		private LoginCredentials GetCredentialsCertificate(string password, byte key)
		{
			LoginCredentials result = null;
			try
			{
				string text = this.ParsePassword(password, key);
				string[] array = text.Split(new string[]
				{
					"#"
				}, StringSplitOptions.None);
				result = new LoginCredentials(array[0], array[1], this.GetTimeFromString(array[2]));
			}
			catch (Exception var_3_40)
			{
			}
			return result;
		}

		public string GetPasswordTechnician(LoginCredentials credentials)
		{
			string result = null;
			try
			{
				byte keyAlgorithm = this.GetKeyAlgorithm(credentials.DeviceID);
				string strForPass = credentials.DeviceID + "#" + this.GetStringFromTime(credentials.Time);
				result = this.BuildPassword(strForPass, keyAlgorithm);
			}
			catch (Exception var_3_3A)
			{
			}
			return result;
		}

		public string GetPasswordCertificate(LoginCredentials credentials)
		{
			string result = null;
			try
			{
				byte keyAlgorithm = this.GetKeyAlgorithm(credentials.DeviceID);
				string strForPass = string.Concat(new string[]
				{
					credentials.UserName,
					"#",
					credentials.DeviceID,
					"#",
					this.GetStringFromTime(credentials.Time)
				});
				result = this.BuildPassword(strForPass, keyAlgorithm);
			}
			catch (Exception var_3_63)
			{
			}
			return result;
		}

		private string BuildPassword(string strForPass, byte key)
		{
			string result;
			if (string.IsNullOrEmpty(strForPass))
			{
				result = null;
			}
			else
			{
				byte[] rawByteArray = this.GetRawByteArray(strForPass);
				this.ShiftArray(ref rawByteArray, key);
				string rawBitString = this.GetRawBitString(rawByteArray);
				int[] positionListFromBitString = this.GetPositionListFromBitString(rawBitString);
				string passwordString = this.GetPasswordString(positionListFromBitString);
				result = passwordString;
			}
			return result;
		}

		private string ParsePassword(string password, byte key)
		{
			string result;
			if (string.IsNullOrEmpty(password))
			{
				result = null;
			}
			else
			{
				int[] passwordArray = this.GetPasswordArray(password);
				string bitStringFromPositionList = this.GetBitStringFromPositionList(passwordArray);
				byte[] rawBitByteArray = this.GetRawBitByteArray(bitStringFromPositionList);
				this.ShiftArray(ref rawBitByteArray, key);
				string rawString = this.GetRawString(rawBitByteArray);
				result = rawString;
			}
			return result;
		}

		private byte[] GetRawByteArray(string strRaw)
		{
			byte[] result;
			if (string.IsNullOrEmpty(strRaw))
			{
				result = null;
			}
			else
			{
				int length = strRaw.Length;
				List<byte> list = new List<byte>(0);
				for (int i = 0; i < length; i++)
				{
					char value = strRaw[i];
					byte item = Convert.ToByte(value);
					list.Add(item);
				}
				result = list.ToArray();
			}
			return result;
		}

		private string GetRawString(byte[] arrRaw)
		{
			string result;
			if (arrRaw == null || arrRaw.Length <= 0)
			{
				result = null;
			}
			else
			{
				int num = arrRaw.Length;
				StringBuilder stringBuilder = new StringBuilder(0);
				for (int i = 0; i < num; i++)
				{
					byte value = arrRaw[i];
					stringBuilder.Append(Convert.ToChar(value).ToString());
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		private void ShiftArray(ref byte[] arr, byte key)
		{
			if (arr != null && arr.Length > 0)
			{
				int num = arr.Length;
				for (int i = 0; i < num; i++)
				{
					arr[i] ^= key;
				}
			}
		}

		private string GetRawBitString(byte[] arrShift)
		{
			string result;
			if (arrShift == null || arrShift.Length <= 0)
			{
				result = null;
			}
			else
			{
				int num = arrShift.Length;
				StringBuilder stringBuilder = new StringBuilder(0);
				for (int i = 0; i < num; i++)
				{
					byte value = arrShift[i];
					string text = Convert.ToString(value, 2);
					text = this.FillZeroes(text, 8);
					stringBuilder.Append(text);
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		private byte[] GetRawBitByteArray(string bitString)
		{
			byte[] result;
			if (string.IsNullOrEmpty(bitString))
			{
				result = null;
			}
			else
			{
				int length = bitString.Length;
				StringBuilder stringBuilder = null;
				List<byte> list = new List<byte>(0);
				for (int i = 0; i < length; i++)
				{
					if (i % 8 == 0)
					{
						if (stringBuilder != null)
						{
							string value = stringBuilder.ToString();
							byte item = Convert.ToByte(value, 2);
							list.Add(item);
						}
						stringBuilder = new StringBuilder(0);
					}
					stringBuilder.Append(bitString[i].ToString());
				}
				if (stringBuilder != null && stringBuilder.Length > 0)
				{
					string value = stringBuilder.ToString();
					byte item = Convert.ToByte(value, 2);
					list.Add(item);
				}
				result = list.ToArray();
			}
			return result;
		}

		private int[] GetPositionListFromBitString(string bitString)
		{
			int[] result;
			if (string.IsNullOrEmpty(bitString))
			{
				result = null;
			}
			else
			{
				int length = bitString.Length;
				StringBuilder stringBuilder = null;
				List<int> list = new List<int>(0);
				for (int i = 0; i < length; i++)
				{
					if (i % 5 == 0)
					{
						if (stringBuilder != null)
						{
							string text = stringBuilder.ToString();
							int item = Convert.ToInt32(text, 2);
							list.Add(item);
						}
						stringBuilder = new StringBuilder(0);
					}
					stringBuilder.Append(bitString[i].ToString());
				}
				if (stringBuilder != null && stringBuilder.Length > 0)
				{
					string text = stringBuilder.ToString();
					int item2 = 5 - text.Length;
					text = this.FillZeroes(text, 5);
					int item = Convert.ToInt32(text, 2);
					list.Add(item);
					list.Add(item2);
				}
				result = list.ToArray();
			}
			return result;
		}

		private string GetBitStringFromPositionList(int[] intArr)
		{
			string result;
			if (intArr == null || intArr.Length <= 0)
			{
				result = null;
			}
			else
			{
				int num = intArr.Length - 1;
				StringBuilder stringBuilder = new StringBuilder(0);
				for (int i = 0; i < num; i++)
				{
					int num2 = intArr[i];
					string text = Convert.ToString(num2, 2);
					text = this.FillZeroes(text, 5);
					if (i == num - 1)
					{
						int startIndex = intArr[intArr.Length - 1];
						text = text.Substring(startIndex);
					}
					stringBuilder.Append(text);
					Trace.WriteLine(string.Format("{0:00} / {1:00000}", num2, text));
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		private string GetPasswordString(int[] arr)
		{
			string result;
			if (arr == null || arr.Length <= 0)
			{
				result = null;
			}
			else
			{
				int num = arr.Length;
				StringBuilder stringBuilder = new StringBuilder(0);
				for (int i = 0; i < num; i++)
				{
					int index = arr[i];
					string value = "ABCDEFGHIJKLM0123456789NOPQRSTUVWXYZ"[index].ToString();
					stringBuilder.Append(value);
					if ((i + 1) % 6 == 0 && i < num - 1)
					{
						stringBuilder.Append("-");
					}
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		private int[] GetPasswordArray(string str)
		{
			int[] result;
			if (string.IsNullOrEmpty(str))
			{
				result = null;
			}
			else
			{
				List<int> list = new List<int>(0);
				List<char> list2 = "ABCDEFGHIJKLM0123456789NOPQRSTUVWXYZ".ToCharArray().ToList<char>();
				string[] array = str.Split(new string[]
				{
					"-"
				}, StringSplitOptions.None);
				int num = array.Length;
				for (int i = 0; i < num; i++)
				{
					string text = array[i];
					int length = text.Length;
					for (int j = 0; j < length; j++)
					{
						char item = text[j];
						int item2 = list2.IndexOf(item);
						list.Add(item2);
					}
				}
				result = list.ToArray();
			}
			return result;
		}

		private string GetStringFromTime(DateTime time)
		{
			string result = null;
			try
			{
				result = string.Format("{0:00}{1:00}{2:0000}", time.Day, time.Month, time.Year);
			}
			catch (Exception var_1_36)
			{
			}
			return result;
		}

		private DateTime GetTimeFromString(string timeStr)
		{
			DateTime result;
			if (string.IsNullOrEmpty(timeStr))
			{
				result = new DateTime(1, 1, 1);
			}
			else
			{
				string s = timeStr.Substring(0, 2);
				string s2 = timeStr.Substring(2, 2);
				string s3 = timeStr.Substring(4, 4);
				DateTime dateTime = new DateTime(int.Parse(s3), int.Parse(s2), int.Parse(s));
				result = dateTime;
			}
			return result;
		}

		private string FillZeroes(string str, int maxNum)
		{
			string result;
			if (str == null || str.Length <= 0 || str.Length >= maxNum)
			{
				result = str;
			}
			else
			{
				int count = maxNum - str.Length;
				StringBuilder stringBuilder = new StringBuilder(0);
				stringBuilder.Append(str);
				stringBuilder.Insert(0, "0", count);
				result = stringBuilder.ToString();
			}
			return result;
		}

		private bool IsUserValidOffline(string username, string password, string domainName)
		{
			int num;
			bool flag = LoginItem.LogonUser(username, domainName, password, 3, 0, out num);
			bool result;
			if (!flag)
			{
				result = flag;
			}
			else
			{
				bool flag2 = false;
				WindowsIdentity current = WindowsIdentity.GetCurrent();
				SecurityIdentifier accountDomainSid = current.User.AccountDomainSid;
				byte[] array = new byte[accountDomainSid.BinaryLength];
				accountDomainSid.GetBinaryForm(array, 0);
				string text = "";
				byte[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					byte b = array2[i];
					text += b.ToString("X2");
				}
				if (text.Equals("010400000000000515000000268E37D8499D3A1470726BB8"))
				{
					flag2 = true;
				}
				flag &= flag2;
				result = flag;
			}
			return result;
		}

		private bool IsUserValidOnline(string username, string password, string domain)
		{
			string username2 = domain + "\\" + username;
			DirectoryEntry searchRoot = new DirectoryEntry("LDAP://DC=solaredge,DC=local", username2, password);
			bool result;
			try
			{
				SearchResult searchResult = new DirectorySearcher(searchRoot)
				{
					Filter = "(SAMAccountName=" + username + ")",
					PropertiesToLoad = 
					{
						"cn"
					}
				}.FindOne();
				if (null == searchResult)
				{
					result = false;
					return result;
				}
			}
			catch (Exception var_4_67)
			{
				result = false;
				return result;
			}
			result = true;
			return result;
		}
	}
}
