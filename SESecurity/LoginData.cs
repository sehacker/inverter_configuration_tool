using System;

namespace SESecurity
{
	public class LoginData
	{
		private LoginCredentials _credentials = null;

		private string _password = null;

		private AccessLevels _levelRequested = AccessLevels.MONITORING;

		private AccessLevels _levelAttained = AccessLevels.MONITORING;

		private bool _successful = false;

		public LoginCredentials Credentials
		{
			get
			{
				return this._credentials;
			}
			set
			{
				this._credentials = value;
			}
		}

		public string Password
		{
			get
			{
				return this._password;
			}
			set
			{
				this._password = value;
			}
		}

		public AccessLevels LevelRequested
		{
			get
			{
				return this._levelRequested;
			}
			set
			{
				this._levelRequested = value;
			}
		}

		public AccessLevels LevelAttained
		{
			get
			{
				return this._levelAttained;
			}
			set
			{
				this._levelAttained = value;
			}
		}

		public bool Successful
		{
			get
			{
				return this._successful;
			}
			set
			{
				this._successful = value;
			}
		}

		public LoginData()
		{
		}

		public LoginData(LoginData data)
		{
			if (data != null)
			{
				this.Credentials = data.Credentials;
				this.Password = data.Password;
				this.LevelAttained = data.LevelAttained;
				this.LevelRequested = data.LevelRequested;
			}
		}

		public LoginData(LoginCredentials credentials, string password, AccessLevels entryLevel)
		{
			this.Credentials = credentials;
			this.Password = password;
			this.LevelAttained = entryLevel;
		}
	}
}
