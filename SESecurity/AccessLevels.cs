using System;

namespace SESecurity
{
	public enum AccessLevels
	{
		MONITORING,
		TECHNICIAN,
		CERTIFICATE,
		ADMIN,
		COUNT
	}
}
