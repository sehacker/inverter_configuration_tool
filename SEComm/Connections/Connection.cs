using System;
using System.IO;
using System.Runtime.InteropServices;

namespace SEComm.Connections
{
	public abstract class Connection : Stream
	{
		private static bool _shouldReconnectOnError = false;

		protected Stream _stream;

		protected object _isConnectionSynchObj = new object();

		private object _readLock;

		private object _writeLock;

		private int _lastWriteDuration;

		private int _lastReadDuration;

		private bool _isRemoteConnection;

		public virtual bool IsConnected
		{
			get
			{
				bool result;
				lock (this._isConnectionSynchObj)
				{
					result = false;
				}
				return result;
			}
		}

		public bool RemoteConnection
		{
			get
			{
				bool isRemoteConnection;
				lock (this)
				{
					isRemoteConnection = this._isRemoteConnection;
				}
				return isRemoteConnection;
			}
			set
			{
				lock (this)
				{
					this._isRemoteConnection = value;
				}
			}
		}

		public override bool CanRead
		{
			get
			{
				bool canRead;
				lock (this)
				{
					canRead = this._stream.CanRead;
				}
				return canRead;
			}
		}

		public override bool CanSeek
		{
			get
			{
				bool canSeek;
				lock (this)
				{
					canSeek = this._stream.CanSeek;
				}
				return canSeek;
			}
		}

		[ComVisible(false)]
		public override bool CanTimeout
		{
			get
			{
				bool canTimeout;
				lock (this)
				{
					canTimeout = this._stream.CanTimeout;
				}
				return canTimeout;
			}
		}

		public override bool CanWrite
		{
			get
			{
				bool canWrite;
				lock (this)
				{
					canWrite = this._stream.CanWrite;
				}
				return canWrite;
			}
		}

		public override long Length
		{
			get
			{
				long length;
				lock (this)
				{
					length = this._stream.Length;
				}
				return length;
			}
		}

		public override long Position
		{
			get
			{
				long position;
				lock (this)
				{
					position = this._stream.Position;
				}
				return position;
			}
			set
			{
				lock (this)
				{
					if (this._stream == null || !this.IsConnected)
					{
						this.Reconnect();
					}
					this._stream.Position = value;
				}
			}
		}

		[ComVisible(false)]
		public override int ReadTimeout
		{
			get
			{
				int readTimeout;
				lock (this)
				{
					readTimeout = this._stream.ReadTimeout;
				}
				return readTimeout;
			}
			set
			{
				lock (this)
				{
					this._stream.ReadTimeout = value;
					this._lastReadDuration = value;
				}
			}
		}

		[ComVisible(false)]
		public override int WriteTimeout
		{
			get
			{
				int writeTimeout;
				lock (this)
				{
					writeTimeout = this._stream.WriteTimeout;
				}
				return writeTimeout;
			}
			set
			{
				lock (this)
				{
					this._stream.WriteTimeout = value;
					this._lastWriteDuration = value;
				}
			}
		}

		public Connection()
		{
			Connection._shouldReconnectOnError = true;
			this._writeLock = new object();
			this._readLock = new object();
		}

		public virtual void OpenConnection()
		{
		}

		public void CloseConnection()
		{
			this.CloseConnection(true);
		}

		protected virtual void CloseConnection(bool isUserInited)
		{
			if (isUserInited)
			{
				Connection._shouldReconnectOnError = false;
			}
			this._stream.Close();
			this._stream.Dispose();
			this._stream = null;
		}

		private void Reconnect()
		{
			lock (this)
			{
				if (!this.IsConnected)
				{
					if (Connection._shouldReconnectOnError)
					{
						this.CloseConnection(false);
						this.OpenConnection();
						if (this._stream != null)
						{
							this._stream.ReadTimeout = this._lastReadDuration;
							this._stream.WriteTimeout = this._lastWriteDuration;
						}
					}
				}
			}
		}

		public override void Flush()
		{
			lock (this)
			{
				this._stream.Flush();
			}
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = Environment.TickCount + this._lastReadDuration;
			int result;
			lock (this._readLock)
			{
				while (Environment.TickCount < num || this._lastReadDuration == -1)
				{
					if (this._stream == null || !this.IsConnected)
					{
						this.Reconnect();
					}
					try
					{
						if (this._lastReadDuration != -1)
						{
							this._stream.ReadTimeout = Math.Max(num - Environment.TickCount, 0);
						}
						result = this._stream.Read(buffer, offset, count);
						return result;
					}
					catch (TimeoutException var_1_7B)
					{
					}
					catch (Exception var_2_80)
					{
						this.Reconnect();
					}
				}
				throw new TimeoutException();
			}
			return result;
		}

		public override int ReadByte()
		{
			int num = Environment.TickCount + this._lastReadDuration;
			int result;
			lock (this._readLock)
			{
				while (Environment.TickCount < num || this._lastReadDuration == -1)
				{
					if (this._stream == null || !this.IsConnected)
					{
						this.Reconnect();
					}
					try
					{
						if (this._lastReadDuration != -1)
						{
							this._stream.ReadTimeout = Math.Max(num - Environment.TickCount, 0);
						}
						result = this._stream.ReadByte();
						return result;
					}
					catch (TimeoutException var_1_78)
					{
					}
					catch (Exception var_2_7D)
					{
						this.Reconnect();
					}
				}
				throw new TimeoutException();
			}
			return result;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			long result;
			lock (this)
			{
				result = this._stream.Seek(offset, origin);
			}
			return result;
		}

		public override void SetLength(long value)
		{
			lock (this)
			{
				this._stream.SetLength(value);
			}
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			int num = Environment.TickCount + this._lastWriteDuration;
			lock (this._writeLock)
			{
				while (Environment.TickCount < num || this._lastWriteDuration == -1)
				{
					if (this._stream == null || !this.IsConnected)
					{
						this.Reconnect();
					}
					try
					{
						if (this._lastWriteDuration != -1)
						{
							this._stream.WriteTimeout = Math.Max(num - Environment.TickCount, 0);
						}
						this._stream.Write(buffer, offset, count);
						return;
					}
					catch (TimeoutException var_1_7A)
					{
					}
					catch (Exception var_2_7F)
					{
						this.Reconnect();
					}
				}
				throw new TimeoutException();
			}
		}

		public override void WriteByte(byte value)
		{
			int num = Environment.TickCount + this._lastWriteDuration;
			lock (this._writeLock)
			{
				while (Environment.TickCount < num || this._lastWriteDuration == -1)
				{
					if (this._stream == null || !this.IsConnected)
					{
						this.Reconnect();
					}
					try
					{
						if (this._lastWriteDuration != -1)
						{
							this._stream.WriteTimeout = Math.Max(num - Environment.TickCount, 0);
						}
						this._stream.WriteByte(value);
						return;
					}
					catch (TimeoutException var_1_78)
					{
					}
					catch (Exception var_2_7D)
					{
						this.Reconnect();
					}
				}
				throw new TimeoutException();
			}
		}
	}
}
