using System;
using System.Collections.Generic;

namespace SEComm.Connections.Types
{
	public class PortReplicatorConnection : TCPConnection
	{
		private const string PR_COMMAND = "polestar {0}\n";

		private int _param_Portia_ID = 0;

		public PortReplicatorConnection(string accessIP, uint accessPort, uint portiaID) : base(accessIP, accessPort)
		{
			this._param_Portia_ID = (int)portiaID;
			base.RemoteConnection = true;
		}

		public override void OpenConnection()
		{
			base.OpenConnection();
			this.ConnectTo((uint)this._param_Portia_ID);
		}

		public void ConnectTo(uint portiaID)
		{
			string text = string.Format("polestar {0}\n", portiaID);
			List<byte> list = new List<byte>(0);
			char[] array = text.ToCharArray();
			int num = array.Length;
			for (int i = 0; i < num; i++)
			{
				char value = array[i];
				byte item = Convert.ToByte(value);
				list.Add(item);
			}
			byte[] array2 = list.ToArray();
			this._stream.Write(array2, 0, array2.Length);
			this._stream.Flush();
		}
	}
}
