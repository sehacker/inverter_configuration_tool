using System;

namespace SEComm.Connections
{
	[Serializable]
	public class ConnectionParams
	{
		private object[] _parameters;

		private Type[] _paramTypes;

		private Type _connectionType;

		private bool _isRemoteConection = false;

		private string _connectionName = null;

		public object[] Parameters
		{
			get
			{
				return this._parameters;
			}
		}

		public Type[] ParameterTypes
		{
			get
			{
				return this._paramTypes;
			}
		}

		public Type ConnectionType
		{
			get
			{
				return this._connectionType;
			}
		}

		public bool RemoteConnection
		{
			get
			{
				return this._isRemoteConection;
			}
		}

		public string Name
		{
			get
			{
				return this._connectionName;
			}
		}

		public ConnectionParams(string name, bool isConcideredRemote, Type connectionType, object[] parameters)
		{
			this._connectionName = name;
			this._connectionType = connectionType;
			this._isRemoteConection = isConcideredRemote;
			this._parameters = parameters;
			if (parameters != null)
			{
				int num = parameters.Length;
				this._paramTypes = new Type[num];
				for (int i = 0; i < num; i++)
				{
					this._paramTypes[i] = parameters[i].GetType();
				}
			}
		}

		public object Get(int index)
		{
			object result = null;
			if (this._parameters != null && this._parameters.Length > 0 && index > -1 && index < this._parameters.Length)
			{
				result = this._parameters[index];
			}
			return result;
		}
	}
}
