using System;
using System.IO;

namespace SEComm
{
	public class ConnectionAccess
	{
		private Stream _baseStream;

		private BinaryWriter _writerBinary;

		private BinaryReader _readerBinary;

		private StreamWriter _writerText;

		private StreamReader _readerText;

		private object _writeLock = new object();

		private object _readLock = new object();

		public ConnectionAccess(Stream stream)
		{
			this._baseStream = stream;
			this.StartAccess();
		}

		~ConnectionAccess()
		{
			this.CloseAccess();
		}

		private bool CanUseRead(bool isBinary)
		{
			return this._baseStream != null && ((isBinary && this._readerBinary != null) || (!isBinary && this._readerText != null)) && this._baseStream.CanRead;
		}

		private bool CanUseWrite(bool isBinary)
		{
			return this._baseStream != null && ((isBinary && this._writerBinary != null) || (!isBinary && this._writerText != null)) && this._baseStream.CanWrite;
		}

		public bool ReadBoolean(int timeOutInMiliSec)
		{
			bool result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadBoolean();
			}
			return result;
		}

		public byte ReadByte(int timeOutInMiliSec)
		{
			byte result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadByte();
			}
			return result;
		}

		public byte[] ReadBytes(int count, int timeOutInMiliSec)
		{
			byte[] result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadBytes(count);
			}
			return result;
		}

		public char ReadChar(int timeOutInMiliSec)
		{
			char result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadChar();
			}
			return result;
		}

		public char[] ReadChars(int count, int timeOutInMiliSec)
		{
			char[] result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadChars(count);
			}
			return result;
		}

		public decimal ReadDecimal(int timeOutInMiliSec)
		{
			decimal result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadDecimal();
			}
			return result;
		}

		public double ReadDouble(int timeOutInMiliSec)
		{
			double result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadDouble();
			}
			return result;
		}

		public short ReadInt16(int timeOutInMiliSec)
		{
			short result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadInt16();
			}
			return result;
		}

		public int ReadInt32(int timeOutInMiliSec)
		{
			int result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadInt32();
			}
			return result;
		}

		public long ReadInt64(int timeOutInMiliSec)
		{
			long result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadInt64();
			}
			return result;
		}

		public ushort ReadUInt16(int timeOutInMiliSec)
		{
			ushort result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadUInt16();
			}
			return result;
		}

		public uint ReadUInt32(int timeOutInMiliSec)
		{
			uint result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadUInt32();
			}
			return result;
		}

		public ulong ReadUInt64(int timeOutInMiliSec)
		{
			ulong result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadUInt64();
			}
			return result;
		}

		public sbyte ReadSByte(int timeOutInMiliSec)
		{
			sbyte result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadSByte();
			}
			return result;
		}

		public float ReadSingle(int timeOutInMiliSec)
		{
			float result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadSingle();
			}
			return result;
		}

		public string ReadString(int timeOutInMiliSec)
		{
			string result;
			lock (this._readLock)
			{
				this.CanUseRead(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerBinary.ReadString();
			}
			return result;
		}

		public void Write(bool value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(byte value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(byte[] value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(char value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(char[] value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(decimal value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(double value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(float value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(int value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(long value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(sbyte value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(short value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(string value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(uint value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(ulong value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public void Write(ushort value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(true);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerBinary.Write(value);
				this._writerBinary.Flush();
			}
		}

		public string ReadLine(int timeOutInMiliSec)
		{
			string result;
			lock (this._readLock)
			{
				this.CanUseRead(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerText.ReadLine();
			}
			return result;
		}

		public string ReadToEnd(int timeOutInMiliSec)
		{
			string result;
			lock (this._readLock)
			{
				this.CanUseRead(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.ReadTimeout = timeOutInMiliSec;
				}
				result = this._readerText.ReadToEnd();
			}
			return result;
		}

		public void TWrite(object value, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerText.Write(value);
				this._writerText.Flush();
			}
		}

		public void TWrite(string format, object arg0, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerText.Write(format, arg0);
				this._writerText.Flush();
			}
		}

		public void TWrite(int timeOutInMiliSec, string format, params object[] arg)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerText.Write(format, arg);
				this._writerText.Flush();
			}
		}

		public void TWrite(char[] vals, int index, int count, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerText.Write(vals, index, count);
				this._writerText.Flush();
			}
		}

		public void TWrite(string format, object arg0, object arg1, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerText.Write(format, arg0, arg1);
				this._writerText.Flush();
			}
		}

		public void TWrite(string format, object arg0, object arg1, object arg2, int timeOutInMiliSec)
		{
			lock (this._writeLock)
			{
				this.CanUseWrite(false);
				if (this._baseStream.CanTimeout)
				{
					this._baseStream.WriteTimeout = timeOutInMiliSec;
				}
				this._writerText.Write(format, arg0, arg1, arg2);
				this._writerText.Flush();
			}
		}

		private void StartAccess()
		{
			this._writerBinary = new BinaryWriter(this._baseStream);
			this._readerBinary = new BinaryReader(this._baseStream);
			this._writerText = new StreamWriter(this._baseStream);
			this._readerText = new StreamReader(this._baseStream);
		}

		private void CloseAccess()
		{
			try
			{
				if (this._writerBinary != null)
				{
					this._writerBinary.Close();
				}
				if (this._readerBinary != null)
				{
					this._readerBinary.Close();
				}
				if (this._writerText != null)
				{
					this._writerText.Close();
				}
				if (this._readerText != null)
				{
					this._readerText.Close();
				}
			}
			catch (Exception var_0_69)
			{
			}
			this._writerBinary = null;
			this._readerBinary = null;
			this._writerText = null;
			this._readerText = null;
		}
	}
}
