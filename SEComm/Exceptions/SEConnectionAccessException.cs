using System;

namespace SEComm.Exceptions
{
	public class SEConnectionAccessException : Exception
	{
		public SEConnectionAccessException()
		{
		}

		public SEConnectionAccessException(string message) : base(message)
		{
		}
	}
}
