using System;

namespace SEComm.Exceptions
{
	public class SEExceptionConnectionAlreadyOpen : Exception
	{
		public SEExceptionConnectionAlreadyOpen()
		{
		}

		public SEExceptionConnectionAlreadyOpen(string message) : base(message)
		{
		}
	}
}
